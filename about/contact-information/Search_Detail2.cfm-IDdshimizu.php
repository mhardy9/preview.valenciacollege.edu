<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/contact-information/Search_Detail2.cfm-IDdshimizu.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/contact-information/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/contact-information/">Contact Information</a></li>
               <li>Valencia Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../support/index.html"></a>
                        
                        <h2>Faculty &amp; Staff Directory</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> 
                                    <a href="directory.html">&amp;lt;&amp;lt; Search Again</a><br>
                                    <br>
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div>
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><font size="+2"><strong>Dominique Shimizu</strong></font></div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><strong>Faculty Devel/Inst Designer</strong></div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <h3>Faculty Development PNC</h3>
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><strong>Phone:</strong></div>
                                                
                                                <div data-old-tag="td">(407) 582-6131</div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><strong>Building/Room:</strong></div>
                                                
                                                <div data-old-tag="td">1-305</div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><strong>Campus:</strong></div>
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><strong>Mail Code:</strong></div>
                                                
                                                <div data-old-tag="td">
                                                   MC: 8-1
                                                </div>
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><strong>Email:</strong></div>
                                                
                                                <div data-old-tag="td"><a href="mailto:dshimizu@valenciacollege.edu">dshimizu@valenciacollege.edu</a></div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>  
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/contact-information/Search_Detail2.cfm-IDdshimizu.pcf">©</a>
      </div>
   </body>
</html>