<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Support | Valencia College</title>
      <meta name="Description" content="Change this to match title A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/contact-information/directory.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/contact-information/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/contact-information/">Contact Information</a></li>
               <li>Valencia Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../support/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Search Offices &amp; Services</h2>
                        
                        <meta content="no" http-equiv="imagetoolbar">
                        
                        <meta content="IE=Edge" http-equiv="X-UA-Compatible">
                        
                        <meta content="https://preview.valenciacollege.edu/images/logos/valencia-college-logo-red.png" property="og:image">
                        
                        <meta content="A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society" name="description">
                        
                        
                        <link href="../includes/styles-v=2.css" rel="stylesheet" type="text/css">
                        
                        <link href="../includes/contribute.css" rel="stylesheet" type="text/css">
                        
                        <link href="../includes/buttons.css" rel="stylesheet" type="text/css">
                        
                        <link href="../includes/headeralert.css" rel="stylesheet" type="text/css">
                        
                        <link href="../includes/nav_expanding.css" rel="stylesheet" type="text/css"> 
                        
                        <link href="../includes/print.css" media="print" rel="stylesheet" type="text/css">
                        
                        <link href="../includes/960grid.css" rel="stylesheet" type="text/css">
                        
                        
                        <link href="../favicon.ico" rel="apple-touch-icon">
                        
                        
                        
                        
                        
                        
                        
                        
                        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
                        
                        
                        <link href="https://atlas.valenciacollege.edu/includes/styles/autocomplete.css" rel="stylesheet">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <input aria-label="Office or Service Lookup" autocomplete="off" placeholder="Start typing an office or service...">
                        <br>
                        
                        
                        
                        
                        
                        <h2>Faculty &amp; Staff Directory</h2>
                        Find faculty and staff phone numbers, extensions, mail codes, room   numbers and email
                        addresses.<br>
                        
                        Please choose <strong>one</strong> of the search   criteria options below options. <br>
                        <br>
                        
                        
                        
                        
                        <div> 
                           
                           
                           
                           <form action="lookup.html" method="post" name="schform" id="schform">
                              <input name="searchType" type="hidden" value="1">
                              
                              <div>
                                 
                                 <h3>Name Search</h3>
                                 <label for="FName">First Name</label>
                                 <br>
                                 <input name="FName" size="75" type="text" value="">
                                 <br>
                                 <br>
                                 <label for="LName">Last Name</label>
                                 <br>
                                 <input name="LName" size="75" type="text" value="">
                                 <br>
                                 <br>
                                 <label for="Title">Title</label>
                                 <br>
                                 <input name="Title" size="75" type="text" value="">
                                 <br>
                                 <br>
                                 <input type="submit" value="Search">
                                 
                              </div>
                              
                              <hr>
                              
                           </form>
                           <br>
                           
                           
                           
                           
                           <form action="lookup.html" method="post" name="deptform" id="deptform">
                              <input name="searchType" type="hidden" value="2">
                              
                              <div>
                                 
                                 <h3>Department Search</h3>
                                 <label for="DeptName">Select Department Name</label>
                                 <br>
                                 <select name="Dept" size="1">
                                    
                                    <option selected value="">Please choose...</option>
                                    
                                    
                                    <option value="AVP Operations">AVP Operations</option>
                                    
                                    
                                    <option value="Academic Affairs">Academic Affairs</option>
                                    
                                    
                                    <option value="Academic Success Center">Academic Success Center</option>
                                    
                                    
                                    <option value="Accounting LNC">Accounting LNC</option>
                                    
                                    
                                    <option value="Accounting PNC">Accounting PNC</option>
                                    
                                    
                                    <option value="Allied Health Office">Allied Health Office</option>
                                    
                                    
                                    <option value="Alumni Relations">Alumni Relations</option>
                                    
                                    
                                    <option value="Anthropology EAC">Anthropology EAC</option>
                                    
                                    
                                    <option value="Anthropology WEC">Anthropology WEC</option>
                                    
                                    
                                    <option value="Application Development">Application Development</option>
                                    
                                    
                                    <option value="Aquaponics and Hydroponics WEC">Aquaponics and Hydroponics WEC</option>
                                    
                                    
                                    <option value="Architectural Design WEC">Architectural Design WEC</option>
                                    
                                    
                                    <option value="Art EAC">Art EAC</option>
                                    
                                    
                                    <option value="Art Gallery EAC">Art Gallery EAC</option>
                                    
                                    
                                    <option value="Art WEC">Art WEC</option>
                                    
                                    
                                    <option value="Arts &amp; Entertainment">Arts &amp; Entertainment</option>
                                    
                                    
                                    <option value="Arts &amp; Humanities Office WEC">Arts &amp; Humanities Office WEC</option>
                                    
                                    
                                    <option value="Assessment and Testing Center OSC">Assessment and Testing Center OSC</option>
                                    
                                    
                                    <option value="Bachelors American Sign Lang EAC">Bachelors American Sign Lang EAC</option>
                                    
                                    
                                    <option value="Behavorial &amp; Social Sciences WEC">Behavorial &amp; Social Sciences WEC</option>
                                    
                                    
                                    <option value="BioTechnology LNC">BioTechnology LNC</option>
                                    
                                    
                                    <option value="Biological Science EAC">Biological Science EAC</option>
                                    
                                    
                                    <option value="Biological Science OSC">Biological Science OSC</option>
                                    
                                    
                                    <option value="Biological Science WEC">Biological Science WEC</option>
                                    
                                    
                                    <option value="Biological Science WPK">Biological Science WPK</option>
                                    
                                    
                                    <option value="Biology LNC">Biology LNC</option>
                                    
                                    
                                    <option value="Bridges">Bridges</option>
                                    
                                    
                                    <option value="Budget Office">Budget Office</option>
                                    
                                    
                                    <option value="Building Construction WEC">Building Construction WEC</option>
                                    
                                    
                                    <option value="Bursar">Bursar</option>
                                    
                                    
                                    <option value="Business &amp; Hospitality Office">Business &amp; Hospitality Office</option>
                                    
                                    
                                    <option value="Business &amp; IT Office EAC">Business &amp; IT Office EAC</option>
                                    
                                    
                                    <option value="Business &amp; IT Office OSC">Business &amp; IT Office OSC</option>
                                    
                                    
                                    <option value="Business EAC">Business EAC</option>
                                    
                                    
                                    <option value="Business OSC">Business OSC</option>
                                    
                                    
                                    <option value="Business Office EAC">Business Office EAC</option>
                                    
                                    
                                    <option value="Business Office OSC">Business Office OSC</option>
                                    
                                    
                                    <option value="Business Office WEC">Business Office WEC</option>
                                    
                                    
                                    <option value="Business PNC">Business PNC</option>
                                    
                                    
                                    <option value="Business WEC">Business WEC</option>
                                    
                                    
                                    <option value="Business WPK">Business WPK</option>
                                    
                                    
                                    <option value="CCI Nova Subaward Dept of State ECA">CCI Nova Subaward Dept of State ECA</option>
                                    
                                    
                                    <option value="CE Administration">CE Administration</option>
                                    
                                    
                                    <option value="CE Advanced Manufacturing">CE Advanced Manufacturing</option>
                                    
                                    
                                    <option value="CE Business Administration">CE Business Administration</option>
                                    
                                    
                                    <option value="CE Business Agreements">CE Business Agreements</option>
                                    
                                    
                                    <option value="CE Business Open Enrollment">CE Business Open Enrollment</option>
                                    
                                    
                                    <option value="CE CIE Administration">CE CIE Administration</option>
                                    
                                    
                                    <option value="CE CIE Intensive English">CE CIE Intensive English</option>
                                    
                                    
                                    <option value="CE CIE Language Program">CE CIE Language Program</option>
                                    
                                    
                                    <option value="CE CIE Youth Programs">CE CIE Youth Programs</option>
                                    
                                    
                                    <option value="CE Client Services Center">CE Client Services Center</option>
                                    
                                    
                                    <option value="CE Construction">CE Construction</option>
                                    
                                    
                                    <option value="CE Custodial Services">CE Custodial Services</option>
                                    
                                    
                                    <option value="CE Fire Program">CE Fire Program</option>
                                    
                                    
                                    <option value="CE Operations">CE Operations</option>
                                    
                                    
                                    <option value="CE Peace &amp; Justice Institute">CE Peace &amp; Justice Institute</option>
                                    
                                    
                                    <option value="CE Public Safety">CE Public Safety</option>
                                    
                                    
                                    <option value="CJI Trust Fund">CJI Trust Fund</option>
                                    
                                    
                                    <option value="CJI Trust Fund 1718">CJI Trust Fund 1718</option>
                                    
                                    
                                    <option value="Campus President EAC">Campus President EAC</option>
                                    
                                    
                                    <option value="Campus President OSC">Campus President OSC</option>
                                    
                                    
                                    <option value="Campus President WEC">Campus President WEC</option>
                                    
                                    
                                    <option value="Campus Security Services">Campus Security Services</option>
                                    
                                    
                                    <option value="Campus Security Svcs EAC">Campus Security Svcs EAC</option>
                                    
                                    
                                    <option value="Campus Security Svcs LNC">Campus Security Svcs LNC</option>
                                    
                                    
                                    <option value="Campus Security Svcs OSC">Campus Security Svcs OSC</option>
                                    
                                    
                                    <option value="Campus Security Svcs PNC">Campus Security Svcs PNC</option>
                                    
                                    
                                    <option value="Campus Security Svcs WEC">Campus Security Svcs WEC</option>
                                    
                                    
                                    <option value="Campus Security Svcs WPK">Campus Security Svcs WPK</option>
                                    
                                    
                                    <option value="Campus Store EAC">Campus Store EAC</option>
                                    
                                    
                                    <option value="Campus Store LNC">Campus Store LNC</option>
                                    
                                    
                                    <option value="Campus Store OSC">Campus Store OSC</option>
                                    
                                    
                                    <option value="Campus Store Operations">Campus Store Operations</option>
                                    
                                    
                                    <option value="Campus Store WEC">Campus Store WEC</option>
                                    
                                    
                                    <option value="Campus Store WPK">Campus Store WPK</option>
                                    
                                    
                                    <option value="Campus Technology Partners EAC">Campus Technology Partners EAC</option>
                                    
                                    
                                    <option value="Campus Technology Partners LNC">Campus Technology Partners LNC</option>
                                    
                                    
                                    <option value="Campus Technology Partners OSC">Campus Technology Partners OSC</option>
                                    
                                    
                                    <option value="Campus Technology Partners WEC">Campus Technology Partners WEC</option>
                                    
                                    
                                    <option value="Campus Technology Partners WPK">Campus Technology Partners WPK</option>
                                    
                                    
                                    <option value="Campus Technology Services WEC">Campus Technology Services WEC</option>
                                    
                                    
                                    <option value="Cardiopulmonary Sciences WEC">Cardiopulmonary Sciences WEC</option>
                                    
                                    
                                    <option value="Cardiovascular Tech WEC">Cardiovascular Tech WEC</option>
                                    
                                    
                                    <option value="Career Development EAC">Career Development EAC</option>
                                    
                                    
                                    <option value="Career Development OSC">Career Development OSC</option>
                                    
                                    
                                    <option value="Career Development WEC">Career Development WEC</option>
                                    
                                    
                                    <option value="Career Pathways">Career Pathways</option>
                                    
                                    
                                    <option value="Career Program Advising EAC">Career Program Advising EAC</option>
                                    
                                    
                                    <option value="Career Program Advising OSC">Career Program Advising OSC</option>
                                    
                                    
                                    <option value="Career Program Advising WEC">Career Program Advising WEC</option>
                                    
                                    
                                    <option value="Career and Workforce Education">Career and Workforce Education</option>
                                    
                                    
                                    <option value="Chemistry EAC">Chemistry EAC</option>
                                    
                                    
                                    <option value="Chemistry LNC">Chemistry LNC</option>
                                    
                                    
                                    <option value="Chemistry OSC">Chemistry OSC</option>
                                    
                                    
                                    <option value="Chemistry WEC">Chemistry WEC</option>
                                    
                                    
                                    <option value="Clinical Compliance">Clinical Compliance</option>
                                    
                                    
                                    <option value="Collaborative Design Center WEC">Collaborative Design Center WEC</option>
                                    
                                    
                                    <option value="College Prep English EAC">College Prep English EAC</option>
                                    
                                    
                                    <option value="College Prep English OSC">College Prep English OSC</option>
                                    
                                    
                                    <option value="College Reading Prep WEC">College Reading Prep WEC</option>
                                    
                                    
                                    <option value="Communication Center WEC">Communication Center WEC</option>
                                    
                                    
                                    <option value="Communication Center WPK">Communication Center WPK</option>
                                    
                                    
                                    <option value="Communications Office EAC">Communications Office EAC</option>
                                    
                                    
                                    <option value="Communications Office OSC">Communications Office OSC</option>
                                    
                                    
                                    <option value="Communications Office WEC">Communications Office WEC</option>
                                    
                                    
                                    <option value="Computer Engineering EAC">Computer Engineering EAC</option>
                                    
                                    
                                    <option value="Computer Engineering OSC">Computer Engineering OSC</option>
                                    
                                    
                                    <option value="Computer Program EAC">Computer Program EAC</option>
                                    
                                    
                                    <option value="Computer Program OSC">Computer Program OSC</option>
                                    
                                    
                                    <option value="Computer Program WEC">Computer Program WEC</option>
                                    
                                    
                                    <option value="Conferencing &amp; College Events">Conferencing &amp; College Events</option>
                                    
                                    
                                    <option value="Courier Services">Courier Services</option>
                                    
                                    
                                    <option value="Criminal Justice Institute SPS">Criminal Justice Institute SPS</option>
                                    
                                    
                                    <option value="Criminal Justice Tech EAC">Criminal Justice Tech EAC</option>
                                    
                                    
                                    <option value="Criminal Justice Tech OSC">Criminal Justice Tech OSC</option>
                                    
                                    
                                    <option value="Criminal Justice Tech WEC">Criminal Justice Tech WEC</option>
                                    
                                    
                                    <option value="Culinary Arts WEC">Culinary Arts WEC</option>
                                    
                                    
                                    <option value="Curriculum &amp; Articulation">Curriculum &amp; Articulation</option>
                                    
                                    
                                    <option value="Custodial Services DO">Custodial Services DO</option>
                                    
                                    
                                    <option value="Custodial Services EAC">Custodial Services EAC</option>
                                    
                                    
                                    <option value="Custodial Services LNC">Custodial Services LNC</option>
                                    
                                    
                                    <option value="Custodial Services OSC">Custodial Services OSC</option>
                                    
                                    
                                    <option value="Custodial Services PNC">Custodial Services PNC</option>
                                    
                                    
                                    <option value="Custodial Services WEC">Custodial Services WEC</option>
                                    
                                    
                                    <option value="Custodial Services WPK">Custodial Services WPK</option>
                                    
                                    
                                    <option value="Dance EAC">Dance EAC</option>
                                    
                                    
                                    <option value="Dean of Students EAC">Dean of Students EAC</option>
                                    
                                    
                                    <option value="Dean of Students OSC">Dean of Students OSC</option>
                                    
                                    
                                    <option value="Dean of Students PNC">Dean of Students PNC</option>
                                    
                                    
                                    <option value="Dean of Students WEC">Dean of Students WEC</option>
                                    
                                    
                                    <option value="Dean of Students WPK">Dean of Students WPK</option>
                                    
                                    
                                    <option value="Dental Hygiene WEC">Dental Hygiene WEC</option>
                                    
                                    
                                    <option value="Digital Media EAC">Digital Media EAC</option>
                                    
                                    
                                    <option value="Drafting &amp; Design EAC">Drafting &amp; Design EAC</option>
                                    
                                    
                                    <option value="Drafting &amp; Design WEC">Drafting &amp; Design WEC</option>
                                    
                                    
                                    <option value="Dual Enrollment">Dual Enrollment</option>
                                    
                                    
                                    <option value="EAP EAC">EAP EAC</option>
                                    
                                    
                                    <option value="EAP OSC">EAP OSC</option>
                                    
                                    
                                    <option value="EAP PNC">EAP PNC</option>
                                    
                                    
                                    <option value="EAP WEC">EAP WEC</option>
                                    
                                    
                                    <option value="Earth Science EAC">Earth Science EAC</option>
                                    
                                    
                                    <option value="Earth Science OSC">Earth Science OSC</option>
                                    
                                    
                                    <option value="Earth Science WEC">Earth Science WEC</option>
                                    
                                    
                                    <option value="Earth Science WPK">Earth Science WPK</option>
                                    
                                    
                                    <option value="Economics EAC">Economics EAC</option>
                                    
                                    
                                    <option value="Economics LNC">Economics LNC</option>
                                    
                                    
                                    <option value="Economics OSC">Economics OSC</option>
                                    
                                    
                                    <option value="Economics WEC">Economics WEC</option>
                                    
                                    
                                    <option value="Economics WPK">Economics WPK</option>
                                    
                                    
                                    <option value="Education EAC">Education EAC</option>
                                    
                                    
                                    <option value="Education OSC">Education OSC</option>
                                    
                                    
                                    <option value="Education WEC">Education WEC</option>
                                    
                                    
                                    <option value="Education WPK">Education WPK</option>
                                    
                                    
                                    <option value="Educational Partnerships">Educational Partnerships</option>
                                    
                                    
                                    <option value="Educator Prep Institute">Educator Prep Institute</option>
                                    
                                    
                                    <option value="Electrical &amp; Computer Engineer WEC">Electrical &amp; Computer Engineer WEC</option>
                                    
                                    
                                    <option value="Electronics Engineering WEC">Electronics Engineering WEC</option>
                                    
                                    
                                    <option value="Emergency Medical Svcs OSC">Emergency Medical Svcs OSC</option>
                                    
                                    
                                    <option value="Emergency Medical Svcs WEC">Emergency Medical Svcs WEC</option>
                                    
                                    
                                    <option value="Employee Development">Employee Development</option>
                                    
                                    
                                    <option value="Employment and Onboarding">Employment and Onboarding</option>
                                    
                                    
                                    <option value="Engineering &amp; IT Office WEC">Engineering &amp; IT Office WEC</option>
                                    
                                    
                                    <option value="Engineering WEC">Engineering WEC</option>
                                    
                                    
                                    <option value="English EAC">English EAC</option>
                                    
                                    
                                    <option value="English LNC">English LNC</option>
                                    
                                    
                                    <option value="English OSC">English OSC</option>
                                    
                                    
                                    <option value="English PNC">English PNC</option>
                                    
                                    
                                    <option value="English WEC">English WEC</option>
                                    
                                    
                                    <option value="English WPK">English WPK</option>
                                    
                                    
                                    <option value="Enrollment Services">Enrollment Services</option>
                                    
                                    
                                    <option value="Exec Dean's Office LNC">Exec Dean's Office LNC</option>
                                    
                                    
                                    <option value="Exec Dean's Office PNC">Exec Dean's Office PNC</option>
                                    
                                    
                                    <option value="Exec Dean's Office UCFV">Exec Dean's Office UCFV</option>
                                    
                                    
                                    <option value="Exec Dean's Office WPK">Exec Dean's Office WPK</option>
                                    
                                    
                                    <option value="Executive Dean's Office SPS">Executive Dean's Office SPS</option>
                                    
                                    
                                    <option value="FL Consortium for Intl Education">FL Consortium for Intl Education</option>
                                    
                                    
                                    <option value="Facilities Planning">Facilities Planning</option>
                                    
                                    
                                    <option value="Facilities Planning and Operations">Facilities Planning and Operations</option>
                                    
                                    
                                    <option value="Faculty &amp; Inst Development">Faculty &amp; Inst Development</option>
                                    
                                    
                                    <option value="Faculty Development EAC">Faculty Development EAC</option>
                                    
                                    
                                    <option value="Faculty Development LNC">Faculty Development LNC</option>
                                    
                                    
                                    <option value="Faculty Development OSC">Faculty Development OSC</option>
                                    
                                    
                                    <option value="Faculty Development Online">Faculty Development Online</option>
                                    
                                    
                                    <option value="Faculty Development PNC">Faculty Development PNC</option>
                                    
                                    
                                    <option value="Faculty Development WEC">Faculty Development WEC</option>
                                    
                                    
                                    <option value="Faculty Development WP">Faculty Development WP</option>
                                    
                                    
                                    <option value="Film Production Tech EAC">Film Production Tech EAC</option>
                                    
                                    
                                    <option value="Financial Aid Office">Financial Aid Office</option>
                                    
                                    
                                    <option value="Financial Aid Office EAC">Financial Aid Office EAC</option>
                                    
                                    
                                    <option value="Financial Aid Office OSC">Financial Aid Office OSC</option>
                                    
                                    
                                    <option value="Financial Aid Office WEC">Financial Aid Office WEC</option>
                                    
                                    
                                    <option value="Financial Aid Office WPK">Financial Aid Office WPK</option>
                                    
                                    
                                    <option value="Financial Services">Financial Services</option>
                                    
                                    
                                    <option value="Fine Arts OSC">Fine Arts OSC</option>
                                    
                                    
                                    <option value="Fine Arts Office EAC">Fine Arts Office EAC</option>
                                    
                                    
                                    <option value="Fire Science Academy SPS">Fire Science Academy SPS</option>
                                    
                                    
                                    <option value="Fire Science SPS">Fire Science SPS</option>
                                    
                                    
                                    <option value="Fire Training Facility">Fire Training Facility</option>
                                    
                                    
                                    <option value="Fleet Grounds Equip Maint EAC">Fleet Grounds Equip Maint EAC</option>
                                    
                                    
                                    <option value="Fleet Grounds Equip Maint OSC">Fleet Grounds Equip Maint OSC</option>
                                    
                                    
                                    <option value="Fleet Grounds Equip Maint WEC">Fleet Grounds Equip Maint WEC</option>
                                    
                                    
                                    <option value="Foreign Languages EAC">Foreign Languages EAC</option>
                                    
                                    
                                    <option value="Foreign Languages OSC">Foreign Languages OSC</option>
                                    
                                    
                                    <option value="Foreign Languages WEC">Foreign Languages WEC</option>
                                    
                                    
                                    <option value="Foreign Languages WPK">Foreign Languages WPK</option>
                                    
                                    
                                    <option value="Grants Accounting Office">Grants Accounting Office</option>
                                    
                                    
                                    <option value="Graphics &amp; Interactive Design WEC">Graphics &amp; Interactive Design WEC</option>
                                    
                                    
                                    <option value="Graphics Tech EAC">Graphics Tech EAC</option>
                                    
                                    
                                    <option value="Graphics Tech OSC">Graphics Tech OSC</option>
                                    
                                    
                                    <option value="Green Fund CW">Green Fund CW</option>
                                    
                                    
                                    <option value="Grounds Maintenance DO">Grounds Maintenance DO</option>
                                    
                                    
                                    <option value="Grounds Maintenance EAC">Grounds Maintenance EAC</option>
                                    
                                    
                                    <option value="Grounds Maintenance LNC">Grounds Maintenance LNC</option>
                                    
                                    
                                    <option value="Grounds Maintenance OSC">Grounds Maintenance OSC</option>
                                    
                                    
                                    <option value="Grounds Maintenance WEC">Grounds Maintenance WEC</option>
                                    
                                    
                                    <option value="Grounds Maintenance WPK">Grounds Maintenance WPK</option>
                                    
                                    
                                    <option value="HR Policy &amp; Compliance Programs">HR Policy &amp; Compliance Programs</option>
                                    
                                    
                                    <option value="Health &amp; Fitness OSC">Health &amp; Fitness OSC</option>
                                    
                                    
                                    <option value="Health Information Technology WEC">Health Information Technology WEC</option>
                                    
                                    
                                    <option value="Health Sciences Advising WEC">Health Sciences Advising WEC</option>
                                    
                                    
                                    <option value="Health Sciences OSC">Health Sciences OSC</option>
                                    
                                    
                                    <option value="Health Sciences WEC">Health Sciences WEC</option>
                                    
                                    
                                    <option value="Health Supplemental Voc WEC">Health Supplemental Voc WEC</option>
                                    
                                    
                                    <option value="Heatlh Sciences EAC">Heatlh Sciences EAC</option>
                                    
                                    
                                    <option value="History EAC">History EAC</option>
                                    
                                    
                                    <option value="History LNC">History LNC</option>
                                    
                                    
                                    <option value="History OSC">History OSC</option>
                                    
                                    
                                    <option value="History WEC">History WEC</option>
                                    
                                    
                                    <option value="History WPK">History WPK</option>
                                    
                                    
                                    <option value="Honors Program">Honors Program</option>
                                    
                                    
                                    <option value="Horticulture WEC">Horticulture WEC</option>
                                    
                                    
                                    <option value="Hospitality &amp; Tourism OSC">Hospitality &amp; Tourism OSC</option>
                                    
                                    
                                    <option value="Hospitality &amp; Tourism WEC">Hospitality &amp; Tourism WEC</option>
                                    
                                    
                                    <option value="Human Resource Compliance">Human Resource Compliance</option>
                                    
                                    
                                    <option value="Human Resource Operation">Human Resource Operation</option>
                                    
                                    
                                    <option value="Human Resources Management">Human Resources Management</option>
                                    
                                    
                                    <option value="Humanities &amp; Social Science Off OSC">Humanities &amp; Social Science Off OSC</option>
                                    
                                    
                                    <option value="Humanities EAC">Humanities EAC</option>
                                    
                                    
                                    <option value="Humanities LNC">Humanities LNC</option>
                                    
                                    
                                    <option value="Humanities OSC">Humanities OSC</option>
                                    
                                    
                                    <option value="Humanities WEC">Humanities WEC</option>
                                    
                                    
                                    <option value="Humanities WPK">Humanities WPK</option>
                                    
                                    
                                    <option value="IT Office">IT Office</option>
                                    
                                    
                                    <option value="Infrastructure">Infrastructure</option>
                                    
                                    
                                    <option value="Institutional Assessment">Institutional Assessment</option>
                                    
                                    
                                    <option value="Institutional Effectiveness &amp; Plan">Institutional Effectiveness &amp; Plan</option>
                                    
                                    
                                    <option value="Institutional Research">Institutional Research</option>
                                    
                                    
                                    <option value="Int'l Student Rec and Global Engage">Int'l Student Rec and Global Engage</option>
                                    
                                    
                                    <option value="Interdisciplinary Studies WEC">Interdisciplinary Studies WEC</option>
                                    
                                    
                                    <option value="Internal Auditing">Internal Auditing</option>
                                    
                                    
                                    <option value="International Student Services">International Student Services</option>
                                    
                                    
                                    <option value="Internship &amp; Workforce Services">Internship &amp; Workforce Services</option>
                                    
                                    
                                    <option value="Journalism EAC">Journalism EAC</option>
                                    
                                    
                                    <option value="Journalism WEC">Journalism WEC</option>
                                    
                                    
                                    <option value="LSAMP">LSAMP</option>
                                    
                                    
                                    <option value="Learning Center OSC">Learning Center OSC</option>
                                    
                                    
                                    <option value="Learning Day">Learning Day</option>
                                    
                                    
                                    <option value="Learning Support Office EAC">Learning Support Office EAC</option>
                                    
                                    
                                    <option value="Learning Support Office OSC">Learning Support Office OSC</option>
                                    
                                    
                                    <option value="Learning Support Office PNC">Learning Support Office PNC</option>
                                    
                                    
                                    <option value="Learning Support Office WEC">Learning Support Office WEC</option>
                                    
                                    
                                    <option value="Learning Technology">Learning Technology</option>
                                    
                                    
                                    <option value="Legal Assisting EAC">Legal Assisting EAC</option>
                                    
                                    
                                    <option value="Legal Services">Legal Services</option>
                                    
                                    
                                    <option value="Library Acquisition &amp; Tech Svcs WEC">Library Acquisition &amp; Tech Svcs WEC</option>
                                    
                                    
                                    <option value="Library EAC">Library EAC</option>
                                    
                                    
                                    <option value="Library LNC">Library LNC</option>
                                    
                                    
                                    <option value="Library OSC">Library OSC</option>
                                    
                                    
                                    <option value="Library Science PNC">Library Science PNC</option>
                                    
                                    
                                    <option value="Library WEC">Library WEC</option>
                                    
                                    
                                    <option value="Library WPK">Library WPK</option>
                                    
                                    
                                    <option value="Maintenance Operations EAC">Maintenance Operations EAC</option>
                                    
                                    
                                    <option value="Maintenance Operations LNC">Maintenance Operations LNC</option>
                                    
                                    
                                    <option value="Maintenance Operations OSC">Maintenance Operations OSC</option>
                                    
                                    
                                    <option value="Maintenance Operations WEC">Maintenance Operations WEC</option>
                                    
                                    
                                    <option value="Marketing &amp; Strategic Communication">Marketing &amp; Strategic Communication</option>
                                    
                                    
                                    <option value="Math EAC">Math EAC</option>
                                    
                                    
                                    <option value="Math LNC">Math LNC</option>
                                    
                                    
                                    <option value="Math OSC">Math OSC</option>
                                    
                                    
                                    <option value="Math Office EAC">Math Office EAC</option>
                                    
                                    
                                    <option value="Math Office OSC">Math Office OSC</option>
                                    
                                    
                                    <option value="Math Office WEC">Math Office WEC</option>
                                    
                                    
                                    <option value="Math WEC">Math WEC</option>
                                    
                                    
                                    <option value="Math WPK">Math WPK</option>
                                    
                                    
                                    <option value="Mathematics PNC">Mathematics PNC</option>
                                    
                                    
                                    <option value="Music EAC">Music EAC</option>
                                    
                                    
                                    <option value="Music Tech EAC">Music Tech EAC</option>
                                    
                                    
                                    <option value="NSF Supplemental STEP UP U of F">NSF Supplemental STEP UP U of F</option>
                                    
                                    
                                    <option value="Network Engineering Technology WEC">Network Engineering Technology WEC</option>
                                    
                                    
                                    <option value="New Student Experience EAC">New Student Experience EAC</option>
                                    
                                    
                                    <option value="New Student Experience LNC">New Student Experience LNC</option>
                                    
                                    
                                    <option value="New Student Experience OSC">New Student Experience OSC</option>
                                    
                                    
                                    <option value="New Student Experience WEC">New Student Experience WEC</option>
                                    
                                    
                                    <option value="New Student Experience WPK">New Student Experience WPK</option>
                                    
                                    
                                    <option value="Nursing Office WEC">Nursing Office WEC</option>
                                    
                                    
                                    <option value="Nursing WEC">Nursing WEC</option>
                                    
                                    
                                    <option value="Nutrition EAC">Nutrition EAC</option>
                                    
                                    
                                    <option value="Nutrition LNC">Nutrition LNC</option>
                                    
                                    
                                    <option value="Nutrition OSC">Nutrition OSC</option>
                                    
                                    
                                    <option value="Nutrition WEC">Nutrition WEC</option>
                                    
                                    
                                    <option value="Office Systems Tech EAC">Office Systems Tech EAC</option>
                                    
                                    
                                    <option value="Office Systems Tech OSC">Office Systems Tech OSC</option>
                                    
                                    
                                    <option value="Office Systems Tech WEC">Office Systems Tech WEC</option>
                                    
                                    
                                    <option value="Office Systems Tech WPK">Office Systems Tech WPK</option>
                                    
                                    
                                    <option value="Office of Public Affairs">Office of Public Affairs</option>
                                    
                                    
                                    <option value="Office of the President">Office of the President</option>
                                    
                                    
                                    <option value="Online Learning">Online Learning</option>
                                    
                                    
                                    <option value="Open Computer Labs WEC">Open Computer Labs WEC</option>
                                    
                                    
                                    <option value="Operations &amp; Finance">Operations &amp; Finance</option>
                                    
                                    
                                    <option value="Org Development &amp; Human Resources">Org Development &amp; Human Resources</option>
                                    
                                    
                                    <option value="Organizational Communications">Organizational Communications</option>
                                    
                                    
                                    <option value="Organizational Development">Organizational Development</option>
                                    
                                    
                                    <option value="Organizational Development &amp; HR WEC">Organizational Development &amp; HR WEC</option>
                                    
                                    
                                    <option value="Osceola Ambassador">Osceola Ambassador</option>
                                    
                                    
                                    <option value="Pathways CW">Pathways CW</option>
                                    
                                    
                                    <option value="Peace and Justice Institute">Peace and Justice Institute</option>
                                    
                                    
                                    <option value="Performing Arts Ctr EAC">Performing Arts Ctr EAC</option>
                                    
                                    
                                    <option value="Perkinds Job Developer &amp; Internship">Perkinds Job Developer &amp; Internship</option>
                                    
                                    
                                    <option value="Perkins Allied Health WEC">Perkins Allied Health WEC</option>
                                    
                                    
                                    <option value="Perkins Career Pathways">Perkins Career Pathways</option>
                                    
                                    
                                    <option value="Perkins Career Program Advisors">Perkins Career Program Advisors</option>
                                    
                                    
                                    <option value="Perkins Coordinator">Perkins Coordinator</option>
                                    
                                    
                                    <option value="Perkins Mentoring and Advising">Perkins Mentoring and Advising</option>
                                    
                                    
                                    <option value="Physical Education EAC">Physical Education EAC</option>
                                    
                                    
                                    <option value="Physical Education WEC">Physical Education WEC</option>
                                    
                                    
                                    <option value="Physical Plant LNC">Physical Plant LNC</option>
                                    
                                    
                                    <option value="Physical Science EAC">Physical Science EAC</option>
                                    
                                    
                                    <option value="Physical Science OSC">Physical Science OSC</option>
                                    
                                    
                                    <option value="Physical Science WEC">Physical Science WEC</option>
                                    
                                    
                                    <option value="Plant Operation - ADV Manufacturing">Plant Operation - ADV Manufacturing</option>
                                    
                                    
                                    <option value="Plant Operations EAC">Plant Operations EAC</option>
                                    
                                    
                                    <option value="Plant Operations OSC">Plant Operations OSC</option>
                                    
                                    
                                    <option value="Plant Operations WEC">Plant Operations WEC</option>
                                    
                                    
                                    <option value="Political Science EAC">Political Science EAC</option>
                                    
                                    
                                    <option value="Political Science LNC">Political Science LNC</option>
                                    
                                    
                                    <option value="Political Science OSC">Political Science OSC</option>
                                    
                                    
                                    <option value="Political Science WEC">Political Science WEC</option>
                                    
                                    
                                    <option value="Political Science WPK">Political Science WPK</option>
                                    
                                    
                                    <option value="Procurement">Procurement</option>
                                    
                                    
                                    <option value="Property Management">Property Management</option>
                                    
                                    
                                    <option value="Property Management OSC">Property Management OSC</option>
                                    
                                    
                                    <option value="Psychology EAC">Psychology EAC</option>
                                    
                                    
                                    <option value="Psychology LNC">Psychology LNC</option>
                                    
                                    
                                    <option value="Psychology OSC">Psychology OSC</option>
                                    
                                    
                                    <option value="Psychology PNC">Psychology PNC</option>
                                    
                                    
                                    <option value="Psychology WEC">Psychology WEC</option>
                                    
                                    
                                    <option value="Psychology WPK">Psychology WPK</option>
                                    
                                    
                                    <option value="Radiography WEC">Radiography WEC</option>
                                    
                                    
                                    <option value="Radiologic &amp; Imaging Sciences WEC">Radiologic &amp; Imaging Sciences WEC</option>
                                    
                                    
                                    <option value="Reading EAC">Reading EAC</option>
                                    
                                    
                                    <option value="Reading LNC">Reading LNC</option>
                                    
                                    
                                    <option value="Reading OSC">Reading OSC</option>
                                    
                                    
                                    <option value="Reading WPK">Reading WPK</option>
                                    
                                    
                                    <option value="Registrar's Office">Registrar's Office</option>
                                    
                                    
                                    <option value="Registrar's Office EAC">Registrar's Office EAC</option>
                                    
                                    
                                    <option value="Registrar's Office OSC">Registrar's Office OSC</option>
                                    
                                    
                                    <option value="Registrar's Office WEC">Registrar's Office WEC</option>
                                    
                                    
                                    <option value="Registrar's Office WPK">Registrar's Office WPK</option>
                                    
                                    
                                    <option value="Resource Development">Resource Development</option>
                                    
                                    
                                    <option value="Respiratory Therapy WEC">Respiratory Therapy WEC</option>
                                    
                                    
                                    <option value="SPD Administration">SPD Administration</option>
                                    
                                    
                                    <option value="SPD Org Devel &amp; Human Resources">SPD Org Devel &amp; Human Resources</option>
                                    
                                    
                                    <option value="SPD QEP Student Success Pathway">SPD QEP Student Success Pathway</option>
                                    
                                    
                                    <option value="Science Office OSC">Science Office OSC</option>
                                    
                                    
                                    <option value="Sciences Office EAC">Sciences Office EAC</option>
                                    
                                    
                                    <option value="Sciences Office WEC">Sciences Office WEC</option>
                                    
                                    
                                    <option value="Service Learning">Service Learning</option>
                                    
                                    
                                    <option value="Sign Language EAC">Sign Language EAC</option>
                                    
                                    
                                    <option value="Social Sciences Office EAC">Social Sciences Office EAC</option>
                                    
                                    
                                    <option value="Sociology EAC">Sociology EAC</option>
                                    
                                    
                                    <option value="Sociology LNC">Sociology LNC</option>
                                    
                                    
                                    <option value="Sociology OSC">Sociology OSC</option>
                                    
                                    
                                    <option value="Sociology WEC">Sociology WEC</option>
                                    
                                    
                                    <option value="Sociology WPK">Sociology WPK</option>
                                    
                                    
                                    <option value="Sonography WEC">Sonography WEC</option>
                                    
                                    
                                    <option value="Speech EAC">Speech EAC</option>
                                    
                                    
                                    <option value="Speech LNC">Speech LNC</option>
                                    
                                    
                                    <option value="Speech OSC">Speech OSC</option>
                                    
                                    
                                    <option value="Speech PNC">Speech PNC</option>
                                    
                                    
                                    <option value="Speech WEC">Speech WEC</option>
                                    
                                    
                                    <option value="Speech WPK">Speech WPK</option>
                                    
                                    
                                    <option value="Standardized Testing">Standardized Testing</option>
                                    
                                    
                                    <option value="Student Activities Atlas">Student Activities Atlas</option>
                                    
                                    
                                    <option value="Student Activities Intramurals EAC">Student Activities Intramurals EAC</option>
                                    
                                    
                                    <option value="Student Activities Intramurals OSC">Student Activities Intramurals OSC</option>
                                    
                                    
                                    <option value="Student Activities Orientation">Student Activities Orientation</option>
                                    
                                    
                                    <option value="Student Activity">Student Activity</option>
                                    
                                    
                                    <option value="Student Activity Answer Center">Student Activity Answer Center</option>
                                    
                                    
                                    <option value="Student Activity EAC">Student Activity EAC</option>
                                    
                                    
                                    <option value="Student Activity Lake Nona">Student Activity Lake Nona</option>
                                    
                                    
                                    <option value="Student Activity OSC">Student Activity OSC</option>
                                    
                                    
                                    <option value="Student Activity SL EAC">Student Activity SL EAC</option>
                                    
                                    
                                    <option value="Student Activity SL OSC">Student Activity SL OSC</option>
                                    
                                    
                                    <option value="Student Activity SL WEC">Student Activity SL WEC</option>
                                    
                                    
                                    <option value="Student Activity SL WPK">Student Activity SL WPK</option>
                                    
                                    
                                    <option value="Student Activity Tutoring EAC">Student Activity Tutoring EAC</option>
                                    
                                    
                                    <option value="Student Activity Tutoring LNC">Student Activity Tutoring LNC</option>
                                    
                                    
                                    <option value="Student Activity Tutoring OSC">Student Activity Tutoring OSC</option>
                                    
                                    
                                    <option value="Student Activity Tutoring PNC">Student Activity Tutoring PNC</option>
                                    
                                    
                                    <option value="Student Activity Tutoring WEC">Student Activity Tutoring WEC</option>
                                    
                                    
                                    <option value="Student Activity Tutoring WPK">Student Activity Tutoring WPK</option>
                                    
                                    
                                    <option value="Student Activity WEC">Student Activity WEC</option>
                                    
                                    
                                    <option value="Student Activity WPK">Student Activity WPK</option>
                                    
                                    
                                    <option value="Student Affairs">Student Affairs</option>
                                    
                                    
                                    <option value="Student Disability Services">Student Disability Services</option>
                                    
                                    
                                    <option value="Student Svcs Administration">Student Svcs Administration</option>
                                    
                                    
                                    <option value="Study Abroad and Global Experiences">Study Abroad and Global Experiences</option>
                                    
                                    
                                    <option value="Sustainability CW">Sustainability CW</option>
                                    
                                    
                                    <option value="TAACCCT Advanced Manufacturing">TAACCCT Advanced Manufacturing</option>
                                    
                                    
                                    <option value="Take Stock">Take Stock</option>
                                    
                                    
                                    <option value="Take Stock in Children">Take Stock in Children</option>
                                    
                                    
                                    <option value="Talent Acquisition &amp; Total Rewards">Talent Acquisition &amp; Total Rewards</option>
                                    
                                    
                                    <option value="Teaching Learning Academy">Teaching Learning Academy</option>
                                    
                                    
                                    <option value="Teaching and Learning">Teaching and Learning</option>
                                    
                                    
                                    <option value="Test">Test</option>
                                    
                                    
                                    <option value="Testing Center EAC">Testing Center EAC</option>
                                    
                                    
                                    <option value="Theater EAC">Theater EAC</option>
                                    
                                    
                                    <option value="Theater Tech EAC">Theater Tech EAC</option>
                                    
                                    
                                    <option value="Theater Technoogy OSC">Theater Technoogy OSC</option>
                                    
                                    
                                    <option value="Title III">Title III</option>
                                    
                                    
                                    <option value="Title IX Equal Opportunity">Title IX Equal Opportunity</option>
                                    
                                    
                                    <option value="Title V EAC">Title V EAC</option>
                                    
                                    
                                    <option value="Title V OSC">Title V OSC</option>
                                    
                                    
                                    <option value="Trade Adjustment Assistance">Trade Adjustment Assistance</option>
                                    
                                    
                                    <option value="Transition Administration">Transition Administration</option>
                                    
                                    
                                    <option value="Transition Planning">Transition Planning</option>
                                    
                                    
                                    <option value="Transition Services Office EAC">Transition Services Office EAC</option>
                                    
                                    
                                    <option value="Transition Services Office OSC">Transition Services Office OSC</option>
                                    
                                    
                                    <option value="Transition Services Office WEC">Transition Services Office WEC</option>
                                    
                                    
                                    <option value="Transtion Services">Transtion Services</option>
                                    
                                    
                                    <option value="Tutoring &amp; Testing WEC">Tutoring &amp; Testing WEC</option>
                                    
                                    
                                    <option value="US DOJ Violence Prevent">US DOJ Violence Prevent</option>
                                    
                                    
                                    <option value="Undergrad Intl Studies/Foreign Lang">Undergrad Intl Studies/Foreign Lang</option>
                                    
                                    
                                    <option value="United Arts of Central FL Career">United Arts of Central FL Career</option>
                                    
                                    
                                    <option value="Universal Orlando Art of Tomorrow">Universal Orlando Art of Tomorrow</option>
                                    
                                    
                                    <option value="Valencia Foundation">Valencia Foundation</option>
                                    
                                    
                                    <option value="Valencia Live">Valencia Live</option>
                                    
                                    
                                    <option value="Veterans Affair Office WEC">Veterans Affair Office WEC</option>
                                    
                                    
                                    <option value="Word Processing EAC">Word Processing EAC</option>
                                    
                                    
                                    <option value="Word Processing WEC">Word Processing WEC</option>
                                    
                                    
                                    <option value="Workforce Industry Certifications">Workforce Industry Certifications</option>
                                    
                                    
                                    <option value="Writing Center WEC">Writing Center WEC</option>
                                    
                                    
                                    <option value="Youthbuild OSC">Youthbuild OSC</option>
                                    
                                    </select>
                                 <br>
                                 <br>
                                 <input name="Search" type="submit" value="Search">
                                 
                              </div>
                              
                           </form>
                           
                           <hr>
                           
                           
                           
                           <form action="lookup.html" method="post" name="rlform" id="rlform">
                              <input name="searchType" type="hidden" value="3">
                              
                              <div>
                                 
                                 <h3>Extension Search</h3>
                                 <label for="Phone">Valencia Extension</label>
                                 <br>
                                 407 - 582 -
                                 <input maxlength="4" name="Phone" size="30" type="text">
                                 <br>
                                 <br>
                                 <input name="Search" type="submit" value="Search">
                                 
                              </div>
                              
                           </form>
                           
                        </div>
                        <br>
                        
                        <p><strong>Students!</strong> If you need assistance resolving a problem with 
                           registration or graduation, the quickest way to get that assistance 
                           is to e-mail AskAtlas at <a href="mailto:askatlas@valenciacollege.edu"> askatlas@valenciacollege.edu</a>. 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/contact-information/directory.pcf">©</a>
      </div>
   </body>
</html>