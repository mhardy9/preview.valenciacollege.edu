<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/contact-information/fax/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/contact-information/fax/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/contact-information/">Contact Information</a></li>
               <li>Fax</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../support/index.html"></a>
                        
                        
                        
                        
                        <h2>Departmental Fax Numbers</h2>
                        
                        <p>Need to Update your department's fax number?<br>
                           <a href="http://net4.valenciacollege.edu/forms/contact/fax/update.cfm">Click Here to Report</a></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Department</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Fax Number</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Academic Affairs - Planning</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Academic Success Center</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Accounting LNC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-3007</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Achieving the Dream/Lumina</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Adminstrative Services</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-3007</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Allied Health Office</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Alternative Delivery Standards</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Alumni Relations</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5685</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Anthropology EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Architectural Design WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1900</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Art EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8917</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Art Gallery EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Arts &amp; Entertainment</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Audio Visual EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Audio Visual OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Audio Visual WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Biological Science EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Biological Science OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Biological Science WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Biological Science WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Biology LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bookstore EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2581</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bookstore Operations</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1770</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bookstore OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4174</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bookstore WEC</div>
                                 
                                 <div data-old-tag="td">407-582-1688</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bookstore WPK</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-6811</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bridges</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1304</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Budget Office</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-3007</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Building Construction WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bursar</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business &amp; IT Lab Support WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business &amp; IT Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8901</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business &amp; IT Office OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4894</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business &amp; Soc Sci Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1675</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8907</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business Office OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4175</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1364</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Business WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Campus Security Services</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Campus Security Svcs EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2251</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Campus Security Svcs OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Campus Security Svcs WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1863</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Campus Security Svcs WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Cardiovascular Tech WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Career Development EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8906</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Career Development OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4187</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Career Development WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1864</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Career Pathways - Partners</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Administration</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Business Administration</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Business Agreements</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Business Open Enrollment</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE CIE Administration</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE CIE Intensive English</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE CIE JEV</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE CIE Open Enrollment</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE CIE Testing</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE CIE Youth</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Client Services Center</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Custodial Services</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Operations</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Public Safety</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CE Security</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CFHLA Hospitality WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5436</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Chemistry EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Chemistry LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Chemistry OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Chemistry WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Chief Learning Officer</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">College Events &amp; Conferencing</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-3004</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">College Prep English OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">College Prep Reading EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">College Reachout</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5523</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">College Reading Prep WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">College Wellness</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Communications Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2465</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Communications Office OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4843</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Communications Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1676</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Computer Engineering EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Computer Engineering WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1900</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Computer Program EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8901</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Computer Program OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Computer Program WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Computer Program WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Counseling Workshop</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Courier Services DTC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Courier Services EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8924</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Courier Services OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Courier Services WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Criminal Justice Institute EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8222</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Criminal Justice Tech EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Criminal Justice Tech WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Culinary Arts WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Curriculum &amp; Articulation</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8905</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Custodial Services EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Custodial Services LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Custodial Services OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Custodial Services WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Custodial Services WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dance EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dean of Students EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dean of Students OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dean of Students WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dean of Students WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dental Hygiene WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Digital Media EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Diversity</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Drafting &amp; Design EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Drafting &amp; Design WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1900</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dual Enrollment</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1901</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">EAP EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Earth Science EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Earth Science OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Earth Science WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Earth Science WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Economic Development Office</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Economics EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Economics OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Economics WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Economics WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Education EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Education OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Education WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Education WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Educator Prep Institute</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Electronics Engineering WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1900</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Emergency Medical Svcs OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Emergency Medical Svcs WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Energy Education CW</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Engineering &amp; IT Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1900</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Engineering WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1900</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">English EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">English LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">English OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">English WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">English WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Enrollment Services</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1865</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Exec Dean's Office LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Exec Dean's Office WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Facilities Planning</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Faculty Development</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8905</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Federal College Workstudy 0910</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Federal College Workstudy 1011</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Federal College Workstudy 1112</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Film Production Tech EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2415</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Financial Aid Office</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Financial Aid Office EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Financial Aid Office OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Financial Aid Office WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Financial Aid Office WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Financial Services</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Fine &amp; Applied Arts WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Fine Arts Office EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Fine Arts Office WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Fine Arts OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Fine Arts Student Svcs EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Florida Work Experience 1011</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Foreign Languages EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8917</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Foreign Languages OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4843</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Foreign Languages WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5436</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Grants Accounting Office</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-3007</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Graphics Tech EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Graphics Tech OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Graphics Tech WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Grounds Maintenance EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Grounds Maintenance OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Grounds Maintenance WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Grounds Maintenance WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Health &amp; Fitness OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Health Education WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Health Sciences Advising WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5462</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Health Sciences OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Health Sciences WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Health Supplemental Voc WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Heatlh Sciences EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">History EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">History OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">History WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Honors Program</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1671</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Horticulture WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Hospitality &amp; Tourism OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Hospitality &amp; Tourism WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5436</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Human Resources</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8202</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Humanities EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8917</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Humanities OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4843</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Humanities WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5436</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Humanities WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Institutional Research</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1821</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Interdisciplinary Studies WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-5436</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Internal Auditing</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Internship &amp; Workforce Services</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1864</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">IT Office</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1821</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Journalism WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Learning Center OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Learning Evidence</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Learning Support Office EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Learning Support Office OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Learning Support Office WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Legal Assisting EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Legal Services</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Library Acquisition &amp; Tech Svcs WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1686</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Library EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8914</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Library OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4280</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Library Science EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Library WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1686</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Library WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">LinC(AtD)</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Maintenance Operations EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8924</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Maintenance Operations OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Maintenance Operations WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Maintenance Operations WPK</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8924</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Marketing &amp; Strategic Communication</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1327</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math Depot (ATD)</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8913</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8913</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math Office OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4843</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1856</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math OSC</div>
                                 
                                 <div data-old-tag="td">407-582-4843</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math WEC</div>
                                 
                                 <div data-old-tag="td">407-582-1856</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Math WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">MDC Dev Education Initiative</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Micro Computer Lab EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Music EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8917</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Music Tech EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">NSF Computer Engineer Tech</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Nursing Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Nursing WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Nutrition EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Nutrition OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Nutrition WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Office of the President</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-3424</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Office Systems Tech EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Office Systems Tech OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Office Systems Tech WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Office Systems Tech WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Open Computer Labs OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Open Computer Labs WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Organizational Comm &amp;    Development</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Payroll Services</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8132</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Peace and Justice Program</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Performing Arts Ctr EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8917</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Perkins</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Perkins Career Pathways</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Perkins Career Program Advisors</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Perkins Mentoring and Advising</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Perkins Nursing WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Physical Education EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2563</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Physical Education WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1744</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Physical Plant LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Physical Plant Operations</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Physical Science EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Physical Science OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Physical Science WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Placement Testing Postsec Education</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Plant Operations</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Plant Operations EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8924</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Plant Operations OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Plant Operations WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Political Science EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Political Science OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Political Science WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Political Science WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Procurement</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Property Management</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1209</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Provost Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2621</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Provost Office OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4858</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Provost Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1912</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Psychology EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Psychology LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Psychology OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Psychology WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Psychology WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Radiography WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Radiologic &amp; Imaging Sciences    WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Reading EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Reading LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Reading OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Reading WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Reading WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Receiving</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Registrar's Office</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Registrar's Office EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Registrar's Office OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Registrar's Office WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Registrar's Office WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Resource Development</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Respiratory Therapy WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Safety &amp; Security General</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1863</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sciences Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8913</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sciences Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1215</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sign Language EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Social Sciences Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8902</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sociology EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sociology LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sociology OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sociology WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sociology WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Sonography WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1278</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">SPA (ATD)</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">SPD Administration</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">SPD Sabbatical</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Speech EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Speech LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Speech OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Speech WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Speech WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Standardized Testing</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activities Intramurals EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2602</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity Lake Nona</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4887</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity OSD</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity SL</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity Tutoring EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity Tutoring LNC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity Tutoring OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity Tutoring WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity Tutoring WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1911</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Activity WPK</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-6915</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Affairs</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Disability Services</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Life Skills Administration</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Life Skills EAC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Life Skills OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Life Skills WEC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Life Skills WPK</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Svcs Administration</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Study Abroad and Global Experiences</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Take Stock in Children</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Teaching Learning Academy</div>
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Testing Center EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8916</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Testing Center OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4892</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Testing Center WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1939</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Testing Center WPK</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-6014</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Theater EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8917</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Theater Tech EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8917</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Theater Technoogy OSC</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Transition Administration</div>
                                 
                                 <div data-old-tag="td">407-582-1138</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Transition Planning</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Transition Services Office EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-2112</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Transition Services Office OSC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-4187</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Transition Services Office WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1138</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Valencia Foundation</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-3156</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Word Processing EAC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-8920</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Word Processing WEC</div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">407-582-1403</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Workforce Development</div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/contact-information/fax/index.pcf">©</a>
      </div>
   </body>
</html>