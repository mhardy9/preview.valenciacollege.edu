<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/contact-information/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/contact-information/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Contact Information</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../support/index.html"></a>
                        
                        <h2>Contact Us</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <h3>Email &amp; Phone 
                                       Directory Search
                                    </h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="directory.html">Email &amp; Phone Directory Search</a></div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="fax/index.html">Departmental Fax Numbers</a></div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Mailing Address, 
                                    Phone &amp; Email
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <blockquote>
                                       
                                       <p>Send all mail to: <br>
                                          P.O. BOX 3028<br>
                                          Orlando, Florida 32802-3028
                                       </p>
                                       
                                    </blockquote>
                                    
                                    <ul>
                                       
                                       <li>Main Phone Number 407-299-5000</li>
                                       
                                       <li>Telecommunication System for the Deaf 407-298-7032</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Student Support</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="../advising-counseling/index.html">Contact an Advisor</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Atlas Student Help Desk</strong><br>407-582-5444 or <a href="#" onmousedown="mask('ollege.edu','las@valenciac','askat','Online Classes Technical Support')">Student Help Desk E-mail</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Enrollment Services </strong><br>407-582-1507
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Florida Department of Education </strong><br><a href="http://www.fldoe.org/department/contact.asp" target="_blank" title="Florida Department of Education">www.fldoe.org/department/contact.asp </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Technical Support</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="../oit/index.html">Faculty &amp; Staff Help Desk</a></div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/contact-information/index.pcf">©</a>
      </div>
   </body>
</html>