<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Top 20 Course Combinations | SAS Visual Analytics | Valencia College</title>
      <meta name="Description" content="Top 20 Course Combinations | SAS Visual Analytics">
      <meta name="Keywords" content="college, school, education, visual, analytics, top, course, combinations"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/top-20-course-combinations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>SAS Visual Analytics</h1>
            <p>Top 20 Course Combinations</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Top 20 Course Combinations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  		
                  <p>
                     		
                     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                     <script type="text/javascript">
      google.load("visualization", "1.1", {packages:["sankey"]});
      google.setOnLoadCallback(drawChart1);
      google.setOnLoadCallback(drawChart2);
	  google.setOnLoadCallback(drawChart3);

      function drawChart1() {
        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', 'Course A');
        data1.addColumn('string', 'Course B');
        data1.addColumn('number', 'Students');
        data1.addRows([
			['ENC1101','MAT1033C',894],
			['ENC1101','MAC1105',759],
			['ENC1101','MAT0028C',610],
			['REA0017C','SLS1122',557],
			['ENC1101','POS2041',502],
			['ENC1101','SLS1122',413],
			['ENC1101','MAT0018C',391],
			['ENC1102','MAC1105',390],
			['EAP1620C','EAP1640C',375],
			['ENC1102','MAT1033C',372],
			['BSC2094C','MCB2010C',370],
			['ENC1101','HUM1020',369],
			['ENC1101','SPC1608',339],
			['REA0007C','SLS1122',304],
			['ENC1102','POS2041',290],
			['MAC2312','PHY2048C',284],
			['ENC1101','PSY2012',266],
			['BSC2093C','HUN2202',266],
			['MAT0018C','REA0017C',254],
			['BSC2093C','MCB2010C',247]
        ]);

        // Sets chart options.
        var options = {
          width: 350,
        };

        // Instantiates and draws our chart, passing in some options.
        var chart1 = new google.visualization.Sankey(document.getElementById('sankey_basic1'));
        chart1.draw(data1, options);
      }  
   
      function drawChart2() {
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', 'Course A');
        data2.addColumn('string', 'Course B');
        data2.addColumn('number', 'Students');
        data2.addRows([
			['ENC1101','MAT0028C',633],
			['ENC1101','MAT1033C',538],
			['ENC1102','MAC1105',444],
			['ENC1102','MAT1033C',361],
			['ENC1102','POS2041',360],
			['ENC1101','MAT0018C',345],
			['ENC1101','MAC1105',340],
			['BSC2094C','MCB2010C',328],
			['REA0017C','SLS1122',320],
			['ENC1102','SPC1608',308],
			['ENC1101','SLS1122',288],
			['EAP1620C','EAP1640C',287],
			['POS2041','SPC1608',279],
			['ENC1101','POS2041',275],
			['MAC2312','PHY2048C',260],
			['BSC2093C','MCB2010C',255],
			['ENC1101','MAT0024C',248],
			['MAC1105','POS2041',248],
			['ENC1101','SPC1608',241],
			['MAT0018C','REA0017C',230]
        ]);

        // Sets chart options.
        var options = {
          width: 350,
        };

        // Instantiates and draws our chart, passing in some options.
		var chart2 = new google.visualization.Sankey(document.getElementById('sankey_basic2'));
        chart2.draw(data2, options);
      }

      function drawChart3() {
        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', 'Course A');
        data3.addColumn('string', 'Course B');
        data3.addColumn('number', 'Students');
        data3.addRows([
			['ENC1101','MAT1033C',478],
			['ENC1101','MAT0028C',450],
			['ENC1102','MAC1105',396],
			['POS2041','SPC1608',338],
			['EAP1620C','EAP1640C',330],
			['ENC1102','MAT1033C',317],
			['MAC1114','MAC1140',313],
			['MAC2312','PHY2048C',306],
			['ENC1101','MAC1105',305],
			['ENC1102','POS2041',288],
			['ENC1101','SPC1608',285],
			['ENC1102','SPC1608',265],
			['ENC1101','MAT0024C',249],
			['ENC1101','POS2041',248],
			['HUM1020','POS2041',245],
			['REA0017C','SLS1122',240],
			['ENC1101','HUM1020',220],
			['MAC1105','SPC1608',219],
			['MAC1105','POS2041',219],
			['MAT1033C','SPC1608',218]
        ]);
		
        // Sets chart options.
        var options = {
          width: 350,
        };

        // Instantiates and draws our chart, passing in some options.
		var chart3 = new google.visualization.Sankey(document.getElementById('sankey_basic3'));
        chart3.draw(data3, options);
      }	  
    </script>
                     			
                     
                     			
                     			
                     			
                     		<ul style="font-size: 10; text-align: left;">
                        			<b>Use Cases:</b>
                        			
                        <li>
                           				If you are considering to increase ENC1101 offerings, for example, also consider
                           to increase MAT/MAC offerings.
                           			
                        </li>
                        			
                        <li>
                           				These charts could also be helpful when building schedules. For example, the Communications
                           and Math departments may want to work together to have the least number of time conflicts.
                           			
                        </li>
                        		
                     </ul>
                     	
                     
                     <div id="dashboard">
                        		
                        		
                        <table class="table ">
                           			
                           <tr style="vertical-align: middle;">
                              				
                              <td style="width: 400px; font-size: 12; text-align: center;">
                                 					<br>
                                 					<strong>Fall terms (201010 to 201510)</strong>
                                 					
                                 <p></p>
                                 					
                                 <div id="sankey_basic1" style="width: 400px; height: 300px;margin:0 auto;" align="center"></div>
                                 				
                              </td>
                              				
                              <td style="width: 400px; font-size: 12; text-align: center;">&nbsp;
                                 					
                                 				
                              </td>
                              				
                              <td style="width: 400px; font-size: 12; text-align: center;">
                                 					<br>
                                 					<strong>Spring terms (201020 to 201520)</strong>
                                 					
                                 <p></p>
                                 					
                                 <div id="sankey_basic2" style="width: 400px; height: 300px;margin:0 auto;" align="center"></div>				
                                 				
                              </td>
                              				
                              <td style="width: 400px; font-size: 12; text-align: center;">&nbsp;
                                 					
                                 				
                              </td>			
                              				
                              <td style="width: 400px; font-size: 12; text-align: center;">
                                 					<br>
                                 					<strong>Summer terms (201030 to 201530)</strong>
                                 					
                                 <p></p>
                                 					
                                 <div id="sankey_basic3" style="width: 400px; height: 300px;margin:0 auto;" align="center"></div>			
                                 				
                              </td>
                              			
                           </tr>		
                           			
                           <tr style="vertical-align: middle;">
                              				
                              <td style="width: 400px; font-size: 10px; text-align: left;padding-left:28px">
                                 					<br>
                                 					ENC1101,MAT1033C<br>
                                 					ENC1101,MAT1033C,SLS1122,SPC1608<br>
                                 					ENC1101,MAC1105<br>
                                 					EAP1500C,EAP1520C,EAP1540C,EAP1560C<br>
                                 					ENC1101,MAT0028C<br>
                                 					REA0017C,SLS1122<br>
                                 					ENC1101,POS2041<br>
                                 					ENC0025C,MAT0018C,REA0017C,SLS1122<br>
                                 					ENC1101,MAC1105,POS2041,SPC1608<br>
                                 					ENC0025C,MAT0028C,REA0017C,SLS1122<br>
                                 					ENC0012,ENC0012L,MAT0012C,REA0002,REA0002L,SLS1122<br>
                                 					ENC1101,SLS1122<br>
                                 					ENC1101,MAT1033C,POS2041,SPC1608<br>
                                 					EMS1119,EMS1119L,EMS1431L<br>
                                 					ENC1101,MAT0018C<br>
                                 					ENC1102,MAC1105<br>
                                 					EAP1620C,EAP1640C<br>
                                 					ENC1102,MAT1033C<br>
                                 					BSC2094C,MCB2010C<br>
                                 					ENC1101,HUM1020<br>
                                 				
                              </td>
                              				
                              <td style="width: 400px; font-size: 12; text-align: center;">&nbsp;
                                 					
                                 				
                              </td>
                              				
                              <td style="width: 400px; font-size: 10px; text-align: left;padding-left:28px">
                                 					<br>
                                 					ENC1101,MAT0028C<br>
                                 					ENC1101,MAT1033C<br>
                                 					EAP1500C,EAP1520C,EAP1540C,EAP1560C<br>
                                 					ENC1102,MAC1105<br>
                                 					EMS1119,EMS1119L,EMS1431L<br>
                                 					HSC2151,NUR2823C,NUR2832L<br>
                                 					ENC1102,MAT1033C<br>
                                 					ENC1102,POS2041<br>
                                 					ENC1101,MAT0018C<br>
                                 					ENC1101,MAC1105<br>
                                 					BSC2094C,MCB2010C<br>
                                 					REA0017C,SLS1122<br>
                                 					ENC1102,SPC1608<br>
                                 					ENC1101,SLS1122<br>
                                 					EAP1620C,EAP1640C<br>
                                 					EAP0400C,EAP0420C,EAP0440C,EAP0460C<br>
                                 					REA0002,REA0002L,SLS1122<br>
                                 					ENC0012,ENC0012L,MAT0012C,REA0002,REA0002L,SLS1122<br>
                                 					POS2041,SPC1608<br>
                                 					ENC1101,POS2041<br>
                                 					<br>
                                 				
                              </td>
                              				
                              <td style="width: 400px; font-size: 12; text-align: center;">&nbsp;
                                 					
                                 				
                              </td>			
                              				
                              <td style="width: 400px; font-size: 10px; text-align: left;padding-left:28px">
                                 					<br>
                                 					ENC1101,MAT1033C<br>
                                 					ENC1101,MAT0028C<br>
                                 					ENC1102,MAC1105<br>
                                 					HSC2151,NUR2823C,NUR2832L<br>
                                 					ENC1101,HUM1020,SLS1101<br>
                                 					POS2041,SPC1608<br>
                                 					EAP1620C,EAP1640C<br>
                                 					ENC1102,MAT1033C<br>
                                 					MAC1114,MAC1140<br>
                                 					MAC2312,PHY2048C<br>
                                 					ENC1101,MAC1105<br>
                                 					ENC1102,POS2041<br>
                                 					ENC1101,SPC1608<br>
                                 					ENC1102,SPC1608<br>
                                 					ENC1101,MAT0024C<br>
                                 					EMS1119,EMS1119L,EMS1431L<br>
                                 					ENC1101,POS2041<br>
                                 					HUM1020,POS2041<br>
                                 					REA0017C,SLS1122<br>
                                 					ENC1101,HUM1020<br>
                                 					<br>		
                                 				
                              </td>
                              			
                           </tr>			
                           		
                        </table>
                        
                     </div>	
                     			
                     		
                     		
                  </p>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/top-20-course-combinations.pcf">©</a>
      </div>
   </body>
</html>