<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office of Compliance and Audit | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/audit/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/audit/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Office of Compliance and Audit</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Audit</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Overview </h2>
                        
                        <p>Internal Auditing is an independent, objective assurance and consulting
                           activity designed to add value and improve an organization’s operations.
                           It helps an organization accomplish its objectives by bringing
                           a systematic, disciplined approach to evaluate and improve the effectiveness
                           of risk management, internal control, and governance processes. 
                        </p>
                        
                        <p>During the performance of any audit, Compliance and Audit shall maintain
                           objectivity and integrity, shall be free of conflicts of interests, and
                           shall not knowingly misrepresent facts, or subordinate their judgment to
                           others.
                        </p>
                        
                        <p>The Compliance and Audit department performs compliance, operational, and
                           financial audits.
                        </p>
                        
                        <ol type="A">
                           
                           <li>Compliance and operational audits conducted by the department
                              generally relate to economy and efficiency of operation, whether
                              resources are being used economically and efficiently, whether
                              good management practices are being followed, and whether there
                              has been compliance with applicable policies, laws, and regulations.
                              Such audits, for example, may consider whether a unit:
                           </li>
                           
                           
                           
                           <ul class="list-style-1">
                              
                              <li>Is following sound procurement practices.</li>
                              
                              <li>Is properly utilizing its personnel to achieve a high level
                                 of performance and is avoiding duplicated effort, overstaffing,
                                 and ineffective management practices.
                              </li>
                              
                              <li>Is complying with institutional policies.</li>
                              
                              <li>Is complying with applicable laws and regulations that may
                                 affect the acquisition, protection, and use of institutional
                                 resources.
                              </li>
                              
                              <li>Is achieving satisfactory results in the performance of its
                                 assigned functions.
                              </li>
                              		
                           </ul>
                           		
                           
                           				  
                           <li>Financial audits include financially-related reviews. The audit
                              of the institution's basic financial statements is performed
                              on an annual basis (year ending June 30) by the Florida Auditor
                              General. Financially-related reviews may include (but not limited
                              to) audits of the following items:
                           </li>
                           	
                           		
                           
                           
                           <ul class="list-style-1">
                              
                              <li>Segments of financial statements</li>
                              
                              <li>Statement of Net Assets</li>
                              
                              <li>Statement of Revenues, Expenses and Changes in Net Assets</li>
                              
                              <li>Statement of Cash Flows</li>
                              
                              <li>Expenditures for specific programs or services</li>
                              
                              <li>Internal control systems as they relate to accounting, reporting,
                                 and transaction processing
                              </li>
                              
                              <li>Computer-based systems and EDP controls.</li>
                              
                              <li>Specific processing systems such as cash collections, payroll,
                                 and accounts payable.
                              </li>
                              
                              <li>Special investigations involving fraud or serious violations
                                 of policy.<strong> </strong>
                                 
                              </li>
                              
                           </ul>	
                           	
                        </ol>
                        
                        <h3>Mission</h3>
                        
                        <p>The mission of the Compliance and Auditing Department is to promote the
                           most economical and efficient use of resources; to ensure compliance with
                           institutional policies, as well as, federal and state laws and regulations;
                           to protect the institution’s assets; to deter and detect fraud, theft,
                           and abuse; to promote accountability throughout the institution, and to
                           promote the reliability and integrity of the institution.
                        </p>
                        
                        <h4>Authority</h4>
                        
                        <p>The Compliance and Auditing Department reports directly to the District
                           Board of Trustees, with the authority to review and appraise all policies,
                           processes, systems, and operations and to recommend any corrective actions
                           necessary. The department has no direct authority or responsibility for
                           the activities it audits. The department works independently, so that its
                           work can be carried out freely and objectively, with impartial and unbiased
                           judgments – essential to the proper conduct of audits.
                        </p>
                        
                        <p>Compliance and Auditing personnel shall have full, free, and unrestricted
                           access to all institutional systems, functions, operations, records, activities,
                           properties and personnel. In the performance of audits and with full consideration
                           of safekeeping and confidentiality, all appropriate arrangements shall be
                           made when examining and reporting on confidential items.
                        </p>
                        
                        <p>Although a common courtesy, Compliance and Auditing staff is not required
                           to give notice prior to performing an audit.
                        </p>
                        
                        <p>In compliance with the Generally Accepted Government Auditing Standards
                           set forth by the United States General Accounting Office and the Standards
                           for the Professional Practice of Internal Auditing set forth by the Institute
                           of Internal Auditors, the Compliance and Auditing Department does not have
                           the authority to implement or insist on the implementation of its recommendations.
                           Implementation is ultimately the decision of management.
                        </p>
                        
                        <h4>Responsibility </h4>
                        
                        <p>It is the responsibility of the Compliance and Auditing Department to develop
                           and execute a comprehensive risk based audit plan for the institution. All
                           audits conducted by the department will be performed with the highest of
                           standards including honesty, objectivity, diligence and loyalty, as required
                           by the District Board of Trustees.
                        </p>
                        
                        <p>One of the primary responsibilities of Compliance and Auditing is to evaluate
                           the adequacy of the systems of internal control to ensure that the systems
                           established provide reasonable assurance that the institution’s objectives
                           and goals will be met efficiently and economically. The Compliance and Auditing
                           Department will identify those activities subject to audit coverage, based
                           upon their significance and the degree of risk inherent in the activity
                           in terms of cost, special requests by the Board of Trustees and Administration
                           of the institution, and areas where non-compliance with guidelines could
                           result in severe penalties to the institution.
                        </p>
                        
                        <p>The Compliance and Auditing Department will produce the results of audit
                           examinations, including recommendations for improvement via audit reports.
                           The department will appraise the adequacy of the action taken by Management
                           to correct reported deficiencies. The Compliance and Auditing Department
                           will report to the District Board of Trustees several times during each
                           fiscal year. During this time, interim reports will be provided to the Board
                           regarding the status of “Audits in Progress” and “Completed
                           Audits.”
                        </p>
                        
                        <p>The Compliance and Auditing Department also has the responsibility of facilitating
                           internal and external audit efforts. This is done to ensure adequate audit
                           coverage and to minimize duplicate efforts. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/audit/index.pcf">©</a>
      </div>
   </body>
</html>