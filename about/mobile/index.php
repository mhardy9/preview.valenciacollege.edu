<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/mobile/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/mobile/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Mobile</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             
                                          </div> 
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Mobile" border="0" height="259" src="featured-banner-02.jpg" width="768">
                                                               
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                         </div>
                                                         
                                                         
                                                      </div>
                                                      <br>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <h2>Valencia College on your Mobile Device</h2>
                                                      
                                                      <p>Connect to Valencia College resources on-the-go with Valencia Mobile for Android &amp;
                                                         iOS devices. 
                                                      </p>
                                                      
                                                      <p>Get access to the college faculty and staff phone directory, campus maps, course schedule,
                                                         news and events, photos and videos, account balance, and grades.  
                                                      </p>
                                                      
                                                      
                                                      <div>
                                                         
                                                         <ul>
                                                            
                                                            <li><a href="index.html#" title="Download">Download</a></li>
                                                            
                                                            <li><a href="index.html#" title="Application Details">Application Details</a></li>
                                                            
                                                            <li><a href="index.html#" title="Help">Help</a></li>
                                                            
                                                            
                                                         </ul>
                                                         
                                                         
                                                         
                                                         
                                                         <div>  
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        <div>
                                                                           <strong>Version 2.0 is now available for Android Devices, iPhone &amp; iPad&nbsp;&nbsp;<img border="0" src="icon-new-quicklink.png"></strong><br>
                                                                           New Features Include:
                                                                           
                                                                           <ul>
                                                                              
                                                                              <li>Stay logged into the app to allow access to your secured information.</li>
                                                                              
                                                                              <li>Better integration of Campus Maps.</li>
                                                                              
                                                                              <li>Notifications directly from your Atlas Account.</li>
                                                                              
                                                                              <li>More information in your Class Schedule.</li>
                                                                              
                                                                              <li>Overall improvements in application performance</li>
                                                                              
                                                                           </ul>
                                                                           
                                                                        </div>
                                                                     </div>
                                                                     
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><a href="http://itunes.apple.com/us/app/valencia-college/id524358776?ls=1&amp;mt=8"><img src="app_store_badge.png"></a></div>
                                                                        
                                                                        <div>
                                                                           <a href="http://itunes.apple.com/us/app/valencia-college/id524358776?ls=1&amp;mt=8" target="_blank">Click here to</a> find the Valencia Mobile App for iPad &amp; iPhones in the App Store.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><a href="https://play.google.com/store/apps/details?id=com.vc.valenciacollege"><img src="droid.png"></a></div>
                                                                        
                                                                        <div>
                                                                           <a href="https://play.google.com/store/apps/details?id=com.vc.valenciacollege">Click here</a> to find the Valencia Mobile App for Android devices in the Google Play Shop (formerly
                                                                           Android Marketplace)
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               <br><br>
                                                               
                                                               <span>Portions of this page are reproduced from work created and shared by Google and used
                                                                  according to terms described in the <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons 3.0 Attribution License</a>.</span>
                                                               
                                                            </div>  
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-alert.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Safety</strong><br>College Safety announcements and contact numbers can be found here.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-contacts.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Directory Search</strong><br>Search for faculty and staff contact information with our directory search.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-events.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Events</strong><br>Find out what's coming up at Valencia.  Browse upcoming Student Events at each campus.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-map.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Maps</strong><br>View a map of each campus and all of the listed buildings. This map includes a GPS
                                                                           location of where you are in respect to the campus facilities.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-news.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>News</strong><br>Keep up-to-date with all of the Valencia College news updates with a simple RSS connection
                                                                           to the Valencia News Web site.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-photos.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Photos</strong><br>Browse Valencia Photos.  Take a look through the photo sets and you may see a familiar
                                                                           face.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img alt="Twitter" src="icon-twitter-44.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Twitter</strong><br>Keep in touch with Valencia tweets.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-videos.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Videos</strong><br>View Valencia's Videos.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-account.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Account Balance</strong><br>Never feel in the dark about your account balance. This app will give you the peace
                                                                           of mind that you expect and deserve when wanting to know where you stand with your
                                                                           account balance.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-grades.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Grades</strong><br>Have easy access to view your grades anytime and from anywhere.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-schedule.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Schedule</strong><br>Know your schedule and stick to it without question.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-soon.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Coming Soon</strong><br>Find up and coming features that Valencia College is producing to better your all
                                                                           around app experience.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-blackboard.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Blackboard Mobile Learn</strong>&nbsp;&nbsp;<img border="0" src="icon-new-quicklink.png"><br>Link to your Blackboard Learn mobile application. If you have Blackboard Learn installed
                                                                           on your mobile device, you can link to it from the Valencia mobile application.
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div><img src="icon-AtlasAnn.png"></div>
                                                                        
                                                                        <div>
                                                                           <strong>Atlas Announcements</strong>&nbsp;&nbsp;<img border="0" src="icon-new-quicklink.png"><br>Get the latest Atlas announcements on your mobile device
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                               
                                                            </div>   
                                                            
                                                            
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        <div><strong>Help Desk Phone Numbers</strong></div>
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Students</div>
                                                                        
                                                                        <div>407-582-5444</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Faculty/Staff</div>
                                                                        
                                                                        <div>407-582-5555</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        <div><span><strong>Help Desk Email</strong></span></div>
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Students</div>
                                                                        
                                                                        <div><a href="mailto:%20askatlas@valenciacollege.edu">askatlas@valenciacollege.edu</a></div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Faculty/Staff</div>
                                                                        
                                                                        <div><a href="mailto:oithelp@valenciacollege.edu">oithelp@valenciacollege.edu</a></div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                               
                                                               
                                                            </div>    
                                                            
                                                            
                                                            
                                                            
                                                         </div>  
                                                         
                                                         
                                                      </div>
                                                      <br><br>
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="770"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/mobile/index.pcf">©</a>
      </div>
   </body>
</html>