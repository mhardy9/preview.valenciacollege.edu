<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/mobile/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/mobile/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/mobile/">Mobile</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <a name="top" id="top"></a>
                        
                        
                        <h2>Valencia Mobile FAQ</h2>
                        
                        
                        
                        <h3>1. Are you going to add more features in the future?</h3>
                        
                        <blockquote>
                           
                           <p>Yes, we are working on the application to improve functionality and add new features.
                              You can preview what is coming by using the Coming Soon button in the application.
                              
                           </p>
                           
                        </blockquote>
                        
                        <h3>2. Is there a suggestion box for feedback? Where do I report issues?</h3>
                        
                        <blockquote>
                           
                           <p>Yes, you can either <a href="feedback.html">click here</a> to submit feedback and report issues.  You can also find the link to the feedback
                              form in the application in the information icon at the bottom right of the application
                              screen. 
                           </p>
                           
                        </blockquote>
                        
                        <h3>3. What user name and password do I use to login?</h3>
                        
                        <blockquote>
                           
                           <p>You can use your Atlas user name and password to login to any application with a lock
                              icon on it. 
                           </p>
                           
                        </blockquote>
                        
                        <h3>4. Who can I ask for help with the mobile application?</h3>
                        
                        <blockquote>
                           
                           <p>Students: Please visit an Atlas Access lab on your campus or call the student help
                              desk at x5444. Faculty and Staff: Please call the OIT help desk at x5555. 
                           </p>
                           
                        </blockquote>
                        
                        <h3>5. Why can't I access the Account Balance, Grades or Schedule?</h3>
                        
                        <blockquote>
                           
                           <p>These applications are only available (after a login) to Students.  If you are not
                              a student, these icons will disappear after you login with your Atlas Username &amp; Password.
                              
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>
                           <a name="6" id="6"></a>6. Can I configure my mobile device to work with my Atlas e-mail?
                        </h3>
                        
                        <p>Valencia recommends the use of the Microsoft Outlook mobile application to access
                           your Atlas e-mail.
                        </p>
                        
                        <p>Detailed instructions for installing the Microsoft Outlook mobile application for
                           iOS and Andriod devices can be found here:  <a href="https://valenciacollege.zendesk.com/hc/en-us/articles/220251088" target="_blank">Atlas Email Mobile Configuration</a></p>
                        
                        <p>NOTICE:   Valencia College does not provide support for Mail Enabled Cell Phones.
                           This information is provided only as a matter of courtesy. OIT and Valencia College
                           assume no liability for use or misuse of this information.
                        </p>
                        
                        <p><strong>IT IS STRONGLY RECOMMENDED THAT IF YOU CHOOSE TO DO THIS THAT YOU USE A PASSCODE ON
                              YOUR MOBILE DEVICE</strong><br>
                           <br>
                           Before you begin please read these <a href="http://www.informationweek.com/news/security/mobile/232601089" target="_blank">10 Steps To Smart Phone Security</a> from <em>Information Week Security</em></p>
                        
                        
                        
                        
                        <h3>
                           <a name="7" id="7"></a>7. I'm having problems connecting to Valencia Wi-Fi.
                        </h3>
                        
                        <blockquote>
                           The Valencia Mobile App will function over the phone network.  But if you would like
                           to avoid data chargese, you can also connect over Wi-Fi to use the Valencia Mobile
                           App.  Some users have reported issues loading the Valencia Mobile App when connected
                           to the Valencia Wi-Fi network.  If you are having problems downloading information
                           while connected to Valencia's Wi-Fi network, try these steps:
                           <br>
                           
                           <ul>
                              
                              <li>Make sure you are connected to the Wi-Fi network "vccnet"</li>
                              
                              <li>Open your device's Web Browser</li>
                              
                              <li>Go to the <a href="https://10.10.6.20:8443/authenticate.!%5E" target="_blank">Wireless Login Page</a>
                                 
                              </li>
                              
                              <li>Login with your Atlas Username &amp; Password</li>
                              
                              <li>If you are redirected to the Valenica College Homepage, you are connected to the "vccnet"
                                 Wi-Fi
                              </li>
                              
                           </ul>
                           You should then be able to connect to the Valencia Mobile app via Valencia Wi-Fi.<br>
                           <br>
                           If you are connected to any other Wi-Fi network and are having problems loading content
                           in the Valencia Mobile App, please make sure that network has connectivity.<br><br>If you are still having problems, please submit an issue report (Broken Bug/Fix) through
                           the <a href="feedback.html">Mobile Feedback Form</a>.
                           
                        </blockquote>
                        
                        
                        <h3>8. I'm having problems opening the Valencia Mobile app after upgrading to iOS6.</h3>
                        
                        <blockquote> 
                           If you are having issues launching the application after the most recent upgrade to
                           iOS6 on your iPhone or iPad, force-quit the application and then relaunch it. If this
                           doesn't work, reboot your device. <br>
                           <br>
                           To Force-quit an application in iOS follow these steps:  
                           
                           <ol>
                              
                              <li>Double-click the home button to display the recently accessed applications.</li>
                              
                              <li>Press and hold any of the icons shown, then navigate to the Valencia Mobile App and
                                 tap the red circled minus button. This sends a signal to the application in question
                                 that allows it to quit.
                              </li>
                              
                              <li>Relaunch the Valenica Mobile App.</li>
                              
                           </ol>
                           
                        </blockquote>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/mobile/faq.pcf">©</a>
      </div>
   </body>
</html>