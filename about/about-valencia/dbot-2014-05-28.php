<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>May 28, 2014 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2014-05-28.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>May 28, 2014 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>May 28, 2014 Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           		
                           <li><a href="/about/about-valencia/documents/Agenda-May282014.pdf"><strong>Agenda</strong></a></li>
                           		
                           <li><a href="/about/about-valencia/documents/Minutes-May-28-2014.pdf">Approval of Regular Minutes - Apr 23, 2014 </a></li>
                           		
                           <li>President's Report </li>
                           	  
                           <li><a href="/about/about-valencia/documents/2014-May-ValenciaBOT-GotCollege.pdf">Update on "Got College?" and Efforts to Increase College-Going Rate in Osceola County
                                 </a></li>
                           
                           <li><a href="/about/about-valencia/documents/2014FloridaLegislativeSessionReport-May282014.pdf">2014 Florida Legislative Session Report </a></li>
                           
                           <li><a href="/about/about-valencia/documents/PublicComments-May282014.pdf">Public Comment</a> 
                           </li>
                           	
                        </ul>
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/ArchitecturalServicesApproval-May282014.pdf">Approval of Contract for Architectural Services Request for Qualifications</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ArchitecturalServicesApproval-May282014.pdf"> (RFQ) 13/14-19 (East Campus Arts &amp; Operations Building)</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PreliminaryBudgetandFees2014-2015-May282014.pdf">Approval of Preliminary Budget &amp; Fees FY 2014-2015 </a></li>
                           
                           <li><a href="/about/about-valencia/documents/Approvalof2014-2015CollegeCatalog-May282014_000.pdf">Approval of 2014-2015 College Catalog</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PolicyAdoptions-May282014_000.pdf">Policy Adoptions</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ContinuingEducationCoursesandFees-May282014.pdf">Continuing Education Courses &amp; Fees</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda-May282014_001.pdf">Human Resources Agenda</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-May282014.pdf">Submission of Grant Proposals</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeletions-May282014.pdf">Property Deletions</a></li>
                           	
                        </ul>
                        	
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/OsceolaCampusSGAReport-May282014.pdf">Osceola Campus Student Government Association (SGA) Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/InternalAuditorReport-May282014_000.pdf">Internal Auditor Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportMay2014.pdf">Financial Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReport-May282014.pdf">Construction Report</a> 
                           </li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReport-May282014.pdf">Valencia Foundation Report</a> 
                           </li>
                           	
                        </ul>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2014-05-28.pcf">©</a>
      </div>
   </body>
</html>