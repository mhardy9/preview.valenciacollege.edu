<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia's Roles  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/roles.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>About Us</h1>
            <p>
               
               			
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>Valencia's Roles </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               			
               <div class="container margin-60" role="main">
                  	
                  <div class="row">
                     	
                     <div class="col-md-12">
                        
                        	
                        <h2>Valencia's Roles and Functions</h2>
                        				
                        <p><strong>The Transfer Function</strong><br>
                           				  A college-level program of general education and specialized courses designed
                           to prepare students to transfer to baccalaureate degree-granting colleges and universities.
                           
                        </p>
                        				
                        <p><strong>The Technical Education Function</strong><br>
                           				  Technical college-level programs designed to prepare graduates to enter immediately
                           into the workforce. 
                        </p>
                        				
                        <p><strong>The Economic Development Function</strong><br>
                           				  Courses, workshops, conferences, seminars, and activities designed to support
                           economic development and meet the needs of the community by preparing students for
                           high-wage, high skill jobs. 
                        </p>
                        				
                        <p><strong>The General Education Function</strong><br>
                           				  General education courses designed to prepare students for responsible citizenship,
                           for wholesome and creative participation in life activities, and for intelligent decision
                           making. 
                        </p>
                        				
                        <p><strong>The Student Services Function</strong><br>
                           				  Student Services and activities designed to assist students in assessing and
                           achieving their goals through academic, career, and personal decision making. 
                        </p>
                        				
                        <p><strong>The College-Preparatory Function</strong><br>
                           				  College-preparatory courses designed to enable students to achieve college-level
                           competency in reading, writing, mathematics, and elementary algebra. 
                        </p>
                        
                        
                        
                        			  
                     </div>
                     
                     	
                  </div>
                  
                  	
               </div>
               		
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/roles.pcf">©</a>
      </div>
   </body>
</html>