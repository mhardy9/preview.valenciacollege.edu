<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Facts  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/facts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>Facts </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        					
                        <h2> Valencia College Facts </h2>
                        					
                        <p><strong>NOTABLE HIGHLIGHTS</strong></p>
                        					
                        <ul class="list-style-1">
                           						
                           <li> Valencia was named the best community college in the nation as the inaugural winner
                              of the Aspen Prize for Community College Excellence.
                           </li>
                           						
                           <li>Valencia’s economic impact on the region is more than $1 billion a year.</li>
                           						
                           <li>One out of four UCF graduates started at Valencia.</li>
                           					
                        </ul>
                        
                        					
                        <hr>
                        					
                        <p><em>Unless otherwise noted, all information below is from the 2015–2016 academic year.</em></p>
                        					
                        <hr>
                        
                        					
                        <h3>ACADEMICS:</h3>TUITION, FINANCIAL AID, DEGREES, GRADS, DirectConnect to UCF
                        					
                        <h4>TUITION PER CREDIT HOUR</h4> <em>(2015-2016)</em>
                        					
                        <p>Valencia’s tuition is  half that of a state university and is the lowest public college
                           tuition in Central Florida.
                        </p>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “TUITIONTYPE”">TUITION TYPE</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “ASSOCIATE”">ASSOCIATE</a></th>
                                    									
                                    <th width="70" scope="col"><a href="#" title="Sort on “BACCALAUREATE”">BACCALAUREATE</a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              							
                              <tr>
                                 								
                                 <td>In-State Tuition:</td>
                                 								
                                 <td>
                                    <div>$103.06</div>
                                 </td>
                                 								
                                 <td>
                                    <div>$112.19</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Out-of-State Tuition:</td>
                                 								
                                 <td>
                                    <div>$390.96</div>
                                 </td>
                                 								
                                 <td>
                                    <div>$427.59</div>
                                 </td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>FINANCIAL AID</h4> <em>(As of September 2016) </em>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “FINAID”">FINAID</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”">AMOUNT</a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              							
                              <tr>
                                 								
                                 <td>Total Aid Awarded</td>
                                 								
                                 <td>$136,339,644</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Students Receiving Aid</td>
                                 								
                                 <td>44.7%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Bright Futures Recipients</td>
                                 								
                                 <td>871</td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>DEGREE PROGRAMS</h4>
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “DEGREE”">DEGREE PROGRAMS</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AVAILABLE”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>Associate in Arts (A.A.)</td>
                                 								
                                 <td>
                                    <div>1</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Associate in Science (A.S.)</td>
                                 								
                                 <td>
                                    <div>34</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Certificate Programs</td>
                                 								
                                 <td>
                                    <div>81</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p>Bachelor of Science (B.S.)</p>
                                    									
                                    <ul class="list-style-1">
                                       										
                                       <li> B.S. in Electrical and Computer Engineering Technology</li>
                                       										
                                       <li> B.S. in Radiologic and Imaging Sciences</li>
                                       										
                                       <li> B.S. in Cardiopulmonary Sciences</li>
                                       									
                                    </ul>                  
                                 </td>
                                 								
                                 <td>
                                    <div>3</div>
                                 </td>
                                 							
                              </tr>
                              
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>GRADUATE SUCCESS</h4>
                        					
                        <p>Valencia is ranked 4th among the nation’s colleges and universities in the number
                           of associate degrees awarded.
                        </p>
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “GRADUATESUCCESS”">GRADUATE SUCCESS</a></th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>A.S. graduate placement rate:</td>
                                 								
                                 <td>93-97%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>A.S. average annual salary:<br>
                                    									*According to FETPIP 2014-2015 data.
                                 </td>
                                 								
                                 <td>
                                    <div>*$44,764</div>
                                 </td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        
                        					
                        <h4>GRADUATES BY DEGREE/CERTIFICATE</h4>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “DEGREE”">DEGREE PROGRAMS</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”"></a></th>
                                    									
                                    <th width="50" scope="col"><a href="#" title="Sort on “PERCENT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>Associate in Arts (A.A.)</td>
                                 								
                                 <td>6,033</td>
                                 								
                                 <td>48.7%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Associate in Science (A.S.)</td>
                                 								
                                 <td>1,392</td>
                                 								
                                 <td>11.2%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Bachelor of Science (B.S.) </td>
                                 								
                                 <td>62</td>
                                 								
                                 <td>0.5%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Technical Certificate</td>
                                 								
                                 <td>4,434</td>
                                 								
                                 <td>35.8%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Career Certificate</td>
                                 								
                                 <td>375</td>
                                 								
                                 <td>3.0%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Educator Preparation Institute Certificate</td>
                                 								
                                 <td>97</td>
                                 								
                                 <td>0.8%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>Total</strong></td>
                                 								
                                 <td><strong>12,393</strong></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>DIRECTCONNECT TO UCF</h4>
                        					
                        <ul class="list-style-1">
                           						
                           <li> Valencia graduates are UCF’s number one source of transfer students (35%).</li>
                           						
                           <li>UCF offers 18 complete bachelor’s degrees on Valencia’s West and Osceola campuses.</li>
                           					
                        </ul>
                        
                        					
                        <hr>
                        					
                        <h4>ANNUAL ENROLLMENT</h4>
                        					
                        <p>Valencia is the 3rd largest of Florida’s 28 community colleges in student headcount.</p>
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “CREDIT”">CREDIT</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td><strong>Credit</strong></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Credit-Seeking Students</td>
                                 								
                                 <td>60,883</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>Non-Credit</strong></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Educator Preparation Institute: </td>
                                 								
                                 <td>200</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Postsecondary Adult Vocational: </td>
                                 								
                                 <td>482</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Continuing Workforce Education: </td>
                                 								
                                 <td>3,967</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Continuing Education: </td>
                                 								
                                 <td>7,586</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Conferences and Seminars: </td>
                                 								
                                 <td>887</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>Total Individuals Served*: </strong></td>
                                 								
                                 <td><strong>74,005</strong></td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           						
                           <p>* Includes credit students, postsecondary adult vocational, Educator Preparation Institute,
                              continuing education, conferences and seminars. Total is approximate - there may be
                              some duplication across categories.
                           </p>
                           						*Average Class size: 21
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>ENROLLMENT BY CAMPUS </h4>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “CAMPUS”">ENROLLMENT BY CAMPUS</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>East</td>
                                 								
                                 <td>28,153</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Lake Nona</td>
                                 								
                                 <td>2,452</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Osceola</td>
                                 								
                                 <td>12,896</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>West</td>
                                 								
                                 <td>25,916</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Winter Park </td>
                                 								
                                 <td>
                                    <div>2,640</div>
                                 </td>
                                 							
                              </tr>
                              
                              							
                              <tr>
                                 								
                                 <td>Students enrolled at more than one campus:</td>
                                 								
                                 <td>27.3%</td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>COUNTY OF ORIGIN</h4>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “COUNTY”">COUNTY</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>Orange County</td>
                                 								
                                 <td>31.8%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Osceola County</td>
                                 								
                                 <td>37.1%</td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>STUDENT CHARACTERISTICS</h4>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “RACE”">RACE / ETHNICITY</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “PERCENT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>Caucasian</td>
                                 								
                                 <td>30.9%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Hispanic</td>
                                 								
                                 <td>34.0%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>African-American</td>
                                 								
                                 <td>17.4%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Asian</td>
                                 								
                                 <td>4.7%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Multi-Race</td>
                                 								
                                 <td>2.4%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Hawaiian</td>
                                 								
                                 <td>0.4%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Native American </td>
                                 								
                                 <td>0.3%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Unknown</td>
                                 								
                                 <td>9.8%</td>
                                 
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “ENROLLMENTSTATUS”">ENROLLMENT STATUS</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>Full-Time</td>
                                 								
                                 <td>36.5%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Part-Time</td>
                                 								
                                 <td>63.5%</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>Median Student Age</strong></td>
                                 								
                                 <td>21</td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <h4>INSTITUTION: HISTORY, BOARD, EMPLOYEES, BUDGET, LOCATION</h4>
                        
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “FACTS”">FACTS</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “INFO”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td><strong>HISTORY</strong></td>
                                 								
                                 <td>Founded in 1967</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>SERVICE AREA</strong></td>
                                 								
                                 <td>Orange and Osceola counties</td>
                                 							
                              </tr>
                              
                              							
                              <tr>
                                 								
                                 <td><strong>ACCREDITATION</strong></td>
                                 								
                                 <td>Commission on Colleges of the Southern Association of Colleges and Schools (SACS)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>ADMINISTRATION</strong></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>College President</strong></td>
                                 								
                                 <td>Sanford C. Shugart</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    
                                    								
                                 </td>
                                 								
                                 <td>
                                    
                                    								
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              
                              							
                              <tr>
                                 								
                                 <td><strong>Campus Presidents</strong></td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              
                              							
                              <tr>
                                 								
                                 <td>Stacey Johnson</td>
                                 								
                                 <td>East &amp; Winter Park campuses</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Kathleen Plinske</td>
                                 								
                                 <td>Osceola, Lake Nona &amp; Poinciana campuses</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Falecia Williams</td>
                                 								
                                 <td>West &amp; UCF/Valencia Downtown Orlando campuses </td>
                                 
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    									<strong>Employees</strong> (as of July 2016)
                                 </td>
                                 								
                                 <td></td>
                                 							
                              </tr>
                              
                              							
                              <tr>
                                 								
                                 <td>Staff</td>
                                 								
                                 <td>2,122</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Full-Time Faculty</td>
                                 								
                                 <td> 546</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Part-Time Faculty (Adjunct)</td>
                                 								
                                 <td>1,065</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td><strong>Total</strong></td>
                                 								
                                 <td><strong>3,733</strong></td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        
                        					
                        <h4>BOARD OF TRUSTEES</h4>
                        					Lewis M. Oliver, III, Chair<br>
                        					Bruce Carlson, Vice Chair <br>
                        					Maria Grulich<br>
                        					John Crossman <br>
                        					Guillermo Hansen<br>
                        					Daisy Lopez-Cid<br>
                        					Raymer Maguire III
                        
                        					
                        <hr>
                        
                        					
                        <h4>BUDGET</h4>
                        					
                        <div class="table-responsive">
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <th width="100" scope="col">
                                       										<a href="#" title="Sort on “BUDGET”">REVENUES</a> 
                                    </th>
                                    									
                                    <th width="100" scope="col"><a href="#" title="Sort on “AMOUNT”"></a></th>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              
                              							
                              <tr>
                                 								
                                 <td>2016-2017 Projected Revenues:</td>
                                 								
                                 <td>$193.5 million</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>2015-2016 Actual Revenues:</td>
                                 								
                                 <td>$184.1 million</td>
                                 							
                              </tr>
                              
                              
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <hr>
                        					
                        <p><strong>Valencia Foundation Endowment:</strong></p>
                        					
                        <p>Thanks to alumni donations, grants and contributions from businesses and individuals,
                           Valencia has one of the country’s largest community college endowments at $63.7 million.
                        </p>
                        				
                     </div>
                     
                     			
                  </div>
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/facts.pcf">©</a>
      </div>
   </body>
</html>