<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>June 21, 2011 Board of Trustees Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2011-06-21.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>June 21, 2011 Board of Trustees Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>June 21, 2011 Board of Trustees Regular Meeting </strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/AgendaBOT6-21-11.pdf"><strong>Agenda</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/Minutes4-19-11.pdf">Minutes 4-19-11</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Minutes5-17-11.pdf">Minutes 5-17-11</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Tillerbio.pdf">Joan Tiller Bio</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReport_000.pdf">Construction Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FoundationReport.pdf">Foundation Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AgendaItemsfrom5-17-11DBOTmtg..pdf">Agenda Items from 5-17-11 DBOT mtg.</a></li>
                           
                           <li><a href="/about/about-valencia/documents/OperatingBudget.pdf">Operating budget</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ResolutionforCapitalOutlayBond.pdf">Resolution for Capital Outlay Bond</a></li>
                           
                           <li><a href="/about/about-valencia/documents/CorporateTrainingandClassroomBldg.WC.pdf">Corporate Training &amp; Classroom Bldg., West Campus</a></li>
                           
                           <li><a href="/about/about-valencia/documents/RequesttoReserveFundBalance.pdf">Request to Reserve Fund Balance</a></li>
                           
                           <li><a href="/about/about-valencia/documents/DowntownCenterLease.pdf">Downtown Center Lease</a></li>
                           
                           <li><a href="/about/about-valencia/documents/OsceolaCampusLease.pdf">Osceola Campus Lease</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SpotSurveyOsceolaCampus.pdf">Spot Survey Osceola Campus</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FireandCasualtySafetyandSanitationInspection.pdf">Fire &amp; Casualty Safety &amp; Sanitation Inspection</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationDirectSupportOrganization.pdf">Valencia Foundation Direct Support Organization</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaEnterprisesCoursesFees062111.pdf">Valencia Enterprises Courses and Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda_001.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AnnualEquityUpdateExecutiveSummaryandCoverLetter.pdf">Annual Equity Update Executive Summary</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AnnualEquityUpdatePlanPartii.pdf">Annual Equity Update Plan Part ii</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals_000.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/DelinquentAccountWriteOff.pdf">Delinquent Account Write-Off</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeletions_001.pdf">Property Deletions</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ProjectPriorityList062111.pdf">Project Priority List 6-21-11 </a> 
                           </li>
                           
                        </ul>                      
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2011-06-21.pcf">©</a>
      </div>
   </body>
</html>