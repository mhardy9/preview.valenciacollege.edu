<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>May 25, 2016 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2016-05-25.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>May 25, 2016 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>May 25, 2016 - Regular Meeting</strong></p>
                        			
                        <ul class="list-style-1">
                           		
                           <li><a href="/about/general-counsel/policy/historical/Agenda-RegularMeeting-OsceolaCampus-May252016.pdf"><strong>Agenda</strong></a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/RegularMeetingMinutes-Apr272016.pdf">APPROVAL OF MINUTES - April 27, 2016 Regular Meeting</a> 
                           </li>
                           
                           <li>President's Report</li>
                           
                           
                           <li><strong><a href="/about/general-counsel/policy/historical/Resume-NewVPInfoTechnologyandCIO-May252016.pdf">Resume of New Vice President, Information Technology &amp; CIO</a></strong></li>
                           
                           <li>Public Comment </li>
                           			
                        </ul>
                        				
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/GMPPoincianaCampusBuilding01PhaseMay2016.pdf">Guaranteed Maximum Price - Poinciana Campus Site Preparation, Bldg. 01 Phase 1</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/2016-2017BudgetOverview-May252016.pdf">2016-2017 Budget Overview</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/StudentFees2016-2017-May252016.pdf">Approval of Student Fees With No Increase 2016-2017</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/Approvalof2016-2017CollegeCatalog-May252016.pdf">Approval of 2016-2017 College Catalog</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/HumanResourcesAgenda-May252016.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/AdditionsDeletionsModificationsofCoursesandPrograms-May252016.pdf">Additions, Deletions or Modifications of Courses &amp; Programs</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/NewBusiness-ContinuingEducationCoursesandFees-May252016.pdf">Continuing Education Courses &amp; Fees </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/PropertyDelete05-25-16.pdf">Property Deletions </a></li>
                           
                           <li>Board Comments</li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        
                        <ul class="list-style-1">
                           	
                           <li><a href="/about/general-counsel/policy/historical/PoincianaCampusReport-May252016.pdf">Poinciana Campus Update</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/OsceolaCampusReport-May252016.pdf">Osceola Campus Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/OsceolaSGAReport-May252016.pdf">Osceola SGA Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/FinancialReport-May252016.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/ConstructionReportMay2016.pdf">Construction Report</a></li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/general-counsel/policy/historical/valenciafoundationtrusteereportmay2016.pdf">Valencia Foundation Report </a></li>
                           
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2016-05-25.pcf">©</a>
      </div>
   </body>
</html>