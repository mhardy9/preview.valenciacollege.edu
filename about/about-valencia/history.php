<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>History  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/history.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>History </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>History of Valencia College</h2>
                        
                        <p>From humble beginnings, Valencia has become an innovative leader in education with
                           a national reputation for student success. 
                        </p>
                        
                        
                        
                        <h4>1967: In the Beginning</h4>
                        
                        <p>In fall of 1967, we began as Valencia Junior College in a few portable buildings on
                           West Oakridge Road in Orlando. Although the facilities weren't lofty, our goal was
                           to help our students become as successful as possible in their classes, their careers
                           and their lives. Armed with a passion for teaching and a concern for the students,
                           a small group of educators began to teach the increasing number of students coming
                           to the campus. 
                        </p>
                        
                        
                        
                        <h4>1971-1998: Growth Years</h4>
                        
                        
                        <p>In 1971, we changed our name to Valencia Community College and moved to our first
                           permanent location, the 180-acre <a href="/locations/west/index.php">West Campus</a> located on South Kirkman Road. To meet the needs of a growing community, Valencia
                           continued to expand, opening additional campus locations. The <a href="/locations/east/index.php">East Campus</a> opened in 1975, the <a href="/locations/osceola/index.php">Osceola Campus</a> in 1997 and the <a href="/locations/winter-park/index.php">Winter Park Campus</a> in 1998. 
                        </p>
                        
                        
                        
                        <h4>1995-2000: New Focus on Student Success</h4>
                        
                        
                        <p>Even as Valencia grew, we never lost sight of our original goals. Armed with practical
                           knowledge, Valencia's faculty and staff put their energies into developing a "learning-centered"
                           approach to teaching. Adopted in 1995, this philosophy emphasizes individual student
                           success and is still in effect today. 
                        </p>
                        
                        <p>In 2000, Valencia was recognized for the effectiveness of our student-first philosophy
                           when we were selected by the League for Innovation in the Community College as one
                           of twelve international <a href="/about/learning-centered-initiative/index.php">Vanguard Learning Colleges</a>.
                        </p>
                        
                        
                        
                        <h4>2000-2010: Building Partnerships</h4>
                        
                        
                        <p>In an ongoing effort to be responsive to the community, Valencia has always built
                           relationships with K-12 school districts, universities and corporate partners to strengthen
                           academic scholarship and economic development. 
                        </p>
                        
                        <p>In 2002, we opened the Walt Disney World® Center for Hospitality and Culinary Arts
                           on the West Campus and in 2004, we opened the <a href="/locations/school-of-public-safety/criminal-justice-institute/index.php">Criminal Justice Institute</a>, which provides training academies for law enforcement and corrections officers.
                           
                        </p>
                        
                        <p>In 2006, we established a unique partnership to expand students’ access to bachelor’s
                           degrees. <a href="http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/">DirectConnect to UCF</a> guarantees* Valencia graduates admission to the University of Central Florida and
                           is now the most productive university-community college partnership in the country
                           (*Consistent with university policy.).
                        </p>
                        
                        <p>In 2008, Valencia, in partnership with Orange County Public Schools, began administering
                           Take Stock in Children of Orange County, a program that provides mentoring and college
                           scholarships to promising middle school students from low-income backgrounds.
                        </p>
                        
                        <p>In 2009, we partnered with Northrop Grumman Laser Systems and Orange County Public
                           Schools to create the Photonics Academy at Wekiva High School and in 2010, we began
                           administering a training program for the Transportation Security Administration (TSA)
                           at the Orlando International Airport. Valencia has hundreds of additional corporate
                           education/training partnerships, including those with Lockheed Martin, CNL, Darden,
                           Universal Studios®, Sysco and Bright House Networks.
                        </p>
                        
                        
                        
                        <h4>2011: Big Changes and Recognition</h4>
                        
                        
                        <p>In July 2011, we dropped the “community” from our name and became Valencia College.
                           That same year, we began to offer <a href="http://preview.valenciacollege.edu/future-students/degree-options/bachelors/">bachelor’s degrees</a> for the first time with the launch of two new programs: Radiologic and Imaging Sciences,
                           and Electrical and Computer Engineering Technology.
                        </p>
                        
                        <p>By the end of 2011, Valencia was named the inaugural winner of the <a href="http://news.valenciacollege.edu/academic-issues/valencia-named-top-community-college-in-nation">Aspen Prize for Community College Excellence</a> as “the best community college in the nation.” The prize was based on measurable
                           achievements in graduation rates, workforce placement and innovative programs.
                        </p>
                        
                        
                        <h4>Present Day</h4>
                        
                        
                        <p>Recognized nationally as the best community college in America, Valencia is also one
                           of the largest, with more than 71,000 students enrolled in 2011. We also award more
                           associate degrees than any other community college in the nation, with our graduates
                           going on to successful futures. Our A.S. and A.A.S. degree graduates have a 95 percent
                           job-placement rate with an average annual salary of about $43,000.
                        </p>
                        
                        <p>In an effort to continue to meet the demands of a growing population, Valencia opened
                           the <a href="/locations/lake-nona/index.php">Lake Nona Campus</a> in east Orlando in August 2012 and also plans to build a Poinciana campus within
                           the next few years. 
                        </p>
                        
                        <p>Looking toward the future, Valencia will continue to seek innovative and effective
                           ways to improve student success and serve the Central Florida community.
                        </p>
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/history.pcf">©</a>
      </div>
   </body>
</html>