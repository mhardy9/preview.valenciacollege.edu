<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Board of Trustees Meeting Schedule  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/trusteeschedule.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>About Us</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>Board of Trustees Meeting Schedule </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees Meeting Schedule</h2>
                        
                        <p>Please note that these meeting dates may be subject to change at the Board's discretion.
                           All meetings begin at <strong>9:30 A.M.</strong> unless otherwise noted.
                        </p>
                        
                        <p>If you wish to contact a Board member, you may address all correspondence to P. O.
                           Box 3028, Orlando, FL 32802.
                        </p>
                        
                        <h4>Meeting Schedule 2017-2018</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2018 July 25</td>
                                    
                                    <td>District Office, Room 502</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td>Agenda and Materials</td>
                                    
                                 </tr>
                                 
                                 <tr data-old-tag="tr">
                                    
                                    <td>2018 July 25</td>
                                    
                                    <td>District Office, Room 502</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td>Agenda and Materials</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2018 June 27</td>
                                    
                                    <td>School of Public Safety, Auditorium</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td>Agenda and Materials</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2018 May 23</td>
                                    
                                    <td>Lake Nona Campus, Room 148</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td>Agenda and Materials</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2018 April 25</td>
                                    
                                    <td>Winter Park Campus, Room 225-226</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td>Agenda and Materials</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2018 February 28</td>
                                    
                                    <td>Osceola Campus, Room 4-105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td>Agenda and Materials</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017 December 6</td>
                                    
                                    <td>Poinciana Campus, Room 125</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td>Agenda and Materials</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017 October 25</td>
                                    
                                    <td>SAE - Room 133</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2017-10-25.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr data-old-tag="tr">
                                    
                                    <td>2017 September 27</td>
                                    
                                    <td>West Campus, Room 8-111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2017-09-27.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017 September 27</td>
                                    
                                    <td>West Campus, Room 8-111</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/dbot-2017-09-27-organizational.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Meeting Schedule 2016-2017</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2017 June 28</td>
                                    
                                    <td>Osceola Campus, Rm 4-105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2017-06-28.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017 May 24</td>
                                    
                                    <td>Winter Park Campus, Rm 225-226</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2017-05-24.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017 April 26</td>
                                    
                                    <td>Lake Nona Campus, Rm 148</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2017-04-26.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017 February 22</td>
                                    
                                    <td>School of Public Safety, Auditorium</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2017-02-22.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017 February 6</td>
                                    
                                    <td>District Office, Rm 502</td>
                                    
                                    <td>Special Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2017-02-06.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 December 7</td>
                                    
                                    <td>Osceola Campus, Rm 4-105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-01-27.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 November 2</td>
                                    
                                    <td>East Campus, Rm 5-112</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-11-02.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 September 28</td>
                                    
                                    <td>West Campus, Rm 8-111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-09-28.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Meeting Schedule 2015-2016</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2016 July 27</td>
                                    
                                    <td>District Office, Rm 502</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-07-27.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 July 27</td>
                                    
                                    <td>District Office, Rm 502</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-07-27-Organizational.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 June 22</td>
                                    
                                    <td>East Campus, Rm 5-112</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-06-22.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 May 25</td>
                                    
                                    <td>Osceola Campus, Rm 4-105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-05-25.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 April 27</td>
                                    
                                    <td>Winter Park Campus, Rm 225-226</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-04-27.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016 February 24</td>
                                    
                                    <td>West Campus, Rm 8-111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2016-02-24.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 December 9</td>
                                    
                                    <td>Osceola Campus, Rm 4-105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-01-29.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 October 21</td>
                                    
                                    <td>Lake Nona, Rm 148</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-10-21.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 September 23</td>
                                    
                                    <td>School of Public Safety - Auditorium</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-09-23.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 September 23</td>
                                    
                                    <td>School of Public Safety - Auditorium</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-09-23-Organizational.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Meeting Schedule 2014-2015</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2015 June 24</td>
                                    
                                    <td>East Campus, Rm 5-112</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-06-24.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 May 27</td>
                                    
                                    <td>Osceola Campus, Rm 4-105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-05-27.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 April 22</td>
                                    
                                    <td>Winter Park Campus, Rm 225-226</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-04-22.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 Mar 2</td>
                                    
                                    <td>District Office, Rm 502</td>
                                    
                                    <td>Budget Workshop</td>
                                    
                                    <td><a href="/about/about-valencia/documents/BudgetWorkshop-Mar22015.pdf" target="_blank">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015 February 25</td>
                                    
                                    <td>West Campus, Rm 8-111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2015-02-25.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 December 17</td>
                                    
                                    <td>Osceola Campus, Rm 4-105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-12-17.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 October 22</td>
                                    
                                    <td>Lake Nona, Rm 148</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-10-22.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 September 24</td>
                                    
                                    <td>CJI - Auditorium</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-09-24.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Meeting Schedule 2013-2014</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2014 July 23</td>
                                    
                                    <td>District Office, Rm 502</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-07-23.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 July 23</td>
                                    
                                    <td>District Office, Rm 502</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/documents/OrganizationalMeeting-Jul232014_000.pdf" target="_blank">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 June 19</td>
                                    
                                    <td>East Campus, Building 5, Rm 112</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-06-19.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 May 28</td>
                                    
                                    <td>Osceola Campus, Building 4, Rm 105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-05-28.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 April 23</td>
                                    
                                    <td>Winter Park Campus, Rm 225-226</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-04-23.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 February 26</td>
                                    
                                    <td>West Campus, Building 8, Rm 111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-02-26.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014 January 27</td>
                                    
                                    <td>West Campus, Building 10, 3rd Floor - Collaborative Design Center</td>
                                    
                                    <td>Facilities Strategy Workshop</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2014-01-27.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 December 4</td>
                                    
                                    <td>CJI - Auditorium</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-12-04.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 October 16</td>
                                    
                                    <td>Lake Nona Campus, Rm 148</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-10-16-02.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 September 17</td>
                                    
                                    <td>East Campus, Building 5, Rm 112</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-09-17.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Meeting Schedule 2012-2013</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2013 July 16</td>
                                    
                                    <td>Osceola Campus, Building 4, Rm 105</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-07-16.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 July 16</td>
                                    
                                    <td>Osceola Campus, Building 4, Rm 105</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/documents/OrganizationalMeeting-07-16-2013.pdf" target="_blank">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 June 18</td>
                                    
                                    <td>Downtown Center, Rm 106</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-06-18.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 May 21</td>
                                    
                                    <td>Winter Park Campus, Rm 225/226</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-05-21.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 April 16</td>
                                    
                                    <td>West Campus, Building 8, Rm 111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-04-16.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 April 9</td>
                                    
                                    <td>Downtown Center, Rm 106</td>
                                    
                                    <td>Budget Workshop</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-02-09-budget-workshop.php">Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 February 28</td>
                                    
                                    <td>Osceola Campus, Building 1, Rm 219B</td>
                                    
                                    <td>Special Meerting</td>
                                    
                                    <td><a href="/about/about-valencia/documents/Agenda-Special-Meeting-022813.pdf" target="_blank">Agenda</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 February 27</td>
                                    
                                    <td>Downtown Center, Rm 106</td>
                                    
                                    <td>Special Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/documents/Agenda-Special-Meeting-022713.pdf" target="_blank">Agenda</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013 February 12</td>
                                    
                                    <td>Criminal Justice Institute<br> Auditorium
                                    </td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2013-02-12.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 December 18</td>
                                    
                                    <td>Osceola Campus, Building 1, Rm 219B</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-12-18.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 October 16</td>
                                    
                                    <td>Lake Nona Campus, Rm 148</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-10-16.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 September 18</td>
                                    
                                    <td>West Campus, Building 8, Rm 111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-09-18.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 September 18</td>
                                    
                                    <td>West Campus, Building 8, Rm 111</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-09-18-Organizational.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Meeting Schedule 2011-2012</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2012 July 17</td>
                                    
                                    <td>Osceola Campus, Building 1, Rm 219B</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td>This meeting was canceled</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 June 19</td>
                                    
                                    <td>East Campus, Building 5, Rm 112</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-06-19.php">Agenda and Materials </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 May 15</td>
                                    
                                    <td>Downtown Center Rm 106</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-05-15.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 April 17</td>
                                    
                                    <td>Winter Park Campus, Rm 225/226</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-04-17.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012 February 21</td>
                                    
                                    <td>West Campus, Building 8, Rm 111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2012-02-21.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 December 13</td>
                                    
                                    <td>Criminal Justice Institute<br> Auditorium
                                    </td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-12-13.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 October 18</td>
                                    
                                    <td>Lake Nona High School, Rm 408</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-10-18.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 September 20</td>
                                    
                                    <td>West Campus, Building 8, Rm 111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-09-20.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 July 19</td>
                                    
                                    <td>Osceola Campus, Building 1, Rm 219B</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/documents/July192011RegularMeeting.pdf" target="_blank">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 July 19</td>
                                    
                                    <td>Osceola Campus, Building 1, Rm 219B</td>
                                    
                                    <td>Organizational Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/documents/July192011OrganizationalMeeting.pdf" target="_blank">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Meeting Schedule 2010-2011</h4>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “MEETINGDATES”" href="#">MEETING DATES</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “LOCATION”" href="#">LOCATION</a></th>
                                    
                                    <th scope="col" width="70"><a title="Sort on “MEETINGTYPE”" href="#">MEETING TYPE</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FILES”" href="#">FILES</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2011 June 21</td>
                                    
                                    <td>East Campus, Building 5, Rm 112</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-06-21.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 May 17</td>
                                    
                                    <td>Downtown Center, Rm 106 A/B</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-05-17.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 April 19</td>
                                    
                                    <td>Downtown Center, Rm 106 A/B</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-04-19.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 March 15</td>
                                    
                                    <td>Downtown Center, Rm 106 A/B</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-03-15.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011 February 15</td>
                                    
                                    <td>West Campus, Building 8, Rm 111</td>
                                    
                                    <td>Regular Meeting</td>
                                    
                                    <td><a href="/about/about-valencia/DBOT-2011-02-15.php">Agenda and Materials</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/trusteeschedule.pcf">©</a>
      </div>
   </body>
</html>