<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>February 25, 2015 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2015-02-25.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>February 25, 2015 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong> February 25, 2015 Meeting </strong> 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Agenda-WestCampus-Feb252015_000.pdf"><strong>Agenda </strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/RegularMeetingMinutes-Dec172014_000.pdf">Approval of Minutes - December 17, 2014 Regular Meeting </a></li>
                           
                           <li><a href="/about/about-valencia/documents/ZORAforDBOT2-25-15.pdf"><strong>President's Report</strong></a></li>
                           
                           <li>Public Comment </li>
                           
                        </ul>
                        
                        <h4>New Business </h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/FoodServicesITNFeb2015_002.pdf">ITN 2015-05, Food Services, Dining &amp; Catering, College Wide, Approval of Short List</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PolicyAdoption-Feb252015.pdf">Policy Adoptions</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdditionsDeletionsModificationsofCoursesandPrograms-Feb252015_000.pdf">Additions, Deletions or Modifications of Courses and Programs</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/NewItems-ContinuingEducationCoursesandFees-February25_000.pdf">Continuing Education Courses and Fees</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda-Feb252015.pdf">Human Resources Agenda</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Feb252015_000.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeleteFeb252015_000.pdf">Property Deletions </a></li>
                           
                           <li>Board Comments </li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/DBOTPresentation_WCLearningSupportServices_022515_000.pdf">West Campus Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/DBOTPresentation_WestCampusSGA_022515_000.pdf">West Campus SGA Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SpringEnrollmentReport-Feb252015_000.pdf">Spring Enrollment Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportFeb2015_000.pdf">Financial Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportFebruary2015_000.pdf">Construction Report</a> 
                           </li>
                           
                           <li>Faculty Council Report </li>
                           
                           <li><a href="/about/about-valencia/documents/valenciafoundationtrusteereportfeb2015_000.pdf">Valencia Foundation Report </a></li>
                           	
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2015-02-25.pcf">©</a>
      </div>
   </body>
</html>