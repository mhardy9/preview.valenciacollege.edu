<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>April 22, 2015 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2015-04-22.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>April 22, 2015 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>April 22, 2015 Meeting</strong> 
                        </p>
                        	
                        <ul class="list-style-1">
                           		
                           <li><a href="/about/about-valencia/documents/Agenda-WinterParkCampus-Apr222015.pdf"><strong> Agenda</strong> </a></li>
                           
                           <li><a href="/about/about-valencia/documents/RegularMeetingMinutes-Feb252015.pdf">Approval of Minutes - February 25, 2015 Regular Meeting</a> 
                           </li>
                           
                           <li>President's Report </li>
                           
                           <li><strong><a href="/about/about-valencia/documents/2015sessionupdate42215.pdf">Legislative Update </a></strong></li>
                           
                           <li><strong><a href="/about/about-valencia/documents/FinancialAuditReport-Apr222015.pdf">Financial Audit Report</a> </strong></li>
                           
                           <li>Public Comment</li>
                           
                        </ul>
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/ArchitectDesignSvcsPoincianaApril2015.pdf">Request for Qualifications (RFQ) 2015-18, Architectural Design Services, Poinciana
                                 Campus </a></li>
                           
                           <li><a href="/about/about-valencia/documents/PolicyAdoption-1-07-FacultyAssociation-22Apr2015.pdf">Policy Adoption</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdditionsDeletionsModifications-Apr222015.pdf">Additions, Deletions or Modifications of Courses and Programs</a></li>
                           
                           <li><a href="/about/about-valencia/documents/NewItems-ContinuingEducationCoursesandFees-April222015.pdf">Continuing Education Courses and Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HRAgenda-Apr222015.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/EquityActReport-Apr222015.pdf">Equity Act Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Apr222015.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDelete04-22-15.pdf">Property Deletions</a></li>
                           
                           <li><a href="/about/about-valencia/documents/2015-2016AnnualDateNotification-Board.pdf">Board Comments</a></li>
                           
                        </ul>
                        
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/FinalBOTWPHonorsPresentationApril22-2015.pdf">Winter Park Campus Report</a></li>
                           
                           <li>Winter Park SGA Report</li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportApril2015.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportApril2015.pdf">Construction Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FacultyCouncilReport-Apr222015_000.pdf">Faculty Council Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/valenciafoundationtrusteereportapril2015.pdf">Valencia Foundation Report </a></li>
                           	
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2015-04-22.pcf">©</a>
      </div>
   </body>
</html>