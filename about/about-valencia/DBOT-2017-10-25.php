<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>DBOT Regular Meeting - October 25, 2017 | Valencia College</title>
      <meta name="Description" content="DBOT Regular Meeting - October 25, 2017">
      <meta name="Keywords" content="DBOT"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/DBOT-2017-10-25.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>DBOT Regular Meeting - October 25, 2017</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>DBOT Regular Meeting - October 25, 2017</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <h2>The District Board of Trustees</h2>
               
               <p class="head"><strong>October 25, 2017 - Regular Meeting- please click on the links below</strong></p>
               
               <p dir="auto"><strong><a class="icon_for_pdf" href="/about/about-valencia/documents/Agenda-Regular-Meeting-EAC-Film-Sound-Music-Tech-Room-133-Oct-25-2017.pdf" target="_blank">AGENDA</a></strong></p>
               
               <p dir="auto"><strong>APPROVAL OF MINUTES - <a class="icon_for_pdf" href="/about/about-valencia/documents/Org-and-Reg-Minutes-Sep-27-2017.pdf" target="_blank">September 27, 2017 Organizational &amp; Regular Meetings</a></strong></p>
               
               <p dir="auto"><strong>PRESIDENT’S REPORT</strong></p>
               
               <p dir="auto"><strong>PUBLIC COMMENT</strong></p>
               
               <p dir="auto"><strong>NEW BUSINESS</strong></p>
               
               <p dir="auto"><a href="/about/about-valencia/documents/Serving-Hurricane-Arrivals-from-Puerto-Rico-Oct-25-2017.pdf"><strong>Valencia’s Coordinated Response Efforts in Serving Hurricane Arrivals from Puerto
                        Rico and the U.S. Virgin Islands</strong></a></p>
               
               <p dir="auto"><strong><a class="icon_for_pdf" href="/about/about-valencia/documents/Board-Transmittal-DACA-Letter-Oct-25-2017.pdf" target="_blank">Request for Congressional Action to Support Students Benefitting from the Deferred
                        Action for Childhood Arrivals Policy</a></strong></p>
               
               <p dir="auto"><strong> <a class="icon_for_pdf" href="/about/about-valencia/documents/Construction-Mgr-at-Risk-CAT-Osceola-Oct-25-2017.pdf" target="_blank">Approval of Contractors - Center for Accelerated Training - Osceola Campus</a></strong></p>
               
               <p dir="auto"><strong><a class="icon_for_pdf" href="/about/about-valencia/documents/Contract-for-Design-Build-CIT-Osceola-Oct-25-2017.pdf" target="_blank">Approval of Contractors - Careers in Technology Building - Osceola Campus</a></strong></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/Valencia-Foundation-Annual-Audit-Review-Cert-of-Valencia-Foundation-Oct-25-2017.pdf" target="_blank"><strong>Valencia Foundation Annual Audit Review/Certification of Valencia Foundation</strong></a></p>
               
               <p dir="auto"><strong>Policy Adoption&nbsp;</strong></p>
               
               <ul>
                  
                  <li><a class="icon_for_pdf" href="/about/about-valencia/documents/Policy-Adoption-Admissions-Oct-25-2017.pdf" target="_blank"><strong>Policy 6Hx28: 8-02 - Admissions</strong></a></li>
                  
                  <li><strong dir="auto"> <a class="icon_for_pdf" href="/about/about-valencia/documents/Policy-Adoption-Non-Motorized-Vehicles-on-Campus-Oct-25-2017.pdf" target="_blank">Policy 6Hx28: 10-7.2 - Non-Motorized Vehicles on Campus</a></strong></li>
                  
               </ul>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/Additions-Deletions-Modifications-of-Courses-and-Programs-Oct-25-2017.pdf" target="_blank"><strong> Additions, Deletions or Modifications of Courses &amp; Programs</strong></a></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/HR-Agenda-Oct-25-2017.pdf" target="_blank"><strong>Human Resources Agenda</strong></a></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/Submission-of-Grant-Proposals-Oct-25-2017.pdf" target="_blank"><strong>Submission of Grant Proposals</strong></a></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/Property-Deletions-Oct-25-2017.pdf" target="_blank"><strong>Property Deletions</strong></a></p>
               
               <p dir="auto"><strong>BOARD COMMENTS</strong></p>
               
               <p dir="auto"><strong>REPORTS</strong></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/East-Campus-Report-Oct-25-2017.pdf" target="_blank"><strong>East Campus Report</strong></a></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/East-Campus-SGA-Report-Oct-25-2017.pdf" target="_blank"><strong>East Campus SGA Report</strong></a></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/Financial-Report-Oct-25-2017.pdf" target="_blank"><strong>Financial Report</strong></a></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/Construction-Report-Oct-25-2017.pdf" target="_blank"><strong>Construction Report</strong></a></p>
               
               <p dir="auto"><strong>Faculty Council Report</strong></p>
               
               <p dir="auto"><a class="icon_for_pdf" href="/about/about-valencia/documents/Foundation-Report-Oct-25-2017.pdf" target="_blank"><strong>Valencia Foundation Report</strong></a></p>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/DBOT-2017-10-25.pcf">©</a>
      </div>
   </body>
</html>