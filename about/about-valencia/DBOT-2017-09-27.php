<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>September 27, 2017 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/DBOT-2017-09-27.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>September 27, 2017 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        	 
                        <h2>The District Board of Trustees</h2>
                        				
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>September 27, 2017 - Regular Meeting</strong></p>
                        				
                        <ul class="list-style-1">
                           				
                           <li><a href="/about/about-valencia/documents/Agenda-Regular-Meeting-West-Campus-Sep-27-2017.pdf"><strong>Agenda</strong></a></li>
                           				
                           <li>APPROVAL OF MINUTES - <a href="/about/about-valencia/documents/Regular-Meeting-Minutes-Jun-28-2017.pdf">Regular-Meeting-Minutes-Jun-28-2017</a></li>
                           				
                           <li> President's Report</li>
                           				
                           <li> Public Comment</li>
                           				
                        </ul>
                        					
                        <h4>New Business </h4>
                        				
                        <ul class="list-style-1">
                           				
                           <li>Consideration of Exceptions and Recommended Order, Luis Salas</li>
                           				
                           <li><a href="/about/about-valencia/documents/Downtown-Project-Agreement-Sep-27-2017.pdf">Project Agreement - Ustler Development, Inc., and Development Ventures Group, Inc.</a></li>
                           				
                           <li><a href="/about/about-valencia/documents/Downtown-Lease-Agreement-Sep-27-2017.pdf">Approval of Downtown Lease Agreement - Ustler Development, Inc., and Development Ventures
                                 Group, Inc.</a></li>
                           				
                           <li><a href="/about/about-valencia/documents/Board-Transmittal-Letter-Downtown-Build-Out-Full-Funding-Sep-27-2017.pdf">Downtown Campus - Funding of Tenant Improvements</a></li>
                           				
                           <li><a href="/about/about-valencia/documents/Approval-of-Annual-Audit-Plan-FY-2017-2018-Sep-27-2017.pdf">Approval of Annual Audit Plan - FY 2017-2018</a></li>
                           				
                        </ul>
                        				
                        <h4>Policy Adoptions</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Policy-Adoption-3B-03-Recruitment-Selection-Hiring-Sep-27-2017.pdf">Policy 6Hx28: 3B-03 - Recruitment, Selection, and Hiring of Employees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Policy-Adoption-3D-05-Sick-Leave-Pool-Sep-27-2017.pdf">Policy 6Hx28: 3D-05 - Sick Leave Pool</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Policy-Adoption-3D-06.1-FMLA-Sep-27-2017.pdf">Policy 6Hx28: 3D-06.1 - Family/Medical Leave</a></li>
                           
                        </ul>
                        
                        <h4>Policy Repeal</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Policy-Repeal-3D-08-Partial-Leave-Sep-27-2017.pdf">Policy 6Hx28: 3D-08 - Partial Leave</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Human-Resources-Agenda-Sep-27-2017.pdf">Human Resources Agenda</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Continuing-Education-Courses-and-Fees-Sep-27-2017.pdf"> Continuing Education Courses &amp; Fees</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Submission-of-Grant-Proposals-Jun-Aug-2017-Sep-27-2017.pdf"> Submission of Grant Proposals</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Property-Deletions-Sep-27-2017.pdf"> Property Deletions</a></li>
                           
                           
                           <li> Board Comments</li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        				
                        				
                        <ul class="list-style-1">
                           				
                           <li><a href="/about/about-valencia/documents/West-Campus-Report-Sep-27-2017.pdf">West Campus Report</a></li>
                           				
                           <li> <a href="/about/about-valencia/documents/West-Campus-SGA-Report-Sep-27-2017.pdf">West Campus SGA Report</a></li>
                           				
                           <li> <a href="/about/about-valencia/documents/DBOT-Fall-2017-Enrollment-Report-Sep-27-2017.pdf">Fall Enrollment Report</a></li>
                           				
                           <li> <a href="/about/about-valencia/documents/Financial-Report-Sep-27-2017.pdf">Financial Report</a></li>
                           				
                           <li><a href="/about/about-valencia/documents/Construction-Report-Sep-27-2017.pdf">Construction Report</a></li>
                           				
                           <li>Faculty Council Report</li>
                           	
                           <li><a href="/about/about-valencia/documents/Valencia-Foundation-Report-Sep-27-2017.pdf">Valencia Foundation Report</a></li>
                           
                           	
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/DBOT-2017-09-27.pcf">©</a>
      </div>
   </body>
</html>