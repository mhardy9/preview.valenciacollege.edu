<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Administration  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/administration.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>Administration </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Administration</h2>
                        
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th width="200" scope="col">
                                       <a href="#" title="Sort on “NAME”">NAME</a> 
                                    </th>
                                    
                                    <th width="200" scope="col"><a href="#" title="Sort on “TITLE”">TITLE</a></th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Executive</strong></td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Sanford C Shugart
                                    </td>
                                    
                                    <td> President</td>
                                    
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Stacey R Johnson
                                    </td>
                                    
                                    <td> Campus President, EAC/WPK</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Kathleen A Plinske
                                    </td>
                                    
                                    <td> Campus President, OSC/LNC/PNC</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Falecia Douglas Williams
                                    </td>
                                    
                                    <td> Campus President, WEC/DTC</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Eugene Garrison Jones
                                    </td>
                                    
                                    <td> Executive Dean, DTC</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Michael Bosley
                                    </td>
                                    
                                    <td> Executive Dean, LNC</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Jennifer Robertson
                                    </td>
                                    
                                    <td> Executive Dean, PNC</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Jeffrey Wayne Goltz
                                    </td>
                                    
                                    <td> Executive Dean, SPS</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Terri Graham Daniels
                                    </td>
                                    
                                    <td> Executive Dean, WPC</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Geraldine Mary Patricia Gallagher
                                    </td>
                                    
                                    <td> Foundation President and CEO</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Susan Elizabeth Ledlow
                                    </td>
                                    
                                    <td> VP, Academic Affairs &amp; Plan'g</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Loren Jay Bender
                                    </td>
                                    
                                    <td> VP, Business Ops &amp; Finance</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Joyce C Romano
                                    </td>
                                    
                                    <td> VP, Educational Partnerships</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Joseph Nicholas Battista
                                    </td>
                                    
                                    <td> VP, Global, Prof &amp; Cont Ed</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       William J Slot
                                    </td>
                                    
                                    <td> VP, Info Technology and CIO</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Amy Nicole Bosley
                                    </td>
                                    
                                    <td> VP, Org Dev &amp; Human Resources</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       William J Mullowney
                                    </td>
                                    
                                    <td> VP, Policy/General Counsel</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>James Robert Galbraith</td>
                                    
                                    <td> VP, Public Affairs &amp; Marketing</td>
                                    
                                 </tr>
                                 
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Kimberly Wyatt Sepich
                                    </td>
                                    
                                    <td> VP, Student Affairs</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td><strong>Administrators</strong></td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       Leslie Bissinger Golden
                                    </td>
                                    
                                    <td> Associate General Counsel</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Linda K Herlocker</td>
                                    
                                    <td> Asst VP, Admissions &amp; Records</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Jamie David Rost</td>
                                    
                                    <td>Asst VP, Application</td>
                                    
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Oscar Javier Cristancho Mercado</td>
                                    
                                    <td> Asst VP, Budgets &amp; Analysis</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Nasser Hedayat</td>
                                    
                                    <td> Asst VP, Career &amp; Workforce Ed</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Amy E Parker Kleeman</td>
                                    
                                    <td> Asst VP, College Transition</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Karen Marie Borglum</td>
                                    
                                    <td> Asst VP, Curric &amp; Assessment</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Jose Antonio Fernandez</td>
                                    
                                    <td> Asst VP, Fac Plan/Real Est Dev</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Daniel T Barkowitz</td>
                                    
                                    <td> Asst VP, Fin Aid &amp; Vet Affairs</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Jacqueline Demaris Lasch</td>
                                    
                                    <td> Asst VP, Financial Services</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Lisa Giddings Eli</td>
                                    
                                    <td> Asst VP, Global and Cont Ed</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Traci Ash Bjella</td>
                                    
                                    <td> Asst VP, Marketing</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Paul Rooney</td>
                                    
                                    <td> Asst VP, Operations</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Ryan D Kane
                                    </td>
                                    
                                    <td> Asst VP, Org Dev &amp; Incl</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Carolyn Renee McMorran</td>
                                    
                                    <td> Asst VP, Prof &amp; Cont Ed</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Kristeen R Christian</td>
                                    
                                    <td> Asst VP, Resource Development</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Sonya Franklin Joseph</td>
                                    
                                    <td> Asst VP, Student Affairs</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Mary Beth Clifton</td>
                                    
                                    <td> Asst VP, Talent Acq/Total Rew</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Wendi M Dew</td>
                                    
                                    <td> Asst VP, Teaching &amp; Learning</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Michelle Renee Foster</td>
                                    
                                    <td> Campus Dean, Academic Aff EAC</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Nicholas J Bekas</td>
                                    
                                    <td> Campus Dean, Academic Aff WEC</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Leonard Christopher Bass</td>
                                    
                                    <td> Campus Dean, Learning Supp EAC</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Landon Paul Shephard</td>
                                    
                                    <td> Campus Dean, Learning Supp OSC</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Karen Lynn Reilly</td>
                                    
                                    <td> Campus Dean, Learning Supp WEC</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Angela Jean Mendolaro</td>
                                    
                                    <td> Chief Philanthropy Officer</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Joseph Michael Sarrubbo</td>
                                    
                                    <td> Dean of Students - East/WP</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Jillian M Szentmiklosi</td>
                                    
                                    <td> Dean of Students - Osceola</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Benjamin C Lion</td>
                                    
                                    <td> Dean of Students - West</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Penny Lou Conners</td>
                                    
                                    <td> Dean, Allied Health</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Ana J Caldero Figueroa</td>
                                    
                                    <td> Dean, Arts and Humanities W</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Wendy Lynn Givoglu</td>
                                    
                                    <td> Dean, Arts/Entertainment, E</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Molly McIntire</td>
                                    
                                    <td> Dean, Behav/Social Science, W</td>
                                    
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Carin Maurine Gordon</td>
                                    
                                    <td> Dean, Bus, Info Tech/Pub Svc,E</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Terry L Allcorn</td>
                                    
                                    <td> Dean, Business &amp; Hospitality</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>James R McDonald</td>
                                    
                                    <td> Dean, Career &amp; Tech Programs</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Jenni Lynn Campbell</td>
                                    
                                    <td> Dean, Commun/Languages, O</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Linda R Neal</td>
                                    
                                    <td> Dean, Communications, E</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Elizabeth Suzanne Renn</td>
                                    
                                    <td> Dean, Communications, W</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>
                                       Lisa R Macon
                                    </td>
                                    
                                    <td> Dean, Engin, Comp Prgrm &amp; Tech</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>David O Sutton</td>
                                    
                                    <td> Dean, Humanities/Fore Lang,E</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Thomas T Takayama</td>
                                    
                                    <td> Dean, Humanities/Soc Science</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Melissa Dewey Pedone</td>
                                    
                                    <td> Dean, Math, O</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Maryke Lee</td>
                                    
                                    <td> Dean, Mathematics, E</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Paul David Blankenship</td>
                                    
                                    <td> Dean, Mathematics, W</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Rise W Sandrowitz</td>
                                    
                                    <td> Dean, Nursing</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Anitza M San Miguel</td>
                                    
                                    <td> Dean, Science OSC</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Jennifer Lynn Snyder</td>
                                    
                                    <td> Dean, Science, E</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Robert Frank Gessner</td>
                                    
                                    <td> Dean, Science, W</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Michelle Denise Matis</td>
                                    
                                    <td> Foundation VP &amp; COO</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Nancy Lea Maurer</td>
                                    
                                    <td> Interim Dean, Social Science E</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Cheryl Lynn Clark Robinson</td>
                                    
                                    <td> Dir, Honors Program</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>Michelle Terese Sever</td>
                                    
                                    <td> Dir, HR Policy &amp; Compliance</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  
                  
                  
                  <main role="main">
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                  </main>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/administration.pcf">©</a>
      </div>
   </body>
</html>