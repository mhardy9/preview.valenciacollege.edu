<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>October 21, 2015 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2015-10-21.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>October 21, 2015 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>October 21, 2015 - Regular Meeting</strong></p>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Agenda-RegularMeeting-LakeNonaCampus-Oct212015_000.pdf"><strong>Agenda</strong> </a></li>
                           
                           <li><a href="/about/about-valencia/documents/ApprovalofMinutes-Sep232015.pdf">Approval of Minutes - September 23, 2015 Organizational &amp; Regular Meetings </a></li>
                           
                           <li>President's Report </li>
                           
                           <li><strong>Presentation on Membership in the League for Innovation in the Community College -
                                 Dr. Gerardo de los Santos, President &amp; CEO </strong></li>
                           
                           <li><strong><a href="/about/about-valencia/documents/ValenciaUCFVCDBOTPresentation_2015_Final_000.pdf">Update on UCF/Valencia Downtown Campus Planning </a></strong></li>
                           
                           <li>Public Comment</li>
                           
                        </ul>
                        
                        <h4>New Business</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/GMPEastCampusBuilding09and10-Oct212015.pdf">Guaranteed Maximum Price (GMP) - Buildings 09 and 10, East Campus Film, Sound, and
                                 Music Technology and Plant Operations </a></li>
                           
                           <li><a href="/about/about-valencia/documents/CMARRankingsPoincianaCampusBuilding1-Oct212015.pdf">Contract for Construction Manager at Risk - Building 01 and Central Energy Plant,
                                 Poinciana Campus (RFQu # 2016-16)</a></li>
                           
                        </ul>
                        
                        <h4>Policy Adoptions </h4>
                        
                        <ul class="list-style-1">
                           
                           <li><strong><a href="/about/about-valencia/documents/PolicyAdoption-6Hx283C-11-RetirementContributions-Oct212015_000.pdf">Policy 6Hx28: 3C-11 - Retirement Contributions</a></strong></li>
                           
                        </ul>
                        
                        
                        <ul class="list-style-1">
                           	
                           <li>Policy Repeals </li>
                           	
                           <li><strong><a href="/about/about-valencia/documents/PolicyRepeal-6Hx283C-13-OptionalRetirementPrograms-Oct212015.pdf">Policy 6Hx28: 3C-13 - Optional Retirement Programs</a> </strong></li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationAnnualAuditReview-Oct212015.pdf">Valencia Foundation Annual Audit Review</a></li>
                           
                           <li><a href="/about/about-valencia/documents/TEASFeeIncreaseOct2015.pdf">Test of Essential Academic Skills (TEAS) Fee Increase</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdditionsDeletionsorModificationsofCoursesorPrograms-Oct212015.pdf">Additions, Deletions or Modifications of Courses &amp; Programs</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda-Oct212015.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Oct212015.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDelete10-21-15.pdf">Property Deletions</a></li>
                           
                           <li>Board Comments</li>
                           
                        </ul>
                        
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li>Lake Nona Campus Report </li>
                           
                           <li><a href="/about/about-valencia/documents/SGApresentation-Oct212015.pdf">Lake Nona SGA Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/BOTPresentationBOTFinal-Oct212015.pdf">Financial Indicators Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportOctober2015.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportOctober2015.pdf">Construction Report</a></li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/about-valencia/documents/valenciafoundationtrusteereport-Oct212015.pdf">Valencia Foundation Report </a></li>
                           
                        </ul>
                        	  
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2015-10-21.pcf">©</a>
      </div>
   </body>
</html>