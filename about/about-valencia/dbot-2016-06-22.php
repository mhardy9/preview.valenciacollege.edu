<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>June 22, 2016 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2016-06-22.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>June 22, 2016 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>June 22, 2016 - Regular Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/Agenda-RegularMeeting-EastCampus-Jun222016.pdf"><strong>Agenda</strong></a> 
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/RegularMeetingMinutes-May252016.pdf">APPROVAL OF MINUTES - May 25, 2016 Regular Meeting </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/StrategicPlanningReport-Jun222016.pdf">President's Report</a></li>
                           
                           <li>Public Comment </li>
                           		   
                        </ul>
                        
                        <h4>New Business </h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/OperatingBudget2016-2017-Jun222016.pdf">Operating Budget 2016-2017</a></li>
                           
                           
                           <li>President's Performance Evaluation</li>
                           
                           
                           <li>President's Employment Contract</li>
                           
                           
                           <li><a href="/about/general-counsel/policy/historical/AwardofBidforITB2016-70EastCampusChillerInstall-Jun222016.pdf">Award of ITB # 2016-70 for Installation Services Chiller Expansion East Campus</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/DowntownCreativeVillageMOU-Jun222016.pdf">Downtown Creative Village Campus - Memorandum of Understanding</a> 
                           </li>
                           	
                        </ul>
                        		
                        <h4>Policy Adoption </h4>
                        		
                        <ul class="list-style-1">
                           
                           <li><em><strong><a href="/about/general-counsel/policy/historical/PolicyAdoption-6Hx284-16-AwardofDegreesandCertificates-Jun222016.pdf">6Hx28: 4-16 - Award of Degrees and Certificates </a></strong></em></li>
                           
                        </ul>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/DelinquentAccountWriteoffJune2016.pdf">Delinquent Account Write-Off</a></li>
                           
                           
                           <li><a href="/about/general-counsel/policy/historical/HRAgenda-Jun222016.pdf">Human Resources Agenda</a></li>
                           
                           
                           <li><a href="/about/general-counsel/policy/historical/AdditionsDeletionsModifications-Jun222016.pdf">Additions, Deletions of Modifications of Courses and Programs</a></li>
                           
                           
                           <li><a href="/about/general-counsel/policy/historical/NewBusiness-ContinuingEducationCoursesandFees-June222016.pdf">Continuing Education Courses &amp; Fees</a></li>
                           
                           
                           <li><a href="/about/general-counsel/policy/historical/SubmissionofGrantProposals-Jun222016.pdf">Submission of Grant Proposals</a></li>
                           
                           
                           <li><a href="/about/general-counsel/policy/historical/PropertyDelete-Jun222016.pdf">Property Deletions</a></li>
                           
                           <li>Board Comments</li>
                           	
                        </ul>
                        		
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/EastCampusReport-Jun222016.pdf">East Campus Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/EastCampusSGAReport-Jun222016.pdf">East Campus SGA Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/FacultyCouncilReport-Jun222016.pdf">Faculty Council Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/FinancialReportJune2016.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/ConstructionReportJune2016.pdf">Construction Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/FoundationReport-Jun222016.pdf">Valencia Foundation Report </a></li>
                           	
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2016-06-22.pcf">©</a>
      </div>
   </body>
</html>