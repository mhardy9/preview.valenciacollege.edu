<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>DBOT Regular Meeting - October 25, 2017 | Valencia College</title>
      <meta name="Description" content="DBOT Regular Meeting - October 25, 2017"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2017-12-06.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>DBOT Regular Meeting - December 6, 2017</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>DBOT Regular Meeting - October 25, 2017</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <h2>The District Board of Trustees</h2>
               
               <p class="head"><strong>December 06, 2017 - Regular Meeting- please click on the links below</strong></p>
               
               <p><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Agenda-Regular-Meeting-Poinciana-Campus-Dec-6-2017.pdf" target="_blank"><strong>AGENDA</strong></a></p>
               
               <p><strong>APPROVAL OF MINUTES - <a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Regular-Meeting-Minutes-Oct-25-2017.pdf" target="_blank">October 25, 2017 Regular Meeting</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/50th-Anniversary-Presentation-Dec-6-2017.pdf" target="_blank">PRESIDENT'S REPORT</a></strong></p>
               
               <p><iframe src="https://www.youtube.com/embed/ByzQeYrU2iw?rel=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
               
               <p><strong>FACULTY COUNCIL REPORT</strong></p>
               
               <p><strong>PUBLIC COMMENTS</strong></p>
               
               <p><strong>NEW BUSINESS</strong></p>
               
               <p><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Briefing-on-the-CFEED-Dec-6-2017.pdf" target="_blank"><strong>Briefing on the Central Florida Education Ecosystem Database (CFEED)</strong></a></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Transmittal-Letter-Award-of-ITN-2017-32-Dec-6-2017.pdf" target="_blank">Award of ITN#2017-32 for the Central Florida Education Ecosystem Database Partnership
                        - Technical Partner Contract</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Approval-of-the-AS-Degree-in-Energy-Mgmt-and-Controls-Tech-Dec-6-2017.pdf" target="_blank">Approval of the Associate in Science Degree in Energy Management and Controls Technology</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/HR-Agenda-Dec-6-2017.pdf" target="_blank">Human Resources Agenda</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Additions-Deletions-Modifications-of-Courses-and-Programs-Dec-6-2017.pdf" target="_blank">Additions, Deletions or Modifications of Courses &amp; Programs</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Continuing-Education-Courses-and-Fees-Dec-6-2017.pdf" target="_blank">Continuing Education Courses &amp; Fees</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Submission-of-Grant-Proposals-Dec-6-2017.pdf" target="_blank">Submission of Grant Proposals</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Property-Deletions-Dec-6-2017.pdf" target="_blank">Property Deletions</a></strong></p>
               
               <p><strong>REPORTS</strong></p>
               
               <p><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Poinciana-Campus-Report-Dec-6-2017.pdf" target="_blank"><strong>Poinciana Campus Report</strong></a></p>
               
               <p><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Poinciana-Campus-SGA-Report-Dec-6-2017.pdf" target="_blank"><strong> Poinciana Campus SGA Report</strong></a></p>
               
               <p><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Recycle-Efforts-Dec-6-2017.pdf" target="_blank"><strong>College Recycling Efforts Report</strong></a></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Financial-Report-Dec-6-2017.pdf" target="_blank">Financial Report</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Construction-Report-Dec-6-2017.pdf" target="_blank">Construction Report</a></strong></p>
               
               <p><strong><a class="icon_for_pdf" href="http://valenciacollege.edu/AboutUs/documents/Valencia-Foundation-Report-Dec-6-2017.pdf" target="_blank">Valencia Foundation Report</a></strong></p>
               
               <p><strong>BOARD COMMENTS</strong></p>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2017-12-06.pcf">©</a>
      </div>
   </body>
</html>