<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>December 7, 2016 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2016-01-27.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>December 7, 2016 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>The District Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>December 7, 2016 Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           	
                           <li><a href="/about/general-counsel/policy/historical/Agenda-RegularMeeting-OSC-Dec72016.pdf"><strong>Agenda</strong> </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/RegularMeetingMinutes-Nov22016.pdf">APPROVAL OF MINUTES - November 2, 2016 Regular Meeting </a></li>
                           
                           <li>President's Report</li>
                           
                           <li>Public Comment </li>
                           			
                        </ul>
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/Five-YearImpactPlan-Dec72016.pdf">The Power to Serve; Valencia's Five-Year Impact Plan - DBOT Goals Added</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/RFQu2017-11-AudiovisualSystemsEACFilmSoundMusicTechnology-Dec72016.pdf">Award of RFQu #2017-11, Audiovisual Systems for East Campus Film, Sound and Music
                                 Technology Building</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/DBOTHVACMaintenanceandRepairs-Dec72016.pdf">Award of RFP #2017-06 for College Wide HVAC Maintenance and Repair Services </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/LOI-BSinNursing-Dec72016.pdf">Letter of Intent - Bachelor of Science in Nursing </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/LOI-BASinSupervisionandManagement-Dec72016.pdf">Letter of Intent - Bachelor of Applied Science in Supervision and Management</a> 
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/AdditionsDeletionsModificationsCoursesandPrograms-Dec72016.pdf">Additions, Deletions or Modifications of Courses &amp; Programs</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/NewBusiness-ContinuingEducationCoursesandFees-December72016.pdf">Continuing Education Courses &amp; Fees </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/HumanResourcesAgenda-Dec72016.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/SubmissionofGrantProposals-Dec72016.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/PropertyDelete-Dec72016.pdf">Property Deletions</a></li>
                           
                           <li>Board Comments</li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        		
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/LSAMPPresentation-Dec72016.pdf">NSF LSAMP Bridges to Baccalaureate Program Update </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/OsceolaCampusReport-Dec72016.pdf">Osceola Campus Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/OsceolaSGAReport-Dec72016.pdf">Osceola SGA Report</a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/FinancialReport-Dec72016.pdf">Financial Report </a></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/ConstructionReport-Dec72016.pdf">Construction Report </a></li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/general-counsel/policy/historical/ValenciaFoundationReport-Dec72016.pdf">Valencia Foundation Report </a></li>
                           		
                        </ul>
                        	  
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2016-01-27.pcf">©</a>
      </div>
   </body>
</html>