<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>December 17, 2014 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2014-12-17.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>December 17, 2014 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>December 17, 2014 Meeting</strong></p>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Agenda-OsceolaCampus-Dec172014.pdf"><strong>Agenda </strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/RegularMeetingMinutes-Oct222014_000.pdf">Approval of Minutes - October 22, 2014 Regular Meeting </a></li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentReport-Dec172014.pdf"><strong>President's Report</strong></a></li>
                           
                           <li>Public Comment </li>
                           	
                        </ul>
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/CommissionofValenciaCollegeSchoolofPublicSafety-Dec172014_001.pdf">Commission of Valencia College School of Public Safety</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ReallocateFundBalanceforInternationalVIllage-Dec172014_000.pdf">Request to Designate $300,000 from Fund Balance for International Village at Osceola
                                 Campus</a></li>
                           
                           <li><a href="/about/about-valencia/documents/EastCampusOakTreeDedicationResolution-Dec172014.pdf">East Campus Oak Tree Dedication Resolution</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationAnnualAuditReview-Dec172014.pdf">Valencia Foundation Annual Audit Review</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdditionsDeletionsModificationsofCoursesandFees-Dec172014.pdf">Additions, Deletions or Modifications of Courses &amp; Programs</a></li>
                           
                           <li><a href="/about/about-valencia/documents/NewItems-ContinuingEducationCoursesandFees-December172014.pdf">Continuing Education Courses &amp; Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda-Dec172014_000.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Dec172014.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDelete-Dec172014.pdf">Property Deletions</a> 
                           </li>
                           
                           <li>Board Comments </li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/REACH-Dec172014.pdf">Osceola Campus Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/BoardofTrusteesSGAPresentation-Dec172014.pdf">Osceola SGA Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/StateAuditResponse-Dec172014.pdf">State Audit Response</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AuditPlanStatus-December2014.pdf">Internal Audit Report</a></li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportDec2014.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportDecember2014.pdf">Construction Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/valenciafoundationtrusteereportdec2014.pdf">Valencia Foundation Report</a> 
                           </li>
                           	
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2014-12-17.pcf">©</a>
      </div>
   </body>
</html>