<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>February 6, 2017 - Special Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2017-02-06.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>February 6, 2017 - Special Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>February 6, 2017 - Special Meeting</strong></p>
                        			
                        			
                        <ul class="list-style-1">
                           			
                           <li><a href="/about/general-counsel/policy/historical/Agenda-SpecialMeeting-DistrictOffice-262017.pdf"><strong>Agenda</strong></a></li>
                           			
                           <li>Public Comment </li>	
                           
                        </ul>
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           			
                           <li><a href="/about/general-counsel/policy/historical/BOT-Agenda-ITN-Appeal-262017.pdf">Appeal of Decision, BENCOR Protest of Recommended Award, ITN 2016-80, Special Pay
                                 Plan,
                                 				457 Voluntary Plan, and FICA Alternative Plan Services</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/BOT-Agenda-ITN_2016-80-Pay-Plan-982016-662017.pdf">Award of ITN 2016-80, Special Pay Plan, 457 Voluntary Plan, and FICA Alternative Plan
                                 				Services (Note: This item to be considered only in the event that BENCOR's Appeal
                                 is denied)</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/PetitionRequestandResolution-InterimRelief-LuisSalas.pdf">Petition and Request for Interim Relief, Luis Salas</a></li>
                           
                           			
                           <li>Other Business</li>
                           			
                           <li>Board Comments</li>
                           			
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2017-02-06.pcf">©</a>
      </div>
   </body>
</html>