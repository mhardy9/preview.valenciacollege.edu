<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>September 24, 2014 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2014-09-24.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>September 24, 2014 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        	
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>September 24, 2014 Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           			   
                           <li><a href="/about/about-valencia/documents/Agenda-RegularMeeting-CJI-Sep242014_000.pdf"><strong>Agenda </strong></a></li>
                           			   
                           <li><strong><a href="/about/about-valencia/documents/OrgandRegMeetingMinutes-Jul232014.pdf">Approval of Minutes - July 23, 2014 Organizational &amp; Regular Meetings</a> </strong></li>
                           			   
                           <li><strong>President's Report</strong></li>
                           				 
                           <li><strong><a href="/about/about-valencia/documents/PresidentReport-NewsRelease-Sep242014.pdf">News Release</a> </strong></li>
                           				 
                           <li><strong><a href="/about/about-valencia/documents/NewStudentExperiencePresentation-BoardofTrusteesSept2014.pdf">New Student Experience Report</a> </strong></li>
                           			
                           <li><strong><a href="/about/about-valencia/documents/PresidentReport-2013-2014CareerandWorkforceEducationProgramSuccesses-Sep242014.pdf">2013-2014 Career and Workforce Education Program Successes</a> </strong></li>
                           			
                           <li>Public Comment </li>
                           
                        </ul>
                        
                        <h4>New Business</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/2014-2015ProposedAuditPlan.pdf">Internal Audit Plan FY15</a></li>
                           
                           <li>Internal Auditor's Performance Evaluation</li>
                           
                           <li><a href="/about/about-valencia/documents/TransferFundstoPlanDesignPoincianaCampus.pdf">Request to Make a Non-Mandatory Transfer to Fund 7 to Plan and Design the Poinciana
                                 Campus </a></li>
                           
                           <li><a href="/about/about-valencia/documents/RequesttoIncreaseFMSPMOBuildingBudgetSeptember2014.pdf">Request to Transfer $2,000,000 from Fund 3 to Fund 7 to Increase the East Campus Film,
                                 Sound, Music Technology/Plant Operations Building Budget </a></li>
                           
                           <li><a href="/about/about-valencia/documents/CJICampusRedesignationSeptember2014.pdf">Authorization to Designate the Criminal Justice Institute as a Special Purpose Center
                                 </a></li>
                           
                           <li><a href="/about/about-valencia/documents/CJIK-9Lease-Sep242014.pdf">Criminal Justice Institute/Orange County K-9 Program Future Lease Authorization</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/CWMechanicalServices1415-24September2014.pdf">Award of Bid #14/15-02 for College-Wide Mechanical Services</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/LifeSafetyAssetRFP14-24-Sep242014.pdf">Award of Request for Proposal (RFP) #13/14-24 - Life Safety &amp; Asset Protection Systems
                                 </a></li>
                           
                           <li><a href="/about/about-valencia/documents/PolicyRepeals-Sep242014.pdf">Policy Repeals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/OperatingBudgetExhibitECorrection.pdf">Approval of Revised Exhibit E of the 2014-2015 Operating Budget</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/NewItems-ContinuingEducationCoursesandFees-September24.pdf">Continuing Education Courses &amp; Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda-Sep242014.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Sep242014.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDelete09-24-14.pdf">Property Deletions </a></li>
                           
                           <li>Board Comments </li>
                           
                        </ul>
                        
                        <h4>Reports</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/PublicSafetyProgrammingattheCriminalJusticeInstitute.pdf">CJI Student Experience Report </a></li>
                           
                           <li><a href="/about/about-valencia/documents/DBOT_Fall_2014_Enrollment_ReportFinalWO_000.pdf">Fall Enrollment Report </a></li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReport-Sep242014.pdf">Financial Report</a> 
                           </li>
                           
                           <li><strong><a href="/about/about-valencia/documents/FinancialIndicatorstoBoTSept2014.pdf">Financial Indicators Report </a></strong></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportSeptember2014.pdf">Construction Report </a></li>
                           
                           <li>Faculty Council Report </li>
                           
                           <li><a href="/about/about-valencia/documents/valenciafoundationtrusteereportsept2014.pdf">Valencia Foundation Report </a></li>
                           
                        </ul>
                        
                     </div>	
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2014-09-24.pcf">©</a>
      </div>
   </body>
</html>