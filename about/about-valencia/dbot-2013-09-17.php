<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>September 17, 2013 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2013-09-17.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>September 17, 2013 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        	
                        <h3> Agenda and Board Materials</h3>
                        	
                        <ul class="list-style-1">
                           
                           <p><strong>September 17, 2013 Meeting</strong></p>
                           
                           <li><a href="/about/about-valencia/documents/Agenda-EastCampus-9-17-2013.pdf"><strong>Agenda</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/OrganizationalandRegularMinutes-7-16-2013.pdf">Approval of Organizational &amp; Regular Minutes - Jul 16, 2013</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentReport_000.pdf"><strong>President's Report </strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentShugart-Goalsfor2013-2014.pdf"><strong>President's Goals for 2013-2014</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/NewBusiness-Items1-2-3.pdf"><strong>New Business - Items 1-2-3</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/2014sessionprioritiesc.pdf">Item 2 - Legislative Priorities Documentation</a> 
                           </li>
                           
                           <li><span><a href="/about/about-valencia/documents/AnnualPrequalificationofContractorsandConstructionManagers-10-31-2013through9-30-2014.pdf">Annual Prequalification of General Contractors &amp; Construction Managers</a> </span></li>
                           
                           <li><a href="/about/about-valencia/documents/PolicyAdoption.pdf">Policy Adoption</a></li>
                           
                           <li><a href="/about/about-valencia/documents/IMPolicyPPBOT2.pdf">Policy Adoption - Presentation</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Additions-Deletions-ModificationsofCoursesandPrograms_000.pdf">Additions, Deletions and Modifications of Courses and Programs</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ContinuingEducationCoursesandFees_001.pdf"><strong>Continuing Education Courses &amp; Fees </strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda_005.pdf">Human Resources Agenda</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals_004.pdf">Submission of Grant Proposals</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeletions_005.pdf">Property Deletions</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/Reports-ItemsAandB_000.pdf">Reports - Items A &amp; B</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/UpdatestoHouseBill7135DBOTfinal.pdf">General Education Requirements Briefing</a></li>
                           
                           <li><a href="/about/about-valencia/documents/EastCampusReport.pdf">East Campus Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/BoardofTrusteesEastCampusSGAReport_09172013.pdf">East Campus SGA Report </a></li>
                           
                           <li><a href="/about/about-valencia/documents/DBOT_Fall_2013_Enrollment_Report.pdf">Fall Enrollment Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/DBOT_Fall_2013_Enrollment_Report.pptx">Faculty Council Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReport_002.pdf">Financial Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReport_004.pdf">Construction Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReport_004.pdf">Valencia Foundation Report</a> 
                           </li>
                           	
                        </ul>
                        
                     </div>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2013-09-17.pcf">©</a>
      </div>
   </body>
</html>