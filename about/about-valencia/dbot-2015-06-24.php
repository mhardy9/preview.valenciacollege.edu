<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>June 24, 2015 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2015-06-24.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>June 24, 2015 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>June 24, 2015 Meeting</strong></p>
                        
                        <ul class="list-style-1">       
                           	
                           <li><strong><a href="/about/about-valencia/documents/Agenda-June242015_001.pdf">Agenda </a></strong></li>
                           
                           <li><a href="/about/about-valencia/documents/RegularMeetingMinutes-May272015.pdf">Approval of Minutes - May 27, 2015 Regular Meeting</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentReport-Jun242015.pdf">President's Report </a></li>
                           
                           <li>Public Comment</li>
                           	
                        </ul>
                        
                        <h4>New Business</h4>
                        		
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/OperatingBudget2015-2016-Jun242015.pdf">Operating Budget 2015-2016</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ReallocationofBoardDesignationTelephoneJune2015.pdf">Request to Reallocate Unexpended Plant Funds for Leasehold Improvements to Advanced
                                 Manufacturing Facility </a></li>
                           
                           <li><a href="/about/about-valencia/documents/CapitalImprovementProgramJune2015.pdf">Capital Improvement Program (CIP) FY 2016/2017 - 2020/2021</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ArchitecturalDesignServicesPoincianaJune2015_000.pdf">RFQ 2015-18, Architectural Design Services, Poinciana Campus</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/CRMSoftwarePurchase-Jun242015.pdf">CRM (Constituent Relationship Management) Software Purchase</a></li>
                           
                           <li><a href="/about/about-valencia/documents/GroupMedicalandPharmacyPlanServices-Jun242015.pdf">Group Medical and Pharmacy Plan Services, RFP 2015-20</a></li>
                           
                           <li><a href="/about/about-valencia/documents/DelinquentAccountWriteoffJune2015.pdf">Delinquent Account Write-Off</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SREFInspectionJune2015.pdf">2014-2015 Annual Fire Safety, Casualty Safety and Sanitation Inspection</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdditionsDeletionsorModificationsofCoursesandPrograms-Jun242015.pdf">Additions, Deletions or Modifications of Courses and Programs</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda-Jun242015.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Jun242015.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDelete06-24-15.pdf">Property Deletions</a></li>
                           
                           <li>President's Performance Evaluation</li>
                           
                           <li>President's Employment Contract</li>
                           
                           <li><a href="/about/about-valencia/documents/2015-2016AnnualDateNotification-Board_000.pdf">Discussion of 2015-2016 Meeting Schedule</a></li>
                           
                        </ul>
                        	
                        <h4>Reports</h4>
                        		
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/EastCampusReport-LearningSpprtPresentation-Jun242015.pdf">East Campus Report </a></li>
                           
                           <li>East Campus SGA Report</li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportJune2015.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportJune2015.pdf">Construction Report</a></li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReport-Jun242015.pdf">Valencia Foundation Report</a> 
                           </li>
                           	
                        </ul>
                        
                     </div>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2015-06-24.pcf">©</a>
      </div>
   </body>
</html>