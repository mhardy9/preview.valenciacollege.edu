<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>February 24, 2016 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2016-02-24.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>February 24, 2016 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p> <strong>February 24, 2016 - Regular Meeting</strong></p>
                        				
                        <ul class="list-style-1">
                           				
                           <li><a href="/about/general-counsel/policy/historical/Agenda-RegularMeeting-WestCampus-Feb242016.pdf"><strong>Agenda</strong> </a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/RegularMeetingMinutes-Dec92015.pdf">Approval of Minutes - December 9, 2015 Regular Meeting </a>    
                           </li>
                           
                           <li>President's Report    </li>
                           					
                           <li><strong><a href="/about/general-counsel/policy/historical/UrbanAgricultureandAquaponicsAbbreviatedSummary_DBOT_022416.pdf">Valencia College - Urban Agriculture and Aquaponics Proposal </a></strong></li>
                           			 
                           <li><a href="/about/general-counsel/policy/historical/PublicComment-Feb242016.pdf">Public Comment</a>   
                           </li>
                           	
                        </ul>
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/JointResolutionOCPS-Feb242016.pdf">Joint Resolution with Orange County Public Schools on Expanding Career Education</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/AdditionsDeletionsModificationsofCoursesandPrograms-Feb242016.pdf">Additions, Deletions or Modifications of Courses &amp; Programs</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/NewItems-ContinuingEducationCoursesandFees-February242016.pdf">Continuing Education Courses &amp; Fees</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/HumanResourcesAgenda-Feb242016.pdf">Human Resources Agenda</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/SubmissionofGrantProposals-Feb242016.pdf">Submission of Grant Proposals</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/PropertyDelete02-24-16.pdf">Property Deletions</a>   
                           </li>
                           
                           <li>Board Comments</li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/historical/BOT-Presentation-PATHWAYS-Feb242016.pdf">West Campus Report</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/WestCampusSGAPPTforBOTMeeting-Feb242016.pdf">West Campus SGA Report</a>   
                           </li>
                           
                           <li><strong><a href="https://www.youtube.com/watch?v=GFyhCUIuTmE">West Campus SGA YouTube Presentation</a></strong></li>
                           
                           <li><a href="/about/general-counsel/policy/historical/DBOTSpring2016EnrollmentReportFinal.pdf">Spring Enrollment Report</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/FinancialReport-Feb242016.pdf">Financial Report</a>   
                           </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/ConstructionReport-Feb242016.pdf">Construction Report</a>   
                           </li>
                           
                           <li>Faculty Council Report   </li>
                           
                           <li><a href="/about/general-counsel/policy/historical/valenciafoundationtrusteereport24feb2016.pdf">Valencia Foundation Report </a>   
                           </li>
                           	
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2016-02-24.pcf">©</a>
      </div>
   </body>
</html>