<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>June 18, 2013 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2013-06-18.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>June 18, 2013 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>June 18, 2013 Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           			   
                           <li><a href="/about/about-valencia/documents/Agenda_001.pdf"><strong>Agenda</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/Approvalof5-21-2013DBOTMeetingMinutes.pdf">Approval of Minutes</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentReport.pdf"><strong>President Report</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/TheAspenInstituteCollegeExcellenceProgramHandout.pdf">The Aspen Institute College Excellence Program Handout</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/NewBusinessItem1-OperationBudget2013-2014.pdf">New Business and Item 1_Operating Budget 2013-2014 </a></li>
                           
                           <li><a href="/about/about-valencia/documents/OperatingBudget2013-14June2013.pdf">2013-2014 Operating Budget Packet</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/OperatingBudget2013-14JuneDBOT.pdf">2013-2014 Operating Budget Presentation to the Board</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/DowntownCenterLease_000.pdf">Downtown Center Lease Renewal</a></li>
                           
                           <li><a href="/about/about-valencia/documents/CapitalImprovementProgram-FY2014-2015-2018-2019.pdf">Capital Improvement Program (CIP) FY 2014/2015 - 2018/2019</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/DelinquentAccountWriteOff_000.pdf">Delinquent Account Write Off </a></li>
                           
                           <li><a href="/about/about-valencia/documents/AmendmenttoOperatingBudget2012-2013.pdf">Amendment to Operating Budget 2012-2013 </a></li>
                           
                           <li><a href="/about/about-valencia/documents/DualEnrollmentFees.pdf">Dual Enrollment Fees</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ContinuingEducationCoursesandFees.pdf">Continuing Education Courses and Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda_003.pdf">Human Resources Agenda</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals_002.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeletions_003.pdf">Property Deletions</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ReportsItem1-PatientProtectionandtheAffordableCareAct.pdf">Reports - Item 1 - Patient Protection and the Affordable Care Act </a></li>
                           
                           <li><a href="/about/about-valencia/documents/InternalAuditReport.pdf">Internal Audit Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/FacultyCouncilReport_000.pdf">Faculty council Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReport_001.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReport_002.pdf">Construction Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReportDocuments.pdf"><strong>Valencia Foundation Report</strong></a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2013-06-18.pcf">©</a>
      </div>
   </body>
</html>