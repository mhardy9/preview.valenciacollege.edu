<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>May 24, 2017 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2017-05-24.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>May 24, 2017 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>The District Board of Trustees</h2>
                        			
                        <h3>Agenda and Materials</h3>
                        	
                        <p><strong>May 24, 2017 - Regular Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           			
                           <li><a href="/about/about-valencia/documents/Agenda-Regular-Meeting-Winter-Park-Campus-May-24-2017.pdf"><strong>Agenda</strong></a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Regular-Meeting-Minutes-Apr-26-2017.pdf">APPROVAL  OF MINUTES - April 26, 2017 Regular Meeting </a></li>
                        </ul>
                        
                        <h4>President Report</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Legislative-Session-Report-May-24-2017.pdf">2017 Legislative Session Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Valencia-College-Graduation-Data.pdf">Valencia College Graduation Data</a></li>
                           			
                           <li>Public Comment</li>
                           
                        </ul>        
                        	
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">	
                           	
                           <li><a href="/about/about-valencia/documents/Budget-Overview-FY1718-May-24-2017.pdf">2017-18 Budget Overview</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Student-Fees-2017-18-May-24-2017.pdf">Approval of Student Fees 2017-2018</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/SREF-Inspection-May-24-2017.pdf">2016-17 Annual Fire Safety, Casualty Safety  and Sanitation Inspection </a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Transfer-of-Loan-Funds-May-24-2017.pdf">Release of Loan Funds to Valencia College  Foundation for Save our Students Scholarships</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Policy-Adoption-May-24-2017.pdf">Policy Adoption<br>
                                 				Policy 6Hx28: 3D-14 – Paid Time Off for Part  Time Faculty Teaching Academic Credit
                                 Programs</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Approval-of-2017-2018-College-Catalog-May-24-2017.pdf">Approval of 2017-2018 College Catalog</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Signature-Authorization-Resolution-May-24-2017.pdf">Signature Authorization Resolution</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/HR-Agenda-May-24-2017.pdf">Human Resources Agenda</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Additions-Deletions-Modifications-Courses-and-Programs-May-24-2017.pdf">Additions, Deletions or Modifications of  Courses &amp; Programs </a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Continuing-Education-Courses-and-Fees-May-24-2017.pdf">Continuing Education Courses &amp; Fees</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Submission-of-Grant-Proposals-May-24-2017.pdf">Submission of Grant Proposals</a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Property-Deletions-May-24-2017.pdf">Property Deletions</a></li>
                           
                           <li>Board Comments</li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        			
                        <ul class="list-style-1">
                           			
                           <li>Winter Park Campus Report</li>
                           			
                           <li><a href="/about/about-valencia/documents/Winter-Park-Campus-SGA-Report-May-24-2017.pdf">Winter Park Campus SGA Report </a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Financial-Report-May-24-2017.pdf">Financial Report </a></li>
                           			
                           <li><a href="/about/about-valencia/documents/Construction-Report-May-24-2017.pdf">Construction Report</a></li>
                           			
                           <li>Faculty Council Report </li>
                           			
                           <li><a href="/about/about-valencia/documents/Valencia-Foundation-Report-May-24-2017.pdf">Valencia Foundation Report</a></li>
                           			
                        </ul>
                        			
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2017-05-24.pcf">©</a>
      </div>
   </body>
</html>