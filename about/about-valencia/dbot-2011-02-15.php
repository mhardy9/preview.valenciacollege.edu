<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>February 15, 2011 Board of Trustees Regular Meeting  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2011-02-15.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>February 15, 2011 Board of Trustees Regular Meeting </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <main role="main">
                     
                     <div class="row">
                        
                        <div class="col-md-12">
                           
                           
                           
                           <h2>Board of Trustees</h2>
                           
                           <h3> Agenda and Board Materials</h3>
                           
                           <p><strong>February 15, 2011 Board of Trustees Regular Meeting</strong></p>
                           
                           <ul class="list-style-1">
                              
                              
                              <li><a href="/about/about-valencia/documents/Agenda-Feb2011.pdf" title="Agenda-Feb2011.pdf"><strong>Agenda</strong></a></li>
                              
                              <li><a href="/about/about-valencia/documents/MinutesofDec2010DBOTMeeting.pdf" title="MinutesofDec2010DBOTMeeting">Minutes of December 2010 DBOT Meeting</a></li>
                              
                              <li><a href="/about/about-valencia/documents/DBOTSpring2011EnrollmentReportl.pptx" title="DBOTSpring2011EnrollmentReportl">Spring Enrollment Report</a></li>
                              
                              <li><a href="/about/about-valencia/documents/QuarterlyFinancialRptFeb2011.pdf" title="QuarterlyFinancialRptFeb2011">Quarterly Financial Report</a></li>
                              
                              <li><a href="/about/about-valencia/documents/ConstructionReport-Feb2011.pdf" title="ConstructionReport-Feb2011">Construction Report</a></li>
                              
                              <li><a href="/about/about-valencia/documents/LetterofIntent-BSCardiopulmonaryFeb2011.pdf" title="LetterofIntent-BSCardiopulmonaryFeb2011">Letter of Intent - BS Cardiopulmonary</a></li>
                              
                              <li><a href="/about/about-valencia/documents/2-15-2011EmeritiBoardAgendaItemFebruary2011.pdf" title="2-15-2011EmeritiBoardAgendaItemFebruary2011">Emeriti Board Agenda Item</a></li>
                              
                              <li><a href="/about/about-valencia/documents/FoundationStateMatchingGrantProgramFundsApproval-Feb2011.pdf" title="FoundationStateMatchingGrantProgramFundsApproval-Feb2011">Foundation State Matching Grant Program Funds Approval</a></li>
                              
                              <li><a href="/about/about-valencia/documents/ProposedPolicyAdoption-FacsimileSignatures6Hx28-5-13.pdf" title="ProposedPolicyAdoption-FacsimileSignatures6Hx28-5-13">Proposed Policy Adoption - Facsimile Signatures 6Hx28-5-13</a></li>
                              
                              <li><a href="/about/about-valencia/documents/AdditionsDeletions&amp;ModificationsofCourses&amp;ProgramsFeb2011.pdf" title="AdditionsDeletions&amp;ModificationsofCourses&amp;ProgramsFeb2011">Additions, Deletions and Modifications of Courses and Programs</a></li>
                              
                              <li><a href="/about/about-valencia/documents/ValenciaEnterprisesCourses&amp;Fees.pdf" title="ValenciaEnterprisesCourses&amp;Fees">Valencia Enterprises Courses and Fees</a></li>
                              
                              <li><a href="/about/about-valencia/documents/HumanResourcesAgendaFebruary2011.pdf" title="HumanResourcesAgendaFebruary2011">Human Resources Agenda</a></li>
                              
                              <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Feb2011.pdf" title="SubmissionofGrantProposals-Feb2011">Submission of Grant Proposals</a></li>
                              
                              <li><a href="/about/about-valencia/documents/PropertyDeletions-Feb2011.pdf" title="PropertyDeletions-Feb2011">Property Deletions </a></li>
                              
                              
                           </ul>
                           
                           
                           
                        </div>
                        
                     </div>
                     
                     
                     
                  </main>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2011-02-15.pcf">©</a>
      </div>
   </body>
</html>