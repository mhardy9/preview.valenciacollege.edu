<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>February 22, 2017 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2017-02-22.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>February 22, 2017 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>The District Board of Trustees</h2>
                        			
                        <h3>Agenda and Materials</h3>
                        
                        <p><strong>February 22, 2017 - Regular Meeting</strong></p>
                        			
                        <ul class="list-style-1">
                           			
                           <li><a href="/about/general-counsel/policy/historical/Agenda-RegularMeeting-SPS-Feb222017.pdf"><strong>Agenda</strong> </a></li>
                           			
                           <li>APPROVAL OF MINUTES</li>
                           				
                           <li><a href="/about/general-counsel/policy/historical/RegularMeetingMinutes-Dec72016.pdf">December 7, 2016 Regular Meeting</a></li>
                           				
                           <li><a href="/about/general-counsel/policy/historical/SpecialMeetingMinutes-Feb62017.pdf">February 6, 2017 Special Meeting </a></li>
                           			
                           <li>President's Report</li>
                           			
                           <li><a href="documents/2017-Legislative-Session-Update-Feb-22-2017.pdf">Legislative Update</a></li>
                           			
                           <li>Public Comment </li>
                           			
                        </ul>
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           			
                           <li><a href="/about/general-counsel/policy/historical/ValenciaandAffordabilityofHigherEducation-Feb222017.pdf">Strategic Conversation: College Affordability and Valencia </a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/TransmittalLetter-ArchitecturalContinuingServices-Feb222017.pdf">Award of RFQu #2017-29 for Architectural Continuing Services Contract </a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/Poinciana-Phase-III-CFAL-February-2017.pdf">Contract Amendment for Poinciana Campus Phase III - Center for Accelerated Training
                                 (Ref: RFQu #2016-06)</a></li>
                           			
                           <li>Policy Repeal</li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/PolicyRepeal3C-06.2-Supplemental-and-Overload-Contracts-Feb222017.pdf">Policy 6Hx28: 3C-06.2 - Supplemental and Overaload Contracts</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/AdditionsDeletionsModificationsofCoursesandPrograms-Feb222017.pdf">Additions, Deletions of Modifications of Courses &amp; Programs</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/NewBusiness-ContinuingEducationCoursesandFees-February222017.pdf">Continuing Education Courses &amp; Fees</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/HRAgenda-Feb222017.pdf">Human Resources Agenda</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/SubmissionofGrantProposals-Feb222017.pdf">Submission of Grant Proposals</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/PropertyDelete2-22-17.pdf">Property Deletions</a></li>
                           			
                           <li>Board Comments</li>
                           			
                        </ul>
                        			
                        <h4>Reports</h4>
                        			
                        <p>
                           <ul class="list-style-1">
                              			
                              <li><a href="/about/general-counsel/policy/historical/AuditQuarterlyUpdate-Feb222017.pdf">Audit Plan Update</a></li>
                              			
                              <li>School of Public Safety Report</li>
                              			
                              <li><a href="documents/Central-FL-Public-Safety-Service-College-and-Career-Readiness-Program-Feb-22-2017.pdf">Central FL Public Safety</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/DBOTSpring2017EnrollmentReport-Feb222017.pdf">Spring Enrollment Report</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/FinancialReportFebruary2017.pdf">Financial Report</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/ConstructionReportFebruary2017.pdf">Construction Report</a></li>
                              			
                              <li>Faculty Council Report</li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/ValenciaFoundationReport-Feb222017.pdf">Valencia Foundation Report </a></li>
                              		
                           </ul>
                           	
                        </p>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2017-02-22.pcf">©</a>
      </div>
   </body>
</html>