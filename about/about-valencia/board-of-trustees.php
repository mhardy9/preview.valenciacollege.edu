<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Board of Trustees  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/board-of-trustees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>About Us</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>Board of Trustees </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <h2>Board of Trustees</h2>
                           
                           <p>Each of the 28 community colleges in Florida's statewide system is under the local
                              control of a lay <a href="http://preview.valenciacollege.edu/trustee-education/">District Board of Trustees</a> composed of citizens who serve without pay and are legally vested with decision making
                              power in all matters of college policy, programs, building, budget and personnel.
                           </p>
                           
                           <p>Each trustee is appointed by the governor. Eight Orange and Osceola County citizens
                              form the Valencia College District Board of Trustees. Meeting in regular sessions
                              once each month, these civic-minded individuals contribute their time and talent to
                              guiding the development of Valencia College so that it remains responsive to the educational
                              needs of its local community.
                           </p>
                           
                        </div>
                        
                        <div class="row staff">
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_1">
                                 
                                 <p><img class="img-circle styled" src="/_resources/images/ABOUT/about-valencia/trustee-lew-oliver.jpg" alt="trustee lew oliver"></p>
                                 
                                 <h4>Lew Oliver, <small>Chair</small></h4>
                                 
                                 <p>Chair, Lew Oliver is the owner of Oliver Title Law Firm, LLC, and resides in Orlando.
                                    He has been a Florida resident since 1970, and received his Bachelor's degree from
                                    Stetson University, and his Juris Doctorate from Georgetown University Law School.
                                    He previously served as Senior Vice President of AMT Inc., a transportation technology
                                    firm, and as Vice President for Neotonus, Inc.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_1">
                                 
                                 <p><img class="img-circle styled" src="/_resources/images/ABOUT/about-valencia/trustee-bruce-carlson.jpg" alt="trustee bruce carlson"></p>
                                 
                                 <h4>Bruce Carlson, <small>Vice Chair</small></h4>
                                 
                                 <p>Vice Chair, Bruce Carlson is the owner of Imagination Realty, Inc. and was previously
                                    the vice president of operations for E. I. DuPont De Nemours Performance Coatings.
                                    He is a former treasurer of the American Chemical Society, Delaware Section and serves
                                    as a member of Sigma Xi, The Scientific Research Society. He received his bachelor's
                                    degree from Cornell University and his doctorate from Purdue University.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_1">
                                 
                                 <p><img class="img-circle styled" src="/_resources/images/ABOUT/about-valencia/trustee-maria-grulich.jpg" alt="trustee maria grulich"></p>
                                 
                                 <h4>Maria Grulich, <small>Trustee</small></h4>
                                 
                                 <p>Trustee, Maria Grulich serves as director of global business for the Florida Association
                                    of Realtors, overseeing and developing global initiatives and programs for Florida,
                                    as well as serving as the team liaison to the National Association of Realtors. Grulich
                                    is a resident of Osceola County and a graduate of the University of South Florida.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row staff">
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_1">
                                 
                                 <p><img class="img-circle styled" src="/_resources/images/ABOUT/about-valencia/trustee-raymer-maguire.jpg" alt="trustee raymer maguire"></p>
                                 
                                 <h4>Raymer F. Maguire III, <small>Trustee</small></h4>
                                 
                                 <p>Trustee, Raymer F. Maguire III is an owner of Maguire Lassman, P.A., an eminent domain
                                    law firm. He currently serves on the boards of the Central Care Mission and the Youth
                                    Ministry Institute. He graduated from the University of Florida with honors and earned
                                    his law degree from Florida State University College of Law. His father, Raymer Maguire
                                    Jr., helped found Valencia Community College and served as a trustee for 17 years.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_1">
                                 
                                 <p><img class="img-circle styled" src="/_resources/images/ABOUT/about-valencia/trustee-guillermo-hansen.jpg" alt="trustee guillermo hansen"></p>
                                 
                                 <h4>Guillermo Hansen, <small>Trustee</small></h4>
                                 
                                 <p>Trustee, Guillermo Hansen, of Kissimmee, is the publisher and editor of the newspaper,
                                    El Osceola Star. He serves on the board of the Kissimmee/Osceola County Chamber of
                                    Commerce for which he founded the Minority Business and the Hispanic Business councils.
                                    He has also been an active member of the Osceola Tourist Development Council since
                                    2000. Born and raised in Mexico, Hansen attended Croydon Technical College and London
                                    Tourism College.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_1">
                                 
                                 <p><img class="img-circle styled" src="/_resources/images/ABOUT/about-valencia/trustee-daisey-lopez.jpg" alt="trustee daisey lopez"></p>
                                 
                                 <h4>Daisy Lopez-Cid, <small>Trustee</small></h4>
                                 
                                 <p>Trustee, Daisy Lopez-Cid is the owner of Remax Premier Properties. She is the past
                                    president of the National Association of Hispanic Real Estate Professionals and is
                                    the president-elect of the Osceola Association of Realtors.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <div class="row staff">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_1">
                              
                              <p><img class="img-circle styled" src="/about/about-valencia/images/trustee-tracey-stockwell-2013.jpg" alt="Tracey Stockwell" width="120" height="168"></p>
                              
                              <h4>Tracey Stockwell, <small>Trustee</small></h4>
                              
                              <p>Trustee, <span>Tracey Stockwell is senior vice president and chief financial officer for Universal
                                    Orlando. Before joining Universal Orlando in 1997, she worked at Price Waterhouse.
                                    Currently treasurer of the Universal Orlando Foundation, Stockwell is a member of
                                    the Florida Institute of CFO’s and has served in a variety of roles in the Orlando
                                    Economic Partnership. She earned her bachelor’s degree from the University of Windsor
                                    in Ontario, Canada. She is both a Florida Certified Public Accountant as well as an
                                    Ontario Chartered Accountant.</span></p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/board-of-trustees.pcf">©</a>
      </div>
   </body>
</html>