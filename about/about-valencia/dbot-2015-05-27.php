<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>May 27, 2015 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2015-05-27.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>May 27, 2015 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>May 27, 2015 Meeting</strong></p>
                        			
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Agenda-OsceolaCampus-May272015.pdf"><strong>Agenda</strong> </a></li>
                           
                           <li><a href="/about/about-valencia/documents/RegularMeetingMinutes-Apr222015.pdf">Approval of Minutes - April 22, 2015 Regular Meeting</a> 
                           </li>
                           
                           <li>Special Recognitions </li>
                           
                           <li>President's Report </li>
                           
                           <li>Public Comment</li>
                           	
                           
                        </ul>
                        	
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/Food-VendingServicesMay272015.pdf">Food Services, Dining and Catering Agreement (Food Services, Inc.) and Vending Services
                                 Agreement (Canteen Vending Services Division)</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdvancedManufacturingFacilityLeaseAgreement-May272015.pdf">Facility Lease Agreement for Advanced Manufacturing </a></li>
                           
                           <li><a href="/about/about-valencia/documents/StudentFees2015-2016-May272015.pdf">Approval of Student Fees 2015-2016</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/2015-2016CollegeCatalogChanges-May272015.pdf">Approval of 2015-2016 College Catalog</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/BoardTransmittalLetter-FloridaCollegeSystemRiskManagementConsortiumAgreement-May272015.pdf">Florida College System Risk Management Consortium Agreement </a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdditionsDeletionsModificationsofCoursesandFees-May272015.pdf">Additions, Deletions or Modifications of Courses and Programs</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/NewItems-ContinuingEducationCoursesandFees-May272015.pdf">Continuing Education Courses and Fees </a></li>
                           
                           <li><a href="/about/about-valencia/documents/HRAgenda-May272015.pdf">Human Resources Agenda </a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-May272015.pdf">Submission of Grant Proposals</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDelete05-27-15.pdf">Property Deletions </a></li>
                           
                        </ul>
                        
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/LSAMPReport-May272015.pdf">NSF LSAMP Bridges to Baccalaureate Program Update</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/OsceolaCampusReport-Ambassadors-May272015.pdf">Osceola Campus Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/SGAReport-May272015.pdf">Osceola Campus SGA Report </a></li>
                           
                           <li><a href="/about/about-valencia/documents/AuditPlanStatus-May2015.pdf">Internal Auditor's Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportMay2015.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportMay2015.pdf">Construction Report</a></li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReport-May272015.pdf">Valencia Foundation Report </a></li>
                           	
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2015-05-27.pcf">©</a>
      </div>
   </body>
</html>