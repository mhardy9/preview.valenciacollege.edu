<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>November 2, 2016 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2016-11-02.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>November 2, 2016 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>The District Board of Trustees</h2>
                        			
                        <h3>Agenda and Materials</h3>
                        			
                        <p><strong>November 2, 2016 Meeting</strong> 
                        </p>
                        			
                        <p>
                           			
                           <ul class="list-style-1">
                              			
                              <li><a href="/about/general-counsel/policy/historical/Agenda-RegularMeeting-EAC-Nov22016.pdf"><strong>Agenda</strong> </a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/RegularMeetingMinutes-Sep282016.pdf">Approval of Minutes - September 28, 2016 Regular Meeting </a></li>
                              			
                              <li>President's Report </li>
                              			
                              <li>Public Comment </li>
                              			
                           </ul>
                           
                           <h4>New Business</h4>
                           	
                           <ul class="list-style-1">
                              			
                              <li><a href="/about/general-counsel/policy/historical/BoardTransmittalLetter-5-yearImpactPlan-Nov22016.pdf">Valencia College 5-Year Impact Plan 2016-2021 </a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/DBOT--ValenciaImpactPlan--Presentation-Nov22016.pdf">Valencia College 5-Year Impact Plan Presentation </a></li>
                              
                              			
                              <li><a href="/about/general-counsel/policy/historical/WestCampusBldg6InteriorRenovation-Nov22016.pdf">Interior Renovation - Building 6 West Campus (RFQu #2017-01) Approval for Budget Expenditure
                                    </a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/VCFAnnualDirectSupportOrganizationAuditReview-Nov22016.pdf">Valencia Foundation Annual Audit Review</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/AuditPlantProposal2016-2017-Nov22016.pdf">Audit Plan 2016-2017</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/BoardTransmittalLetter-AuditCharter-Nov22016.pdf">Audit Charter</a></li>
                              			
                           </ul>
                        </p>
                        			
                        <h4>Policy Adoptions </h4>
                        			
                        <p>
                           <ul class="list-style-1">
                              			
                              <li><a href="/about/general-counsel/policy/historical/FairLaborStandardsActpresentation-Nov22016.pdf">Fair Labor Standards Act Presentation</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/BoardApprovalLetter-PolicyAdoptions-Nov22016.pdf">Board Approval Letter - Policy Adoptions </a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/3C-01-Total-Rewards-Compensation-Hours-Work-Employees-Nov22016.pdf">6Hx28: 3C-01 - Total Rewards: Compensation &amp; Hours of Work for Employees of the College</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/3C-04-Total-Rewards-Employee-Educ-Recognition-Nov22016.pdf">6Hx28: 3C-04 - Total Rewards: Recognition of Full-time Employees for Educational Advancement</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/3C-061-Contracts-for-Administrative-Professional-and-Instructional-Personnel-Nov22016.pdf">6Hx28: 3C-06.1 - Contracts for Instructional, Executive, and Administrative Employees</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/3C-08-Total-Rewards-Employee-Benefits-Nov22016.pdf">6Hx28: 3C-08 - Total Rewards: Employee Benefits</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/3E-01-Full-Time-PersonnelPerf-Evaluations-Nov22016.pdf">6Hx28: 3E-01 - Full-Time Employee Performance Evaluations</a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/3F-02-Terminal-Pay-Nov22016.pdf">6Hx28: 3F-02 - Terminal Pay for Full-Time Employees </a></li>
                              			
                              <li><a href="/about/general-counsel/policy/historical/3F-03-SuspensionDismissalRtntoAnnualContract-NonrenewalofContracts-Nov22016.pdf">6Hx28: 3F-03 - Suspension, Dismissal, Return to Annual Contract or Non-Renewal of
                                    Contracts</a></li>
                              			
                           </ul>
                        </p>
                        		
                        <h4>Policy Repeals </h4>
                        		
                        <ul class="list-style-1">
                           			
                           <li><a href="/about/general-counsel/policy/historical/BoardApprovalLetter-PolicyRepeals-Nov22016.pdf">Board Approval Letter - Policy Repeals </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-01-Job-Description-for-Personnel-of-the-College-11-2-2016.pdf">6Hx28: 3A-01 - Job Description for Personnel of the College </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-02.1-Def-of-Admin-Inst-and-Prof-Employees-11-2-2016.pdf">6Hx28: 3A-02.1 - Definition of Administrative, Instructional, and Professional Employees
                                 </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-02.2-Definition-of-Full-Time-Employment-Instructional-and-Admin-Employees-11-2-2016.pdf">6Hx28: 3A-02.2 - Definition of Full-Time Employment - Instructional and Administrative
                                 Employees</a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-02.3-Definition-of-a-Career-Service-Employee-11-2-2016.pdf">6Hx28: 3A-02.3 - Defnition of a Career Service Employee </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-02.4-Definition-of-Full-Time-Employment-for-Career-11-2-2016.pdf">6Hx28: 3A-02.4 - Definition of Full-Time Employment for Career Service </a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-02.4-Definition-of-Full-Time-Employment-for-Career-11-2-2016.pdf"><strong>Employees </strong></a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-03-Classification-of-Career-Service-Positions-11-2-2016.pdf">6Hx28: 3A-03 - Classification of Career Service Positions </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-04.1-Part-Time-Personnel-11-2-2016.pdf">6Hx28: 3A-04.1 - Part-Time Personnel</a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3A-04.2-Part-Time-Instructional-Personnel-11-2-2016.pdf">6Hx28: 3A-04.2 - Part-Time Instructional Personnel </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-01.1-Hours-of-Work-for-Instructional-and-Administrative-Employees-11-2-2016.pdf">6Hx28: 3C-01.1- Hours of Work for Instructional and Administrative Employees</a> 
                           </li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-02.1-Hours-of-Work-for-Career-Service-Employees-11-2-2016.pdf">6Hx28: 3C-02.1 - Hours of Work for Career Service Employees </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-02.2-Recording-of-Hours-Worked-for-Career-Service-11-2-2016.pdf">6Hx28: 3C-02.2 - Recording of Hours Worked - Career Service Employees</a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-02.3-Overtime-Compensation-for-Career-Service-Employees-11-2-2016.pdf">6Hx28: 3C-02.3 - Overtime Compensation for Career Service Employees </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-02.4-Night-Shift-Differential-11-2-16.pdf">6Hx28: 3C-02.4 - Night Shift Differential</a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-02.5-Payment-to-Career-Service-Employees-for-Holidays-Worked-11-2-2016.pdf">6Hx28: 3C-02.5 - Payment to Career Service Employees for Holidays Worked </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-03.1-Payroll-Deduction-Authorization-11-2-2016.pdf">6Hx28: 3C-03.1 - Payroll Deduction Authorization</a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-03.2-Payroll-Deductions-for-Dues-for-Certain-Qualifying-Employee-Organizations-11-2-2016.pdf">6Hx28: 3C-03.2 - Payroll Deductions for Dues for Certain Qualifying Employee Organizations</a> 
                           </li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-04.1-Salary-Schedule-11-2-2016.pdf">6Hx28: 3C-04.1 - Salary Schedule </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-04.3.1-Pay-of-Instructional-Personnel-Receiving-Advanced-College-Credit-11-2-2016.pdf">6Hx28: 3C-04.3.1 - Pay of Instructional Personnel Receving Advanced College Credit
                                 </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-04.3.2-Pay-of-Professional-and-Administrative-Personnel-for-Advanced-College-Credit-11-2-.pdf">6Hx28: 3C-04.3.2 - Pay of Professional and Administrative Personnel Receving Advanced
                                 College Credit </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3C-05-Transfer-from-Temporary-Grant-Funded-Position-to-a-College-Funded-Position-11-2-2016.pdf">6Hx28: 3C-05 - Tranfer from Temporary Grant-Funded Position to a College-Funded Position</a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/3D-01-PaidNon-Duty-Days-11-2-2016.pdf">6Hx28: 3D-01 - Paid Non-Duty Days </a></li>
                           
                           			
                           <li><a href="/about/general-counsel/policy/historical/AmendedSalarySchedule2016-2017-Nov22016.pdf">Amended 2016-2017 Salary Schedule </a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/AdditionsDeletionsModificationsofCoursesandPrograms-Nov22016.pdf">Additions, Deletions or Modifications of Courses &amp; Programs</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/NewBusiness-ContinuingEducationCoursesandFees-November22016.pdf">Continuing Education Courses &amp; Fees </a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/HRAgenda-Nov22016.pdf">Human Resources Agenda</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/SubmissionofGrantProposals-Nov22016.pdf">Submission of Grant Proposals</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/PropertyDelete11-02-16.pdf">Property Deletions</a></li>
                           			
                           <li>Board Comments </li>
                           		
                        </ul>
                        			
                        <h4>Reports</h4>
                        			
                        <ul class="list-style-1">
                           			
                           <li><a href="/about/general-counsel/policy/historical/EACReport-Nov22016.pdf">East Campus Report</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/EACSGAReport-Nov22016.pdf">East Campus SGA Report</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/Presentation-ValenciaDBOTDowntown-Nov22016.pdf">Downtown UCF Campus Hospitality &amp; Culinary School Update</a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/FinancialReport-Nov22016.pdf">Financial Report </a></li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/ConstructionReportNovember2016.pdf">Construction Report </a></li>
                           			
                           <li>Faculty Council Report</li>
                           			
                           <li><a href="/about/general-counsel/policy/historical/ValenciaFoundationReport-Nov22016.pdf">Valencia Foundation Report </a></li>
                           	
                        </ul>
                        	  
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2016-11-02.pcf">©</a>
      </div>
   </body>
</html>