<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>December 4, 2013 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2013-12-04.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>December 4, 2013 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        	
                        <p><strong>December 4, 2013 Meeting</strong></p>
                        	
                        <ul class="list-style-1">     
                           	
                           <li><a href="/about/about-valencia/documents/Agenda-CJI-120413_000.pdf"><strong>Agenda</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/ApprovalofRegularMeetingMinutes_Oct16_2013_000.pdf">Approval of Regular Minutes - Oct 16, 2013</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentReport_003.pdf">President's Report </a></li>
                           			
                           <li><a href="/about/about-valencia/documents/PublicComment.pdf">Public Comment</a></li>
                           	
                        </ul>
                        
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           	
                           <li><a href="/about/about-valencia/documents/TenYearAgreementwithSummitBroadbandDarkFiberNetwork_000.pdf"> Ten-Year Agreement with Summit Broadband Dark Fiber Network </a></li>
                           
                           <li><a href="/about/about-valencia/documents/DevelopmentalEducationImplementationPlan_000.pdf">Developmental Education Implementation Plan </a></li>
                           
                           <li><a href="/about/about-valencia/documents/DevelopmentalEducationImplementationPlanPresentation.pdf">Developmental Education Implementation Plan (materials)</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ApprovalofRevisedRiskManagementConsortiumAgreement_000.pdf">Approval of Revised Risk Management Consortium Agreement</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PolicyAdoption_002.pdf">Policy Adoption</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Additions_Deletions_ModificationsofCoursesandPrograms_000.pdf">Additions, Deletions, or Modifications of Courses and Programs</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ContinuingEducationCoursesandFees_004.pdf">Continuing Education Courses &amp; Fees </a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda_008.pdf">Human Resources Agenda</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals_007.pdf">Submission of Grant Proposals</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeletions_008.pdf">Property Deletions</a> 
                           </li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        		
                        <ul class="list-style-1">
                           		
                           <li><a href="/about/about-valencia/documents/CJI_CentralFloridaFireInstituteReport_000.pdf">Criminal Justice Institute - Central Florida Fire Institute Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/CJIPartnershipInformationwithAnaG.MendezUniversity.pdf">Criminal Justice Institute (additional materials) </a></li>
                           
                           <li><a href="/about/about-valencia/documents/ContinuingEducationReport_000.pdf">Continuing Education Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/AuditPlanStatus_000.pdf">Audit Plan Status</a></li>
                           
                           <li><a href="/about/about-valencia/documents/CompetitiveSolicitationProcessAuditReport_000.pdf">Competitive Solicitation Process Audit Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/AgreementsonDualEnrollmentwithOrangeandOsceolaCounties_000.pdf">Agreements on Dual Enrollment with Orange &amp; Osceola Counties</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/DualEnrollmentAgreements2013-2014.pdf">Agreements on Dual Enrollment with Orange &amp; Osceola Counties (presentation)</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FacultyCouncilReport_005.pdf">Faculty Council Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialIndicatorsReport_001.pdf">Financial Indicators Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReport_004.pdf">Financial Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReport_007.pdf">Construction Report</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReport_008.pdf">Valencia Foundation Report</a> 
                           </li>
                           		
                        </ul>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2013-12-04.pcf">©</a>
      </div>
   </body>
</html>