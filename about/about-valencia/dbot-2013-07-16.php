<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>July 16, 2013 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2013-07-16.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>July 16, 2013 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>July 16, 2013 Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           				
                           <li><a href="/about/about-valencia/documents/Agenda-07162013.pdf"><strong>Agenda</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/Approvalof07182013Minutes.pdf">Approval of Minutes - Jun18, 2013</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentReportandNewBusinessItems1and2.pdf"><strong>President's Report - New Business Items 1 and 2 </strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/OsceolaCountyLandLease.pdf">New Business - Item 3 - Osceola County Land Lease </a></li>
                           
                           <li><a href="/about/about-valencia/documents/DistrictOfficeLease.pdf">New Business - Item 4 - District Office Lease</a></li>
                           
                           <li><a href="/about/about-valencia/documents/GuaranteedMaximumPrice-RenovationoftheNewDistrictOffice.pdf">New Business - Item 5 - Guaranteed Maximum Price - New District Office </a></li>
                           
                           <li><a href="/about/about-valencia/documents/AuthorizationtoDesignateaPoincianaCampus.pdf">New Business - Item 6 - Authorization to Designate a Poinciana Campus</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PoliciesAdoption.pdf">New Business - Item 7 - Policies Adoption</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Revised2013-2014BaccalaureateTuitionFee-Documentation.pdf">New Business - Item 8 - Revised 2013-2014 Baccalaureate Tuition Fee</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Revised2013-2014OperatingBudget.pdf">New Business - Item 9 - Revised 2013-2014 Operating Budget</a></li>
                           
                           <li><a href="/about/about-valencia/documents/RequesttoReleaseFundBalance.pdf">New Business - Item 10 - Request to Release Fund Balance Reserve</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SACSReaccreditationQEP.pdf">New Business - Item 11 - SACS Reaccreditation Quality Enhancement Plan</a></li>
                           
                           <li><a href="/about/about-valencia/documents/RequestforQualifications-RFQ1213-04.pdf">New Business - Item 12 - Delivery Order/RFQ 12/13-04</a></li>
                           
                           <li><a href="/about/about-valencia/documents/InternalAuditPlanFY14.pdf">New Business - Item 13 - Internal Audit Plan FY14</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Additions-Deletions-ModificationsofCoursesandPrograms.pdf">New Business - Item 14 - Additions, Deletions or Modifications of Courses &amp; Programs</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ContinuingEducationCoursesandFees_000.pdf">New Business - Item 15 - Continuing Education Courses &amp; Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda_004.pdf">New Business - Item 16 - Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals_003.pdf">New Business - Item 17 - Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeletions_004.pdf">New Business - Item 18 - Property Deletions </a></li>
                           
                           <li><a href="/about/about-valencia/documents/Reports-ItemsAandB.pdf">Reports - Osceola Campus Report </a></li>
                           
                           <li><a href="/about/about-valencia/documents/OsceolaSGAReportPresentation.pdf">Item B - SGA Report Presentation</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/SummerEnrollmentReport.pdf">Reports - Item C - Summer Enrollment Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FacultyCouncilReport_001.pdf">Reports - Item D - Faculty Council Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/LabFeeAuditReport.pdf">Reports - Item E - Lab Fee Audit Report </a></li>
                           
                           <li><a href="/about/about-valencia/documents/InternalAuditorSelf-Assessment.pdf">Reports - Item F - Internal Auditor Self-Assessment</a></li>
                           
                           <li><a href="/about/about-valencia/documents/2012-2013AnnualInternalAuditReport.pdf">Reports - Item G - 2012-2013 Annual Internal Audit Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReport_003.pdf">Reports - Item H - Construction Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReport-Documentation.pdf">Reports - Item I - Valencia Foundation Report</a> 
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2013-07-16.pcf">©</a>
      </div>
   </body>
</html>