<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>May 21, 2013 Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2013-05-21.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>May 21, 2013 Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        			
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>May 21, 2013 Meeting</strong>  
                        </p>
                        
                        <ul class="list-style-1">   
                           		   
                           <li><a href="/about/about-valencia/documents/Agenda_000.pdf"><strong>Agenda</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/ApprovalofMinutes.pdf">Approval of Minutes</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PresidentReportandNewBusiness1and2.pdf"><strong>President Report and New Business  1 and 2</strong></a></li>
                           
                           <li><a href="/about/about-valencia/documents/ProposedStudentFeesScheduleFY2013-2014.pdf">Approval of Student Fees for FY 2013-2014</a></li>
                           
                           <li><a href="/about/about-valencia/documents/NewBusiness_Items4thru8.pdf">New Business_Items 4 thru 8</a></li>
                           
                           <li><a href="/about/about-valencia/documents/2013_05_21_13_29_02.pdf">Item 5 Material_Advertising by Ethnicity</a> 
                           </li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda_002.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals_001.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDeletions_002.pdf">Property Deletions</a></li>
                           
                           <li><a href="/about/about-valencia/documents/Reports-ItemA-ResponsetoCIGReport.pdf">Reports - Item A - Response to CIG Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/WinterParkCampusReport_SGAReport.pdf">Winter Park Campus Report_SGA Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FacultyCouncilReport.pdf">Faculty Council Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReport_000.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReport_001.pdf">Construction Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ValenciaFoundationReport_000.pdf">Valencia Foundation Report</a></li>
                           	
                           <li><a href="/about/about-valencia/documents/FoundationReportMaterials.pdf">Valencia Foundation Materials</a> 
                           </li>
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2013-05-21.pcf">©</a>
      </div>
   </body>
</html>