<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Workforce Development  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/workforce-development.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>Workforce Development </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Workforce Development</h2>
                        
                        
                        <p>The Career &amp; Workforce Education Division works cooperatively college wide and in
                           partnership with
                           business/industry and the community to develop and strengthen learning opportunities
                           for students that
                           prepare them for a successful transition from college to the workplace while addressing
                           the economic
                           development needs of our Central Florida community.&nbsp; The division includes facets
                           of assessment of
                           workforce demands, program development and review of performance and relevancy of
                           programs to meet the
                           workforce needs, work based learning experiences for students and faculty/staff, placement
                           services for
                           students and the articulation of programs.
                        </p>
                        
                        <h3>Who We Are</h3>
                        
                        <ul class="list-style-1">
                           
                           <li> Dr. Nasser Hedayat, Assistant Vice President for Career &amp; Workforce Education</li>
                           
                           <li>LeSena Jones, Manager, Career and Workforce Education</li>
                           
                           <li>Anjela Madison, Coordinator, Perkins Grant</li>
                           
                           <li>Cathy Campbell, Career &amp; Workforce Research Specialist</li>
                           
                           <li>Anissa Mohun, Career &amp; Workforce Research Specialist</li>
                           
                        </ul>
                        
                        <h3><a href="/students/career-pathways/"><strong>Career Pathways</strong></a></h3>
                        
                        <ul class="list-style-1">
                           
                           <li>Vicki Fine, Manager, Career Pathways</li>
                           
                           <li>Kailyn Williams, Career Pathways Specialist</li>
                           
                           <li>Rob Strobbe, Perkins Database Specialist</li>
                           
                        </ul>
                        
                        <h3><a href="/students/internship/"><strong>Internships &amp; Workforce
                                 Services</strong></a></h3>
                        
                        <ul class="list-style-1">
                           
                           <li>Michelle Terrell, Director</li>
                           
                           <li>Kamla Billings, Coordinator, Employer Relations</li>
                           
                           <li>Debra Sembrano, Technical Speclialist</li>
                           
                           <li>Janice Callaway, Implementation Coordinator</li>
                           
                           <li>Kerry Fleming, East Campus, Coordinator, Internship and Workforce Services</li>
                           
                           <li>Mary El-Nahas, Staff Assistant II</li>
                           
                           <li>Xavier Humphrey, Osceola and Lake Nona Campuses, Coordinator, Internship and Workforce
                              Services
                           </li>
                           
                           <li>Staphany Pachon, Staff Assistant II</li>
                           
                           <li>Jessica Camilo, West and Winter Park Campuses, Coordinator, Internship and Workforce
                              Services
                           </li>
                           
                           <li>Martha Parada, Staff Assistant II</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/workforce-development.pcf">©</a>
      </div>
   </body>
</html>