<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>September 23, 2015 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/DBOT-2015-09-23.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>September 23, 2015 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Board of Trustees</h2>
                        		
                        <h3> Agenda and Board Materials</h3>
                        
                        <p><strong>September 23, 2015 - Regular Meeting</strong></p>
                        
                        <ul class="list-style-1">
                           			   
                           <li><a href="/about/about-valencia/documents/Agenda-RegularMeeting-SPS-Sep232015.pdf"><strong>Agenda</strong> </a></li>
                           
                           <li><a href="/about/about-valencia/documents/RegularMeetingMinutes-Jun242015.pdf">Approval of Minutes - June 24, 2015 Regular Meeting</a></li>
                           
                           <li>President's Report</li>
                           
                           <li>Public Comment</li>
                           
                        </ul>	
                        
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/201509DBOTGMPAdvancedManufacturingOsceolaCounty-FINAL.pdf">Guaranteed Maximum Price (GMP) - Advanced Manufacturing Building, Osceola County</a> 
                           </li>
                           	
                        </ul>
                        		
                        <h4>Policy Adoptions</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><strong><a href="/about/about-valencia/documents/Policy6Hx283C-12-RetirementIncentivePrograms-Sep232015.pdf">Policy 6Hx28: 3C-12 - Retirement Incentive Programs</a></strong></li>
                           
                           <li><strong><a href="/about/about-valencia/documents/BoardApprovalLetter-PolicyAdoption-6Hx282-01-DiscrimationHarassmentRelatedMisconduct-Sep2320.pdf">Policy 6Hx28: 2-01 - Discrimination, Harassment and Related Misconduct</a></strong></li>
                           
                           <li><strong><a href="/about/about-valencia/documents/BoardApprovalLetter-PolicyAdoption-6Hx288-10-Sep232015.pdf">Policy 6Hx28: 8-10 - Student Academic Dispute and Administrative Complaint Resolution</a></strong></li>
                           
                        </ul>
                        
                        <h4>Policy Repeals</h4>
                        
                        <ul class="list-style-1">
                           
                           <li><strong><a href="/about/about-valencia/documents/BoardApprovalLetter-PolicyAdoption-6Hx285-12-Sep232015.pdf">Policy 6Hx28: 5-12 - Other Insurance (College Property Liability, and Workers Compensation)
                                    </a></strong></li>
                           
                        </ul>
                        	
                        <ul class="list-style-1">
                           
                           <li><a href="/about/about-valencia/documents/201509DBOTSPOTSURVEYPoincianaCampus-FINAL.pdf">Spot Survey - Poinciana Campus</a></li>
                           
                           <li><a href="/about/about-valencia/documents/BoardTransmittalLetter-InternalAuditor-ComplianceFunction-Sep232015.pdf">Scope and Hiring of Valencia College Audit/Compliance Position</a></li>
                           
                           <li><a href="/about/about-valencia/documents/AdditionsDeletionsModificationsofCoursesandPrograms-Sep232015.pdf">Additions, Deletions or Modifications of Courses &amp; Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/NewItems-ContinuingEducationCoursesandFees-September232015.pdf">Continuing Education Courses &amp; Fees</a></li>
                           
                           <li><a href="/about/about-valencia/documents/HumanResourcesAgenda-Sep232015.pdf">Human Resources Agenda</a></li>
                           
                           <li><a href="/about/about-valencia/documents/SubmissionofGrantProposals-Sep232015.pdf">Submission of Grant Proposals</a></li>
                           
                           <li><a href="/about/about-valencia/documents/PropertyDelete09-23-15.pdf">Property Deletions </a></li>
                           
                           <li>Board Comments</li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        	
                        <ul class="list-style-1">	
                           
                           <li><a href="/about/about-valencia/documents/DBOT_Fall_2015_Enrollment_ReportFINAL_000.pdf">Fall Enrollment Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/FinancialReportSept2015.pdf">Financial Report</a></li>
                           
                           <li><a href="/about/about-valencia/documents/ConstructionReportSeptember2015.pdf">Construction Report</a></li>
                           
                           <li>Faculty Council Report</li>
                           
                           <li><a href="/about/about-valencia/documents/valenciafoundationtrusteereportsept2015_000.pdf">Valencia Foundation Report</a> 
                           </li>
                           	
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/DBOT-2015-09-23.pcf">©</a>
      </div>
   </body>
</html>