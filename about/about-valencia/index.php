<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>About Us  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>About Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <p>At Valencia we never stop asking the big questions about why we exist and what is
                           our purpose. As we strive to have our students
                           learn as much as possible for as little as possible, we constantly look back at our
                           history and mission to make sure we're on the
                           right track. These documents aren't just words to us, they help explain who we are
                           and why we're here.
                        </p>
                        
                        
                        <h3>Valencia Experience</h3>
                        
                        
                        <iframe src="https://www.youtube.com/embed/IJpazqJ7uGU" width="520" height="380"></iframe>
                        
                        <hr>        
                        	
                        
                        <h3>Vision</h3>
                        
                        <p>
                           Valencia is a premier learning college that transforms lives, strengthens community,
                           and inspires individuals to excellence.
                           
                        </p>
                        
                        
                        <h4>Values</h4>
                        
                        <p><strong>Learning</strong> by committing to Valencia's core competencies - Think, Value, Communicate, and Act
                           - and the potential of each person to learn at the highest levels of achievement for
                           personal and professional success.
                        </p>
                        
                        <p><strong>People</strong> by creating a caring, inclusive and safe environment that inspires all people to
                           achieve their goals, share their success and encourage others.
                        </p>
                        
                        <p><strong>Diversity</strong> by fostering the understanding it builds in learning relations and appreciating the
                           dimensions it adds to our quality of life.
                        </p>
                        
                        <p><strong>Access</strong> by reaching out to our communities, inviting and supporting all learners and partners
                           to achieve their goals.
                        </p>
                        
                        <p><strong>Integrity</strong> by respecting the ideas of freedom, civic responsibility, academic honesty, personal
                           ethics, and the courage to act. 
                        </p>
                        
                        
                        
                        <hr>
                        
                        <h3>Mission</h3>
                        
                        <p>Valencia provides opportunities for academic, technical and life-long learning in
                           a collaborative culture dedicated to inquiry, results and excellence.
                        </p>
                        
                        
                        
                        <h3>Strategic Pillars</h3>
                        
                        <p><strong>Build Pathways</strong></p>
                        
                        
                        
                        <ul class="list-style-1">
                           
                           <li>Remove barriers to college.</li>
                           
                           <li> Create connections that raise personal aspirations of students and enable them to
                              achieve their aspirations. 
                           </li>
                           
                           <li>Develop and renew programs.</li>
                           
                        </ul>
                        
                        
                        <p>
                           
                        </p>
                        
                        <p><strong>Learning Assured</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>Create optimal conditions for student learning.</li>
                           
                           <li>Partner with students to improve their contribution to achieving their potential.</li>
                           
                           <li>Close achievement gaps.</li>
                           
                        </ul>
                        
                        
                        <p><strong>Invest in Each Other</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>Strengthen our collaborative institutional culture to foster deep stewardship of our
                              work.
                           </li>
                           
                           <li> Support the professional development, career growth and healthy lives of Valencia's
                              employees
                           </li>
                           
                        </ul>   
                        
                        
                        
                        
                        <p><strong>Partner with the Community</strong></p>
                        
                        
                        
                        <ul class="list-style-1">
                           
                           <li>Cooperate with community partners in meeting students' needs and college goals.</li>
                           
                           <li> Involve the College in meeting the community's needs and goals. </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/index.pcf">©</a>
      </div>
   </body>
</html>