<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>June 28, 2017 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2017-06-28.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>June 28, 2017 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>The District Board of Trustees</h2>
                        					
                        <h3>Agenda and Materials</h3>
                        	
                        <p><strong>June 28, 2017 - Regular Meeting</strong></p>
                        					
                        					
                        <ul class="list-style-1">
                           					
                           <li><a href="/about/about-valencia/documents/Agenda-Regular-Meeting-Osceola-Campus-Jun-28-2017.pdf"><strong>Agenda</strong></a></li>
                           					
                           <li>APPROVAL OF MINUTES - <a href="/about/about-valencia/documents/Regular-Meeting-Minutes-May-24-2017.pdf">May 24, 2017 Regular Meeting</a></li>
                           					
                           <li> Presidents's Report</li>
                           					
                           <li> Public Comment</li>
                           					
                        </ul>
                        					
                        <h4> New Business</h4>
                        	
                        <ul class="list-style-1">				
                           	
                           <li> <a href="/about/about-valencia/documents/Operating-Budget-2017-18-Jun-28-2017.pdf">Operating Budget 2017-2018</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Capital-Improvement-Program-Jun-28-2017.pdf"> Capital Improvement Program (CIP) - Fiscal Years  2018/2019 - 2022/2023</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Ed-Plant-Survey-Jun-28-2017.pdf">Educational Plant Survey</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Board-Transmittal-Letter-Downtown-Campus-Dev-fees-Jun-28-2017.pdf">Approval of Design/Development Fees for Valencia Academic Center at Creative Village</a></li>
                           					
                           <li> <a href="/about/about-valencia/documents/Approval-of-School-of-Public-Safety-Master-Plan-Design-Jun-28-2017.pdf">Approval of School of Public Safety Master Plan Design</a></li>
                           					
                           <li> President's Performance Evaluation</li>
                           					
                           <li> President's Employment Contract </li>
                           					
                           <li><a href="/about/about-valencia/documents/Approval-of-the-AS-Degree-in-Residential-Property-Management-Jun-28-2017.pdf">Approval of the Associate  in Science Degree in Residential Property Management</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Delinquent-Account-Charge-Off-Jun-28-2017.pdf"> Delinquent Account Charge-Off</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Human-Resources-Agenda-Jun-28-2017.pdf"> Human Resources Agenda</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Additions-Deletions-Modifications-of-Courses-and-Programs-Jun-28-2017.pdf"> Additions, Deletions or Modifications of Courses &amp;  Programs </a></li>
                           					
                           <li> <a href="/about/about-valencia/documents/New-Business-Continuing-Education-Courses-and-Fees-Jun-28-2017.pdf">Continuing Education Courses &amp; Fees</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Submission-of-Grant-Proposals-Jun-28-2017.pdf"> Submission of Grant Proposals</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Property-Deletions-Jun-28-2017.pdf"> Property Deletions</a></li>
                           					
                           <li>Board Comments</li>
                           	
                        </ul>
                        
                        <h4>Reports</h4>
                        					
                        					
                        <ul class="list-style-1">
                           					
                           <li> <a href="/about/about-valencia/documents/Osceola-Campus-Report-Jun-28-2017.pdf">Osceola Campus Report </a></li>
                           					
                           <li> <a href="/about/about-valencia/documents/Osceola-Campus-SGA-Report-Jun-28-2017.pdf">Osceola Campus SGA Report </a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Audit-Quarterly-Update-Jun-28-2017.pdf"> 2016-2017 Audit Update </a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Financial-Report-Jun-28-2017.pdf"> Financial Report</a></li>
                           					
                           <li><a href="/about/about-valencia/documents/Construction-Report-Jun-28-2017.pdf"> Construction Report</a></li>
                           					
                           <li> Faculty Council Report </li>
                           					
                           <li> <a href="/about/about-valencia/documents/Valencia-Foundation-Report-Jun-28-2017.pdf">Valencia Foundation Report </a></li>
                           					
                        </ul>
                        	
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2017-06-28.pcf">©</a>
      </div>
   </body>
</html>