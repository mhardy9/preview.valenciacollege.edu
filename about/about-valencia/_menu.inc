
<ul>
<li class="submenu">
<a href="/about/about-valencia/index.php" class="show-submenu">About Us <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
<ul>
<li><a href="/about/about-valencia/facts.php">Facts</a></li>
<li><a href="/about/about-valencia/history.php">History</a></li>
<li><a href="/about/about-valencia/roles.php">Roles</a></li>
<li><a href="/about/about-valencia/workforce-development.php">Workforce Development</a></li>
</ul>
</li>
<li><a href="/about/about-valencia/administration.php" class="show-submenu">Administration</a></li>
<li>
<a href="/about/about-valencia/board-of-trustees.php" class="show-submenu">Board of Trustees <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
<ul>
<li><a href="/about/about-valencia/trusteeschedule.php">Board of Trustees Schedule</a></li>
</ul>
</li>
<li><a href="/about/about-valencia/awards-recognition.php">Awards and Recognition</a></li>
</ul>

