<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Awards, Rankings and Recognition  | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/awards-recognition.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>Awards, Rankings and Recognition </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_1">
                           
                           <div class="indent_title_in">
                              
                              <h2>Awards, Rankings and Recognition</h2>
                              
                              <p>(Compiled January 6, 2014 MT - Updated August 25, 2014 CB)</p>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p></p>
                              
                              <ul class="list-style-1">
                                 
                                 <li><a href="#Initiatives">Education Initiatives</a></li>
                                 
                                 <li><a href="#Rankings">Education Rankings</a></li>
                                 
                                 <li><a href="#Community">Community</a></li>
                                 
                                 <li><a href="#Operations">Operations</a></li>
                                 
                                 <li><a href="#Programs">Degree Programs</a></li>
                                 
                                 <li><a href="#Staff">Faculty, Staff and Administration</a></li>
                                 
                              </ul>
                              
                              <h3 id="Initiatives">Education Initiatives</h3>
                              
                              <p><strong><a href="http://news.valenciacollege.edu/valencia-today/valencia-coalition-of-colleges-win-2-9-million-grant-to-train-laser-fiber-optic-technicians/" target="_blank">National Science Foundation grant for laser and fiber optics training</a></strong> <br> In
                                 Sept. 2013, Indian River State College, along with Valencia College and eight other
                                 colleges, was awarded
                                 a $2.9 million grant from the NSF to establish a regional laser and fiber optics training
                                 center.
                                 
                              </p>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/valencia-partners-with-seminole-state-lake-sumter-colleges-to-win-1-5-million-stem-grant/" target="_blank"><strong>National Science Foundation grant for minority students in STEM</strong></a>
                                 <br> In Aug. 2013, Valencia, along with Seminole State College and Lake-Sumter State College,
                                 was awarded
                                 the three-year, $1.5 million Louis Stokes Alliances for Minority Participation (LSAMP)
                                 “Bridges to
                                 the Baccalaureate Alliances” grant by the NSF to increase the number of minority students
                                 moving
                                 into majors in science, technology, engineering and math (STEM)&nbsp;when they transfer
                                 to four-year
                                 universities.
                              </p>
                              
                              <p><strong>Achieving the Dream</strong> <br> From 2009 to 2012, Valencia served as a “Leader College”
                                 for the Achieving the Dream Foundation.
                              </p>
                              
                              <p><strong>Leah Meyer Austin Institutional Student Success Leadership Award</strong> <br> In 2008, Valencia
                                 was selected as the first winner of this award, given by Achieving the Dream, because
                                 of excellent
                                 execution of data-informed initiatives to close performance gaps among students from
                                 different ethnic and
                                 economic backgrounds.
                              </p>
                              <br>
                              
                              <h3 id="Rankings">Education Rankings</h3>
                              
                              <p><strong><a href="http://news.valenciacollege.edu/about-valencia/orlando-sentinel-valencia-named-one-of-nations-top-50-colleges/" target="_blank">Top 50 List</a></strong> <br> Valencia named to Washington Monthly's list of the top 50
                                 community colleges in the nation (2013).
                                 
                              </p>
                              
                              <p><strong><a href="http://news.valenciacollege.edu/about-valencia/valencia-ranks-4th-in-nation-for-number-of-associate-degrees-2/" target="_blank">Top Certificate and Degree Rankings, 2013</a></strong> <br> In Community College Week's
                                 latest rankings (for the 2012– 2013 academic year), Valencia was at the top of the
                                 national list in
                                 several categories, including:
                              </p>
                              
                              <ul class="list-style-1">
                                 
                                 <li>2nd in the number of one-year certificates awarded.</li>
                                 
                                 <li>4th in the number of associate degrees awarded.</li>
                                 
                                 <li>5th in the number of associate degrees awarded to Hispanic students.</li>
                                 
                                 <li>7th in the number of associate degrees awarded to African-American students.</li>
                                 
                              </ul>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/about-valencia/valencia-college-makes-top-100-colleges-for-hispanics-list/" target="_blank"><strong>Valencia Makes the Top 100 Colleges for Hispanics list</strong></a> <br> The
                                 Hispanic Outlook in Higher Education magazine named Valencia to the list in 2013 based
                                 on the college's
                                 ranking as 17th in the nation for the number of degrees awarded to Hispanic college
                                 students.
                                 
                              </p>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/about-valencia/directconnect-program-among-nations-best-for-increasing-hispanic-college-success/" target="_blank"><strong>DirectConnect Named Top Program for Latino Success </strong></a> <br> In Oct.
                                 2012, Valencia College's DirectConnect to UCF program was selected by Excelencia in
                                 Education, a
                                 Washington, D.C.-based organization, as America's top program for increasing academic
                                 opportunities and
                                 success for Latino students at the associate level.
                              </p>
                              
                              <p><a href="http://news.valenciacollege.edu/academic-issues/valencia-named-top-community-college-in-nation/" target="_blank"><strong>Winner of the inaugural Aspen Prize for Community College Excellence</strong></a>
                                 <br> In 2011, the Aspen Institute named Valencia the top community college in the nation
                                 after a year-long
                                 effort to recognize excellence in the nation's 1,200 community colleges, based on
                                 student performance and
                                 graduation data collected by the U.S. Department of Education.
                              </p>
                              <br>
                              
                              <h3 id="Community">Community</h3>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/bridges-program-wins-beta-center-award-for-its-impact-on-the-lives-of-teen-moms-and-families/" target="_blank"><strong>Family Impact Award</strong></a> <br> Valencia's Bridges to Success Program
                                 received the 2013 Family Impact Award from Orlando's BETA Center, a nonprofit agency
                                 that helps at-risk
                                 families, pregnant teenagers and teen moms.
                              </p>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/valencia-college-named-military-friendly-school-for-second-year/" target="_blank"><strong>Valencia named a “military-friendly school”</strong></a> <br> In
                                 2013, Valencia named a “military-friendly school” for the second year in a row. The
                                 designation is awarded to the top 20 percent of colleges, universities and trade schools
                                 in the country
                                 that are doing the most to embrace military students and ensure their success in the
                                 classroom and after
                                 graduation.
                              </p>
                              <br>
                              
                              <h3 id="Operations">Operations</h3>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/valencia-wins-green-building-award-for-west-campus-university-center/" target="_blank"><strong>Environmentally-Friendly Building Awards</strong></a> <br> In Jan. 2103, the
                                 United States Green Building Council's Central Florida Chapter awarded Valencia College
                                 a “Building
                                 of the Year LEEDership Award” for the college's University Center, a joint-use facility
                                 that
                                 Valencia shares with University of Central Florida (UCF) on Valencia's West Campus.
                              </p>
                              
                              <p>In 2012, the Lake Nona Campus and Building 10 on West Campus were awarded Level 3
                                 Green Globes
                                 certification by the Green Building Initiative (GBI).&nbsp; 
                              </p>
                              
                              <p><strong>Winner of the National Purchasing Institute's Achievement of Excellence in Procurement
                                    Award.</strong> (2011)
                              </p>
                              <br>
                              
                              <h3 id="Programs">Degree Programs</h3>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/valencia-sponsors-conference-on-cybersecurity-education/" target="_blank">Computer Engineering Technology </a> <br> The National Security Agency and the U.S.
                                 Department of Homeland Security have designated Valencia College as a National Center
                                 of Academic
                                 Excellence in Information Assurance 2-Year Education for the years 2012-2017.
                              </p>
                              
                              <p><strong>Culinary</strong> <br> Valencia's Culinary Management team won gold medal in the state American
                                 Culinary Federation competition and a silver medal in the ACF regional competition
                                 (fall 2011 – spring
                                 2012). 
                              </p>
                              
                              <p><strong><a href="http://news.valenciacollege.edu/valencia-today/phoenix-magazine-wins-big-again/" target="_blank">English</a></strong> <br> Valencia's student-produced literary magazine,
                                 “Phoenix,” is an award-winning publication, having received first place in the Florida
                                 College
                                 System Publications Association's completion for four years in a row.
                              </p>
                              
                              <p><strong>Film</strong> <br> Valencia's Film Production Technology students have worked on-set for
                                 professional movies, including 2012's “Renee,” starring Kat Dennings. In 2011, International
                                 Cinematographers Guild Magazine named the program one of “Super Six Cinema Education
                                 Programs.”
                                 
                              </p>
                              
                              <p><strong><a href="http://valenciagraphicdesign.com/category/achievements/" target="_blank">Graphic
                                       Design</a></strong> <br> Valencia students routinely sweep the ADDY Awards, an advertising and design
                                 competition. Within the student category, we have had local gold winners and a national
                                 silver winner.
                              </p>
                              
                              <p><strong>Nursing</strong> <br> Valencia's Nursing R.N. students consistently score above 90 percent on the
                                 national licensing exam for registered nurses—higher than state and national averages.
                              </p>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/making-a-big-bang-valencia-students-win-second-place-in-international-recording-competition/" target="_blank"><strong>Sound and Music Technology</strong></a> <br> In Nov. 2013, two Valencia students
                                 won second place in the “Sound for Visual Media”
                                 category of the student competition at the annual convention of the Audio Engineering
                                 Society, an
                                 international sound-recording competition held in New York. <br></p>
                              <br>
                              
                              <h3 id="Staff">Faculty, Staff and Administration</h3>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/about-valencia/president-of-valencias-east-and-winter-park-campuses-named-to-united-arts-board-of-directors/" target="_blank"><strong>United Arts of Central Florida</strong></a> <br> In 2013, Dr. Stacey R. Johnson,
                                 president of Valencia College's East and Winter Park campuses, was named to the board
                                 of directors of
                                 United Arts of Central Florida, the umbrella organization that supports more than
                                 50 Central Florida arts
                                 and cultural groups.
                              </p>
                              
                              <p><a href="http://news.valenciacollege.edu/valencia-today/mullowney-elected-nacua-board-chair/" target="_blank"><strong>National Association of College and University Attorneys</strong></a> <br> In
                                 2013, Valencia College Vice President for Policy and General Counsel William J. (Bill)
                                 Mullowney has been
                                 elected chair of the Board of Directors of the National Association of College and
                                 University Attorneys
                                 (NACUA).
                              </p>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/valencia-professor-named-one-of-nations-top-innovators/" target="_blank"><strong>Top 40 Innovators in Education</strong></a> <br> In 2013, Dr. James May, who
                                 teaches English as a Second Language at Valencia College, has been named one of the
                                 Top 40 Innovators in
                                 Education by the Center for Digital Education (CDE). May is known for pioneered the
                                 use of cell phones and
                                 computer-assisted learning in the classroom.
                              </p>
                              
                              <p>
                                 <a href="http://news.valenciacollege.edu/valencia-today/osceola-president-named-outstanding-female-of-the-year-in-biz-journals-40-under-40/" target="_blank"><strong>Outstanding Female of the Year</strong></a> <br> Kathleen Plinske, Osceola
                                 Campus President, Named Outstanding Female of the Year by Orlando Business Journal
                                 (2012)
                              </p>
                              
                              <p><strong>Recognition for Dr. Sanford Shugart, President, Valencia College</strong></p>
                              
                              <ul class="list-style-1">
                                 
                                 <li>Named to Orlando Life magazine's 2013 list of “Indelibles.”</li>
                                 
                                 <li>Named to<a href="http://www.orlandosentinel.com/news/politics/os-scott-maxwell-orlandos-25-most-powerful-123012-20121229,0,318997.column" target="_blank"> Orlando Sentinel columnist Scott Maxwell's 2013 list of Central Florida's 25 most
                                       powerful people</a>.
                                    
                                 </li>
                                 
                                 <li>Awarded a<a href="http://news.valenciacollege.edu/about-valencia/ucf-awards-valencia-president-honorary-doctorate/" target="_blank"> Doctor of Humane Letters honorary doctorate degree by the University of Central
                                       Florida</a> in 2012.
                                    
                                 </li>
                                 
                              </ul>
                              
                              <p>&nbsp;</p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/awards-recognition.pcf">©</a>
      </div>
   </body>
</html>