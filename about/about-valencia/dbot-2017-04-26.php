<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>April 26, 2017 - Regular Meeting | Valencia College</title>
      <meta name="Description" content="Valencia is a premier learning college that transforms lives, strengthens community, and inspires individuals to excellence.">
      <meta name="Keywords" content="valencia college, about us, facts, administration, awards, vision, mission, valenciacollege.edu">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/about-valencia/dbot-2017-04-26.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/about-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>About Us</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/about-valencia/">About Valencia</a></li>
               <li>April 26, 2017 - Regular Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>The District Board of Trustees</h2>
                        		
                        <h3>Agenda and Materials</h3>
                        	
                        <p><strong>April 26, 2017 - Regular Meeting</strong></p>
                        	
                        <p>
                           	
                           <ul class="list-style-1">
                              		
                              <li><a href="/about/general-counsel/policy/historical/Agenda-RegularMeeting-LakeNonaCampus-Apr262017.pdf"><strong>Agenda</strong></a> 
                              </li>
                              	
                              <li><a href="/about/general-counsel/policy/historical/RegularMeetingMinutes-Feb222017.pdf">APPROVAL OF MINUTES - February 22, 2017 Regular Meeting </a></li>
                              	
                              <li><a href="/about/general-counsel/policy/historical/PresidentReport-FETPIPFindings-Apr262017.pdf">PRESIDENT'S REPORT </a></li>
                              	
                              <li>Public Comment</li>	
                           </ul>
                        </p>
                        	
                        <h4>New Business</h4>
                        	
                        <ul class="list-style-1">
                           	
                           <li><a href="/about/general-counsel/policy/historical/REStrategyvFinalBOT-Apr262017.pdf">Strategic Conversation: Valencia College's Property Strategy</a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/OsceolaCampusBldg11FundReallocation-Apr262017.pdf">Request to Reallocate Funds in Fund 1 to Fund 7 – Building of CAT and Jobs Building
                                 11 on Osceola</a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/EastCampus1BRenovation-Apr262017.pdf"><strong>Fund Allocation Request - Renovation of Building 1B - East Campus </strong></a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/DowntownCampusBuildout-Apr262017.pdf">Fund Allocation Request - 2019 Downtown Campus - Tenant Improvement </a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/CanvasSoftwarePurchase-Apr262017.pdf">Canvas Software Purchase</a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/2016-2017ValenciaCollegeEquityReport-Apr262017.pdf">2016-2017 Annual Equity Update</a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/NewBusiness-ContinuingEducationCoursesandFees-Apr262017.pdf">Continuing Education Courses &amp; Fees</a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/HumanResourcesAgenda-Apr262017.pdf">Human Resources Agenda</a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/SubmissionofGrantProposals-Apr262017.pdf">Submission of Grant Proposals</a></li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/PropertyDeletions-Apr262017.pdf">Property Deletions </a></li>
                           	
                           <li>Board Comments</li>
                           	
                           <li><a href="/about/general-counsel/policy/historical/DBOTTrusteeCrossmanPPT04262017.pdf">Trustee Crossman Presentation</a></li>
                           	
                        </ul>
                        	
                        <h4>Reports</h4>
                        		
                        <p>
                           <ul class="list-style-1">
                              	
                              <li><a href="/about/general-counsel/policy/historical/LakeNonaBiotechLabSciencesReport-Apr262017.pdf">Lake Nona Campus Report</a></li>
                              	
                              <li><a href="/about/general-counsel/policy/historical/LakeNonaCampusSGAReport-Apr262017.pdf">Lake Nona SGA Report</a></li>
                              	
                              <li><a href="/about/general-counsel/policy/historical/FinancialReport-Apr262017.pdf">Financial Report</a></li>
                              	
                              <li><a href="/about/general-counsel/policy/historical/ConstructionReportApril2017.pdf">Construction Report</a></li>
                              	
                              <li>Faculty Council Report</li>
                              	
                              <li><a href="/about/general-counsel/policy/historical/ValenciaFoundationReport-Apr262017.pdf">Valencia Foundation Report </a></li>
                              	
                           </ul>
                        </p>
                        	  
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/about-valencia/dbot-2017-04-26.pcf">©</a>
      </div>
   </body>
</html>