<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Governance Councils</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li>Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Governance Resources</h2>
                        
                        
                        <p>Most resources are in PDF format.</p>
                        
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/governance-glossary.pdf" target="_blank">Governance Glossary</a></li>
                           
                           <li><a href="/documents/about/governance/governance-diagram-1211.pdf" target="_blank">Governance Diagram</a></li>
                           
                           <li><a href="/documents/about/governance/taxonomy-contiuum-horiz.pdf" target="_blank">Governance Taxonomy</a></li>
                           
                           <li><a href="/documents/about/governance/GovernanceCouncilsMatrix.pdf">Governance Council Matrix</a></li>
                           
                           <li><a href="/documents/about/governance/collaborativeskillslesson1.pdf" target="_blank"> Collaborative Discussion Skills PowerPoint<br></a></li>
                           
                           <li><a href="/documents/about/governance/SkillfulDisagreementPowerpoint.pptx">Skillful Disagreement PowerPoint<br></a></li>
                           
                           <li><a href="/documents/about/governance/llcteamtraining3-listening.ppt">How to Listen PowerPoint</a></li>
                           
                        </ul>
                        
                        <h3>Historical Documents</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/GovernanceReviewInquiryReport-2011.pdf" target="_blank">Governance Review Inquiry Report - 2011</a></li>
                           
                           <li><a href="/documents/about/governance/SharedGovernanceRedesignRetreatReport-July2013.pdf" target="_blank">Shared Governance Redesign Retreat Report - July 2013</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/resources.pcf">©</a>
      </div>
   </body>
</html>