<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Leadership Council Work Plans  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/learning-leadership-council/work-plans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Governance Councils</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li><a href="/about/governance/learning-leadership-council/">Learning Leadership Council</a></li>
               <li>Learning Leadership Council Work Plans </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Learning Leadership Council Work Plans</h2>
                        
                        
                        <h3>Work In Progress </h3>
                        
                        <p><strong>WORK PLANS IN PROGRESS</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <a href="/documents/about/governance/learning-leadership-council/onlinelearningplan-yearoneimplementationplan.pdf" target="_blank">Online Learning Year One Implementation Plan - September 2016 </a>
                              
                              <ul class="list-style-1">
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlinelearningplanupdate-septfinal.ppt" target="_blank">Online Learning Plan - Updated   September 2016 </a></li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlinelearningplan-lccnovember.ppt" target="_blank">Online Learning Plan - Updated November 2015</a></li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/valenciacollegedistancelearningassessment-reportfinal6-10-2013.pdf" target="_blank">Valencia College Distance Learning Report - DM&amp;A 2013</a></li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlinedata-initialreport-llcdraft.pdf" target="_blank">Online Data Report - January 2015</a></li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlinelearningplan-enhancingquality-llcdraft.pdf" target="_blank">Online Learning Plan - March 2015</a></li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlineworkteamsroster.pdf" target="_blank">Online Learning Teams Roster</a></li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlinelearningstrategicplanningteamroster.pdf" target="_blank">Online Learning Strategic Planning Team Roster</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Online Data and Evaluation Team: 
                              <a href="/documents/about/governance/learning-leadership-council/workproposalforonlinedataandevaluationteam-llcdraft.pdf" target="_blank">Work Proposal</a>
                              
                           </li>
                           
                           <li>Student Feedback of Instruction: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/SFILadderofAbstractionLLC.pptx" target="_blank">Presentation</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/SFIWorkPlan.docx" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/FinalSummativeAssessment.docx" target="_blank">Summative Assessment</a>, 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/FinalFormativeAssessment.docx" target="_blank">Formative Assessment</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/FinalImplicitBiasSFI.docx" target="_blank">Implicit Bias</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Undergraduate Research: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/undergraduateresearchworkproposal.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/undergraduateresearchrecommendations.pdf" target="_blank">Recommendations</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/UndergradResearchFeedback.docx" target="_blank">Feedback from LLC</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/undergraduateresearch-finalreport_050516apa2.pdf" target="_blank">Final Report</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/undergraduateresearchllcupdate1_18_17.pdf">Update - January 2017</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/work-proposal-part-time-and-annually-appointed-faculty-engagement.pdf" target="_blank">Part-time  and Annually Appointed New Faculty Experience Work Team Proposal </a></li>
                           
                        </ul>
                        
                        
                        <h3>Completed Work Plans </h3>
                        
                        <p><strong>COMPLETED WORK (OR WORK IN THE IMPLEMENTATION PHASE)</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>Hybrid Design Team: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/workproposalforhybriddesign.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/Draft-RecommendationsfromHybridWorkTeam.docx" target="_blank">Recommendations</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/HybridWorkTeamReviewofRecommendations.pptx" target="_blank">Presentation - March 2017</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Student Preparedness for Online Learning: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/workproposalforonlinestudentpreparedness.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/actionplanforstudentreadinessforonlinelearning32515.pdf" target="_blank">Recommendations</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlinestudentreadinessassessmentplan.pdf" target="_blank">Assessment Plan</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Faculty Preparedness for Online Learning: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/workproposalforfacultypreparedness-llcdraft.pdf" target="_blank">Work Proposal;</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/facultypreparednessworkteamrecommendations.pdf" target="_blank">Recommendations</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/facultypreparednessworkteamcomposition.pdf">Work Team Composition</a> 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Dean and Chair Development in Leadership for Online Learning: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/workproposalfordeanandchairdevelopment-llcdraft.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/deanandchairtrainingworkteamrecommendations.pdf" target="_blank">Recommendations</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Course and Curriculum Design for Online Learning: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/workproposalforcourseandcurriculumdesign-llcdraft.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/courseandcurriculumworkteamrecommendations.pdf" target="_blank">Recommendations</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/courseandcurriculumworkteamrationales.pdf" target="_blank">Rationale </a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Student Services and Support Team for Online Learning: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/workproposalforonlinestudentservicesandsupport-llcdraft.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/onlinestudentservicesandsupportworkteamfinalreport.pdf">Recommendations</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/onlinestudentservicesandsupportimplementationguide.pdf" target="_blank">Implementation Plan</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Essential Competencies of a Valencia Educator: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/approvedessentialcompetenciesworkplan-november2014.pdf" target="_blank">Work Plan</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/essentialcompetenciesrecommendations.pdf" target="_blank">Recommendations</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>New Student Experience (NSE) Front Door Alignment: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/nsefrontdooralignmentworkproposal.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/nsefrontdooralignmentworkplan.pdf">Work Plan</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Internship &amp; Workforce Services Redesign: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/workproposalforiwsreview.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/internshipworkforceservicesreviewworkplan.pdf">Work Plan</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/report-redesignofiws-final-9.8.2015.pdf" target="_blank">Final Report</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/recommendationsfromworkteam-redesign9.4.15.pdf">Recommendations</a> 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Incentive Components of the Faculty Compensation Plan: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/2015incentiveplanandfeedbackresponsememo.pdf" target="_blank">Revised Proposal;</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/incentivecomponentsofthefacultycompensationplanworkplanforllc2.11.15.pdf" target="_blank"> Work Plan</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/recommendationsontheincentiveplan.pdf" target="_blank">Recommendations</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/2015incentiveplanandfeedbackresponsememo.pdf" target="_blank"> </a></li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/Incentiveplandraftv8a.docx" target="_blank"> Incentive Plan - 2015 </a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Academic Initiative Review (AIR) for LinC: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/airproposalforlincinitiative.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/airtimelineforactivities-lincinitiative.pdf" target="_blank">Timeline</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/airfinalreport.pdf" target="_blank">Final Report with Recs</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/strategydevelopmentforlinc.pdf" target="_blank">Strategies for LinC</a>; 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/AIRforLinCUpdate.pptx" target="_blank">Update - January 2017</a> 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Academic Initiative Review (AIR) for SL: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/SupplementalLearning-AIR.pptx" target="_blank">Presentation</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/SLAIRReport-November2016.docx" target="_blank">Final Report with Recommendations</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Work On Hold</h3>
                        
                        <p><strong>WORK ON HOLD OR NOT ENDORSED</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>Vendor Selection Team for Instructional Materials Pilot: <a href="/documents/about/governance/learning-leadership-council/workproposalvendorselectionteamforimpilot.pdf" target="_blank">Work Proposal </a>
                              
                           </li>
                           
                           <li>Online Proctoring: 
                              
                              <ul class="list-style-1">
                                 
                                 <li>
                                    <a href="/documents/about/governance/learning-leadership-council/onlineproctoringworkproposal.pdf" target="_blank">Work Proposal</a>; 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/learning-leadership-council/OnlineProctoringDemo.pptx" target="_blank">Online Proctoring Demo</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/learning-leadership-council/work-plans.pcf">©</a>
      </div>
   </body>
</html>