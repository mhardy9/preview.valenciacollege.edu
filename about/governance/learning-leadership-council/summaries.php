<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Leadership Council Meeting Summaries  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/learning-leadership-council/summaries.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Governance Councils</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li><a href="/about/governance/learning-leadership-council/">Learning Leadership Council</a></li>
               <li>Learning Leadership Council Meeting Summaries </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Learning Leadership Council Meeting Summaries</h2>
                        
                        
                        <h3>2017</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/LLC-Meeting-Summary-7-28-17.pdf" target="_blank">Learning  Leadership Council Meeting Summary - 7/28/17</a></li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llc-meeting-summary-5-17-17.pdf" target="_blank">Learning  Leadership Council Meeting Summary - 5/24/17</a></li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary32217.pdf" target="_blank">Learning Leadership Council Meeting Summary - 3/22/17 </a></li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary11817.pdf" target="_blank">Learning Leadership Council Meeting Summary - 1/18/17 </a></li>
                           
                        </ul>
                        
                        
                        <h2>2016</h2>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary111616.pdf" target="_blank">Learning Leadership Council Meeting Summary - 11/16/16</a></li>
                           
                           <li>
                              <a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary51816.pdf" target="_blank">Learning Leadership Council Meeting Summary - 5/18/16</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary12016.pdf" target="_blank">Learning Leadership Council Meeting Summary - 1/20/16</a></li>
                           
                        </ul>
                        
                        
                        <h3>2015</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary11.4.15.pdf" target="_blank">Learning Leadership Council Meeting Summary - 11/4/15 </a></li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary91615.pdf" target="_blank">Learning Leadership Council Meeting Summary - 9/16/15</a></li>
                           
                           <li>
                              <a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary6315.pdf" target="_blank">Learning Leadership Council Meeting Summary - 6/3/15</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary4.1.15.pdf" target="_blank">Learning Leadership Council Meeting Summary - 4/1/15</a></li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary2.4.15.pdf" target="_blank">Learning Leadership Council Meeting Summary - 2/4/15 </a></li>
                           
                        </ul>
                        
                        
                        <h3>2014</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary11.6.14.pdf" target="_blank">Learning Leadership Council Meeting Summary - 11/6/14</a></li>
                           
                           <li><a href="/documents/about/governance/learning-leadership-council/llcmeetingsummary9.4.14.pdf" target="_blank">Learning Leadership Council Meeting Summary - 9/4/14</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/learning-leadership-council/summaries.pcf">©</a>
      </div>
   </body>
</html>