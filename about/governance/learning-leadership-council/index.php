<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Leadership Council  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/learning-leadership-council/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Governance Councils</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li>Learning Leadership Council</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">              
                        
                        <h2>Learning Leadership Council</h2>
                        
                        
                        <p>The <strong>Learning Leadership  Council (LLC) </strong>is responsible for leading the college's work towards achievement of the core mission
                           - learning. The Council meets regularly to consider the state of the college's progress
                           in achieving its core mission, to discuss potential major initiatives to advance the
                           work. The Learning Leadership Council bears chief responsibility for identifying a
                           select number of major initiatives and commissioning collaborative work teams to design
                           the implementation of these initiatives.
                        </p>
                        
                        
                        <p>Comprised of a broad cross-section of college leaders who work most closely with the
                           core mission and strategy, the LLC is co-chaired by the VP of Academic Affairs &amp; Planning
                           and the immediate past-chair of the Faculty Council.
                        </p>
                        
                        
                        <h3>Council Officers 2017-18</h3>
                        
                        <p>Susan Ledlow, Co-Chair<br>
                           VP of Academic Affairs &amp; Planning 
                        </p>
                        
                        
                        <p>Neal  Phillips, Co-Chair<br>
                           Past-Chair  of Faculty Council<br>
                           Professor of  English
                        </p>
                        
                        
                        <p>Kari Makepeace, Council Coordinator<br>
                           Coordinator of Academic Planning &amp; Support 
                        </p>
                        
                        
                        <h3>Past Council Officers</h3>
                        
                        <p>2016-17: Suzette Dohany, Professor, Speech<br>
                           2015-16: Carl Creasman, Professor of History
                        </p>
                        
                        
                        
                        <h3>Schedule</h3>
                        
                        <p>Please note that these meeting dates, times, and locations are subject to change.</p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Meeting Dates</th>
                                 
                                 <th>Meeting Times </th>
                                 
                                 <th>Location</th>
                                 
                                 <th>Meeting Type </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>September 20, 2017</td>
                                 
                                 <td>2-5pm</td>
                                 
                                 <td>Lake Nona Campus<br>
                                    Room 148
                                 </td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>November 15, 2017</td>
                                 
                                 <td>2-5pm </td>
                                 
                                 <td>Winter Park Campus<br>
                                    Room 225-226
                                 </td>
                                 
                                 <td>Regular Meeting </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>January 17, 2018</td>
                                 
                                 <td>2-5pm </td>
                                 
                                 <td>East Campus<br>
                                    5-112
                                 </td>
                                 
                                 <td>Regular Meeting </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 21, 2018</td>
                                 
                                 <td>2-5pm </td>
                                 
                                 <td>Osceola Campus<br>
                                    4-105
                                 </td>
                                 
                                 <td>Regular Meeting </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>May 16, 2018</td>
                                 
                                 <td>2-5pm </td>
                                 
                                 <td>West Campus<br>
                                    11-106
                                 </td>
                                 
                                 <td>Regular Meeting </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/learning-leadership-council/index.pcf">©</a>
      </div>
   </body>
</html>