<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Leadership Council Membership Roster  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/learning-leadership-council/roster.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Governance Councils</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li><a href="/about/governance/learning-leadership-council/">Learning Leadership Council</a></li>
               <li>Learning Leadership Council Membership Roster </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Learning Leadership Council Membership Roster</h2>
                        
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Member Name</th>
                                 
                                 <th>Job Title </th>
                                 
                                 <th>Member Term</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Susan Ledlow </td>
                                 
                                 <td>Co-Chair, LLC &amp; VP, Academic Affairs &amp;    Planning - District </td>
                                 
                                 <td>Standing Member</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Neal Phillips</td>
                                 
                                 <td>Co-Chair, LLC &amp; Professor, English - West</td>
                                 
                                 <td>07/2017 - 06/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Kari Makepeace </td>
                                 
                                 <td>Coordinator, Academic Planning &amp; Support -    District </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Stacey Johnson </td>
                                 
                                 <td>Campus President - East / Winter Park</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Kathleen Plinske</td>
                                 
                                 <td>Campus President - Osceola / Lake Nona /    Poinciana</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Falecia Williams</td>
                                 
                                 <td>Campus President – West / Downtown</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Joyce Romano </td>
                                 
                                 <td>VP, Educational Partnerships - District </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Joe Battista</td>
                                 
                                 <td>VP, Global, Professional &amp; Continuing    Education - West </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>John Slot</td>
                                 
                                 <td>VP, Information Technology &amp; CIO - West</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Kim Sepich </td>
                                 
                                 <td>VP, Student Affairs - District </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Linda Herlocker </td>
                                 
                                 <td>AVP, Admissions &amp; Records - West </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Nasser Hedayat </td>
                                 
                                 <td>AVP, Career &amp; Workforce Education -    District </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Amy Kleeman </td>
                                 
                                 <td>AVP, College Transitions - West</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Karen Borglum </td>
                                 
                                 <td>AVP, Curriculum &amp; Assessment - District </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Kris Christian </td>
                                 
                                 <td>AVP, Resource Development - District </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Sonya Joseph </td>
                                 
                                 <td>AVP, Student Affairs - Lake Nona </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Wendi Dew </td>
                                 
                                 <td>AVP, Teaching &amp; Learning - District </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Daryl Davis </td>
                                 
                                 <td>Director, Institutional Research - West </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Michelle Foster </td>
                                 
                                 <td>Dean of Academic Affairs - East</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Nick Bekas</td>
                                 
                                 <td>Dean of Academic Affairs - West </td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Landon Shephard </td>
                                 
                                 <td>Dean of Learning Support - Osceola</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jill Szentmiklosi </td>
                                 
                                 <td>Dean of Students - Osceola</td>
                                 
                                 <td>Standing Member </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Al Groccia </td>
                                 
                                 <td>Chair, Faculty Council &amp; Professor, Math -    Osceola </td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Vasudha Sharma</td>
                                 
                                 <td>EC / WPC Faculty Assembly Rep &amp; Professor,    Chemistry - East</td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Diana Ciesko</td>
                                 
                                 <td>EC / WPC Faculty Assembly Rep &amp; Professor,    Psychology - East</td>
                                 
                                 <td>07/2016 - 06/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>April Raneri </td>
                                 
                                 <td>OC / LNC Faculty Assembly Rep &amp; Professor,    Speech - Lake Nona</td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Laura Sessions </td>
                                 
                                 <td>OC / LNC Faculty Assembly Rep &amp; Professor,    Chemistry - Lake Nona</td>
                                 
                                 <td>07/2016 - 06/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Andy Ray </td>
                                 
                                 <td>WC Faculty Assembly Rep &amp; Professor, Bldg.    Const. Tech. - West</td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Ruby Alvarez</td>
                                 
                                 <td>WC Faculty Assembly Rep &amp; Professor, Nursing    - West</td>
                                 
                                 <td>07/2016 - 06/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Terri Daniels </td>
                                 
                                 <td>Instructional Affairs Committee Rep &amp; Exec.    Dean - Winter Park Campus</td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Linda Neal </td>
                                 
                                 <td>Instructional Affairs Committee Rep &amp;    Dean, Communications - East </td>
                                 
                                 <td>07/2016 - 06/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Rise Sandrowitz </td>
                                 
                                 <td>Instructional Affairs Committee Rep &amp;    Dean, Nursing - West</td>
                                 
                                 <td>07/2016 - 06/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Maryam Khan </td>
                                 
                                 <td>Staff Association Rep &amp; Asst. Director, ODHR    East Region - East </td>
                                 
                                 <td>07/2016 - 06/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Mary Beth Thornton </td>
                                 
                                 <td>Staff Association Rep &amp; Exec. Asst. Sr., President’s    Office - East </td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Elvin Cruz-Vargas</td>
                                 
                                 <td>Staff Association Rep &amp; Coord, Foundation Scholarships    &amp; Sp Proj. - District</td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Yaremis Fullana</td>
                                 
                                 <td>Staff Association Rep &amp; Director, Auxiliary    Services - West </td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dan Diehl</td>
                                 
                                 <td>Staff Association Rep &amp; Asst. Director,    Fire Rescue Institute - SPS</td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Michelle Terrell</td>
                                 
                                 <td>Staff Association Rep &amp; Director, Work-Based    Learning - District </td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>David Renteria</td>
                                 
                                 <td>Staff Association Rep &amp; Director, Enterprise App.    Services - West </td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Courtney James</td>
                                 
                                 <td>Staff Association Rep &amp; Paralegal, Policy    &amp; General Counsel - District</td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jeannie Rodriguez</td>
                                 
                                 <td>Staff Association Rep &amp; Library Office Systems    Manager - West </td>
                                 
                                 <td>07/2017 - 06/2019</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/learning-leadership-council/roster.pcf">©</a>
      </div>
   </body>
</html>