<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Governance Councils  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Governance Councils</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Governance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div width="150px"><a href="ec/index.html" title=" ">Executive Council</a></div>
                                                      <br>
                                                      
                                                      <div><a href="../faculty/association/index.html" target="_blank" title="Faculty Council">Faculty Council</a></div>
                                                      <br>
                                                      
                                                      <div><a href="llc/index.html" title=" ">Learning Leadership Council</a></div>
                                                      <br>
                                                      
                                                      <div><a href="coct/index.html" title=" ">Council Officers' Coordination Team</a></div>
                                                      <br>              
                                                      
                                                   </div>
                                                   
                                                   
                                                </div>
                                                
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <div>         
                                                      
                                                      <div><a href="workplan/index.html" title=" ">Work Plans</a></div>
                                                      <br>
                                                      
                                                      <div><a href="resources/index.html" title=" ">Resources</a></div>
                                                      <br>
                                                      
                                                      
                                                      <div><a href="othergroups/index.html" title=" ">Other College-wide Groups</a></div>
                                                      <br>
                                                      
                                                      
                                                      <div><a href="http://net4.valenciacollege.edu/forms/governance/contact.cfm" target="_blank">Contact Us</a></div>
                                                      <br>
                                                      
                                                      
                                                      
                                                   </div> 
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>            
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h3>The Grove Events </h3>
                                       
                                       
                                       <div data-id="http://feedproxy.google.com/~r/TheGroveValenciaCollegeGovernance/~3/X2jCQYSNG20/">
                                          
                                          
                                          <div>
                                             Oct<br>
                                             <span>16</span>
                                             
                                          </div>
                                          
                                          
                                          <div>        
                                             <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeGovernance/~3/X2jCQYSNG20/" target="_blank">Upcoming Meetings</a><br>
                                             
                                             <span>11:00 AM</span>
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       
                                       <div data-id="http://feedproxy.google.com/~r/TheGroveValenciaCollegeGovernance/~3/--tKoiQORKU/">
                                          
                                          
                                          <div>
                                             Oct<br>
                                             <span>11</span>
                                             
                                          </div>
                                          
                                          
                                          <div>        
                                             <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeGovernance/~3/--tKoiQORKU/" target="_blank">A Faculty Heads Up — October 2017</a><br>
                                             
                                             <span>12:45 PM</span>
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       
                                       <div data-id="http://feedproxy.google.com/~r/TheGroveValenciaCollegeGovernance/~3/Xg8S7HMBNfA/">
                                          
                                          
                                          <div>
                                             Sep<br>
                                             <span>28</span>
                                             
                                          </div>
                                          
                                          
                                          <div>        
                                             <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeGovernance/~3/Xg8S7HMBNfA/" target="_blank">Faculty Governance Update, September 2017</a><br>
                                             
                                             <span>12:30 PM</span>
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <a href="http://wp.valenciacollege.edu/thegrove/category/governance/"><strong>View More</strong></a>
                                       
                                    </div>
                                    
                                 </div>      
                                 
                                 
                                 
                                 
                                 
                              </div>          
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/index.pcf">©</a>
      </div>
   </body>
</html>