<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Operations Leadership Team  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/operations-leadership-team/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Operations Leadership Team</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li>Operations Leadership Team</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Operations Leadership Team</h2>
                        
                        
                        <p>The <strong>Operations Leadership Team (OLT)</strong> meets to research, design, and implement solutions to address major operational changes
                           that impact how the college manages its day to day functions, communication regarding
                           operations changes that affect the college, and planning/training for operational
                           changes that cross impact college locations.
                        </p>
                        
                        
                        <h3>Leadership 2017-18</h3>
                        
                        <p>Loren Bender, Chair/Convener<br>
                           VP, Business Operations &amp; Finance
                        </p>
                        
                        
                        <p>Michelle Sever, Co-Chair<br>
                           Director of HR Policy and Compliance Programs 
                        </p>
                        
                        
                        <p>Terry Allcorn, Co-Chair<br>
                           Dean of Business &amp; Hospitality
                        </p>
                        
                        
                        <h3>Past Co-Chairs</h3>
                        
                        <p>
                           <strong>2016-2017</strong>: Jennifer Page, Director of Employment &amp; Onboarding <br>
                           <strong>2015-2016</strong>: Jackie Lasch, AVP Financial Services<br>
                           <strong>2014-2015</strong>: Roger Corriveau, Manager of Campus Operations
                           
                        </p>
                        
                        
                        <h3>Schedule</h3>
                        
                        <p>Please note that these meeting dates, times, and locations are subject to change.</p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Meeting Dates</th>
                                 
                                 <th>Meeting Times</th>
                                 
                                 <th>Location</th>
                                 
                                 <th>Meeting Type </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>August 29, 2017</td>
                                 
                                 <td>2:00-5:00pm</td>
                                 
                                 <td>East Campus, Room 3-113</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>October 10, 2017</td>
                                 
                                 <td>2:00-5:00pm</td>
                                 
                                 <td>Winter Park Campus, Room 225-226</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>December 5, 2017</td>
                                 
                                 <td>2:00-5:00pm</td>
                                 
                                 <td>Lake Nona Campus, Room 148</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>February 6, 2018</td>
                                 
                                 <td>2:00-5:00pm</td>
                                 
                                 <td>District Office, Room 252</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>April 3, 2018</td>
                                 
                                 <td>2:00-5:00pm </td>
                                 
                                 <td>Osceola Campus, Room 4-105</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>June 5, 2018</td>
                                 
                                 <td>2:00-5:00pm </td>
                                 
                                 <td>East Campus, Room 5-112</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/operations-leadership-team/index.pcf">©</a>
      </div>
   </body>
</html>