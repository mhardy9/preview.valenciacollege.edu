<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Operations Leadership Team Membership  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/operations-leadership-team/roster.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Operations Leadership Team</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li><a href="/about/governance/operations-leadership-team/">Operations Leadership Team</a></li>
               <li>Operations Leadership Team Membership </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Operations Leadership Team 2017-2018 Membership</h2>
                        
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>
                                    Member Name
                                    
                                 </th>
                                 
                                 <th>
                                    Job Title
                                    
                                 </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Terry Allcorn</td>
                                 
                                 <td>Co-Chair &amp; Dean, Business &amp; Hospitality - District Office</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Michelle Sever</td>
                                 
                                 <td>Co-Chair &amp; Director, HR Policy &amp; Compliance - District Office</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Loren Bender </td>
                                 
                                 <td>Convener &amp; VP, Business Operations &amp; Finance - District Office</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Daniel Barkowitz </td>
                                 
                                 <td>AVP Financial Aid &amp; Vet Affairs</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Joe Livingston </td>
                                 
                                 <td>AVP, Human Resources - District Office</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Traci Thornton </td>
                                 
                                 <td>AVP, Marketing &amp; Strategic Communication - District Office</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Paul Rooney </td>
                                 
                                 <td>AVP, Operations - District Office</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Sonya Joseph </td>
                                 
                                 <td>AVP, Student Affairs - Lake Nona Campus</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Judy Jackson </td>
                                 
                                 <td>Director, Business Operations - Continuing Education - West Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jamie Rost</td>
                                 
                                 <td>Interim AVP, Enterprise Solutions - West Campus</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Rhonda Ulmer </td>
                                 
                                 <td>Managing Director, Procurement - District Office</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Roger Corriveau</td>
                                 
                                 <td>Operations Manager - East Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jonathan Hernandez</td>
                                 
                                 <td> Manager - Lake Nona Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jasmin Cruz </td>
                                 
                                 <td> Operations Manager - Osceola Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Neisa Farley </td>
                                 
                                 <td>Operations Manager - West Campus</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Mike Lergier</td>
                                 
                                 <td>Operations Manager- Winter Park Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Joe Lopez </td>
                                 
                                 <td>Public Safety Facilities Specialist - School of Public Safety </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/operations-leadership-team/roster.pcf">©</a>
      </div>
   </body>
</html>