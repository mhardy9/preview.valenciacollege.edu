<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Operations Leadership Team Meeting Summaries  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/operations-leadership-team/summaries.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Operations Leadership Team</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li><a href="/about/governance/operations-leadership-team/">Operations Leadership Team</a></li>
               <li>Operations Leadership Team Meeting Summaries </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Meeting Summaries</h2>
                        
                        
                        <h3>2017</h3>
                        
                        <ul>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLT-Meeting-Summary-8-29-17.pdf" target="_blank">Operations Leadership Team Meeting Summary - 8/29/17</a></li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLT-Meeting-Summary-6-6-17.pdf" target="_blank">Operations Leadership Team Meeting Summary - 6/6/17</a></li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary4417.pdf" target="_blank">Operations Leadership Team Meeting Summary - 4/4/17 </a></li>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary2717.pdf" target="_blank">Operations Leadership Team Meeting Summary - 2/7/17</a> 
                           </li>
                           
                        </ul>
                        
                        
                        <h3>2016</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary11116.pdf" target="_blank">Operations Leadership Team Meeting Summary - 11/1/16</a> 
                           </li>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary9616.pdf" target="_blank">Operations Leadership Team Meeting Summary - 9/6/16</a> 
                           </li>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary8216.pdf" target="_blank">Operations Leadership Team Meeting Summary - 8/2/16</a> 
                           </li>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary6716.pdf" target="_blank">Operations Leadership Team Meeting Summary - 6/7/16</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary5316.pdf" target="_blank">Operations Leadership Team Meeting Summary - 5/3/16 </a></li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary2216.pdf" target="_blank">Operations Leadership Team Meeting Summary - 2/2/16</a></li>
                           
                        </ul>
                        
                        
                        <h3>2015</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary12.1.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 12/1/15</a> 
                           </li>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary10615.pdf" target="_blank">Operations Leadership Team Meeting Summary - 10/6/15</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary9.1.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 9/1/15 </a></li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary8415.pdf" target="_blank">Operations Leadership Team Meeting Summary - 8/4/15</a></li>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary6215.pdf" target="_blank">Operations Leadership Team Meeting Summary - 6/2/15</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary4.7.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 4/7/15 </a></li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary2.3.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 2/3/15 </a></li>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary1.6.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 1/6/15</a> 
                           </li>
                           
                        </ul>
                        
                        
                        <h3>2014</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary11.4.14.pdf" target="_blank">Operations Leadership Team Meeting Summary - 11/4/14</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/operations-leadership-team/OLTMeetingSummary10.7.14.pdf" target="_blank">Operations Leadership Team Meeting Summary - 10/7/14</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/operations-leadership-team/summaries.pcf">©</a>
      </div>
   </body>
</html>