<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Council Officers' Coordination Team  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/council-officers-coordination-team.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Governance Council</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li>Council Officers' Coordination Team </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Council Officers' Coordination Team</h2>
                        
                        <p>
                           The Council Officers' Coordination Team (COCT) oversees collaborative work teams commissioned
                           by the governance councils. The team will assess and assign new work for consideration
                           by a particular council and ensure that work is not being addressed by multiple councils
                           at once. An annual planning retreat will be scheduled by this team for all members
                           of the three governance councils, where new members are trained, revisions to the
                           new governance system are determined and the agenda for the year is planned.
                           
                        </p>
                        
                        <p>The COCT is comprised of the Officers' of each of the Governance Councils.</p>
                        
                        
                        <h3>Council Officers' 2016-17</h3>
                        
                        <p>
                           <strong>Susan Ledlow</strong><br>
                           VP of Academic Affairs &amp; Planning<br>
                           Co-Chair, Learning Leadership Council
                           
                        </p>
                        
                        <p>
                           <strong>Suzette Dohany</strong> <br>
                           Past-President of the Valencia College-Wide Faculty Association<br>
                           Co-Chair, Learning Leadership Council
                           
                        </p>
                        
                        <p>
                           <strong>Amy Bosley</strong><br>
                           VP of Organizational Development / HR<br>Co-Chair, Executive Council 
                           
                        </p>
                        
                        <p>
                           <strong>Loren Bender</strong> <br>
                           VP of Business Operations &amp; Finance <br>
                           Co-Chair, Executive Council
                           
                        </p>
                        
                        <p>
                           <strong>Kim Sepich</strong> <br>
                           VP of Student Affairs<br>
                           Incoming Co-Chair, Executive Council 
                           
                        </p>
                        
                        <p>
                           <strong>Neal Phillips</strong><br>
                           Professor of English, West Campus <br>
                           President of the Valencia College-Wide Faculty Association, Chair of Faculty Council
                           
                        </p>
                        
                        <p>
                           <strong>Al Groccia</strong><br>
                           Professor of Mathematics, Osceola Campus<br>
                           Vice President of the Valencia College-Wide Faculty Association, Co-Chair of Faculty
                           Council
                           
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/council-officers-coordination-team.pcf">©</a>
      </div>
   </body>
</html>