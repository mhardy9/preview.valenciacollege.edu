<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>No Title Found | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/_temp_nav.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li>No Title Found</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <ul>
                  
                  <li><a href="/about/governance/index.html">Governance Councils</a></li>
                  
                  <li class="submenu megamenu">
                     <a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
                     
                     <div class="menu-wrapper c3">
                        
                        <div class="col-md-4">
                           
                           <h3><a href="/about/governance/executive-council/">Executive Council</a></h3>
                           
                           <ul>
                              
                              <li><a href="/about/governance/executive-council/summaries.html">Meeting Summaries</a></li>
                              
                              <li><a href="/about/governance/executive-council/work-plans.html">Work Plans</a></li>
                              
                           </ul>
                           
                           
                           <h3><a href="/about/governance/learning-leadership-council/">Learning Leadership Council</a></h3>
                           
                           <ul>
                              
                              <li><a href="/about/governance/learning-leadership-council/summaries.html">Meeting Summaries</a></li>
                              
                              <li><a href="/about/governance/learning-leadership-council/roster.html">Roster</a></li>
                              
                              <li><a href="/about/governance/learning-leadership-council/work-plans.html">Work Plans</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <h3><a href="/about/governance/operations-leadership-team/">Operations Leadership Council</a></h3>
                           
                           <ul>
                              
                              <li><a href="/about/governance/operations-leadership-team/summaries.html">Meeting Summaries</a></li>
                              
                              <li><a href="/about/governance/operations-leadership-team/roster.html">Roster</a></li>
                              
                           </ul>
                           
                           
                           <ul>
                              
                              <li><a href="/about/governance/council-officers-coordination-team.html">Council Officers' Coordination Team</a></li>
                              
                              <li><a href="/about/governance/resources.html">Resources</a></li>
                              
                              <li><a href="/about/governance/work-plans.html">Work Plans</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </li>
                  
               </ul>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/_temp_nav.pcf">©</a>
      </div>
   </body>
</html>