<ul>
	<li class="submenu"><a class="show-submenu" href="/about/governance/index.php">Governance Councils <span class="caret" aria-hidden="true"></span></a>
	<ul>		
	<li><a href="/about/governance/council-officers-coordination-team.php">Council Officers' Coordination Team</a></li>
	<li><a href="/about/governance/resources.php">Resources</a></li>
	<li><a href="/about/governance/work-plans.php">Work Plans</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/about/governance/executive-council/">Executive Council <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/about/governance/executive-council/summaries.php">Meeting Summaries</a></li>
			<li><a href="/about/governance/executive-council/work-plans.php">Work Plans</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/about/governance/learning-leadership-council/">Learning Leadership Council <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/about/governance/learning-leadership-council/summaries.php">Meeting Summaries</a></li>
			<li><a href="/about/governance/learning-leadership-council/roster.php">Roster</a></li>
			<li><a href="/about/governance/learning-leadership-council/work-plans.php">Work Plans</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/about/governance/operations-leadership-team/">Operations Leadership Team <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/about/governance/operations-leadership-team/schedule.php">Meeting Schedule</a></li>
			<li><a href="/about/governance/operations-leadership-team/summaries.php">Meeting Summaries</a></li>
			<li><a href="/about/governance/operations-leadership-team/roster.php">Roster</a></li>
		</ul>
	</li>
</ul>