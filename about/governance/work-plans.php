<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Work Plans  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/work-plans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Governance Councils</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li>Work Plans </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Work Plans</h2>
                        
                        
                        <p>A Work Proposal should be brought to a Council if it meets one or more of the following
                           criteria:
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>The work is College-wide effort.</li>
                           
                           <li>There will be an impact on multiple divisions, processes, and/or systems.</li>
                           
                           <li>There will be an impact on the College budget.</li>
                           
                        </ul>
                        
                        
                        <p>Click on the buttons below to access information on the work plan process and templates
                           to guide you along the way.
                        </p>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-3">
                              <a href="/documents/about/governance/WorkPlanProcess.pdf" class="button-action_outline" target="_blank">Work Plan Process</a>
                              
                           </div>
                           
                           <div class="col-md-3">
                              <a href="/documents/about/governance/Work-Proposal-Template.docx" class="button-action_outline" target="_blank">Work Proposal</a>
                              
                           </div>
                           
                           <div class="col-md-3">
                              <a href="/documents/about/governance/Work-Plan-With-Timeline.docx" class="button-action_outline" target="_blank">Work Plan</a>
                              
                           </div>
                           
                           <div class="col-md-3">
                              <a href="/documents/about/governance/RecommendationsTemplate.docx" class="button-action_outline" target="_blank">Recommendations from Work Team</a>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/work-plans.pcf">©</a>
      </div>
   </body>
</html>