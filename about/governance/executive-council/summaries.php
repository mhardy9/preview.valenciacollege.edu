<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Executive Council Meeting Summaries  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/executive-council/summaries.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Governance Council</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li><a href="/about/governance/executive-council/">Executive Council</a></li>
               <li>Executive Council Meeting Summaries </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Executive Council Meeting Summaries</h2>
                        
                        
                        <h3>2017</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/executive-council/Executive-Council-Meeting-Summary-7-19-17.pdf" target="_blank">Executive Council Meeting Summary - 7/19/17</a></li>
                           
                           <li><a href="/documents/about/governance/executive-council/executivecouncilmeetingsummary4517.pdf" target="_blank">Executive Council Meeting Summary - 4/5/17</a></li>
                           
                        </ul>
                        
                        
                        <h3>2016</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/executive-council/executivecouncilmeetingsummary12716.pdf" target="_blank">Executive Council Meeting Summary - 12/7/16 </a></li>
                           
                           <li>
                              <a href="/documents/about/governance/executive-council/executivecouncilmeetingsummary102816.pdf" target="_blank">Executive Council Meeting Summary - 10/28/16</a> 
                           </li>
                           
                           <li>
                              <a href="/documents/about/governance/executive-council/executivecouncilmeetingsummary62216.pdf" target="_blank">Executive Council Meeting Summary - 6/22/16</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/executive-council/executivecouncilmeetingsummary41316.pdf" target="_blank">Executive Council Meeting Summary - 4/13/16</a></li>
                           
                           <li>
                              <a href="/documents/about/governance/executive-council/executivecouncilsummary2.17.16.pdf" target="_blank">Executive Council Meeting Summary - 2/17/16</a> 
                           </li>
                           
                        </ul>
                        
                        
                        <h3>2015</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/executive-council/ecmeetingsummary12215.pdf" target="_blank">Executive Council Meeting Summary - 12/2/15 </a></li>
                           
                           <li><a href="/documents/about/governance/executive-council/ecmeetingsummaryfrom10.21.15.pdf" target="_blank">Executive Council Meeting Sumary - 10/21/15 </a></li>
                           
                           <li>
                              <a href="/documents/about/governance/executive-council/ecmeetingsummary62415.pdf" target="_blank">Executive Council Meeting Summary - 6/24/15</a> 
                           </li>
                           
                           <li>
                              <a href="/documents/about/governance/executive-council/ecmeetingsummary42915.pdf" target="_blank">Executive Council Meeting Summary - 4/29/15</a> 
                           </li>
                           
                           <li><a href="/documents/about/governance/executive-council/ecmeetingsummary2.25.15.pdf" target="_blank">Executive Council Meeting Summary - 2/25/15 </a></li>
                           
                        </ul>
                        
                        
                        <h3>2014</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/documents/about/governance/executive-council/ecmeetingsummary12.17.14.pdf" target="_blank">Executive Council Meeting Summary - 12/17/14</a></li>
                           
                           <li><a href="/documents/about/governance/executive-council/ecmeetingsummary92414.pdf" target="_blank">Executive Council Meeting Summary - 9/24/14</a></li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/executive-council/summaries.pcf">©</a>
      </div>
   </body>
</html>