<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Executive Council Work Plans  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/executive-council/work-plans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Governance Council</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li><a href="/about/governance/executive-council/">Executive Council</a></li>
               <li>Executive Council Work Plans </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Executive Council Work Plans</h2>
                        
                        
                        <h3>Work In Progress </h3>
                        
                        <p><strong>WORK PLANS IN PROGRESS</strong></p>
                        
                        <ul>
                           
                           <li>N/A</li>
                           
                        </ul>
                        
                        
                        <h3>Completed Work Plans </h3>
                        
                        <p><strong>COMPLETED WORK (OR WORK IN THE IMPLEMENTATION PHASE)</strong></p>
                        
                        <ul>
                           
                           <li>Annually Appointed Faculty Role Design: 
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/WorkProposal-AnnuallyAppointedFacultyRoleDesign.docx">Work Proposal</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/ProposalFeedback12.2.15-AAFacultyRoleDesign.docx">Feedback - Dec 2015</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/facultycontractmatrix.pdf">Faculty Contract Matrix</a> 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/executive-council/annuallyappointedroledesignrecommendations.pdf">Recommendations - October 2016</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Signage and Branding: 
                              
                              <ul>
                                 
                                 <li><a href="/documents/about/governance/executive-council/SignageandBrandingWorkProposal.pdf"> Work Proposal </a></li>
                                 
                                 <li><a href="/documents/about/governance/executive-council/SignageandBrandingWorkPlan-FINAL.pdf"> Work Plan</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>50th Anniversary Commemoration: 
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/50thAnniversaryWorkProposal.pdf"> Work Proposal</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/WorkPlan-50thAnniversary.pdf">Work Plan</a> 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/executive-council/50thAnniversaryPresentation.pptx">Final Report</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>College ID's: 
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/CollegeIDsP2P.pdf">Work Plan</a> 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/executive-council/RecommendationsfromCollegeIDsWorkTeam.pdf">Recommendations</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Strategic Planning Software Search: 
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/StrategicPlanningSoftwareSearchWorkProposal04-22-2015kb.pdf"> Work Proposal</a> 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/executive-council/StrategicPlanningSoftwareSearchReport2015-Final.pdf">Final Report with Recommendations</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Policy Office Advisory Group (POAG): 
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/PolicyAdvisoryGroupWorkProposal.pdf">Work Proposal</a> 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/executive-council/WorkPlanforPolicyAdvisoryGroup.pdf">Work Plan</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Strategic Planning Process: 
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/StrategicPlanWorkProposal021915.pdf">Work Proposal</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/YearofReflectionReport04-23-15.pdf">Year of Reflection Report</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/ValenciaCollegeStrategicPlanningTimelineTable042115kb.pdf">Strategic Planning Timeline</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/CECImpactPlanJune22final.ppt">5-year Impact Plan</a> 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Work On Hold</h3>
                        
                        <p><strong>WORK ON HOLD OR NOT ENDORSED</strong></p>
                        
                        <ul>
                           
                           <li> LifeMap Environmental Campaign: 
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/WorkProposal-LifeMapEnvironmentalCampaign.docx">Work Proposal</a> 
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/about/governance/executive-council/DRAFTWorkPlan-LifeMapEnvironmentalCampaign.docx">DRAFT Work Plan</a> 
                                 </li>
                                 
                                 <li><a href="/documents/about/governance/executive-council/ProposalFeedback12.2.15-LifeMapEnvironmentalCampaign.docx">Proposal Feedback - Dec 2015</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Data Quality/Strategy: <a href="/documents/about/governance/executive-council/DataStrategyAdvisoryTeamProposalforExecutiveCouncil92414.pdf">Work Proposal</a> 
                           </li>
                           
                           <li>Distance Learning Fees: <a href="/documents/about/governance/executive-council/WorkProposal-DistanceLearningFee.pdf">Work Proposal</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/executive-council/work-plans.pcf">©</a>
      </div>
   </body>
</html>