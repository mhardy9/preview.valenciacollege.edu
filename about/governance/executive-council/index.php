<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Executive Council  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/governance/executive-council/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/governance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Governance Council</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/governance/">Governance</a></li>
               <li>Executive Council</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Executive Council</h2>
                        
                        <p>The <strong>Executive Council (EC) </strong>meets to communicate, celebrate, and explore together in ways that align leadership
                           from deans and directors to the senior team, keep the working theories - big ideas,
                           in front of everyone, and connect the dots between projects and issues and the overall
                           strategy and narrative.
                        </p>
                        
                        <p>The co-chairs of the EC rotate  every two years among the VP of Organization Development
                           &amp; HR,  the VP of Business Operations &amp; Finance, and the VP of Student Affairs. 
                        </p>
                        
                        
                        <h3>Council Officers 2017-18</h3>
                        
                        <p>
                           Kim Sepich, Co-Chair<br>
                           VP of Student Affairs
                           
                        </p>
                        
                        <p>
                           Loren Bender, Co-Chair<br>
                           VP of Business Operations &amp; Finance
                           
                        </p>
                        
                        <p>
                           Kari Makepeace, Council Coordinator<br>
                           Coordinator of Academic Planning &amp; Support
                           
                        </p>
                        
                        
                        <h3>Past Council Officers</h3>
                        
                        <p>
                           2016-17: Amy Bosley, VP of Organizational Development &amp; HR<br>
                           2015-16: Joyce Romano, VP of Educational Partnerships
                           
                        </p>
                        
                        
                        <h3>Roster</h3>
                        
                        <p>The Executive Council  is comprised of all Executives and Administrators, the full
                           Faculty Council, and Staff Association representatives.
                        </p>
                        
                        
                        <h3>Schedule</h3>
                        
                        <p>The Executive Council meets three times per year (fall, spring, summer). The meeting
                           dates and locations for 2017-18 are provided below. Please note that these meeting
                           dates, times, and locations are subject to change.
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Meeting Dates</th>
                                 
                                 <th>Meeting Times</th>
                                 
                                 <th>Location</th>
                                 
                                 <th>Meeting Type </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>October 4, 2017</td>
                                 
                                 <td>1:00 - 4:00 PM </td>
                                 
                                 <td>TBD</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 7, 2018</td>
                                 
                                 <td>1:00 - 4:00 PM </td>
                                 
                                 <td>TBD</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>June 6, 2018</td>
                                 
                                 <td>1:00 - 4:00 PM </td>
                                 
                                 <td>TBD</td>
                                 
                                 <td>Regular Meeting</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/governance/executive-council/index.pcf">©</a>
      </div>
   </body>
</html>