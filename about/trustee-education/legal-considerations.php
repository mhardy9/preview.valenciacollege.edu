<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/legal-considerations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/trustee-education/">Trustee Education</a></li>
               <li>Trustee Education &amp; Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <link href="../../includes/colorbox.css.html" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        
                        <img alt="Bill Mullowney, VP of Policy and General Counsel" height="240" hspace="40" src="images/4535644768_43d5f0e811_m.jpg" width="172">
                        
                        
                        
                        <h2>Legal Considerations</h2>
                        
                        <p> Many legal considerations inform your role and guide the way that you perform your
                           duties as a trustee at Valencia. Understanding the Florida College System, governance
                           processes and obligations, as well as key provisions in the law is integral to your
                           position. 
                        </p>
                        
                        <p>In this section, Bill Mullowney, vice president of policy and general counsel, provides
                           important information on your role and responsibilities as a trustee of the college,
                           accompanied by an overview of Florida’s Sunshine and Ethics Law. 
                        </p>
                        
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FLORIDA COLLEGE SYSTEM OVERVIEW </div>
                                 
                                 <div data-old-tag="td">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/N591rLv6AHI?rel=0" target="_blank"><img alt="Overview of the Florida College System" border="0" height="168" src="images/Bill-1.png" width="300"><br>
                                          <br><strong>Florida College System Overview </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/DBOT_overview.pdf">The District Board of Trustees of Valencia College: An Overview</a></li>
                                       
                                       <li>
                                          <a href="../about/general-counsel/policy/documents/Volume1/1-05-Officers-and-Their-Duties.pdf">Officers and Their Duties</a> 
                                       </li>
                                       
                                    </ul>                  
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">BOARD OF TRUSTEES OVERVIEW </div>
                                 
                                 <div data-old-tag="td">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/xozpEuFFJXk?rel=0" target="_blank"><img alt="Board of Trustees Roles and Responsibilities" border="0" height="169" src="images/bot-roles-responsibilities.jpg" width="300"><br>
                                          <br><strong>Board of Trustees Roles and Responsibilities </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/DBOT_governance-and-administration-in-FL-law.pdf">Florida College System Governance and Administration</a></li>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume1/1-01-Organization-Authority-and-Location.pdf">Organization, Authority and Location</a></li>
                                       
                                       <li>
                                          <a href="../about/general-counsel/documents/TheSessionReport2013-h.pdf">2013 Legislative Session Report</a>  
                                       </li>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume1/1-11-Indemnification.pdf">Policy on Indemnification</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/5Uy3xv7RCKg?rel=0" target="_blank">  <img alt="Board Organization and Structure" border="0" height="168" src="images/Bill-3.png" width="300"><br>
                                          <br><strong>Board Organization and Structure</strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">                
                                    <ul>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume1/1-02-General.pdf">General Governance</a></li>
                                       
                                       <li>
                                          <a href="../about/general-counsel/policy/documents/Volume1/1-08-College-Accountability-Process.pdf">College Accountability Process</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="../about/general-counsel/policy/documents/Volume1/1-04-Rules-of-Procedures-for-Meetings-of-the-District-Board-of-Trustees.pdf">Rules of Procedure for Meetings</a>  
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">POLICY AND LEGAL CONSIDERATIONS </div>
                                 
                                 <div data-old-tag="td">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/F5t41j7IjD8?rel=0" target="_blank"><img alt="Florida's Sunshine Law" border="0" height="168" src="images/sunshine-law.jpg" width="300"><br>
                                          <br><strong>Governing in the Sunshine: The Florida Sunshine Law </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/DBOT_sunshine-laws.pdf">Florida's Sunshine Laws and Florida College System Boards of Trustees</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/0hxkkvw2HUE?rel=0" target="_blank"><img alt="Florida's Ethics Law" border="0" height="168" src="images/Bill-5.png" width="300"><br>
                                          <br><strong>The Florida Ethics Law </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/DBOT_ethics-laws.pdf">Ethics Laws in Florida and Florida College System Boards of Trustees</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/4LJmEfQ4-WU?rel=0" target="_blank"><img alt="Policy Overview and Resources" border="0" height="168" src="images/bill-link6.jpg" width="300"><br>
                                          <br>
                                          <strong>Policy Overview and Resources </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">                  
                                    
                                    <ul>
                                       
                                       <li><a href="../general-counsel/index.html">Office of Policy and General Counsel</a></li>
                                       
                                       <li><a href="../general-counsel/policy/index.html">Policy and Procedures Manual </a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/legal-considerations.pcf">©</a>
      </div>
   </body>
</html>