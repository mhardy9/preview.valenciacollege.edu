<ul>
<li><a href="index.php">Trustee Education &amp; Preparation</a></li>
<li class="submenu">
<a class="show-submenu" href="javascript:void(0);">Resources <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
<li><a href="legal-considerations.php">Legal Considerations</a></li>
<li><a href="operations-finance.php">Operations &amp; Finance</a></li>
<li><a href="internal-auditing.php">Internal Auditing</a></li>
<li><a href="human-resources.php">HR Functions</a></li>
<li><a href="student-affairs.php">Student Affairs</a></li>
<li><a href="academicaffairs.php">Academic Affairs &amp; Planning</a></li>
<li><a href="valenciasbigideas.php">Valencia's Big Ideas</a></li>
</ul>
</li>
</ul>

