<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/operations-finance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/trustee-education/">Trustee Education</a></li>
               <li>Trustee Education &amp; Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <link href="../../includes/colorbox.css.html" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        
                        <img alt="Keith Houck" height="213" hspace="15" src="images/Keith_Houck_headshot_001.jpg" width="150">
                        
                        
                        
                        <h2>Operations and Finance</h2>
                        
                        <p>Valencia is a large, complex institution of higher learning that has an operating
                           budget of more than $200 million. Fiscal responsibility and stewardship is required
                           to manage and operate the college and ensure resources are allocated and expended
                           in the best interest of our students, faculty, staff and community.
                        </p>
                        
                        <p>In this section, Keith Houck, vice president of operations and finance, explains Valencia’s
                           accounting practices, reviews the operating budget and provides an overview of the
                           college’s facilities and sustainability efforts. 
                        </p>
                        
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">THE ROLE OF OPERATIONS AND FINANCE </div>
                                 
                                 <div data-old-tag="th">SUPPLEMENTAL RESOURCES </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/teU9YW1jYBc?rel=0" target="_blank"><img alt="Overview of Operations and Finance" height="168" src="images/Keith-1.png" width="300"><br>
                                          <br>
                                          <strong>Overview of Operations and Finance  </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../budget/timeline.html">Budget Development Timeline</a></li>
                                       
                                       <li><a href="documents/BudgetPrinciplesfor2013.pdf">2013-2014 Budget Planning Principles </a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/OzLtCtF9v38?rel=0" target="_blank"><img alt="State Reporting and College Accounting" height="169" src="images/state-reporting.jpg" width="300"><br>
                                          <br>
                                          <strong>State Reporting and College Accounting  </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../about/budget/documents/FinancialAuditFY2012-13.pdf">2013 State Financial Audit Report</a></li>
                                       
                                       <li>
                                          <a href="../about/budget/documents/OperationalAuditFY2011-12.pdf">2012 State Operational Audit Report</a>  
                                       </li>
                                       
                                       <li><a href="../about/budget/documents/AccountingTerminology.pdf">Selected Fund Accounting Terminology</a></li>
                                       
                                       <li><a href="../about/budget/documents/OperatingBudget2013-14.pdf">2013-2014 Operating Budget </a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">FACILITIES AND SUSTAINABILITY </div>
                                 
                                 <div data-old-tag="th">SUPPLEMENTAL RESOURCES </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/Y9k1MTqfySQ?rel=0" target="_blank"><img alt="Facilities Funding" height="168" src="images/facilities.jpg" width="300"><br>
                                          <br>
                                          <strong>Facilities Funding</strong> </a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="../facilities/projects.html">Current Facilities Projects</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="../about/facilities/documents/AEGuidelinesFinal.pdf.html">Architecture and Engineering Guidelines</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/fpIrQQppq4E?rel=0" target="_blank"><img alt="Sustainability at Valencia" height="169" src="images/sustainability.jpg" width="300"><br>
                                          <br>
                                          <strong>Sustainability at Valencia </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    <ul>
                                       
                                       <li><a href="../about/facilities/sustainability/action/documents/ValenciaCollegeSustainabilityPlanOctober2012.pdf.html">Sustainability Plan</a></li>
                                       
                                       <li><a href="../about/facilities/sustainability/action/energy-efficiency.html">Energy Efficiency </a></li>
                                       
                                       <li><a href="http://thegrove.valenciacollege.edu/valencia-reaches-1-million-milestone-in-energy-savings/">Energy Savings Milestone</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">COLLEGE FUNDING SOURCES </div>
                                 
                                 <div data-old-tag="th">SUPPLEMENTAL RESOURCES </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/9nYfCGt1kn4?rel=0" target="_blank"><img alt="State Funding Model" height="169" src="images/state-funding.jpg" width="300"><br>
                                          <br>
                                          <strong>State Funding Model  </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../about/budget/documents/FinancialIndicatorsDec2011PDF.pdf.html">Financial Indicators</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/operations-finance.pcf">©</a>
      </div>
   </body>
</html>