<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="container-fluid header-college">
         <nav class="container navbar navbar-expand-lg">
            <div class="navbar-toggler-right"><button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-val" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div>
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/_menu-collegewide.inc"); ?></div>
         </nav>
         <nav class="container navbar navbar-expand-lg">
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
         </nav>
      </div>
      <div class="page-header bg-interior">
         <div id="intro-txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Trustee Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid">
            		
            		
            <div class="container margin-60" role="main">
               
               
               			
               <h2>Welcome to Valencia's District Board of Trustees! </h2>
               			
               <div class="row">
                  				
                  <div class="col-md-8">
                     					
                     <p>As a member of the board of trustees, you have a unique opportunity to govern and
                        help Valencia fulfill its mission of serving students. On this site, you will find
                        videos and resources to familiarize you with Valencia's history, culture, operations
                        and student and academic affairs. All videos are segmented by topic and designed to
                        give you a quick overview in five minutes or less, give or take.
                     </p>
                     					
                     <p>This program begins with Valencia's College President Dr. Sandy Shugart, who provides
                        a synopsis of our learning culture, student demographics, our campuses and programs
                        and most importantly a foundation for effective board stewardship. Videos and supplemental
                        resources are provided below. We hope that you find these sessions informative and
                        beneficial during your tenure as a Valencia District Board of Trustee member.
                     </p>             
                     				
                  </div>
                  				
                  <div class="col-md-4">
                     					<img alt="Dr. Sandy Shugart, Valencia College President" src="/about/trustee-education/images/Shugart_websized.jpg" class="img=fluid">
                     				
                  </div>
                  			
               </div>
               			
               <hr>
               			
               <h3 class="bg-light p-2">An Introduction to Valencia</h3>
               
               			
               <section class="mb-4">
                  
                  				
                  <!-- Cards, Row 1 -->
                  				
                  <div class="card-deck mb-4">
                     					
                     <!-- Card Column 1 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/hpGhRhQj-3Y?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">Welcome and Overview of the College</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="/about/about-valencia/vision.php">Vision, Values &amp; Mission</a></li>
                                 									
                                 <li><a href="/about/our-next-big-idea/documents/HistoryMilestones.pdf">Valencia's History Milestones</a></li>
                                 									
                                 <li><a href="http://www.youtube.com/watch?v=8uBlx9ZUJg0">History of Valencia Video</a></li>
                                 									
                                 <li><a href="/about/trustee-education/documents/big-ideas-trustees.pdf">Our Big Ideas</a></li>
                                 									
                                 <li><a href="http://thegrove.valenciacollege.edu/valencia-named-americas-top-college/">Aspen Prize</a> 
                                 </li>
                                 								
                              </ul>
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     
                     					
                     <!-- Card Column 2 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/Ky02_STtWxA?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">Valencia's Service Districts and Economic Impact</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="http://news.valenciacollege.edu/impact/">Economic Impact Study</a></li>
                                 									
                                 <li><a href="/about/about-valencia/facts.php">Valencia College Facts</a></li>
                                 								
                              </ul>
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     				
                  </div>
                  			
               </section>	
               			
               			
               <h3 class="bg-light p-2">Trustee Stewardship</h3>
               
               			
               <section class="mb-4">
                  
                  				
                  <!-- Cards, Row 1 -->
                  				
                  <div class="card-deck mb-4">
                     					
                     <!-- Card Column 1 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/W1d4ZmFoIYs?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">Trustee Stewardship and Responsibility</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="/about/about-valencia/administration.php">College Administration</a></li>
                                 									
                                 <li><a href="/about/trustee-education/documents/valencia-leadership-profiles-r-052416.pdf">Organizational Chart (Administrative) </a></li>
                                 									
                                 <li><a href="/about/about-valencia/trustees.phpl">Board of Trustees Website</a></li>
                                 									
                                 <li><a href="/about/office-of-the-president">Office of the President </a></li>
                                 								
                              </ul>
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     
                     					
                     <!-- Card Column 2 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/TNgVVAW425Q?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">College Culture and Trustee Involvement</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="/about/learning-centered-initiative/">Learning-Centered Initiative</a> 
                                 </li>
                                 									
                                 <li><a href="/about/about-valencia/trusteeschedule.php">Board of Trustees Meetings</a></li>
                                 									
                                 <li><a href="http://events.valenciacollege.edu/">Valencia's Event Calendar</a></li>
                                 								
                              </ul>
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     				
                  </div>
                  			
               </section>		
               
               			
               <h3 class="bg-light p-2">Campus Leadership &amp; Programs</h3>
               
               			
               <section class="mb-4">
                  
                  				
                  <!-- Cards, Row 1 -->
                  				
                  <div class="card-deck mb-4">
                     					
                     <!-- Card Column 1 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/ZgwRtcubG7k?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">Campus Overview and Culture of Learning</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="/locations/">Valencia's Campuses</a></li>
                                 									
                                 <li><a href="http://www.youtube.com/watch?v=IJpazqJ7uGU">The Valencia Experience</a> 
                                 </li>
                                 									
                                 <li><a href="/about/about-valencia/facts.php">Valencia College Facts</a></li>
                                 								
                              </ul>
                              
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     
                     					
                     <!-- Card Column 2 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/tesqY815x7M?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">West Campus Leadership and Programs</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="/locations/west/">West Campus Website</a></li>
                                 									
                                 <li>
                                    										<a href="http://preview.valenciacollege.edu/continuing-education/">Continuing Education</a> 
                                 </li>
                                 									
                                 <li>
                                    										<a href="http://preview.valenciacollege.edu/continuing-education/programs/collaborative-design-center/">The Collaborative Design Center</a> 
                                 </li>
                                 								
                              </ul>
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     				
                  </div>
                  				
                  <div class="card-deck">
                     					
                     <!-- Card Column 1 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/hQY556nhmP0?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">East Campus Leadership and Programs</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="/locations/east/">East Campus Website</a></li>
                                 									
                                 <li><a href="/locations/winter-park/">Winter Park Campus Website</a></li>
                                 									
                                 <li><a href="/locations/school-of-public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
                                 								
                              </ul>
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     
                     					
                     <!-- Card Column 2 -->
                     					
                     <div class="card">
                        						
                        <div>
                           							
                           <div class="aspect-ratio">
                              								<iframe src="https://www.youtube.com/embed/tgboqm42N1c?showinfo=0&amp;rel=0&amp;modestbranding=1"></iframe>
                              							
                           </div>
                           						
                        </div>
                        						
                        <div class="card-body">
                           							<span class="card-text">
                              								
                              <h4 class="card-title">Osceola Campus Leadership and Programs</h4>
                              								
                              <h5>Supplemental Materials</h5>
                              								
                              <ul>
                                 									
                                 <li><a href="/locations/osceola/">Osceola Campus Website</a></li>
                                 									
                                 <li><a href="/locations/lakenona/">Lake Nona Website</a></li>
                                 								
                              </ul>
                              							</span>
                           						
                        </div>
                        					
                     </div>
                     				
                  </div>
                  			
               </section>	
               		
            </div>
            	
            <hr class="styled_2">
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/index.pcf">© 2018 - All rights reserved.</a>
      </div>
   </body>
</html>