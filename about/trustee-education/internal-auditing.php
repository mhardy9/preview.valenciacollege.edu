<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/internal-auditing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/trustee-education/">Trustee Education</a></li>
               <li>Trustee Education &amp; Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <link href="../../includes/colorbox.css.html" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        <img alt="Undria Stalling" height="118" hspace="15" src="images/cynthia-santiago.jpg" width="194">              
                        
                        
                        <h2>The Internal Audit Function</h2>
                        
                        <p>Cynthia Santiago-Guzman is the Director of Compliance and Audit at Valencia College.
                           Her duties include reviewing, analyzing and assessing college operations, processes
                           and procedures in order to improve efficiencies and minimize risk. The Director of
                           Compliance and Audit reports directly to Valencia College's District Board of Trustees.
                        </p>
                        
                        <p>This segment will provide you with an overview of the internal audit function, the
                           scope and primary responsibilities of the role and the structure as it pertains to
                           reporting and commissioning audit-related activities.
                        </p>
                        
                        
                        <h3>&nbsp;</h3>            
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">INTERNAL AUDITING OVERVIEW </div>
                                 
                                 <div data-old-tag="th">SUPPLEMENTAL RESOURCES </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/JK2oZmEz_i0?rel=0" target="_blank"><img alt="Overview of the Internal Audit Function" height="168" src="images/Undria-1.png" width="300"><br>
                                          <br>
                                          <strong>Overview of the Internal Audit Function </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="../audit/index.html">Office of the Internal Auditor</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/74p-y6ynCjM?rel=0" target="_blank"><img alt="Functional and Administrative Reporting Structure" height="168" src="images/Undria-3.png" width="300"><br>
                                          <br>
                                          <strong>Functional and Administrative Reporting Structure </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="../audit/staff.html">Office of the Internal Auditor Contact Information</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/internal-auditing.pcf">©</a>
      </div>
   </body>
</html>