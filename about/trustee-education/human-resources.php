<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/human-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/trustee-education/">Trustee Education</a></li>
               <li>Trustee Education &amp; Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <link href="../../includes/colorbox.css.html" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        
                        <img alt="Amy Bosley" height="158" hspace="15" src="images/amy-bosley-144w.jpg" width="144">
                        
                        
                        
                        <h2>Human Resources Functions </h2>
                        
                        <p> Valencia is one of the largest community colleges in Florida, serving more than 60,000
                           students annually; but it's our quality of education, not size, that truly makes us
                           great. The Human Resources function allows the College to do more than fill positions.
                           We look for individuals who share our belief in students and are dedicated to helping
                           them succeed.
                        </p>
                        
                        <p>In the following segments, Amy Bosley, our newly appointed vice president of organizational
                           development and human resources, shares Valencia's philosophy on hiring, creating
                           a diverse and safe workplace, and outlines the procedural and developmental process
                           that are in place to help us recruit and retain the very best. 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">OVERVIEW OF  HUMAN RESOURCES </div>
                                 
                                 <div data-old-tag="th">SUPPLEMENTAL RESOURCES</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/bZyVTo72WnA?rel=0" target="_blank"><img alt="Welcome and Overview of Human Resources and Diversity" height="200" src="images/amy-r1.jpg" width="300"><br>
                                          <br>
                                          <strong>Welcome and Overview of Human Resources </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../hr/aboutValencia.html">Organizational Development &amp; Human Resources</a></li>
                                       
                                       <li>
                                          <a href="../HR/benefits.html">Valencia Benefits</a> 
                                       </li>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume3B/3B-03-Pre-EmploymentDrugTesting-Policy-08-18-1993.pdf.html">Pre-Employment Testing</a></li>
                                       
                                       <li><a href="../HR/index.html">Employment</a></li>
                                       
                                       <li>
                                          <a href="../HR/EqualAccessEqualOpportunity.html">Equal Access and Equal Opportunity</a> 
                                       </li>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume2/2-01-NondiscriminationandEqualOpportunity-Policy-12-18-2012.pdf.html">Non-Discrimination and Equal Opportunity</a></li>
                                       
                                    </ul>                  
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">HR REPORTS AND BOARD ACTIONS </div>
                                 
                                 <div data-old-tag="th">SUPPLEMENTAL RESOURCES</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/HeOdO6PLUGg?rel=0" target="_blank"><img alt="The Human Resources Agenda, Reports and Board Actions" height="168" src="images/Amy-2_000.png" width="300"><br>
                                          <br>
                                          <strong>Human Resources Agenda, Reports and Board Actions</strong></a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../about-valencia/documents/HumanResourcesAgenda_008.pdf">Human Resources Agenda</a></li>
                                       
                                       <li><a href="../HR/documents/ValenciaCollegeSalarySchedule14-15.pdf.html">2014-2015 Salary Schedule</a></li>
                                       
                                       <li><a href="../about-valencia/documents/EquityActReport-Apr232014.pdf">2013-2014 Equity Report</a></li>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume3B/3B-01-Human-Resource-Actions.pdf">HR Actions</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> CLASSIFICATION AND COMPENSATION </div>
                                 
                                 <div data-old-tag="th">SUPPLEMENTAL RESOURCES</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       
                                       <p><a href="http://www.youtube.com/embed/-aKKhVrKGGY?rel=0" target="_blank"><img alt="Employment Categories at Valencia" height="175" src="images/amy-3r.jpg" width="300"><br>
                                             <br><strong>Employment Categories at Valencia</strong></a></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../about/general-counsel/policy/ValenciaCollegePolicy.cfm-policyID=144.html">Employment Categories: Definitions</a></li>
                                       
                                       <li><a href="../about/general-counsel/policy/ValenciaCollegePolicy.cfm-policyID=147.html">Definition of Full-time Employees</a></li>
                                       
                                       <li><a href="../about/general-counsel/policy/default.cfm-policyID=151&amp;volumeID_1=14&amp;navst=0.html">Policy on Employee Contracts</a></li>
                                       
                                       <li><a href="../about/general-counsel/policy/ValenciaCollegePolicy.cfm-policyID=157.html">Issuance of Continuing Contracts</a></li>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume3A/3-02-AwardofTenureandEvaluationofTenuredandTenureTrackFaculty-Policy-07-16-2013.pdf.html">Awarding of Tenure</a></li>
                                       
                                       <li><a href="http://net5.valenciacollege.edu/facultyroster/">Instructional Staff Roster</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       
                                       <p><a href="http://www.youtube.com/embed/DxQelprzaEM?rel=0" target="_blank"><img alt="Valencia's Compensation System" height="168" src="images/Amy-4_000.png" width="300"><br>
                                             <br>Valencia's Compensation System</a></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../about/general-counsel/policy/documents/Volume3C/3C-04.1-Salary-Schedule.pdf.html">Salary Schedule Policy</a></li>
                                       
                                       <li><a href="../HR/FacultyCompensation.html">Faculty Compensation</a></li>
                                       
                                       <li><a href="../HR/compensation.html">Compensation at Valencia</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/human-resources.pcf">©</a>
      </div>
   </body>
</html>