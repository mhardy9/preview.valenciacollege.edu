<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/valenciasbigideas.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/trustee-education/">Trustee Education</a></li>
               <li>Trustee Education &amp; Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <link href="../../includes/colorbox.css.html" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2>Valencia's Big Ideas </h2>
                        
                        <p>An important part of the sustained efforts toward improving student learning at Valencia
                           has been the development of several key ideas that serve as fulcrums for change, signifiers
                           for emerging organizational culture, and rallying points for action. The process of
                           moving from promising innovation to large-scale pilot, to sustained solution, that
                           is, the process of institutionalizing the work, depends heavily on a community of
                           practice shaped by powerful common ideas. While these ideas aren't unique to Valencia,
                           they are authentically ours in the sense that they are organic to our work, having
                           rooted themselves in the discourse of campus conversations, planning, development,
                           and day to day activity.
                        </p>
                        
                        <p>In this section, Dr. Shugart discusses the Big Ideas  that have made and continue
                           to make a difference in the efforts  to make dramatic and sustained progress in student
                           learning. He emphasizes that these ideas have emerged from deep discourse, important
                           stories, long reflection, iterative and inclusive planning, and, most importantly,
                           deep collaboration within our organization. The ideas themselves may have some value,
                           but their power to engage, change, sustain, redirect, unify, and encourage our work
                           is rooted in the authenticity of their origins in our ongoing conversation.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>CULTIVATING BIG IDEAS </div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/UcLprk6JZNk?rel=0" target="_blank"><img alt="Big Ideas: Theory and Approach" height="168" src="images/sandy-theory-r.jpg" width="300"><br>
                                          <br>
                                          <strong>Big Ideas: Theory and Approach </strong></a> 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/big-ideas-trustees.pdf">Our Big Ideas</a></li>
                                       
                                       <li><a href="http://youtu.be/aCi5ouKT7Lk?list=UU3AP52r__FXkAjtb4Z5IU5g" target="_blank">Big Ideas as Working Theories</a></li>
                                       
                                       <li><a href="http://president.valenciacollege.edu/president/files/2011/shugart_unprepared_student_main.pdf">Focus on the Front Door </a></li>
                                       
                                       <li><a href="http://www.nationaljournal.com/features/restoration-calls/how-a-community-college-s-big-ideas-are-transforming-education-20121004">Big Ideas Receive National Attention</a></li>
                                       
                                    </ul>                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="https://www.youtube.com/embed/_wo_QGOYUjo" target="_blank"><img alt="Valencia's Culture" height="182" src="images/sandy-culture-r.jpg" width="300"><br>
                                          <br>
                                          <strong>Valencia's Culture </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../about-valencia/vision.html">Mission, Vision, Values</a></li>
                                       
                                       <li>
                                          <a href="../about-valencia/history.html">Valencia History</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.youtube.com/embed/IJpazqJ7uGU?rel=0" target="_blank">Valencia Experience</a> 
                                       </li>
                                       
                                    </ul>                  
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/CG2BmVR_VtI?rel=0" target="_blank"><img alt="Culture Changes From the Inside Out" height="168" src="images/3_000.png" width="300"><br>
                                          <br>
                                          <strong>Culture Changes From the Inside Out </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="../../lci/index.html">The Learning-Centered Initiative</a> 
                                       </li>
                                       
                                       <li><a href="http://president.valenciacollege.edu/valencia-life-and-ideas/rethinking-the-completion-agenda/">Rethinking the Completion Agenda</a></li>
                                       
                                       <li>
                                          <a href="http://cdnapi.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/33226061/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1461949808&amp;entry_id=1_a1q7omon&amp;flashvars%5BstreamerType%5D=auto" target="_blank">To Change the Work, Change the Theories</a>  
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/ficr4fYuCw8?rel=0" target="_blank"><img alt="Cultivating a Culture of Evidence" height="188" src="images/sandy-evidence-r.jpg" width="300"><br>
                                          <br>
                                          <strong>Cultivating A Culture of Evidence </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../about-valencia/roles.html">Valencia's Role</a></li>
                                       
                                       <li><a href="../about-valencia/facts.html">Valencia Facts</a></li>
                                       
                                    </ul>                  
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>VALENCIA'S BIG IDEAS </div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/oANfZmtyH_I?rel=0" target="_blank"><img alt="Anyone Can Learn Anything Under the Right Conditions" height="168" src="images/5r.png" width="300"><br>
                                          <br><strong>
                                             Big Idea 1: Anyone Can Learn Anything <br>
                                             Under the Right Conditions </strong></a> 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.youtube.com/embed/QAnWxfdRK44" target="_blank">Big Idea #1 Demonstrated: Angel Sanchez</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/Iy5V1uzmH9g?rel=0" target="_blank"><img alt="Start Right" height="168" src="images/6r.png" width="300"><br>
                                          <br>
                                          <strong>Big Idea 2: Start Right </strong></a></p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/srx0UriX2BI?rel=0" target="_blank"><img alt="Connection and Direction" height="168" src="images/7r.png" width="300"><br>
                                          <br>
                                          <strong>Big Idea 3: Connection and Direction </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="http://cdnapi.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/33226061/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1461949931&amp;entry_id=1_nkkhymcy&amp;flashvars%5BstreamerType%5D=auto" target="_blank">Pathways and Purpose</a></li>
                                       
                                       <li>
                                          <a href="https://www.youtube.com/embed/cQn4_4yJgHg" target="_blank">Culture of Personal Connection</a>  
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/vJlsoZ3u_ZA?rel=0" target="_blank"><img alt="The College Is How the Students Experience Us" height="168" src="images/8r.png" width="300"><br>
                                          <br>
                                          <strong>Big Idea 4: The College Is How the Students Experience Us </strong></a> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/U-lv1gjmGZQ?rel=0" target="_blank"><img alt="The Purpose of Assessment is to Improve Learning" height="168" src="images/9r.png" width="300"><br>
                                          <br>
                                          <strong>Big Idea 5: The Purpose of Assessment is to Improve Learning</strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="../academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/documents/HandoutofPPTOutcomestoAssessment-SAM2014WendiandLaura.pdf">Developing and Assessing Program Outcomes</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/5PPXteMfktg?rel=0" target="_blank"><img alt="Collaboration Matters" height="168" src="images/10.png" width="300"><br>
                                          <br>
                                          <strong>Big Idea 6: Collaboration</strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.youtube.com/embed/fNUh21sXmOE#t=13%23t=13" target="_blank">Collaborative Governance Model </a></li>
                                       
                                       <li>
                                          <a href="https://www.youtube.com/embed/mHyAhyyF1oA" target="_blank">Collaboration and Engagement</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/tQC03CutwcA?rel=0" target="_blank"><img alt="Collaboration as an Architectural Metaphor" height="168" src="images/sandy-collab-metaphor-r.png" width="300"><br>
                                          <br>
                                          <strong>Collaboration as an Architectural Metaphor </strong></a></p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/eiwTTGRq6Fg" target="_blank"><img alt="Our Next Big Idea" height="169" src="images/6p-bridge-r.jpg" width="300"><br>
                                          <br>
                                          <strong>Our Next Big Idea </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="../about/our-next-big-idea/documents/NovemberSummaryPresentation.pdf">QEP: Our Next Big Idea</a> 
                                       </li>
                                       
                                       <li><a href="http://wp.valenciacollege.edu/thegrove/president-shugart-summarizes-new-student-experience-the-six-ps-in-video-series/">Introduction to the 6Ps</a></li>
                                       
                                       <li><a href="../../academic-affairs/new-student-experience/index.html">The New Student Experience</a></li>
                                       
                                       <li>
                                          <a href="https://www.youtube.com/embed/bdWLAa973Co" target="_blank">New Student Experience Explained</a> 
                                       </li>
                                       
                                    </ul>                  
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <hr>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/valenciasbigideas.pcf">©</a>
      </div>
   </body>
</html>