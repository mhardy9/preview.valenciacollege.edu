<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/academicaffairs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/trustee-education/">Trustee Education</a></li>
               <li>Trustee Education &amp; Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <link href="../../includes/colorbox.css.html" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        <img alt="Dr.Susan Ledlow, vice president of academic affairs and planning" height="214" hspace="50" src="images/Susan-Ledlow-170w_000.jpg" width="153">
                        
                        
                        
                        <h2>Academic Affairs and Planning </h2>
                        
                        <p>The Division of Academic Affairs and Planning encompasses curriculum and articulation,
                           assessment and institutional effectiveness, resource development, career and workforce
                           planning and faculty and instructional development. Individually, leaders in this
                           area provide collegewide  measurement, training, planning and support of campus-based
                           academic affairs and learning support.
                        </p>
                        
                        <p>Susan Ledlow, the vice president of academic affairs and planning, provides an overview
                           of her division and highlights the major functions and roles of each of the six areas
                           of academic affairs and planning, and gives context to and a framework for the work
                           underway for the New Student Experience.
                           
                        </p>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>ACADEMIC AFFAIRS AND PLANNING OVERVIEW </div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/4SoO0HUBWOw?rel=0" target="_blank"><img alt="Academic Affairs and Planning" height="168" src="images/1.png" width="300"><br>
                                          <br>
                                          <strong>Overview of Academic Affairs and Planning</strong></a> 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../academic-affairs/index.html">Academic Affairs &amp; Planning</a></li>
                                       
                                       <li>
                                          <a href="../../academic-affairs/people.html">Academic Affairs Leadership </a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/EhLKKixa7xw?rel=0" target="_blank"><img alt="Career and Workforce Education" height="168" src="images/careerworkforce.png" width="300"><br>
                                          <br>
                                          <strong>Career and Workforce Education </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../academic-affairs/career-workforce-education/index.html">Career and Workforce Education</a></li>
                                       
                                       <li><a href="../../internship/index.html">Internship and Workforce Services</a></li>
                                       
                                       <li>
                                          <a href="../career-pathways/index.html">Career Pathways</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/krBUAmo-piI?rel=0"><strong>Curriculum and Articulation</strong></a><a href="http://www.youtube.com/embed/4SoO0HUBWOw?rel=0" target="_blank"><strong></strong></a>  
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li> <a href="../college-curriculum-committee/default.html">College Curriculum Committee</a>
                                          
                                       </li>
                                       
                                       <li> <a href="../../international/studyabroad/index.html">Study Abroad</a>
                                          
                                       </li>
                                       
                                       <li> <a href="../../honors/index.html">Honors College</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/jyyIfX8JEoE?rel=0"><strong>Faculty and Instructional Development</strong></a><a href="http://www.youtube.com/embed/4SoO0HUBWOw?rel=0" target="_blank"><strong></strong></a>  
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../faculty/development/index.html">Faculty Development</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/EjPx9DoOAcc?rel=0"><strong>Institutional Effectiveness and Planning</strong></a><a href="http://www.youtube.com/embed/4SoO0HUBWOw?rel=0" target="_blank"><strong></strong></a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../college-curriculum-committee/default.html">Institutional Research</a></li>
                                       
                                       <li><a href="../../academic-affairs/institutional-effectiveness-planning/institutional-assessment/index.html">Institutional Assessment</a></li>
                                       
                                       <li> <a href="../academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/CCSSE2013Executivesummary.pdf">Community College Survey of Student Engagement (CCSSE) 2013 Key Findings</a> 
                                       </li>
                                       
                                       <li> <a href="../academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/2013Benchmarks-Valencia.pdf">CCSSE 2013 Benchmark Scores </a>  
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/qSRvOaCepfc?rel=0"><strong>Resource Development</strong></a><a href="http://www.youtube.com/embed/4SoO0HUBWOw?rel=0" target="_blank"><strong></strong></a>  
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../academic-affairs/resource-development/default.html">Resource Development</a></li>
                                       
                                       <li> <a href="../academic-affairs/resource-development/documents/ProposalDevelopmentGuide11052012.pdf.html">Grant Development Guide</a> 
                                       </li>
                                       
                                       <li> <a href="../academic-affairs/resource-development/documents/10302012processflowchart.pdf">Grant Process Diagram</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/2uu_uDqHBWY?rel=0"><strong>Teaching/Learning Academy</strong></a><a href="http://www.youtube.com/embed/4SoO0HUBWOw?rel=0" target="_blank"><strong></strong></a>  
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../faculty/development/tla/index.html">Teaching/Learning Academy</a></li>
                                       
                                       <li>
                                          <a href="documents/14TLA002-5year-tenure-process-r0716.pdf">5 Year Tenure Process</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/xk5ci_LMirg?rel=0"><strong>The New Student Experience</strong></a><a href="http://www.youtube.com/embed/4SoO0HUBWOw?rel=0" target="_blank"><strong></strong></a>  
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../academic-affairs/new-student-experience/index.html">The New Student Experience</a></li>
                                       
                                       <li><a href="http://wp.valenciacollege.edu/thegrove/new-student-experience-spring-2014-update/">New Student Experience: Our Next Big Idea</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <hr>
                        
                        <hr>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/academicaffairs.pcf">©</a>
      </div>
   </body>
</html>