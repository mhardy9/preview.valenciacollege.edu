<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Trustee Education &amp; Preparation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/trustee-education/student-affairs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/trustee-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Trustee Education &amp; Preparation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/trustee-education/">Trustee Education</a></li>
               <li>Trustee Education &amp; Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <link href="../../includes/colorbox.css.html" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2>
                           <img alt="Kim Sepich" height="240" hspace="20" src="images/kim-sepich-BOT.jpg" width="166">Student Affairs
                        </h2>
                        
                        <p>Valencia's Student Affairs team builds systems and partnerships with our students
                           and cooperating organizations that encourage increased engagement in the educational
                           experience. Resources and tools such as advising, counseling and Valencia's LifeMap
                           system offer students the support they need to be successful in their college career.
                        </p>
                        
                        <p dir="auto">Kim Sepich, vice president of student affairs, provides a big picture view of the
                           scope and responsibilities of the division, along with details of the financial aid
                           and enrollment processes, the LifeMap developmental advising system and the DirectConnect
                           program.
                        </p>
                        
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">STUDENT AFFAIRS SCOPE AND PHILOSOPHY </div>
                                 
                                 <div data-old-tag="td">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/3geO2g9gmc4?rel=0" target="_blank"><img alt="Overview of Student Affairs" height="221" src="images/joyce-1r.jpg" width="300"><br>
                                          <br>
                                          <strong>Overview of Student Affairs </strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../students/student-affairs/index.html">Student Affairs</a></li>
                                       
                                       <li><a href="../student-services/index.html">Student Services</a></li>
                                       
                                       <li><a href="../../veterans-affairs/index.html">Veteran Affairs</a></li>
                                       
                                       <li><a href="../students/student-affairs/documents/Org.ChartStudentAffairs13-14withnames.pdf.html">Student Affairs Organization</a></li>
                                       
                                    </ul>                  
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.youtube.com/embed/lKkTM66S4ec?rel=0" target="_blank"><strong><img alt="Our Big Idea: Start Right" height="169" src="images/joyce-2r.jpg" width="300"><br>
                                             <br>
                                             Our Big Idea: Start Right</strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/big-ideas-trustees.pdf">Big Ideas</a></li>
                                       
                                       <li><a href="../students/student-affairs/documents/SAAssessmentPlanLink.pdf">Student Affairs Assessment Plan</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">COLLEGE ENROLLMENT PROCESSES </div>
                                 
                                 <div data-old-tag="td">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/AVfxIRLakCY?rel=0" target="_blank"><strong><img alt="Financial Aid" height="168" src="images/Joyce-3.png" width="300"><br>
                                             <br>Financial Aid</strong></a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../../finaid/index.html">Financial Aid</a></li>
                                       
                                       <li><a href="http://preview.valenciacollege.edu/future-students/financial-aid/">Types of Financial Aid</a></li>
                                       
                                       <li>
                                          <a href="http://www.valencia.org/">Valencia Foundation</a>  
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/-XJ1RRCsfcY?rel=0" target="_blank"><strong><img alt="Our Enrollment Process" height="168" src="images/Joyce-4.png" width="300"><br>
                                             <br>Our Enrollment Process</strong></a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="../admissions-records/steps.html">Steps to Enroll</a></li>
                                       
                                       <li><a href="../admissions-records/registration-details/default.html">Registration Details</a></li>
                                       
                                       <li><a href="../admissions-records/index.html">Admission and Records </a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">EDUCATIONAL PLANNING AND  PATHWAYS </div>
                                 
                                 <div data-old-tag="td">
                                    <div>SUPPLEMENTAL RESOURCES </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/gZEmeAdSG_c?rel=0" target="_blank"><strong><img alt="LifeMap Developmental Advising System" height="168" src="images/joyce-5r.jpg" width="300"><br>
                                             <br>LifeMap Developmental Advising System</strong></a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="http://youtu.be/GIdFaFMQlXM">Valencia Pillars: LifeMap</a></li>
                                       
                                       <li>
                                          <a href="../lifemap/index.html">LifeMap Overview</a>  
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.youtube.com/embed/DNgFUOXE3qA?rel=0" target="_blank"><strong><img alt="Direct Connect" height="170" src="images/joyce-6r.jpg" width="300"><br>
                                             <br>
                                             DirectConnect</strong></a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="http://youtu.be/e4i_5Tj25LM">Valencia Pillars: DirectConnect</a></li>
                                       
                                       <li><a href="http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/">DirectConnect to UCF</a></li>
                                       
                                       <li><a href="http://regionalcampuses.ucf.edu/directconnect/">UCF Connect Valencia Campuses </a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/trustee-education/student-affairs.pcf">©</a>
      </div>
   </body>
</html>