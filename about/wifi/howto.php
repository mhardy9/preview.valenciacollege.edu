<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Wireless Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/wifi/howto.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/wifi/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Wireless Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/wifi/">Wifi</a></li>
               <li>Wireless Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="index.html"></a>
                        
                        
                        <h2>Accessing the New Valencia College Wireless Networks</h2>
                        
                        <p>Starting in the Spring 2017 semester, Valencia College will be splitting their present
                           wireless network, VCNet, into three different wireless networks.  They will be visible
                           to users as:<br>
                           
                           <strong>VC-Staff</strong> — Valencia College Staff (faculty/non-faculty)<br>
                           <strong>VC-Student</strong> — Valencia College Students<br>
                           <strong>VC-Guest</strong> — Valencia College Visitors and Guests<br>
                           
                           
                        </p>
                        
                        
                        
                        <p>Throughout the Spring 2017 semester, these wireless networks will be advertised concurrently
                           with the existing VCNet wireless.  By the start of the Fall 2017 semester, the VCNet
                           wireless will cease to be offered.  This gives all users many months to transfer any
                           wireless devices they have previously been using on VCNet over to the appropriate
                           new wireless SSID.
                        </p>
                        
                        
                        <a name="simple" id="simple"></a>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Windows 10 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Logging on to the VC-Staff or VC-Student wireless networks is accomplished in exactly
                           the same fashion as the existing VCNet network.  You choose the wireless network from
                           the list of available networks on your device.  (In Windows 10, click in the lower-right
                           corner of the screen and select <strong>"Settings"</strong> from the menu that appears on the right of the screen.)
                        </p>
                        
                        <p><img on="" or="" src="Wifi1.png" title="Logging on to VC-Staff or VC-Student" to="" vc-staff="" vc-student=""></p>
                        
                        
                        <p>Then select the wireless network of your choice.  You will want to check the <strong> "Connect automatically"</strong> box if you intend to use this wireless network repeatedly, and then click the <strong> "Connect" </strong>button.
                        </p>
                        
                        
                        <p><img alt="Connect automatically and then click the Connect button" src="Wifi9.png" title="Connect automatically and then click the Connect button"></p>
                        <br>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Mac desktop/laptops</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>On Mac desktop/laptops, the new SSIDs will be available via the wireless icon on the
                           top-right of your desktop screen  
                        </p>
                        
                        
                        <p><img alt="Screenshot for Mac desktop/laptops" src="Wifi2.png" title="Screenshot for Mac desktop/laptops"></p>
                        <br>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Apple iOS devices</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>On Apple iOS devices, open the <strong>"Settings" </strong>app icon and select <strong>"Wi-Fi" </strong>from the menu on the left-hand side.  Then select the appropriate wireless network
                           on the right.
                        </p>
                        
                        
                        <p><img alt="Screenshot for Settings on Apple iOS devices" src="Wifi3.png" title="Screenshot for Settings on Apple iOS devices"></p>
                        
                        <br>
                        <a name="hybrid" id="hybrid"></a>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Android devices</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>On most current Android devices, go to your WiFi, then select the applicable <strong>"VC"</strong> SSID
                        </p>
                        
                        <p><img alt="Android devices settings icons" src="Wifi4.png" title="Android devices settings icons"></p>
                        
                        
                        <p><img alt="Android devices settings" src="Wifi5.png" title="Android devices settings"></p>
                        
                        <br>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div> (HTTP, not HTTPS) webpage </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>Once the correct network has been chosen, open your browser and navigate to any unencrypted
                           <strong>(HTTP, not HTTPS) webpage </strong><a href="http://www.valenciacollege.edu/wifi"><strong>(valenciacollege.edu/wifi</strong></a><strong>)</strong>. You will be redirected to the log-in page, displayed below, where you enter your
                           username and password
                        </p>
                        <br>
                        
                        
                        <p><img alt="Welcome to the Valencia College Network Login Page" src="Wifi6.png" title="Welcome to the Valencia College Network Login Page"></p>
                        
                        <br>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Logging on to the VC-Guest Wireless Network</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>To log in to the VC-Guest wireless network, you choose VC-Guest from the available
                           networks, and open your browser.  You will be directed to the login page.  If this
                           is your first time logging into VC-Guest (in the last seven days), click the <strong>"Request Guest Account" </strong>button at the bottom of the screen.  
                        </p>
                        <br>
                        
                        
                        <p><img alt="Welcome to the Valencia College Network Login Page - Request Guest Account" src="Wifi7.png" title="Welcome to the Valencia College Network Login Page - Request Guest Account"></p>
                        <br>
                        
                        <p>You will be prompted to enter your information, as shown below:</p>
                        
                        
                        <p><img alt="Guest Access Request" src="Wifi8.png" title="Guest Access Request"></p>
                        
                        
                        <p>
                           You enter your email address, full name, and your phone number.  If you select one
                           of the listed Mobile Service Providers, it will text the confirmation with your randomly-generated
                           password to your mobile phone.  Click <strong>"Submit" </strong>to initiate account creation, the system will automatically log you into the network
                           within a few seconds.  
                           
                        </p>
                        
                        
                        <p> <strong>Caveat:</strong> These credentials will be good for 12 hours. If you do not have a phone with SMS
                           capability, do not get the text with your password, or lose the password before the
                           account expires, you will have to request access again using a different email address
                           if you wish to connect to the VC-Guest network for subsequent sessions.
                        </p>
                        <br>
                        
                        <p>As always, should you encounter any issues, please contact the related support team:<br>
                           <a href="mailto:oitservicedesk@valenciacollege.edu"><strong>oitservicedesk@valenciacollege.edu</strong></a></p>
                        
                        <strong>  
                           <p>Faculty &amp; Staff Call 407-582-5555 - Students Call 407-582-5444</p>
                           </strong>  
                        
                        
                        
                        <p><a href="howto.html#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/wifi/howto.pcf">©</a>
      </div>
   </body>
</html>