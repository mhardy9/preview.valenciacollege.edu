<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Progression to Degree  | Valencia College</title>
      <meta name="Description" content="Students enrolled while completing their 16 to 44 credit hours of college course work are implementing career and educational plans and confirming decisions about their goals.">
      <meta name="Keywords" content="progression, degree, lifemap, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/progression-to-degree.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>Progression to Degree </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Success Indicators</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Students enrolled while completing their 16 to 44 credit hours of college course work
                              are implementing
                              career and educational plans and confirming decisions about their goals. Students
                              benefit from exploring,
                              adjusting or confirming their career and educational goals and connecting with Valencia
                              resources and
                              educational experiences to enhance their educational experience.
                           </p>
                           
                           <ul class="list-style-1">
                              
                              <li>Revise education plan as needed documenting progression toward goal.</li>
                              
                              <li>Determine career options in My Career Planner.</li>
                              
                              <li>Develop financial literacy and a financial plan for continued education and life after
                                 graduation
                                 using My Financial Planner.
                                 
                              </li>
                              
                              <li>Complete General Education requirements.</li>
                              
                              <li>Examine and reflect on what you learned in terms of the defined program learning outcomes
                                 for chosen
                                 degree.
                                 
                              </li>
                              
                              <li>If seeking an Associate of Arts degree to transfer to an upper-division program, determine
                                 pre-requisites that may be needed for intended degree at the transfer institution.
                                 
                              </li>
                              
                              <li>Explore chosen career field by participating in internships, job shadowing, information
                                 interviews,
                                 volunteer experiences or by conducting internet searches.
                                 
                              </li>
                              
                              <li>Explore options for continued education or employment after graduation.</li>
                              
                              <li>Maintain social connections on campus with staff, students, advisors, and faculty.</li>
                              
                              <li>Continue to participate in campus activities or organizations.</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Programs and Services</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>For more information about these programs, go to the Answer Center on any campus.</p>
                           
                           <h4>Academic Warning System</h4>
                           
                           <p>Students whose grade point average and completion of course rates fall below 2.0 G.P.A.
                              are placed on
                              academic warning or probation and receive special advising intervention and coaching
                              in success strategies
                              to assist them in improving their academic performance. Students who are re-admitted
                              after academic
                              suspension must meet with an Academic Advisor, Career Advisor or Counselor and are
                              required to have their
                              educational plan and to meet with them during the term.
                           </p>
                           
                           <h4>Professional Development Days</h4>
                           
                           <p>Collegewide staff development programs for Student Affairs are held three times a
                              year to correspond with
                              the preparation for the next registration period. During the half-day workshop, staff
                              participates in
                              learning activities to enhance their understanding of LIFEMAP and develop improved
                              strategies to assist
                              students.
                           </p>
                           
                           <h4>AA Degrees with Pre-Majors</h4>
                           
                           <p>In an effort to assist students with their long-term career plans, Valencia has articulated
                              pre-majors.
                              These pre-major programs are designed for easy articulation into Florida’s State University
                              System. They
                              also facilitate a student’s academic progress through Valencia by providing a well-developed
                              plan of
                              action from term to term.
                           </p>
                           
                           <h4>Career Centers </h4>
                           
                           <p>The Career Center assist students with the opportunity for take assessments and make
                              career decisions.
                              Those students who have already taken the Student Success class may choose to use
                              My Career Planner and My
                              Job Prospects to be sure they are aligned with current skills and training requirements
                              for today’s
                              fast-paced job market. Students may also check current transfer requirements for their
                              chosen University
                              or College. 
                           </p>
                           
                           <h4>Core Competencies</h4>
                           
                           <p>As students progress toward their degrees, they are given repeated opportunities to
                              employ Valencia’s
                              Core Competencies – THINK, VALUE, COMMUNICATE, ACT. The goal of the college is to
                              assist students to THINK
                              clearly, critically, and creatively; to analyze, synthesize, integrate, and evaluate
                              in many domains of
                              human life; to make reasoned VALUE judgments and responsible commitments; to COMMUNICATE
                              with different
                              audiences using varied means, and to ACT purposefully, reflectively, and responsibly.
                           </p>
                           
                           <h4>My Portfolio</h4>
                           
                           <p>My Portfolio enables students to work with faculty and staff to document mastery of
                              the Core
                              Competencies: think, value, communicate, act. The portfolio enables students to capture
                              demonstrations of
                              learning (via assignments, written work, art work, co-curricular activities) from
                              specific courses and
                              experiences throughout their educational program.
                           </p>
                           
                           <h4>Ethnically Diverse Student Programs</h4>
                           
                           <p>The Coordinator of the Recruitment and Retention of Ethnically Diverse students has
                              developed a number of
                              strategies to specially assist these traditionally under-represented students. Letters
                              are sent to all
                              students at the beginning of the academic year to inform them about special services,
                              collegewide
                              workshops, and strategies for success. Career interest forms are included which students
                              are invited to
                              return. Follow-up with the students that return the interest cards includes academic
                              warning
                              interventions, congratulations on academic achievement, and invitation to special
                              programs based on career
                              interests.
                           </p>
                           
                           <h4>Honors Program</h4>
                           
                           <p>Valencia’s Honors Program provides the academically gifted and highly motivated student
                              with an enriched
                              program of course work and extra-curricular activities. The program focuses on developing
                              the student’s
                              critical thinking skills and helping the student become an independent learner.
                           </p>
                           
                           <h4>Internships</h4>
                           
                           <p>Valencia boast one of the largest internship programs in Florida. Each year over 200
                              employers provide
                              opportunities for more than 500 students in areas ranging from accounting to zoology.
                              To see what
                              opportunities are available, visit our website at: www.valenciacollege.edu/internship
                              
                           </p>
                           
                           <h4>Leadership Development</h4>
                           
                           <p>The Student Development office fosters the development of leadership skills. The annual
                              Student
                              Leadership Symposium is one opportunity for students from all campuses to explore
                              leadership theories and
                              practices and to interact with each other. 
                           </p>
                           
                           <h4>Student Leaders</h4>
                           
                           <p>Peer Educators are student leaders who are recruited for a one-year term to work at
                              Valencia. They assist
                              with office responsibilities, become educators in health and wellness issues, such
                              as alcohol awareness,
                              personal safety, safe sex, and general wellness practices, and serve as ambassadors
                              to the college.
                           </p>
                           
                           <p>Welcome Team are student leaders who assist with individual tours, orientation tours
                              and assist the
                              Student Development Office with other college-wide activities. They assist students
                              in the Information
                              Station and provide assistance with college-wide and campus events.
                           </p>
                           
                           <p>Atlas Access Lab leaders are student leaders who assist inside the Atlas Access Labs
                              on each campus. They
                              assist students with registration, financial aid, and admissions processes. In addition,
                              they support New
                              Student Orientation by giving the overview of Atlas and support the Student Success
                              classes by presenting
                              the LifeMap Tools introduction. 
                           </p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side">
                        
                        <h3>Resource Links</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="https://preview.valenciacollege.edu/careercenter/">Career Development Services</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/Labs/">Computer Labs</a></li>
                           
                           <li><a href="http://www.floridashines.org/">FloridaShines.org</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/finaid/">Financial Aid</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/library/">Library</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook (pdf)</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/progression-to-degree.pcf">©</a>
      </div>
   </body>
</html>