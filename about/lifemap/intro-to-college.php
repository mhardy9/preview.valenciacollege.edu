<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Introduction to College  | Valencia College</title>
      <meta name="Description" content="Students enrolled in their first 15 credit hours of college course work are making important transitions and connections that will impact success at Valencia.">
      <meta name="Keywords" content="introduction, college, lifemap, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/intro-to-college.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>Introduction to College </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               				
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="box_style_1">
                           
                           <div class="indent_title_in">
                              
                              <h3>Success Indicators</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>Students enrolled in their first 15 credit hours of college course work are making
                                 important transitions
                                 and connections that will impact success at Valencia. Valencia assists students in
                                 the beginning of their
                                 college career to make the connections and directions needed to plan a successful
                                 college experience.
                              </p>
                              
                              <ul>
                                 
                                 <li>Develop, save, and follow an educational plan that will guide course selection toward
                                    degree
                                    completion using My Education Plan.
                                    
                                 </li>
                                 
                                 <li>Successfully complete (with an A, B, or C) all coursework in which enrolled.</li>
                                 
                                 <li>Reduce the number of remaining preparatory courses each term by enrolling in the next
                                    level of the
                                    sequence until completion.
                                    
                                 </li>
                                 
                                 <li>Create and follow a study plan for each enrolled course.</li>
                                 
                                 <li>Use campus resources (writing centers, tutoring, library, Skillshops, etc.)</li>
                                 
                                 <li>Identify Life Goals by completing the LifeMap Student Handbook exercise.</li>
                                 
                                 <li>Identify and research a potential career path using My Career Planner.</li>
                                 
                                 <li>Engage with students, staff, advisors, and faculty on campus.</li>
                                 
                                 <li>Participate in campus activities or organizations.</li>
                                 
                                 <li>Write down intended graduation date.</li>
                                 
                              </ul>
                              
                           </div>
                           
                           <hr class="styled_2">
                           
                           <div class="indent_title_in">
                              
                              <h3>Programs and Services</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>For more information about these programs, go to the Answer Center on any campus.</p>
                              
                              <h4>Advanced Placement (AP) and CLEP</h4>
                              
                              <p>Students who transfer AP credit from high school are awarded college credit for specific
                                 courses.
                                 Students can take CLEP examinations in specific subject areas and earn college credit
                                 based on appropriate
                                 scores and satisfy degree requirements. Both programs provide accelerated mechanisms
                                 for degree completion
                                 at Valencia.
                              </p>
                              
                              <h4>Advising Presentations</h4>
                              
                              <p>Counselors and Academic Advisors make classroom presentations to students enrolled
                                 in Student Success and
                                 in English for Academic Purposes courses. These presentations include degree completion
                                 requirements and
                                 specifically prepare students for enrollment in courses for the next term.
                              </p>
                              
                              <h4>Bridges to Success</h4>
                              
                              <p>Recent high school graduates selected for this program enroll in courses during the
                                 summer term as a
                                 transition to their first year in college. Students participate in workshops and support
                                 services designed
                                 to assist with college transition. Students who successfully complete the program
                                 are eligible for a
                                 two-year scholarship at Valencia.
                              </p>
                              
                              <h4>Skillshops </h4>
                              
                              <p>Skillshops are offered to assist students succeed in college and life. These interactive
                                 workshops
                                 include topics such as time management, stress management, study skills, note taking,
                                 math and speech
                                 anxiety, career planning, and diversity awareness.
                              </p>
                              
                              <h4>PERT Preparation </h4>
                              
                              <p>The PERT is the standard assessment test used in all Florida community colleges. Valencia
                                 has designed
                                 study guides for entering students. You can find the study guides and review sessions
                                 on the Assessments
                                 website under PERT.
                              </p>
                              
                              <h4>My Education Plan</h4>
                              
                              <p>This education planning tool enables students to plan their courses for a specific
                                 degree at Valencia.
                                 Students can save several educational plans and make adjustments to their course loads
                                 in order to
                                 successfully progress through their degree requirements.
                              </p>
                              
                              <h4>My Career Planner</h4>
                              
                              <p>This career planning tool enables students to make career decisions. This tool guides
                                 students through
                                 assessments to explore self, majors, and occupations; examine options and then set
                                 goals. Students can
                                 share their outcomes with the Career Advisors and Counselors. 
                              </p>
                              
                              <h4>Honors Program</h4>
                              
                              <p>Students who qualify for the Honors Program and have not yet applied are identified
                                 through the New
                                 Student Orientation program and offered the opportunity to apply for acceptance. Students
                                 enrolled in the
                                 Honors Program receive special advising and registration prior to our regularly scheduled
                                 registration
                                 period. Honors courses are small and offer in-depth study of subject matter. Honors
                                 graduates have special
                                 opportunities for transfer scholarships and admission to competitive four-year colleges
                                 and
                                 universities.
                              </p>
                              
                              <h4>Learning in Community (LINC)</h4>
                              
                              <p>Students enroll as a cohort into the same set of courses (usually 2 or 3 courses)
                                 that are taught
                                 collaboratively by faculty who coordinate their syllabi and learning activities. Learning
                                 communities
                                 support student success through shared support systems and complimentary curricula
                                 that enrich the
                                 learning experience. 
                              </p>
                              
                              <h4>Math and Communications Support Centers</h4>
                              
                              <p>Instructional assistants provide specialized learning systems including computer-based
                                 tutorials,
                                 supplemental instructional materials, practice assessments, and tutoring to support
                                 student success in
                                 these academic areas.
                              </p>
                              
                              <h4>New Student Orientation</h4>
                              
                              <p>All degree-seeking students are required to attend an orientation session in which
                                 the curriculum
                                 introduces them to Valencia, core competencies and LIFEMAP, degree options and requirements,
                                 support
                                 services, and prepares them for registration in first semester courses. Students can
                                 register for courses
                                 based on an assigned registration time ticket.
                              </p>
                              
                              <h4>RoadMap to Success Award</h4>
                              
                              <p>Students enrolled in Student Success classes are given the opportunity to complete
                                 criteria based on
                                 "best practices" for academic success and to earn a $500.00 award. Students must meet
                                 with an Academic
                                 Advisor, Career Program Advisor or Counselor to certify their completion of the criteria.
                              </p>
                              
                              <h4>Student Development</h4>
                              
                              <p>Student Government Associations, a wide variety of student clubs and organizations,
                                 campus activities,
                                 intramural sports, and Valencia Volunteer programs are actively available for students
                                 who want to get
                                 involved in campus life. Opportunities are promoted through Welcome Tables, Student
                                 Success classes, club
                                 and activity fairs, Matador Day week, and advertisements on campus. 
                              </p>
                              
                              <h4>Student Handbook</h4>
                              
                              <p>The Student Handbook is the "text" for LIFEMAP. It describes what LIFEMAP is, and
                                 provides
                                 self-assessments and information to guide students through Life Goals, Career Goals,
                                 Educational Plans,
                                 Building a Schedule, and Academic Success Skills. College information is provided
                                 in the chapter that is
                                 relevant to each topic. A weekly calendar provides key college dates and allows students
                                 to record their
                                 assignments and academic schedule. "To Do" cues are provided each week for students
                                 according to their
                                 LIFEMAP stage.
                              </p>
                              
                              <h4>Student Success Course</h4>
                              
                              <p>The Student Success course is a 3-hour, elective college level credit course in which
                                 all new students
                                 are strongly encouraged to enroll. The curriculum focuses on career and educational
                                 planning,
                                 understanding self and learning styles, and academic success skills. Student Success
                                 faculty employ active
                                 and collaborative learning strategies to engage students in the learning process.
                                 Students develop a
                                 Learning Portfolio which starts the documentation of their learning and achievement
                                 of Core Competencies
                                 at Valencia. Faculty who teach Student Success participate in faculty development
                                 workshops to prepare for
                                 the course and improve their teaching.
                              </p>
                              
                              <h4>Atlas Access </h4>
                              
                              <p>These laboratories have computers for student access to Atlas and the LifeMap tools
                                 (My Education Plan,
                                 My Career Planner, My Portfolio, My Financial Planner and My Job Prospects), the Internet,
                                 and e-mail.
                                 Student Success classes use the Atlas Access as learning laboratories for certain
                                 class periods. Students
                                 are welcome to drop-in to the Atlas Access at any time. 
                              </p>
                              
                              <h4>Tutoring Centers</h4>
                              
                              <p>Free tutoring is available for students who need additional academic support through
                                 one-on-one tutoring
                                 in specific courses. Supplemental Instruction models are implemented on some campuses.
                                 Tutors who are
                                 hired have the opportunity to earn a supplemental income while working on campus.
                                 
                              </p>
                              
                              <h4>Preparatory Students</h4>
                              
                              <p>Students who are not ready for college level course work in English, reading, and
                                 mathematics are placed
                                 in college preparatory courses based on their assessment scores. Policies and procedures
                                 are in place to
                                 ensure appropriate placement into these courses. Faculty design and implement creative
                                 instructional
                                 strategies to improve student success. Support services such as the Math and Communications
                                 Support
                                 Centers, tutoring, students with disabilities services, and academic success workshops
                                 assist students in
                                 completing these courses.
                              </p>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        <div class="box_side">
                           
                           <h3>Resource Links</h3>
                           
                           <ul>
                              
                              <li><a href="https://preview.valenciacollege.edu/careercenter/">Career Development Services</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/Labs/">Computer Labs</a></li>
                              
                              <li><a href="http://www.floridashines.org/">FloridaShines.org</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/finaid/">Financial Aid</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/library/">Library</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook (pdf)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/intro-to-college.pcf">©</a>
      </div>
   </body>
</html>