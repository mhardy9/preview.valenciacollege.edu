<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>PBS/Valencia LifeMap | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/pbs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/pbs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>Pbs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p> </p>
               
               <p>&nbsp; 
                  
               </p>
               
               <table class="table ">
                  
                  <tr> 
                     
                     <td> 
                        
                        <table class="table ">
                           
                           
                           <tr>   
                              
                              <td><img src="http://preview.valenciacollege.edu/lifemap/pbs/images/shim.gif" width="475" height="1" border="0"></td>
                              
                              <td><img src="http://preview.valenciacollege.edu/lifemap/pbs/images/shim.gif" width="225" height="1" border="0"></td>
                              
                              <td><img src="http://preview.valenciacollege.edu/lifemap/pbs/images/shim.gif" width="1" height="1" border="0"></td>
                              
                           </tr>
                           
                           <tr valign="top">
                              
                              
                              <td colspan="2"><img name="maintitle_r1_c1" src="http://preview.valenciacollege.edu/lifemap/pbs/images/maintitle_r1_c1.gif" width="700" height="50" border="0" id="maintitle_r1_c1"></td>
                              
                              <td><img src="http://preview.valenciacollege.edu/lifemap/pbs/images/shim.gif" width="1" height="50" border="0"></td>
                              
                           </tr>
                           
                           <tr valign="top">
                              
                              
                              <td><img name="maintitle_r2_c1" src="http://preview.valenciacollege.edu/lifemap/pbs/images/maintitle_r2_c1.jpg" width="475" height="120" border="0" id="maintitle_r2_c1"></td>
                              
                              <td><img name="maintitle_r2_c2" src="http://preview.valenciacollege.edu/lifemap/pbs/images/maintitle_r2_c2.gif" width="225" height="120" border="0" id="maintitle_r2_c2"></td>
                              
                              <td><img src="http://preview.valenciacollege.edu/lifemap/pbs/images/shim.gif" width="1" height="120" border="0"></td>
                              
                           </tr>
                           
                           
                           
                        </table>
                        
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     
                     <td> 
                        
                        <p><font face="Arial, Helvetica, sans-serif"><br>
                              In this teleconference, you will learn what LIFEMAP is, why and how we 
                              developed the conceptual framework on which it is based, how it has changed 
                              the way in which we interact with students, and most importantly, the 
                              student learning results we have documented. </font></p>
                        
                        <p>&nbsp; </p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <table class="table ">  
                  <tr bgcolor="#000000">     
                     <td>       
                        <p align="center"><strong><font face="Arial, Helvetica, sans-serif"><a href="program.htm"><font size="-1">Program         Outline</font></a><font size="-1"> | </font></font></strong><font size="-1"><strong><font face="Arial, Helvetica, sans-serif"><a href="panelists.htm">Moderator         and Panelists</a> | <a href="damodel.htm">DA Model</a> | <a href="../index.html">LifeMap</a>         | <a href="../../students/student-success/index.html">Student         Success</a></font></strong></font></p>    
                     </td>  
                  </tr>
                  
               </table>
               
               <div align="center"></div>
               
               <p align="center">&nbsp;</p>
               
               <p align="center">&nbsp; </p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/pbs/index.pcf">©</a>
      </div>
   </body>
</html>