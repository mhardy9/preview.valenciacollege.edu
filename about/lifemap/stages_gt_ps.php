<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/stages_gt_ps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <h2>Graduation Transition Programs and Services        </h2>
                                    
                                    <p>For more information about  these programs, contact the Answer Centers on any campus.
                                       <br>
                                       <br>
                                       <strong>Career Center</strong><br>
                                       The office where a variety of services are available to  assist students in making
                                       career decisions, setting career goals, and preparing  for a job search.  Click <a href="../../students/career-center/index.html">here</a> for more information. <br>
                                       <br>
                                       <strong>College Night<br>
                                          </strong>Each October, Valencia College hosts College  Night - an event intended to connect
                                       high school students with a variety of  post-secondary opportunities. Typically, we
                                       host over 170 colleges and  universities from around the country to connect them with
                                       students from our  community to discuss transfer requirements, housing, and financial
                                       aid.
                                    </p>
                                    
                                    <p><strong>My</strong> <strong>Portfolio<br>
                                          </strong>Throughout their tenure at Valencia, students are encouraged to create a  portfolio
                                       that illustrates their skills and experiences. My Portfolio is an  electronic version
                                       of the portfolio that can be displayed in a public area for  potential employers to
                                       review. As students near graduation, they can analyze  the contents of their portfolio
                                       to determine the most relevant examples of  mastery of each of Valencia's core competencies
                                       - Think, Value, Communicate,  Act - at an advanced level in their academic programs.
                                       <br>
                                       <br>
                                       <strong>Job Fairs<br>
                                          </strong>The Workplace Learning and Placement Office also arranges on-campus Job  Fairs at
                                       which time students may meet employers who are actively recruiting.  They have the
                                       opportunity to ask questions about potential employers, and get a  first-hand impression
                                       of the companies represented.<br>
                                       <br>
                                       <strong>Job Search Skills Course<br>
                                          </strong>Valencia offers a 1-credit course in Job Search Skills. Students learn to  write resumes,
                                       compile portfolios, find leads, search the Internet, and utilize  other skills in
                                       seeking employment. They also can participate in mock  interviews.<br>
                                       <br>
                                       <strong>Internship and Workforce  Services</strong><br>
                                       Valencia boast one of the largest  internship programs in Florida. Each year over
                                       200 employers provide  opportunities for more than 500 students in areas ranging from
                                       accounting to  zoology. To see what opportunities are available, visit our <a href="http://www.valenciacollege.edu/internship">website</a>. 
                                    </p>      
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/stages_gt_ps.pcf">©</a>
      </div>
   </body>
</html>