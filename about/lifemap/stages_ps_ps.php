<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/stages_ps_ps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td"> 
                                    <h2>
                                       <strong>College Transition 
                                          Programs and Services</strong> 
                                       
                                    </h2>
                                    
                                    <p><strong>Answer Center</strong><br>
                                       Valencia’s Answer Center is your  place to go for questions about your educational
                                       plans, including the  enrollment process on how to apply for admission and financial
                                       aid. Visit the  Answer Center on any campus for help or <a href="../admissions-records/steps.html">click here</a> for  more information.                  
                                       
                                    </p>
                                    
                                    <p><strong>Bridges to Success</strong><br>
                                       Mentoring, academic and financial  support is provided for low income, first generation
                                       in college or minority  students who are graduates of Orange or Osceola high schools.&nbsp;
                                       Admission  to Bridges to Success is selective.&nbsp; For more information, <a href="../../students/bridges-to-success/index.html">click here.</a><u></u></p>
                                    
                                    <p><strong>Campus Tours</strong><br>
                                       The Valencia Welcome Team gives campus tours at different campuses. Visit  our <a href="http://preview.valenciacollege.edu/future-students/visit-valencia">website</a> to schedule a tour.
                                    </p>
                                    
                                    <p><strong>Career Development</strong><br>
                                       An  important component of LifeMap is Career Development, which focuses on helping
                                       students explore and identify career goals. Explore the world of work by  visiting
                                       <a href="../../students/career-center/index.html">Career Development  Services</a>.  
                                    </p>
                                    
                                    <p><strong>Career Pathways (Tech Prep)</strong><br>
                                       High school students enroll in  career programs and earn college credit by completing
                                       specified coursework and  an assessment.&nbsp; Articulation agreements allow students to
                                       achieve a  seamless transition from high school to college and save time and money
                                       by  earning college credit while in high school.<br>
                                       <br>
                                       <strong>College Night</strong><br>
                                       The Central Florida community is  invited to visit Valencia to meet with more than
                                       130 other college  representatives in a college fair forum during two evenings in
                                       October.
                                    </p>
                                    
                                    <p><strong>Direct Connect to UCF</strong><br>
                                       Valencia has a partnership with UCF  that will guarantee admission to UCF for any
                                       student who graduates with an Associate  of Arts degree from Valencia.&nbsp; UCF staff
                                       are located on Valencia campuses  to meet with students and encourage a smooth transition.
                                    </p>
                                    
                                    <p><strong>Dual Enrollment</strong> <br>
                                       An opportunity for high school  students with a 3.0 grade point average or higher
                                       and college-level placement  scores to take college-level courses and earn both high
                                       school and college  credit, free of charge.&nbsp; 
                                    </p>
                                    
                                    <p><strong>Enrollment Services</strong> <br>
                                       For telephone assistance regarding  admission, registration or financial aid call
                                       407-582-1507.
                                    </p>
                                    
                                    <p><strong>Financial Aid</strong> <br>
                                       Valencia offers a wide range of  financial aid programs including grants, scholarships,
                                       student loans and work  study programs. <a href="../../finaid/index.html">Click here</a> for  more information. 
                                    </p>
                                    
                                    <p><strong>FAFSA Frenzy</strong><br>
                                       Central Florida high school seniors  and their parents can receive one on one help
                                       with completing their financial  aid applications. <a href="../../finaid/fafsafrenzy.html">Click  here</a> for more information.
                                    </p>
                                    
                                    <p><strong>Group Visits</strong><br>
                                       Group visits are designed for  schools or organizations interested in bringing a group
                                       of eight or more people  to learn more about Valencia College.&nbsp; Topics may include:
                                       planning for  college, how to enroll, financial aid, and will be followed by a campus
                                       tour  experience.&nbsp; If you are interested in requesting a group visit. Visit our <a href="http://preview.valenciacollege.edu/visit">website</a> for more information. &nbsp; 
                                    </p>
                                    
                                    <p><strong>High School Visits</strong><br>
                                       During the fall and spring of the  academic year, Valencia Coordinators will visit
                                       with high school juniors and  seniors to share enrollment information.
                                    </p>
                                    
                                    <p><strong>Learning in Community  (LinC)</strong><br>
                                       Valencia College offers a variety of learning  communities for students to participate
                                       in. Some of the most popular models  include REACH, CAMINO, First 30, and LinC. Learning
                                       communities are regarded as  a High Impact Educational Practices and have shown to
                                       increase student's  retention and success in college. The key goals for learning communities
                                       are to  encourage integration of learning across courses and to involve students with
                                       "big questions" that matter beyond the classroom (AAC&amp;U, 2008).  Students often build
                                       stronger bonds with their peers, including classmates,  faculty and staff at the college
                                       because they spend more time together. <a href="../../academics/learning-in-community/index.html">Click here</a> to learn more.
                                    </p>
                                    
                                    <p><strong>New Student Orientation  (NSO)</strong><br>
                                       Before registering  for classes, all degree-seeking students (freshmen and transfer)
                                       are required  to attend an Orientation session. Sessions fill up fast, so be sure
                                       to <a href="../../admissions/orientation/index.html">sign up</a> and attend early! 
                                    </p>
                                    
                                    <p><strong>Valencia College Previews</strong></p>
                                    The College Transition team encourages  interested students or community groups to
                                    visit Valencia for a presentation  and a campus tour.&nbsp; To schedule a Valencia Preview,
                                    visit our <a href="http://preview.valenciacollege.edu/visit/">website</a>.                
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>     
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/stages_ps_ps.pcf">©</a>
      </div>
   </body>
</html>