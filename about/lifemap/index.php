<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>LifeMap  | Valencia College</title>
      <meta name="Description" content="LifeMap is a student's guide to figuring out 'what to do when' in order to complete their career and education goals.">
      <meta name="Keywords" content="lifemap, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Lifemap</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Lifemap</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="box_style_1">
                           
                           <div class="indent_title_in">
                              
                              <h3>What is Lifemap?</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>LifeMap is a student's guide to figuring out "what to do when" in order to complete
                                 their career and
                                 education goals. LifeMap links all of the components of Valencia (faculty, staff,
                                 courses, technology,
                                 programs, services) into a personal itinerary to help students succeed in their college
                                 experience.
                              </p>
                              
                              <h4>Guide to My LifeMap</h4>
                              
                              <ul class="list-style-1">
                                 
                                 <li>LifeMap tools for planning your education and career are within Atlas on the My LifeMap
                                    tab.
                                 </li>
                                 
                                 <li>LifeMap stage links at the top of the screen provide more detail on how to progress
                                    through Valencia
                                    programs and services.
                                    
                                 </li>
                                 
                                 <li>LifeMap resource links on the right bar provide additional Valencia resources.</li>
                                 
                              </ul>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        <div class="box_side">
                           
                           <h3>Resource Links</h3>
                           
                           <ul class="list-style-1">
                              
                              <li><a href="https://preview.valenciacollege.edu/careercenter/">Career Development Services</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/Labs/">Computer Labs</a></li>
                              
                              <li><a href="http://www.floridashines.org/">FloridaShines.org</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/finaid/">Financial Aid</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/library/">Library</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook (pdf)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/index.pcf">©</a>
      </div>
   </body>
</html>