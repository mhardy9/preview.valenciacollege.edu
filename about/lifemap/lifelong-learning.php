<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lifelong Learning  | Valencia College</title>
      <meta name="Description" content="After students transition into the workforce, they will discover the need for continued learning, retooling of skills/knowledge and the need to create new skills. Students will be able to re-create the cycle of setting goals, evaluating options, identifying additional educational needs, and acting to meet those educational needs at Valencia.">
      <meta name="Keywords" content="lifelong, learning, lifemap, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/lifelong-learning.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Lifemap</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>Lifelong Learning </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               				
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="box_style_1">
                           
                           <div class="indent_title_in">
                              
                              <h3>Success Indicators</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>After students transition into the workforce, they will discover the need for continued
                                 learning,
                                 retooling of skills/knowledge and the need to create new skills. Students will be
                                 able to re-create the
                                 cycle of setting goals, evaluating options, identifying additional educational needs,
                                 and acting to meet
                                 those educational needs at Valencia.
                              </p>
                              
                              <ul class="list-style-1">
                                 
                                 <li>Set goals that reflect ability to think critically, evaluate opportunities, identify
                                    additional
                                    educational needs, and act to meet those educational needs at Valencia.
                                    
                                 </li>
                                 
                                 <li>Document professional development by maintaining a resume and portfolio that reflect
                                    continual
                                    improvement.
                                    
                                 </li>
                                 
                                 <li>In periods of career transition, re-tooling, or acquiring new skills, re-engage the
                                    cycle of goal
                                    setting, career development, and educational planning that you learned at Valencia.
                                    
                                 </li>
                                 
                                 <li>Build and maintain professional networks.</li>
                                 
                              </ul>
                              
                           </div>
                           
                           <hr class="styled_2">
                           
                           <div class="indent_title_in">
                              
                              <h3>Programs and Services</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>For more information about any of these programs, contact the Continuing Education
                                 office at 407-582-6931
                                 or Student Services on any campus.
                              </p>
                              
                              <h4>Credit Courses</h4>
                              
                              <p>Valencia provides credit courses that are open to anyone interested in the subject
                                 matter. These Special
                                 Interest students will find classes for upgrading job skills, working toward Teacher
                                 Recertification,
                                 personal growth or a Technical Certificate.
                              </p>
                              
                              <p>Classes are offered in a variety of formats:</p>
                              
                              <ul class="list-style-1">
                                 
                                 <li>Video-on-Demand</li>
                                 
                                 <li>Online Courses</li>
                                 
                                 <li>Hybrid Courses</li>
                                 
                                 <li>Individualized Learning Courses</li>
                                 
                                 <li>Computer Instruction Courses</li>
                                 
                                 <li>Classroom Setting Course</li>
                                 
                              </ul>
                              
                              <h4>Technical Certificates</h4>
                              
                              <p>Technical Certificates are available for job improvement courses taken in the areas
                                 of: Business,
                                 Engineering and Technology, Entertainment and Arts Technology, Health Care, Horticulture
                                 and Landscape,
                                 Hospitality and Culinary, Information Technology; and Vocational Certificates are
                                 available from the
                                 Criminal Justice Institute.
                              </p>
                              
                              <p>You can find the descriptions of the all of the Technical Certificates available at
                                 Valencia College on
                                 the <a href="https://preview.valenciacollege.edu/asdegrees/">Associate in Science (A.S.) Degree and Certificate
                                    Programs page</a></p>
                              
                              <h4>Valencia Continuing Education</h4>
                              
                              <p>All of the Valencia's Continuing Education programs are delivered through the Valencia
                                 Institute. It
                                 combines the expertise of our Continuing Professional Education and Development Team
                                 with the resources of
                                 our Office for Corporate Services. Courses are researched, designed and developed
                                 with the needs of the
                                 workforce in mind. Care is given in offering relevant, competency-based topics designed
                                 to maintain and
                                 improve workplace skills and job performance.
                              </p>
                              
                              <p>Some examples of offerings from Continuing Education are:</p>
                              
                              <ul class="list-style-1">
                                 
                                 <li>Seminars</li>
                                 
                                 <li>Professional Training Programs</li>
                                 
                                 <li>Licensure Preparation and Renewal Classes</li>
                                 
                                 <li>Special Topics</li>
                                 
                              </ul>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        <div class="box_side">
                           
                           <h3>Resource Links</h3>
                           
                           <ul class="list-style-1">
                              
                              <li><a href="https://preview.valenciacollege.edu/careercenter/">Career Development Services</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/Labs/">Computer Labs</a></li>
                              
                              <li><a href="http://www.floridashines.org/">FloridaShines.org</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/finaid/">Financial Aid</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/library/">Library</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook (pdf)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/lifelong-learning.pcf">©</a>
      </div>
   </body>
</html>