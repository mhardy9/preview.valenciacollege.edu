<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graduation Transition  | Valencia College</title>
      <meta name="Description" content="Students enrolled while completing 45 credit hours and beyond are completing their degrees and making plans for transfer to complete a bachelor's degree or preparing to enter the workforce. Students preparing for this transition can connect with Valencia resources to assist in this process.">
      <meta name="Keywords" content="graduation, transition, lifemap, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/graduation-transition.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>Graduation Transition </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Success Indicators</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Students enrolled while completing 45 credit hours and beyond are completing their
                              degrees and making
                              plans for transfer to complete a bachelor's degree or preparing to enter the workforce.
                              Students preparing
                              for this transition can connect with Valencia resources to assist in this process.
                           </p>
                           
                           <ul class="list-style-1">
                              
                              <li>Confirm the term you are graduating.</li>
                              
                              <li>Complete an associate's degree without exceeding the required number of credit hours
                                 by more than 15%.
                                 
                              </li>
                              
                              <li>Apply for graduation by the appropriate deadline.</li>
                              
                              <li>Create a portfolio using My Portfolio that demonstrates achievements and skills.</li>
                              
                              <li>Document achievement of Valencia’s Core Competencies in My Portfolio.</li>
                              
                              <li>Prepare a plan for continuing education or employment after graduation from Valencia.</li>
                              
                              <li>Complete the first step in career plan.</li>
                              
                              <li>Write a resume that documents educational and workplace achievements and skills.</li>
                              
                              <li>Review resume with Career Advisor or Counselor.</li>
                              
                              <li>Update My Portfolio and create a "public view" that demonstrates your achievement
                                 and skills.
                              </li>
                              
                              <li>Gather recommendation letters from staff, advisors, counselors and professors.</li>
                              
                              <li>Communicate to others what you have learned and what you can do orally and in writing.</li>
                              
                              <li>Maintain social connections made at Valencia with staff, students, advisors, counselors
                                 and professors
                                 for references and future opportunities.
                                 
                              </li>
                              
                              <li>Document changes (technological, practices, laws, budgets, etc.) that may occur between
                                 the Associate
                                 Degree and employment.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Programs and Services</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>For more information about these programs, go to the Answer Center on any campus.</p>
                           
                           <h4>Career Centers</h4>
                           
                           <p>Students are encouraged to take advantage of the services offered in the Career Center.
                              They will find
                              assistance in assessing their preparedness in the career field of their choice, and
                              advice about how to
                              approach the job market. They can also research transfer decisions.
                           </p>
                           
                           <h4>College Night</h4>
                           
                           <p>College Night is held each October. It has a dual purpose - to allow high school students
                              to investigate
                              college opportunities, and to allow transferring Valencia students to talk to representatives
                              from many
                              colleges and universities around the country. This year, we had over 170 representatives
                              on hand to chat
                              with our students about transfer requirements, housing, financial aid, and other relevant
                              issues.
                           </p>
                           
                           <h4>My Portfolio</h4>
                           
                           <p>Throughout their tenure at Valencia, students are encouraged to create a portfolio
                              that illustrates their
                              skills and experiences. My Portfolio is an electronic version of the portfolio that
                              can be displayed in a
                              public area for potential employers to review. As students near graduation, they can
                              analyze the contents
                              of their portfolio to determine the most relevant examples of mastery of each of Valencia's
                              core
                              competencies - Think, Value, Communicate, Act - at an advanced level in their academic
                              programs. 
                           </p>
                           
                           <h4>Job Fairs</h4>
                           
                           <p>The Workplace Learning and Placement Office also arranges on-campus Job Fairs at which
                              time students may
                              meet employers who are actively recruiting. They have the opportunity to ask questions
                              about potential
                              employers, and get a first-hand impression of the companies represented.
                           </p>
                           
                           <h4>Job Search Skills Course</h4>
                           
                           <p>Valencia offers a 1-credit course in Job Search Skills. Students learn to write resumes,
                              compile
                              portfolios, find leads, search the Internet, and utilize other skills in seeking employment.
                              They also
                              have the opportunity to participate in mock interviews.
                           </p>
                           
                           <h4>Internship Services</h4>
                           
                           <p>Valencia boast one of the largest internship programs in Florida. Each year over 200
                              employers provide
                              opportunities for more than 500 students in areas ranging from accounting to zoology.
                              To see what
                              opportunities are available, <a href="https://preview.valenciacollege.edu/internship">visit Internship
                                 Services</a>.
                           </p>
                           
                           <h4>Workforce Services</h4>
                           
                           <p>Workforce Services, a part of the Internship and Workforce Services department, is
                              the means by which
                              students and employers connect. Students may identify part-time or full-time opportunities,
                              determine
                              on-campus employer recruiting schedules by <a href="https://preview.valenciacollege.edu/internship">visiting
                                 Internship Services</a>. Other Workforce Services may include:
                           </p>
                           
                           <ul class="list-style-1">
                              
                              <li>
                                 <strong>Dining Etiquette Workshop</strong> - Workshops are conducted by employers and Career
                                 Development Services staff to provide students with another opportunity to fine-tune
                                 their job search
                                 skills
                                 
                              </li>
                              
                              <li>
                                 <strong>Employment Links</strong> - Links to job fairs, employer websites, etc.
                              </li>
                              
                              <li>
                                 <strong>Employer Profile Series</strong> – An annual event co-coordinated with Career Development
                                 Services that highlights the career path of a specific business partner.
                                 
                              </li>
                              
                              <li>
                                 <strong>Internship Fairs</strong> – Events designed to connect students with prospective internship
                                 employers
                                 
                              </li>
                              
                              <li>
                                 <strong>My Job Prospects</strong> – online resource of thousands of employers in the Central Florida
                                 area.
                                 
                              </li>
                              
                           </ul>
                           
                           <p>Through the Internship and Workforce Services, students can connect with employers
                              to gain valuable
                              experience in their program/major. Students graduating with an AS degree may to take
                              advantage of services
                              which include assistance in setting up interviews, resume referral service, job interview
                              resource
                              materials. Students may identify employment opportunities in their career fields throughout
                              the online job
                              bank called First Place. They may also take advantage of the informational resource
                              library on local
                              businesses.
                           </p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side">
                        
                        <h3>Resource Links</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="https://preview.valenciacollege.edu/careercenter/">Career Development Services</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/Labs/">Computer Labs</a></li>
                           
                           <li><a href="http://www.floridashines.org/">FloridaShines.org</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/finaid/">Financial Aid</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/library/">Library</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook (pdf)</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/graduation-transition.pcf">©</a>
      </div>
   </body>
</html>