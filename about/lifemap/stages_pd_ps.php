<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/stages_pd_ps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <h2>Progression to Degree Programs and Services</h2>
                                    
                                    <p>For more information about any of  these programs, go to the Answer Center on any
                                       campus. <br>
                                       <br>
                                       <strong>Academic Warning System</strong><br>
                                       Students whose grade point average and completion of course rates fall below  2.0
                                       G.P.A. are placed on academic warning or probation and receive special  advising intervention
                                       and coaching in success strategies to assist them in  improving their academic performance.
                                       Students who are re-admitted after  academic suspension must meet with an Academic
                                       Advisor, Career Advisor or  Counselor and are required to have their educational plan
                                       and to meet with them  during the term.<br>
                                       <br>
                                       <strong>Advising and Counseling</strong><br>
                                       Educational advising  staff and counselors provide developmental advising which includes
                                       life, career  and educational planning, interpretation of assessments, strategies
                                       to address  academic difficulties, programs to develop student success skills, preparation
                                       for university transfer, and workforce preparedness. Click <a href="../../advising-counseling/index.html">here</a> for more information. 
                                    </p>
                                    
                                    <p> <strong>Associate  of Art Degrees with Pre-Majors<br>
                                          </strong>To assist students with  their long-term career plans, Valencia has articulated pre-majors.
                                       These  pre-major programs are designed for easy articulation into Florida’s State
                                       University System. They also facilitate a student’s academic progress through  Valencia
                                       by providing a well-developed plan of action from term to term.<br>
                                       <br>
                                       <strong>BayCare Student Assistant Services </strong><br>
                                       Students can access confidential,  professional assistance to help resolve problems
                                       that affect their personal  life and/or college performance. Besides being confidential,
                                       this voluntary  program is designed for all students to seek help on their own. Credit
                                       students  can use this service at no charge as the service is funded through the student
                                       activity fee.
                                    </p>
                                    
                                    <p><strong>Career Centers <br>
                                          </strong>The Career Center assist  students with the opportunity for take assessments and make
                                       career decisions.  Those students who have already taken the Student Success class
                                       may choose to  use My Career Planner and My Job Prospects to be sure they are aligned
                                       with  current skills and training requirements for today’s fast-paced job market.
                                       Students may also check current transfer requirements for their chosen  University
                                       or College. 
                                    </p>
                                    
                                    <p> <strong>Core  Competencies<br>
                                          </strong>As students progress  toward their degrees, they are given repeated opportunities
                                       to employ  Valencia’s Core Competencies - THINK, VALUE, COMMUNICATE, ACT. The goal
                                       of the  college is to assist students to THINK clearly, critically, and creatively;
                                       to  analyze, synthesize, integrate, and evaluate in many domains of human life; to
                                       make reasoned VALUE judgments and responsible commitments; to COMMUNICATE with  different
                                       audiences using varied means, and to ACT purposefully, reflectively,  and responsibly.<br>
                                       <br>
                                       <strong>Financial Literacy</strong><br>
                                       The Financial Learning  Ambassador Program is a financial literacy program designed
                                       to empower Valencia  students to improve their personal financial awareness; fostering
                                       a life-long  road to financial success. Commissioning Valencia College Cohort Default
                                       Rate  to continuously display a frequency of community, integrity, access, deep  stewardship,
                                       professional development, civic responsibility, and excellence.
                                    </p>
                                    
                                    <p> <strong>Honors  Program<br>
                                          </strong>Valencia’s Honors Program  provides the academically gifted and highly motivated student
                                       with an enriched  program of course work and extra-curricular activities. The program
                                       focuses on  developing the student’s critical thinking skills and helping the student
                                       become an independent learner.<br>
                                       <br>
                                       <strong>Internships<br>
                                          </strong>Valencia boast one of the largest internship programs in Florida. Each year  over
                                       200 employers provide opportunities for more than 500 students in areas  ranging from
                                       accounting to zoology. To see what opportunities are available,  visit our website
                                       at: <a href="../../internship/index.html">valenciacollege.edu/internship</a> <br>
                                       <br>
                                       <strong>Leadership Development<br>
                                          </strong>The Student Development office fosters the development of leadership  skills. The
                                       annual Student Leadership Symposium is one opportunity for students  from all campuses
                                       to explore leadership theories and practices and to interact  with each other. <br>
                                       <br>
                                       <strong>My Education Plan</strong><br>
                                       Valencia College’s  web-based education planning tool in Atlas that allows students
                                       to plan their  degree completion requirements, determine course sequence, and project
                                       the time  it will take to complete a degree. 
                                    </p>
                                    
                                    <p> <strong>My  Portfolio<br>
                                          </strong>My Portfolio enables  students to work with faculty and staff to document mastery
                                       of the Core  Competencies: think, value, communicate, act. The portfolio enables students
                                       to  capture demonstrations of learning (via assignments, written work, art work, 
                                       co-curricular activities) from specific courses and experiences throughout  their
                                       educational program.
                                    </p>
                                    
                                    <p><strong>Office  for Students with Disabilities</strong><br>
                                       Valencia is committed  to ensuring that all its programs and services are accessible
                                       to students with disabilities.  The Office for Students with Disabilities (OSD) provides
                                       individual assistance  to students with documented disabilities based upon the need
                                       and impact of a  student’s specific disability.
                                    </p>
                                    
                                    <p> <strong>Skillshops</strong><br>
                                       Skillshops  are free workshops, or mini courses, that provide real-life solutions
                                       to common  student issues including financial literacy, improving study habits, managing
                                       depression, and dealing with stress.
                                    </p>
                                    
                                    <p><strong>Student  Development</strong><br>
                                       Involvement in campus life outside the  classroom is an important component of a well-rounded
                                       college experience.  Student Development on each campus offers a variety of programs
                                       and services.  These include: Student Government Association; campus activities; student
                                       clubs  and organizations; co-curricular programs; student leadership programs;  intramural
                                       sports and wellness programs; community service programs; campus  publicity; and college
                                       and community information.
                                    </p>
                                    
                                    <p> <strong>Student Leaders<br>
                                          </strong>Peer Educators are student  leaders who are recruited for a one-year term to work
                                       at Valencia. They assist  with office responsibilities, become educators in health
                                       and wellness issues,  such as alcohol awareness, personal safety, safe sex, and general
                                       wellness  practices, and serve as ambassadors to the college. 
                                    </p>
                                    
                                    <p> Welcome Team are student leaders  who assist with individual tours, orientation tours
                                       and assist the Student  Development Office with other college-wide activities. They
                                       assist students in  the Information Station and provide assistance with college-wide
                                       and campus  events. 
                                    </p>
                                    
                                    <p> Atlas Access Lab leaders are  student leaders who assist inside the Atlas Access
                                       Labs on each campus.&nbsp;  They assist students with registration, financial aid, and
                                       admissions  processes.&nbsp; In addition, they support New Student Orientation by giving
                                       the overview of Atlas and support the Student Success classes by presenting the  LifeMap
                                       Tools introduction.
                                    </p>
                                    
                                    <p><strong>Study  Abroad and Global Experiences (SAGE)</strong><br>
                                       The SAGE Office offers a variety of  international learning experiences that will
                                       help students live, work and  collaborate effectively in a global community. Students
                                       can participate in  faculty-led, short-term study abroad programs which are for credit
                                       and offered  during winter, spring or summer break. Students can also elect to enroll
                                       in  semester or year-long study abroad programs through another educational  institution.
                                       Financial aid may be used for both course tuition and all program  fees related to
                                       the program. Click <a href="../../international/studyabroad/index.html">here</a> for more information. <br>
                                       <br>
                                       <strong>Veteran’s Affairs</strong><br>
                                       The Veteran’s Affairs Office is  responsible for providing veterans and their eligible
                                       dependents assistance  that will enable them to maximize their veteran’s educational
                                       entitlement.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/stages_pd_ps.pcf">©</a>
      </div>
   </body>
</html>