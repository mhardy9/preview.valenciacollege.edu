<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Development Services | Lifemap | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/CareerCenters.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lifemap</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>Career Development Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Career Development Services Summary</h2>
                        
                        <p>An important component of LifeMap is Career Development, which focuses on helping
                           students explore and identify career goals. Explore the world of work by visiting
                           <a href="/students/career-center/index.php">Career Development Services</a>.<br> <br> The Career Development Services are located on:
                        </p>
                        
                        <p><a href="/students/career-center/aboutus.php">East Campus</a>, Bldg. 5-230, extension 2259&nbsp;<br> <a href="/students/career-center/aboutus.php">West Campus</a>, Bldg. SSB-206, extension 1464<br> <a href="/students/career-center/aboutus.php">Winter Park Campus</a>, Bldg. 1-104, extension 6851<br> <a href="/students/career-center/aboutus.php">Osceola Campus</a>, Bldg. 2-125, extension 4897
                        </p>
                        
                        <p>Career Center services and programs are designed to help students:</p>
                        
                        <ul>
                           
                           <li>learn more about their personality, interests, skills and values</li>
                           
                           <li>discover career options best suited to them</li>
                           
                           <li>decide on a major related to career choice</li>
                           
                           <li>research 4-year college/university, graduate, and professional school transfer requirements</li>
                           
                           <li>review resumes and cover letters</li>
                           
                           <li>learn about job interviewing techniques</li>
                           
                           <li>research career fields and job outlook</li>
                           
                        </ul>
                        
                        <p>Career development is a lifelong process. Once you learn the steps of the process,
                           you will be able to repeat it whenever you need to throughout your life.
                        </p>
                        
                        <p>Learn About Yourself<br> Identify career interests through assessments that relate your personality, interests,
                           skills, values and aptitudes to various career fields.&nbsp;<br> Such assessments include:
                        </p>
                        
                        <ul>
                           
                           <li>Myers-Briggs Type Indicator</li>
                           
                           <li>CHOICES Career Exploration</li>
                           
                           <li>Career Assessment Inventory</li>
                           
                           <li>Harrington-O'Shea Career Decision-Making System</li>
                           
                           <li>Holland Self-Directed Search</li>
                           
                           <li>Work Values Inventory</li>
                           
                           <li>Career Values Card Sort</li>
                           
                           <li>Motivated Skills Sort</li>
                           
                           <li>Occupational View-Deck</li>
                           
                        </ul>
                        
                        <p>Learn About Career Fields<br> Explore the world of work through a wealth of online and hard-copy resources:
                        </p>
                        
                        <ul>
                           
                           <li>&nbsp;Occupational Outlook Handbook</li>
                           
                           <li>&nbsp;Encyclopedia of Careers</li>
                           
                           <li>&nbsp;Vocational Biographies</li>
                           
                           <li>&nbsp;Career Books, Videos, and CD-ROMs</li>
                           
                           <li>&nbsp;Career Web sites</li>
                           
                        </ul>
                        
                        <p>Develop a Plan for Action&nbsp;<br> Based on your assessments and new knowledge of career options, you can begin to develop
                           an educational plan. Understanding your decision-making process will assist you. You
                           can then select an area ofstudy that will lead to your initial career decision.
                        </p>
                        
                        <p><em>Researching your major includes:</em></p>
                        
                        <ul>
                           
                           <li>Understanding career options related to your major and/or meta major.</li>
                           
                           <li>Identifying colleges that offer your major and the courses required for transfer into
                              that major.
                           </li>
                           
                           <li>Talking with faculty with experience in your career or major.</li>
                           
                           <li>Conducting a career interview with people who are working in your career field. Find
                              out their career preparation and path.
                           </li>
                           
                           <li>Exploring options for Internships, part-time or volunteer work to gain experience
                              in your career and major. Check out Valencia's site: (click on "Current Student,"
                              and then "Workplace Learning and Placement").
                           </li>
                           
                           <li>Learning about professional associations that can enhance your career preparation.</li>
                           
                           <li>Develop an educational plan using My Educational Planner in Atlas - LifeMap tab. You
                              can also visit the Atlas Acces lab.
                           </li>
                           
                        </ul>
                        
                        <p>Commitment and Implementation<br> As you become more confident in your career and educational plan, you can continue
                           to evaluate your options and make adjustments as needed.&nbsp;<br> <br> <em>As you move toward completing your degree at Valencia, you will want to explore:</em></p>
                        
                        <ul>
                           
                           <li>Transition to a 4-year college/university or acceptance into one of Valencia's bachelor
                              degree programs.
                           </li>
                           
                           <li>Transition to Employment.Career Advisors and Career Counselors can help you with your
                              job search such as:
                           </li>
                           
                        </ul>
                        
                        <ul type="disc">
                           
                           <ul type="circle">
                              
                              <li>Preparing a resume</li>
                              
                              <li>Developing a cover letter</li>
                              
                              <li>Practice interviewing techniques</li>
                              
                              <li>Videotape a practice interview</li>
                              
                              <li>Referrals to Central Florida employers<br> 
                              </li>
                              
                           </ul>
                           
                           <li>Transition to Graduate School. The Career Development Services have information about
                              graduate, law, medical, dental, pharmacy, physical therapy, and other professional
                              schools.&nbsp;
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/CareerCenters.pcf">©</a>
      </div>
   </body>
</html>