<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Transition  | Valencia College</title>
      <meta name="Description" content="Students involved in college transition are making decisions about educational directions following high school or as a career change. Understanding the educational options available and other issues in planning for college attendance is important.">
      <meta name="Keywords" content="college, transition, lifemap, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/college-transition.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lifemap</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>College Transition </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               				
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="box_style_1">
                           
                           <div class="indent_title_in">
                              
                              <h3>Success Indicators</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>Students involved in college transition are making decisions about educational directions
                                 following high
                                 school or as a career change. Understanding the educational options available and
                                 other issues in planning
                                 for college attendance is important. A variety of Valencia programs are designed to
                                 assist this decision
                                 making process.
                              </p>
                              
                              <ul>
                                 
                                 <li>Explain why a college education is possible for you.</li>
                                 
                                 <li>Fully engage in the enrollment process: timely completion of application, assessment,
                                    orientation,
                                    financial aid, and register for classes prior to priority deadlines.
                                    
                                 </li>
                                 
                                 <li>Make initial choices about educational and career interests.</li>
                                 
                                 <li>Discuss educational and career interests with advisors and others.</li>
                                 
                                 <li>Establish a financial plan for obtaining a college education.</li>
                                 
                                 <li>Complete assessment for appropriate placement in reading, mathematics and English.</li>
                                 
                              </ul>
                              
                           </div>
                           
                           <hr class="styled_2">
                           
                           <div class="indent_title_in">
                              
                              <h3>Programs and Services</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>For more information about these programs, please visit <a href="https://preview.valenciacollege.edu/transitions/">Valencia Transitions Planning</a>.
                              </p>
                              
                              <h4>Answer Center</h4>
                              
                              <p>Valencia’s Answer Center is your place to go for questions about your educational
                                 plans, how to apply for
                                 admission or how to apply for financial aid. Visit the Answer Center on any campus
                                 for help.
                              </p>
                              
                              <h4>Bridges to Success</h4>
                              
                              <p>Mentoring, academic and financial support is provided for low income, first generation
                                 in college or
                                 minority students who are at-risk graduates of Orange or Osceola high schools. Admission
                                 to Bridges to
                                 Success is selective. For more information, <a href="/students/bridges-to-success/">visit
                                    the Bridges to Success web page</a>.
                              </p>
                              
                              <h4>Campus Tours</h4>
                              
                              <p>The Valencia Welcome Team gives campus tours each day at 10:00 am, 2:00 pm and 6:00
                                 pm on Mondays through
                                 Thursdays. Check out the <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit
                                    Valencia webpage</a> to schedule a tour.
                              </p>
                              
                              <h4>Career Pathways/ Tech Prep</h4>
                              
                              <p>High school students enroll in career programs and earn college credit by completing
                                 specified coursework
                                 and an assessment. Articulations agreements allow students to achieve a seamless transition
                                 from high
                                 school to college and save time and money by earning college credit while in high
                                 school.
                              </p>
                              
                              <h4>College Night</h4>
                              
                              <p>The Central Florida community is invited to visit Valencia to meet with more than
                                 130 other college
                                 representatives in a college fair forum during two evenings in October.
                              </p>
                              
                              <h4>Direct Connect to UCF</h4>
                              
                              <p>Valencia has a partnership with UCF that will guarantee admission to UCF for any student
                                 who graduates
                                 with an AA degree from Valencia. UCF staff are located on Valencia campuses to meet
                                 with students and
                                 encourage a smooth transition.
                              </p>
                              
                              <h4>Dual Enrollment</h4>
                              
                              <p>An opportunity for high school students with a 3.0 grade point average or higher and
                                 college-level
                                 placement scores to take college-level courses and earn both high school and college
                                 credit, free of
                                 charge. 
                              </p>
                              
                              <h4>Enrollment Services</h4>
                              
                              <p>For telephone assistance regarding admission, registration or financial aid call 407-582-1507.</p>
                              
                              <h4>Financial Aid</h4>
                              
                              <p>Valencia offers a wide range of financial aid programs including grants, scholarships,
                                 student loans and
                                 work study programs. Visit the <a href="/admissions/finaid">Financial Aid webpage</a> for
                                 more information.
                              </p>
                              
                              <h4>FAFSA Frenzy Fridays</h4>
                              
                              <p>Central Florida high school seniors and their parents can receive one on one help
                                 with completing their
                                 financial aid applications.
                              </p>
                              
                              <h4>Freshman Orientation</h4>
                              
                              <p>Special new student orientation workshops designed for previous year high school graduates
                                 and their
                                 parents are held in May each year.
                              </p>
                              
                              <h4>Group Visits</h4>
                              
                              <p>Group visits are designed for schools or organizations interested in bringing a group
                                 of eight or more
                                 people to learn more about Valencia College. Topics may include: planning for college,
                                 how to enroll,
                                 financial aid, and will be followed by a campus tour experience. If you are interested
                                 in requesting a
                                 group visit, Visit our website at: http://preview.valenciacollege.edu/visit
                              </p>
                              
                              <h4>Go Higher Get Accepted</h4>
                              
                              <p>This statewide campaign is offered by Valencia to Orange and Osceola county high school
                                 students who
                                 would like guidance in completing the community college admission application process.
                              </p>
                              
                              <h4>High School Visits</h4>
                              
                              <p>During the fall and spring of the academic year, Valencia Coordinators will visit
                                 with high school
                                 juniors and seniors to share enrollment information.
                                 
                                 
                                 
                              </p>
                              
                              <h4>Valencia Previews</h4>
                              
                              <p>The College Transition team encourages interested students or community groups to
                                 visit Valencia for a
                                 presentation and a campus tour. To schedule a Valencia Preview, visit the <a href="http://preview.valenciacollege.edu/visit/">Visit Valencia webpage</a>.
                              </p>
                              
                              <h4>Youth Education Day</h4>
                              
                              <p>High school students in Central Florida can learn more about college opportunities
                                 and financial aid at
                                 this conference style event in February each year.
                              </p>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        <div class="box_side">
                           
                           <h3>Resource Links</h3>
                           
                           <ul>
                              
                              <li><a href="https://preview.valenciacollege.edu/careercenter/">Career Development Services</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/Labs/">Computer Labs</a></li>
                              
                              <li><a href="http://www.floridashines.org/">FloridaShines.org</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/finaid/">Financial Aid</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/library/">Library</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook (pdf)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/college-transition.pcf">©</a>
      </div>
   </body>
</html>