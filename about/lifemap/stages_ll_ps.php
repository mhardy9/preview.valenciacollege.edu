<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/stages_ll_ps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 
                                 <div data-old-tag="td"> 
                                    <h2>Lifelong Learning Programs and Services</h2>
                                    
                                    <p><strong>Credit Courses</strong><br>
                                       Valencia provides credit courses  that are open to anyone interested in the subject
                                       matter. These Special  Interest students will find classes for upgrading job skills,
                                       working toward  Teacher Recertification, personal growth or a Technical Certificate.
                                       Classes  are offered in a variety of formats:
                                    </p>
                                    
                                    <ul type="disc">
                                       
                                       <li>On-line Courses </li>
                                       
                                       <li>Hybrid Courses</li>
                                       
                                       <li>Computer Instruction       Courses</li>
                                       
                                       <li>Classroom Setting       Course</li>
                                       
                                    </ul>
                                    
                                    <p><strong>Technical Certificates</strong><strong></strong><br>
                                       Technical Certificates are  available for job improvement courses taken in the areas
                                       of: Business,  Engineering and Technology, Entertainment and Arts Technology, Health
                                       Care,  Horticulture and Landscape, Hospitality and Culinary, Information Technology;
                                       and Vocational Certificates are available from the Criminal Justice Institute. 
                                    </p>
                                    
                                    <p>You can find the descriptions of  the all of the Technical Certificates available
                                       at Valencia College at: <a href="../../academics/asdegrees/index.html">Associate in Science (A.S.) Degree  and Certificate Programs</a></p>
                                    
                                    <p><strong>Valencia <strong>Continuing  Education</strong></strong><br>
                                       At Valencia  Continuing Education, we combine the expertise of our Continuing Professional
                                       Education and Development Team with the resources of our Office for Corporate  Services.
                                       The programs that we offer are as diverse as the individuals and  organizations that
                                       we serve.
                                    </p>
                                    
                                    <p>Our programs  include industry-focused training, professional development, languages,
                                       certifications, organizational planning services and everything in between. So,  whether
                                       your goals are personal, professional or organizational, we have a  program to help
                                       you reach them.
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>Accounting and Finance</li>
                                       
                                       <li>Advanced Manufacturing</li>
                                       
                                       <li>Arts and Entertainment</li>
                                       
                                       <li>Certifications</li>
                                       
                                       <li>Communications</li>
                                       
                                       <li>Computer Training</li>
                                       
                                       <li>Construction and Architecture</li>
                                       
                                       <li>Corporate University</li>
                                       
                                       <li>Criminal Justice and Corrections</li>
                                       
                                       <li>Customer Service, Sales and Marketing</li>
                                       
                                       <li>Emergency Management</li>
                                       
                                       <li>English as a Second Language</li>
                                       
                                       <li>English Pronunciation</li>
                                       
                                       <li>Facilities Management and Sustainability</li>
                                       
                                       <li>Fire Rescue</li>
                                       
                                       <li>Global Workforce Programs</li>
                                       
                                       <li>Government</li>
                                       
                                       <li>Healthcare</li>
                                       
                                       <li>Human Resources</li>
                                       
                                       <li>Insurance</li>
                                       
                                       <li>Intensive English Program</li>
                                       
                                       <li>Landscape and Horticulture Technology</li>
                                       
                                       <li>Language Programs</li>
                                       
                                       <li>Leadership Development</li>
                                       
                                       <li>Leadership+</li>
                                       
                                       <li>Management and Supervision</li>
                                       
                                       <li>Organizational Planning and Redesign</li>
                                       
                                       <li>Private Security</li>
                                       
                                       <li>Public Service Academy</li>
                                       
                                       <li>Public Safety Leadership Development Certification</li>
                                       
                                       <li>Real Estate</li>
                                       
                                       <li>Workforce Testing</li>
                                       
                                       <li>Workforce Training and Organizational Development</li>
                                       
                                    </ul>
                                    
                                    <p>For more information, please click <a href="https://preview.valenciacollege.edu/continuing-education/">here</a>.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/stages_ll_ps.pcf">©</a>
      </div>
   </body>
</html>