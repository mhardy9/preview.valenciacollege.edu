<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Development Services  | Valencia College</title>
      <meta name="Description" content="You can explore the world of work by visiting a Valencia Career Center. The Career Center can help you decide on a career or major, discover options for careers related to your major, choose a major to support your career interests, develop a career plan, and more.">
      <meta name="Keywords" content="career, development, services, lifemap, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/career-development-services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>Career Development Services </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Services Offered By Career Centers</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You can explore the world of work by visiting a <a href="https://preview.valenciacollege.edu/careercenter/">Valencia
                                 Career Center</a>. The Career Center can help you:
                           </p>
                           
                           <ul class="list-style-1">
                              
                              <li>Decide on a career or major</li>
                              
                              <li>Discover options for careers related to your major</li>
                              
                              <li>Choose a major to support your career interests</li>
                              
                              <li>Get information on factors you need to consider in making a career decision - education
                                 level, salary,
                                 working conditions, skills, certification or license requirements, number of openings
                                 projected in 5-10
                                 years, and more.
                                 
                              </li>
                              
                              <li>Develop a career plan</li>
                              
                           </ul>
                           
                           <p><strong>Career development is a lifelong process.</strong> Once you learn the steps of the process, you
                              will be able to repeat it whenever you need to throughout your life.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Learn About Yourself</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Identify career interests through assessments that relate your personality, interests,
                              skills, values and
                              aptitudes to various career fields. Such assessments include:
                           </p>
                           
                           <ul class="list-style-1">
                              
                              <li>Myers-Briggs Type Indicator</li>
                              
                              <li>CHOICES Career Exploration</li>
                              
                              <li>Career Assessment Inventory</li>
                              
                              <li>Harrington-O'Shea Career Decision-Making System</li>
                              
                              <li>Holland Self-Directed Search</li>
                              
                              <li>Work Values Inventory</li>
                              
                              <li>Career Values Card Sort</li>
                              
                              <li>Motivated Skills Sort</li>
                              
                              <li>Occupational View-Deck</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Learn About Career Fields</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Explore the world of work through a wealth of online and hard-copy resources:</p>
                           
                           <ul class="list-style-1">
                              
                              <li>Occupational Outlook Handbook</li>
                              
                              <li>Encyclopedia of Careers</li>
                              
                              <li>Vocational Biographies</li>
                              
                              <li>Career Books, Videos, and CD-ROMs</li>
                              
                              <li>Career Web sites</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Develop a Plan for Action</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Based on your assessments and new knowledge of career options, you can begin to develop
                              an educational
                              plan. Understanding your decision-making process will assist you. You can then select
                              an area of study
                              that will lead to your initial career decision.
                           </p>
                           
                           <p>Researching your major includes:</p>
                           
                           <ul class="list-style-1">
                              
                              <li>Understanding career options related to your major.</li>
                              
                              <li>Identifying colleges that offer your major and the courses required for transfer into
                                 that major.
                              </li>
                              
                              <li>Talking with faculty with experience in your career or major.</li>
                              
                              <li>Conducting a career interview with people who are working in your career field. Find
                                 out their career
                                 preparation and path.
                                 
                              </li>
                              
                              <li>Exploring options for Internships, part-time or volunteer work to gain experience
                                 in your career and
                                 major. Visit <a href="https://preview.valenciacollege.edu/internship">Internship Services</a> for more
                                 information.
                                 
                              </li>
                              
                              <li>Learning about professional associations that can enhance your career preparation.</li>
                              
                              <li>Develop an educational plan using My Educational Planner in Atlas - LifeMap tab. You
                                 can also visit
                                 the Atlas Access Lab for assistance.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Commitment and Implementation</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>As you become more confident in your career and educational plan, you can continue
                              to evaluate your
                              options and make adjustments as needed.
                           </p>
                           
                           <p>As you move toward completing your degree at Valencia, you will want to explore:</p>
                           
                           <ul class="list-style-1">
                              
                              <li>Transition to a 4-year college or university. Career Development Servcies have transfer
                                 manuals,
                                 catalogs, and admissions guides for Florida colleges and universities, and resources
                                 for out-of-state
                                 colleges and universities.
                                 
                              </li>
                              
                              <li> Transition to Employment. Career Advisors and Career Counselors can help you with
                                 your job search
                                 such as:
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>Preparing a resume</li>
                                    
                                    <li>Developing a cover letter</li>
                                    
                                    <li>Practice interviewing techniques</li>
                                    
                                    <li>Videotape a practice interview</li>
                                    
                                    <li>Referrals to Central Florida employers</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Transition to Graduate School. The Career Development Services have information about
                                 graduate, law,
                                 medical, dental, pharmacy, physical therapy, and other professional schools.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side">
                        
                        <h3>Resource Links</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="https://preview.valenciacollege.edu/careercenter/">Career Development Services</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/Labs/">Computer Labs</a></li>
                           
                           <li><a href="http://www.floridashines.org/">FloridaShines.org</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/finaid/">Financial Aid</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/library/">Library</a></li>
                           
                           <li><a href="https://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook (pdf)</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/career-development-services.pcf">©</a>
      </div>
   </body>
</html>