<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/lifemap/stages_ic_ps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/lifemap/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/lifemap/">Lifemap</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <h2>Introduction to College Programs and Services </h2>
                                    
                                    <p>For  more information about any of our programs and services, please visit the  Answer
                                       Center on any campus. <br>
                                       <br>
                                       <strong>Admissions</strong><br>
                                       Valencia College has a rolling admissions process, meaning we accept new  students
                                       year-round. Information about admissions, including a checklist of  steps to enrollment,
                                       is available by clicking <a href="https://preview.valenciacollege.edu/future-students/admissions/">here</a>.  Important dates and deadlines can be found on the Valencia <a href="../../calendar/index.html">calendar</a>.
                                       
                                    </p>
                                    
                                    <p><strong>Advanced Placement (AP) and CLEP</strong><br>
                                       Students who transfer AP credit from high school are awarded college credit for  specific
                                       courses. Students can take CLEP examinations in specific subject areas  and earn college
                                       credit based on appropriate scores and satisfy degree  requirements. Both programs
                                       provide accelerated mechanisms for degree  completion at Valencia.<br>
                                       <br>
                                       <strong>Advising and Counseling</strong><br>
                                       Educational advising staff and counselors provide developmental advising which  includes
                                       life, career and educational planning, interpretation of assessments,  strategies
                                       to address academic difficulties, programs to develop student  success skills, preparation
                                       for university transfer, and workforce  preparedness. More information can be located
                                       by clicking <a href="../../advising-counseling/index.html">here</a>.
                                       
                                    </p>
                                    
                                    <p> <strong>Atlas Access </strong><strong><br>
                                          </strong>Atlas Access computer labs are open to assisted prospective students in the  enrollment
                                       process, and all registered students conducting educational and  career related research
                                       and planning. Open use includes access to the internet  and your Atlas account, with
                                       staff on hand to assist. For help with Atlas,  browse the Student Atlas How-To, or
                                       call the Student Help Desk at (407)  582-5444.        
                                       
                                    </p>
                                    
                                    <p> <strong>Bridges to Success</strong><br>
                                       Recent high school graduates selected for this program enroll in courses during  the
                                       summer term as a transition to their first year in college. Students  participate
                                       in workshops and support services designed to assist with college  transition. Students
                                       who successfully complete the program are eligible for a  two-year scholarship at
                                       Valencia.<br>
                                       <br>
                                       <strong>Developmental Courses</strong><br>
                                       Developmental courses help students attain basic skills in reading, English and  mathematics
                                       so that they have a better chance at success in college-level  courses. Valencia offers
                                       developmental courses in reading, mathematics,  English, and English as a second language
                                       for academic purposes. <br>
                                       <br>
                                       <strong>Honors Program</strong><strong><br>
                                          </strong>Students who qualify for the Honors Program and have not yet applied are  identified
                                       through the New Student Orientation program and offered the  opportunity to apply
                                       for acceptance. Students enrolled in the Honors Program  receive special advising
                                       and registration prior to the our regularly scheduled  registration period. Honors
                                       courses are small and offer in-depth study of  subject matter. Honors graduates have
                                       special opportunities for transfer  scholarships and admission to competitive four-year
                                       colleges and universities.  Click <a href="../../honors/index.html">here</a> for more  information. <br>
                                       <br>
                                       <strong>Learning in Community (LinC)</strong><strong><br>
                                          </strong>Students enroll as a cohort into the same set of courses (usually 2 or 3  courses)
                                       that are taught collaboratively by faculty who coordinate their  syllabi and learning
                                       activities. Learning communities support student success  through shared support systems
                                       and complimentary curricula that enrich the  learning experience. <br>
                                       <br>
                                       <strong>My Career Planner</strong><strong><br>
                                          </strong>This career planning tool enables students to make career decisions. This  tool guides
                                       students through assessments to explore self, majors, and  occupations; examine options
                                       and then set goals. Students can share their  outcomes with the Career Advisors and
                                       Counselors.         
                                       
                                    </p>
                                    
                                    <p> <strong>My Education Plan</strong><strong><br>
                                          </strong>This education planning tool enables students to plan their courses for a  specific
                                       degree at Valencia. Students can save several educational plans and  make adjustments
                                       to their course loads in order to successfully progress  through their degree requirements.<br>
                                       <br>
                                       <strong>Math and Communications Support Centers</strong><strong><br>
                                          </strong>Instructional assistants provide specialized learning systems including  computer-based
                                       tutorials, supplemental instructional materials, practice  assessments, and tutoring
                                       to support student success in these academic areas.<br>
                                       <br>
                                       <strong>New Student Experience </strong><br>
                                       Valencia College provides a coordinated experience for all new students. The  New
                                       Student Experience includes a required credit-earning course (SLS 1122) and  provides
                                       and extended orientation to college, integrated student success  skills, career and
                                       academic advising, which includes the development of an  individualized education
                                       plan.        
                                       
                                    </p>
                                    
                                    <p> <strong>New Student Orientation (NSO)</strong><strong><br>
                                          </strong>All degree-seeking students are required to attend an orientation session  in which
                                       the curriculum introduces them to Valencia, core competencies and  LIFEMAP, degree
                                       options and requirements, support services, and prepares them  for registration in
                                       first semester courses. Students can register for courses  based on an assigned registration
                                       time ticket.<br>
                                       <br>
                                       <strong>PERT Preparation </strong><br>
                                       Incoming students must meet entry placement requirements that may include  Postsecondary
                                       Education Readiness Test (PERT). Valencia has designed study  guides for entering
                                       students. General information, study guides, and review  sessions can be found by
                                       clicking <a href="../../assessments/pert/taking-the-exam.html">here</a>.
                                       
                                    </p>
                                    
                                    <p> <strong>Skillshops </strong><br>
                                       Skillshops are offered to assist students succeed in college and life. These  interactive
                                       workshops include topics such as time management, stress  management, study skills,
                                       note taking, math and speech anxiety, career  planning, and diversity awareness. More
                                       information pertaining to the Skillshops  can be found by clicking <a href="../student-services/skillshops.html">here</a>.        
                                       
                                    </p>
                                    
                                    <p> <strong>Student Development</strong><strong><br>
                                          </strong>Student Active involvement in events, activities, and organizations outside  of class
                                       is a key to student success. Student Development facilitates  co-curricular and extracurricular
                                       experiences student growth and development.  Whether it is a club or organization,
                                       a work-study position, a leadership  series, or a co-curricular certificate The Office
                                       of Student Development  invites students to explore interests and passions on campus.<br>
                                       <br>
                                       <strong>Student Handbook</strong><strong><br>
                                          </strong>The Student Handbook is the "text" for LIFEMAP. It describes what  LIFEMAP is, and
                                       provides self-assessments and information to guide students  through Life Goals, Career
                                       Goals, Educational Plans, Building a Schedule, and  Academic Success Skills. College
                                       information is provided in the chapter that is  relevant to each topic. A weekly calendar
                                       provides key college dates and allows  students to record their assignments and academic
                                       schedule. "To Do"  cues are provided each week for students according to their LIFEMAP
                                       stage.        
                                       
                                    </p>
                                    
                                    <p> <strong>Tutoring Centers</strong><strong><br>
                                          </strong>Free tutoring is available for students who need additional academic  support through
                                       one-on-one tutoring in specific courses. Supplemental  Instruction models are implemented
                                       on some campuses. Tutors who are hired can  earn a supplemental income while working
                                       on campus. <br>
                                       <br>
                                       <strong>Valencia Support </strong>        
                                       For additional  services and assistance, click <a href="../support/index.html">here</a>.        
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/lifemap/stages_ic_ps.pcf">©</a>
      </div>
   </body>
</html>