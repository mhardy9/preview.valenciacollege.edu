<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Facilities &amp; Sustainability | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/facilities/planning.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/facilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Facilities &amp; Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/facilities/">Facilities</a></li>
               <li>Facilities &amp; Sustainability</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <h2>Planning, Design, and Construction</h2>
                        
                        <ul>
                           
                           <li>
                              <strong>Mark Hose</strong>, Facilities Planner/ Architect<br>
                              <a href="mailto:mhose@valenciacollege.edu">mhose@valenciacollege.edu</a> ; 407-582-1704 <br>
                              
                              <ul>
                                 
                                 <li>
                                    <strong>Brenda Jones</strong>, Facilities Specialist<br>
                                    <a href="mailto:bjones@valenciacollege.edu">bjones@valenciacollege.edu</a> ; 407-582-1749
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>            
                        
                        
                        <ul>
                           
                           <li>
                              <strong>Bareaa Darkhabani</strong> , Director of Facilities <br>
                              <a href="mailto:bdarkhabani@valenciacollege.edu">bdarkhabani@valenciacollege.edu</a> ; 407-582-1499<br>
                              
                           </li>
                           
                           <ul>
                              
                              <li>
                                 <strong>Andrew Youngman</strong>, Construction Specialist<br>
                                 <a href="mailto:ayoungman@valenciacollege.edu">ayoungman@valenciacollege.edu</a> ; 407-582-1703<br>
                                 
                              </li>
                              
                              <li>
                                 <strong>Lee Pahl</strong>, CADD Technician<br>
                                 <a href="mailto:lpahl@valenciacollege.edu">lpahl@valenciacollege.edu</a> ; 407-582-1705<br>
                                 
                              </li>
                              
                           </ul>
                           
                           
                        </ul>            
                        
                        <ul>
                           
                           <li>
                              <strong>Tammy Spencer</strong>, Facilities Accounting Specialist<br>
                              <a href="mailto:tspencer@valenciacollege.edu">tspencer@valenciacollege.edu</a> ; 407-582-1708
                           </li>
                           
                        </ul>            
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/facilities/planning.pcf">©</a>
      </div>
   </body>
</html>