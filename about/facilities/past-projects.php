<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Facilities &amp; Sustainability | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/facilities/past-projects.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/facilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Facilities &amp; Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/facilities/">Facilities</a></li>
               <li>Facilities &amp; Sustainability</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        
                        <h2>Past Projects</h2>
                        
                        <p><strong>East  Campus Projects:</strong></p>
                        
                        <ul>
                           
                           <li>Building  08 (XXXX) <strong>[insert date completed]</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Horizon  West Campus Projects:</strong></p>
                        
                        <ul>
                           
                           <li>Undeveloped  at present time</li>
                           
                        </ul>
                        
                        <p><strong>Lake  Nona Campus Projects:</strong></p>
                        
                        <ul>
                           
                           <li>Building  01 (XXXX) <strong>[insert date completed]</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Osceola  Campus Projects:</strong></p>
                        
                        <ul>
                           
                           <li>Building  04 (XXXX) <strong>[insert date completed]</strong>
                              
                           </li>
                           
                           <li>Building  14 (XXXX) <strong>[insert date completed]</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Poinciana  Campus Projects:</strong></p>
                        
                        <ul>
                           
                           <li>See  current projects</li>
                           
                        </ul>
                        
                        <p><strong>Valencia  College District Office:</strong></p>
                        
                        <ul>
                           
                           <li>Building  01 (XXXX) <strong>[insert date completed]</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>West  Campus Projects:</strong></p>
                        
                        <ul>
                           
                           <li>Building  10 (XXXX) <strong>[insert date completed]</strong>
                              
                           </li>
                           
                           <li>Building  11 ((XXXX) <strong>[insert date completed]</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Winter  Park Campus Projects:</strong></p>
                        
                        <ul>
                           
                           <li>Purchased  Building 01 (XXXX) <strong>[insert  date purchased]</strong>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/facilities/past-projects.pcf">©</a>
      </div>
   </body>
</html>