<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Facilities &amp; Sustainability | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/facilities/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/facilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Facilities &amp; Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Facilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Facilities and Sustainability</h2>
                        
                        <p>Valencia's Facilities and Sustainability Department is integral in the strategic growth,
                           real estate acquisition, and long-range capital planning of the College and sustainable
                           optimization of the College’s physical environment.
                        </p>
                        
                        <p>These planning functions are carried out by two primary teams within the Department:</p>
                        
                        
                        <h3>Planning, Design, and Construction &amp; Sustainability</h3>
                        
                        <p>The Planning, Design, and Construction team is tasked with renewing and refining each
                           campuses palette while contemplating and realizing new architectural aspects and elements
                           within all our campuses. The PD &amp; C team accomplishes this through efficient renovation,
                           tactical remodeling, and inspired new construction in purposeful collaboration with
                           the College’s leadership and stakeholders. This team also provides support with the
                           selection, ordering, and installation of all furniture throughout the College, as
                           well as CADD operations and drafting in support of the teams’ functions.
                        </p>
                        
                        
                        <p>Our Sustainability team supports and advances the comprehensive integration of environmental
                           performance, energy efficiency, and economic responsibility through a variety of initiatives
                           in the College and community. The Sustainability team oversees the requirements of
                           the American College and University Presidents Climate Commitment (ACUPCC); supports
                           faculty integration of sustainability in the curriculum; champions green practices
                           for custodial and grounds services, and directs college wide energy savings efforts.
                        </p>
                        
                        
                        <h4>Our Unifying Purpose:</h4>
                        
                        <p>How can we make a better student experience?</p>
                        
                        <p>
                           Valencia College West Campus <br>
                           1800 South Kirkman Road <br>
                           Building 14, General Services Building<br>
                           407.582.1707 or 407.582.1708<br>
                           Fax 407.582.1209
                           
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/facilities/index.pcf">©</a>
      </div>
   </body>
</html>