<ul>
	<li><a href="index.php">Facilities &amp; Sustainability</a></li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
		<li><a href="planning.php">Planning, Design, Construction &amp; Staff</a></li>
		<li><a href="staff-sustainability.php">Sustainability Staff</a></li>
		<li><a href="services.php">Contracted Services</a></li>
		<li><a href="projects.php">Current Major Projects</a></li>
		<li><a href="documents.php">Relevant Facilities Documents</a></li>
		<li><a href="rfp.php">Request for Proposals</a></li>
		<li><a href="pastprojects.php">Past Projects</a></li>
		</ul>
	</li>
</ul>