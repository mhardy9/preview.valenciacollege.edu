<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Leadership Team  | Valencia College</title>
      <meta name="Description" content="Association of Florida Colleges - Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/facilities/leadership-team.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/facilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/facilities/">Facilities</a></li>
               <li>Leadership Team </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        
                        <div class="wrapper_indent">
                           <img src="http://preview.valenciacollege.edu/images/no_photo.gif"> <br> <strong>Allen Bottorff - P.E., Ph.D</strong>
                           <br> Assistant Vice President: Facilities and Sustainability Department
                           
                           <p></p>
                           <br> <img src="http://preview.valenciacollege.edu/images/no_photo.gif" alt="Valencia image description"> <br>
                           <strong>John Letterman</strong> <br> Managing Director: Plant &amp; Facilities Operations Team
                           
                           <p></p> <img src="http://preview.valenciacollege.edu/images/no_photo.gif" alt="Valencia image description"> <br>
                           <strong>Mark Hose</strong> <br> College Architect: Planning, Design, and Construction Team
                           
                           <p></p> <img src="http://preview.valenciacollege.edu/images/no_photo.gif" alt="Valencia image description"> <br>
                           <strong>Bareaa Darkhabani</strong> <br> Director of Facilities: Planning, Design, and Construction Team
                           
                           <p></p> <img src="http://preview.valenciacollege.edu/images/no_photo.gif" alt="Valencia image description"> <br>
                           <strong>Resham Shirsat, LEED AP</strong> <br> Director of Sustainability: Sustainability Team
                           
                           <p></p> <img src="http://preview.valenciacollege.edu/images/no_photo.gif" alt="Valencia image description"> <br>
                           <strong>Lily Lozada</strong> <br> Administrative Assistant to Managing Director: Plant &amp; Facilities
                           Operations Team
                           
                           <p></p> <img src="http://preview.valenciacollege.edu/images/no_photo.gif" alt="Valencia image description"> <br>
                           <strong>Stephanie Vazquez</strong> <br> Administrative Assistant to AVP: Planning, Design and Construction
                           &amp; Sustainability Team
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               
               
               
               <div class="row">
                  
                  <div class="col-md-8 col-md-offset-2 text-center">
                     
                     <h3>Subscribe to our Newsletter for latest news.</h3>
                     
                     <div id="message-newsletter"></div>
                     
                     <form method="post" action="assets/newsletter.php" name="newsletter" id="newsletter" class="form-inline">
                        <input name="email_newsletter" id="email_newsletter" type="email" value="" placeholder="Your Email" class="form-control">
                        <button id="submit-newsletter" class="button"> Subscribe</button>
                        
                     </form>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/facilities/leadership-team.pcf">©</a>
      </div>
   </body>
</html>