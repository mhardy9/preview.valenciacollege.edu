<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitors &amp; Friends  | Valencia College</title>
      <meta name="Description" content="Visitors &amp;amp; Friends | About | Valencia College">
      <meta name="Keywords" content="college, school, educational, valencia, visitors, friends, about">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Visitors &amp; Friends</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li>About</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Visitors &amp; Friends </h2>
                        
                        
                        <h3>Who We Are</h3>
                        
                        <div class="csList">
                           
                           <ul>
                              
                              
                              <li><a href="/aboutus/">About Valencia</a></li>
                              
                              <li><a href="/aboutus/administration.cfm">Administration</a></li>
                              
                              <li><a href="/aboutus/awards-recognition.cfm">Awards and Recognition</a></li>
                              
                              <li><a href="/aboutus/trustees.cfm">Board of Trustees </a></li>
                              
                              <li><a href="/aboutus/facts.cfm">Facts</a></li>
                              
                              <li><a href="/aboutus/history.cfm">History</a></li>
                              
                              <li><a href="/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
                              
                              <li><a href="/lci/">Learning Centered</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/aboutus/vision.cfm">Learning College</a></li>
                              
                              <li><a href="/map/">Locations</a></li>
                              
                              <li><a href="http://president.valenciacollege.edu/">Office of the President</a></li>
                              
                              <li><a href="/contact/directory.cfm">Phone Directory</a></li>
                              
                              <li><a href="/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Planning</a></li>
                              
                              <li><a href="/sustainability/">Sustainability</a></li>
                              
                              <li><a href="/aboutus/vision.cfm">Vision, Values &amp; Mission </a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Doing Business with Valencia </h3>
                        
                        <div class="csList">
                           
                           <ul>
                              
                              <li><a href="/facilities/">Facilities</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/brand/">Marketing and Brand Guidelines </a></li>                       
                              
                              <li><a href="/procurement/">Procurement</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>What We Offer</h3>
                        
                        
                        <div class="csList">
                           
                           <ul>
                              
                              <li><a href="http://preview.valenciacollege.edu/continuing-education">Continuing Education </a></li>
                              
                              <li><a href="/students/courses.cfm">Courses Offered</a></li>
                              
                              <li><a href="/programs/">Degrees &amp; Programs </a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>In the Community</h3>
                        
                        <div class="csList">
                           
                           <ul>
                              
                              <li><a href="/alumni/">Alumni Association</a></li>
                              
                              <li><a href="/bac/">Black Advisory Committee</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/economicdevelopment/">Economic Development</a></li>
                              
                              <li><a href="/internship/employers/">Interns for Employers</a></li>
                              
                              <li><a href="/retireeconnection/">Retiree Connection</a></li>
                              
                              <li><a href="/servicelearning/">Service Learning</a></li>
                              
                              <li><a href="/facultystaff/specialprograms.cfm">Special Programs</a></li>
                              
                              <li><a href="http://www.valencia.org/">Valencia Foundation</a></li>
                              
                              <li><a href="/valencia-promise/">Valencia Promise</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit Valencia </a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        <a target="_blank" href="http://preview.valenciacollege.edu/economicdevelopment/"><img src="/_resources/img/about/visitors/featured-economic-development-4.png" width="550" height="350" border="0" alt="22% of all UCF grads started at Valencia College"></a>
                        
                        
                        <p>&nbsp;</p>
                        
                        
                        <p><a href="http://www.valencia.org/fsg/" target="_blank"><img src="/_resources/img/about/visitors/16ALU004-giving-campaign-270x60.jpg" width="270" height="60" alt="Faculty &amp; Staff Giving Opportunity"></a>
                           
                           &nbsp;
                           
                           <a href="http://net4.valenciacollege.edu/promos/Internal/Foundation.cfm"><img src="/_resources/img/about/visitors/giving_foundation.png" alt="Giving to the Valencia Foundation" width="270" height="60" border="0"></a>
                           
                           
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/index.pcf">©</a>
      </div>
   </body>
</html>