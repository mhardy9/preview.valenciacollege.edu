<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Privacy Policy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/privacy/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/privacy/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Privacy Policy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Privacy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Privacy Statement </h2>
                        
                        <blockquote>
                           
                           <p><strong>Valencia College<br>
                                 Information Technology Resource Policies <br>
                                 [6Hx28:04-38]<br>
                                 Implementing Procedures for
                                 Online Privacy, Access and Security<br>
                                 [04-38.04]</strong><br>
                              
                           </p>
                           
                        </blockquote>
                        <strong>Online Privacy Statement<br>
                           <br>
                           </strong>The policy of Valencia College is to respect the 
                        privacy of all Valencia proprietary web site visitors to the extent permitted by law.
                        
                        This online privacy statement is intended to inform you of the 
                        ways in which these web sites collect information, the uses to 
                        which that information will be put, and the ways in which we will 
                        protect any information you choose to provide us. <br>
                        <br>
                        There are four types of information that this site may collect 
                        during your visit: network traffic logs, web visit logs, cookies, 
                        and information voluntarily provided by you. <br>
                        <br>
                        <strong>Network Traffic Logs</strong> <br>
                        In the course of ensuring network security and consistent service 
                        for all users, Valencia College employs software programs 
                        to do such things as monitor network traffic, identify unauthorized 
                        access or access to nonpublic information, detect computer viruses 
                        and other software that might damage college computers or the 
                        network, and monitor and tune the performance of the college network. 
                        In the course of such monitoring, these programs may detect such 
                        information as e-mail headers, addresses from network packets, 
                        and other information. Information from these activities is used 
                        only for the purpose of maintaining the security and performance 
                        of the college's networks and computer systems. Personally identifiable 
                        information from these activities is not released to external 
                        parties without your consent unless required by law. <br>
                        <br>
                        <strong>Web Visit Logs</strong> <br>
                        Valencia College web sites routinely collect and store 
                        information from online visitors to help manage those sites and 
                        improve service. This information includes the pages visited on 
                        the site, the date and time of the visit, the internet address 
                        (URL or IP address) of the referring site, the domain name and 
                        IP address from which the access occurred, the version of browser 
                        used, the capabilities of the browser, and search terms used on 
                        our search engines. This site makes no attempt to identify individual 
                        visitors from this information: any personally identifiable information 
                        is not released to external parties without your consent unless 
                        required by law. <br>
                        <br>
                        <strong>Cookies </strong><br>
                        Cookies are pieces of information stored by your web browser on 
                        behalf of a web site and returned to the web site on request. 
                        This site may use cookies for two purposes: to carry data about 
                        your current session at the site from one web page to the next, 
                        and to identify you to the site between visits. If you prefer 
                        not to receive cookies, you may turn them off in your browser, 
                        or may set your browser to ask you before accepting a new cookie. 
                        Some pages may not function properly if the cookies are turned 
                        off. Unless otherwise notified on this site, we will not store 
                        data, other than for these two purposes, in cookies. Cookies remain 
                        on your computer, and accordingly we neither store cookies on 
                        our computers nor forward them to any external parties. Unless 
                        otherwise notified on this site, we do not use cookies to track 
                        your movement among different web sites and do not exchange cookies 
                        with other entities. <br>
                        <br>
                        <strong>Information Voluntarily Provided by You </strong><br>
                        In the course of using these web sites, you may choose to provide 
                        us with information to help us serve your needs. For example, 
                        you may send us electronic mail (through a mailer or a web form) 
                        to request information, you may sign up for a mailing list, or 
                        you may send us your address so we may send you an application 
                        or other material. Any personally identifiable information you 
                        send us will be used only for the purpose indicated. Requests 
                        for information will be directed to the appropriate staff to respond 
                        to the request, and may be recorded to help us update our site 
                        to better respond to similar requests. We will not sell, exchange 
                        or otherwise distribute your personally identifiable information 
                        without your consent, except to the extent required by law. We 
                        do reserve the right to work with third-party vendors, to host 
                        this information solely for the purposes intended by Valencia 
                        College and in accordance with this policy. We 
                        do not retain the information longer than necessary for normal 
                        operations. Each web page requesting information discloses the 
                        purpose of that information. If you do not wish to have the information 
                        used in that manner, you are not required to provide it. Please 
                        contact the person listed on the specific page, or listed below, 
                        with questions or concerns on the use of personally identifiable 
                        information. <br>
                        <br>
                        Valencia College web sites and subsites may provide 
                        links to other World Wide Web sites or resources. We do not control 
                        these sites and resources, do not endorse them, and are not responsible 
                        for their availability, content, or delivery of services. In particular, 
                        external sites are not bound by the college's online privacy policy; 
                        they may have their own policies or none at all. Often you can 
                        tell you are leaving a Valencia College web site by 
                        noting the URL of the destination site. <br>
                        <br>
                        If you have questions, please contact the Vice President for Policy 
                        and General Counsel via email <a href="mailto:policy@valenciacollege.edu?Subject=Regarding%20Privacy%20Statement">policy@valenciacollege.edu</a> or telephone (407) 582-3433.
                        
                        <p>Online Privacy, Access and Security [04-38.04] Revision: July 
                           10, 2012
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/privacy/index.pcf">©</a>
      </div>
   </body>
</html>