<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Privacy Policy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/privacy/ssnusage.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/privacy/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Privacy Policy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/privacy/">Privacy</a></li>
               <li>Privacy Policy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Notification of Social Security Number Collection and  Usage</h2>
                        
                        <p>In compliance with FL Statute 119.071(5), this document  serves to notify you of the
                           purpose for the collection and usage of your Social  Security Number (SSN).<br>
                           Valencia College (Valencia) collects and uses your  SSN only for the following purposes
                           in performance of the College’s duties and  responsibilities. To protect your identity,
                           Valencia will secure your SSN from  unauthorized access, strictly prohibits the release
                           of your SSN to unauthorized  parties contrary to state and federal law, and assigns
                           you a unique  student/employee identification number. This unique ID number is used
                           for all  associated employment and educational purposes at Valencia.
                        </p>
                        
                        <p><a href="#TableofStatutoryRegulatoryAuthority">Table of Statutory and Regulatory Authority</a></p>
                        
                        <h3>EMPLOYEES</h3>
                        
                        <p><strong>Human Resources </strong><br>
                           Your SSN is used for legitimate business purposes for  completing and processing the
                           following:
                        </p>
                        
                        <ul>
                           
                           <li>Federal I-9 (Department of Homeland Security)</li>
                           
                           <li>Federal W4, W2, 1099 (Internal Revenue Service)</li>
                           
                           <li>Federal Social Security taxes (FICA)</li>
                           
                           <li>Distributing Federal W2 (Internal Revenue Service)</li>
                           
                           <li>Unemployment Reports (FL Dept of Revenue)</li>
                           
                           <li>Florida Retirement Contribution reports (FL Dept of  Revenue)</li>
                           
                           <li>Workers Comp Claims (FCCRMC and Department of Labor)</li>
                           
                           <li>403b contribution reports</li>
                           
                           <li>Group health, life and dental coverage enrollment</li>
                           
                           <li>Work study work assignment</li>
                           
                        </ul>
                        
                        <p>Providing your Social Security Card is a condition of  employment at Valencia.</p>
                        
                        
                        <h3>STUDENTS</h3>
                        
                        <p><strong>Admissions </strong><br>
                           Federal legislation relating to the Hope Tax Credit requires  that all postsecondary
                           institutions report student SSN’s to the Internal  Revenue Service (IRS). This IRS
                           requirement makes it necessary for community  colleges to collect the SSN of every
                           student. A student may refuse to disclose  his or her SSN to the College for this
                           purpose, but the IRS is then authorized  to fine the student in the amount of $50.00.
                        </p>
                        
                        <p>In addition to the federal reporting requirements, the  public school system in Florida
                           uses SSN’s as a student identifier (section 1008.386, F.S.). In a seamless K-20  system,
                           it is beneficial for postsecondary institutions to have access to the same  information
                           for purposes of tracking and assisting students in the smooth  transition form one
                           education level to the next. 
                        </p>
                        
                        <p><strong>Financial Aid </strong><br>
                           A student’s SSN is required for the following financial aid  purposes:<br>
                           The United States Department of Education’s (USDOE) Free  Application for Federal
                           Student Aid (FAFSA) requires all applicants to report  their SSN to be used for all
                           federal financial aid programs as a student identifier  for processing and reporting.
                           In addition to its use by USDOE as a student  identifier, the SSN is required in order
                           for the Department of Homeland  Security to investigate citizenship status, for the
                           Federal Work Study Program,  and is required on all loan applications for use by the
                           lender/servicer/guarantor.
                        </p>
                        
                        <p>Valencia  collects a student’s SSN on certain institutional scholarship applications
                           for  student files and federal and state audit/reporting purposes.
                        </p>
                        
                        <p>If you are a recipient of a State of Florida grant or scholarship such as the  Florida
                           Student<br>
                           Assistance Grant,   Florida Work Experience or Bright  Futures the State of Florida
                           Department of Education will require the use of  the SSN on their grant/scholarship
                           disbursement website and for reporting  purposes.
                        </p>
                        
                        <p><strong>Library</strong><br>
                           Student, faculty, and staff Valencia ID (VID) number will be used in the libraries'
                           patron database (LINCC) for online login authentication, patron verification, and
                           the elimination of duplicate records.
                        </p>
                        
                        <p><strong>Outreach Programs</strong><br>
                           The Bridges and College Reach-Out Programs are youth outreach  (intervention) projects
                           funded by discretionary grants from the US or FL  Departments of Education. In order
                           to verify a participant’s project  eligibility, social security numbers are required
                           and also later used when  submitting information for the Annual Performance Reports
                           due to the US or FL Departments  of Education.
                        </p>
                        
                        <p><strong>Workforce Programs</strong> (i.e., BANNER Center)<br>
                           These programs, funded through the Agency for Workforce  Innovation (AWI), use your
                           SSN as an identifier for program enrollment and  completion. Also, it is used for
                           entering placement information into either the  OSMIS or the Employ Florida Marketplace
                           statewide data collection and reporting  system. Because these are performance based
                           contract programs, AWI requires  that all participants and their program related activities
                           be recorded in the Florida state system.
                        </p>
                        
                        <p><strong>CONTRACTORS:</strong><br>
                           Valencia  collects contractor SSN information in order to file the required information
                           returns with the Internal Revenue Service, as required and authorized by  federal
                           law.
                        </p>            
                        
                        <p> Appendix 1</p>
                        
                        <p><strong>State and Federal Statutes and Regulations</strong></p>
                        
                        <p><strong>That Mandate or Authorize the Use of Social Security Numbers</strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Purpose </div>
                                 
                                 <div data-old-tag="th">Required Use of SSN's </div>
                                 
                                 <div data-old-tag="th">Notice Requirements </div>
                                 
                                 <div data-old-tag="th">Statute</div>
                                 
                                 <div data-old-tag="th">Mandatory</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">EMPLOYEES</div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Employment / Hiring</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>College required to collect and examine employee's SSN for verification, work eligibility,
                                       and reporting for social security and taxes.
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>Employment Application</li>
                                       
                                       <li>I-9</li>
                                       
                                       <li>SSN tax/FICA</li>
                                       
                                       <li>W-9's (request for SSN)</li>
                                       
                                       <li>W-2 statement / re-issue</li>
                                       
                                       <li>W-4</li>
                                       
                                    </ul>                
                                 </div>
                                 
                                 <div data-old-tag="td">Filing and issuance of federal employment forms; I-9, W-4, W-9, and annual wage statements;
                                    Notice provided on forms 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>8 USC 1324a(b); (I-9)</p>
                                    
                                    <p>20 CFR 404.452 (W-4)</p>
                                    
                                    <p>26 CFR 31.6011(b)-2</p>
                                    
                                    <p>requires employee provide SSN to employer;</p>
                                    
                                    <p>119.071(5), FS regarding the collection, use and disclosure of SSNs </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Yes, it is mandatory, however subject to the conditions of use in the Privacy Act
                                    of 1974, 5 USC 552a (unlawful to deny benefits upon refusal to disclose SSNs unless
                                    authorized by federal statute, or law enacted prior to 1975) 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Employment (Contractor) </div>
                                 
                                 <div data-old-tag="td">Federal W-9, Form 1099 </div>
                                 
                                 
                                 <div data-old-tag="td">Internal Revenue Code, Section 6109; Title 26 US Code </div>
                                 
                                 <div data-old-tag="td">Required (if no FEIN provided) </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Employment</div>
                                 
                                 <div data-old-tag="td">Unemployment</div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">State of Florida; disclosure per 119.071(5)(a)6.b., FS </div>
                                 
                                 <div data-old-tag="td">Yes, for administrative use </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Employment</div>
                                 
                                 <div data-old-tag="td">Retirement</div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">Florida Department of Revenue - disclosure per 119.071(5)(a)6.b. </div>
                                 
                                 <div data-old-tag="td">Yes, for administrative use </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Employment</div>
                                 
                                 <div data-old-tag="td">Workers Compensation Claims </div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">440.185, F.S. Department of Labor, FCCRMC </div>
                                 
                                 <div data-old-tag="td">Yes, for administrative use </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Employment</div>
                                 
                                 <div data-old-tag="td">403(b) Contribution Reports </div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">US Tax Code 501(c)(3) </div>
                                 
                                 <div data-old-tag="td">Yes, for administrative use </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Employment</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Group health, life and dental coverage enrollment </p>
                                    
                                    <p>Employee and dependents of employee </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Notice of Admistrative use or HR form for employees and dependents </div>
                                 
                                 <div data-old-tag="td">119.071(5)(a)6.f. permits disclosure of SSNs held by agency for purpose of administering
                                    health plan for employees and dependents. 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Authorized</p>
                                    
                                    <p>Notice of Admistrative use or HR form for employees and dependents </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Contracts and Grants </div>
                                 
                                 <div data-old-tag="td">With respect to certain federal contracts / grants requires reports including employee
                                    SSN's for equal employment opportunity reports and reports to IRS 
                                 </div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>41 CFR 60-4.3 </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Yes, where required reporting. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STUDENTS</strong></div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Financial Aid </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Requires institution to verify student's SSN, collection of information, including
                                       parents of dependents seeking aid. Requires student to submit SSN to obtain grant,
                                       loan or work assistance. 
                                    </p>
                                    
                                    <p>Requires institutions to verify SSN in national database and reporting.</p>
                                    
                                    <p>Required to complete FAFSA.</p>
                                    
                                    <p>Required for Bright Futures and other scholarships for eligibility purposes.  </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Notice through FAFSA and applications </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>20 USC 1078; 20 USC sections 1090, 1091, 1092.</p>
                                    
                                    <p>Section 483 of the Higher Education Act of 1965 (collection of SSNs of students and
                                       parents)
                                    </p>
                                    
                                    <p>34 CFR 668.16, (administrative use)</p>
                                    
                                    <p>34 CFR 668.33 (verify residency)</p>
                                    
                                    <p>34 CFR 668.36, (verify with FAFSA) </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Yes, to be eligible for assistance. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Admissions / Application</div>
                                 
                                 <div data-old-tag="td">Requires SSNs for tracking students and authorizes collection under IRS Code </div>
                                 
                                 <div data-old-tag="td">Application statement / notice </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>1008.386 , F.S. (College may not require SSN as a condition of </p>
                                    
                                    <p>admission or graduation) </p>
                                    
                                    <p>SBE Rule 6A-1.0955(3)(e) (authorizes college to maintain information including SSN
                                       on adult students)
                                    </p>
                                    
                                    <p>IRC Section 25A</p>
                                    
                                    <p>(Hope/Lifetime learning credit tax reporting)</p>
                                    
                                    <p>119.071(5)(a), FS. Collection and disclosure </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Student may refuse, but may be subject to $50 penalty under IRC Sect.25A. (Hope /
                                       Lifetime Learning tax credit) 
                                    </p>
                                    
                                    <p>Pursuant to 1008.386 F.S.</p>
                                    
                                    <p>College may not require SSN as a condition of admission or of graduation  </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Student Income Taxes </div>
                                 
                                 <div data-old-tag="td">Issuance of Forms 1098T (tuition payments report) and 1098 E (loan interest) </div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>26 USC 3402, 6051</p>
                                    
                                    <p>26 CFR 1.6050  </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">SSN or TIN required for filing </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Hope / Lifetime Tax Credit </div>
                                 
                                 <div data-old-tag="td">Reporting of SSNs to IRS </div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">Federal Register, June 16, 2000\IRC Sect. 25A </div>
                                 
                                 <div data-old-tag="td">Student may refuse, but subject to fine. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>College Reach-Out program (CROP) </p>
                                    
                                    <p>Agency for Workforce Programs</p>
                                    
                                    <p>Federal Work Study </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Administrative use and state (FDOE) or federal reporting (DOE); SSNs may also be used
                                    for verifying eligibility for programs and establishing residency requirements. 
                                 </div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>119.071(5)(a), FS.</p>
                                    
                                    <p>FWS</p>
                                    
                                    <p>34CFR 668.36</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Yes, eligibility and administrative use </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>          
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/privacy/ssnusage.pcf">©</a>
      </div>
   </body>
</html>