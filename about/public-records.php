<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Custodian of Public Records | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-records.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Custodian of Public Records</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Custodian of Public Records</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        <h2>CONTACT INFORMATION FOR CUSTODIAN OF PUBLIC RECORDS*</h2>
                        
                        <p>*FOR PURPOSES OF FILING PUBLIC RECORDS CIVIL ACTIONS</p>
                        
                        <h3 dir="auto">Pursuant to Section 119.12, F.S., prior to filing any civil action to enforce Florida's
                           public records laws, a public records requestor must provide written notice identifying
                           the public records request(s) at issue, at least five (5) business days before filing
                           the civil action, to:
                        </h3>
                        
                        <blockquote>
                           
                           <h3 dir="auto"> Valencia College<br>
                              Office of Director, Contracts and Records<br>
                              1768 Park Center Drive DO - 353<br>
                              Orlando, Florida 32835
                           </h3>
                           
                           <h3 dir="auto"> Attn: Public Records Request<br>
                              Telephone (407) 582-3429<br>
                              <a href="mailto:publicrecordsnotice@valenciacollege.edu" rel="noreferrer">publicrecordsnotice@valenciacollege.edu</a>
                              
                           </h3>
                           
                        </blockquote>
                        
                        <hr>
                        
                        
                        <ul>
                           
                           <li>              The President of Valencia College is charged with responsibility for
                              the operation and administration of the College, including the management of public
                              records. To effectively administer those records, the President has designated multiple
                              personnel in all areas of College operations to serve as the "custodian of public
                              records" for those areas, as that term is defined in Section 119.011(5), F.S.
                           </li>
                           
                           <li>For the convenience of the public, and for purposes of compliance with the requirements
                              of Section 119.12, F.S., the Office of the Director, Contracts and Records is designated
                              as the general Custodian of Public Records, specifically to receive written notices
                              provided by public records requestors prior to their filing of civil actions to enforce
                              the provisions of Florida's public records laws, in accordance with the requirements
                              set forth in Section 119.12, F.S.
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-records.pcf">©</a>
      </div>
   </body>
</html>