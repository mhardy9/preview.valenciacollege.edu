<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Narrative Time Line  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/budget/time-line.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/budget/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/budget/">Budget</a></li>
               <li>Narrative Time Line </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Narrative Time Line</h2>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in"></div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>2012-2013 Budget Timeline</strong></p>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top">
                                    
                                    <td width="126">November 18th</td>
                                    
                                    <td width="569">
                                       
                                       <p align="left">Memo from Assistant Vice President for Budget and Auxiliary Services describing the
                                          current year budget process.
                                       </p>
                                       
                                       <p>Begin 2012-2013 Budget Development System set up</p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>November 30th</td>
                                    
                                    <td>
                                       
                                       <p>Budget Development System (BDS) available for online input of the 2012-2013 budget</p>
                                       
                                       <p>Budget Development System (BDS) training begins</p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>December/January</td>
                                    
                                    <td>Review with Sr. Staff vacant positions and new positions</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>January 18th</td>
                                    
                                    <td>Budget Development System working sessions</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <br> January 20th
                                       
                                    </td>
                                    
                                    <td>Budget Office receives 2012-2013 individual department equipment needs from Sr. Staff&nbsp;<strong>prioritized</strong>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>January 30th</td>
                                    
                                    <td>Deans provide the Budget Office the number of faculty participating in the Professional
                                       Development
                                       Plan and at what level ($750 or $1500)
                                       
                                       <p>Receive pay-out information from AVP of Academic Learning Support regarding Institutional
                                          Assessment goal
                                       </p>
                                       
                                       <p>All paperwork for special lab fees should be submitted to Kim Adams for consideration
                                          at the
                                          February 8th Curriculum Committee meeting
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>February 1st</td>
                                    
                                    <td>Budget Office sends college-wide proritized equipment needs list to OIT and Facilities
                                       for review
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>February 20th</td>
                                    
                                    <td>
                                       
                                       <p>Departments complete online budget entry</p>
                                       
                                       <p>BDS is locked, no online entry except for Budget Office</p>
                                       
                                       <p>Submit to Budget Office suplemental funding requests using the Supplemental Budget
                                          Request Form
                                       </p>
                                       
                                       <p>Submit to Budget Office Overload/Adjunct forms</p>
                                       
                                       <p>Budget Development System working session</p>
                                       
                                       <p>Open, vacant, temporary 4 month and 8 month position analysis mailed to each Vice
                                          President
                                       </p>
                                       
                                       <p>OIT and Facilities comments/recommendations of the 2012-2013 prioritized equipment
                                          needs list is
                                          received in the Budget Office
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>February 27th</td>
                                    
                                    <td>
                                       
                                       <p>Scenarios regarding pay increases due to HR and Budget Office</p>
                                       
                                       <p>Prioritized equipment list is shared with Vice President for Academic Affairs, Vice
                                          President for
                                          Student Affairs, Assistant Vice President for Resource Development and Assistant Vice
                                          President for
                                          Workforce Development for any grant opportunities or special funding that may be available
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <br> March 16th
                                       
                                    </td>
                                    
                                    <td>Vice Presidents send confirmation to Human Resources and Budget Office regarding the
                                       analysis of
                                       open, vacant, temporary, 4 month and 8 month positions to be included in the 2012-2013
                                       operating
                                       budget
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>March 19th</td>
                                    
                                    <td>OIT to submit Out-of-Cycle Emergency computer list to Budget Office</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>April 2nd</td>
                                    
                                    <td>
                                       
                                       <p>Human Resources complete Salary Modeling and submit to the Budget Office</p>
                                       
                                       <p>Human Resources to report on exact number of positions included in the Salary Modeling</p>
                                       
                                       <p>Budget Office to send Colegewide Equipment needs list to Sr. Staff for review before
                                          the COC
                                          meeting
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>April 11th</td>
                                    
                                    <td>Budget Office meets COC to prioritize Collegewide Equipment needs list</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>April 16th</td>
                                    
                                    <td>Library to submit budget by FTE for Books index</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>April 17th</td>
                                    
                                    <td>Budget Office meets with Sr. Staff to discuss priorities and make any needed adjustments</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>April 23rd</td>
                                    
                                    <td>District Board of Trustees budget meetings begin</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>April 26th</td>
                                    
                                    <td>
                                       
                                       <p>CPC to forward approved 2012-2013 SBI information to Budget Office</p>
                                       
                                       <p>SPD budget allocation recommendations submitted to Budget Office</p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>May 1st</td>
                                    
                                    <td>Budget Office meets with Sr. Staff to further refine budget recommendations before
                                       submission to
                                       President
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>May 10th</td>
                                    
                                    <td>Budget Office presents collegewide budget recommendation to the President for review</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>May 14th</td>
                                    
                                    <td>Communicate tuition fees for the 2012-2013 fiscal year (Business Office, CJI, Course
                                       Catalog)
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>May 15th</td>
                                    
                                    <td>District Board of Trustees develop Board Budget Principles</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>May 28th</td>
                                    
                                    <td>
                                       
                                       <p>Budget Office presents recommendation to College Planning Council</p>
                                       
                                       <p>Salary schedule completed and sent to the Budget Office for incorporation into the
                                          College
                                          Operating Budget package
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>June 1st</td>
                                    
                                    <td>College Operating Budget package due for inclusion in Board package</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>June 19th</td>
                                    
                                    <td>Present budget recommendation to the District Board of Trustees for approval</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>June 22nd</td>
                                    
                                    <td>Budget Office and OIT Programmer to load 2012-2013 budget into Banner</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>June 26th</td>
                                    
                                    <td>Roll 2012-2013 positions</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>June 29th</td>
                                    
                                    <td>Budget document submitted to the Florida College System</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/budget/time-line.pcf">©</a>
      </div>
   </body>
</html>