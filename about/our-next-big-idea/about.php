<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Our Next Big Idea | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/our-next-big-idea/about.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/our-next-big-idea/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Our Next Big Idea</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/our-next-big-idea/">Our Next Big Idea</a></li>
               <li>Our Next Big Idea</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>About QEP</h2>
                        
                        
                        <p>The QEP is a long-term, college-wide project focused on improvement, enrichment and
                           enhancement of the student learning experience, and a major part of our reaffirmation
                           process through the <a href="http://www.sacscoc.org/" target="_blank" title="Southern Association of Colleges and Schools">Southern Association of Colleges and Schools (SACS)</a>, a regional accreditation agency for an eleven-state region.
                        </p>
                        
                        
                        <p>Every 10 years, Valencia goes through a process of <a href="http://www.sacscoc.org/pdf/2012PrinciplesOfAcreditation.pdf" target="blank" title="">reaffirming our accreditation with SACS</a>. The revised accreditation process is particularly focused on student learning and
                           requires the development a QEP that we will implement over a five-year period. Our
                           plan must be submitted in September 2013.
                           
                        </p>
                        
                        
                        
                        <p>The Quality Enhancement Plan (QEP) describes a carefully designed course of action
                           that
                        </p>
                        
                        <ul>
                           
                           <li>Is well-defined and focused on a topic or issue</li>
                           
                           <li>Is focused on enhancing student learning and/or the environment supporting student
                              learning
                           </li>
                           
                           <li>Is clearly connected to the mission of the college</li>
                           
                        </ul>
                        
                        
                        <p>Our QEP should</p>
                        
                        <ul>
                           
                           <li>be embedded within the college's planning and evaluation process</li>
                           
                           <li>evolve from existing work or from emerging concerns about student learning</li>
                           
                           <li>be evaluated based on relevant data</li>
                           
                        </ul>
                        
                        
                        
                        
                        <p><a href="documents/QEP_Design_Principles.docx" title="QEP Design Principles">QEP Design Principles</a></p>
                        
                        <p><a href="documents/QEP_Core_Team_Membership.pdf" title="QEP Core Team">QEP Core Team</a></p>
                        
                        
                        
                        <h3>QEP � The Next Big Idea to Guide Our Work into the Future</h3>
                        
                        <p>Many of Valencia's current programs and innovative solutions began as commonly held
                           ideas about the way students learn. So, in preparation for the development of Valencia's
                           Quality Enhancement Plan - recognizing that the plan should evolve from existing work
                           � we are focusing on the emerging ideas within the College to build the foundation
                           for this work.  Valencia's current Big Ideas have laid the groundwork for us to innovate,
                           create and achieve our mission authentically. Through our discussions, considering
                           what we already know about our students, and by connecting to the national conversation
                           on student completion, it is our hope that these courses of action will produce the
                           emergence of our next "big idea." To find out how you can be involved in creating
                           the next big idea, click "Get Involved" on the navigation bar. 
                        </p>
                        
                        <img src="qep-pipeline.jpg" width="770">
                        
                        <p><a href="documents/qep-up-to-date.pdf" title="QEP Design Principles">QEP Up-to-Date</a></p>
                        
                        <p><a href="documents/vision-statement.pdf" title="QEP Core Team">New Student Experience at Valencia</a></p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/our-next-big-idea/about.pcf">©</a>
      </div>
   </body>
</html>