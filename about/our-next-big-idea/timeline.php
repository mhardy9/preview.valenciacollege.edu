<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Our Next Big Idea | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/our-next-big-idea/timeline.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/our-next-big-idea/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Our Next Big Idea</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/our-next-big-idea/">Our Next Big Idea</a></li>
               <li>Our Next Big Idea</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Timeline</h2>
                        
                        <p><a href="documents/QEPTimelineSpring13.pdf"> QEP Timeline Spring 13</a></p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Documents</h3>
                        
                        <p><a href="documents/ADiscussionwithBetsyBarefoot.pdf">A Discussion with Betsy Barefoot</a></p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Student Discussions</h3>
                        
                        <p><a href="documents/Fall2012StudentCampusDiscussionDates.pdf">Fall 2012 Student Campus Discussion Dates</a></p>
                        
                        <p><a href="documents/OsceolaCampus_StudentDiscussionGroupSummary.pdf">Osceola Campus _ Student Discussion Group Summary</a></p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Summer Reading Circles</h3>
                        
                        <p><a href="documents/QEPReadingCircleMeetingsandLeaderList.pdf">QEP Reading Circle Meetings and Leader List</a></p>
                        
                        <p><a href="documents/SummaryofQEPSummerReadingCircles.pdf">2012 Summer Reading Circles Summaries </a></p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Big Idea Groups</h3>
                        
                        <p><a href="documents/NewStudentExp_CampusSummary.pdf">New Student Experience_Campus Summary</a></p>
                        
                        <p><a href="documents/StudentLearning_CampusSummary.pdf">Student Learning_Campus Summary</a></p>
                        
                        <p><a href="documents/StudentPurpose_CampusSummary.pdf">Student Purpose_Campus Summary</a></p>
                        
                        <p><a href="documents/StudentNavigation_CampusSummary.pdf">Student Navigation Campus Summary </a></p>
                        
                        <h3>&nbsp; </h3>
                        
                        <h3>Information Sessions</h3>
                        
                        <p><a href="http://prezi.com/befuhuvlwewr/qep-kick-off-september-7/?auth_key=f1e681d6ada8f62398d89a3b7a52df83805a4a64&amp;kw=view-befuhuvlwewr&amp;rc=ref-643541"> QEP Kickoff Event </a></p>
                        
                        <p><a href="documents/NovemberSummaryPresentation.pdf">November BIGS Summary Presentation</a></p>
                        
                        <p><a href="documents/LearningDay2013Presentation.pdf">Learning Day 2013 Presentation</a></p>
                        
                        <p><a href="documents/QEPSpring13InformationSessions.pdf">QEP Spring 13 Information Sessions</a></p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>QEP Task Force Plans - Summer 2013</h3>
                        
                        
                        <p><a href="documents/TransitiontoCollegeQEPDesignandAssessmentJuly2013.pdf"><strong>Introduction to Valencia</strong></a></p>
                        
                        <p><strong>Common Curricular Experience</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CommonCurricularExperienceSLOsFINAL07102013.pdf">Common Curricular Experience SLO's</a></li>
                           
                           <li><a href="documents/CommonCurricularExperienceQEPDesignandAssessment.pdf">Common Curricular Experience QEP Design and Assessment</a></li>
                           
                           <li><a href="documents/StudentLearningOutcomesREFINED07082013.pdf">Student Learning Outcomes</a></li>
                           
                        </ul>
                        
                        <p><strong>Co-Curricular Engagement</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/TaskforcechartJuly2013.pdf">Taskforce Chart</a></li>
                           
                           <li><a href="documents/Co-CurricularProgramJulyDraft.pdf">Co-Curricular Program</a></li>
                           
                        </ul>
                        
                        <p><a href="documents/ProgressiontoDegreeReadiness.pdf"><strong>Progression to Degree Readiness</strong></a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/our-next-big-idea/timeline.pcf">©</a>
      </div>
   </body>
</html>