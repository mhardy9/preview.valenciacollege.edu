<ul>
<li><a href="/ournextbigidea/">Home</a></li>
<li><a href="/ournextbigidea/about.cfm">What is a QEP?</a></li>
<li><a href="/ournextbigidea/valencias-qep.cfm">Valencia's QEP</a></li>
<li><a href="/ournextbigidea/timeline.cfm">QEP Development Process</a></li>
<li><a href="/ournextbigidea/history-resources.cfm">History and Resources</a></li>
<li><a href="/ournextbigidea/qep.cfm">Our Next Big Idea</a></li>
<li><a href="/ournextbigidea/videos.cfm">Videos</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/ournextbigidea/contact.cfm" target="_blank">Contact</a></li>
</ul>