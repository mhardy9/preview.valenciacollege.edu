<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Our Next Big Idea | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/our-next-big-idea/history-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/our-next-big-idea/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Our Next Big Idea</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/our-next-big-idea/">Our Next Big Idea</a></li>
               <li>Our Next Big Idea</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Valencia History</h2>
                        
                        <p>In our process to develop the QEP, it was important to ensure the connection to the
                           college's past in order to build an authentic continuation of our work. Guided by
                           the QEP design principles, the QEP process should "demonstrate a tangible and meaningful
                           connection to the College's Learning-Centered past," while focusing on the student
                           experience. A review of Valencia's history is integral to this approach and will allow
                           us to "build upon other institutional activities and priorities wherever possible
                           (e.g. the 2008-2015 Strategic Plan, Campus Plans, Program Reviews for the Associate
                           in Arts and Science Degrees, Direct Connect, etc.).
                        </p>
                        
                        
                        <p>"Valencia Community College: A History of an Extraordinary Learning Community"</p>
                        
                        <p>The history of Valencia chronicled here tells of a college populated by people of
                           extraordinary commitment, talent, energy, and hope. These, and many other strengths,
                           have made and continue to make Valencia a remarkable place to learn and work.<br><br>-Dr. Sanford Shugart, Valencia College President
                        </p>
                        
                        <div><a href="../documents/isu/Valencia-History-Book.pdf.html"> Valencia History Book</a></div>
                        
                        
                        <p><a href="documents/HistoryMilestones.pdf" title="Valencia History Milestone">Valencia History Milestones</a></p>
                        
                        
                        
                        <h3>Our Work</h3>
                        
                        <p><strong>Websites</strong></p>
                        
                        <p><a href="../../academic-affairs/institutional-effectiveness-planning/institutional-assessment/index.html" target="_blank" title="Institutional Assessment">Institutional Assessment</a></p>
                        
                        <p><a href="../../competencies/index.html" target="_blank" title="TVCA">TVCA</a></p>
                        
                        
                        
                        
                        
                        <p><strong>Documents</strong></p>
                        
                        <p><a href="../academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/FoEFinalReport-ApprovedbyCLC5-7-09.pdf.html" target="_blank" title="Foundations of Excellence">Foundations of Excellence</a></p>
                        
                        <p><a href="documents/Big_Meeting_2012.pptx" target="_blank" title="Strategic Planning: Big Meeting 2012 PowerPoint">Strategic Planning: Big Meeting 2012 PowerPoint</a></p>
                        
                        <p><a href="documents/East_WinterPark_Campus_Plan.pptx" target="_blank" title="East and Winter Park Campus Plan">East and Winter Park Campus Plan</a></p>
                        
                        <p><a href="documents/OsceolaLakeNonaCampusPlanBigMeeting.pdf" target="_blank" title="Osceola and Lake Nona Campus Plan">Osceola and Lake Nona Campus Plan</a></p>
                        
                        <p><a href="documents/West_Campus_Plan.pptx" target="_blank" title="West Campus Plan">West Campus Plan</a></p>
                        
                        <p><a href="documents/StratPlanBOOKMARKED_000.pdf" target="_blank" title="Valencia's Strategic Plan">Valencia's Strategic Plan</a></p>
                        
                        <p><a href="documents/ValenciaAdvisingSurveyResults.pdf">Valencia's Advising Survey Results </a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2>Resources</h2>
                        
                        <h3>Completion Agenda Websites</h3>
                        
                        <ul>
                           
                           <li><a href="http://Completionmatters.org" target="_blank" title="Completionmatters.org&amp;quot;&amp;quot;">Completionmatters.org</a></li>
                           
                           <li><a href="http://Collegecompletion.chronicle.com" target="_blank" title="Collegecompletion.chronicle.com">Collegecompletion.chronicle.com</a></li>
                           
                           <li><a href="http://Nces.ed.gov/ipeds" target="_blank" title="Nces.ed.gov/ipeds">Nces.ed.gov/ipeds</a></li>
                           
                           <li><a href="http://Completecollege.org" target="_blank" title="Completecollege.org">Completecollege.org</a></li>
                           
                           <li><a href="http://cccompletionchallenge.org" target="_blank" title="cccompletionchallenge.org">cccompletionchallenge.org</a></li>
                           
                        </ul>
                        
                        <h3><a href="videos.html">Completion Agenda Videos </a></h3>
                        
                        
                        
                        <h3>Completion Agenda Data</h3>
                        
                        <p>Schneider, M., Yin,  L.M. (2012) Completion Matters: The High Cost of Low Community
                           College Graduation Rates.  American Enterprise Institute for Public Policy Research,
                           2. www.aei.org.
                        </p>
                        
                        <ul>
                           
                           <li>30% of higher education students are enrolled in community colleges</li>
                           
                           <li>The community college three-year graduation rate is 20%; 1 in 4 students in community
                              colleges will graduate. 
                           </li>
                           
                           <li>In 2009, over four hundred community colleges had graduation rates less than 15%</li>
                           
                           <li>In 2009, over 320,000 full-time degree seeking students who entered community colleges
                              in 2006 had not earned their degree, and most were no longer enrolled in any postsecondary
                              institution.
                           </li>
                           
                           <li>According to the Bureau of Labor Statistics, the nation has an unmet need of around
                              300,000 new employees with associate's degrees per year.
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>Possible solutions</h3>
                        
                        <ul>
                           
                           <li>Streamline remediation programs</li>
                           
                           <li>Restructure traditional college programs</li>
                           
                           <li>Provide more online delivery options for courses</li>
                           
                           <li>Establish competency-based models of education - students progress through courses
                              at their own pace as they master learning outcomes
                           </li>
                           
                        </ul>
                        
                        
                        <p>Time is the Enemy: The surprising truth about why today's college students aren't
                           graduating ... and what needs to change.  Complete college America.  Sept. 27, 2011.
                           <a href="http://www.completecollege.org/docs/CCA_national_EMBARGO.pdf" title="CCA National EMBARGO">CCA National Embargo </a></p>
                        
                        
                        <p>Selingo, J. (2012) The Rise and Fall of the Graduation Rate. <a href="http://chronicle.com/article/The-RiseFall-of-the/131036/" title="The Chronicle of Higher Education.">The Chronicle of Higher Education.</a></p>
                        
                        
                        <p>"About one-third of students now transfer from the college where they started, according
                           to a recent report from the National Student Clearinghouse Research Center."
                        </p>
                        
                        
                        <p>College Completion: Who graduates from college, who doesn't, and why it matters. <a href="http://collegecompletion.chronicle.com/" target="_blank" title="The Chronicle of Higher Education">The Chronicle of Higher Education.</a></p>
                        
                        
                        <ul>
                           
                           <li>4.3 Million freshman started college in 2004
                              
                              <ul>
                                 
                                 <li>2.1 million didn't officially graduate - includes dropouts</li>
                                 
                                 <li>1.2 million aren't counted. "It's impossible to know whether many freshmen graduated
                                    or not, because the U.S. government simply doesn't track them. Part-time students
                                    are among those not counted."
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>Reference Documents</h3>
                        
                        <ul>
                           
                           <li><a href="documents/1_Measuring_Institutional_Conditions.pdf" target="_blank" title="Measuring Institutional Conditions">Measuring Institutional Conditions</a></li>
                           
                           <li><a href="documents/2_Raising_the_Bar_from_Ready.pdf" target="_blank" title="Raising the Bar from Ready">Raising the Bar from Ready</a></li>
                           
                           <li><a href="documents/3_Closing_the_Door.pdf" target="_blank" title="Closing the Door">Closing the Door</a></li>
                           
                           <li><a href="documents/4_Credential_Differential.pdf" target="_blank" title="Credential Differential">Credential Differential</a></li>
                           
                           <li><a href="documents/5_Completion_Matters.pdf" target="_blank" title="Completion Matters">Completion Matters</a></li>
                           
                           <li><a href="documents/6_Its_a_Matter_of_Time.pdf" target="_blank" title="It's a Matter of Time">It's a Matter of Time</a></li>
                           
                           <li><a href="documents/7_Reclaiming_the_American_Dream.pdf" target="_blank" title="Reclaiming the American Dream">Reclaiming the American Dream</a></li>
                           
                           <li><a href="documents/8_Valuable_Learning.pdf" target="_blank" title="Valuable Learning">Valuable Learning</a></li>
                           
                           <li><a href="documents/9_The_College_Keys_Compact.pdf" target="_blank" title="The College Keys Compact">The College Keys Compact</a></li>
                           
                           <li><a href="documents/10_Ensuring_Americas_Future.pdf" target="_blank" title="Ensuring America's Future">Ensuring America's Future</a></li>
                           
                           <li><a href="documents/11_Student_Voices.pdf" target="_blank" title="Student Voices">Student Voices</a></li>
                           
                           <li><a href="documents/Innovation_in_Higher_Education_-_A_Kirschner._Kirschner.pdf" target="_blank" title="Innovation in Higher Education">Innovation in Higher Education</a></li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/our-next-big-idea/history-resources.pcf">©</a>
      </div>
   </body>
</html>