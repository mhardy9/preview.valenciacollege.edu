<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Our Next Big Idea | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/our-next-big-idea/qep.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/our-next-big-idea/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Our Next Big Idea</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/our-next-big-idea/">Our Next Big Idea</a></li>
               <li>Our Next Big Idea</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Quality Enhancement Plan (QEP)</h2>
                        
                        <p>"An important part of the sustained efforts toward improving student learning at Valencia
                           College has been the development of several key ideas that serve as fulcrums for change,
                           signifiers for emerging organizational culture, and rallying points for action.  The
                           process of moving from promising innovation to large-scale pilot, to sustained solution,
                           that is, the process of institutionalizing the work, depends heavily on a community
                           of practice shaped by powerful common ideas.  These ideas are not unique to Valencia,
                           but they are authentically ours in the sense that they are organic to our work, and
                           have rooted themselves in the discourse of campus conversations, planning, development,
                           and day to day activities."<br><br>
                           <span>Valencia's Big Ideas: Sustaining Authentic Organizational Change through Shared Purpose
                              and Culture. By Sanford C. Shugart, Julie Phelps, Ann Puyana, Joyce Romano, and Kaye
                              Walter.</span></p>
                        
                        
                        <h3>Our Big Ideas currently include:</h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Anyone can learn anything under the right conditions</strong><br>
                              We believe that our students have all the biological gifts, the inherent capabilities
                              to learn anything we teach.  This belief shifts the focus from the deficiencies of
                              the learner to the condition we create for learning.
                           </li>
                           
                           <li>
                              <strong>Start Right</strong><br>
                              We believe that the greatest challenge and opportunity for improvement in student
                              success and graduation at Valencia is at the beginning of every new experience a student
                              has with us.  Our rallying cry is "make the first minute of the first meeting, in
                              every course a learning minute."
                           </li>
                           
                           <li>
                              <strong>Connection and Direction</strong><br>
                              We believe:<br>
                              --- a Student must make a personal connection very early in her experience at the
                              college with staff, with faculty and with other students.<br>
                              --- a student needs a clear direction and a plan to graduate, as soon as possible
                              in his college career.
                           </li>
                           
                           <li>
                              <strong>The College is how the students experience us, not how we experience them</strong><br>
                              We believe that our students are unique individuals and they experience college in
                              powerfully personal ways.  Our measures of our success are what students' experience.
                           </li>
                           
                           <li>
                              <strong>The purpose of assessment is to improve learning</strong><br>
                              We believe that the most important beneficiaries of authentic assessment are the learners
                              and those who facilitate learning.   Establishing clear learning expectations and
                              identifying the methods of assessments are essential steps to creating partnerships
                              between these groups that can improve learning.
                           </li>
                           
                           <li>
                              <strong>Collaboration</strong><br>
                              We believe that our best ideas are formed and embraced when everyone collectively
                              contributes to a shared purpose through an ongoing dialogue.  All our Big Ideas depend
                              on authentic collaboration for their legitimacy.
                           </li>
                           
                           
                        </ul>
                        
                        <p>Over the past ten years Valencia has been continually engaged in national, grant-related
                           projects focused on improving student learning / success. These projects have included
                           Achieving the Dream, Foundations of Excellence, and the Developmental Education Initiative.
                           The impetus for our nextcollege-wide project focused on student learning / successis
                           not coming from a national grant, but fromour regional accrediting body - the Southern
                           Association of Colleges and Schools (SACS) – which is required to reaffirm the college’s
                           accreditation every ten years. Our next reaffirmation is in 2014. The reaffirmation
                           process includes the development of a Quality Enhancement Plan (QEP) - a broad based,
                           college-wide change initiative focused on student learning or the conditions that
                           impact student learning. Valencia’s QEP needs to be articulated by August 2013. TheQEP
                           is the centerpiece of the reaffirmation process and will be implemented over the course
                           of 5 years beginning in the Fall of 2013 or Spring of 2014.
                        </p>
                        
                        <p>Beginning in the Spring of 2012, faculty, staff, and students entered into an exploration
                           phase working toward identifying Valencia’s next <a href="about.html">Big Idea</a>. A series of reading circle discussions, meetings, and brainstorming sessions led
                           to four emerging themes – Student Purpose, Student Learning, Student Navigation, and
                           the New Student Experience. During the Fall of 2012, a total of 14 groups (approximately
                           280 faculty and staff) across the college met to further explore and unpack each theme
                           as it related to Valencia and its students. The recurring proposition within the BIGs
                           was that Valencia students need a coordinated first year experience to guide them
                           toward the successful completion of their first 15 college-level credits at Valencia.
                           
                        </p>
                        
                        <p>The result of the BIGs and a post-SACS conference discussion among the college’s senior
                           team, faculty leadership, and QEP leadership led to the creation of a vision statement
                           for a proposed New Student Experience (NSE); in order for a new Valencia student to
                           navigate the college experience successfully, it requires access to mandatory / universal
                           career and academic advising, an extended orientation, integrated student success
                           skills, and the development of an educational plan. During the Spring of 2013, the
                           QEP Leadership team, along with the Press Grant team, will work to establish consensus
                           on what the New Student Experience will look like and refine the emerging idea through
                           a series of meetings at each campus. Finally, faculty and staff are invited to a QEP
                           Summit to be held on March 22. The summit will be an open, collaborative, decision
                           making forum in which the QEP project is identified. The QEP will be tied to work
                           such as the WalMart Press Grant, Pathways Title III Grant, and the Atlas portal redesign,
                           which is currently happening across the college. 
                        </p>              
                        
                        <p>For information and summaries of the reading circles, Big Idea Groups (BIGS), and
                           information sessions<a href="http://valenciacc.edu/ournextbigidea/timeline.cfm"> click here </a></p>
                        
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/our-next-big-idea/qep.pcf">©</a>
      </div>
   </body>
</html>