<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/our-next-big-idea/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/our-next-big-idea/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Our Next Big Idea</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       
                                       
                                       <div> 
                                          
                                          <div>
                                             <img alt="assessment-learning" border="0" height="314" src="assessment-learning-w.jpg" width="930">
                                             
                                             <div>
                                                
                                                <div>We believe that the most important beneficiaries of authentic assessment are the learners
                                                   and those who facilitate learning. Establishing clear learning expectations and identifying
                                                   the methods of assessments are essential steps to creating partnerships between these
                                                   groups that can improve learning.
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="collaboration" border="0" height="314" src="collaboration-w.jpg" width="930">
                                             
                                             <div>
                                                
                                                <div>We believe that our best ideas are formed and embraced when everyone collectively
                                                   contributes to a shared purpose through an ongoing dialogue.  All our Big Ideas depend
                                                   on authentic collaboration for their legitimacy.
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="college-experience" border="0" height="314" src="college-experience-w.jpg" width="930">
                                             
                                             <div>
                                                
                                                <div>We believe that our students are unique individuals and they experience college in
                                                   powerfully personal ways. Our measures of our success are what students' experience.
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="connection-direction" border="0" height="314" src="connection-direction-w.jpg" width="930">
                                             
                                             <div>
                                                
                                                <div>We believe that the greatest challenge and opportunity for improvement in student
                                                   success and graduation at Valencia is at the beginning of every new experience a student
                                                   has with us. Our rallying cry is "make the first minute of the first meeting, in every
                                                   course a learning minute."
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="learn-anything" border="0" height="314" src="learn-anything-w.jpg" width="930">
                                             
                                             <div>
                                                
                                                <div>We believe that our students have all the biological gifts, the inherent capabilities
                                                   to learn anything we teach.  This belief shifts the focus from the deficiencies of
                                                   the learner to the condition we create for learning.
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="qep-logo-reveal" border="0" height="314" src="qep-logo-reveal-w.jpg" width="930">
                                             
                                             <div>
                                                
                                                <div>We believe that an authentic Quality Enhancement Plan starts by focusing on the Big
                                                   Ideas that inspire our work.
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="start-right" border="0" height="314" src="start-right-w.jpg" width="930">
                                             
                                             <div>
                                                
                                                <div>We believe that the greatest challenge and opportunity for improvement in student
                                                   success and graduation at Valencia is at the beginning of every new experience a student
                                                   has with us. Our rallying cry is "make the first minute of the first meeting, in every
                                                   course a learning minute."
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <p><a href="about.html" title="About QEP">What is  a QEP?</a></p>
                                       
                                       <p><a href="valencias-qep.html" title="Get Involved">Valencia's QEP</a></p>
                                       
                                       <p><a href="timeline.html" title="QEP Timeline">QEP Timeline</a></p>
                                       
                                       <p><a href="history-resources.html" title="History and Resources">History and Resources</a></p>
                                       
                                       <p><a href="qep.html" title="Our Next Big Idea">Our Next Big Idea</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h2>QEP Updates</h2>
                                       
                                       
                                       <none>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeBigIdeas/~3/hc6zs8kkbT8/" target="_blank">Valencia’s Big Ideas a Common Theme in The Community College Conference on Learning
                                             Assessment</a> <br>
                                          
                                          
                                          
                                          
                                          
                                          <span>Mr. Shelby England</span><br>
                                          
                                          <span>Feb 14, 2017</span><br>
                                          
                                          
                                          <p> On Sunday, February 5, 2017, many of our colleagues kicked off their day at <a href="https://conferences.valenciacollege.edu/learning-assessment/" target="_blank">Valencia’s Community College Conference on Learning Assessment</a> at the Rosen Plaza Hotel in Orlando. The day began with pre-conference workshops
                                             in the morning on a variety of assessment topics before more than 300 attendees gathered
                                             ...
                                          </p>
                                          <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeBigIdeas/~3/hc6zs8kkbT8/" target="_blank" title="Read More"><span>More</span> <img alt=" " border="0" height="10" src="smore.png" width="10"></a>
                                          
                                          
                                       </none>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h2>QEP Video Library</h2>
                                       
                                       <iframe allowfullscreen="" frameborder="0" height="184" src="http://www.youtube.com/embed/8uBlx9ZUJg0?rel=0" width="245"></iframe>
                                       
                                       
                                       <p><a href="videos.html" title="QUP Videos">View More Videos</a></p>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>          
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/our-next-big-idea/index.pcf">©</a>
      </div>
   </body>
</html>