<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Our Next Big Idea | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/our-next-big-idea/valencias-qep.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/our-next-big-idea/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Our Next Big Idea</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/our-next-big-idea/">Our Next Big Idea</a></li>
               <li>Our Next Big Idea</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Valencia's QEP</h2>
                        
                        <p>Beginning in November 2011, Valencia College faculty and staff members were involved
                           in a deliberate collaborative process to discuss and design the Quality Enhancement
                           Plan (QEP) that is part of the decennial review of accreditation with the Southern
                           Association of Colleges and Schools (SACS). Valencia’s QEP effort is a reflection
                           of the intersection between our past initiatives, work completed by QEP design teams,
                           new requirements regarding developmental education from the Florida legislature, and
                           the practical demands of committing to a Quality Enhancement Plan that is both meaningful
                           and manageable.
                        </p>
                        
                        
                        <p>The design process was formally presented and adopted by multiple levels of College
                           governance and consisted of four phases: Education and Planning, Narrowing the College's
                           Strategic Focus, Building Consensus, and QEP Design and Articulation. The process
                           invited and engaged substantial numbers of students, faculty and staff. 
                        </p>
                        
                        
                        <p>Valencia College’s QEP is to create a New Student Experience that will provide a coordinated
                           experience for all new students with fewer than 15 college-level credits at Valencia.
                           The plan has curricular and co-curricular components that will be implemented and
                           enhanced over the next five years, but with the majority of the scale of proposed
                           initiatives achieved within the first three years. The New Student Experience will
                           include a required credit-earning course and an extended orientation to college; student
                           success skills integrated into select program introduction courses; front door general
                           education alignment, and career and academic advising to include the development of
                           an individualized education plan. We envision the new student experience will result
                           in enhanced curricular and co-curricular student engagement, leading to the successful
                           completion of the first 15 college-level credits at Valencia. Wherever possible, the
                           curricular aspects of the new student experience will be offered in partnership with
                           faculty in academic and career programs.
                        </p>            
                        <div>
                           <a href="documents/Valencia-College-Quality-Enhancement-Plan.pdf"> Valencia College Quality Enhancement-Plan </a>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/our-next-big-idea/valencias-qep.pcf">©</a>
      </div>
   </body>
</html>