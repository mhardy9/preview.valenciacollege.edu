var MAX_QUESTIONS = 12; // it's zero-indexed
var currentQuestion = 0;
var result = 0;

// array to contain punctuation for each question: 1 or 0 (to be sum up at the end)
var values = new Array();
for (var i = 0; i <= MAX_QUESTIONS; i++) {
    values[i] = 0;
}

//array to contain questions' text
var questions = [
    "If a student's parent calls asking how a student is doing in a class, can you give out that information?",
    "You receive a call from a recruiting firm asking for names and addresses of students with a GPA of 3.0 or better. They say they have good job information for these students. Can you help these students get jobs by giving out this information?",
    "A person goes to the Admissions and Records District Office with a letter containing a signature that gives consent to the release the transcript of a student. Do you give the transcript to them?",
    "You receive a phone call from the local police department indicating that they are trying to determine whether a particular student was in attendance on a specific day. Since they are in the middle of an investigation are you allowed to give them this information?",
    "You receive a frantic phone call from an individual who says he is a student's father and must get in touch with her immediately because of a family emergency. Can you tell him when and where her next class is today?",
    "Is it wrong for professors to leave exams, papers, etc. outside their offices or in another public place for students to pick up?",
    "An unauthorized person retrieves information from a computer screen that was left unattended. Under FERPA, is the institution responsible?",
    "You work with student organizations and keep on file, in your office, a current copy of a financial statement for each organization. A reporter from the Valencia Source calls you and asks you for a copy of the most recent financial statement of an organization that is rumored to have misspent its approved funding. Does FERPA prohibit the disclosure of this information?",
    "Does FERPA prohibit the disclosure of the work address and telephone number of an alumnus by the Office of Alumni Relations?",
    "Valencia's West campus suffers significant damage from vandals. As Chief of Safety and Security, you investigate the incident and obtain several students' confessions that they committed the acts of vandalism. You share this information with Orlando Police Department and the Dean of Students. The Dean of Students intends to use the information to initiate disciplinary action against the students. Do the students involved have the right to inspect and review the records you have made of their confessions?",
    "You work in Admissions at Valencia. A new student who was admitted under special conditions requests the opportunity to review her admissions file. She insists on reviewing these materials no later than the close of business the following day. Does FERPA require you to respond to this request by allowing her to review the records when she wants to?",
    "A student's father comes to Valencia's Admissions and Records District Office and presents a piece of paper signed by the student that states: \"I consent to the disclosure of my education records to my father.\" The paper is signed and dated. The father proves to you that he is the father of the student in question. Does this constitute sufficient written consent under FERPA?",
    "While working at the college, you learn that a Valencia student is under criminal investigation for selling drugs. The Orange County district attorney's office delivers a grand jury subpoena that requests copies of the student's disciplinary records. May Valencia comply with the request without first obtaining the student's consent?"
];

//array to contain values for correct answers
var answers = [
    "No",
    "No",
    "No",
    "No",
    "No",
    "Yes",
    "Yes",
    "No",
    "No",
    "Yes",
    "No",
    "No",
    "Yes"
];

//array to contain explanation to questions
var explanations = [
    "Even though the person inquiring may be the student's parent, FERPA recognizes students in post-secondary education as adults, regardless of age. Therefore, you cannot give out that grade, or any other non-directory information.\n\nGeneral Rule:\nYou must assume that the student is an adult who is entitled to privacy, even from parents. Under state law, parents may assert their rights to the records if the student is a dependent according to the tax code, and the college has their tax records on file in the Financial Aid Services office.",
    "Although we all want to help students to get good jobs, that request should be sent to the appropriate office.\n\nGeneral Rule:\nDo not give out student information that pertains to grade point average to anyone without prior written consent of that student. In this case the request should be forwarded to the Admissions and Records District Office. ALL outside requests for any information such as Dean's Lists must be referred to the Admissions and Records District Office. Information about the recruiting firm could be provided to students in the appropriate major, and to the campus Career Center.",
    "Transcripts and record information are available only through the Admissions and Records District Office. Transcripts are mailed to third parties upon the written request of the student, but may not be handed to a third party.\n\nGeneral Rule:\nDo not give any records to a third party.",
    "The police should first be directed to the Safety and Security Office. The Safety and Security Office will coordinate the college's response, as appropriate. \n\nGeneral Rule:\nInformation about whether or not a student was enrolled in a particular term is directory information and can be obtained through the Office of the Director of Admissions and Records. If the police require more information, a subpoena may be required. Additionally, FERPA requires prior notification of the student, unless it is specifically stated on the subpoena that the student must not be notified.",
    "For the safety of the student you cannot tell another person where a student is at any time. Inform the caller they should contact the Admissions and Records District Office for more information.",
    "That is a violation of the privacy rule because it is inappropriate for students to have access to other students' information.\n\nGeneral Rule:\nYou should not leave personally identifiable materials in a public place.",
    "Information on a computer screen should be treated the same as printed reports.\n\nGeneral Rule:\nThe medium in which the information is held is unimportant. No information should be left accessible or unattended, including computer displays, forms and handwritten notes.",
    "FERPA only pertains to information that is directly related to a student. The finances of a student organization do not directly relate to a student. While other considerations might restrict sharing this information with the Source, FERPA does not. FERPA would prohibit sharing this information if the financial statement became a document used in an investigation by Valencia into whether the organization violated college purchasing policy.",
    "FERPA does not protect information about a student that is gathered after the student graduates. Had the request been for the alum's GPA, FERPA would apply.",
    "Once the information is shared with the Dean of Students, it becomes protected by FERPA and is subject to the right of inspection and review. Had it been shared only with Orlando Police, students would not have a right to inspect because FERPA would not apply.",
    "Valencia must grant the request to review within a reasonable time but in no case more than 45 days after the request is received. It is likely not reasonable to have to respond to a request within 24 hours.",
    "This consent does not specify the records to be disclosed, the identity of the person to whom they are disclosed, or the purpose of the disclosure. Specific information concerning the records, the name of the person to whom the disclosure is made and the purpose of the disclosure must be presented in writing.",
    "Prior written consent to disclosure is not required where a subpoena has been issued. However, Valencia's procedure is to refer the matter to the Director of Admissions and Records."
];

var correctMsg = "Correct! You can read the explanation and then click \"Next\".";
var incorrectMsg = "Incorrect! Please, read the explanation below the question and then click \"Next\".";

function evaluateAnswer(buttonClicked) {
    // if (document.getElementById('buttonYes').value == answers[currentQuestion]) {
    if (buttonClicked == answers[currentQuestion]) {
        values[currentQuestion] = 1;
        document.getElementById('alertMessages').textContent = correctMsg;
    } else {
        values[currentQuestion] = 0;
        document.getElementById('alertMessages').textContent = incorrectMsg;
    }
    document.getElementById('buttonNext').style.visibility = "visible";
    document.getElementById('buttonYES').style.visibility  = "hidden";
    document.getElementById('buttonNO').style.visibility = "hidden";
    document.getElementById('textForAnswer').textContent = explanations[currentQuestion];
}

function goToNext() {
    if (currentQuestion < MAX_QUESTIONS) {
        currentQuestion++;
        document.getElementById('questionNumber').textContent = "Question " + (currentQuestion + 1);
        document.getElementById('textForQuestion').textContent = questions[currentQuestion];
        document.getElementById('textForAnswer').textContent = "";
        document.getElementById('buttonYES').style.visibility  = "visible";
        document.getElementById('buttonNO').style.visibility = "visible";
        document.getElementById('buttonNext').style.visibility  = "hidden";
        document.getElementById('alertMessages').textContent = "";
    } else {
        if ((currentQuestion == MAX_QUESTIONS) && (confirm("Are you sure you want to submit your answers?"))) {
            currentQuestion++;
            results();
            document.getElementById('buttonYES').style.visibility  = "hidden";
            document.getElementById('buttonNO').style.visibility = "hidden";
            document.getElementById('buttonNext').style.visibility  = "hidden";
            document.getElementById('alertMessages').textContent = "";
        }
    }
}

function goToPrevious() {
    if (confirm("Press OK if you want to start over, press Cancel to continue.")) {
        restart();
    }
}

function results() {
    for (var i = 0; i <= MAX_QUESTIONS; i++) {
        result = result + values[i];
    }
    document.getElementById('questionNumber').textContent = "Your results...";
    document.getElementById('textForQuestion').textContent = "" + result + " question(s) correct out of " + (MAX_QUESTIONS + 1) + checkPassingScore();
    document.getElementById('textForAnswer').textContent = "";
}

function checkPassingScore() {
    if (result > 9) {
        return ("\n\nCongratulations! You've passed!");
    } else {
        return ("\n\nSorry, you didn't pass, you may want to review your FERPA information again!");
    }
}

function restart() {
    currentQuestion = 0;
    result = 0;
    for (var i = 0; i <= MAX_QUESTIONS; i++) {
        values[i] = 0;
    }
    document.getElementById('questionNumber').textContent = "Question " + (currentQuestion + 1);
    document.getElementById('textForQuestion').textContent = questions[currentQuestion];
    document.getElementById('textForAnswer').textContent = "";
    document.getElementById('buttonYES').style.visibility  = "visible";
    document.getElementById('buttonNO').style.visibility = "visible";
    document.getElementById('buttonNext').style.visibility  = "hidden";
    document.getElementById('alertMessages').textContent = "";
}