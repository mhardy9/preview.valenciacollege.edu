<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>FERPA  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/ferpa/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/ferpa/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>FERPA</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Ferpa</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        <h2>FERPA</h2>
                        
                        
                        <h3>Quick Links</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="#student">Student Privacy </a></li>
                           
                           <li><a href="#whatis">What is FERPA?</a></li>
                           
                           <li>
                              <a href="#faqs">FERPA FAQs</a>
                              
                              <ul class="list-style-1">
                                 
                                 <li><a href="#Records">What are Education Records? </a></li>
                                 
                                 <li><a href="#Right">What is the Right to Inspect and Review?</a></li>
                                 
                                 <li><a href="#Prior">What is Prior Written Consent? </a></li>
                                 
                                 <li><a href="#Not">When is Consent Not Required?</a></li>
                                 
                                 <li><a href="#Directory">What is Directory Information for Valencia College?</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><a href="#donts">Special "DON'TS" For Faculty and Staff </a></li>
                           
                           <li><a href="quiz.html">FERPA Quiz</a></li>
                           
                        </ul>
                        
                        
                        <h3>
                           <a name="student" id="student"></a>Student Privacy 
                        </h3>
                        
                        <p>Valencia College has a firm commitment to protecting the privacy rights of its students.
                           In making this commitment, the College wants to ensure that all faculty and staff
                           are familiar with state and federal laws pertaining to student privacy, as well as
                           College policies and procedures that have been implemented to help guarantee student
                           privacy. 
                        </p>
                        
                        <p>All faculty and staff  are asked to review this Web site, and take the short FERPA
                           Quiz.
                        </p>
                        
                        
                        <h4>Notification of Rights under FERPA </h4>
                        
                        <p>The Family Educational Rights and Privacy Act (FERPA) afford students certain rights
                           with respect to their education records. These rights include: 
                        </p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>The right to inspect and review the student’s education records within 45 days of
                                 the day the College receives a request for access. A student should submit to the
                                 registrar, dean, head of the academic program, or other appropriate official, a written
                                 request that identifies the record(s) the student wishes to inspect. The College official
                                 will make arrangements for access and notify the student of the time and place where
                                 the records may be inspected. If the records are not maintained by the College official
                                 to whom the request should be addressed.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The right to request the amendment of the student’s education records that the student
                                 believes is inaccurate, misleading, or otherwise in violation of the student’s privacy
                                 rights under FERPA. A student who wishes to ask the College to amend a record should
                                 write the College official responsible for the record, clearly identify the part of
                                 the record the student wants changed, and specify why it should be changed.
                              </p>
                              
                              <p>If the college decides not to amend the record as requested, the College will notify
                                 the student in writing of the decision and the student’s right to a hearing regarding
                                 the request for amendment. Additional information regarding the hearing procedures
                                 will be provided to the student when notified of the right to a hearing.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The right to provide written consent before the College discloses personally identifiable
                                 information from the student’s education records, except to the extent that FERPA
                                 authorizes disclosure without consent.
                              </p>
                              
                              <p>The College discloses education records without a student’s prior written consent
                                 under the FERPA exception for disclosure to school officials with legitimate educational
                                 interests. A school official is a person employed by the College or its Foundation
                                 in an administrative, supervisory, academic or research, or support staff position;
                                 a person or company with whom the College has contracted as its agent to provide a
                                 service instead of using College employees or officials (such as an attorney, auditor,
                                 or collection agent); a person serving on the District Board of Trustees or the Foundation
                                 Board of Directors or a student or other person serving on an official committee,
                                 including without limitations disciplinary, grievance, or scholarship committee, or
                                 assisting another school official in performing his or her tasks.
                              </p>
                              
                              <p>A school official has a legitimate educational interest if the official needs to review
                                 an education record in order to fulfill his or her professional or official responsibilities
                                 for the College. Upon request, the College also discloses education records without
                                 consent to officials of another school in which a student seeks or intends to enroll
                                 or is already enrolled, when the disclosure is for purposes related to the student’s
                                 enrollment or transfer.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The right to file a complaint with the U.S. Department of Education concerning alleged
                                 failures by the College to comply with the requirements of FERPA. The name and address
                                 of the Office that administers FERPA is:
                              </p>
                              
                              <address>Family Policy Compliance Office
                                 <br>U.S. Department of Education
                                 <br>400 Maryland Avenue, SW
                                 <br>Washington, DC 20202-5901
                                 
                              </address>
                              
                           </li>
                           
                        </ol>
                        
                        
                        <h4>Directory Information Public Notice</h4>
                        
                        <p>(a) The College may disclose directory information upon request without consent. The
                           College has identified the following as directory information:
                        </p>
                        
                        <ol>
                           
                           <li>Student’s name</li>
                           
                           <li>Major field of study</li>
                           
                           <li>Dates of attendance</li>
                           
                           <li>Degrees, honors and awards received</li>
                           
                        </ol>
                        
                        <p>An eligible parent or eligible student has the right to refuse to let the College
                           designate any or all of those types of information about the student as directory
                           information. If you refuse to permit the College to release any or all of those types
                           of directory information, you must inform the Admissions/Records Office in writing
                           within forty five (45) days of the first day of classes of each session. Your decision
                           to refuse the release of any or all of those types of directory information also means
                           that your name will not appear on recognition lists or in news releases, etc. Thereafter,
                           you must provide written consent for the release of information to second parties.
                           The confidential hold will remain on your record until you submit your written consent
                           to release the hold. 
                        </p>
                        
                        
                        <h3>
                           <a name="whatis" id="whatis"></a>What is FERPA?
                        </h3>
                        
                        <p> FERPA stands for Family Educational Rights and Privacy Act (sometimes called the
                           Buckley Amendment). Passed by Congress in 1974, the Act grants four specific rights
                           to college students. <a href="http://www.flsenate.gov/Statutes/index.cfm?App_mode=Display_Statute&amp;Search_String=&amp;URL=Ch1002/SEC22.HTM&amp;Title=-%3E2002-%3ECh1002-%3ESection%2022" target="_blank"> Section 1002.22</a>, Florida Statute, substantially enacts provisions of FERPA as a matter of state law,
                           with minor variations. <a href="../generalcounsel/policy/documents/Volume7B/7B-02-Student-Records.pdf" target="_blank">Valencia Policy</a> contains policy and procedures related to implementation of these laws:
                        </p>
                        
                        <ul>
                           
                           <li>The right to see the information that the institution is keeping on the student</li>
                           
                           <li>The right to seek amendment to those records and in certain cases append a statement
                              to the record
                           </li>
                           
                           <li>The right to consent to disclosure of his/her records</li>
                           
                           <li>The right to file a complaint with the FERPA Office in Washington
                              <br> Information is also available at <a href="http://www.ed.gov/offices/OII/fpco/ferpa/" target="_blank"> http://www.ed.gov/offices/OII/fpco/ferpa/</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h3>
                           <a name="faqs" id="faqs"></a>FERPA FAQs
                        </h3>
                        
                        <h4>What are Education <a name="Records" id="Records"></a>Records? 
                        </h4>
                        
                        <p>Information recorded in any form that is directly related to a student and maintained
                           by the college and by those acting for the college. 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>personal information </li>
                           
                           <li>enrollment records </li>
                           
                           <li>grades </li>
                           
                           <li>schedules </li>
                           
                        </ul>
                        
                        <p>The storage medium in which you find this information does not matter. A student educational
                           record may be:
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>a document in the Admissions and Records District office </li>
                           
                           <li>a computer printout in your office </li>
                           
                           <li>a class list on your desktop </li>
                           
                           <li>a computer display screen </li>
                           
                        </ul>
                        
                        
                        <p>Education records <strong>do not</strong> include: 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>Records of instructional, supervisory and administrative personnel kept in the sole
                              possession of the maker of the record and not revealed to anyone other than the maker's
                              substitute;
                           </li>
                           
                           <li>Records of a campus law enforcement unit created and maintained by that unit and used
                              solely for law enforcement purposes; 
                           </li>
                           
                           <li>Employment records, maintained apart from educational records, relating to persons
                              who are employees; 
                           </li>
                           
                           <li>Records kept and maintained by a health care professional, used solely in connection
                              with treatment and disclosed only to individuals providing treatment; or 
                           </li>
                           
                           <li>Records which include information about an individual after he or she is no longer
                              a student. 
                           </li>
                           
                        </ul>
                        
                        
                        <h4>What is the <a name="Right" id="Right"></a>Right to Inspect and Review? 
                        </h4>
                        
                        <p>Valencia must grant requests to review within a reasonable time but in no case more
                           than 45 days after the request is received. Valencia must respond to reasonable requests
                           for explanations and interpretations of the records.
                        </p>
                        
                        
                        <h4>What is <a name="Prior" id="Prior"></a>Prior Written Consent? 
                        </h4>
                        
                        <p>A signed and dated document that includes specification of the records to be disclosed,
                           the purpose of the disclosure and the identity of the person to whom records will
                           be disclosed. A written consent form can be accessed by<a href="../pdf/stcnsnt.pdf.html" target="_blank"> clicking here</a> (pdf).
                        </p>
                        
                        
                        <h4>When is Consent <a name="Not" id="Not"></a>Not Required? (This is not an exhaustive list of exceptions)
                        </h4>
                        
                        <ul class="list-style-1">
                           
                           <li>For legitimate educational purposes within the college. </li>
                           
                           <li>To officials at an institution in which student seeks to enroll. </li>
                           
                           <li>To comply with a court order or subpoena. </li>
                           
                           <li>In connection with a health or safety emergency if necessary to protect the student
                              or others. 
                           </li>
                           
                           <li>To parents of students who are dependents for income tax purposes. </li>
                           
                           <li>If it is directory information. </li>
                           
                           <li>To parents of a student younger than 21 years of age if the disclosure concerns discipline
                              for violation of the campus drug and alcohol policy. 
                           </li>
                           
                        </ul>
                        
                        
                        <h4>What is <a name="Directory" id="Directory"></a>Directory Information for Valencia Community College?
                        </h4>
                        
                        <ul class="list-style-1">
                           
                           <li>Student's Name</li>
                           
                           <li>Major Field of Study</li>
                           
                           <li>Dates of Attendance</li>
                           
                           <li>Degrees, honors and awards received</li>
                           
                        </ul>
                        
                        
                        <h3>
                           <a name="donts" id="donts"></a>Special "DON'TS" For Faculty and Support Staff
                        </h3>
                        
                        <p>To avoid violations of FERPA rules, <strong>do not:</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>at any time use the Social Security Number of a student in a public posting of grades</li>
                           
                           <li>ever link the name of a student with that student's social security number in any
                              public manner
                           </li>
                           
                           <li>leave graded tests in a stack for students to pick up by sorting through the papers
                              of all students
                           </li>
                           
                           <li>circulate a printed class list with student name and social security number or grades
                              as an attendance roster
                           </li>
                           
                           <li>discuss the progress of any student with anyone other than the student (including
                              parents) without the consent of the student
                           </li>
                           
                           <li>provide anyone with lists of students enrolled in your classes for any commercial
                              or other purpose
                           </li>
                           
                           <li>provide anyone with student schedules or assist anyone other than college employees
                              in finding a student on campus
                           </li>
                           
                        </ul>
                        
                        
                        <p><a href="quiz.php" name="quiz" id="quiz">Click here to take the FERPA Quiz</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/ferpa/index.pcf">©</a>
      </div>
   </body>
</html>