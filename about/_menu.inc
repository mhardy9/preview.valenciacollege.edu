
							<ul>
								<li> <a href="/academics/index.php"> Academics </a> </li>
								<li class="megamenu submenu"> <a class="show-submenu-mega" href="javascript:void(0);"> Admissions <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
									<div class="menu-wrapper">
										<div class="col-md-6">
											<ul>
												<li> <a href="/icon-pack-1.php"> Admission &amp; Records </a> </li>
												<li> <a href="/icon-pack-2.php"> Admission Details </a> </li>
												<li> <a href="/icon-pack-3.php"> Applications &amp; Forms </a> </li>
												<li> <a href="/icon-pack-4.php"> Change of Program/Major </a> </li>
												<li> <a href="/shortcodes.php"> Degree Verification </a> </li>
											</ul>
										</div>
										<div class="col-md-6">
											<ul>
												<li> <a href="/academic_single_course.php"> Enrollment Verification </a> </li>
												<li> <a href="/blog.php"> Florida Residency </a> </li>
												<li> <a href="/contacts.php"> Paying for Classes </a> </li>
												<li> <a href="/agenda_calendar.php"> Registration Details </a> </li>
												<li> <a href="/gallery.php"> Steps to Enroll </a> </li>
											</ul>
										</div>
									</div>

								</li>
								<li class="megamenu submenu"> <a class="show-submenu-mega" href="javascript:void(0);"> Programs &amp; Degrees <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
									<div class="menu-wrapper c3">
										<div class="col-md-4">
											<h3>
												Programs 
											</h3>
											<ul>
												<li> <a href="/icon-pack-1.php"> General Studies </a> </li>
												<li> <a href="/icon-pack-2.php"> Arts &amp; Entertainment </a> </li>
												<li> <a href="/icon-pack-3.php"> Business </a> </li>
												<li> <a href="/icon-pack-4.php"> Communications </a> </li>
												<li> <a href="/shortcodes.php"> Education </a> </li>
												<li> <a href="/shortcodes.php"> Engineering </a> </li>
												<li> <a href="/shortcodes.php"> Computer Programming </a> </li>
												<li> <a href="/shortcodes.php"> Health Sciences </a> </li>
											</ul>
										</div>
										<div class="col-md-4">
											<h3>
												Areas 
											</h3>
											<ul>
												<li> <a href="/academic_single_course.php"> Hospitality &amp; Culinary </a> </li>
												<li> <a href="/blog.php"> Information Technology </a> </li>
												<li> <a href="/contacts.php"> Landscape &amp; Horticulture </a> </li>
												<li> <a href="/agenda_calendar.php"> Public Safety &amp; Paralegal Studies </a> </li>
												<li> <a href="/gallery.php"> Science &amp; Mathematics </a> </li>
												<li> <a href="/shortcodes.php"> Social Sciences </a> </li>
											</ul>
										</div>
										<div class="col-md-4">
											<h3>
												Degrees 
											</h3>
											<ul>
												<li> <a href="/shortcodes.php"> Associate in Arts </a> </li>
												<li> <a href="/shortcodes.php"> Associate in Science </a> </li>
												<li> <a href="/shortcodes.php"> Bachelor of Science </a> </li>
												<li> <a href="/shortcodes.php"> Certificate Programs </a> </li>
												<li> <a href="/shortcodes.php"> Advanced Technical Certificates </a> </li>
											</ul>
										</div>
									</div>

								</li>
								<li> <a href="/ABOUT/index.php"> About </a> </li>
							</ul>

