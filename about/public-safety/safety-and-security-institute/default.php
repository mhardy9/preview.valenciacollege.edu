<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety and Security | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/safety-and-security-institute/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/safety-and-security-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety and Security</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/safety-and-security-institute/">Safety And Security Institute</a></li>
               <li>Safety and Security</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html">
                           </a>
                        
                        
                        
                        
                        
                        <p>Still in development, the Safety and Security Institute will build upon programming
                           already being offered by Valencia College and the School of Public Safety to provide
                           professional and leadership training for private security officers and managers, as
                           well as homeland security specialists.&nbsp;The Safety and Security Institute will be the
                           first of its kind to provide standardized training and certifications to the security
                           professionals working within Central Florida’s many airports, hotels, theme parks
                           and attractions.&nbsp; 
                        </p>
                        
                        <h2> Current Offerings WIthin the Fields of Safety and Security​</h2>
                        
                        <p> Valencia College and the School of Public Safety currently offer the following programs,
                           which&nbsp;may be beneficial to students&nbsp;interested in entering or advancing within&nbsp;the
                           fields of safety and security. 
                        </p>
                        
                        <h3>Associate in Science Degree</h3>
                        
                        <p>The two-year&nbsp;<a href="../criminal-justice-institute/index.html"><u>Criminal&nbsp;Justice&nbsp;A.S. degree program</u></a>&nbsp;offers a broad&nbsp;background in the history, philosophy, organization, management and
                           operation of the criminal justice system. It is a path to employment, a bachelor's
                           degree and career enhancement.&nbsp;As part of&nbsp;Valencia's “Two for All” initiative,&nbsp;state-certified
                           professionals may be able to&nbsp;receive&nbsp;credits toward the A.S. degree for professional
                           experience.
                        </p>
                        
                        <p>Related Certificates:</p>
                        
                        <p>Many certificates can be completed in less than a year and credits earned apply toward
                           the A.S. degree in Criminal Justice.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext"><u>Criminal Justice Technology Specialist</u></a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext"><u>Homeland Security Law Enforcement Specialist</u></a></li>
                           
                        </ul>
                        
                        
                        <h3>Pathways to&nbsp;Advanced Degrees</h3>
                        
                        <p><strong>Bachelor's Opportunities</strong></p>
                        
                        <p>Students who complete the&nbsp;Criminal Justice Technology A.S. degree may&nbsp;utilize the
                           Articulated A.S. to B.S. Career Path to transfer to any Florida public university
                           as a junior to complete a four-year Bachelor of Science degree in Criminal Justice.
                           And through the&nbsp;<a href="http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/"><u>DirectConnect to UCF program</u></a>, Valencia graduates will receive guaranteed* admission to the University of Central
                           Florida, which offers a Bachelor of Science degree in Criminal Justice on Valencia's
                           West Campus.&nbsp;(*Consistent with university policy.)
                        </p>
                        
                        <p><strong>Master's Opportunities</strong></p>
                        
                        <p>Professionals&nbsp;who participate in Valencia's&nbsp;<u><a href="../LDCP.html" target="_blank">Public Safety Leadership Development Certification Program</a></u> (PSLDCP)&nbsp;will&nbsp;receive credits toward&nbsp;the&nbsp;<u><a href="http://www.graduatecatalog.ucf.edu/programs/program.aspx?id=11555" target="_blank">Graduate Certificate in Criminal&nbsp;Justice Executive</a></u>&nbsp;from the University of Central Florida, which is offered exclusively to&nbsp;PSLDCP&nbsp;participants.
                        </p>
                        
                        <h3>Continuing Education</h3>
                        
                        <p><strong>Courses and Training</strong></p>
                        
                        <p>Valencia’s Continuing Education division and the School of Public Safety have partnered
                           to bring professional training, seminars and workshops to the public safety community.
                           We provide opportunities for security officers, animal control officers and others
                           working in the fields of safety and security.&nbsp;&nbsp;
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/criminal-justice/"><u>View Upcoming Courses</u></a></p>
                           
                        </blockquote>
                        
                        <p><strong>The Public Safety Leadership Development Certification Program</strong></p>
                        
                        <p>This four-week program&nbsp;focuses on developing public safety professionals into innovative
                           leaders. It is offered&nbsp;in&nbsp;partnership between Valencia College Continuing Education,
                           Valencia’s Criminal Justice Institute and Fire Rescue Institute, and the University
                           of Central Florida’s Criminal Justice Program.
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/public-safety-leadership-development-certification/"><u>Learn More</u></a></p>
                           
                        </blockquote>
                        
                        <h3>Internal Training for Valencia Security Officers </h3>
                        
                        <p> Valencia's Criminal Justice Institute and Valencia's Safety &amp; Security Office have
                           established a collaborative training allowing security officers to attend specified
                           blocks of regularly occuring institute training. This training is currently only open
                           to Valencia security officers and is scheduled by the campus security supervisors
                           at a rate of no more than 2 officers per campus, under normal circumstances. This
                           training is repeated every 2-3 months. 
                        </p>
                        
                        <p><a href="../Security.html">Interactions in a Diverse Community &amp; Interviewing and Report Writing</a> 
                        </p>
                        
                        
                        <h3>Future Opportunities</h3>
                        
                        <p>Programs for future development may include:</p>
                        
                        <ul>
                           
                           <li>Bachelor's degree in Homeland Security Public Safety</li>
                           
                           <li>Theme Park and Hotel Security Officer Professional Development</li>
                           
                           <li>ASIS Certified Protection Professional (through&nbsp;Security Management Certificate)</li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <p><a href="../how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="../contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="../partner-with-us.html"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;%3BSearchType=occupation&amp;Search=public+safety&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="career-coach.png" width="250">            
                           </a>
                        
                        
                        
                        
                        <div>
                           
                           <h4>Locations</h4>
                           
                           <align><strong>The School of Public Safety offers training and education at six convenient locations.</strong>
                              <img alt="School of Public Safety at Valencia College" height="174" src="public-safety-locations-250x174.png.html" title="School of Public Safety at Valencia College" width="250">
                              <a href="documents/16SPS002-programs-and-campuses-flyer.pdf.html" target="_blank">Download Locations Map <i aria-hidden="true" title="Locations Map .pdf"></i><span>Locations Map .pdf</span></a>
                              
                              
                           </align>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/safety-and-security-institute/default.pcf">©</a>
      </div>
   </body>
</html>