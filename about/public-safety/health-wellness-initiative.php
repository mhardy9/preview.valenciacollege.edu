<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>School of Public Safety | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/health-wellness-initiative.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li>School of Public Safety</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Health &amp; Wellness Initiative</h2>
                        
                        <p>Valencia College's School of Public Safety is taking a leadership role in providing
                           training in health and wellness for our communities' first responders—helping Central
                           Florida maintain the heathiest public safety workforce. The Health &amp; Wellness Initiative
                           is the product of a collaborative group of public safety and private sector organizations
                           and individuals who have volunteered to develop and deliver training and seminars
                           to improve the quality of life for central Florida's public safety employees. This
                           group includes; 
                           
                           
                           <a href="http://centralfloridawellnesscouncil.org/index.html">Central Florida Wellness Council</a>, Executive Wellness Strategies, <a href="https://www.facebook.com/firefighterfitnesscollaborative">Florida Firefighter Fitness Collaborative</a>, Spectrum Sports Performance, wellness professionals from Orange &amp; Osceola County
                           government, and others. 
                        </p>
                        
                        <h3> <strong>Vision:</strong>
                           
                        </h3>
                        
                        
                        <p>Greatly increase the overall health and wellness of public safety professionals in
                           central Florida while greatly reducing health care costs for public safety organizations
                           and individuals.
                        </p>
                        
                        
                        <h3><strong>Mission:</strong></h3>
                        
                        
                        <p><em>Integrated health and wellness</em> education and resources for central Florida public safety professionals and organizations:
                        </p>
                        
                        <ul>
                           
                           <li>Enhance quality of life</li>
                           
                           <li>Reduce stress</li>
                           
                           <li>Lower health care costs</li>
                           
                           <li>Improve productivity and morale</li>
                           
                        </ul>
                        
                        
                        <p><strong>This program would benefit individuals and agencies by:</strong></p>
                        
                        
                        <p>Building a foundation for future health and wellness education and resources that
                           will prepare public safety professionals for a healthy lifestyle while serving their
                           community, and into their retirement years. 
                        </p>
                        
                        <p><strong>Delivery of Information:</strong></p>
                        
                        <p>Advertisements, Annual calendar of events, Boot camps, Electronic media (clearing
                           house of information), Health food events,
                        </p>
                        
                        <p>Newsletter, Peer fitness trainer program (IFF model), Promotion of information to
                           public safety agencies, Public Safety Health Fairs, Symposiums.  
                        </p>
                        
                        <h3>****** Notice ***** </h3>
                        
                        <h2>Health &amp; Wellness  Seminar</h2>
                        
                        <p>To be scheduled soon!</p>
                        
                        
                        <h2><a href="Veggie.html"><strong>Local Plant Based Dining Locations </strong></a></h2>
                        
                        
                        <p><strong>How healthy is your meal? </strong></p>
                        
                        <h3>
                           <a href="https://chipotle.com/nutrition-calculator">Chipotle Lunch Builder</a> - Burrito or Salad? You compare! 
                        </h3>
                        
                        <p> <a href="https://chipotle.com/nutrition-calculator"><u>https://chipotle.com/nutrition-calculator</u></a> 
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <p><a href="how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="partner-with-us.html"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;%3BSearchType=occupation&amp;Search=public+safety&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="career-coach.png" width="250">            
                           </a>
                        
                        
                        
                        
                        <div>
                           
                           <h4>Locations</h4>
                           
                           <align><strong>The School of Public Safety offers training and education at six convenient locations.</strong>
                              <img alt="School of Public Safety at Valencia College" height="174" src="public-safety-locations-250x174.png" title="School of Public Safety at Valencia College" width="250">
                              <a href="documents/16SPS002-programs-and-campuses-flyer.pdf" target="_blank">Download Locations Map <i aria-hidden="true" title="Locations Map .pdf"></i><span>Locations Map .pdf</span></a>
                              
                              
                           </align>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/health-wellness-initiative.pcf">©</a>
      </div>
   </body>
</html>