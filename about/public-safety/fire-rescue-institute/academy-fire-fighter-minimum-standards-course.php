<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Fire Fighter Minimum Standards Course | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/fire-rescue-institute/academy-fire-fighter-minimum-standards-course.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/fire-rescue-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Fire Fighter Minimum Standards Course</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/fire-rescue-institute/">Fire Rescue Institute</a></li>
               <li>Fire Fighter Minimum Standards Course</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h3>PROGRAM DESCRIPTION:</h3>
                        
                        <p> The Fire Fighter Minimum Standards program (MSC) includes both Fire Fighter I and
                           Fire Fighter II courses, and is designed to incorporate the orientation to fire service
                           with the necessary theory and applications needed to become a certified Fire Fighter.
                           This program meets the requirements for students to take the state certification exam
                           with the Bureau of Fire Standards and Training (which includes both written and practical
                           skills) to become a Certified Fire Fighter in the state of Florida. This is a vocational
                           credit, limited-access program. Once the state Certification is obtained, students
                           can apply to have 3 academic credits awarded toward an AS degree in Fire Science Technology.
                           
                        </p>
                        
                        <p><strong>Note:</strong></p>
                        
                        <p>The practical training portions of Fire Fighter I &amp; II will be offered at the Fire
                           training facility located at John Young Parkway and Oak Ridge Road. Students must
                           furnish their own transportation.
                        </p>
                        
                        <p><strong>POTENTIAL CAREERS: </strong>Fire Fighter
                        </p>
                        
                        <h3>ADMISSION REQUIREMENTS:</h3>
                        
                        <p>Admission to this program is limited. Students must meet the following criteria:</p>
                        
                        <ul>
                           
                           <li>Must be at least 18 years of age</li>
                           
                           <li>Possess a high school diploma or a GED</li>
                           
                           <li>Must successfully complete  a Florida EMT or Paramedic program </li>
                           
                           <li>Have no conviction of felonies or significant misdemeanors</li>
                           
                           <li>Cannot have been&nbsp;dishonorably discharged from military service</li>
                           
                           <li>Be of good moral character</li>
                           
                           <li>Must be in good physical condition</li>
                           
                        </ul>
                        
                        <h3>REQUIREMENTS FOR STATE CERTIFICATION:</h3>
                        
                        <ul>
                           
                           <li>Successfully complete required coursework for this program&nbsp;</li>
                           
                           <li>Receive Certificate of Competency as Firefighter I</li>
                           
                           <li>Successfully pass the State Exam for Fire Fighter II</li>
                           
                        </ul>
                        
                        <h3>REQUIRED COURSEWORK:</h3>            
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Contact Hours </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Vocational Credit </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 0010 </div>
                                 
                                 <div data-old-tag="td">Fire Fighter I </div>
                                 
                                 <div data-old-tag="td">206</div>
                                 
                                 <div data-old-tag="td">6.867</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 0020 </div>
                                 
                                 <div data-old-tag="td">Fire Fighter II </div>
                                 
                                 <div data-old-tag="td">192</div>
                                 
                                 <div data-old-tag="td">6.4</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total Career Certificate Clock Hours</div>
                                 </div>
                                 
                                 <div data-old-tag="td">418</div>
                                 
                                 <div data-old-tag="td">13.934</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p><strong>Fire Fighter I Course</strong></p>
                        
                        <p>The Fire Fighter I course provides both practical applications and an orientation
                           to the fire service. This course includes many functions required of a fire fighter
                           such as: fire service safety; fire behavior; building construction; protective clothing;
                           SCBA; portable extinguishers; ropes and knots; building search and victim removal;
                           forcible entry tools; constructions techniques; ground ladders; ventilation; water
                           supply;  loading and rolling hose; laying, carrying and advancing hose lines; water
                           streams; vehicle and wildland fire control; sprinkler system fundamentals; salvage
                           and overhaul; protecting evidence of fire cause; fire department communication; equipment
                           and techniques; fire prevention and public fire education. The course also includes
                           Operations-Level Hazardous Materials Training. Upon completion of the program, the
                           student will receive a Certificate of Competency from the Bureau of Fire Standards
                           and Training as a Firefighter I which will qualify them to serve as a volunteer Fire
                           Fighter. 
                        </p>
                        
                        <p><strong>Fire Fighter II&nbsp;Course</strong></p>
                        
                        <p>The student must successfully complete the requirements for Fire Fighter I to enter
                           Fire Fighter II. The Fire Fighter II course prepares the student to meet the requirements
                           to become a state certified fire fighter. Course content includes implementing the
                           incident management system; construction materials and building collapse; rescue and
                           extrication tools; vehicle extrication and special rescue; hydrant flow and hose operability;
                           tools and appliances; foam fire systems; ignitable liquid and gas fire control; fire
                           detection; alarm and suppression systems; fire cause and origin; radio communications;
                           incident reports; pre-incident survey; and wildland fire fighting. Students must successfully
                           complete the program to be eligible to test for certification with the Florida Bureau
                           of Fire Standards and Training for  Fire Fighter II. This exam encompasses both written
                           and practical skills. Certification is required in the state of Florida to become
                           a career Fire Fighter.&nbsp;
                        </p>
                        
                        <p><strong>Program Outcomes</strong></p>
                        
                        <ul>
                           
                           <li>Demonstrate knowledge of fire department organization,&nbsp;procedures and responsibilities&nbsp;</li>
                           
                           <li>Demonstrate knowledge of personal protective equipment</li>
                           
                           <li>Demonstrate knowledge of ventilation practices</li>
                           
                           <li>Demonstrate the proper use of ropes, tools and equipment&nbsp;</li>
                           
                           <li>Demonstrate proper use of fire hose, nozzles and appliances&nbsp;</li>
                           
                        </ul>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="../contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="../partner-with-us.html"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        <h3>Events</h3>
                        
                        <div data-id="minimum_standards_class_6443">
                           
                           
                           <div>
                              Jan<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/minimum_standards_class_6443" target="_blank">Minimum Standards Class at School of Public Safety</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        <h3>Upcoming Courses</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              <strong>FFP 1505 </strong>
                              
                              Fire Prevention
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3888&amp;master_version=1&amp;course_area=FRI&amp;course_number=1505&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 1540 </strong>
                              
                              Private Fire Protection Systems I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Both Classroom and Lab</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3882&amp;master_version=1&amp;course_area=FRI&amp;course_number=1540&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2740 </strong>
                              
                              Fire Service Course Delivery
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3878&amp;master_version=1&amp;course_area=FRI&amp;course_number=2740&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2810 </strong>
                              
                              Fire Fighting Tactics and Strategy I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3876&amp;master_version=1&amp;course_area=FRI&amp;course_number=2810&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           <br>
                           
                           <li>
                              <a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/">View More Courses...</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;%3BSearchType=occupation&amp;Search=fire&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="career-coach.png" width="250">
                           </a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/fire-rescue-institute/academy-fire-fighter-minimum-standards-course.pcf">©</a>
      </div>
   </body>
</html>