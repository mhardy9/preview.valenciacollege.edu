<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Fire Science Academy Track | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/fire-rescue-institute/academy-track.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/fire-rescue-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Fire Science Academy Track</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/fire-rescue-institute/">Fire Rescue Institute</a></li>
               <li>Fire Science Academy Track</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h3>PROGRAM DESCRIPTION:</h3>
                        
                        <p>The Fire Science Academy is a degree-granting program that provides students with
                           the skills, abilities and education for a successful fire service career. It provides
                           a strong educational foundation in fire science coupled with training and education
                           in emergency medical services and firefighting. Students will complete the two-year
                           academy as a cohort, learning to function as team members through instructional and
                           team-building exercises.
                        </p>
                        
                        <p><strong>One Program, Three Areas of Education</strong></p>
                        
                        <p>The Fire Science Academy is a fast-paced, intensive program. Participants will complete
                           three components of career development in just two years.
                        </p>
                        
                        <p>Upon completion, students will receive:</p>
                        
                        <ul>
                           
                           <li>A.S. degree in Fire Science Technology (60 college credits)</li>
                           
                           <li>EMT (Emergency Medical Technology) Technical Certificate (must pass a state exam)</li>
                           
                           <li>Minimum Standards Training – Firefighter I and II (must pass a state exam) </li>
                           
                        </ul>
                        
                        <h3>ADMISSION REQUIREMENTS:</h3>
                        
                        <p>The Fire Science Academy is a limited-access program and admission is competitive.
                           The top candidates will be selected based on their application information. Before
                           being considered, all candidates must attend a mandatory information session.
                        </p>
                        
                        <ul>
                           
                           <li>Must be at least 18 years of age<u> </u>
                              
                           </li>
                           
                           <li>Possess a high school diploma or GED<u> </u>
                              
                           </li>
                           
                           <li>Have no felony convictions or significant misdemeanors</li>
                           
                           <li>Be of good moral character<u> </u>
                              
                           </li>
                           
                           <li>Be in good physical condition<u> </u>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>REQUIRED COURSEWORK:</h3>            
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong> Credits </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 1000 </div>
                                 
                                 <div data-old-tag="td"> Fire Science Fundamentals </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 1505 </div>
                                 
                                 <div data-old-tag="td"> Fire Prevention I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 1612 </div>
                                 
                                 <div data-old-tag="td"> Fire Behavior and 
                                    
                                    
                                    Combustion
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> ENC 1101 </div>
                                 
                                 <div data-old-tag="td"> English Composition I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> MGF 1106 </div>
                                 
                                 <div data-old-tag="td"> College Mathematics </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 1540 </div>
                                 
                                 <div data-old-tag="td"> Private Fire Protection Systems I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 2120 </div>
                                 
                                 <div data-old-tag="td"> Building Construction for the Fire Service </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> SPC 1608 </div>
                                 
                                 <div data-old-tag="td"> Fundamentals of Speech </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 2740 </div>
                                 
                                 <div data-old-tag="td"> Fire Service Course Delivery </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> POS 2112 </div>
                                 
                                 <div data-old-tag="td"> State and Local Government </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 1040 </div>
                                 
                                 <div data-old-tag="td"> Minimum Standards Training Classes: Firefighter I, Firefighter II, Emergency Vehicle
                                    Operator 
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 2810 </div>
                                 
                                 <div data-old-tag="td"> Firefighting Tactics and Strategy I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 2510 </div>
                                 
                                 <div data-old-tag="td"> Fire Codes and Standards </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 2720 </div>
                                 
                                 <div data-old-tag="td"> Company Officer </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> FFP 2521 </div>
                                 
                                 <div data-old-tag="td"> Blueprint Reading and Plans Review </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> PHI 2600 </div>
                                 
                                 <div data-old-tag="td"> Ethics and Critical Thinking </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> EMS 1119 </div>
                                 
                                 <div data-old-tag="td"> Fundamentals of Emergency Medical Technology </div>
                                 
                                 <div data-old-tag="td">8</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> EMS 1119L </div>
                                 
                                 <div data-old-tag="td"> Fundamentals of Emergency Medical Technology </div>
                                 
                                 <div data-old-tag="td">2</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> EMS 1431L </div>
                                 
                                 <div data-old-tag="td"> Emergency Medical Technician Practicum </div>
                                 
                                 <div data-old-tag="td">2</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total Credits</div>
                                 </div>
                                 
                                 <div data-old-tag="td">60</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <h3>REQUIREMENTS FOR STATE CERTIFICATION:</h3>
                        
                        <ul>
                           
                           <li>After completing FFP 0020, students must pass the Florida Bureau of Fire Standards
                              test (FL FF II) and complete the three EMS courses in the spring of the second year
                              of the program to be certified as a Firefighter.
                           </li>
                           
                           <li>After completing FFP 2810 Firefighting Tactics and Strategy I, students will earn
                              FEMA ICS 300 certificate. 
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="../contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="../partner-with-us.html"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        <h3>Events</h3>
                        
                        <div data-id="minimum_standards_class_6443">
                           
                           
                           <div>
                              Jan<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/minimum_standards_class_6443" target="_blank">Minimum Standards Class at School of Public Safety</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        <h3>Upcoming Courses</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              <strong>FFP 1505 </strong>
                              
                              Fire Prevention
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3888&amp;master_version=1&amp;course_area=FRI&amp;course_number=1505&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 1540 </strong>
                              
                              Private Fire Protection Systems I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Both Classroom and Lab</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3882&amp;master_version=1&amp;course_area=FRI&amp;course_number=1540&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2740 </strong>
                              
                              Fire Service Course Delivery
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3878&amp;master_version=1&amp;course_area=FRI&amp;course_number=2740&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2810 </strong>
                              
                              Fire Fighting Tactics and Strategy I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3876&amp;master_version=1&amp;course_area=FRI&amp;course_number=2810&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           <br>
                           
                           <li>
                              <a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/">View More Courses...</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;%3BSearchType=occupation&amp;Search=fire&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="career-coach.png" width="250">
                           </a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/fire-rescue-institute/academy-track.pcf">©</a>
      </div>
   </body>
</html>