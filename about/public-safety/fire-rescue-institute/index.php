<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Fire Rescue | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/fire-rescue-institute/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/fire-rescue-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Fire Rescue</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li>Fire Rescue Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <p>Valencia College in collaboration with Central Florida fire agencies has established
                           the premier Fire Rescue Institute to provide career pathways to meet the needs of
                           the fire service community in Central Florida, nationally and globally. Our programs
                           infuse high quality and diverse learning and training opportunities to advance all
                           professionals in the industry. The Institute prepares all levels of fire service professionals,
                           including the fire fighter, fire officer, fire inspector, fire investigator and fire
                           instructor. Also, the Fire Science Technology Associate in Science degree program
                           provides opportunities for students to continue on to a bachelor's degree.
                        </p>
                        
                        
                        <h3>Fire Science Academy Track</h3>
                        
                        
                        <p><strong><a href="http://events.valenciacollege.edu/event/fire_science_academy_track#.WOQJKE0zXrc">Mandatory Information Session Dates</a></strong></p>
                        
                        
                        <p>The Fire Science Academy Track is a unique degree-granting program that provides students
                           with three components of career development in just two years.
                        </p>
                        
                        <p>Upon completion, students will receive:</p>
                        
                        <ul>
                           
                           <li>A.S. degree in Fire Science Technology (60 credits)</li>
                           
                           <li>EMT (Emergency Medical Technology) Technical Certificate (must pass a state exam)<strong></strong>
                              
                           </li>
                           
                           <li>Minimum Standards Training – Firefighter I and Firefighter II (must pass a state exam)</li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p><a href="academy-track.html">Learn More</a></p>
                           
                        </blockquote>
                        
                        
                        <h3>Associate in Science Degree</h3>
                        
                        <p><strong>With specializations in: Fire Officer, Fire Prevention</strong></p>
                        
                        <p>This two-year <a href="http://preview.valenciacollege.edu/asdegrees/fire-services/fire-science-technology.cfm">Fire Science Technology A.S.</a> program offers a scientific understanding of fire and emergency hazards, with emphasis
                           on effective prevention, control and management. It is a path to employment, a bachelor's
                           degree and career enhancement. Valencia also offers an A.S. degree in <a href="http://preview.valenciacollege.edu/asdegrees/health/emst.cfm">Emergency Medical Services Technology</a>, through the division of Allied Health.
                        </p>
                        
                        <p><strong>Related Certificate: Fire Officer Supervisor</strong></p>
                        
                        <p>Many certificates can be completed in less than a year and credits earned apply toward
                           the A.S. degree in Fire Science Technology.
                        </p>
                        
                        
                        <h3>Fire Fighter Minimum Standards Class</h3>
                        
                        <p>This program fulfills the requirements of the Bureau of Fire Standards and Training
                           for students to take the state certification exam  to become a certified fire fighter
                           in the state of Florida.
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="academy-fire-fighter-minimum-standards-course.html">Learn More</a></p>
                           
                        </blockquote>
                        
                        
                        <h3>Fire Apparatus Operator</h3>
                        
                        <p>This course covers the laws, rules and driving techniques for emergency vehicles,
                           as well as fire service hydraulic.  This course will include 40 hours of classroom
                           and 40 hours of practical training on the training grounds.  The practical training
                           will include fire ground hose evolutions and a driving course.
                        </p>
                        
                        
                        <h3>Florida Bureau of Fire Standards and Training Certifications</h3>
                        
                        <p>Valencia offers sets of courses that qualify students to test for the Florida Bureau
                           of Fire Standards and Training certifications. Students must complete the Valencia
                           courses and meet other requirements set by the Florida Bureau of Fire Standards and
                           Training. Students should check with the Bureau for the most current requirements.
                        </p>
                        
                        <ul>
                           
                           <li><a href="florida-bureau-of-fire-standards-and-training-certifications.html#OFFICER1">Fire Officer I</a></li>
                           
                           <li><a href="florida-bureau-of-fire-standards-and-training-certifications.html#OFFICER2">Fire Officer II</a></li>
                           
                           <li><a href="florida-bureau-of-fire-standards-and-training-certifications.html#INSPECT1">Firesafety Inspector I</a></li>
                           
                           <li><a href="florida-bureau-of-fire-standards-and-training-certifications.html#INSPECT2">Firesafety Inspector II</a></li>
                           
                           <li><a href="florida-bureau-of-fire-standards-and-training-certifications.html#INVEST1">Fire Safety Investigator I</a></li>
                           
                           <li><a href="florida-bureau-of-fire-standards-and-training-certifications.html#INSTRUCTOR">Fire Service Instructor</a></li>
                           
                        </ul>
                        
                        
                        <h3>Pathways to Advanced Degrees</h3>
                        
                        <p><strong>Bachelor's Opportunities</strong></p>
                        
                        <p>Valencia’s Associate in Science degree graduates have the opportunity to transfer
                           into the Bachelor of Applied Science degree offered at the University of Central Florida’s
                           regional campuses, as well as other Florida public universities. Also, graduates from
                           select A.S. degrees are guaranteed* admission to the University of Central Florida
                           through <a href="http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/">DirectConnect to UCF</a> (*Consistent with university policy.). Students who wish to transfer to a university
                           should contact their school of choice about transfer requirements, and meet with a
                           Valencia advisor for assistance with education planning. UCF also offers master’s
                           degree-level credits for participation in Valencia’s Public Safety Leadership Development
                           Certification Program.
                        </p>
                        
                        <p><strong>Master's Opportunities</strong></p>
                        
                        
                        
                        <h3>Advanced Specialized Training and Continuing Education</h3>
                        
                        <p><strong>Courses and Training</strong></p>
                        
                        <p>Professional training courses cover the most recent updates, topics and equipment
                           for fire service professionals of all levels to refresh skills and build on experience
                           in order to stay safe and stimulate career advancement. Topic areas include training
                           in fire fighter survival, rescue and vehicle operations, fire apparatus operations,
                           leadership development and more.
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/">View Upcoming Courses</a></p>
                           
                        </blockquote>
                        
                        <p><strong>The Public Safety Leadership Development Certification Program</strong></p>
                        
                        <p>This four-week program focuses on developing public safety professionals into innovative
                           leaders. It is offered in partnership between Valencia College Continuing Education,
                           Valencia’s Criminal Justice Institute and Fire Rescue Institute, and the University
                           of Central Florida’s Criminal Justice Program.
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/public-safety-leadership-development-certification/">Learn More</a></p>
                           
                        </blockquote>
                        
                        
                        <h3>Non-Credit Courses</h3>
                        
                        <p>For students not wishing to pursue a degree, we offer all of the Fire Science Courses
                           at a reduced rate of $150 plus required text.  These courses will not transfer for
                           college credit however, they will meet the Florida Bureau of Fire Standards and Training
                           requirements for certification as a Fire Inspector, Fire Officer, etc.
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/">View Upcoming Courses</a>
                              
                           </p>
                           
                        </blockquote>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="../contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="../partner-with-us.html"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        <h3>Events</h3>
                        
                        <div data-id="minimum_standards_class_6443">
                           
                           
                           <div>
                              Jan<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/minimum_standards_class_6443" target="_blank">Minimum Standards Class at School of Public Safety</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        <h3>Upcoming Courses</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              <strong>FFP 1505 </strong>
                              
                              Fire Prevention
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3888&amp;master_version=1&amp;course_area=FRI&amp;course_number=1505&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 1540 </strong>
                              
                              Private Fire Protection Systems I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Both Classroom and Lab</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3882&amp;master_version=1&amp;course_area=FRI&amp;course_number=1540&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2740 </strong>
                              
                              Fire Service Course Delivery
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3878&amp;master_version=1&amp;course_area=FRI&amp;course_number=2740&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2810 </strong>
                              
                              Fire Fighting Tactics and Strategy I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3876&amp;master_version=1&amp;course_area=FRI&amp;course_number=2810&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           <br>
                           
                           <li>
                              <a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/">View More Courses...</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;%3BSearchType=occupation&amp;Search=fire&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="career-coach.png" width="250">
                           </a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/fire-rescue-institute/index.pcf">©</a>
      </div>
   </body>
</html>