<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Florida Bureau of Fire Standards andTraining Certifications - Required Courses | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/fire-rescue-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Florida Bureau of Fire Standards andTraining Certifications - Required Courses</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/fire-rescue-institute/">Fire Rescue Institute</a></li>
               <li>Florida Bureau of Fire Standards andTraining Certifications - Required Courses</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h3><a name="OFFICER1" id="OFFICER1">Fire Officer I Certification:</a></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Credit</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2720 </div>
                                 
                                 <div data-old-tag="td">Company Officer </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2120 </div>
                                 
                                 <div data-old-tag="td">Building Construction for the Fire Service </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2810 </div>
                                 
                                 <div data-old-tag="td">Firefighting Tactics and Strategy I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2740 </div>
                                 
                                 <div data-old-tag="td">Fire Service Course Delivery </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total</div>
                                 </div>
                                 
                                 <div data-old-tag="td">12</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p><strong>Florida Bureau of Fire Standards and Training additional requirements Fire Officer
                              I</strong></p>
                        
                        <ul>
                           
                           <li>Possess an active Firefighter Certificate of Compliance issued by the Division or
                              have met the curriculum requirements for Firefighter Part I as defined in 69A-37.055(I)
                              F.A.C.
                           </li>
                           
                           <li>Meet the job performance requirements of NFPA 1021-Fire Officer I (2009)</li>
                           
                           <li>Complete Fire Officer I curriculum</li>
                           
                           <li>Complete the “Fire Officer I Task Book” with required signatures.</li>
                           
                           <li>Pass the Fire Officer I Exam with a score of 70% or higher.</li>
                           
                           <li>Complete the National Fallen Firefighters Foundation course titled “Courage to Be
                              Safe” or a course determined by the Division to be equivalent.
                           </li>
                           
                        </ul>            
                        
                        <h3><a name="OFFICER2" id="OFFICER2">Fire Officer II Certification:</a></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Credit</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 1505 </div>
                                 
                                 <div data-old-tag="td">Fire Prevention </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 1540 </div>
                                 
                                 <div data-old-tag="td">Private Fire Protection Systems I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2811 </div>
                                 
                                 <div data-old-tag="td">Firefighting Tactics and Strategy II*</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2741</div>
                                 
                                 <div data-old-tag="td">Fire Service Course Design*</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total</div>
                                 </div>
                                 
                                 <div data-old-tag="td">12</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p><strong>Florida Bureau of Fire Standards and Training additional requirements, Fire Officer
                              II</strong></p>
                        
                        <ul>
                           
                           <li>Possess an active Firefighter Certificate of Compliance issued by the Division or
                              have met the curriculum requirements for Firefighter Part I as defined in 69A-37.055(I)
                              F.A.C.
                           </li>
                           
                           <li>Possess an active Fire Officer I Certificate of Competency issued by the Division.</li>
                           
                           <li>Meet the job performance requirements of NFPA 1021- Fire Officer II (2009)</li>
                           
                           <li>Complete the Fire Officer II Curriculum.</li>
                           
                           <li>Complete the “Fire Officer II Task Book” with required signatures.</li>
                           
                           <li>Pass the Fire Officer II Exam with a score of 70% or higher.</li>
                           
                           <li>Complete the prerequisite course titled “ICS-300: Intermediate ICS for Expanding Incidents”
                              or a course determined by the Division to be equivalent.
                           </li>
                           
                           <li>Complete the Florida State Fire College “Florida State-Wide Emergency Response Plan
                              (SERP)” course.
                           </li>
                           
                           <li> Complete the National Fallen Firefighters Foundation course titled “Courage to Be
                              Safe” or a course determined by the Division to be equivalent.
                           </li>
                           
                        </ul>            
                        <h3><a name="INSPECT1" id="INSPECT1">Firesafety Inspector I Certification:</a></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Credit</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 1505 </div>
                                 
                                 <div data-old-tag="td">Fire Prevention </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 1540 </div>
                                 
                                 <div data-old-tag="td">Private Fire Protection Systems I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2120</div>
                                 
                                 <div data-old-tag="td">Building Construction for the Fire Service</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2521</div>
                                 
                                 <div data-old-tag="td">Blueprint Reading and Plans Review</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2510 </div>
                                 
                                 <div data-old-tag="td">Codes and Standards </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total</div>
                                 </div>
                                 
                                 <div data-old-tag="td">15</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <p><strong>Florida Bureau of Fire Standards and Training additional requirements Inspector I</strong></p>
                        
                        <p>To be certified as a Firesafety Inspector I in the State of Florida, an individual
                           must successful complete 200 hours of certification training for Firesafety Inspector,
                           or have received equivalent training in another state, and pass a state written examination.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3><a name="INSPECT2" id="INSPECT2">Firesafety Inspector II Certification:</a></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Credit</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 1111 </div>
                                 
                                 <div data-old-tag="td">Fire Chemistry </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2541 </div>
                                 
                                 <div data-old-tag="td">Private Fire Protection Systems II*</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2610</div>
                                 
                                 <div data-old-tag="td">Fire Origin and Cause </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 1793 or FFP 2706 </div>
                                 
                                 <div data-old-tag="td">Fire and Life Safety Educator I or <br>
                                    Public Information Officer 
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total</div>
                                 </div>
                                 
                                 <div data-old-tag="td">12</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><strong>Florida Bureau of Fire Standards and Training additional requirements Inspector II</strong></p>
                        
                        <p>To be certified as a Firesafety Inspector II, an individual must be certified as a
                           Firesafety Inspector I in the State of Florida, and must successful complete 160 hours
                           of certification training for Firesafety Inspector II, or have received equivalent
                           training in another state.
                        </p>
                        
                        
                        <h3><a name="INVEST1" id="INVEST1">Firesafety Investigator I Certification:</a></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Credit</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2111 </div>
                                 
                                 <div data-old-tag="td">Fire Chemistry </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2610</div>
                                 
                                 <div data-old-tag="td">Fire Origin and Cause </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 1540 </div>
                                 
                                 <div data-old-tag="td">Fire Protection Systems I </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2120 </div>
                                 
                                 <div data-old-tag="td">Building Construction for the Fire Service </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total</div>
                                 </div>
                                 
                                 <div data-old-tag="td">12</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><strong>Florida Bureau of Fire Standards and Training additional requirements Investigator
                              I</strong></p>
                        
                        <p>To be certified as a Fire Investigator I in the State of Florida, an individual must
                           be State of Florida certified Firefighter, Firesafety Inspector or Police Officer
                           and successfully complete 160 hours of basic certification training for fire investigators,
                           or have received equivalent training in another state, and pass a state written examination.
                        </p>
                        
                        
                        <h3><a name="INSTRUCTOR" id="INSTRUCTOR">Fire Service Instructor Certification:</a></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Course</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Title</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Credit</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">FFP 2740</div>
                                 
                                 <div data-old-tag="td">Fire Service Course Delivery </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>FFP 2741 </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Fire Service Course Design*</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Total</div>
                                 </div>
                                 
                                 <div data-old-tag="td">6</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><strong>Florida Bureau of Fire Standards and Training additional requirements Fire Instructor</strong></p>
                        
                        <p><strong>Instructor I</strong></p>
                        
                        <ul>
                           
                           <li>Six years’ experience as a regular member of an organized fire department.</li>
                           
                           <li>Documentation of completing the 40-hour Fire Service Course Delivery class or equivalent.</li>
                           
                           <li>Documentation of high school graduation or equivalent.</li>
                           
                           <li>Physical ability to perform the tasks associated with the training.</li>
                           
                           <li>Successful completion of the State Written Examination.</li>
                           
                        </ul>
                        
                        <p><strong>Instructor II</strong></p>
                        
                        <ul>
                           
                           <li>Six years’ experience as a regular member of an organized fire department.</li>
                           
                           <li>Documentation of completing the 40-hour Fire Service Course Delivery class or equivalent.</li>
                           
                           <li>Documentation of completing the 40-hour Fire Service Course Design class or equivalent.</li>
                           
                           <li>Documentation of an Associate’s Degree or higher that meets the requirements of s
                              817.567.
                           </li>
                           
                           <li>Physical ability to perform the tasks associated with the training.</li>
                           
                           <li>Successful completion of the State Written Examination.</li>
                           
                        </ul>
                        
                        <p><strong>Instructor III</strong></p>
                        
                        <ul>
                           
                           <li>Six years’ experience as a regular member of an organized fire department.</li>
                           
                           <li>Documentation of completing the 40-hour Fire Service Course Delivery class or equivalent.</li>
                           
                           <li>Documentation of completing the 40-hour Fire Service Course Design class or equivalent.</li>
                           
                           <li>Documentation of a Bachelor’s Degree or higher that meets the requirements of s 817.567.</li>
                           
                           <li>Physical ability to perform the tasks associated with the training.</li>
                           
                           <li>No written examination required.</li>
                           
                        </ul>
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">+</div>
                                 
                                 <div data-old-tag="td">This course must be completed with a grade of C or better.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">*</div>
                                 
                                 <div data-old-tag="td">This course has a prerequisite; check description in Valencia catalog.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">~</div>
                                 
                                 <div data-old-tag="td">This is a general education course.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">1</div>
                                 
                                 <div data-old-tag="td">Students planning to transfer to UCF should select from these humanities courses:
                                    EUH 2000, EUH 2001, ARH 2050, ARH 2051, LIT 2110, LIT 2120, MUL 1010, PHI 2010, REL
                                    2300, and THE 1020.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">2</div>
                                 
                                 <div data-old-tag="td">Students planning to transfer to UCF should select from these math courses: MGF 1106,
                                    MAC 1105, MAC 1114, MAC 2233, MAC 2311, MAC 2312, MAC 2313, and STA 2023. Students
                                    planning to transfer to UCF should select from these science courses: AST 1002, CHM
                                    1020, PHY 2053C, PSC 1020C, BSC 1005, BSC 1050, BSC 1010C, and ESC 1000.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">3</div>
                                 
                                 <div data-old-tag="td">Students who want to continue their education should contact their university of choice
                                    for acceptance of this degree. For students who are interested in pursuing UCF's B.A.S.
                                    degree, 36 hours of general education courses will be required. Students will also
                                    have to fulfill the state's foreign language requirement, which is two years of the
                                    same high school foreign language or two semesters of the same college-level foreign
                                    language. It is also recommended that those students take PSY 2012 General Psychology
                                    rather than SYG 2000 Introductory Sociology.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">(GR)</div>
                                 
                                 <div data-old-tag="td">Denotes a Gordon Rule course.</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="../contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="../partner-with-us.html"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        <h3>Events</h3>
                        
                        <div data-id="minimum_standards_class_6443">
                           
                           
                           <div>
                              Jan<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/minimum_standards_class_6443" target="_blank">Minimum Standards Class at School of Public Safety</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        <h3>Upcoming Courses</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              <strong>FFP 1505 </strong>
                              
                              Fire Prevention
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3888&amp;master_version=1&amp;course_area=FRI&amp;course_number=1505&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 1540 </strong>
                              
                              Private Fire Protection Systems I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Both Classroom and Lab</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3882&amp;master_version=1&amp;course_area=FRI&amp;course_number=1540&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2740 </strong>
                              
                              Fire Service Course Delivery
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3878&amp;master_version=1&amp;course_area=FRI&amp;course_number=2740&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              <strong>FFP 2810 </strong>
                              
                              Fire Fighting Tactics and Strategy I
                              <br>
                              
                              <i aria-hidden="true"></i>&nbsp;<strong>Aug 28  - Oct 19</strong> 
                              
                              <span><i aria-hidden="true"></i>&nbsp;Hybrid Classroom and Online</span>
                              <br>
                              
                              
                              <span><i aria-hidden="true"></i>&nbsp;<a href="http://c2k.valenciacollege.edu/ce/CourseListing.asp?master_id=3876&amp;master_version=1&amp;course_area=FRI&amp;course_number=2810&amp;course_subtitle=00">Continuing Ed</a></span>
                              
                              
                           </li>
                           
                           <br>
                           
                           <li>
                              <a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-services/">View More Courses...</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;%3BSearchType=occupation&amp;Search=fire&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="career-coach.png" width="250">
                           </a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.pcf">©</a>
      </div>
   </body>
</html>