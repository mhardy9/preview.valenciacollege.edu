<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/criminal-justice-institute/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/criminal-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li>Criminal Justice Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        
                        
                        <p>Valencia's Criminal Justice Institute is one of the most recognized and respected
                           criminal justice training centers in the state of Florida.&nbsp;The institute is certified
                           by the Florida Criminal Justice Standards and Training Commission to deliver all Commission-approved
                           curricula to law enforcement, correctional&nbsp;and correctional probation officers in
                           Florida. With primary responsibility for the criminal justice agencies in Orange County,
                           the&nbsp;Criminal Justice Institute&nbsp;is a regional training center that serves over 45 organizations,
                           including state law enforcement agencies, Department of Corrections, Department of
                           Juvenile Justice and&nbsp;numerous county, municipal, regional and international agencies.
                        </p>
                        
                        <p>Our courses are developed in coordination with, and  taught by, experts in the field
                           who come from a variety of criminal justice&nbsp;agencies.
                        </p>
                        
                        
                        <h3>Academies/Industry Certifications</h3>
                        
                        <p>Our state-certified&nbsp;academies fulfill the training requirements to become sworn corrections
                           or law enforcement officers in Florida.&nbsp;Academy graduates can also&nbsp;earn credits toward
                           a two-year A.S. degree at Valencia.
                        </p>
                        
                        <ul>
                           
                           <li><a href="basic_law.html"><u>Law Enforcement Officer</u></a></li>
                           
                           <li><a href="basic_correctional.html"><u>Correctional Officer</u></a></li>
                           
                        </ul>
                        
                        
                        <p><a href="../faq.html"><strong>Frequently Asked Questions?</strong></a> 
                        </p>
                        
                        <h3>Associate in Science Degree</h3>
                        
                        <p>The two-year&nbsp;<a href="index.html"><u>Criminal Justice A.S. degree</u></a>&nbsp;program offers a broad background in the history, philosophy, organization, management
                           and operation of the criminal justice system. It is a path to employment, a bachelor's
                           degree and career enhancement. As part of Valencia's “Two for All” initiative, state-certified
                           professionals may be able to receive credits toward the A.S. degree for professional
                           experience.
                        </p>
                        
                        <p>Related Certificates:&nbsp;</p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext"><u>Homeland Security Specialist</u></a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext"><u>Criminal Justice Technology Specialist</u></a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext"><u>Homeland Security Law Enforcement Specialist</u></a></li>
                           
                        </ul>
                        
                        <p>Many certificates can be completed in less than a year and credits earned apply toward
                           the A.S. degree in Criminal Justice.
                        </p>
                        
                        <p>Pathways to&nbsp;Advanced Degrees</p>
                        
                        <p><strong>Bachelor's Opportunities</strong></p>
                        
                        <p>Valencia’s Associate in Science degree graduates have the&nbsp;opportunity to transfer
                           into the Bachelor of Applied Science degree&nbsp;offered at the University of Central Florida’s
                           regional campuses, as&nbsp;well as other Florida public universities. Also, graduates from
                           select&nbsp;A.S. degrees are guaranteed* admission to the University of Central&nbsp;Florida
                           through&nbsp;<a href="http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/"><u>DirectConnect to UCF</u></a>. (*Consistent with university policy.) Students who wish to transfer&nbsp;to a university
                           should contact their school of choice about transfer&nbsp;requirements, and meet with a
                           Valencia advisor for assistance with&nbsp;education planning. UCF also offers master’s
                           degree-level credits&nbsp;for participation in Valencia’s Public Safety Leadership Development&nbsp;Certification
                           Program. 
                        </p>
                        
                        <p><strong>Master's Opportunities</strong></p>
                        
                        <p>Professionals&nbsp;who participate in Valencia's&nbsp;<a href="http://preview.valenciacollege.edu/continuing-education/programs/public-safety-leadership-development-certification/"><u>Public Safety Leadership Development Certification Program&nbsp;</u></a>(PSLDCP)&nbsp;will&nbsp;receive credits toward&nbsp;the&nbsp;<a href="http://www.graduatecatalog.ucf.edu/programs/program.aspx?id=11555"><u>Graduate Certificate in Criminal&nbsp;Justice Executive</u></a>&nbsp;from the University of Central Florida, which is offered exclusively to&nbsp;PSLDCP&nbsp;participants.
                        </p>
                        
                        
                        <h3>Advanced Specialized Training</h3>
                        
                        <p>Advanced Specialized Training Courses are open to&nbsp;sworn Florida law enforcement, corrections
                           or probation officers. Officers from agencies located in Orange County have priority
                           enrollment over officers from other counties or state agencies. Only agency training
                           coordinators can register officers for Advanced Specialized Training courses. Contact
                           your agency training coordinator if you are interested in enrolling in a course or
                           program.&nbsp;
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="https://secure.valenciacollege.edu/cjisignup2/index.cfm"><u>View Upcoming Courses</u></a></p>
                           
                        </blockquote>
                        
                        <p>Continuing Education</p>
                        
                        <p><strong>Courses and Training</strong></p>
                        
                        <p>Valencia’s Continuing Education division offers&nbsp;professional training, seminars and
                           workshops dedicated&nbsp;to the public safety community. We provide opportunities for sworn
                           law enforcement and corrections officers as well as civilian criminal justice professionals.
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/criminal-justice/"><u>View Upcoming Courses</u></a></p>
                           
                        </blockquote>
                        
                        <p><strong>The Public Safety Leadership Development Certification Program</strong></p>
                        
                        <p>This four-week program&nbsp;focuses on developing public safety professionals into innovative
                           leaders. It is offered&nbsp;in&nbsp;partnership between Valencia College Continuing Education,
                           Valencia’s Criminal Justice Institute and Fire Rescue Institute, and the University
                           of Central Florida’s Criminal Justice Program.
                        </p>
                        
                        <blockquote>
                           
                           <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/public-safety-leadership-development-certification/"><u>Learn More</u></a></p>
                           
                        </blockquote>
                        
                        
                        <div>
                           <a href="../login.html">Secure Login Staff Only
                              </a> 
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../how-to-apply.html"><strong>How to Apply</strong></a></p>
                        
                        <p><a href="../contact.html"><strong>Contact an Advisor</strong></a></p>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/public-safety/partner-with-us.cfm" target="_blank"><strong>Partner with Us</strong></a></p>
                        
                        
                        
                        
                        
                        <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;%3BSearchType=occupation&amp;Search=criminal+justice&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" target="_blank">
                           <img height="140" src="career-coach.png" width="250">            
                           </a>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/criminal-justice-institute/index.pcf">©</a>
      </div>
   </body>
</html>