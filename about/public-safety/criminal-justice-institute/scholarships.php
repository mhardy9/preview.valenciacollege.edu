<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/criminal-justice-institute/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/criminal-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
               <li>Criminal Justice</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <div>
                           <span><strong><font size="5">SCHOLARSHIP RESOURCES </font></strong></span>
                           
                           <hr>
                           
                        </div>
                        
                        
                        
                        
                        
                        <h3><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466"><strong><img alt="Click to Apply" border="0" height="85" src="scholarshipClickNow.png" width="297"></strong></a></h3>
                        
                        <h3><strong>VALENCIA COLLEGE'S ONE STOP SHOP FOR SCHOLARSHIPS! </strong></h3>
                        
                        
                        <hr>
                        
                        <h3>The two scholarships below are in honor of Orange County Sheriff's Deputies and </h3>
                        
                        <h3>Valencia Criminal Justice Institute graduates who lost their lives in the line of
                           duty. 
                        </h3>
                        
                        <hr>
                        
                        <p><a href="http://www.michaelcallin.org/criteria.cfm"><strong>Michael Callin Scholarship</strong></a></p>
                        
                        
                        
                        <ul>
                           
                           <li>    <strong>Criminal Justice Institute at Valencia Community College - Law Enforcement Academy
                                 Recruit.</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              
                              <p>Acceptance Letter from the Criminal Justice Institute at Valencia College. </p>
                              
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li> Complete Essay:
                              
                              <ul>
                                 
                                 <li>List the specific examples of how you exhibit these qualities</li>
                                 
                                 <li>Give examples of your service to the community</li>
                                 
                                 <li>The typewritten essay must be between 500 – 750 words</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <blockquote>&nbsp;
                              
                           </blockquote>
                           
                           <ul>
                              
                              <li>Transcripts from previous school showing at least a 2.5 grade point average.</li>
                              
                           </ul>
                           
                        </ul>
                        
                        <ul>
                           
                           <ul>
                              
                              <li>Two character references (non family).</li>
                              
                              <li>
                                 
                              </li>
                           </ul>
                           
                           <blockquote>&nbsp;
                              
                           </blockquote>
                           
                           <p> $1000.00 scholarships will be awarded to the recipients chosen by the Michael Callin
                              Memorial Scholarship Fund Board of Directors based on the listed qualifications. 
                           </p>
                           
                        </ul>
                        
                        <hr>
                        
                        <p> <span><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">DFC Brandon Lee Coates Memorial Scholarship</a></span></p>
                        
                        <p>Apply with <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation</a>  to automatically apply for this scholarship and any other eligible scholarships
                           from the foundation. 
                        </p>
                        
                        
                        
                        
                        <p>DFC Brandon Lee Coates Memorial Scholarship was created in memory of Deputy First
                           Class Brandon Lee Coates. This memorial scholarship is for academy students at Valencia's
                           Criminal Justice Institute.  
                           Students must attend the School of Public Safety and be enrolled full time. Eligible
                           students must complete a FAFSA and demonstrate financial need. 
                           Deputy Coates attended basic recruit law enforcement training at CJI in 2006. His
                           wife, Orange County Deputy Sheriff Virginia Coates, is also a graduate of the program.
                           <br>
                           <br>
                           Created by the Basic Law Enforcement Class 11-02, donations are still being accepted
                           at <a href="http://www.VALENCIA.org">www.VALENCIA.org</a>. Click on &amp;gt;&amp;gt;Make A Donation and when asked to specify the designation please
                           select DFC Brandon Lee Coates Memorial Scholarship. Those who wish to donate may also
                           do so by mail to Valencia Foundation, 190 S. Orange Avenue, Orlando, FL 32801. 
                        </p>
                        
                        <hr>
                        
                        <hr>
                        
                        <h3>SCHOLARSHIP FUND RAISERS</h3>
                        
                        <h3><img alt="2011 Night LEO Class Donation" height="250" src="CJI-Grads-2011-013-523x374.jpg" width="349"></h3>
                        
                        <h3> Nineteen students graduated from the Criminal Justice Institute (CJI) Basic Law Enforcement
                           Class on August 25, leaving behind more than just memories of defensive tactics and
                           first aid training. Through fundraisers, the class made a $1,250 donation to the Valencia
                           Foundation for the Deputy First Class <a href="http://preview.valenciacollege.edu/public-safety/criminal-justice-institute/scholarship.cfm">Brandon Lee Coates Memorial Scholarship</a>. 
                        </h3>
                        
                        <p>Courtesy of Valencia College's <a href="http://thegrove.valenciacollege.edu/cji-graduates-give-back/">THE GROVE </a></p>
                        
                        <hr>
                        
                        <p> &nbsp; "I cannot teach anybody anything, I can only make them think." </p>
                        
                        <p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Socrates 
                        </p>
                        
                        
                        
                        
                        <hr>
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/criminal-justice-institute/scholarships.pcf">©</a>
      </div>
   </body>
</html>