<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/criminal-justice-institute/LEO_Auxiliary.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/criminal-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
               <li>Criminal Justice</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        
                        
                        
                        <h2>LEO Auxiliary  Academy</h2>
                        
                        <hr>                                                
                        <p><span> 
                              Program Description:<br>
                              </span>This program is designed to train applicants for employment or appointment by criminal
                           justice agencies, with or without compensation, to assist or aid full-time or part-time
                           officers. 
                        </p>
                        
                        <p><span>Program 
                              Content:<br>
                              </span> Courses include  Vehicle Operations,First Aid, Firearms, Defensive Tactics, Patrol
                           andTraffic,Tactical Applications, Dart-Firing Stun Gun and State Exam Review. <span><br>
                              <br>
                              Program Length:  hours. </span></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>8 Weeks&nbsp; (319 hours in addition to 20 hour exam review)</strong></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> 
                                    <p><strong>                    Monday through Friday. (Or as scheduled) </strong></p>
                                    
                                    <p><strong>Hours: To be determined. </strong></p>
                                    
                                    <p><strong>Saturday: Hours vary
                                          </strong></p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><span>Estimated 
                              Costs:</span></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Tuition: </strong></div>
                                 
                                 <div data-old-tag="td">Florida Resident $1,225.00<br>
                                    (approximate; subject to change)
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Materials Cost:</strong></div>
                                 
                                 <div data-old-tag="td">$60.00<br>
                                    (approximate) includes books
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Valencia College 
                                       Application Fee:</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <p><br>
                                       $35.00
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>CJ BAT fee: </strong></div>
                                 
                                 <div data-old-tag="td">$40.00</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>State exam fee:</strong></div>
                                 
                                 <div data-old-tag="td">$100.00</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Program Courses</p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> CJK 0240</div>
                                 
                                 <div data-old-tag="td">Law Enforcement Auxiliary Introduction </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0241</div>
                                 
                                 <div data-old-tag="td">Law Enforcement Auxiliary Patrol and Traffic </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0242</div>
                                 
                                 <div data-old-tag="td">Law Enforcement Auxiliary Investigations </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> CJK 0422</div>
                                 
                                 <div data-old-tag="td">Dart Firing Stun Gun </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0031</div>
                                 
                                 <div data-old-tag="td">CMS First Aid for Criminal Justice Officers </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0040</div>
                                 
                                 <div data-old-tag="td">CMS Criminal Justice Firearms</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> CJK 0051</div>
                                 
                                 <div data-old-tag="td">CMS Criminal Justice Defensive Tactics </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0020</div>
                                 
                                 <div data-old-tag="td">CMS Criminal Justice Vehicle Operations </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJD 0939 </div>
                                 
                                 <div data-old-tag="td">State Exam Review (3 days) </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Admission 
                           Requirements
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <div>Applicant must be at least 19 years of age.<br>
                                 
                              </div>
                              
                           </li>
                           
                           <li> 
                              
                              <div>Applicant must be a citizen of the United 
                                 States.<br>
                                 
                              </div>
                              
                           </li>
                           
                           <li> 
                              
                              <div>Applicant must have a high school diploma 
                                 or equivalent.<br>
                                 
                              </div>
                              
                           </li>
                           
                           <li> 
                              
                              <div>Applicant cannot have a dishonorable discharge 
                                 from the Armed Forces of the United States.<br>
                                 
                              </div>
                              
                           </li>
                           
                           <li> 
                              
                              <div>Applicant cannot have been convicted of any 
                                 felony, or of a misdemeanor involving perjury or false statement. 
                                 Any person who, after July 1, 1981, pleads guilty or nolo contendere 
                                 to, or is found guilty of a felony, or of a misdemeanor involving 
                                 perjury or a false statement, shall not be eligible for employment 
                                 or appointment as an officer, not-withstanding suspension of 
                                 sentence or withholding of adjudication.<br>
                                 
                              </div>
                              
                           </li>
                           
                           <li> 
                              
                              <div>Applicant must take the Criminal Justice 
                                 Basic Abilities Test (CJ BAT) Law Enforcement Version and achieve 
                                 required scores.<br>
                                 
                              </div>
                              
                           </li>
                           
                           <li> 
                              
                              <div>Applicant will be required to provide birth 
                                 certificate, high school diploma or equivalent, valid Florida 
                                 Driver's License, Social Security Card, Military Form DD214 
                                 (if applicable), college transcript or other proof of degree 
                                 (if applicable), and proof of medical insurance.
                              </div>
                              
                           </li>
                           
                        </ul>
                        
                        <div>
                           
                           <ul>
                              
                              <li>Proof of completion of the FDLE Basic Correctional Academy 
                                 and passed the State Correctional Officer Certificate Examination 
                                 within the past 4 years of the start of the crossover academy 
                                 or if an active certified officer currently employed by a correctional 
                                 agency a copy of your current pay stub.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <div> 
                           
                           <ul>
                              
                              <li>If <strong>NOT</strong> currently employed, a background check 
                                 will be performed by CJI.<br>
                                 
                              </li>
                              
                              <li>Applicant must fill out a Valencia Student Application.<br>
                                 
                              </li>
                              
                              <li>Applicant must fill out a Criminal justice Institute Application.</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr>            
                        <p><strong>For 
                              further information call </strong></p>
                        
                        <p><strong><font size="5">(407) 
                                 582-8200</font></strong></p>
                        
                        <p><a href="#top">TOP</a> 
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/criminal-justice-institute/LEO_Auxiliary.pcf">©</a>
      </div>
   </body>
</html>