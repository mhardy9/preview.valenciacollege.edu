<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/criminal-justice-institute/basic_law.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/criminal-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
               <li>Criminal Justice</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>Basic Law Enforcement Academy</h2>
                        
                        <p><a href="#01">Program Description</a> <br>
                           <a href="#02">Estimated Costs </a><br>
                           <a href="#03">Program Courses</a> <br>
                           <a href="#04">Admission Requirements </a><a name="01" id="01"></a></p>
                        
                        <p>The Criminal Justice Institute at Valencia College is 
                           certified by the Florida Department of Law Enforcement, Division 
                           of Criminal Justice Standards and Training.
                        </p>
                        
                        <p><span>Program Description:<br>
                              </span>This program is designed to prepare individuals for entry 
                           level positions in law enforcement, such as police officers and 
                           deputy sheriffs.
                        </p>
                        
                        <p><span>Program Content:<br>
                              </span>Courses include Law, Vehicle Operations, Communications, 
                           First Aid, Firearms, Defensive Tactics, Patrol, Investigations, 
                           Investigating Offenses, Traffic Stops, Crash Investigations, Tactical 
                           Applications, Dart-Firing Stun Gun and State Exam Review.
                        </p>
                        
                        <p><a href="documents/ApplicantFitnessLetter.doc">Minimum Fitness Standards</a></p>
                        
                        <p><span>Practical Skills and Field Exercises:<br>
                              </span>Practical skills and field exercises include courtroom demeanor 
                           and testifying, report writing, identification, collection and preservation 
                           of evidence, interviewing and interrogation techniques, radio communications, 
                           patrol techniques, traffic direction, traffic crash investigations, 
                           police vehicle operation, defensive tactics, arrest techniques, 
                           firearms training, chemical agent application, cpr and first aid, 
                           fingerprinting and sobriety testing.<span><br>
                              <br>
                              Program Length: 770 hours</span></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>DAY - 19 Weeks</strong></div>
                                 
                                 <div data-old-tag="td"><strong>EVENING - 9 Months</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">6:00AM - 5:00PM<br>
                                    Monday - Thursday <br> 
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <p>6:00PM - 10:00PM<br>
                                       Monday through Thursday and 8-12 hours on Saturday
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a name="02" id="02"></a><a href="#top">TOP</a></p>
                        
                        <p><span>Estimated Costs:</span></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Tuition: </strong></div>
                                 
                                 <div data-old-tag="td">Florida Resident $3,000<br>
                                    (approximate; subject to change)
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Materials Cost:</strong></div>
                                 
                                 <div data-old-tag="td">$550.00<br>
                                    (subject to change) Includes books, uniforms and other equipment
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>CJI Application Fee:</strong></div>
                                 
                                 <div data-old-tag="td">$100.00 (non refundable)</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Valencia College Application 
                                       Fee:</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>$35.00 (non refundable)</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>CJ BAT fee: </strong></div>
                                 
                                 <div data-old-tag="td">$40.00</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>State exam fee:</strong></div>
                                 
                                 <div data-old-tag="td">$100.00 &amp; ($35 proctoring fee if exam taken at CJI) </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a name="03" id="03"></a><a href="#top">TOP</a></p>
                        
                        <p>Program Courses</p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0007</div>
                                 
                                 <div data-old-tag="td">Introduction to Law Enforcement </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0008 </div>
                                 
                                 <div data-old-tag="td">Legal</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0011<br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Human Issues</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0017</div>
                                 
                                 <div data-old-tag="td"> Communications</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0020</div>
                                 
                                 <div data-old-tag="td">CMS Law Enforcement Vehicle Operations</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0031</div>
                                 
                                 <div data-old-tag="td">CMS First Aid for Criminal Justice Officers</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0040</div>
                                 
                                 <div data-old-tag="td">CMS Criminal Justice Firearms</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0051</div>
                                 
                                 <div data-old-tag="td">CMS Criminal Justice Defensive Tactics</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0061</div>
                                 
                                 <div data-old-tag="td"> Patrol I </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0062 </div>
                                 
                                 <div data-old-tag="td"> Patrol II </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0071</div>
                                 
                                 <div data-old-tag="td">Criminal  Investigations</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> CJK 0076</div>
                                 
                                 <div data-old-tag="td">Crime Scene Investigations </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0082 </div>
                                 
                                 <div data-old-tag="td">Traffic Stops </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0083 </div>
                                 
                                 <div data-old-tag="td">DUI Traffic Stops </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> CJK 0086</div>
                                 
                                 <div data-old-tag="td">Traffic  Crash Investigations</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> CJK 0096</div>
                                 
                                 <div data-old-tag="td">Criminal Justice Office Physical Fitness</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0422</div>
                                 
                                 <div data-old-tag="td">Dart-Firing Stun Gun</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJD 0939 </div>
                                 
                                 <div data-old-tag="td">CJ Exam Review </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a name="04" id="04"></a><a href="#top">TOP</a></p>
                        
                        <p>Admission Requirements</p>
                        
                        <ul>
                           
                           <li>Applicant must be at least 19 years of age.<br>
                              
                           </li>
                           
                           <li>Applicant must be a citizen of the United States.<br>
                              
                           </li>
                           
                           <li>Applicant must have a high school diploma or equivalent.<br>
                              
                           </li>
                           
                           <li>Applicant cannot have a dishonorable discharge from the Armed 
                              Forces of the United States.<br>
                              
                           </li>
                           
                           <li>Applicant cannot have been convicted of any felony, or of a 
                              misdemeanor involving perjury or false statement. Any person who, 
                              after July 1, 1981, pleads guilty or nolo contendere to, or is 
                              found guilty of a felony, or of a misdemeanor involving perjury 
                              or a false statement, shall not be eligible for employment or 
                              appointment as an officer, not-withstanding suspension of sentence 
                              or withholding of adjudication.<br>
                              
                           </li>
                           
                           <li>Applicant must take the Criminal Justice Basic Abilities Test 
                              (CJ BAT) and achieve required scores.<br>
                              
                           </li>
                           
                           <li>Applicant will be required to provide birth certificate, high 
                              school diploma or equivalent, valid Florida Driver's License, 
                              Social Security Card, Military Form DD214 (if applicable), college 
                              transcript or other proof of degree (if applicable), and proof 
                              of medical insurance.<br>
                              
                           </li>
                           
                           <li>Applicant must have a physical examination prior to program 
                              entry. Forms for this purpose are part of the application packet 
                              available in the Criminal Justice Institute Office.<br>
                              
                           </li>
                           
                           <li>Applicant must fill out a Valencia Student Application.<br>
                              
                           </li>
                           
                           <li>Applicant must fill out a Criminal Justice Institute Application.</li>
                           
                        </ul>
                        
                        
                        <p><strong>Students will be admitted into the Academy in the following 
                              order of priority. </strong></p>
                        
                        <ul>
                           
                           <li>Applicants who are employed by any agency which is a member 
                              of the CJI Advisory Board.<br>
                              
                           </li>
                           
                           <li>Applicants who are employed by any other agency in the state.<br>
                              
                           </li>
                           
                           <li>Applicants who are sponsored by any agency.</li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>Valencia College AA/AS students or US Military Veteran.<br>
                              
                           </li>
                           
                           <li>All other qualified pre-service applicants.<br>
                              Upon completion of the Academy, each student will be required 
                              to take and pass a state examination prior to being certified 
                              in the State of Florida. This exam consists of five sections and 
                              requires a passing score of 80%.
                           </li>
                           
                        </ul>            
                        
                        <p><a href="scholarships.html">Apply for Scholarships </a></p>
                        
                        <h2>
                           <strong><a href="LEO_Auxiliary.html">Auxiliary Law Enforcement Academy</a></strong><a href="../LEO_Auxiliary.html"></a>
                           
                        </h2>
                        
                        
                        
                        <p><strong>For further information call (407) 582-8200</strong></p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/criminal-justice-institute/basic_law.pcf">©</a>
      </div>
   </body>
</html>