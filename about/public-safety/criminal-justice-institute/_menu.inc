<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-window-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../index.php">Criminal Justice</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3"><div class="col-md-4"><ul>
<li><a href="../index.php">School of Public Safety</a></li>
<a href="index.php"><h3>Criminal Justice</h3></a><li><a href="basic_law.php">Law Enforcement Officer</a></li>
<li><a href="basic_correctional.php">Correctional Officer</a></li>
<a href="../fire-rescue-institute/index.php"><h3>Fire Rescue</h3></a><li><a href="../fire-rescue-institute/academy-track.php">Fire Science Academy Track</a></li>
<li><a href="../fire-rescue-institute/academy-fire-fighter-minimum-standards-course.php">Fire Fighter Minimum Standards Class</a></li>
<li><a href="../fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.php">Florida Bureau of Fire Standards and Training Certifications</a></li>
<a href="../safety-and-security-institute/index.php"><h3>Safety and Security</h3></a><li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Criminal Justice Technology Specialist</a></li>
<li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Homeland Security Law Enforcement Specialist</a></li>
<li><a href="../how-to-apply.php">How to Apply</a></li>
<li><a href="../contact.php">Contact Us</a></li>
</ul></div></div>
</li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
