<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/criminal-justice-institute/basic_correctional.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/criminal-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li><a href="/about/public-safety/criminal-justice-institute/">Criminal Justice Institute</a></li>
               <li>Criminal Justice</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>Basic Correctional Officer Academy</h2>
                        
                        <p><a href="#01">Program Description</a> <br>
                           <a href="#02">Estimated Costs </a><br>
                           <a href="#03">Program Courses</a> <br>
                           <a href="#04">Admission Requirements </a><a name="01" id="01"></a></p>
                        
                        <p><span> Program Description:<br>
                              </span>This program is designed to prepare individuals for entry 
                           level positions as correctional officers.
                        </p>
                        
                        <p><span>Program Content:<br>
                              </span>Course includes introduction to criminal justice, legal aspects, 
                           defensive tactics, physical fitness training, weapons, first responder 
                           techniques, court system, communications, interpersonal skills, 
                           correctional operations, emergency preparedness and state exam review.
                        </p>
                        
                        <p><a href="documents/ApplicantFitnessLetter.doc">Minimum Fitness Standards</a></p>
                        
                        <p><span>Practical Skills and Field Exercises:<br>
                              </span>Practical skills and field exercises include courtroom demeanor 
                           and testifying, report writing, identification, collection and preservation 
                           of evidence, radio communications, patrol techniques, defensive 
                           tactics, and firearms training; chemical agent application, first 
                           responder, and fingerprinting.<span><br>
                              <br>
                              Program Length: 420 hours</span></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>DAY - 12 Weeks</strong></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> 6:00AM to 5:00PM<br>
                                    Monday - Thursday<br>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p> The Criminal Justice Institute at Valencia College is 
                           certified by the Florida Department of Law Enforcement, Division 
                           of Criminal Justice Standards and Training.
                        </p>
                        
                        <p><a name="02" id="02"></a><a href="#top">TOP</a></p>
                        
                        <p><span>Estimated Costs:</span></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Tuition: </strong></div>
                                 
                                 <div data-old-tag="td">Florida Resident $1500 <br>
                                    (approximate; subject to change)
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Materials Cost:</strong></div>
                                 
                                 <div data-old-tag="td">$500.00<br>
                                    (approximate) Includes books, uniforms and other equipment
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>CJI Application Fee:</strong></div>
                                 
                                 <div data-old-tag="td">$100.00 (non refundable)</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>Valencia College Application 
                                       Fee:</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>$35.00 (non refundable)</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>CJ BAT fee: </strong></div>
                                 
                                 <div data-old-tag="td">$40.00</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"><strong>State exam fee:</strong></div>
                                 
                                 <div data-old-tag="td">$100.00
                                    
                                    
                                    &amp; ($35 proctoring fee if exam taken at CJI) 
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a name="03" id="03"></a><a href="#top">TOP</a>
                           
                        </p>
                        
                        <p>Program Courses</p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0300</div>
                                 
                                 <div data-old-tag="td">Introduction to Corrections </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0305</div>
                                 
                                 <div data-old-tag="td">Communications</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0310</div>
                                 
                                 <div data-old-tag="td">Officer Safety </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0315</div>
                                 
                                 <div data-old-tag="td">Facility and Equipment </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0320<br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Intake and Release </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0325</div>
                                 
                                 <div data-old-tag="td">Suervising in a Correctional Facility</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0330<br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Supervising Special Populations </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0335</div>
                                 
                                 <div data-old-tag="td">Responding to Incidents and Emergencies </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0031<br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">CMS First Aid for Criminal Justice Officers </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0040</div>
                                 
                                 <div data-old-tag="td">CMS Criminal Justice Firearms</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">CJK 0051</div>
                                 
                                 <div data-old-tag="td">CMS Criminal Justice Defensive Tactics </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> CJK 0340</div>
                                 
                                 <div data-old-tag="td">Officer Wellness and Physical Abilities </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">CJK 0939 </div>
                                 
                                 <div data-old-tag="td">Criminal Justice Exam Review</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a name="04" id="04"></a>
                           <a href="#top">TOP</a>                        
                        </p>
                        
                        <p>Admission Requirements</p>
                        
                        <ul>
                           
                           <li>Applicant must be at least 19 years of age.<br>
                              
                           </li>
                           
                           <li>Applicant must be a citizen of the United States.<br>
                              
                           </li>
                           
                           <li>Applicant must have a high school diploma or equivalent.<br>
                              
                           </li>
                           
                           <li>Applicant cannot have a dishonorable discharge from the Armed 
                              Forces of the United States.<br>
                              
                           </li>
                           
                           <li>Applicant cannot have been convicted of any felony, or of a 
                              misdemeanor involving perjury or false statement. Any person who, 
                              after July 1, 1981, pleads guilty or nolo contendere to, or is 
                              found guilty of a felony, or of a misdemeanor involving perjury 
                              or a false statement, shall not be eligible for employment or 
                              appointment as an officer, not-withstanding suspension of sentence 
                              or withholding of adjudication.<br>
                              
                           </li>
                           
                           <li>Applicant must take the Criminal Justice Basic Abilities Test 
                              (CJ BAT) and achieve required scores.<br>
                              
                           </li>
                           
                           <li>Applicant will be required to provide birth certificate, high 
                              school diploma or equivalent, valid Florida Driver's License, 
                              Social Security Card, Military Form DD214 (if applicable), college 
                              transcript or other proof of degree (if applicable), and proof 
                              of medical insurance.<br>
                              
                           </li>
                           
                           <li>Applicant must have a physical examination prior to program 
                              entry. Forms for this purpose are part of the application packet 
                              available in the Criminal justice Institute Office.<br>
                              
                           </li>
                           
                           <li>Applicant must fill out a Valencia Student Application.<br>
                              
                           </li>
                           
                           <li>Applicant must fill out a Criminal justice Institute Application.</li>
                           
                        </ul>
                        
                        <p> <strong>Note: </strong>Most of the recruits attending Valencia's Corrections Academy are employeed by Orange
                           County Corrections Department (OCCD). For hired employees, tuition and salaries are
                           paid by OCCD during attendence at the academy. <a href="http://www.ocfl.net/JailInmateServices/CorrectionsCareers.aspx#.VorBKE_G9pU">Cick here for information to APPLY at Orange County Corrections</a>.
                        </p>
                        
                        <p><strong>Students will be admitted into the Academy in the following 
                              order of priority. </strong></p>
                        
                        <ul>
                           
                           <li>Applicants who are employed by any agency which is a member 
                              of the C.J.I. Advisory Board.<br>
                              
                           </li>
                           
                           <li>Applicants who are employed by any other agency in the state.<br>
                              
                           </li>
                           
                           <li>Applicants who are sponsored by any agency. </li>
                           
                           <li>
                              
                           </li>
                           <li>Valencia AA/AS students or US Military Veterans. </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>All other qualified pre-service applicants.<br>
                              
                           </li>
                           
                           <li>Upon completion of the Academy, each student will be required 
                              to take and pass a state examination prior to being certified 
                              in the State of Florida. This exam consists of five sections, 
                              which requires a passing score of 80%.
                           </li>
                           
                        </ul>
                        
                        <p><strong>For further information call (407) 582-8200</strong></p>
                        
                        <p><a href="#top">TOP</a> 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/criminal-justice-institute/basic_correctional.pcf">©</a>
      </div>
   </body>
</html>