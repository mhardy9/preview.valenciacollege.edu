<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>School of Public Safety | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/public-safety/login.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>School of Public Safety</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/public-safety/">Public Safety</a></li>
               <li>School of Public Safety</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        
                        <h3>PSLDCP Secure Login Form</h3>
                        
                        
                        <form action="login.html" method="post" name="frmlogin" id="frmlogin">
                           <input name="processing" type="hidden" value="1">
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       Username&nbsp; 
                                    </div> 
                                    
                                    <div data-old-tag="td">
                                       
                                       <noscript>&amp;lt;input type="text" name="UserName" id="focusfield" size="50" maxlength="50"&amp;gt;</noscript>
                                       <input maxlength="50" name="UserName" onkeydown="if(event.keyCode==13) document.frmlogin.UserPass.focus();" size="50" type="text">
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">   
                                    
                                    <div data-old-tag="td">Password&nbsp;</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    
                                    <div data-old-tag="td">
                                       <a href="javascript:frmlogin.submit();">Log In</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </form> 
                        
                        
                        <p><strong>NOTE:</strong> This login page is for secure Class or Group access only.<br> If you are looking for the CJI Online Registration login page, <a href="https://secure.valenciacollege.edu/cjisignup2/index.cfm?fuseaction=main.login" title="Click Here To Access the Valencia Criminal Justice Institute Online Course Registration System.">click here.</a>
                           
                        </p>
                        
                        
                        <hr>
                        
                        
                        
                        
                        <hr>  
                        
                        <h2>For Criminal Justice Agency Use ONLY</h2>
                        
                        <h2>(ONLY Criminal Justice Agencies may request training room space at CJI.)</h2>
                        
                        <h5>
                           <strong>College Employees</strong> may schedule classrooms through ATLAS (Employees Tab, Room Request Forms) 
                        </h5>
                        
                        <h3><a href="http://net4.valenciacollege.edu/forms/public-safety/room-request.cfm" target="_blank">CJI Room Request (for Criminal Justice Agency use only)</a></h3>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/public-safety/room-request-cancellation.cfm" target="_blank">Room Request Cancellation Form</a></p>
                        
                        
                        <hr>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/public-safety/login.pcf">©</a>
      </div>
   </body>
</html>