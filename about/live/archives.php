<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Archives | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/live/archives.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/live/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Live</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/live/">Valencia Live</a></li>
               <li>Archives</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     	  
                     		          
                     <h2>Archives</h2>
                     
                     <p>Below you can browse a complete list of of our video archive from past Live Events.</p>
                     	  
                     <ul>
                        		  
                        
                        <li><a href="https://livestream.com/accounts/4834874/events/6167070/">Academic Assembly 2016</a></li>
                        
                        <li><a href="https://livestream.com/accounts/4834874/events/5596456/">Open Forum - VP Student Affairs - Kim Sepich - Event  End Date - Jun. 20, 2016</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/5596512">Open Forum - VP Student Affairs -Del Rosario Cruz - Event  End Date - Jun. 21, 2016</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/5610792">Valencia Orlando United Vigil  End Date - Jun. 15, 2016</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4837746">PAC Will Haygood - Event  End Date - Feb. 17, 2016</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4749829">WMFE Speaker Series  - Event End Date - Feb. 2, 2016</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4551879">Empowerment Symposium  - Event  End Date - Dec. 4, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4425012">2 Dr Temple Grandin - Event  End Date - Nov. 6, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4423020">1 Dr Temple Grandin - Event  End Date - Nov. 5, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4466058">Day 2 - Conversations on Peace - UBUNTU - Event  End Date - Nov. 3, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4466060">Day 1 - Conversations on Peace - UBUNTU - Event  End Date - Nov. 2, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4321122">A Tribute to Ralph Clemente - Event  End Date - Sept. 20, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4213054">Academic Assembly - Event  End Date - Aug. 27, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4230632">2-Bridges to Success Award Ceremony - West Campus - Event  End Date - Aug. 6, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4230616">1-Bridges to Success Award Ceremony - East &amp; Osceola - Event  End Date - Aug. 5, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3824207">Wayne Henderson (Appalachian) - Event  End Date - Mar. 5, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3824202">Matt Palmer (classical) - Event  End Date - Mar. 3, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3824190">Adam Rafferty (steel string) - Event  End Date - Feb. 27, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3824185">Tribute to Jorge Morel (Latin) - Event  End Date - Feb. 25, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/4834874">Dr. Julian Chambliss - Black Panther - Event  End Date - Feb. 10, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3760598">Francesco Attesti - Event  End Date - Feb. 4, 2015</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3521589">Dr. Michael Shermer - Event  End Date - Oct. 28, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3336195">SAGE Student Ambassador Medieval Restoration Presentation - Event  End Date - Sept.
                              3, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3351441">Education Student Orientation - Event  End Date - Sept. 3, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3360670">Academic Assembly 2014 - Event  End Date - Aug. 21, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3214871">Bridges to Success Graduation - Event  End Date - Jul. 30, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/3152799">Summer STEM Institute Orientation - Event  End Date - Jul. 21, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2840555">David Pogue - West - Event  End Date - Mar. 26, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2835100">David Pogue - East - Event  End Date - Mar. 25, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2764169">Business and Accounting Conference OSC - Event  End Date - Feb. 12, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2682550">Valencia Presents: Dr. Harold Kroto - Event  End Date - Jan. 30, 2014</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2547020">Fall Nursing Pinning - Event  End Date - Dec. 12, 2013</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2544095">East Campus Town Hall Meeting - Event  End Date - Nov. 21, 2013</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2505237">Yoani Sanchez - Event  End Date - Oct. 31, 2013</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2507165">James McBride - Event  End Date - Oct. 30, 2013</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2388060">Help Build a Peaceful World by Seeing the Other as Yourself - Event  End Date - Sept.
                              19, 2013</a></li>
                        		  
                        <li><a href="https://livestream.com/accounts/4834874/events/2328626">Academic Assembly 2013 - Event  End Date - Aug. 22, 2013</a></li>
                        	  
                     </ul>
                     		
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/live/archives.pcf">©</a>
      </div><script type="text/javascript" data-embed_id="ls_embed_1471441908" src="//livestream.com/assets/plugins/referrer_tracking.js"></script></body>
</html>