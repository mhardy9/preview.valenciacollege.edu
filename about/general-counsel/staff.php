<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Meet the Staff  | Valencia College</title>
      <meta name="Description" content="Meet the Staff of Office of Policy and General Counsel">
      <meta name="Keywords" content="college, school, educational, general, counsel, policy, procedure, staff">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/general-counsel/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/general-counsel/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/general-counsel/">Policy &amp; General Counsel</a></li>
               <li>Meet the Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Meet the Staff</h3>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           <img src="/_resources/img/about/general-counsel/bill-mullowney.jpg" alt="Bill Mullowney" width="125" height="177" hspace="10" vspace="10" data-pin-nopin="true" align="left">
                           
                           <h3>Bill Mullowney</h3>
                           <em>- Vice President, Policy &amp; General Counsel</em>
                           
                           <p align="justify">William J. Mullowney serves as Vice President for Policy and General Counsel for Valencia
                              College in Orlando, Florida. He serves as Valencia's chief legal officer and as its
                              lobbyist and
                              legislative counsel to the executive and legislative branches of the State of Florida.
                              He also serves as
                              Program Chair for the biannual Community College Conference on Legal Issues. He earned
                              his B.B.A. degree
                              from the University of Miami School of Business, and his J.D. and LL.M. degrees from
                              the University of
                              Miami School of Law. Prior to his position at Valencia, Dr. Mullowney served as General
                              Counsel at
                              Whittier College in California and before that as University Ombudsman at the University
                              of Miami. While
                              at Whittier, Dr. Mullowney was an Adjunct Professor of Law at the Whittier Law School,
                              where he taught
                              classes in Sports Law and Higher Education Law. While at Miami, he launched the university's
                              Student Honor
                              Code and served as an Associate Faculty Master at the Hecht Residential College.
                           </p>
                           
                           <p></p>
                           
                           <p align="justify"><img src="/_resources/img/about/general-counsel/Education-Law.jpg" alt="Ed Law Cert" width="109" height="133" hspace="10" vspace="10" align="right">Dr. Mullowney is a member of the
                              Florida Bar and the State Bar of California. He is Florida Bar Board Certified as
                              a Specialist in
                              Education Law. He is a member of the Florida Association of Professional Lobbyists,
                              and is certified as a
                              Designated Professional Lobbyist.He has completed three elected terms of service on
                              the Board of Directors
                              of the National Association of College and University Attorneys (NACUA), serving as
                              a member-at-large,
                              then as Secretary, and in his most recent term, he served as the Chair of the Board.
                              He also has served on
                              the Boards of the Florida Colleges Activities Association, the Association of Florida
                              Colleges (AFC), and
                              the AFC Foundation. He also serves on the Florida Bar's Education Law Committee and
                              the American
                              Association of Community College's Legal Advisory Group. He has spoken on issues regarding
                              higher
                              education law at numerous panel discussions and conferences presented by NACUA, the
                              Association of
                              Governing Boards, the American Council on Education, College and University Personnel
                              Administrators-HR,
                              Stetson University Conference on Law and Higher Education, National Association of
                              Student Personnel
                              Administrators, Southern Association of College and University Business Officers,
                              American Association of
                              Community Colleges, Association of Community College Trustees, National Association
                              of Directors of
                              Athletics, International Association of Campus Law Enforcement Administrators, and
                              AFC.
                           </p>
                           
                           
                           <div align="justify">
                              <img src="/_resources/img/about/general-counsel/Leslie-Bissinger-Golden.jpg" alt="Leslie Bissinger Golden" width="125" height="177" hspace="10" vspace="10" border="0" align="left">
                              
                              <h3>Leslie Bissinger Golden</h3>
                              <em>- Associate General Counsel, Office of Policy and General Counsel</em>
                              
                           </div>
                           
                           <p>Leslie Bissinger Golden serves as Associate General Counsel for Valencia College.
                              In this role, Ms.
                              Golden supports the Vice President for Policy and General Counsel by providing legal
                              advice and counseling
                              to the College, participating in the creation and enforcement of College policy, and
                              working with members
                              of the Valencia community to further the College’s interests.
                           </p>
                           
                           <p>Ms. Golden previously served as an administrator at another institution before coming
                              to Valencia.&nbsp; Prior
                              to that, Ms. Golden practiced with the firms of Baker &amp; Hostetler and Rumberger, Kirk
                              and Caldwell in
                              Orlando as a civil and commercial litigator.&nbsp; She received her undergraduate degree
                              in English from the
                              University of Florida and her law degree,&nbsp;<em>cum laude</em>, from Stetson University’s College of
                              Law.
                           </p>
                           
                           <p>Ms. Golden is a member of the Florida Bar and the National Association of College
                              and University
                              Attorneys. An Orlando native, her community involvement includes the Adult Literacy
                              League of Central
                              Florida and UCF Athletics.
                           </p>
                           
                           
                           <p><img src="/_resources/img/about/general-counsel/Courtney-James.jpg" width="132" height="176" alt="Courtney James" hspace="10" vspace="10" border="0" align="left"></p>
                           
                           <div align="justify">
                              
                              <h3>Courtney James</h3>
                              <em>- Paralegal, Office of Policy and General Counsel</em>
                              
                           </div>
                           
                           <p></p>
                           
                           <p>Courtney James serves as the Paralegal for the Vice President for Policy and General
                              Counsel at Valencia
                              College. In this role, Courtney assists the General Counsel and the associate General
                              Counsel with various
                              tasks such as; college contract administration, policy research and development, litigation
                              management,
                              and much more.
                           </p>
                           
                           <p>Courtney comes to Valencia College with an rich background in legal services. Her
                              paralegal experience
                              started in high school where she completed 2 years of training as a legal secretary.
                              Upon graduation, she
                              worked as a legal/administrative assistant for a regional government agency in Cleveland,
                              OH. She recently
                              moved to Orlando, Florida from Cincinnati, Ohio where she was employed with Crandall,
                              Pera and Wilt, LLC
                              as a skilled Litigation Paralegal.
                           </p>
                           
                           <p>Courtney received her undergraduate degree in Political Science from Bowling Green
                              State University
                              (Bowling Green, OH).
                           </p>
                           
                           <div><img src="/_resources/img/about/general-counsel/Solange-Fernndez.jpg" alt="Solange Del Pino" width="132" height="184" hspace="10" vspace="10" align="left"></div>
                           
                           <div align="justify">
                              
                              <h3>Solange Fernández del Pino</h3>
                              <em>- Executive Assistant, Office of Policy and General Counsel</em>
                              
                           </div>
                           
                           <p align="justify">Solange Fernández del Pino is the Executive Assistant to the Vice President for Policy
                              and General Counsel at Valencia College in Orlando.&nbsp;
                           </p>
                           
                           <p align="justify">Ms. Del Pino comes to Valencia with a wealth of executive office administration.&nbsp;
                              Prior
                              to joining Valencia, Ms. Del Pino was employed at Starwood Vacation Ownership, &nbsp;the
                              University of Central
                              Florida in Orlando, and Embry-Riddle Aeronautical University Extended Campus in Kaiserslautern,
                              Germany.&nbsp;
                              She also worked for the United States Air Force in Spain as a contracted administrator,
                              providing clerical
                              services at the military base Aerial Port and Communications Squadrons.&nbsp;
                           </p>
                           
                           <p align="justify">Born and raised in Madrid, Spain, she lived in Palaiseau, France as a young child.&nbsp;
                              Ms.
                              del Pino is fluent in Spanish and French.
                           </p>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/general-counsel/staff.pcf">©</a>
      </div>
   </body>
</html>