
<ul>
	<li><a href="index.php">
		Office of Policy and General Counsel
		</a></li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="policy/">Policies &amp; Procedures</a></li>
			<li><a href="policy/archives.php">Historical Archives</a></li>
			<li><a href="people.php">Meet the Staff</a></li>
			<li><a href="scope.php">Role and Scope</a></li>
			<li><a href="links.php">Links and Resources</a></li>
			<li><a href="spotlight-archives.php">Spotlight Archives</a></li>
			<li><a href="session-reports.php">Session Report Archives</a></li>
			<li><a href="http://net4.valenciacollege.edu/forms/generalcounsel/policy/contact.cfm" target="_blank">Contact Us</a></li>
		</ul>
	</li>
</ul>