<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Historical Archives | Valencia College</title>
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/general-counsel/policy/archives.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/general-counsel/policy/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Office of Policy and General Counsel</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/general-counsel/">Policy &amp; General Counsel</a></li>
               <li><a href="/about/general-counsel/policy/">Policy &amp; Procedures</a></li>
               <li>Historical Archives</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Historical Archives</h2>
                        
                        <p>These documents are for historical purposes only. They have either been updated or
                           discontinued. <strong>They in no way reflect current policies or procedures</strong>.
                        </p>
                        
                        <h3>Volume 1 - Governance</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume1/1-01-Organization-Authority-and-Location-Policy-11-20-2001.pdf" target="_blank">1 01 Organization Authority and Location Policy 11 20 2001</a></li>
                           
                           <li><a href="historical/Volume1/1-04-Rules-of-Procedures-for-Meetings-of-the-District-Board-of-Trustees-Policy-11-20-20021.pdf" target="_blank">1 04 Rules of Procedures for Meetings of the District Board of Trustees Policy 11
                                 20 20021</a></li>
                           
                           <li><a href="historical/Volume1/1-07-Faculty-Association-Policy-11-18-1992.pdf" target="_blank">1 07 Faculty Association Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume1/1-09-Policy-Development-and-Review-01-3-2017.pdf" target="_blank">1 09 Policy Development and Review 01 3 2017</a></li>
                           
                           <li><a href="historical/Volume1/1-10-Policy-Against-Improper-Activities-Whistleblower-Protection-06-16-2009.pdf" target="_blank">1 10 Policy Against Improper Activities Whistleblower Protection 06 16 2009</a></li>
                           
                           <li><a href="historical/Volume1/1-12-Discrimination-and-Sexual-Harassment-Prohibited-Policy-11-20-2001.pdf" target="_blank">1 12 Discrimination and Sexual Harassment Prohibited Policy 11 20 2001</a></li>
                           
                        </ul>
                        
                        <h3>Volume 2 - Nondiscrimination and Equal Opportunity</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct-09-22-2016.pdf" target="_blank">2 01 Discrimination Harassment and Related Conduct 09 22 2016</a></li>
                           
                           <li><a href="historical/Volume2/2-01-Nondiscrimination-and-Equal-Opportunity-Policy-12-18-2012.pdf" target="_blank">2 01 Nondiscrimination and Equal Opportunity Policy 12 18 2012</a></li>
                           
                           <li><a href="historical/Volume2/2-01-Nondiscrimination-and-Equal-Opportunity-Policy-12-21-2004.pdf" target="_blank">2 01 Nondiscrimination and Equal Opportunity Policy 12 21 2004</a></li>
                           
                           <li><a href="historical/Volume2/2-02-Harassment-Sexual-Harassment-Policy-12-18-2012.pdf" target="_blank">2 02 Harassment Sexual Harassment Policy 12 18 2012</a></li>
                           
                           <li><a href="historical/Volume2/2-03-Investigating-and-Resolving-Discrimination-Harrasment-and-Sexual-Harassment-Complaints-Policy-12-18-2012.pdf" target="_blank">2 03 Investigating and Resolving Discrimination Harrasment and Sexual Harassment Complaints
                                 Policy 12 18 2012</a></li>
                           
                           <li><a href="historical/Volume2/2-03-Investigating-and-Resolving-Discrimination-Harrasment-and-Sexual-Harassment-Complaints-Procedure-12-18-2012.pdf" target="_blank">2 03 Investigating and Resolving Discrimination Harrasment and Sexual Harassment Complaints
                                 Procedure 12 18 2012</a></li>
                           
                           <li><a href="historical/Volume2/2-04-AIDS-Policy-12-21-2004.pdf" target="_blank">2 04 AIDS Policy 12 21 2004</a></li>
                           
                           <li><a href="historical/Volume2/2-07-Substitute-Admission-and-Graduation-Requirements-for-Students-with-Disabilities-Policy-12-21-2004.pdf" target="_blank">2 07 Substitute Admission and Graduation Requirements for Students with Disabilities
                                 Policy 12 21 2004</a></li>
                           
                           <li><a href="historical/Volume2/2-07-Substitute-Admission-and-Graduation-Requirements-for-Students-with-Disabilities-Procedure--02-25-2015.pdf" target="_blank">2 07 Substitute Admission and Graduation Requirements for Students with Disabilities
                                 Procedure  02 25 2015</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3A - Human Resources, General Employment</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume3A/3A-01-Job-Description-for-Personnel-of-the-College%20-%2011-18-1992.pdf" target="_blank">3A 01 Job Description for Personnel of the College   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-02.1-Def-of-Admin-Inst-and-Prof-Employees%20-%2011-18-1992.pdf" target="_blank">3A 02.1 Def of Admin Inst and Prof Employees   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-02.2-Definition-of-Full-Time-Employment-Instructional-and-Admin-Employees%20-%2011-18-1992.pdf" target="_blank">3A 02.2 Definition of Full Time Employment Instructional and Admin Employees   11
                                 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-02.3-Definition-of-a-Career-Service-Employee%20-%2011-18-1992.pdf" target="_blank">3A 02.3 Definition of a Career Service Employee   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-02.4-Definition-of-Full-Time-Employment-for-Career-%2011-18-1992.pdf" target="_blank">3A 02.4 Definition of Full Time Employment for Career  11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-03-Classification-of-Career-Service-Positions%20-%2011-18-1992.pdf" target="_blank">3A 03 Classification of Career Service Positions   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-04.1-Part-Time-Personnel%20-%2011-18-1992.pdf" target="_blank">3A 04.1 Part Time Personnel   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-04.2-Part-Time-Instructional-Personnel%20-%2011-18-1992.pdf" target="_blank">3A 04.2 Part Time Instructional Personnel   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3A-05-Substitute-Professors-Procedure-11-18-1992.pdf" target="_blank">3A 05 Substitute Professors Procedure 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3A/3C-02.1-Hours-of-Work-for-Career-Service-Employees%20-%2011-18-1992.pdf" target="_blank">3C 02.1 Hours of Work for Career Service Employees   11 18 1992</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3B - Human Resources, Recruitment &amp; Selection</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume3B/3B-01-Human-Resources-Actions-Policy-11-18-1992.pdf" target="_blank">3B 01 Human Resources Actions Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3B/3B-01-Human-Resources-Actions-Procedure-11-18-1992.pdf" target="_blank">3B 01 Human Resources Actions Procedure 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3B/3B-02-Inprocessing-of-New-Employees-Policy-11-18-1992.pdf" target="_blank">3B 02 Inprocessing of New Employees Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3B/3B-02-Inprocessing-of-New-Employees-Procedure-11-18-1992.pdf" target="_blank">3B 02 Inprocessing of New Employees Procedure 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3B/3B-02-Pre-Employment-Screening-and-Inprocessing-of-New-Employees-Procedure-7-25-2017.pdf" target="_blank">3B 02 Pre Employment Screening and Inprocessing of New Employees Procedure 7 25 2017</a></li>
                           
                           <li><a href="historical/Volume3B/3B-03-Pre-Employment-Drug-Testing-Policy-08-18-1993.pdf" target="_blank">3B 03 Pre Employment Drug Testing Policy 08 18 1993</a></li>
                           
                           <li><a href="historical/Volume3B/3B-03-Pre-Employment-Drug-Testing-Procedure--08-18-1993.pdf" target="_blank">3B 03 Pre Employment Drug Testing Procedure  08 18 1993</a></li>
                           
                           <li><a href="historical/Volume3B/3B-03-Recruitment-and-Selection-09-27-2017.pdf" target="_blank">3B 03 Recruitment and Selection 09 27 2017</a></li>
                           
                           <li><a href="historical/Volume3B/3B-03-Recruitment-and-Selection-of-Employees--Procedure-05-28-2014.pdf" target="_blank">3B 03 Recruitment and Selection of Employees  Procedure 05 28 2014</a></li>
                           
                           <li><a href="historical/Volume3B/3B-04.1-Recruitment-of-Admin-and-Inst-Employees.pdf" target="_blank">3B 04.1 Recruitment of Admin and Inst Employees</a></li>
                           
                           <li><a href="historical/Volume3B/3B-04.2-Recruitment-of-Career-Service-Employees.pdf" target="_blank">3B 04.2 Recruitment of Career Service Employees</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3C - Human Resources, Hours of Work, Compensation, Retirement and Benefits</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume3C/3C-01.1-Hours-of-Work-for-Instructional-and-Administrative-Employees%20-%2011-18-1992.pdf" target="_blank">3C 01.1 Hours of Work for Instructional and Administrative Employees   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3C/3C-01.2-Assignment-of-Classroom-and-Laboratory-Professors-Procedure-07-16-2013.pdf" target="_blank">3C 01.2 Assignment of Classroom and Laboratory Professors Procedure 07 16 2013</a></li>
                           
                           <li><a href="historical/Volume3C/3C-02.2-Recording-of-Hours-Worked-for-Career-Service%20-%2011-18-1992.pdf" target="_blank">3C 02.2 Recording of Hours Worked for Career Service   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3C/3C-02.3-Overtime-Compensation-for-Career-Service-Employees%20-%2005-15-2012.pdf" target="_blank">3C 02.3 Overtime Compensation for Career Service Employees   05 15 2012</a></li>
                           
                           <li><a href="historical/Volume3C/3C-02.3-Overtime-Compensation-for-Career-Service-Employees-Policy-05-15-2001.pdf" target="_blank">3C 02.3 Overtime Compensation for Career Service Employees Policy 05 15 2001</a></li>
                           
                           <li><a href="historical/Volume3C/3C-02.3-Overtime-Compensation-for-Career-Service-Employees-Procedure-05-15-2001.pdf" target="_blank">3C 02.3 Overtime Compensation for Career Service Employees Procedure 05 15 2001</a></li>
                           
                           <li><a href="historical/Volume3C/3C-02.4-Night-Shift-Differential%20-%2011-18-1992.pdf" target="_blank">3C 02.4 Night Shift Differential   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3C/3C-02.5-Payment-to-Career-Service-Employees-for-Holidays-Worked%20-%2011-18-1992.pdf" target="_blank">3C 02.5 Payment to Career Service Employees for Holidays Worked   11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3C/3C-03.1-Payroll-Deduction-Authorization%20-%2001-21-1998.pdf" target="_blank">3C 03.1 Payroll Deduction Authorization   01 21 1998</a></li>
                           
                           <li><a href="historical/Volume3C/3C-03.2-Payroll-Deductions-for-Dues-for-Certain-Qualifying-Employee-Organizations%20-%2011-18-1992.pdf" target="_blank">3C 03.2 Payroll Deductions for Dues for Certain Qualifying Employee Organizations
                                 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3C/3C-04-Total-Rewards-Recognition-Full-Time-Employees-Educational-Advancement%20-%2003-23-2017.pdf" target="_blank">3C 04 Total Rewards Recognition Full Time Employees Educational Advancement   03 23
                                 2017</a></li>
                           
                           <li><a href="historical/Volume3C/3C-04.1-Salary-Schedule%20-%2009-20-1995.pdf" target="_blank">3C 04.1 Salary Schedule   09 20 1995</a></li>
                           
                           <li><a href="historical/Volume3C/3C-04.3.1-Pay-of-Instructional-Personnel-Receiving-Advanced-College-Credit%20-04-16-1997.pdf" target="_blank">3C 04.3.1 Pay of Instructional Personnel Receiving Advanced College Credit  04 16
                                 1997</a></li>
                           
                           <li><a href="historical/Volume3C/3C-04.3.1-Pay-of-Instructional-Personnel-Receiving-Advanced-College-Credit.pdf" target="_blank">3C 04.3.1 Pay of Instructional Personnel Receiving Advanced College Credit</a></li>
                           
                           <li><a href="historical/Volume3C/3C-04.3.2-Pay-of-Professional-and-Administrative-Personnel-for-Advanced-College-Credit%20-%2001-20-1999.pdf" target="_blank">3C 04.3.2 Pay of Professional and Administrative Personnel for Advanced College Credit
                                 01 20 1999</a></li>
                           
                           <li><a href="historical/Volume3C/3C-04.3.2-Pay-of-Professional-and-Administrative-Personnel.pdf" target="_blank">3C 04.3.2 Pay of Professional and Administrative Personnel</a></li>
                           
                           <li><a href="historical/Volume3C/3C-05-Transfer-from-Temporary-Grant-Funded-Position-to-a-College-Funded-Position%20-%2011-18-1992.pdf" target="_blank">3C 05 Transfer from Temporary Grant Funded Position to a College Funded Position 
                                 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3C/3C-06.1-Contracts-for-Administrative-Professional-and-Instructional-Personnel.pdf" target="_blank">3C 06.1 Contracts for Administrative Professional and Instructional Personnel</a></li>
                           
                           <li><a href="historical/Volume3C/3C-06.2-Supplemental-and-Overload-Contracts.pdf" target="_blank">3C 06.2 Supplemental and Overload Contracts</a></li>
                           
                           <li><a href="historical/Volume3C/3C-08-Insurance-Comprehensive-Medical-and-Life-Program.pdf" target="_blank">3C 08 Insurance Comprehensive Medical and Life Program</a></li>
                           
                           <li><a href="historical/Volume3C/3C-11-Retirement-Contributions-Policy-05-15-2001.pdf" target="_blank">3C 11 Retirement Contributions Policy 05 15 2001</a></li>
                           
                           <li><a href="historical/Volume3C/3C-11-Retirement-Contributions-Procedure-05-15-2001.pdf" target="_blank">3C 11 Retirement Contributions Procedure 05 15 2001</a></li>
                           
                           <li><a href="historical/Volume3C/3C-12-NF-NN-Retirement-Incentive-Program-Mar-17-1999.rtf" target="_blank">3C 12 NF NN Retirement Incentive Program Mar 17 1999</a></li>
                           
                           <li><a href="historical/Volume3C/3C-12-Retirement-Incentive-Program-Policy-and-Procedure-03-17-1999.pdf" target="_blank">3C 12 Retirement Incentive Program Policy and Procedure 03 17 1999</a></li>
                           
                           <li><a href="historical/Volume3C/3C-12-Retirement-Incentive-Programs-Procedure-09-12-2015.pdf" target="_blank">3C 12 Retirement Incentive Programs Procedure 09 12 2015</a></li>
                           
                           <li><a href="historical/Volume3C/3C-13-Optional-Retirement-Program.pdf" target="_blank">3C 13 Optional Retirement Program</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3D - Human Resources, Leave</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume3D/3D-01-Holidays-and-Other-Paid-Non-Duty-Days-10-22-2002.pdf" target="_blank">3D 01 Holidays and Other Paid Non Duty Days 10 22 2002</a></li>
                           
                           <li><a href="historical/Volume3D/3D-01-Paid%20Non-Duty-Days%20-%2004-16-2013.pdf" target="_blank">3D 01 Paid Non Duty Days   04 16 2013</a></li>
                           
                           <li><a href="historical/Volume3D/3D-02-Leave-of-Absence-Policy-03-22-1995.pdf" target="_blank">3D 02 Leave of Absence Policy 03 22 1995</a></li>
                           
                           <li><a href="historical/Volume3D/3D-03-Vacation-Leave-12-14-2010.pdf" target="_blank">3D 03 Vacation Leave 12 14 2010</a></li>
                           
                           <li><a href="historical/Volume3D/3D-04-Sick-Leave-12-10-2002.pdf" target="_blank">3D 04 Sick Leave 12 10 2002</a></li>
                           
                           <li><a href="historical/Volume3D/3D-05-Sick-Leave-Pool-09-27-2017.pdf" target="_blank">3D 05 Sick Leave Pool 09 27 2017</a></li>
                           
                           <li><a href="historical/Volume3D/3D-06.1-Family-Medical-Leave-09-27-2017.pdf" target="_blank">3D 06.1 Family Medical Leave 09 27 2017</a></li>
                           
                           <li><a href="historical/Volume3D/3D-06.1.1-Parenting-Leave-Procedure-10-22-2014.pdf" target="_blank">3D 06.1.1 Parenting Leave Procedure 10 22 2014</a></li>
                           
                           <li><a href="historical/Volume3D/3D-06.2-Medical-Examinations-04-20-1994.pdf" target="_blank">3D 06.2 Medical Examinations 04 20 1994</a></li>
                           
                           <li><a href="historical/Volume3D/3D-06.3-Illness-in-Line-of-Duty-Leave-Policy-11-18-1992.pdf" target="_blank">3D 06.3 Illness in Line of Duty Leave Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3D/3D-07.1-Leave-for-Personal-Reasons-Policy-11-18-1992.pdf" target="_blank">3D 07.1 Leave for Personal Reasons Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3D/3D-08-Partial-Leave-09-27-2017.pdf" target="_blank">3D 08 Partial Leave 09 27 2017</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3E - Human Resources, Standards for Performance and Conduct; Evaluation, Disciplinary
                           Actions; Dispute Resolution Procedures
                        </h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume3E/3E-01-Career-Service-Employee-Performance-Eval.pdf" target="_blank">3E 01 Career Service Employee Performance Eval</a></li>
                           
                           <li><a href="historical/Volume3E/3E-01-Career-Service-Employee-Performance-Evaluations--Policy-11-18-1992.pdf" target="_blank">3E 01 Career Service Employee Performance Evaluations  Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3E/3E-01-Career-Service-Employee-Performance-Evaluations--Procedure-11-18-1992.pdf" target="_blank">3E 01 Career Service Employee Performance Evaluations  Procedure 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3E/3E-01-Full-Time-%20Personnel%20Perf.%20Evaluations%20-%2006-15-2012.pdf" target="_blank">3E 01 Full Time  Personnel Perf. Evaluations   06 15 2012</a></li>
                           
                           <li><a href="historical/Volume3E/3E-02-Award-of-Tenure-and-Evaluation-of-Tenured-and-Tenure-Track-Faculty-Policy--07-16-2013.pdf" target="_blank">3E 02 Award of Tenure and Evaluation of Tenured and Tenure Track Faculty Policy  07
                                 16 2013</a></li>
                           
                           <li><a href="historical/Volume3E/3E-02-Award-of-Tenure-and-Evaluation-of-Tenured-and-Tenure-Track-Faculty-Procedure-07-16-2013.pdf" target="_blank">3E 02 Award of Tenure and Evaluation of Tenured and Tenure Track Faculty Procedure
                                 07 16 2013</a></li>
                           
                           <li><a href="historical/Volume3E/3E-02-Award-of-Tenure-and-Evaluation-of-Tenured-and-Tenured-Track-Faculty-Procedure-08-8-2017.pdf" target="_blank">3E 02 Award of Tenure and Evaluation of Tenured and Tenured Track Faculty Procedure
                                 08 8 2017</a></li>
                           
                           <li><a href="historical/Volume3E/3E-05.1-Reasons-for-Dismissal.pdf" target="_blank">3E 05.1 Reasons for Dismissal</a></li>
                           
                           <li><a href="historical/Volume3E/3E-05.2-Code-of-Ethics-for-Public-Employees-Policy-11-18-1992.pdf" target="_blank">3E 05.2 Code of Ethics for Public Employees Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3E/3E-05.2-Ethical-Conduct-and-Performance-02-26-2014.pdf" target="_blank">3E 05.2 Ethical Conduct and Performance 02 26 2014</a></li>
                           
                           <li><a href="historical/Volume3E/3E-05.5-Tutoring.pdf" target="_blank">3E 05.5 Tutoring</a></li>
                           
                           <li><a href="historical/Volume3E/3E-08-Disciplinary-Action-Policy-11-18-1992.pdf" target="_blank">3E 08 Disciplinary Action Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3E/3E-08-Disciplinary-Action-Policy-and-Procedure-11-18-1992.pdf" target="_blank">3E 08 Disciplinary Action Policy and Procedure 11 18 1992</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3F - Human Resources, Separation and Termination from Employment</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume3F/3F-02-Terminal-Pay-Nov-2-2016.pdf" target="_blank">3F 02 Terminal Pay Nov 2 2016</a></li>
                           
                           <li><a href="historical/Volume3F/3F-03-Suspension-Dismissal-or-Non-Renewal-of-Admin.-Professional-or-Instructional-Employees-Policy-11-18-1992.pdf" target="_blank">3F 03 Suspension Dismissal or Non Renewal of Admin. Professional or Instructional
                                 Employees Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume3F/3F-03-Suspension-Dismissal-Return-to-Annual-Contract-or-Non-Renewal-of-Contracts.pdf" target="_blank">3F 03 Suspension Dismissal Return to Annual Contract or Non Renewal of Contracts</a></li>
                           
                        </ul>
                        
                        <h3>Volume 4 - Curriculum and Instruction</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume4/4-09-Instructional-Materials-Policy-and-Procedure-11-18-1992.pdf" target="_blank">4 09 Instructional Materials Policy and Procedure 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume4/4-09-Instructional-Materials-Procedure-09-17-2013.pdf" target="_blank">4 09 Instructional Materials Procedure 09 17 2013</a></li>
                           
                           <li><a href="historical/Volume4/4-09-Instructional-Materials.pdf" target="_blank">4 09 Instructional Materials</a></li>
                           
                           <li><a href="historical/Volume4/4-09-Textbook-Selection-Policy-11-18-1992.pdf" target="_blank">4 09 Textbook Selection Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume4/4-38-Information%20Technology%20Resources.pdf" target="_blank">4 38 Information Technology Resources</a></li>
                           
                        </ul>
                        
                        <h3>Volume 5 - General Administrative</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume5/5-08-Travel-by-Authorized-Personnel-03-30-2009.pdf" target="_blank">5 08 Travel by Authorized Personnel 03 30 2009</a></li>
                           
                           <li><a href="historical/Volume5/5-08-Travel-by-Authorized-Personnel-Procedure-05-15-2007.pdf" target="_blank">5 08 Travel by Authorized Personnel Procedure 05 15 2007</a></li>
                           
                           <li><a href="historical/Volume5/5-12-Other-Insurance-College-Property-Liability-and-Workers-Compensation-Policy-11-18-1992.pdf" target="_blank">5 12 Other Insurance College Property Liability and Workers Compensation Policy 11
                                 18 1992</a></li>
                           
                           <li><a href="historical/Volume5/5-13-Facsimile-Signatures-Policy-02-15-2011.pdf" target="_blank">5 13 Facsimile Signatures Policy 02 15 2011</a></li>
                           
                        </ul>
                        
                        <h3>Volume 6 - Fiscal</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume6/6-01.1-Receipt-and-Deposit-of-Funds-Policy-11-18-1992.pdf" target="_blank">6 01.1 Receipt and Deposit of Funds Policy 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume6/6-01.1-Receipt-and-Deposit-of-Funds-Procedure-11-18-1992.pdf" target="_blank">6 01.1 Receipt and Deposit of Funds Procedure 11 18 1992</a></li>
                           
                           <li><a href="historical/Volume6/6-08.1-Student-Fees-and-Refunds-11-10-2017.pdf" target="_blank">6 08.1 Student Fees and Refunds 11 10 2017</a></li>
                           
                           <li><a href="historical/Volume6/6-13.1-Collection-of-Money-from-Students-Policy-11-18-1992.pdf" target="_blank">6 13.1 Collection of Money from Students Policy 11 18 1992</a></li>
                           
                        </ul>
                        
                        <h3>Volume 7A - Information Technology</h3>
                        
                        <h3>Volume 7B - Records Management</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume7B/7B-02-Student-Records-04-10-2017.pdf" target="_blank">7B 02 Student Records 04 10 2017</a></li>
                           
                        </ul>
                        
                        <h3>Volume 8 - Students</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume8/8-02-Admission-09-18-2012.pdf" target="_blank">8 02 Admission 09 18 2012</a></li>
                           
                           <li><a href="historical/Volume8/8-02-Admission-Policy-06-20-2012.pdf" target="_blank">8 02 Admission Policy 06 20 2012</a></li>
                           
                           <li><a href="historical/Volume8/8-06-Residency-12-20-2005.pdf" target="_blank">8 06 Residency 12 20 2005</a></li>
                           
                           <li><a href="historical/Volume8/8-10-Student-Academic-Dispute-and-Administrative-Complaint-Resolution-Policy-01-05-2009.pdf" target="_blank">8 10 Student Academic Dispute and Administrative Complaint Resolution Policy 01 05
                                 2009</a></li>
                           
                        </ul>
                        
                        <h3>Volume 9 - Advancement</h3>
                        
                        <h3>Volume 10 - Campus Safety and Security</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="historical/Volume10/10-04-Firearms-on-Campus.pdf" target="_blank">10 04 Firearms on Campus</a></li>
                           
                           <li><a href="historical/Volume10/10-05-Smoking-Regulations-Policy-09-19-2000.pdf" target="_blank">10 05 Smoking Regulations Policy 09 19 2000</a></li>
                           
                           <li><a href="historical/Volume10/10-05-Smoking-Regulations-Procedure-09-19-2000.pdf" target="_blank">10 05 Smoking Regulations Procedure 09 19 2000</a></li>
                           
                           <li><a href="historical/Volume10/10-06-Sexual-Assault-and-Other-Crimes-of-Violence-Policy-09-16-2008.pdf" target="_blank">10 06 Sexual Assault and Other Crimes of Violence Policy 09 16 2008</a></li>
                           
                           <li><a href="historical/Volume10/10-06-Sexual-Assault-and-Other-Crimes-of-Violence-Procedure-09-16-2008.pdf" target="_blank">10 06 Sexual Assault and Other Crimes of Violence Procedure 09 16 2008</a></li>
                           
                        </ul>
                        
                        <h3>Volume 11 - Campus Facilities</h3>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/general-counsel/policy/archives.pcf">©</a>
      </div>
   </body>
</html>