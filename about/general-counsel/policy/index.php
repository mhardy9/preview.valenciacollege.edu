<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Policies and Procedures  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/general-counsel/policy/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/general-counsel/policy/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Office of Policy and General Counsel</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/general-counsel/">Policy &amp; General Counsel</a></li>
               <li>Policy &amp; Procedures</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Policies and Procedures</h2>
                        
                        <p>Welcome to the Valencia College Policies and Procedures web site. All Board approved
                           policies and related procedures can be viewed here. You may search policies and procedures
                           for specific words or terms by using the search box. Each policy is in PDF format
                           for easy printing, saving and viewing.
                        </p>
                        
                        <h3>Volume 1 - Governance</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-01-Organization-Authority-and-Location.pdf" target="_blank">1 01 Organization Authority and Location</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-02-General.pdf" target="_blank">1 02 General</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-03-Meetings-of-the-District-Board-of-Trustees.pdf" target="_blank">1 03 Meetings of the District Board of Trustees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-04-Rules-of-Procedures-for-Meetings-of-the-District-Board-of-Trustees.pdf" target="_blank">1 04 Rules of Procedures for Meetings of the District Board of Trustees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-05-Officers-and-Their-Duties.pdf" target="_blank">1 05 Officers and Their Duties</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-06-Powers-and-Duties-of-the-President-of-the-College.pdf" target="_blank">1 06 Powers and Duties of the President of the College</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-07-Faculty-Association.pdf" target="_blank">1 07 Faculty Association</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-08-College-Accountability-Process.pdf" target="_blank">1 08 College Accountability Process</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-09-Policy-Development-and-Review.pdf" target="_blank">1 09 Policy Development and Review</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-10-Policy-Against-Improper-Activities-Whistleblower-Protection.pdf" target="_blank">1 10 Policy Against Improper Activities Whistleblower Protection</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-11-Indemnification.pdf" target="_blank">1 11 Indemnification</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume1/1-13-Review-and-Amendment-of-Governance-Policies.pdf" target="_blank">1 13 Review and Amendment of Governance Policies</a></li>
                           
                        </ul>
                        
                        <h3>Volume 2 - Nondiscrimination and Equal Opportunity</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">2 01 Discrimination Harassment and Related Conduct</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume2/2-04-AIDS.pdf" target="_blank">2 04 AIDS</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume2/2-05-Women-and-Minority-Business-Opportunities.pdf" target="_blank">2 05 Women and Minority Business Opportunities</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume2/2-06-Developing-Business-Program.pdf" target="_blank">2 06 Developing Business Program</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume2/2-07-Substitute-Admission-and-Graduation-Requirements-for-Students-with-Disabilities.pdf" target="_blank">2 07 Substitute Admission and Graduation Requirements for Students with Disabilities</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume2/2-08-Accommodation-of-Religious-Observances-by-Students.pdf" target="_blank">2 08 Accommodation of Religious Observances by Students</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3A - Human Resources, General Employment</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3A/3A-05-Substitute-Professors.pdf" target="_blank">3A 05 Substitute Professors</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3A/3A-06-Employee-and-Student-Exchanges.pdf" target="_blank">3A 06 Employee and Student Exchanges</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3A/3A-07-Temporary-Duty.pdf" target="_blank">3A 07 Temporary Duty</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3A/3A-08-Unpaid-Internships.pdf" target="_blank">3A 08 Unpaid Internships</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3A/3A-09-Volunteers.pdf" target="_blank">3A 09 Volunteers</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3B - Human Resources, Recruitment &amp; Selection</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3B/3B-01-Human-Resource-Actions.pdf" target="_blank">3B 01 Human Resource Actions</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3B/3B-02-Pre-Employment-Screening-and-Inprocessing-of-New-Employees.pdf" target="_blank">3B 02 Pre Employment Screening and Inprocessing of New Employees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3B/3B-03-Recruitment-Selection-and-Hiring-of-Employees.pdf" target="_blank">3B 03 Recruitment Selection and Hiring of Employees</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3C - Human Resources, Hours of Work, Compensation, Retirement and Benefits</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-01-Total-Rewards-Compensation-and-Hours-of-Work-for-Employees-of-the-College.pdf" target="_blank">3C 01 Total Rewards Compensation and Hours of Work for Employees of the College</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-01.2-Workload-for-Professors.pdf" target="_blank">3C 01.2 Workload for Professors</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-01.3-Councils-and-Committees.pdf" target="_blank">3C 01.3 Councils and Committees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-01.4-Commencement.pdf" target="_blank">3C 01.4 Commencement</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-01.5-Absence-of-a-Professor-from-Class.pdf" target="_blank">3C 01.5 Absence of a Professor from Class</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-04-Total-Rewards-Recognition-Full-Time-Employees-Educational-Advancement.pdf" target="_blank">3C 04 Total Rewards Recognition Full Time Employees Educational Advancement</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-04.2-Credit-for-Prior-Experience-and-Service-at-the-College-on-the-Instructional-Salary-Schedule.pdf" target="_blank">3C 04.2 Credit for Prior Experience and Service at the College on the Instructional
                                 Salary Schedule</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-06%201-Contracts-for-Instructional-Executive-Administrative-Employees.pdf" target="_blank">3C 06 1 Contracts for Instructional Executive Administrative Employees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-07-Staff-Paid-Through-Staff-and-Program-Development-Funds.pdf" target="_blank">3C 07 Staff Paid Through Staff and Program Development Funds</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-08-Total-Rewards-Employee-Benefits.pdf" target="_blank">3C 08 Total Rewards Employee Benefits</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-09-Waiver-of-Matriculation-or-Tuition-Fees-for-Spouses-and-Dependents-of-Faculty-and-Staff.pdf" target="_blank">3C 09 Waiver of Matriculation or Tuition Fees for Spouses and Dependents of Faculty
                                 and Staff</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-10-Time-of-Service-for-Work-Credit.pdf" target="_blank">3C 10 Time of Service for Work Credit</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-11-Retirement-Programs.pdf" target="_blank">3C 11 Retirement Programs</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-12-Retirement-Incentive-Programs.pdf" target="_blank">3C 12 Retirement Incentive Programs</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-14-401-a-Qualified-Retirement-Plan.pdf" target="_blank">3C 14 401 a Qualified Retirement Plan</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3C/3C-15-403-b-Qualified-Retirement-Plan.pdf" target="_blank">3C 15 403 b Qualified Retirement Plan</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3D - Human Resources, Leave</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-02-Leave-of-Absence.pdf" target="_blank">3D 02 Leave of Absence</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-03-Vacation-Leave.pdf" target="_blank">3D 03 Vacation Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-04-Sick-Leave.pdf" target="_blank">3D 04 Sick Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-05-Sick-Leave-Pool.pdf" target="_blank">3D 05 Sick Leave Pool</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-06.1-Family-Medical-Leave.pdf" target="_blank">3D 06.1 Family Medical Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-06.1.1Parenting-Leave.pdf" target="_blank">3D 06.1.1Parenting Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-06.2-Fitness-for-Duty.pdf" target="_blank">3D 06.2 Fitness for Duty</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-06.3-Illness-or-Injury-In-Line-Of-Duty-Leave.pdf" target="_blank">3D 06.3 Illness or Injury In Line Of Duty Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-07.1-Leave-for-Personal-Reasons.pdf" target="_blank">3D 07.1 Leave for Personal Reasons</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-07.2-Personal-Leave-Without-Pay.pdf" target="_blank">3D 07.2 Personal Leave Without Pay</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-08-Partial-Leave.pdf" target="_blank">3D 08 Partial Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-09-Court-Related-Leave.pdf" target="_blank">3D 09 Court Related Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-10-Military-Leave.pdf" target="_blank">3D 10 Military Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-11-Professional-Leave-and-Extended-Professional-Leave.pdf" target="_blank">3D 11 Professional Leave and Extended Professional Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-12-Sabbatical-Leave.pdf" target="_blank">3D 12 Sabbatical Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-13-Domestic-Violence-Leave.pdf" target="_blank">3D 13 Domestic Violence Leave</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3D/3D-14-Paid-Time-Off-for-PT-Faculty-Teaching-Acad-Credit-Programs.pdf" target="_blank">3D 14 Paid Time Off for PT Faculty Teaching Acad Credit Programs</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3E - Human Resources, Standards for Performance and Conduct; Evaluation, Disciplinary
                           Actions; Dispute Resolution Procedures
                        </h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-01-Full-Time-Employee-Performance-Evaluations.pdf" target="_blank">3E 01 Full Time Employee Performance Evaluations</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-02-Award-of-Tenure-and-Evaluation-of-Tenured-and-Tenured-Track-Faculty.pdf" target="_blank">3E 02 Award of Tenure and Evaluation of Tenured and Tenured Track Faculty</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-03-Issuance-of-Annual-Contracts-to-Instructional-Personnel.pdf" target="_blank">3E 03 Issuance of Annual Contracts to Instructional Personnel</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-04-Acceptance-by-All-Employees-of-the-Policies-of-the-College.pdf" target="_blank">3E 04 Acceptance by All Employees of the Policies of the College</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-05.2-Ethical-Conduct-and-Performance.pdf" target="_blank">3E 05.2 Ethical Conduct and Performance</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-05.3-Extra-College-Employment-and-Activities.pdf" target="_blank">3E 05.3 Extra College Employment and Activities</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-05.4-Acceptance-of-Gifts-Resulting-from-Purchaser.pdf" target="_blank">3E 05.4 Acceptance of Gifts Resulting from Purchaser</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-05.5-Tutoring.pdf" target="_blank">3E 05.5 Tutoring</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-06-Student-Loan-Code-of-Conduct.pdf" target="_blank">3E 06 Student Loan Code of Conduct</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-07-Personal-Financial-Obligations.pdf" target="_blank">3E 07 Personal Financial Obligations</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-08-Disciplinary-action.pdf" target="_blank">3E 08 Disciplinary action</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3E/3E-09-Employee-Dispute-Resolution.pdf" target="_blank">3E 09 Employee Dispute Resolution</a></li>
                           
                        </ul>
                        
                        <h3>Volume 3F - Human Resources, Separation and Termination from Employment</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3F/3F-01-Outprocessing-of-Employees.pdf" target="_blank">3F 01 Outprocessing of Employees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3F/3F-02-Terminal-Pay-for-Full-Time-Employees.pdf" target="_blank">3F 02 Terminal Pay for Full Time Employees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3F/3F-03-Suspension,-Dismissal,-Return-to-Annual-Contract,-or-Non-Renewal-of-Contracts.pdf" target="_blank">3F 03 Suspension, Dismissal, Return to Annual Contract, or Non Renewal of Contracts</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3F/3F-04-Reductions-in-Force-or-Consolidation-or-Reduction.pdf" target="_blank">3F 04 Reductions in Force or Consolidation or Reduction</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3F/3F-05.1-Resignations-of-Administrative-or-Instructional-Employees.pdf" target="_blank">3F 05.1 Resignations of Administrative or Instructional Employees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume3F/3F-05.2-Abandonment-of-Position.pdf" target="_blank">3F 05.2 Abandonment of Position</a></li>
                           
                        </ul>
                        
                        <h3>Volume 4 - Curriculum and Instruction</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-01-Accreditation.pdf" target="_blank">4 01 Accreditation</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-02-Academic-Freedom.pdf" target="_blank">4 02 Academic Freedom</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-03-Research-by-Faculty.pdf" target="_blank">4 03 Research by Faculty</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-04-Course-Competencies-and-Student-Outcomes.pdf" target="_blank">4 04 Course Competencies and Student Outcomes</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-05-Collegewide-Course-Outline-Syllabus.pdf" target="_blank">4 05 Collegewide Course Outline Syllabus</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-06-Award-of-Credit.pdf" target="_blank">4 06 Award of Credit</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-07-Academic-Progress-Course-Attendance-and-Grades-and-Withdrawals.pdf" target="_blank">4 07 Academic Progress Course Attendance and Grades and Withdrawals</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-09-Instructional-Materials.pdf" target="_blank">4 09 Instructional Materials</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-10-Materials-Required-of-Students.pdf" target="_blank">4 10 Materials Required of Students</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-12-Speakers-or-Guests.pdf" target="_blank">4 12 Speakers or Guests</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-13-Field-Trips.pdf" target="_blank">4 13 Field Trips</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-14-Deletion-of-Courses-Not-Taught-for-Five-Years.pdf" target="_blank">4 14 Deletion of Courses Not Taught for Five Years</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-15-Offering-Continuing-Professional-Ed.pdf" target="_blank">4 15 Offering Continuing Professional Ed</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume4/4-16-Award-of-Degrees-and-Certificates.pdf" target="_blank">4 16 Award of Degrees and Certificates</a></li>
                           
                        </ul>
                        
                        <h3>Volume 5 - General Administrative</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-01.1-Calendar.pdf" target="_blank">5 01.1 Calendar</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-01.2-Schedule-of-Classes.pdf" target="_blank">5 01.2 Schedule of Classes</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-02-Regulation-of-Promotion-and-Public-Speaking.pdf" target="_blank">5 02 Regulation of Promotion and Public Speaking</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-03-Public-Contact-with-Students-Faculty-and-Staff.pdf" target="_blank">5 03 Public Contact with Students Faculty and Staff</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-04-Student-Faculty-and-Staff-Child-Care-Services.pdf" target="_blank">5 04 Student Faculty and Staff Child Care Services</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-05-College-Vehicles.pdf" target="_blank">5 05 College Vehicles</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-06-Institutional-Membership.pdf" target="_blank">5 06 Institutional Membership</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-07-Contracts.pdf" target="_blank">5 07 Contracts</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-08-Travel-by-Authorized-Personnel.pdf" target="_blank">5 08 Travel by Authorized Personnel</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-09.1-Copyright-and-Trademark-Ownership.pdf" target="_blank">5 09.1 Copyright and Trademark Ownership</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-09.2-Educational-Work-Products.pdf" target="_blank">5 09.2 Educational Work Products</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-10-Bidding-Requirements.pdf" target="_blank">5 10 Bidding Requirements</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-10.1-No-Preferences-in-Procurement.pdf" target="_blank">5 10.1 No Preferences in Procurement</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-11-College-Property-Control.pdf" target="_blank">5 11 College Property Control</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume5/5-13-Signatures.pdf" target="_blank">5 13 Signatures</a></li>
                           
                        </ul>
                        
                        <h3>Volume 6 - Fiscal</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-01.1-Receipt-and-Deposit-of-Funds.pdf" target="_blank">6 01.1 Receipt and Deposit of Funds</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-01.2-Bank-Depositories.pdf" target="_blank">6 01.2 Bank Depositories</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-02-Investment-of-Funds.pdf" target="_blank">6 02 Investment of Funds</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-03.1-Expenditures.pdf" target="_blank">6 03.1 Expenditures</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-03.2-Signatures-on-Checks-and-Personnel-Contracts.pdf" target="_blank">6 03.2 Signatures on Checks and Personnel Contracts</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-04-Expenditures-of-Funds-for-Auxiliary-Enterprises-and-Undsignated-Gifts.pdf" target="_blank">6 04 Expenditures of Funds for Auxiliary Enterprises and Undsignated Gifts</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-05-Auxiliary-Enterprises.pdf" target="_blank">6 05 Auxiliary Enterprises</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-06-Student-Activity-Fee-Expenditures.pdf" target="_blank">6 06 Student Activity Fee Expenditures</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-07-Budget-Amendments.pdf" target="_blank">6 07 Budget Amendments</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-08.1-Student-Fees-and-Refunds.pdf" target="_blank">6 08.1 Student Fees and Refunds</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-08.2-College-Preparatory-and-College-Level-Credit-Course-Repeat-Fees.pdf" target="_blank">6 08.2 College Preparatory and College Level Credit Course Repeat Fees</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-09-Event-Admission-Prices.pdf" target="_blank">6 09 Event Admission Prices</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-10-Scholarships.pdf" target="_blank">6 10 Scholarships</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-11.1-Collection-of-Loans-VA-Deferments-and-Other-Student-Accounts-Rec.pdf" target="_blank">6 11.1 Collection of Loans VA Deferments and Other Student Accounts Rec</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-11.2-Collection-of-Returned-Checks.pdf" target="_blank">6 11.2 Collection of Returned Checks</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-12-Bonds.pdf" target="_blank">6 12 Bonds</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-13.1-Collection-of-Money-from-Students.pdf" target="_blank">6 13.1 Collection of Money from Students</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume6/6-13.2-Waiver-of-Fees-for-Persons-60-Years-of-Age-and-Older.pdf" target="_blank">6 13.2 Waiver of Fees for Persons 60 Years of Age and Older</a></li>
                           
                        </ul>
                        
                        <h3>Volume 7A - Information Technology</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7A/7A-01-Information-Technology-Resources-Policies.pdf" target="_blank">7A 01 Information Technology Resources Policies</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7A/7A-02-Acceptable-Use-of-Information-Technology-Resources.pdf" target="_blank">7A 02 Acceptable Use of Information Technology Resources</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7A/7A-03-Computer-Hardware-and-Software-Standards.pdf" target="_blank">7A 03 Computer Hardware and Software Standards</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7A/7A-04-Online-Privacy-Access-and-Security.pdf" target="_blank">7A 04 Online Privacy Access and Security</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7A/7A-05-User-Authentication-Requirements.pdf" target="_blank">7A 05 User Authentication Requirements</a></li>
                           
                        </ul>
                        
                        <h3>Volume 7B - Records Management</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7B/7B-01-Preservation-and-Disposal-of-Records.pdf" target="_blank">7B 01 Preservation and Disposal of Records</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7B/7B-02-Student-Records.pdf" target="_blank">7B 02 Student Records</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7B/7B-03-Financial-Records-and-Reports.pdf" target="_blank">7B 03 Financial Records and Reports</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7B/7B-04-Financial-Information-Security.pdf" target="_blank">7B 04 Financial Information Security</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume7B/7B-05-Human-Resources-Record-Information.pdf" target="_blank">7B 05 Human Resources Record Information</a></li>
                           
                        </ul>
                        
                        <h3>Volume 8 - Students</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-01-Academic-Standards-of-Satisfactory-Progress.pdf" target="_blank">8 01 Academic Standards of Satisfactory Progress</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-02-Admissions.pdf" target="_blank">8 02 Admissions</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-03-Student-Code-of-Conduct.pdf" target="_blank">8 03 Student Code of Conduct</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-04-Student-Possession-or-Consumption-of-Alcoholic-Beverages.pdf" target="_blank">8 04 Student Possession or Consumption of Alcoholic Beverages</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-05-Housing.pdf" target="_blank">8 05 Housing</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-06-Residency.pdf" target="_blank">8 06 Residency</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-07-Student-Clubs-and-Organizations.pdf" target="_blank">8 07 Student Clubs and Organizations</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-08-Student-Loans-and-Financial-Aid.pdf" target="_blank">8 08 Student Loans and Financial Aid</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-09-Requirement-of-Accident-and-Hospitalization-Insurance-for-International-Students.pdf" target="_blank">8 09 Requirement of Accident and Hospitalization Insurance for International Students</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-10-Student-Academic-Dispute-and-Administrative-Complaint-Resolution.pdf" target="_blank">8 10 Student Academic Dispute and Administrative Complaint Resolution</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-11-Academic-Dishonesty.pdf" target="_blank">8 11 Academic Dishonesty</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume8/8-12-Hazing.pdf" target="_blank">8 12 Hazing</a></li>
                           
                        </ul>
                        
                        <h3>Volume 9 - Advancement</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume9/9-01-The-Valencia-College-Foundation-Inc.pdf" target="_blank">9 01 The Valencia College Foundation Inc</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume9/9-02-Gifts-to-the-College.pdf" target="_blank">9 02 Gifts to the College</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume9/9-03-Photography.pdf" target="_blank">9 03 Photography</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume9/9-04-Advertising.pdf" target="_blank">9 04 Advertising</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume9/9-05-College-Publications.pdf" target="_blank">9 05 College Publications</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume9/9-06-Grant-Funding.pdf" target="_blank">9 06 Grant Funding</a></li>
                           
                        </ul>
                        
                        <h3>Volume 10 - Campus Safety and Security</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-01-Incidents-Accidents-or-Injuries.pdf" target="_blank">10 01 Incidents Accidents or Injuries</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-02-Trespass.pdf" target="_blank">10 02 Trespass</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-03-Fire-or-Other-Emergency.pdf" target="_blank">10 03 Fire or Other Emergency</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-04-Firearms-and-Weapons-on-College-Property-and-at-College-Events.pdf" target="_blank">10 04 Firearms and Weapons on College Property and at College Events</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-05-Smoking-Regulations.pdf" target="_blank">10 05 Smoking Regulations</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-07-Parking-and-Vehicle-Traffic.pdf" target="_blank">10 07 Parking and Vehicle Traffic</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-07.2-Non-Motorized-Vehicles-on-Campus.pdf" target="_blank">10 07.2 Non Motorized Vehicles on Campus</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-08.1-Drug-Free-Campuses.pdf" target="_blank">10 08.1 Drug Free Campuses</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-08.2-Drug-Free-Workplace.pdf" target="_blank">10 08.2 Drug Free Workplace</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume10/10-09-Child-Abuse-Reporting.pdf" target="_blank">10 09 Child Abuse Reporting</a></li>
                           
                        </ul>
                        
                        <h3>Volume 11 - Campus Facilities</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-01-Physical-Plant-Maintenance-and-Sanitation.pdf" target="_blank">11 01 Physical Plant Maintenance and Sanitation</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-02-Animals-and-Pets.pdf" target="_blank">11 02 Animals and Pets</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-03-Key-Control.pdf" target="_blank">11 03 Key Control</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-04-Lake-Pamela-West-Campus.pdf" target="_blank">11 04 Lake Pamela West Campus</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-05-Selecting-Professional-Services.pdf" target="_blank">11 05 Selecting Professional Services</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-06-Pre-qualification-of-Contractors-for-Educational-Facilities-Construction.pdf" target="_blank">11 06 Pre qualification of Contractors for Educational Facilities Construction</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-07-Changes-in-Construction-After-Award-of-Contract.pdf" target="_blank">11 07 Changes in Construction After Award of Contract</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-08.1-Temporary-Use-of-Rooms-or-Campus-Facilities.pdf" target="_blank">11 08.1 Temporary Use of Rooms or Campus Facilities</a></li>
                           
                           <li><a href="/about/general-counsel/policy/documents/Volume11/11-08.2-College-Facilities-Non-College-Use-of.pdf" target="_blank">11 08.2 College Facilities Non College Use of</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Notices of Rule Development</h3>
                        
                        <ul class="list-style-1">
                           
                           <li>Policy 6Hx28: 1-12 Educational Affordability</li>
                           
                           <li>Policy 6Hx28: 3C-07 Staff and Program Development Funds</li>
                           
                           <li>Policy 6Hx28: 3C-09 Waiver of Matriculation or Tuition Fees for Spouses and Dependents
                              of Faculty and Staff
                           </li>
                           
                           <li>Policy 6Hx28: 3D-03 - Total Rewards: Vacation, Sick, and Personal Leave for Full Time
                              Employees
                           </li>
                           
                           <li>Policy 6Hx28: 3D-06.2 Fitness for Duty</li>
                           
                           <li>Policy 6Hx28: 3E-05.2 Ethical Conduct and Performance</li>
                           
                           <li>Policy 6Hx28: 5-05 College Vehicles</li>
                           
                           <li>Policy 6Hx28: 5-10 Bidding Requirements</li>
                           
                           <li>Policy 6Hx28: 5-13 Signatures</li>
                           
                           <li>Policy 6Hx28: 8-02 - Admissions</li>
                           
                           <li>Policy 6Hx28: 9-06 Grant Funding</li>
                           
                           <li>Policy 6Hx28: 10-01 Incidents, Accidents or Injuries</li>
                           
                           <li>Policy 6Hx28: 10-02 Trespass Warning/Arrest</li>
                           
                           <li>Policy 6Hx28: 10-03 Fire or Other Emergency</li>
                           
                           <li>Policy 6Hx28: 10-04 Firearms on Campus</li>
                           
                           <li>Policy 6Hx28: 10-05 Smoking Regulations</li>
                           
                           <li>Policy 6Hx28: 10-07 Parking and Vehicle Traffic</li>
                           
                           <li>Policy 6Hx28: 10-7.2 Non-Motorized Vehicles on Campus</li>
                           
                           <li>Policy 6Hx28: 10-09 Child Abuse Reporting</li>
                           
                           <li>Policy 6Hx28: 11-02 Animals and Pets</li>
                           
                        </ul>
                        
                        <h3>Notices of Rule Adoption</h3>
                        
                        <ul class="list-style-1">
                           
                           <li>Policy 6Hx28: 8-02 - Admissions</li>
                           
                           <li>Policy 6Hx28: 10-7.2 - Non-Motorized Vehicles on Campus</li>
                           
                        </ul>
                        
                        <h3>Notices of Rule Repeal</h3>
                        
                        <ul class="list-style-1">
                           
                           <li>None at this time.</li>
                           
                        </ul>
                        
                        <h3>Recently Adopted Policies</h3>
                        
                        <ul class="list-style-1">
                           
                           <li>Policy 6Hx28: 3B-03 - Recruitment and Selection of Employees</li>
                           
                           <li>Policy 6Hx28: 3C-01 - Total Rewards: Employee Compensation</li>
                           
                           <li>Policy 6Hx28: 3C-04 Total Rewards: Recognition of Full-Time Employees for Educational
                              Advancement
                           </li>
                           
                           <li>Policy 6Hx28: 3C-08 Total Rewards: Benefits</li>
                           
                           <li>Policy 6Hx28: 3C-06-1 Contracts for Administrative and Instructional Employees</li>
                           
                           <li>Policy 6Hx28: 3D-05 - Sick Leave Pool</li>
                           
                           <li>Policy 6Hx28: 3D-06.1 - Family/Medical Leave</li>
                           
                           <li>Policy 6Hx28: 3D-14 Paid Time Off for Part Time Faculty Teaching Academic Credit Programs</li>
                           
                           <li>Policy 6Hx28: 3E-01 Full-Time Employee Performance Evaluation</li>
                           
                           <li>Policy 6Hx28: 3F-02 Terminal Pay</li>
                           
                           <li>Policy 6Hx28: 3F-03 Suspension, Dismissal, Return to Annual Contract or Non-Renewal
                              of Contract
                           </li>
                           
                           <li>Policy 6Hx28: 3E-05.5 Tutoring</li>
                           
                           <li>Policy 6Hx28: 4-16 Award of Degrees and Certificates</li>
                           
                           <li>Policy 6Hx28: 8-2 Admissions</li>
                           
                        </ul>
                        
                        <h3>Recently Repealed Policies</h3>
                        
                        <ul class="list-style-1">
                           
                           <li>Policy 6Hx28: 3A-01 Job Description for Personnel of the College</li>
                           
                           <li>Policy 6Hx28: 3A-02.1 Definition of Administrative, Instructional, and Professional</li>
                           
                           <li>Policy 6Hx28: 3A-02.2 Definition of Full-Tiime Employment - Instructional and Administrative
                              Employees
                           </li>
                           
                           <li>Policy 6Hx28: 3A-02.3 Definition of Career Service Employees</li>
                           
                           <li>Policy 6Hx28: 3A-02.4 Definition of Full-Time Employment for Career Service Employees</li>
                           
                           <li>Policy 6Hx28: 3A-03 Classification of Career Service Positions</li>
                           
                           <li>Policy 6Hx28: 3A-04.1 Part-Time Personnel</li>
                           
                           <li>Policy 6Hx28: 3A-04.2 Part-Time Instructional Personnel</li>
                           
                           <li>Policy 6Hx28: 3C-01.1 Hours of Work for Instructional and Administrative Employees</li>
                           
                           <li>Policy 6Hx28: 3C-02.1 Hours of Work for Career Service Employees</li>
                           
                           <li>Policy 6Hx28: 3C-02.2 Recording of Hours Worked - Career Service Employees</li>
                           
                           <li>Policy 6Hx28: 3C-02.3 Overtime Compensation for Career Service Employees</li>
                           
                           <li>Policy 6Hx28: 3C-02.4 Night Shift Differential</li>
                           
                           <li>Policy 6Hx28: 3C-02.5 Payment to Career Service Employees for Holidays Worked</li>
                           
                           <li>Policy 6Hx28: 3C-03.1 Payroll Deduction Authorization</li>
                           
                           <li>Policy 6Hx28: 3C-03.2 Payroll Deductions for Dues for Certain Employee Organizations</li>
                           
                           <li>Policy 6Hx28: 3C-04.1 Salary Schedule</li>
                           
                           <li>Policy 6Hx28: 3C-04.3.1 Pay of Instructional Personnel Receiving Advanced College
                              Credit
                           </li>
                           
                           <li>Policy 6Hx28: 3C-04.3.2 Pay of Professional and Administrative Personnel Receving
                              Advanced College Credit
                           </li>
                           
                           <li>Policy 6Hx28: 3C-05 Transfer from Temporary Grant-Funded Position to a College-Funded
                              Position
                           </li>
                           
                           <li>Policy 6Hx28: 3C-06.2 Supplemental and Overload Contracts</li>
                           
                           <li>Policy 6Hx28: 3-C13 Optional Retirement Programs</li>
                           
                           <li>Policy 6Hx28: 3D-01 Paid Non-Duty Days</li>
                           
                           <li>Policy 6Hx28: 3D-08 - Partial Leave</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/general-counsel/policy/index.pcf">©</a>
      </div>
   </body>
</html>