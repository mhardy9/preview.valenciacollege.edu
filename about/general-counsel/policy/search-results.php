<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/general-counsel/policy/search-results.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/general-counsel/policy/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office of Policy and General Counsel</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/general-counsel/">Policy &amp; General Counsel</a></li>
               <li><a href="/about/general-counsel/policy/">Policy &amp; Procedures</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           
                           <div>
                              
                              
                              <div> 
                                 <img alt="" height="8" src="arrow.gif" width="8">  
                                 <img alt="" height="8" src="arrow.gif" width="8">  
                                 <img alt="" height="8" src="arrow.gif" width="8">  
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>Navigate</div>
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div> 
                                 
                                 
                                 
                                 <h2>Policies and Procedures</h2>
                                 
                                 
                                 <h3>Welcome</h3>
                                 
                                 <p>Welcome to the Valencia College Policies and Procedures web site.  All Board approved
                                    policies and related procedures can be viewed here. You may notice the new layout
                                    and design of this website, which should provide for easier navigation and ease of
                                    use.
                                 </p>
                                 
                                 <h3>Navigation</h3>
                                 
                                 <p>By using the navigation boxes to the right, you may view all policies (and related
                                    procedures) within each volume by clicking on the volume number and referring to the
                                    resulting drop-down list. 
                                 </p>
                                 
                                 
                                 <h3>Search</h3>
                                 
                                 <p>You may search policies and procedures for specific words or terms by using the search
                                    box found on the right-hand side and below.
                                 </p>
                                 
                                 
                                 
                                 <p>Each policy is in PDF format for easy printing, saving and viewing.</p>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <form action="search-results.html" method="post">  
                                          <input name="bonceOut" type="text" value="">
                                          <input name="navst" type="hidden" value="0">
                                          <input name="searchFor" onclick="if (this.value == 'search policies and procedures') this.value=''" size="30" type="text" value="search policies and procedures">  
                                          <input type="submit" value="Find">
                                          
                                       </form>
                                       <br>
                                       <span>To search for multiple terms, please separate words with a comma (example: college,
                                          studies)</span>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    
                                    <h2>Policies and Procedures</h2>
                                    
                                    
                                    <p>Enter a search term above</p>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <a href="http://net4.valenciacollege.edu/forms/generalcounsel/policy/contact.cfm" target="_blank">Contact Us</a>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <p>Valencia College policy documents open with Adobe<sup>�</sup> Acrobat<sup>�</sup> Reader.
                                    </p>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/general-counsel/policy/search-results.pcf">©</a>
      </div>
   </body>
</html>