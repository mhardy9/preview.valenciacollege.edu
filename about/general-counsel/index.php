<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office of Policy and General Counsel  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/general-counsel/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/general-counsel/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Office of Policy and General Counsel</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Policy &amp; General Counsel</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        					
                        <h2>Mission and Goals</h2>
                        					
                        <p>The Valencia College Office of Policy and General Counsel is committed to act with
                           integrity, knowledge of the law, and reason, in support of the dynamic educational
                           environment of the College by providing timely, updated and accurate legal advice
                           to the College administrators, faculty and staff, in their roles on behalf of the
                           College. The Office provides, manages, and coordinates all legal services for the
                           College, whether through the Office or outside law firms, to ensure efficiency, containment
                           of costs, and reliability of service.
                        </p>
                        
                        					
                        <div class="row">
                           					
                           <div class="col-md-4">
                              						
                              <h3>Spotlight</h3>
                              
                              						
                              <p><strong>Title VII: <a href="documents/Sessions-memo-reversing-gender-identity-civil.pdf" target="_blank">AG Sessions MEMO Re: Revised Transgender Employment Discrimination</a> (October 4, 2017) </strong></p>
                              						
                              <p>Attorney General Jeff Sessions, sent a memorandum to all US Attorneys reversing AG
                                 Holder's December 15, 2014 memo prohibiting employers from discriminating against
                                 transgender individuals under Title VII.
                              </p>
                              
                              						
                              <p><strong>Title IX: <a href="documents/Issue-Brief-Title-IX-QA-2017.pdf" target="_blank">ACE-Brief-Title IX-Q&amp;A</a> (September 27, 2017)</strong></p>
                              						
                              <p>The American Council on Education (ACE) has issued a brief summarizing the Department
                                 of Education's interim guidance on Title IX campus sexual assault, and it's preparation
                                 to launch a formal rulemaking process to develop a new policy.
                              </p>
                              
                              						
                              <p><strong>Title IX: <a href="documents/colleague-title-ix-201709.pdf" target="_blank">OCR Dear Colleague Letter</a> (September 22, 2017)</strong></p>
                              						
                              <p>The U.S. Department of Education, the Office for Civil Rights division, has announced
                                 that it is withdrawing the existing 2011 Dear Colleague Letter and the 2014 guidance
                                 on sexual misconduct on campus.
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="documents/Letter-to-Congress-on-DACA-Sept-2017.pdf" target="_blank">ACE-Letter to Congress on DACA</a> (September 12, 2017)</strong></p>
                              						
                              <p>The American Council on Education (ACE) and 77 other associations sent this letter
                                 urging Congress to pass legislation as soon as possible to permanently protect the
                                 nearly 800,000 individuals currently protected under DACA.
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="documents/Issue-Brief-Trump-DACA-Decision.pdf" target="_blank">ACE- Issue Brief regarding Trump's DACA Decision</a> (September 5, 2017)</strong></p>
                              						
                              <p>In the wake of the Trump administration’s rollback of Deferred Action on Childhood
                                 Arrivals (DACA), ACE has prepared this issue brief detailing the key elements of the
                                 administration’s “DACA Rescission Memorandum” and how it will impact students and
                                 campuses. 
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="documents/Letter-to-President-Trump-on-DACA-August-2017.pdf" target="_blank">ACE - Letter to President Donald Trump in Support of DACA</a> (August 29, 2017) </strong></p>
                              						
                              <p>The American Council on Education (ACE) along with 36 other educational associations
                                 sent a letter to President Trump urging the administration to keep Deferred Action
                                 for Childhood Arrivals (DACA) intact until a permanent solution can be reached.
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="documents/DREAM-Act-Letter-McConnell-Schumer.pdf" target="_blank">ACE-Letter in Support of Dream Act of 2017</a> (August 1, 2017)</strong></p>
                              						
                              <p>The American Council on Education (ACE) released a letter addressed to Senators Mitch
                                 McConnell (R-KY) and Charles Schumer (D-NY) from 32 higher education associations
                                 supporting the bipartisan Dream Act of 2017. The Dream Act would allow some undocumented
                                 young people, who have already invested in our country and in whom the country has
                                 already invested, to earn lawful permanent residence in the United States.
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="policy/historical/ACE-Issue-Brief-RevisedTravelBan.pdf">ACE- Issue Brief: Revised Travel Ban </a>(March 7, 2017)</strong></p>
                              						
                              <p>The American Council on Education (ACE) has released an issue brief giving an overview
                                 of President Trump's revised temporary travel ban, which will take effect on March
                                 16, 2017. The brief also addresses concerns regarding how this will affect international
                                 students from the six countries in the revised Immigration Executive Order.
                              </p>
                              
                              						
                              <p><strong>Title IX: <a href="policy/historical/dearcolleagueletterfebruary2017.pdf">OCR Dear Colleague Letter</a> </strong><strong> (February 22, 2017) </strong></p>
                              						
                              <p>The U.S. Department of Justice and Department of Education, Civil Rights Division,
                                 has released a Dear Colleague Letter withdrawing the statements of policy and guidance
                                 reflected during the Obama Administration as it relates to transgender individuals
                                 using bathrooms consistent with their gender identity.
                              </p>
                              
                              						
                              <p><strong>Faculty and Staff: <a href="policy/historical/Empl.ProvidedEd.Assistance.pdf">Letter from Coalition to Preserve Employer Provided Education Assistance</a> (February 7, 2017)</strong></p>
                              						
                              <p>A coalition made up of higher education, businesses and labor organizations wrote
                                 Senators Orrin Hatch (R-UT) and (D-OR) in support of maintaining and strengthening
                                 Internal Revenue Code (IRC) Section 127, which allows employers to provide education
                                 assistance to their employees. Currently, the Code allows an employee to exclude up
                                 to $5,250 per year from their income to assist with educational courses at the undergraduate
                                 and graduate level.
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="policy/historical/ACE-Letter-to-DHS-John-Kelly-International-Students-Scholars.pdf">ACE- Letter- DHS- John Kelly</a> (January 31, 2017) </strong></p>
                              						
                              <p>The American Council on Education (ACE) sent a letter to newly appointed Secretary
                                 of Homeland Security, John Kelly, expressing their concern regarding President Trump's
                                 executive order, <a href="https://www.whitehouse.gov/the-press-office/2017/01/27/executive-order-protecting-nation-foreign-terrorist-entry-united-states">"Protecting the Nation From Foreign Terrorist Entry into the United States".</a> The letter also reminds Secretary Kelly of the importance of International students,
                                 and the need for the United States to remain the destination of choice for the world's
                                 best and brightest students, faculty and scholars. 
                              </p>
                              
                              						
                              <p><strong>Title IX &amp; Title VII: <a href="policy/historical/pregnantparentstudent.pdf">NACUA- The Pregnant and Parenting Student</a> (January 30, 2017) </strong></p>
                              						
                              <p>The National Assocation of College and University Attorneys (NACUA) released a note
                                 regarding pregnant and parenting students. This note seeks to provide guidance to
                                 universities and colleges on how to comply with federal laws and regulations, and
                                 afford equitable treatment and accomodations for these students.
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="policy/historical/Letter-Graham-Durbin-BRIDGE-Act.pdf">ACE- Letter- BRIDGE ACT</a> (January 12, 2017)</strong></p>
                              						
                              <p>Letter from The American Council on Education (ACE) to Sentaors Lindsey Grahram (R-SC)
                                 and Richard Durbin (D-IL) in support of the Bar Removal of Individuals Who Dream and
                                 Grow Our Economy (BRIDGE) Act. This bill will assist recipients or eligible individuals
                                 of the Deferred Action for Childhood Arrivals (DACA) program by providing them temporary
                                 relief from deportation and employment authorization for three years.
                              </p>
                              
                              						
                              <p><strong>Immigration: <a href="policy/historical/ACE-Issue-Brief-Immigration-DACA-Sanctuary-Campus.pdf">ACE-Issue-Brief-Immigration-DACA-Sanctuary-Campus</a> (December 2, 2016) </strong></p>
                              						
                              <p>The American Council on Education (ACE) has released an Issue Brief relating to undocumented
                                 community members following this year’s presidential election. This brief addresses
                                 concerns regarding the new administration’s stance on immigration, and its potential
                                 effects on college students who were approved for the Deferred Action for Childhood
                                 Arrivals (DACA) program – an executive order created under the Obama administration.
                                 
                              </p>
                              					
                           </div>
                           					
                           <div class="col-md-4">
                              						
                              <h3>State Legislative Information</h3>
                              
                              						
                              <h4>2017 Legislative Session</h4>
                              						
                              <p><a href="documents/2017-Legislative-Summary-Report-Interim.pdf" target="_blank">2017 Legislative Report- Interim</a></p>
                              
                              						
                              <h4>Past Valencia Session Reports</h4>
                              						
                              <p>Past Issues of the Valencia Session Report may be found under the "Session Report
                                 Archives" tab.
                              </p>
                              						
                              <p>The Session Report provides brief summaries of budget matters and substantive legislation
                                 affecting Florida Colleges. Please note that at present, all appropriations and many
                                 bills are subject to the Governor's veto. All bill numbers are hyperlinked to their
                                 respective bill pages on the state's Online Sunshine site. There, you can find more
                                 information including all versions of bill text, histories, and staff analyses.
                              </p>
                              						
                              <p>At the end of the Report, you will find information about the members of Valencia's
                                 state legislative delegation - that is, those legislators who represent Florida House
                                 or Senate districts located within Orange or Osceola counties. Here you will find
                                 their names, pictures (<em>pictures</em> <em>are hyperlinked to their Online Sunsine Member pages</em>), committee assignments and contact information. 
                              </p>
                              						
                              <p>Finally, interspersed throughout the report are images of paintings depicting Florida's
                                 history and heritage, the originals of which are displayed in the House Chamber. Click
                                 on them and discover the stories behind the paintings!
                              </p>
                              						
                              <p>We hope you find the Valencia College Session Report to be informative and helpful.
                                 
                              </p>
                              					
                           </div>
                           					
                           <div class="col-md-4">
                              						
                              <h3>Policies &amp; Procedures</h3>
                              						
                              						
                              <h4><a href="policy/">Valencia College Policies &amp; Procedures (All Volumes)</a></h4>
                              
                              						
                              <h4>Notices of Rule Development</h4>
                              						
                              <ul class="list-style-1">
                                 							
                                 <li>Policy 6Hx28: 1-12 Educational Affordability</li>
                                 							
                                 <li>Policy 6Hx28: 3C-07 Staff and Program Development Funds </li>
                                 							
                                 <li>Policy 6Hx28: 3C-09 Waiver of Matriculation or Tuition Fees for Spouses and Dependents
                                    of Faculty and Staff 
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3D-03 - Total Rewards: Vacation, Sick, and Personal Leave for Full Time
                                    Employees
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3D-06.2 Fitness for Duty </li>
                                 							
                                 <li>Policy 6Hx28: 3E-05.2 Ethical Conduct and Performance</li>
                                 							
                                 <li>Policy 6Hx28: 5-05 College Vehicles </li>
                                 							
                                 <li>Policy 6Hx28: 5-10 Bidding Requirements</li>
                                 							
                                 <li>Policy 6Hx28: 5-13 Signatures</li>
                                 							
                                 <li>Policy 6Hx28: 8-02 - Admissions</li>
                                 							
                                 <li>Policy 6Hx28: 9-06 Grant Funding </li>
                                 							
                                 <li>Policy 6Hx28: 10-01 Incidents, Accidents or Injuries</li>
                                 							
                                 <li>Policy 6Hx28: 10-02 Trespass Warning/Arrest </li>
                                 							
                                 <li>Policy 6Hx28: 10-03 Fire or Other Emergency</li>
                                 							
                                 <li>Policy 6Hx28: 10-04 Firearms on Campus</li>
                                 							
                                 <li>Policy 6Hx28: 10-05 Smoking Regulations</li>
                                 							
                                 <li>Policy 6Hx28: 10-07 Parking and Vehicle Traffic </li>
                                 							
                                 <li>Policy 6Hx28: 10-7.2 Non-Motorized Vehicles on Campus</li>
                                 							
                                 <li>Policy 6Hx28: 10-09 Child Abuse Reporting </li>
                                 							
                                 <li>Policy 6Hx28: 11-02 Animals and Pets</li>
                                 						
                              </ul>
                              
                              						
                              <h4>Notices of Rule Adoption</h4>
                              						
                              <ul class="list-style-1">
                                 							
                                 <li>Policy 6Hx28: 8-02 - Admissions</li>
                                 							
                                 <li>Policy 6Hx28: 10-7.2 - Non-Motorized Vehicles on Campus</li>
                                 						
                              </ul>
                              
                              						
                              <h4>Notices of Rule Repeal</h4>
                              
                              						
                              <h4>Recently Adopted Policies</h4>
                              						
                              <ul class="list-style-1">
                                 							
                                 <li>Policy 6Hx28: 3B-03 - Recruitment and Selection of Employees </li>
                                 							
                                 <li>Policy 6Hx28: 3C-01 - Total Rewards: Employee Compensation</li>
                                 							
                                 <li>Policy 6Hx28: 3C-04 Total Rewards: Recognition of Full-Time Employees for Educational
                                    Advancement 
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3C-08 Total Rewards: Benefits </li>
                                 							
                                 <li>Policy 6Hx28: 3C-06-1 Contracts for Administrative and Instructional Employees</li>
                                 							
                                 <li>Policy 6Hx28: 3D-05 - Sick Leave Pool </li>
                                 							
                                 <li>Policy 6Hx28: 3D-06.1 - Family/Medical Leave</li>
                                 							
                                 <li>Policy 6Hx28: 3D-14 Paid Time Off for Part Time Faculty Teaching Academic Credit Programs
                                    
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3E-01 Full-Time Employee Performance Evaluation</li>
                                 							
                                 <li>Policy 6Hx28: 3F-02 Terminal Pay</li>
                                 							
                                 <li>Policy 6Hx28: 3F-03 Suspension, Dismissal, Return to Annual Contract or Non-Renewal
                                    of Contract 
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3E-05.5 Tutoring </li>
                                 							
                                 <li>Policy 6Hx28: 4-16 Award of Degrees and Certificates</li>
                                 							
                                 <li>Policy 6Hx28: 8-2 Admissions </li>
                                 						
                              </ul>
                              
                              						
                              <h4>Recently Repealed Policies</h4>
                              						
                              <ul class="list-style-1">
                                 							
                                 <li>Policy 6Hx28: 3A-01 Job Description for Personnel of the College </li>
                                 							
                                 <li>Policy 6Hx28: 3A-02.1 Definition of Administrative, Instructional, and Professional
                                    
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3A-02.2 Definition of Full-Tiime Employment - Instructional and Administrative
                                    Employees 
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3A-02.3 Definition of Career Service Employees</li>
                                 							
                                 <li>Policy 6Hx28: 3A-02.4 Definition of Full-Time Employment for Career Service Employees
                                    
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3A-03 Classification of Career Service Positions </li>
                                 							
                                 <li>Policy 6Hx28: 3A-04.1 Part-Time Personnel </li>
                                 							
                                 <li>Policy 6Hx28: 3A-04.2 Part-Time Instructional Personnel </li>
                                 							
                                 <li>Policy 6Hx28: 3C-01.1 Hours of Work for Instructional and Administrative Employees
                                    
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3C-02.1 Hours of Work for Career Service Employees </li>
                                 							
                                 <li>Policy 6Hx28: 3C-02.2 Recording of Hours Worked - Career Service Employees </li>
                                 							
                                 <li>Policy 6Hx28: 3C-02.3 Overtime Compensation for Career Service Employees </li>
                                 							
                                 <li>Policy 6Hx28: 3C-02.4 Night Shift Differential </li>
                                 							
                                 <li>Policy 6Hx28: 3C-02.5 Payment to Career Service Employees for Holidays Worked </li>
                                 							
                                 <li>Policy 6Hx28: 3C-03.1 Payroll Deduction Authorization </li>
                                 							
                                 <li>Policy 6Hx28: 3C-03.2 Payroll Deductions for Dues for Certain Employee Organizations</li>
                                 							
                                 <li>Policy 6Hx28: 3C-04.1 Salary Schedule </li>
                                 							
                                 <li>Policy 6Hx28: 3C-04.3.1 Pay of Instructional Personnel Receiving Advanced College
                                    Credit
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3C-04.3.2 Pay of Professional and Administrative Personnel Receving
                                    Advanced College Credit 
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3C-05 Transfer from Temporary Grant-Funded Position to a College-Funded
                                    Position 
                                 </li>
                                 							
                                 <li>Policy 6Hx28: 3C-06.2 Supplemental and Overload Contracts </li>
                                 							
                                 <li>Policy 6Hx28: 3-C13 Optional Retirement Programs </li>
                                 							
                                 <li>Policy 6Hx28: 3D-01 Paid Non-Duty Days </li>
                                 							
                                 <li>Policy 6Hx28: 3D-08 - Partial Leave</li>
                                 						
                              </ul>
                              					
                           </div>
                           					
                        </div>
                        					
                        					
                        <div class="box_style_3">
                           						
                           <h3>Search Policies</h3>
                           						
                           <form action="policy/search-results.php" method="post">
                              							<input name="navst" type="hidden" value="0">
                              							<input name="searchFor" size="25" type="text" placeholder="search policies...">
                              							<input type="submit" value="Find">
                              						
                           </form>
                           						<br>
                           						
                           <p>To search for multiple terms, please separate words with a comma (example: college,
                              studies)
                           </p>
                           					
                        </div>
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/general-counsel/index.pcf">©</a>
      </div>
   </body>
</html>