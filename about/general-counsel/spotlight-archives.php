<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Spotlight Archives  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/general-counsel/spotlight-archives.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/general-counsel/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office of Policy and General Counsel</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/general-counsel/">Policy &amp; General Counsel</a></li>
               <li>Spotlight Archives </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Spotlight Archives</h2>
                        
                        
                        <h3>FAQ Section</h3>
                        
                        <h4>Legal Defense and Indemnification of Valencia Employees</h4>
                        
                        <p><em>FAQ: Under which circumstances may Valencia College provide legal defense and indemnify
                              a member of the faculty or staff in the event of the filing of a lawsuit against a
                              member of the faculty or staff?</em></p>
                        
                        <p>In the daily course of business at Valencia College, the Office of Policy and General
                           Counsel (and external legal counsel, under the supervision of the Office of Policy
                           and General Counsel) represents the College and provides counsel in decision-making
                           to the Board of Trustees, the President, Vice Presidents, Provosts, Deans and Directors,
                           faculty, supervisors and other employees acting in their capacity as employees, and
                           may not represent individual students, faculty, or staff except when these individuals
                           are named as defendants in adversary proceedings as a result of actions or omissions
                           within the course and scope of their employment or institutional representation. For
                           personal legal advice, employees and students should consult a private attorney.
                        </p>
                        
                        
                        <p>In the event a lawsuit is filed against the College and/or a member of its faculty
                           and staff, Valencia's Policy 6Hx28:1-11 applies, stating:
                        </p>
                        
                        
                        <blockquote>
                           
                           <p>"[i]n accordance with section 1012.85, F.S., whenever any civil action has been brought
                              against any officer of the District Board of Trustees, including a District Board
                              of Trustees member, or any person employed by or agent of the District Board of Trustees
                              for any act or omission arising out of and in the course of the performance of his
                              or her duties and responsibilities, the District Board of Trustees may defray all
                              costs of defending such action, including reasonable attorney's fees and expenses
                              together with costs of appeal, if any, and may save harmless and protect such person
                              from any financial loss resulting there from.
                           </p>
                           
                           
                           <p>However, any attorney's fees paid from public funds for any officer, employee, or
                              agent who is found to be personally liable by virtue of acting outside the scope of
                              his or her employment or acting in bad faith, with malicious purpose, or in a manner
                              exhibiting wanton and willful disregard of human rights, safety, or property may be
                              recovered by the College."
                           </p>
                           
                        </blockquote>
                        
                        
                        <p>Therefore, Valencia College will indemnify an employee of the College against liability
                           and associated costs if the conduct that is the subject of the claim or action occurred
                           within the scope of the employee's duties and the employee was acting in good faith,
                           without criminal or other willful misconduct, and in a manner the employee reasonably
                           believed to be in or not opposed to the best interests of the College, and with respect
                           to any criminal action or proceeding, the employee must have had no reasonable cause
                           to believe the conduct was unlawful. Covered expenses may include defense costs and
                           the payment of judgments, fines, penalties, settlements, and other expenses an employee
                           would reasonably incur in connection with the defense of the claim or action.
                        </p>
                        
                        
                        <p>Conversely, the College reserves the right to either withhold indemnification or require
                           reimbursement of defense and investigative expenses if the claim arises from the employee's
                           willful negligence or misconduct or results from the employee's action/inaction when
                           acting outside the scope of his or her employment or acting in bad faith, with malicious
                           purpose, or in a manner exhibiting wanton and willful disregard of human rights, safety,
                           or property. These indemnification provisions do not apply in any action in which
                           the College is the plaintiff, or moving party, or initiator of action against the
                           person who might otherwise be eligible to receive indemnification from the College.
                        </p>
                        
                        
                        <h3>Political Activity </h3>
                        
                        <p><strong>Political Activity: <a href="documents/MemoPoliticalCampaigns.pdf">ACE Memorandum on Political Activities</a><br>
                              (September 15, 2014)</strong></p>
                        
                        <p>The American Council on Education has released a memorandum on political campaign-related
                           activities at colleges and universities. The educational memo summarizes federal restrictions
                           on political activity and involvement at 501(c)(3) institutions and offers “do’s”
                           and “don’ts” based on these restrictions. 
                        </p>
                        
                        <p><strong>2016 Florida Legislative Session: <a href="documents/FirearmsonCampusBriefingPaper.pdf">Update on Pending Firearms Bills January 12, 2016 </a></strong></p>
                        
                        
                        <h3>Sexual Misconduct Articles</h3>
                        
                        <p><strong>Sexual Misconduct:<a href="documents/CommentsCASAHarkinFinal_9-9-14.pdf"> American Council on Education Comments on the Campus Accountability and Safety Act</a><br>
                              (September 9, 2014)</strong></p>
                        
                        <p>Letter from the American Council on Education (ACE) to Senators Tom Harkin (D-IA)
                           and Lamar Alexander (R-TN) regarding the <a href="http://www.nacua.org/documents/CampusAccountabilitySafetyActCASA_113s2692is.pdf">Campus Accountability and Safety Act</a> (CASA) (S. 2692). ACE states that it "strongly support[s]" many of the concepts embodied
                           in the legislation but points to certain provisions that should be altered to reinforce
                           effective implementation and to promote a safe campus environment. Specific provisions
                           addressed include those mandating confidential advisors, climate surveys, memoranda
                           of understanding with law enforcement agencies, Title IX training for responsible
                           employees, and new Clery reporting requirements. The letter also calls upon Congress
                           to resolve ambiguities and conflicts between Title IX and the Clery Act and to require
                           the Department of Education to engage in extensive outreach with all stakeholders
                           before implementing new policies that address campus sexual misconduct. 
                        </p>
                        
                        <p><strong>Sexual Misconduct:<a href="documents/ACE_LtrSenateHELPSexualAssaultHearing.pdf"> Letter from the American Council on Education (ACE) on Campus Efforts to Address
                                 Sexual Assault</a><br>
                              (July 1, 2014)</strong></p>
                        
                        <p>Letter from the American Council on Education (ACE) to the Senate Committee on Health,
                           Education, Labor and Pensions (HELP) regarding sexual assault on campus. The letter
                           describes the efforts of colleges and universities to address the problem and details
                           certain difficulties that institutions are facing in light of the policies and procedures
                           issued by the Department of Education's Office for Civil Rights. ACE concludes its
                           letter by providing six recommended steps for Congress to take that would assist institutions
                           in reducing incidents of sexual assault and facilitating institutional responses to
                           reports of such incidents. 
                        </p>
                        
                        <p><strong>Sexual Assault: <a href="http://www.nacua.org/documents/WhiteHouseTaskForceonSexualAssaultReport.pdf">First Report of the White House Task Force to Protect Students From Sexual Assault</a><br>
                              (April 29, 2014)</strong></p>
                        
                        <p>Report from the White House Task Force to Protect Students from Sexual Assault providing
                           recommendations and action steps intended to combat sexual assault on college and
                           university campuses. The Report's recommendations focus on four action steps: 1) identifying
                           the problem and its extent on campuses, 2) preventing campus sexual assault, 3) responding
                           effectively to student victims, and 4) rendering the federal government's enforcement
                           efforts more effectual and transparent. The Report includes policy recommendations
                           and resources to better execute the action steps.
                        </p>
                        
                        <p><strong>Sexual Assault: <a href="http://www.nacua.org/documents/OCRQandA_TitleIXSexualViolence.pdf">OCR Questions and Answers on Title IX and Sexual Violence</a><br>
                              (April 29, 2014)</strong></p>
                        Guidance from the Department of Education Office for Civil Rights (OCR) in the form
                        of questions and answers intended to further clarify the legal requirements and guidance
                        articulated in the 2011 Dear Colleague Letter and OCR's 2001 guidance.
                        
                        
                        <h3>Toward a Smoke Free Valencia </h3>
                        
                        <p>Valencia is committed to providing a safe and healthy learning environment for our
                           students, employees and visitors. In recent years, we have made improvements toward
                           healthier campuses with the implementation of LEED-certified buildings, employee wellness
                           programs and recycling efforts. Smoke-free campuses will further promote the health,
                           safety and well-being of members of the Valencia community by reducing exposure to
                           secondhand smoke on Valencia campuses.
                        </p>
                        
                        <p>In taking this step, Valencia joins the University of Florida, Edison State College
                           and many other colleges and universities in Florida and around the country in providing
                           for healthier academic environments. Valencia's work plan in crafting this policy,
                           including a list of those who worked on this initiative, can be found <a href="documents/Smoke_Free_Policy_Work_Plan.pdf" target="_blank">here.</a></p>
                        
                        <p>To connect to related resources, including help for those who may want to quit smoking,
                           please visit the following links:
                        </p>
                        
                        <ul>
                           
                           <li>Valencia's Current Policy: <a href="policy/documents/Volume10/10-05-Smoking-Regulations.pdf" target="_blank">Smoking Regulations - Policy: 6Hx28:10-05</a>
                              
                           </li>
                           
                           <li>ACHA Statement: <a href="http://www.unh.edu/health-services/ohep/pdf/tobacco_ACHA-statement.pdf" target="_blank">Position Statement on Tobacco on College and University Campuses</a>
                              
                           </li>
                           
                           <li>Colleges and Universities with Policies: <a href="http://www.no-smoke.org/pdf/smokefreecollegesuniversities.pdf" target="_blank">U.S. Colleges and Universities with Smokefree Air Policies</a>
                              
                           </li>
                           
                           <li>Valencia Wellness: <a href="../HR/wellness/index.html" target="_blank">http://preview.valenciacollege.edu/HR/wellness</a>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Your thoughts and ideas about this important new initiative are critical to implementing
                           a successful policy. Please email us at <a href="mailto:smokefree@valenicacollege.edu">smokefree@valenicacollege.edu</a> and let us know what you think. 
                        </p>
                        
                        
                        <h3>Notification of Social Security Number Collection and Usage</h3>
                        
                        <p>In compliance with FL Statute 119.071(5), this document serves to notify you of the
                           purpose for the collection and usage of your Social Security Number (SSN). Valencia
                           Community College (Valencia) collects and uses your SSN only for the following purposes
                           in performance of the College's duties and responsibilities. To protect your identity,
                           Valencia will secure your SSN from unauthorized access, strictly prohibits the release
                           of your SSN to unauthorized parties contrary to state and federal law, and assigns
                           you a unique student/employee identification number. This unique ID number is used
                           for all associated employment and educational purposes at Valencia.
                        </p>
                        
                        
                        <h4>Employees</h4>
                        
                        <p>Your SSN is used for legitimate business purposes for completing and processing the
                           following:
                        </p>
                        
                        <ul>
                           
                           <li>Federal I-9 (Department of Homeland Security)</li>
                           
                           <li>Federal W4, W2, 1099 (Internal Revenue Service)</li>
                           
                           <li>Federal Social Security taxes (FICA)</li>
                           
                           <li>Distributing Federal W2 (Internal Revenue Service)</li>
                           
                           <li>Unemployment Reports (FL Dept of Revenue)</li>
                           
                           <li>Florida Retirement Contribution reports (FL Dept of Revenue)</li>
                           
                           <li>Workers Comp Claims (FCCRMC and Department of Labor)</li>
                           
                           <li>Direct Deposit Files</li>
                           
                           <li>403b contribution reports</li>
                           
                           <li>Group health, life and dental coverage enrollment</li>
                           
                           <li>Supplemental insurance deduction reports</li>
                           
                           <li>Work study work assignments</li>
                           
                           <li>Background checks</li>
                           
                        </ul>
                        
                        <p>Providing your Social Security Card is a condition of employment at Valencia.</p>
                        
                        <h4>Students</h4>
                        
                        <h5>Admissions</h5>
                        
                        <p>Federal legislation relating to the Hope Tax Credit requires that all postsecondary
                           institutions report student SSN's to the Internal Revenue Service (IRS). This IRS
                           requirement makes it necessary for community colleges to collect the SSN of every
                           student. A student may refuse to disclose his or her SSN to the College for this purpose,
                           but the IRS is then authorized to fine the student in the amount of $50.00.
                        </p>
                        
                        <p>In addition to the federal reporting requirements, the public school system in Florida
                           uses SSN's as a student identifier (section 1008.386, F.S.). In a seamless K-20 system,
                           it is beneficial for postsecondary institutions to have access to the same information
                           for purposes of tracking and assisting students in the smooth transition form one
                           education level to the next.
                        </p>
                        
                        <h5>Financial Aid</h5>
                        
                        <p>A student's SSN is required for the following financial aid purposes: The United States
                           Department of Education's (USDOE) Free Application for Federal Student Aid (FAFSA)
                           requires all applicants to report their SSN to be used for all federal financial aid
                           programs as a student identifier for processing and reporting. In addition to its
                           use by USDOE as a student identifier, the SSN is required in order for the Department
                           of Homeland Security to investigate citizenship status, for the Federal Work Study
                           Program, and is required on all loan applications for use by the lender/servicer/guarantor.
                        </p>
                        
                        <p>Valencia collects a student's SSN on certain institutional scholarship applications
                           for student files and federal and state audit/reporting purposes.
                        </p>
                        
                        <p>If you are a recipient of a State of Florida grant or scholarship such as the Florida
                           Student Assistance Grant, Florida Work Experience or Bright Futures the State of Florida
                           Department of Education will require the use of the SSN on their grant/scholarship
                           disbursement website and for reporting purposes.
                        </p>
                        
                        <h5>Library</h5>
                        
                        <p>Student, faculty, and staff Valencia ID (VID) number will be used in the libraries'
                           patron database (LINCC) for online login authentication, patron verification, and
                           the elimination of duplicate records.
                        </p>
                        
                        <h5>Outreach Programs</h5>
                        
                        <p>The Bridges and College Reach-Out Programs are youth outreach (intervention) projects
                           funded by discretionary grants from the US or FL Departments of Education. In order
                           to verify a participant's project eligibility, social security numbers are required
                           and also later used when submitting information for the Annual Performance Reports
                           due to the US or FL Departments of Education.
                        </p>
                        
                        <h5>Workforce Programs</h5>
                        
                        <p>These programs, funded through the Agency for Workforce Innovation (AWI), use your
                           SSN as an identifier for program enrollment and completion. Also, it is used for entering
                           placement information into either the OSMIS or the Employ Florida Marketplace statewide
                           data collection and reporting system. Because these are performance based contract
                           programs, AWI requires that all participants and their program related activities
                           be recorded in the Florida state system.
                        </p>
                        
                        
                        <h3>Contractors</h3>
                        
                        <p>Valencia collects contractor SSN information in order to file the required information
                           returns with the Internal Revenue Service, as required and authorized by federal law.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/general-counsel/spotlight-archives.pcf">©</a>
      </div>
   </body>
</html>