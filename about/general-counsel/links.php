<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Links and Resources  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/general-counsel/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/general-counsel/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office of Policy and General Counsel</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/general-counsel/">Policy &amp; General Counsel</a></li>
               <li>Links and Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Links and Resources</h2>
                        
                        
                        <h3>State Resources</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.flcourts.org/">Florida Supreme Court Decisions</a></li>
                           
                           <li><a href="http://myfloridalegal.com/opinions">Attorney General Opinions</a></li>
                           
                           <li><a href="http://www.myflsunshine.com/sun.nsf/sunmanual">Florida Public Records Law</a></li>
                           
                           <li><a href="http://myfloridalegal.com/sunshine">Florida OpenMeetings Law</a></li>
                           
                           <li><a href="http://www.fldoe.org/">Florida Department of Education</a></li>
                           
                           <li><a href="http://www.flsenate.gov/statutes/index.cfm?App_mode=Display_Index&amp;Title_Request=XLVIII#TitleXLVIII">Chapter 1000-1013 of the Florida Statutes</a></li>
                           
                           <li><a href="http://www.leg.state.fl.us/">Florida Statutes</a></li>
                           
                           <li><a href="http://www.myflorida.com/">www.myflorida.com</a></li>
                           
                           <li><a href="http://www.flsenate.gov/">Florida State Senate</a></li> 
                           
                           <li><a href="http://www.myfloridahouse.gov/">Florida State House of Representatives</a></li>
                           
                        </ul>
                        
                        
                        <h3>County Resources</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.onetgov.net/">Orange County Government</a></li>
                           
                           <li><a href="http://www.osceola.org/">Osceola County Government</a></li>
                           
                        </ul>
                        
                        
                        <h3>City Resources</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.cityoforlando.net/">City of Orlando</a></li>
                           
                           <li><a href="http://www.stcloud.org/">City of St. Cloud</a></li>
                           
                           <li><a href="http://www.orlando.org/">Orlando Regional Chamber of Commerce</a></li>
                           
                           <li><a href="http://www.business-orlando.org/">Metro Orlando Economic Development Commission</a></li>         
                           
                           <li><a href="http://www.kissimmee.org/">City of Kissimmee</a></li>
                           
                           <li><a href="http://www.cityofwinterpark.org/">City of Winter Park</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/general-counsel/links.pcf">©</a>
      </div>
   </body>
</html>