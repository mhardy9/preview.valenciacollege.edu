<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sustainability | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/campuses/university-center.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/campuses/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/campuses/">Campuses</a></li>
               <li>Sustainability</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>University Center</h2>
                        
                        
                        <p>Valencia's largest building is a three-story, 100,000 square foot $23 million facility
                           that houses classes for the University of Central Florida (UCF) and Valencia students.
                           Located on the West Campus, the building includes more than 40 classrooms, a state-of-the-art
                           testing center, computer labs, study rooms, faculty offices and a cafe.
                        </p>
                        
                        <p>The lights, windows, roof and wall insulation and air conditioning system are energy
                           efficient.  The University Center is 28 percent more efficient than a conventional
                           building, which results in about $35,000 in annual energy savings.
                        </p>
                        
                        <p>Solar panels installed on the roof provide 10 percent of the electricity that powers
                           the building.  The lights inside each room automatically adjust to the brightness
                           needed to illuminate the room.  Faucets use about 75 percent less water than conventional
                           faucets, saving about a half-gallon of water with each use.
                        </p>
                        
                        <p>More than 600,000 pounds of construction waste were recycled instead of being sent
                           to a landfill.
                        </p>
                        
                        <p>The ceiling tiles, floor tiles and carpets all contain recycled materials.  The counter
                           of the Little Bean Cafe is made up of shredded aluminum cans.
                        </p>
                        
                        <img alt="University Center" border="1" height="250" src="UniversityCenter.jpg" width="450">
                        <img alt="LEED" height="100" src="leedB.jpg" width="452">
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/" target="_blank" title="The Grove - Sustainability">
                              
                              LATEST SUSTAINABLE NEWS FROM
                              
                              <br>
                              <em>
                                 The Grove</em>            
                              </a>
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/dxO-IIUJFCE/" target="_blank">Sustainability Tip: Let’s Stay Sustainable This Halloween</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Oct 09, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/7DMKulMpwTw/" target="_blank">Help Save the College Money and Shut Down for the Labor Day Holiday</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Aug 29, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        <p>
                           
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All Articles</a>
                           
                           
                           
                        </p>
                        
                        
                        <p>
                           
                           <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank">
                              Subscribe</a>&nbsp; <img alt="RSS" border="0" src="icon_rss.png">
                           
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/campuses/university-center.pcf">©</a>
      </div>
   </body>
</html>