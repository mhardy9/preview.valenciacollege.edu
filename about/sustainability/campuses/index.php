<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sustainability | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/campuses/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/campuses/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li>Campuses</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Our Green Buildings </h2>
                        
                        <p>Since 2006, Valencia College has built all new buildings to LEED Silver or Green Globes
                           Level 2. We hire architects and building contractors with LEED AP qualifications and
                           require that they design and implement to follow the LEED certification criteria or
                           more recently to Green Globes standards. 
                        </p>
                        
                        <p>Guidelines are provided to architects upon new construction. Compliance is assured
                           through design review process and inspection by superintendents and Asst VP for Facilities
                           and Sustainability.
                        </p>
                        
                        <p><a href="documents/Valencia_College_Green_Buildings_features.pdf">General Information on our Green Buildings </a></p>
                        
                        <p>Valencia College has been working to protect indoor air quality for students, faculty,
                           staff and visitors. We are now a <a href="http://smokefree.valenciacollege.edu">Smoke Free Campus</a>. We work hard to prevent mold and mildew and to eliminate volatile organic compounds
                           in paints, carpets, furniture, and equipment used. We use green housekeeping products.
                           
                        </p>
                        
                        <p><a href="../../about/facilities/documents/AEGuidelinesFinal.pdf.html">Architectural and Engineering Guidelines</a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/OperationsandMaintenanceGreenGuidelines9_5_13.pdf">Operations and Maintenance Guidelines </a></p>
                        
                        <p>See tabs at left for information on each campus and our green buildings.</p>
                        
                        <h2>Our Green Campuses</h2>
                        
                        <p>
                           All of the Valencia College campuses have elements of the natural Central Florida
                           environment. We seek to maintain 
                           and enhance these to use each Campus as a Living Laboratory. Special attention is
                           given to using Florida native plants and create wildilfe habitat.
                        </p>
                        
                        <p><strong>Tree Campus USA </strong></p>
                        
                        <p>Valencia College has been awarded the prestigious Tree Campus USA designation by the
                           Arbor Day Foundation, and is one of only 7 colleges in Florida to achieve this status
                           this year. The college achieved this designation by meeting the required five core
                           standards for sustainable campus forestry: a tree advisory committee, a campus tree-care
                           plan, dedicated annual expenditures for its campus tree program, an Arbor Day observance
                           and the sponsorship of student service-learning projects.More information about the
                           Tree Campus USA program is available at <a href="http://www.arborday.org/TreeCampusUSA">www.arborday.org/TreeCampusUSA</a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/VCTreeCarePlan2012_Final.pdf">Tree Campus USA 2012</a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/ServiceLearning2012_FINAL.pdf">Tree Campus Service Learning Project 2012</a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/AborDayObservance2012_Final.pdf">Arbor Day Observances 2012 </a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/2011TreeCarePlan_ApplicationforTreeCampus.pdf">Tree Campus USA 2011 </a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/2011TreeInventorySERVICELEARNING.pdf">Tree Campus Service Learning Project 2011<br>
                              </a><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/2011TreeInventorySERVICELEARNING.pdf"><br>
                              </a><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/ArborDayObservance2011.pdf">Arbor Day Observance 2011 </a></p>
                        
                        <p>Valencia College received its Tree Campus USA plaque and flag at our March 29, 2012
                           Arbor Day Foundation Tree Planting Event. Jennifer Boettcher of the Arbor Day Foundation
                           and Charlie Marcus, of the Florida Forest Service, presented Valencia with its Tree
                           Campus USA plaque and flag.
                        </p>
                        
                        <p><a href="http://thegrove.valenciacollege.edu/employees-students-plant-100-trees-on-west-campus/">Tree Campus USA Planting Event March 29.</a> The grant to purchase 100 large trees for planting came from the Arbor Day Foundation
                           in conjunction with the Association for Advancement of Sustainability in Higher Education,
                           with support from Toyota. Valencia is the only two-year college to receive a tree-planting
                           grant this year – and it’s just the second community college to receive the grant
                           since the Tree Campus program began in 2008. The other colleges receiving grants this
                           year are Purdue University, University of Colorado, Colorado State University, Virginia
                           Commonwealth University, Hobart and William Smith Colleges, and University of Illinois,
                           Chicago.
                        </p>
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/NewStudentTours-allcampuses.pdf">New Student Orientation Tours</a> 
                        </p>
                        
                        <p><a href="documents/WestCampusWALKINGFIELDGUIDEchecklist2.pdf">Plant and Habitat Checklist West Campus</a><br>
                           <a href="documents/WestCampus_ANIMALCHECKLIST20080519.pdf">Animal Checklist West Campus </a></p>
                        
                        <p><strong>Native Plants and Xeriscape </strong></p>
                        
                        <p>Valencia College has the following policy in its Architectural and Engineering Guidelines
                           for new construction: 2.11 Landscaping 2.11.1 Statement of Intent
                        </p>
                        
                        <p>Landscape designs should be developed to provide landscaping that is relatively low
                           cost and low maintenance and should emphasize simplicity, balance and ecological sensitivity.&nbsp;
                           Designs should incorporate plant materials that are water wise, disease, pest and
                           drought tolerant.&nbsp; The use of native plant material and natural plant arrangements
                           is strongly encouraged, whenever possible. Natural landscaping is considered an important
                           component of the design and the designer shall make every effort to incorporate existing
                           natural landscapes into the design and to preserve any natural vegetation on the site.&nbsp;
                           Landscape and site designs should preserve existing trees to the maximum extent possible.
                           Proposed removal of existing trees shall be thoroughly evaluated before committing
                           to a design strategy.&nbsp; The college encourages the preservation of wildlife habitat
                           and the consideration of wildlife use in plant material selection. Landscape designs
                           shall be based on the long term cost effectiveness and sustainability of the materials
                           selected.&nbsp; The use of materials requiring excessive pruning, which drop noxious fruit
                           or plant parts is discouraged.&nbsp; Landscape designs shall be cognizant of the need for
                           a safe and secure environment for campus users.&nbsp; The use of inappropriate plant materials
                           (e.g., poisonous, sharp needled, nuisance or invasive) is discouraged.&nbsp; Landscape
                           and pedestrian area lighting shall be incorporated into the landscape design.&nbsp; In
                           general, all designs shall promote the principles of low cost, safe, sustainable,
                           cost efficient and low maintenance landscapes.
                        </p>
                        
                        <p>See also<a href="http://preview.valenciacollege.edu/sustainability/action/water-efficiency.cfm"> Outdoor Water Use Efficiency</a> 
                        </p>
                        
                        <p><strong>Wildlife Habitat</strong> 
                        </p>
                        
                        <p>Valencia College campuses are managed as Wildlife Habitat to the extent possible.
                           We have installed wood duck and other bird boxes on West Campus, an osprey platform
                           on Osceola Campus, and are working on bat boxes, with monitoring for student research
                           on West and East Campuses. We maintain native habitat, remove invasive plants to the
                           extent possible, and plant native plants.
                        </p>
                        
                        <p><strong>Master Planning with the Environment in mind </strong></p>
                        
                        <p>Valencia College has contracted with landscape and design firms to produce Master
                           Plans for our campuses with the environment in mind. 
                        </p>
                        
                        <p>Here are sample pages of plans for East and West Campuses. A Master Plan for Osceola
                           Campus, to guide further native plant landscaping, is currently being developed. For
                           further information on these plans, contact our Director of Sustainability at dgreen1@valenciacollege.edu
                           or 407-582-1830. 
                        </p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/LAKEPAMELADesignIntent2009-p.1-5.pdf">West Campus LAKE PAMELA Design Intent 2009-partial</a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/EastCampusMasterPlanpartial2009CTHsu.pdf">East Campus Master Plan partial 2009 CTHsu</a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/sustainability/locationses/documents/EastCampusGreenPlan2011-partialAecom.pdf">East Campus Green Plan2011-partial Aecom</a></p>
                        
                        <p><strong>Integrated Pest Management</strong> 
                        </p>
                        
                        <p>Valencia College Grounds Services Departments follow an integrated pest management
                           plan using the U.S. Environmental Protection Agency’s four-tiered approach to integrated
                           pest management as follows:
                        </p>
                        
                        <p>1)&nbsp; Set action thresholds - Valencia College Grounds Services Departments will use
                           a threshold of greater than 65% infestation prior to instituting chemical control.
                           <br>
                           2)&nbsp; Monitoring - The Grounds Services Departments will monitor and identify pests
                           on a routine basis.<br>
                           3)&nbsp; Prevention - IPM programs work to manage the crop, lawn, or indoor space to prevent
                           pests from becoming a threat. The Design Choices and Cultural Practices described
                           in these guidelines are Prevention measures.<br>
                           4)&nbsp; Control - The college uses mechanical control, such as trapping or weeding, and
                           where chemical treatment seems to be the only resort, organic and biological controls
                           are preferentially chosen. 
                        </p>
                        
                        <p><strong>Landscape Waste Composting </strong></p>
                        
                        <p>East Campus and Osceola Campus have compost piles for landscape waste, with East Campus
                           grinding its woody material. For all of our campuses Municipal Solid Waste goes to
                           the Orange County Landfill which charges for disposal of Yard Waste. Yard waste collected
                           from Orange County homes and businesses is composted and made available to residents
                           at no charge. Grounds crew periodically picks up and uses this compost. 
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/" target="_blank" title="The Grove - Sustainability">
                              
                              LATEST SUSTAINABLE NEWS FROM
                              
                              <br>
                              <em>
                                 The Grove</em>            
                              </a>
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/dxO-IIUJFCE/" target="_blank">Sustainability Tip: Let’s Stay Sustainable This Halloween</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Oct 09, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/7DMKulMpwTw/" target="_blank">Help Save the College Money and Shut Down for the Labor Day Holiday</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Aug 29, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        <p>
                           
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All Articles</a>
                           
                           
                           
                        </p>
                        
                        
                        <p>
                           
                           <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank">
                              Subscribe</a>&nbsp; <img alt="RSS" border="0" src="icon_rss.png">
                           
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/campuses/index.pcf">©</a>
      </div>
   </body>
</html>