<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sustainability | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/action/water-efficiency.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/action/">Action</a></li>
               <li>Sustainability</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Water Efficiency</h2>
                        
                        
                        <h3>
                           Outdoor Water Use Efficiency 
                           
                        </h3>
                        
                        <p>
                           As early as 2006, Valencia College contracted for landscape master plans for East
                           Campus and West Campus from GAI Consultants. These plans recommended a reduction in
                           irrigation, fertilization, and pesticides through the changeover from St. Augustine
                           grass to drought-tolerant Bahia grass and various native plants.
                           
                        </p>
                        
                        <p>
                           This changeover was carried out on East Campus from 2006 on and adopted slightly later
                           on West Campus. Use of drought-tolerant Bahia and native plants is now our standard
                           on all campuses. We also use very limited amounts of fertilizer and pesticides, following
                           Integrated Pest Management techniques. 
                           
                        </p>
                        
                        <h3>
                           Rainwater Harvesting
                           and Reclaimed           
                        </h3>
                        
                        <p>
                           On West Campus the Special Events Center has a 9000 gallon underground cistern that
                           provides water for flushing on the low-volume toilets. On Osceola Campus, the new
                           Building 4 also has a cistern storing 10,000 gallons of rainwater for flushing toilets.
                           
                        </p>
                        
                        <p> Reclaimed wastewater provided by the wastewater utility is used for irrigation on
                           our largest campuses: West Campus, East Campus, Osceola Campus, and Lake Nona Campus
                           
                        </p>
                        
                        <h3>
                           Stormwater Management 
                           
                        </h3>
                        
                        <p>
                           Valencia College recognizes that by decreasing stormwater runoff and treating stormwater
                           on site, we can help replenish natural aquifers, reduce erosion impacts, and minimize
                           local water contamination. We construct and maintain retention ponds and use vegetated
                           swales to minimize stormwater runoff from our properties. All practices are developed
                           in coordination with the permitting process of the St. Johns River Water Management
                           District and/or South Florida Water Management District. 
                           
                        </p>
                        
                        <p>
                           Florida Friendly Best Management Practices for Protection of Water Resources by the
                           Green Industries are followed (<a href="http://fyn.ifas.ufl.edu/">University of Florida Friendly Landscaping</a>). 
                           
                        </p>
                        
                        <h3>
                           Indoor Water Use Efficiency
                           
                        </h3>
                        
                        <p>
                           Valencia College takes advantage of water efficiency advances on a national scale,
                           including the <a href="http://www.map-testing.com/">Maximum Performance (MaP) testing</a> for toilets and <a href="http://www.epa.gov/watersense/">EPA WaterSense</a> labeling program (similar to the EPA's Energy Star program)  
                           
                        </p>
                        
                        <p>
                           We use dual-flush toilets and others using no more than 1.28 gallons per flush for
                           new installations and retrofits. Automatic flushing toilets have been chosen by the
                           college to assist maintenance crews. If you are aware of a toilet on campus that is
                           functioning improperly- giving extra flushes- please contact us at the number below.
                           
                           
                        </p>
                        
                        <p>
                           We install urinals that use only 0.25 gallons per flush. This is a tremendous savings
                           over older urinals that used as much as 3 gallons per flush.
                           
                        </p>
                        
                        <p>
                           We install faucets with aerators flowing at the rate of 0.5 gallons per minute or
                           less. 
                           
                        </p>
                        
                        <h3>
                           Cooling Towers 
                           
                        </h3>
                        
                        <p>
                           Valencia College has converted most of its chillers from air-cooled to water-cooled
                           to take advantage of higher efficiencies, but with this conversion water use went
                           up drastically.  In June 2007 on West Campus 1,200 tons of Chiller Water provided
                           cooling for nine buildings.  The monthly potable water consumption was 1,835,000 gallons
                           with an operating cost was $11,634.00. 
                           
                        </p>
                        
                        <p>
                           With the growth of the college came construction of our new Central Energy Plant and
                           the chilled water loop was expanded to include 2 new buildings and one that had previously
                           used air-cooled chillers. By 2009, another building was added to the loop which raised
                           the monthly consumption to 2,163,000 gallons with costs of $14,891.00.
                           
                        </p>
                        
                        <p>
                           In an effort to reduce this large consumption of potable water in our chiller plants,
                           in 2010 we engaged the services of CG Solutions to provide a low water usage chemical
                           treatment program. In the first year of using this chemical treatment, Valencia saved
                           9,369,000 gallons of billed water. This translates into savings of $102,747 for the
                           year.  In addition to the potable water savings, Valencia currently has a water softener
                           for the cooling tower make-up water.  By reducing its water consumption, Valencia
                           was also able to reduce its salt consumption, thus producing another $9,000 annually
                           in savings.  Overall the savings realized is $111,000.00; a remarkable achievement
                           for the investment.  
                           
                        </p>
                        
                        <p>
                           With these new techniques, the HVAC equipment has continued performing well. The condenser
                           approaches never exceeded 0.5 degrees even during the hottest days of the summer.
                           The tubes were clean, but not shiny and the face sheets showed no signs of pitting,
                           scale or corrosion.
                           
                        </p>
                        
                        <h3>
                           Ice-Machines
                           
                        </h3>
                        
                        <p>
                           All new installations and any replacements we use air-cooled ice machines, that are
                           
                           ENERGY STAR labeled. See <a href="http://www.energystar.gov">www.energystar.gov</a>. 
                           
                        </p>
                        
                        <p>
                           For information on water efficiency or to report water wastage, contact Deborah Green,
                           Director of Sustainability, at dgreen1@valenciacollege.edu or 407-582-1830.
                           
                        </p>
                        
                        
                        <p><a href="water-efficiency.html#">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/" target="_blank" title="The Grove - Sustainability">
                              
                              LATEST SUSTAINABLE NEWS FROM
                              
                              <br>
                              <em>
                                 The Grove</em>            
                              </a>
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/7DMKulMpwTw/" target="_blank">Help Save the College Money and Shut Down for the Labor Day Holiday</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Aug 29, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/m6mpON46gI0/" target="_blank">Sustainability Tip: It Pays To Reduce, Reuse and Recycle</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Aug 15, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        <p>
                           
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All Articles</a>
                           
                           
                           
                        </p>
                        
                        
                        <p>
                           
                           <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank">
                              Subscribe</a>&nbsp; <img alt="RSS" border="0" src="icon_rss.png">
                           
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/action/water-efficiency.pcf">©</a>
      </div>
   </body>
</html>