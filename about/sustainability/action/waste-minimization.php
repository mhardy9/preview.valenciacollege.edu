<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sustainability | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/action/waste-minimization.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/action/">Action</a></li>
               <li>Sustainability</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Waste Minimization &amp; Recycling</h2>
                        
                        <p><img alt="Recycle" height="357" src="recyclemania-win-wm-grove_001.png" width="570"></p>
                        
                        <p><a href="http://thegrove.valenciacollege.edu/valencia-wins-again-in-recyclemania-waste-minimization-competition/">We Won Recyclemania 2013! </a>Article in The Grove
                        </p>
                        
                        <p><strong><a href="http://thegrove.valenciacollege.edu/recyclemania-2013-another-opportunity-for-national-recognition/?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+valenciasustainability+(The+Grove+%7C+Valencia+College+%C3%AF%C2%BF%C2%BD+Sustainability)">Recyclemania 2013 Underway- February 3 to March 23</a></strong></p>
                        
                        <p>February 3, 2013 started Valencia College’s fourth year competing in the Recyclemania
                           competition. <a href="http://www.recyclemaniacs.org">Recyclemania</a> is a national program, supported by the US EPA’s WasteWise program, in which colleges
                           and universities compete for awards. In 2012, Valencia College won First Prize nationally
                           in the Waste Minimization category. The trophy Mr. Pinman passed to us from North
                           Lake College in Texas, who had kept it for several years. 
                        </p>
                        
                        <p>We hope to keep this trophy for 2013 also, and while Jerry Cochran, Valencia's Recycling
                           Coordinator who initiated the program, has recently retired, the college’s recycling
                           effort and our enthusiastic competition in Recyclemania continue. Recyclemania encourages
                           us to maximize diversion of the college’s waste from the landfill so that we reduce
                           waste disposal costs, stimulate public awareness and show sensitivity to the environment.
                           Marty Campbell, Superintendent Osceola Campus, and Boris Feijoo, Trades/Maintenance
                           Supervisor, Osceola Campus have taken on the coordination of Recycling, in coordination
                           with the Office of Sustainability. 
                        </p>
                        
                        <p>Recyclemania is a lot of work for the coordinators, as they must report poundage of
                           recycled material and of total waste weekly for ten weeks (and dumpsters are normally
                           not weighed). According to the rules, reporting must include all campuses<em>.</em></p>
                        
                        <p>Since 2008, Valencia College has had bins for aluminum cans, plastic bottles and mixed
                           paper on all campuses. Purchase of these bins was accomplished through the money saved
                           in our waste disposal through the recycling program.
                        </p>
                        
                        <p>Receptacles for non-recyclable materials were labeled “Landfill” last year to remind
                           everyone where the trash goes and to help students, faculty, and staff get in the
                           habit of looking for a recycle bin. Please let the new recycling coordinators, custodial
                           supervisors or Office of Sustainability know if there are Landfill bins with no Recycle
                           bins nearby. The idea is that everyone always has a choice of where to throw items
                           without walking too far.
                        </p>
                        
                        <p>Gray bins for mixed paper are in all the classrooms and office areas, and you see
                           a location with extensive paper use without these bins, please contact the Office
                           of Sustainability. 
                        </p>
                        
                        <p>Although many people bring recyclables from home if they do not have curbside recycling
                           for their neighborhoods or apartments, everyone is requested <u>not</u> to bring materials from home between February 3 and March 23, as this is supposed
                           to reflect our school’s waste stream only. 
                        </p>
                        
                        <h2><img alt="We Won Recyclemania" height="276" src="WewonRecyclemania.jpg" width="327"></h2>
                        
                        
                        <div>
                           
                           
                        </div>
                        
                        <p>The goal of Valencia's recycling program is to maximize the diversion of in-house
                           generated waste material from disposal at the landfill for the purpose of reducing
                           waste disposal costs, stimulating public awareness and showing sensitivity to the
                           environment.
                        </p>
                        
                        <p>The vision of Valencia's recycling program is to expand into recycling of other materials
                           which will be phased in as our program advances. An additional target is to get students
                           involved as stakeholders to share in the experience of bettering the world around
                           us through recycling.            
                           
                        </p>
                        
                        <p>Our program started in February 2007 with the recycling of paper at our East and West
                           Campuses. Our Winter Park Campus, and The Criminal Justice Institute have been integrated
                           into the program throughout the summer months of 2007. Starting in the fall semester
                           of 2008, we are going to introduce the collection of aluminum cans, plastic bottles
                           and mixed paper into our program at all Valencia campuses.            
                           
                        </p>
                        
                        <p>We are proud to announce that in July, 2010, after all of the recycling bills were
                           paid, we processed enough recycling to receive a check for $242.19 from our recycling
                           company.  The program has proven itself profitable for Valencia College.  View our
                           <a href="Recycle-Stats.html"><span>facts and statistics</span></a>.
                        </p>
                        
                        
                        <p><span>Valencia Recycling Procedures </span></p>
                        
                        <ul>
                           
                           <li>Large blue, green, and gray recycling containers will be located at the end of each
                              building on each campus. The containers will have our," Valencia Recycles" logo stamped
                              on them and will be clearly labeled for the particular recyclables to be placed in
                              each. Blue containers are for aluminum cans, green containers for plastic drink bottles,
                              and gray containers for mixed paper.
                           </li>
                           
                           <li>Custodial staffs collect the contents and deposit them into a larger recycling container.
                              The recyclable materials are then collected by a contracted company and transported
                              to a recycling center.
                           </li>
                           
                        </ul>
                        
                        <p>Plant Operations is responsible for administration and coordination of the recycling
                           program. Please contact Jerry Cochran, Valencia's Recycling Coordinator, via e-mail
                           <a href="mailto:jcochran@valenciacollege.edu">jcochran@valenciacollege.edu</a>, should you have any questions about our recycling program.
                           
                        </p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <img src="recycling1.jpg"><br>
                                    <span>(Lizza Burgos-Reynoso from the Security Department demonstrating our new recycle containers.)</span>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img src="recycling2.jpg"><br>
                                    <span>(Lizza Burgos-Reynoso from the Security Department demonstrating our new recycle containers.)</span>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        <p>Paper recycling bins have been placed in all classrooms. Faculty members-Please encourage
                           students to utilize the recycling bins.
                        </p>
                        
                        <p>In efforts to minimize waste, Valencia College has also installed water refilling
                           stations, and is encourging less use of throwaway plastic water bottles. Refillable
                           bottles in different styles are for sale at the Valencia College bookstores. 
                        </p>
                        
                        <p><span>Electronic Waste  Recycling Program</span></p>
                        
                        <p>Valencia College has verbal agreements with three recyclers (A1-Assets, Orlando Recyclers
                           and Southeastern Data) to pick up and recycle college electronic and some other surplus
                           equipment. These agreements have been in place for more than five years. Any data
                           from hard drives is wiped clean or destroyed. The equipment may be re-sold and the
                           college gets 50% of the proceeds from the sale or the equipment is destroyed and disposed
                           of as recycled material. Leased copiers are returned to the vendor at the end of its
                           life cycle or at the end of the lease. Purchased copiers are still disposed of through
                           our current recyclers the same as all of our other electronics. Surplus electronics
                           and/or furniture are donated to non-profits when they request items or when the college
                           has equipment that will benefit the community better with a second life.
                        </p>
                        
                        <p><strong><span>Hazardous Waste Disposal </span></strong></p>
                        
                        <p>Valencia College instituted green cleaning practices in 2006, which have reduced our
                           hazardous chemical waste. We also have implemented Integrated Pest Management which
                           has virtually eliminated our use of pesticides. 
                        </p>
                        
                        <p>Additionally, the science departments have implemented multi student lab experiments
                           in order to reduce the amount of waste produced. In 2008, the college’s Science departments
                           have implemented greener chemicals for classroom experiments. 
                        </p>
                        
                        <p>Hazardous, Universal and non-regulated chemical waste is collected on our three major
                           campuses and recycled through licensed disposal companies. Valencia College contracts
                           with an outside firm to correctly dispose of all fluorescent light bulbs.
                        </p>
                        
                        <p><img alt="Recycle at Valencia" src="recyclingB.jpg" width="490"></p>
                        
                        <p><a href="waste-minimization.html#">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/" target="_blank" title="The Grove - Sustainability">
                              
                              LATEST SUSTAINABLE NEWS FROM
                              
                              <br>
                              <em>
                                 The Grove</em>            
                              </a>
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/7DMKulMpwTw/" target="_blank">Help Save the College Money and Shut Down for the Labor Day Holiday</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Aug 29, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/m6mpON46gI0/" target="_blank">Sustainability Tip: It Pays To Reduce, Reuse and Recycle</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Aug 15, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        <p>
                           
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All Articles</a>
                           
                           
                           
                        </p>
                        
                        
                        <p>
                           
                           <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank">
                              Subscribe</a>&nbsp; <img alt="RSS" border="0" src="icon_rss.png">
                           
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/action/waste-minimization.pcf">©</a>
      </div>
   </body>
</html>