<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Plan  | Valencia College</title>
      <meta name="Description" content="Action Plan | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, action, plan">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/about-us/action-plan.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/about-us/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		About Us
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/about-us/">About Us</a></li>
               <li>Action Plan </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Sustainability Action Plan</h2>
                        
                        <p>A Sustainability Action Plan is a long term planning  document that organizations
                           develop to identify focus areas important to their  sector and outline specific strategies
                           within each of those areas that are  customized for the institution. For Valencia
                           College, this sector is higher  education and our seven focus areas include: 
                        </p>
                        
                        <ol>
                           
                           <li>Transportation</li>
                           
                           <li>Building Energy</li>
                           
                           <li>Education and Outreach</li>
                           
                           <li>Solid Waste</li>
                           
                           <li>Water Efficiency</li>
                           
                           <li>Grounds</li>
                           
                           <li><a href="http://theseedcenter.org/getattachment/Resources/SEED-Resources/SEED-Toolkits/Campus-as-a-Living-Lab/Campus-as-a-Living-Lab.pdf">Campus  as a Living Laboratory</a></li>
                           
                        </ol>
                        
                        <p>Read the complete report <a href="../action/documents/ValenciaCollegeSustainabilityPlanOctober2012.pdf">here</a>.  This report was developed as part of our STARS certification in 2012. To  faculty,
                           please consider integrating this action plan into your curriculum by  aligning class
                           projects and assignments with our goals. Students get to work on  real world problems
                           and have the opportunity to get hands on experience right  on campus. 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/about-us/action-plan.pcf">©</a>
      </div>
   </body>
</html>