<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>STARS  | Valencia College</title>
      <meta name="Description" content="STARS | About Us | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, about, stars">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/about-us/stars.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/about-us/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		About Us
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/about-us/">About Us</a></li>
               <li>STARS </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>STARS Credit &amp; Social Responsibility</h2>
                        
                        <p><img alt="/img/about/sustainability/STARS_Silver.png" height="113" hspace="12" src="/_resources/img/about/sustainability/STARS_silver.png" width="113">STARS  Silver<br>
                           
                        </p>
                        
                        <p>Valencia College is proud to have achieved the <a href="https://stars.aashe.org/">STARS (Sustainability Tracking and Rating  System)</a> Silver Rating from the AASHE (Association for the Advancement of  Sustainability
                           in Higher Education) in October 2012. We are among 15 two-year  colleges around the
                           country to have achieved this rating, with only one having  achieved STARS Gold.
                        </p>
                        
                        <p> <br>
                           See our scorecard <a href="https://stars.aashe.org/institutions/valencia-community-college-fl/report/2012-10-08/">here</a>. 
                        </p>
                        
                        <ul>
                           
                           <li>Recognition</li>
                           
                           <ul>
                              
                              <li><a href="http://www.nacubo.org/Documents/BusinessPolicyAreas/Sustainability%20Campus%20Posters/ValenciaCollege.pdf">NACUBO</a></li>
                              
                              <li>2012,  2013, and 2014 <a href="http://thegrove.valenciacollege.edu/?s=recyclemania">Recyclemania  Waste Minimization Champions</a>
                                 
                              </li>
                              
                              <li><a href="http://secondnature.org/award_year/2014/">2014 Climate Leadership Award</a></li>
                              
                              <li>Metroplan Orlando Clean Air Award -Honorable  Mention</li>
                              
                              <li><a href="http://thegrove.valenciacollege.edu/chevy-carbon-reduction-initiative-project-wins-two-awards/">Chevy  Carbon Reduction Pilot Partnership</a></li>
                              
                           </ul>
                           
                           
                        </ul>
                        
                        <ul>
                           
                           <li>Partner Organizations</li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/about-us/stars.pcf">©</a>
      </div>
   </body>
</html>