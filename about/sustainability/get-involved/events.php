<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Events  | Valencia College</title>
      <meta name="Description" content="Events | Get Involved | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, get, involved, events">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/get-involved/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/get-involved/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		Get Involved
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/get-involved/">Get Involved</a></li>
               <li>Events </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3>Annual Events</h3>
                        
                        
                        <ul>
                           
                           <li>January </li>
                           
                           <ul>
                              
                              <li><a href="../../PJI/events/index.html">Conversation  on Justice</a></li>
                              
                           </ul>
                           
                           <li>April </li>
                           
                           <ul>
                              
                              <li><a href="http://www.earthday.org/">Earth Day  (April 22nd)</a></li>
                              
                              <li><a href="https://www.arborday.org/">Arbor Day  (April 29th)</a></li>
                              
                           </ul>
                           
                           <li>September </li>
                           
                           <ul>
                              
                              <li><a href="../../PJI/events/index.html">Conversation  on Peace</a></li>
                              
                              <li><a href="http://greenapple.org/">USGBC Green  Apple Day of Service</a></li>
                              
                           </ul>
                           
                           <li>October </li>
                           
                           <ul>
                              
                              <li><a href="http://www.aashe.org/locations-sustainability-month/">AASHE Campus  Sustainability Day</a></li>
                              
                              <li><a href="http://conference.aashe.org/">AASHE Annual National Conference</a></li>
                              
                              <li><a href="https://greenbuildexpo.com/">USGBC Annual International Conference</a></li>
                              
                           </ul>
                           
                           <li>November </li>
                           
                           <ul>
                              
                              <li><a href="http://americarecyclesday.org/">America  Recycles Day</a></li>
                              
                           </ul>
                           
                        </ul>
                        
                        
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/get-involved/events.pcf">©</a>
      </div>
   </body>
</html>