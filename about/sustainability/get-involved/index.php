<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Get Involved  | Valencia College</title>
      <meta name="Description" content="Get Involved | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, resources, get, involved">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/get-involved/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/get-involved/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		Resources
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li>Get Involved</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3>Students</h3>
                        
                        
                        <ul>
                           
                           <li>Sustainability Committee</li>
                           
                           <li>Internships</li>
                           
                           <ul>
                              
                              <li>Credit and non-credit (volunteer) internships  with the Office of Sustainability are
                                 available through the College’s  Internship Office. 
                                 <p>Apply today: <a href="../../internship/students/internships.html">http://preview.valenciacollege.edu/internship/students/internships.cfm</a></p>
                                 
                              </li>
                              
                              <p>Or contact Resham Shirsat, Director of Sustainability at <a href="mailto:rshirsat@valenciacollege.edu">rshirsat@valenciacollege.edu</a> or  407-582-1830 directly with any questions or to explore opportunities. 
                                 
                              </p>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>Clubs</li>
                              
                              <ul>
                                 
                                 <li>VESA (Valencia Environmental Studies  Association) on East Campus</li>
                                 
                                 <ul>
                                    
                                    <li>Contact faculty advisor James Adamski at <a href="mailto:jadamski1@valenciacollege.edu">jadamski1@valenciacollege.edu</a>
                                       
                                    </li>
                                    
                                    <li>Visit and like <a href="https://www.facebook.com/clubvesa">VESA’s FaceBook page</a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <li>IDEAS (Intellectual Decisions for Environmental  Awareness Solutions) for Valencia
                                    on West Campus
                                 </li>
                                 
                                 <ul>
                                    
                                    <li>Contact faculty advisor James Vrhovac at <a href="mailto:jvrhovac@valenciacollege.edu">jvrhovac@valenciacollege.edu</a>
                                       
                                    </li>
                                    
                                    <li>Facebook: <a href="http://www.facebook.com/ideasforvalencia">www.facebook.com/ideasforvalencia</a>
                                       
                                    </li>
                                    
                                    <li>Instagram: ideasforvalencia </li>
                                    
                                    <li>Twitter: @ideasvalencia </li>
                                    
                                    <li>Email: <a href="mailto:valencia@ideasforus.org">valencia@ideasforus.org</a>
                                       
                                    </li>
                                    
                                    <li>Website: <a href="http://www.ideasforus.org">www.ideasforus.org</a>
                                       
                                    </li>
                                    
                                    <li>Visit and like <a href="https://www.facebook.com/ideasforvalencia/">IDEAS’ FaceBook page</a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </ul>
                              
                              <li><a href="events.html">Events</a></li>
                              
                              <li>Skillshops</li>
                              
                              <ul>
                                 
                                 <li>
                                    <em>Keeping  Cool, Clean and Green </em>(energy conservation &amp; efficiency; green  cleaning)
                                 </li>
                                 
                                 <li>
                                    <em>Careers  with Purpose </em>(careers in sustainability)
                                 </li>
                                 
                              </ul>
                              
                              <li>Sustainability Ambassadors</li>
                              
                              <ul>
                                 
                                 <li>Coming in 2017! Stay tuned. </li>
                                 
                                 <li>For credit (1-3 credits/20-60 hours per  semester) through <a href="../../students/service-learning/index.html">Service  Learning</a> or volunteer
                                 </li>
                                 
                                 <li>Focus areas include: recycling champions, energy  auditing, and behavioral change
                                    challenge
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           <br>
                           
                           <p><img alt="https://scontent-mia1-1.xx.fbcdn.net/v/t1.0-9/11109151_924655864246345_63695376722103538_n.jpg?oh=18f07a813f187793ec058c301a8bedfd&amp;oe=579EBA2C" height="161" src="/_resources/img/about/sustainability/default_clip_image002.jpg" width="121"></p>
                           
                           
                           <h3>&nbsp;</h3>
                           
                           
                           
                           
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/get-involved/index.pcf">©</a>
      </div>
   </body>
</html>