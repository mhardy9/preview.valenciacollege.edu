
<ul>
<li><a href="/about/sustainability/index.php">Sustainability</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">About Us <i aria-hidden="true" class="far fa-angle-down"></i></a>
<ul>
<li><a href="/about/sustainability/about-us/our-team.php">Our Team</a></li>
<li><a href="/about/sustainability/about-us/action-plan.php">Sustainability Action Plan</a></li>
<li><a href="/about/sustainability/about-us/stars.php">STARS</a></li>
<li><a href="http://blogs.valenciacollege.edu/sustainable/curriculum-development/">Academics</a></li>
<li><a href="about-us/committee.php">Sustainability Committee</a></li>
</ul>
</li>

<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Operations <i aria-hidden="true" class="far fa-angle-down"></i></a>
<ul>
<li><a href="/about/sustainability/operations/index.php">Energy and Climate</a></li>
<li><a href="/about/sustainability/operations/buildings.php">Buildings</a></li>
<li><a href="/about/sustainability/operations/grounds.php">Grounds</a></li>
<li><a href="/about/sustainability/operations/waste-recycling.php">Waste and Recycling</a></li>
<li><a href="/about/sustainability/operations/transportation.php">Transportation</a></li>
</ul>
</li>

<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Get Involved <i aria-hidden="true" class="far fa-angle-down"></i></a>
<ul>
<li><a href="/about/sustainability/get-involved/index.php">Students</a></li>
<li><a href="/about/sustainability/get-involved/employees.php">Valencia Employees</a></li>
<li><a href="/about/sustainability/get-involved/events.php">Annual Events</a></li>
</ul>
</li>
</ul>

