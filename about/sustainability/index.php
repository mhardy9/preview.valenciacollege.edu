<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sustainability  | Valencia College</title>
      <meta name="Description" content="Sustainability">
      <meta name="Keywords" content="college, school, educational, sustainability">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Sustainability</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>   
                        
                        
                        
                        <h3>What is sustainability?</h3>
                        
                        
                        <p><em>"Sustainable  development is meeting the needs of the present without compromising
                              the  ability of future generations to meet their own needs."</em> - Brundtland  Commission of the United Nations on March 20, 1987.<br>
                           
                        </p>
                        
                        <p>Sustainability at colleges and universities typically  entails: </p>
                        
                        <ul>
                           
                           <li>Identifying and strengthening strategies for  resource efficiency (energy, water,
                              waste, transportation, etc.)
                           </li>
                           
                           <li>Working with faculty across disciplines to  integrate sustainability across the curriculum</li>
                           
                           <li>Working with Procurement and college vendors to  identify and strengthen best practices,
                              decrease negative impacts and inputs  from services and projects, and use materials
                              and products that are not  wasteful or toxic
                           </li>
                           
                           <li>Raising awareness of sustainability/quality of  life issues among employees, students,
                              and the community
                           </li>
                           
                           <li>Engaging local municipalities and community  groups to work towards overlapping goals</li>
                           
                        </ul>
                        
                        <img src="/_resources/img/about/sustainability/triple-bottom-line.png" style="height: 25%; width: 25%;" alt="Triple Bottom Line image">
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>What Does Valencia Do for Sustainability?</h3>
                        
                        
                        
                        <p>Download our flyer on <em>Top  10 Things Valencia College Does for Sustainability and Top 10 Things You Can
                              Do </em><a href="documents/SustainabilityRackCard.pdf">here</a>.
                        </p>
                        
                        <ul>
                           
                           <li>What can you do to help Valencia embrace and  expand on Valencia’s sustainability
                              efforts?
                           </li>
                           
                           <li>Join your Sustainability Committee Campus  Chapter (link to Sustainability Committee
                              page). 
                           </li>
                           
                           <li>Integrate sustainability into your curriculum  (link to Sustainability Across the
                              Curriculum page). 
                           </li>
                           
                           <li>Become a sustainability intern.</li>
                           
                           <li>Make a conscious effort to use less resources  like energy, water, paper, etc. </li>
                           
                           <li>Be a voice for sustainability. Demand  sustainable products and less wasteful practices.</li>
                           
                           <li>Support sustainability. Buy products that are  recycled, recyclable, local, organic,
                              and cruelty free.
                           </li>
                           
                        </ul>
                        
                        <a href="documents/SustainabilityRackCard.pdf">Download</a> our flyer on Top 10 Things Valencia College Does for Sustainability and Top 10 Things
                        You Can Do or 
                        pick up a printed copy in the Student Development office
                        <p></p>
                        
                        <a href="documents/StudentTips12_3_13.pdf">Download</a> our flyer on Student Tips to Forming a Car Pool
                        <p></p>
                        
                        
                        <h3>Valencia's Sustainability Partner Organizations</h3>
                        
                        
                        <div id="climate" alt="Valencia Climate Video">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/kprJlLyL3PA" frameborder="0" allowfullscreen=""></iframe>
                           
                        </div>
                        
                        
                        <h3>In the News</h3>
                        
                        <p>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/" target="_blank" title="The Grove - Sustainability">
                              
                              LATEST SUSTAINABLE NEWS FROM
                              
                              <br>
                              <em>
                                 The Grove</em>            
                              </a>
                           
                        </p>
                        
                        
                        
                        <div>            
                           
                           <ul>
                              
                              
                              <li>     
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/dxO-IIUJFCE/" target="_blank">Sustainability Tip: Let’s Stay Sustainable This Halloween</a> <br>
                                 <span>Oct 09, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/7DMKulMpwTw/" target="_blank">Help Save the College Money and Shut Down for the Labor Day Holiday</a> <br>
                                 
                                 <span>Aug 29, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        <p>
                           
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All Articles</a>
                           
                           
                           
                        </p>
                        
                        
                        <p>
                           
                           <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank">
                              Subscribe</a>&nbsp; <img alt="RSS" border="0" src="icon_rss.png">
                           
                           
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/index.pcf">©</a>
      </div>
   </body>
</html>