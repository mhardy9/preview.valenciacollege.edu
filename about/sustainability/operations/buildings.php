<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Buildings  | Valencia College</title>
      <meta name="Description" content="Buildings | Operations | Sustainability">
      <meta name="Keywords" content="college, school, educational, sustainability, operations, buildings">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/operations/buildings.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/operations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		Operations
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/operations/">Operations</a></li>
               <li>Buildings </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <h3>Buildings</h3>
                        
                        
                        <p>According to the Environmental Protection  Agency (EPA), Americans, on average, spend
                           approximately 90 percent of their  time indoors where the concentrations of some pollutants
                           are often 2 to 5 times  higher than typical outdoor concentrations. Health effects
                           that have been  associated with indoor air pollutants include irritation of the eyes,
                           nose, and  throat; headaches, dizziness, and fatigue; respiratory diseases; heart
                           disease;  and cancer.
                        </p>
                        
                        
                        
                        
                        
                        <p>Since 2006, Valencia College has built all new buildings to <a href="http://www.usgbc.org/leed">LEED</a> Silver or <a href="http://www.greenglobes.com/home.asp">Green Globes</a> Level 2. Learn more  about our green buildings <a href="../locationses/documents/Valencia_College_Green_Buildings_features.pdf">here</a>. In  addition to constructing “green” buildings with better materials and more  daylighting,
                           Valencia also maintains a green cleaning program that utilizes <a href="http://www.greenseal.org/">Green Seal</a> products as a way to reduce  indoor toxins. 
                        </p>
                        
                        
                        <strong>Allied Health Sciences (West Campus)</strong><br>
                        <a href="../locationses/allied-health-center.html" target="_blank">
                           <img alt="Allied Health Sciences" border="1" height="250" src="/_resources/img/about/sustainability/AlliedHealthSciences.jpg" width="450">
                           </a><p></p>
                        
                        <strong>Special Events Center (West Campus)</strong><br>
                        <a href="../locationses/special-events-center.html" target="_blank">
                           <img alt="Special Events Center" border="0" height="250" src="/_resources/img/about/sustainability/specialEventsCenter.jpg" width="450">
                           </a><p></p>
                        
                        <strong>University Center (West Campus)</strong><br>
                        <a href="../locationses/university-center.html" target="_blank">      
                           <img alt="University Center" border="1" height="250" src="/_resources/img/about/sustainability/UniversityCenter.jpg" width="450">
                           </a><p></p>
                        
                        <strong>Building 10 (West Campus)</strong><br>
                        <a href="../locationses/building-10.html" target="_blank">      
                           <img alt="Building 10 West Campus" border="1" height="250" src="/_resources/img/about/sustainability/building-10-front-1024x681.jpg" width="450">
                           </a><p></p>
                        
                        <strong>Lake Nona Campus</strong><br>
                        <a href="../locationses/lakenona.html" target="_blank">      
                           <img alt="Lake Nona Campus" src="/_resources/img/about/sustainability/lakenona.jpeg">
                           </a><p></p>
                        
                        <strong>Osceola Campus</strong><br>
                        <a href="../locationses/osceola.html" target="_blank">      
                           <img alt="Osceola Campus" src="/_resources/img/about/sustainability/osceola.jpeg">
                           </a>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/operations/buildings.pcf">©</a>
      </div>
   </body>
</html>