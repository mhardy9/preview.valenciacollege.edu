<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Transportation  | Valencia College</title>
      <meta name="Description" content="Transportation | Operations | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, operations, transportation">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/operations/transportation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/operations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		Operations
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/operations/">Operations</a></li>
               <li>Transportation </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3>Transportation</h3>
                        
                        
                        <p>Transportation makes up almost half of the College’s <a href="../documents/VCC_GHGINVENTORY_2010.pdf">carbon  footprint</a>. It is challenging to participate in modes of alternative  transportation such as
                           the bus, train, carpooling, biking, and walking in  Florida since the state has not
                           been developed with pedestrians and bicyclists  in mind. This is slowly changing in
                           the Central Florida region, with more  emphasis on biking infrastructure and electric
                           vehicle charging stations. 
                        </p>
                        
                        <p><em><u>Electric Vehicle  Charging Stations</u></em><br>
                           
                        </p>
                        
                        <p>The College currently has charging stations on East and West  Campuses.</p>
                        
                        <ul>
                           
                           <li>
                              <a href="../../map/map-east.html">East Campus</a>, northeast  corner Parking Garage, 1st Floor
                           </li>
                           
                           <ul>
                              
                              <li>Two single units and 1 double unit (4 plugs) </li>
                              
                           </ul>
                           
                           <li>
                              <a href="../../map/map-west.html">West Campus</a>, northeast  side of AHS building, Parking Lot B
                           </li>
                           
                           <li>
                              <a href="../../map/map-west.html">West Campus</a>, northeast  corner of Parking Lot F
                           </li>
                           
                        </ul>
                        
                        <p>Installing EV charging stations at each campus is a goal and  is being investigated
                           currently. Orlando currently ranks 2nd in the state of  Florida for electric vehicle
                           sales according to the Florida Department of Motor  Vehicles and is a top 30 national
                           market for numerous models including the  Nissan LEAF, Chevy Volt and Tesla Model
                           S according to Pike Research.
                        </p>
                        
                        <p><em><u>ReThink Your  Commute</u></em><br>
                           
                        </p>
                        
                        <p><a href="https://www.rethinkyourcommute.com/">ReThink Your  Commute</a> is a free online tool funded by the Department of Transportation to  promote and
                           facilitate carpool matching in Central Florida. <br>
                           
                        </p>
                        
                        <p>A summer 2013 survey of over 3000 students from East and  West Campuses indicates
                           that 75% of students drive their own cars to campus.  Data indicates that approximately
                           30% of Valencia College staff live 15 miles  from campus or more.<br>
                           
                        </p>
                        
                        <p>Carpooling can bring tremendous savings in cost of gas,  tolls, and car maintenance
                           to each student or Valencia employee. Set up an  account to be matched up with other
                           riders with the same or similar commute  schedules as you by <a href="https://www.rethinkyourcommute.com/">click on this  website</a>, click Find Me a Match and sign up.<br>
                           There is even an <a href="https://www.rethinkyourcommute.com/emergency-ride-home-info/">Emergency  Ride Home program</a> with ReThink for occasions in which you have to get home  before the carpool.<br>
                           
                        </p>
                        
                        <p>Consider <a href="../action/documents/StudentTipstoStartaSuccessfulCarpool2.pdf">these  Tips</a> for a Successful Carpool<br>
                           
                        </p>
                        
                        <p><em><u>Lynx </u></em><br>
                           
                        </p>
                        
                        <p>Try riding the bus (Lynx). There is an <a href="http://www.golynx.com/">easy online tool</a> to help you plan your trip  from point A to point B. <br>
                           
                        </p>
                        
                        <p><em><u>Sun Rail </u></em><br>
                           
                        </p>
                        
                        <p>Try riding the train (<a href="http://sunrail.com/">Sunrail</a>).  Plan your trip using the map and schedules provided. 
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://www.sunrail.com/map/">Train  Stops Map</a></li>
                           
                           <li><a href="http://sunrail.com/schedules/northbound/">Northbound Schedule</a></li>
                           
                           <li><a href="http://sunrail.com/schedules/southbound/">Southbound Schedule</a></li>
                           
                        </ul>
                        
                        <p><em><u>Biking</u></em><br>
                           Biking is a healthy and effective way to travel. Central  Florida is currently evolving
                           to provide safer and more functional  infrastructure for bikers. Here is an overview
                           of how biking addresses many college student related issues.
                        </p>
                        
                        <p>            Juice Bike Share<br>
                           
                        </p>
                        
                        <p>Winter Park Campus has a <a href="https://orlando.socialbicycles.com/">Juice Bike Share</a> rack with five  bicycles to serve students. They also have a bike club and <a href="https://register.cyclingsavvy.org/home">Cycle Savvy safety classes</a> through ReThink Your Commute. Contact <a href="mailto:vwoldman@valenciacollege.edu">Valerie Woolman</a> for more Winter  Park Campus specific questions about biking.<br>
                           We hope to integrate biking infrastructure and opportunities  for biking at more campuses
                           in the future. 
                        </p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/operations/transportation.pcf">©</a>
      </div>
   </body>
</html>