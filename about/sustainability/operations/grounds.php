<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Grounds  | Valencia College</title>
      <meta name="Description" content="Grounds | Operations | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, operations, grounds">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/operations/grounds.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/operations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		Operations
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/operations/">Operations</a></li>
               <li>Grounds </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Grounds</h2>
                        
                        
                        <p>All of the Valencia College campuses have elements of the  natural Central Florida
                           environment. West Campus employees have catalogued  several <a href="../locationses/documents/WestCampus_ANIMALCHECKLIST20080519.pdf">animal</a> and <a href="../locationses/documents/WestCampusWALKINGFIELDGUIDEchecklist2.pdf">plant</a> populations on their campus. We seek to maintain and enhance these to use each <a href="http://www.theseedcenter.org/getattachment/Resources/SEED-Resources/SEED-Toolkits/Campus-as-a-Living-Lab/Campus-as-a-Living-Lab.pdf">Campus  as a Living Laboratory</a>. Special attention is given to using Florida native  plants, creating wildlife habitat
                           and living laboratory opportunities, integrated  pest management (IPM), protecting
                           water quality and campus water fronts, and  organic fertilizers.
                        </p>
                        
                        
                        <ul>
                           
                           <li>Tree Campus USA</li>
                           
                        </ul>
                        
                        
                        <p>Valencia College has been awarded the <a href="https://www.arborday.org/programs/treecampususa/">Tree Campus USA</a> designation by the <a href="https://www.arborday.org/">Arbor Day Foundation</a>.  The college achieved this designation by meeting the required <a href="https://www.arborday.org/programs/treecampususa/standards.cfm">five core  standards</a> for sustainable campus forestry.
                        </p>
                        
                        <p>
                           Read Valencia’s Tree Care Plan here. Contact Dave Gennaro, college-wide  Director
                           of Grounds at <a href="mailto:dgennaro@valenciacollege.edu">dgennaro@valenciacollege.edu</a> or (407) 582-3453 to be added to the Tree Campus USA Advisory Committee email  list
                           for meeting invitations and agendas or to learn more.  Quarterly meetings are held
                           the third week in January, April, July, and October to discuss strategies and areas
                           to advance the quality and benefits of campus trees. The committee is open to Valencia
                           employees and students, as well as community tree professionals.
                        </p>
                        
                        
                        <ul>
                           
                           <li>Campus as a Living Laboratory</li>
                           
                        </ul>
                        
                        <h3>Outdoor Classrooms </h3>
                        
                        Outdoor Classrooms at <a href="http://thegrove.valenciacollege.edu/west-offers-new-outdoor-space-for-classes/">West</a> and <a href="http://thegrove.valenciacollege.edu/osceola-building-4-courtyard-classrooms/">Osceola</a> Campuses can be reserved in the same way that regular indoor classroom  reservation.
                        Outdoor classrooms provide an alternate space for instructors to  get their students
                        closer to the subject they are learning about, to stimulate  student creativity and
                        productivity by being in the outdoors with sunlight and  fresh air, or to change up
                        a routine classroom schedule or meeting.
                        
                        
                        <h3>Educational Signage</h3>
                        
                        <p>Adding educational signage to outdoor features provides  instructors and students
                           with learning opportunities outside of the classroom.  In the future, these preliminary
                           efforts may be expanded with a long term plan  for outdoor educational signage as
                           part of a Living Laboratory plan. 
                        </p>
                        
                        
                        
                        <p>Caption: West Campus <a href="http://thegrove.valenciacollege.edu/local-boy-scout-heads-lake-pamela-kiosk-project-to-earn-eagle-scout-rank/">educational  double sided kiosks built by a local Eagle Scouts troop</a> positioned along  the Lake Pamela Trail.
                        </p>
                        
                        
                        <h3>Nature  Walks</h3>
                        
                        <p>Nature walks have been provided by different faculty, staff,  and students over the
                           years at the East, West, and Osceola Campuses. To inquire  about scheduling a nature
                           walk at your campus, contact Resham Shirsat, Director  of Sustainability at <a href="mailto:rshirsat@valenciacollege.edu">rshirsat@valenciacollege.edu</a> or 407-582-1830. 
                        </p>
                        
                        
                        
                        <h3>Pollinator Gardens and Habitat</h3>
                        
                        <p>Pollinators such as bees, butterflies, bats, and birds are  in global decline. All
                           three major campuses (East, West, and Osceola) have  small <a href="http://thegrove.valenciacollege.edu/sustainability-office-creates-new-habitats-for-butterflies/">pollinator  gardens</a> to educate students about the role of birds, bats, bees, and  butterflies in our
                           food system and ecosystem. West and East Campus also have <a href="http://thegrove.valenciacollege.edu/bats-have-found-their-home-on-campus/">bat  boxes</a> that are both inhabited. Building <a href="http://thegrove.valenciacollege.edu/duck-nesting-a-key-impact-of-learning-day-volunteerism-on-the-west-campus-environment/">bird  boxes</a> has been a popular hands on activity at West Campus Learning Day over  the last few
                           years with participants’ boxes being used to support campus  populations or taken
                           home to support <a href="http://www.nwf.org/Garden-For-Wildlife/Create.aspx?campaignid=WH14F1ASCXX&amp;s_src=700000000082645&amp;s_subsrc=NWF_Habitats%7cNWF_Backyard_Habitat&amp;ssource=700000000082645&amp;kw=NWF_Habitats%7cNWF_Backyard_Habitat%3Cvaries%3E&amp;gclid=CjwKEAjw1riwBRD61db6xtWTvTESJACoQ04QKPkmCXQA78sYtDU5tKlrtgY3aZdyA2mYLSomvWP38BoCvwXw_wcB?s_src=700000000082645&amp;s_subsrc=NWF_Habitats%7cNWF_Backyard_Habitat&amp;ssource=700000000082645&amp;kw=NWF_Habitats%7cNWF_Backyard_Habitat&amp;gclid=CjwKEAjw1Iq6BRDY_tK-9OjdmBESJABlzoY7Yfs0vUQm3pFLBpuPbNe-ZEGDyPcJfHtks6yfzc2GKBoCK9vw_wcB&amp;gclid=CjwKEAjw1riwBRD61db6xtWTvTESJACoQ04QKPkmCXQA78sYtDU5tKlrtgY3aZdyA2mYLSomvWP38BoCvwXw_wcB?s_src=%5b*EngineAccountID*%5d">backyard  wildlife habitats</a>. Osceola Campus has <a href="http://thegrove.valenciacollege.edu/ospreys-nest-on-a-new-platform-at-osceola-campus/">osprey  towers</a> which we hope to replicate at other campuses.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/operations/grounds.pcf">©</a>
      </div>
   </body>
</html>