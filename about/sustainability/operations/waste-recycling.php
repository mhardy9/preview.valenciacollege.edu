<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Waste and Recycling  | Valencia College</title>
      <meta name="Description" content="Waste and Recycling | Operations | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, operations, waste, recycling">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/operations/waste-recycling.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/operations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability</h1>
            <p>
               		Operations
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li><a href="/about/sustainability/operations/">Operations</a></li>
               <li>Waste and Recycling </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        <h3>Waste and Recycling</h3>
                        
                        
                        
                        <p>Valencia’s recycling program started in February 2007 with  the voluntary collection
                           of paper at our East and West Campuses. The number of  campuses participating and
                           the streams of recyclables grew to include more  materials. The College has always
                           practiced multi-stream recycling, meaning  that recyclables are separated at the source
                           through different bins for  different materials such as paper/cardboard (aka “fiber”),
                           plastic, and  aluminum. 
                        </p>
                        
                        <p>              In 2016, the College transitioned to single stream recycling  through
                           a new vendor for several reasons:
                        </p>
                        
                        <ul>
                           
                           <li>Make recycling easier for employees and students</li>
                           
                           <li>Align with what most of Central Florida already  practices</li>
                           
                           <li>Decrease custodial labor time and plastic liners  needed to support three (aluminum,
                              plastic, paper) recycle collection bins vs.  one
                           </li>
                           
                           <li>Single stream collection bins require less floor  space</li>
                           
                        </ul>
                        
                        <p>Beginning in September 2016, you can comingle paper,  cardboard, aluminum, and plastic
                           all into one recycle bin, like many are  already used to at home. Please keep trash
                           out of recycling bins to avoid  contamination penalty fees and to prevent collected
                           recyclables from  contaminated, which results in recycling going to landfill instead
                           of being  turned into new products.
                        </p>
                        
                        <p>              Plant Operations/Custodial and Sustainability manage this  program.
                           
                        </p>
                        
                        <p><em><u>Printer Cartridges  and Toners</u></em><br>
                           
                        </p>
                        
                        <p>As of January 2016, a single vendor (<a href="http://www.pcramerica.com/">PCR America</a>) has been identified to serve  all campus locations to provide free recycling of
                           unwanted or used printer  cartridges and toners within the United States, free shipping,
                           a small  financial return per unit, and sustainability metrics reporting.
                        </p>
                        
                        <p>              An implementation toolkit can be downloaded here. Employees  can simply
                           place their unmarked and unpackaged used ink cartridge or toner from  work or home
                           directly into the outgoing mailbox. We have partnered with Courier  Services to collect
                           and store these items at College shipping and receiving  warehouses until there are
                           enough to be shipped out on pallets. Students may  bring in their used items and place
                           them in the collection bin which can be  found in any campus library. 
                        </p>
                        
                        <p>              Sustainability, Courier Services, and Procurement manage  this program.
                           
                        </p>
                        
                        <p><em><u>Pallets</u></em><br>
                           
                        </p>
                        
                        <p>In 2015, a single vendor (EcoDepot) was identified to serve  all campus locations
                           to provide free reuse and recycling of used or broken  pallets. A modest return of
                           0.05 cents per pallet is returned to the College. 
                        </p>
                        
                        <p>Plant Operations manages this program. </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/operations/waste-recycling.pcf">©</a>
      </div>
   </body>
</html>