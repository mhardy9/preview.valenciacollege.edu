<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Our Team  | Valencia College</title>
      <meta name="Description" content="Our Team | Sustainability | Valencia College">
      <meta name="Keywords" content="college, school, educational, sustainability, team, us">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/our-team.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li>Our Team </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Our Team</h3>
                           
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Resham Shirsat, Director of Sustainability</li>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="mailto:rshirsat@valenciacollege.edu">rshirsat@valenciacollege.edu</a>&nbsp;or (407) 582-1830
                                    
                                 </li>
                                 
                              </ul>
                              
                              <li>Robert Hickman, Energy Efficiency Operations Manager</li>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="mailto:rhickman@valenciacollege.edu">rhickman@valenciacollege.edu</a>&nbsp;or (407) 582-1768
                                    
                                 </li>
                                 
                              </ul>
                              
                              <li>Estevan Baza, Energy Conservation Operations Manager
                                 
                                 <ul>
                                    
                                    <li>
                                       <a href="mailto:ebaza@valenciacollege.edu" rel="noreferrer">ebaza@valenciacollege.edu</a>Â&nbsp;or
                                       (407) 582-5495
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        <style>
            #feed ul {
              list-style: none;
              margin: 0;
              padding: 0;
            }

            #feed ul li {
              padding: 5px;
            }

            #feed ul li a, #feed p {
              font-size: 13px;
            }

            #feed a {
              text-decoration: none;
            }

            #feed a:hover {
              text-decoration: none;
              color: #bf311a;
            }
          </style>
                        
                        <div id="fb-root"></div>
                        
                        
                        
                        <div class="fb-like-box" data-href="https://www.facebook.com/SustainableValencia/" data-width="215" data-height="80" data-show-faces="false" data-stream="false" data-header="false"></div>
                        
                        
                        <p class="rightColumnBreaks"></p>
                        
                        
                        
                        <cfparam name="EffFeed" default="0">
                           
                           
                           <p class="center">
                              <a href="http://thegrove.valenciacollege.edu/category/sustainability/" title="The Grove - Sustainability" target="_blank">
                                 
                                 <cfif efffeed="" eq=""> ENERGY SAVING TIPS FROM
                                    
                                    <cfelse>
                                       LATEST SUSTAINABLE NEWS FROM
                                    </cfelse>
                                 </cfif><br> <em class="center">
                                    The Grove</em>
                                 </a>
                              
                           </p>
                           
                           <cfset listtype="ul">  
                              
                              <cfset loopend="2">  
                                 
                                 <cfset desclen="0">  
                                    
                                    <cfset showauthor="0">  
                                       
                                       <cfset showpublish="1">  
                                          
                                          <cfset showcontent="0">  
                                             
                                             <style>#feed .smoreIcon {
                          display: none;
                        }</style>
                                             
                                             
                                             
                                             
                                             <cfif efffeed="" eq="">
                                                
                                                <cfset urltopull="http://feeds.feedburner.com/energy-tips">
                                                   
                                                   <cfelse>
                                                      
                                                      <cfset urltopull="http://feeds.feedburner.com/valenciasustainability">
                                                         
                                                      </cfset>
                                                   </cfelse>
                                                </cfset>
                                             </cfif>
                                             
                                             
                                             <div id="feed">
                                                
                                                
                                                <cfinclude template="/includes/incl_RSS_format_new.cfm">
                                                   
                                                </cfinclude>
                                                
                                             </div>
                                             
                                             
                                             <a href="http://thegrove.valenciacollege.edu/category/energy-tips/">View All Energy Tips
                                                Articles</a>
                                             
                                             <p></p>
                                             
                                             <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All
                                                Sustainability Articles</a>
                                             
                                             
                                             
                                             
                                             <a href="http://feeds.feedburner.com/energy-tips" target="_blank">Subscribe to Energy Tips</a>&nbsp; <img src="/_resources/img/icon_rss.png" alt="RSS" border="0">
                                             
                                             <p></p>
                                             <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank"> Subscribe to Sustainability</a>&nbsp; <img src="/_resources/img/icon_rss.png" alt="RSS" border="0">
                                             
                                             
                                             
                                             
                                             
                                             <p>&nbsp;</p>
                                             
                                          </cfset>
                                       </cfset>
                                    </cfset>
                                 </cfset>
                              </cfset>
                           </cfset>
                        </cfparam>
                        
                     </div>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/our-team.pcf">©</a>
      </div>
   </body>
</html>