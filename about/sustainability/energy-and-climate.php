<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Energy and Climate  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/energy-and-climate.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li>Energy and Climate </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Energy and Climate</h3>
                           
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>President Shugart's Commitment<br>
                              
                           </p>
                           
                           <p>The American College &amp; University Presidents' Climate Commitment (ACUPCC) is a high-visibility
                              effort
                              to address global climate disruption undertaken by a network of colleges and universities
                              that have made
                              institutional commitments to eliminate net greenhouse gas emissions from specified
                              campus operations, and
                              to promote the research and educational efforts of higher education to equip society
                              to&nbsp;<img width="300" height="220" src="/_resources/img/about/sustainability/shugart.jpg" align="right" alt="President Shugart">re-stabilize
                              the earth's climate. Its mission is to accelerate progress towards climate neutrality
                              and sustainability
                              by empowering the higher education sector to educate students, create solutions, and
                              provide
                              leadership-by-example for the rest of society.<br>
                              
                           </p>
                           
                           <p>The Commitment recognizes the unique responsibility that institutions of higher education
                              have as role
                              models for their communities and in educating the people who will develop the social,
                              economic and
                              technological solutions to reverse global warming and help create a thriving, civil
                              and sustainable
                              society.<br>
                              
                           </p>
                           
                           <p>Dr. Shugart signed the commitment on June 16, 2009. As of February 2014, 679 colleges
                              and university
                              presidents have signed the commitment. Read his commitment&nbsp;<a href="http://preview.valenciacollege.edu/sustainability/documents/Presidents_Climate_Commitment.pdf">here</a>.
                              
                           </p>
                           
                           <p>ACUPCC institutions have agreed to:</p>
                           
                           <ul>
                              
                              <li>Complete an emissions inventory.</li>
                              
                              <li>Within two years, set a target date and interim milestones for becoming climate neutral.</li>
                              
                              <li>Take immediate steps to reduce greenhouse gas emissions by choosing from a list of
                                 short-term actions.
                                 
                              </li>
                              
                              <li>Integrate sustainability into the curriculum and make it part of the educational experience.</li>
                              
                              <li>Make the action plan, inventory and progress reports publicly available.</li>
                              
                           </ul>
                           
                           <p>Programs such as recycling, converting to organic fertilizers, using renewable energy,
                              or replacement of
                              older less efficient machinery can help reduce our footprint. The College’s major
                              GHG source
                              include:
                           </p>
                           
                           <ul>
                              
                              <li>Electricity Consumption</li>
                              
                              <li>Natural Gas Consumption</li>
                              
                              <li>Vehicle Fleet Fuel Consumption</li>
                              
                              <li>Commuter Fuel Consumption</li>
                              
                              <li>Fugitive Emissions from refrigerants</li>
                              
                              <li>Solid Waste</li>
                              
                              <li>Emissions from applications of fertilizer</li>
                              
                           </ul>
                           
                           <p>Read Valencia’s 2006-2008 Greenhouse Gas (GHG) Inventory&nbsp;<a href="http://preview.valenciacollege.edu/sustainability/action/documents/VALENCIA-CAP.pdf">here</a>.&nbsp;<br>
                              
                           </p>
                           
                           <p>Follow Valencia’s GHG reporting&nbsp;<a href="http://reporting.secondnature.org/search/?institution_name=&amp;commitment_type=%3F%3F&amp;carnegie_class=%3F%3F&amp;state_or_province=FL">here</a>.
                              
                           </p>
                           
                           <ul>
                              
                              <ul>
                                 
                                 <li>Climate Action Plan and Update</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <p>Based on the 2010 Greenhouse Gas Inventory, the College developed a long term Climate
                              Action Plan in
                              2010. This plan outlines a set of strategies across four categories (transportation,
                              energy, solid waste,
                              and education &amp; outreach) which, when implemented, will guide the college to climate
                              neutrality by
                              2060.
                           </p>
                           
                           <p><br>
                              
                           </p>
                           
                           <p>Read Valencia’s 2010 Climate Action Plan&nbsp;<a href="http://preview.valenciacollege.edu/sustainability/action/documents/2010-signed-CAP.pdf">here</a>.&nbsp;<br>
                              
                           </p>
                           
                           <p>Read Valencia’s 2014 Climate Action Plan Update&nbsp;<a href="http://preview.valenciacollege.edu/sustainability/action/documents/2014-CAP.pdf">here</a>.
                           </p>
                           
                           <ul>
                              
                              <ul>
                                 
                                 <li>Energy Efficiency and Conservation</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <p>AASHE had published a&nbsp;<a href="https://hub.aashe.org/browse/types/casestudy/">case study</a>&nbsp;on Valencia
                              College's Energy Efficient efforts.
                           </p>
                           
                           <ul>
                              
                              <li>Energy Conservation and Behavioral Change</li>
                              
                           </ul>
                           
                           <p>In 2009, Valencia College embarked on an ambitious program to achieve savings through
                              strategic shut down
                              procedures, with the Operations Manager, Energy Conservation focused on this task.
                              Systems shut down
                              during unoccupied periods include everything but science labs, security lighting,
                              and the mdf (main
                              distribution facility) and server rooms. The College now saves approximately $1.3
                              million dollars per year
                              from this behavioral change program. Savings, compared against a baseline of one year
                              prior to start of
                              the program, continue to grow.&nbsp;<br>
                              
                           </p>
                           
                           <p>Unoccupied and occupied energy audits, oversight of comfort complaints, adoption of&nbsp;<a href="https://preview.valenciacollege.edu/sustainability/documents/EnergyEducationGuidelines.pdf" target="_blank">college-wide guidelines for energy efficiency</a>, publicity in&nbsp;<a href="http://thegrove.valenciacollege.edu/">The Grove</a>&nbsp;on savings achieved and recognition of "<a href="http://preview.valenciacollege.edu/sustainability/getinvolved/hero.cfm">energy heroes</a>," and
                              communication with faculty and staff by a variety of means are aspects of this effort.
                              Review of class
                              schedules and relocation of classes where appropriate allows HVAC energy adjustments
                              and important
                              strategies. This successful program has involved a five-year contract with Cenergistic,
                              an outside firm
                              specializing in behavioral energy savings.<br>
                              
                           </p>
                           
                           <p>See energy savings tips&nbsp;<a href="http://thegrove.valenciacollege.edu/?s=energy+saving+tips">here</a>.<br>
                              
                           </p>
                           
                           <p>For information about your on-campus energy use, shutdowns, to make suggestions, or
                              request a
                              presentation, contact&nbsp;<a href="mailto:priva@valenciacollege.edu">Patti Riva</a>, Operations Manager,
                              Energy Conservation at&nbsp;<a href="mailto:priva@valenciacollege.edu">priva@valenciacollege.edu</a>&nbsp;or 407-
                              582-5495
                           </p>
                           
                           <p><em><u>Become An Energy Hero</u></em><br>
                              
                           </p>
                           
                           <p>The&nbsp;<a href="http://thegrove.valenciacollege.edu/employees-recognized-for-energy-saving-efforts/">Energy
                                 Hero Award</a>&nbsp;will be designated periodically as a way to recognize those who submit energy-saving
                              ideas
                              and programs. Nominations for the Energy Hero Award, including self-nominations, and
                              energy saving ideas
                              should be submitted to&nbsp;<a href="mailto:priva@valenciacollege.edu">Patti Riva</a>, Operations Manager of
                              Energy Conservation.<br> <img border="0" width="570" height="470" src="/_resources/img/about/sustainability/energy_clip_image002.jpg" alt="Energy Heroes"></p>
                           
                           <ul>
                              
                              <li>Equipment</li>
                              
                           </ul>
                           
                           <p><em><u>Building Automation</u></em><br>
                              
                           </p>
                           
                           <p>All campuses of Valencia College are using building automation systems (BAS). BAS
                              is used automate
                              temperatures and lighting.&nbsp;<br>
                              
                           </p>
                           
                           <p>West Campus just received a $900,000 3-year grant from the National Science Foundation
                              to develop
                              curriculum, hire and train faculty, and purchase lab equipment for a new A.S. degree
                              in Energy Management
                              Controls Technology (EMCT).
                           </p>
                           
                           <p><strong>Watch this video:</strong>&nbsp;<a href="https://www.facebook.com/SustainableValencia/">www.facebook.com/SustainableValencia/</a>&nbsp;to
                              learn more. Contact Dr. Deb Hall at&nbsp;<a href="mailto:dhall@valenciacollege.edu">dhall@valenciacollege.edu</a>&nbsp;or 407-582-1963 for more
                              information about the Valencia’s first sustainability degree.&nbsp;<br>
                              
                           </p>
                           
                           <p>For technical information about the college's energy efficiency programs, contact
                              Robert Hickman,
                              Operations Manager, Energy Efficiency, at&nbsp;<a href="mailto:rhickman@valenciacollege.edu">rhickman@valenciacollege.edu</a>&nbsp;or
                              407-582-1768.<br> <img border="0" width="570" height="321" src="/_resources/img/about/sustainability/energy_clip_image003.jpg"><br>
                              
                           </p>
                           
                           <p><em><u>Renewable Energy&nbsp;</u></em><br>
                              
                           </p>
                           
                           <p>The different campuses purchase electricity from four power companies, each with different
                              rate
                              structures, providing challenges and opportunities. Without the rebates that utilities
                              provide in states
                              that have Renewable Energy Portfolio Standards, utilizing renewable energy has not
                              been pursued. However,
                              we do have solar panels on one of our LEED Gold buildings, the UCF-Valencia Joint
                              Use Building on West
                              Campus. This building houses electrical engineering and the teaching of renewable
                              energy technologies.<br>
                              
                           </p>
                           
                           <p>A 102.48 kW array of 336 solar panels each producing 305 watts provides approximately
                              16 percent of the
                              building's electricity (July 2011-June 2012 data).
                           </p>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        <style>
            #feed ul {
              list-style: none;
              margin: 0;
              padding: 0;
            }

            #feed ul li {
              padding: 5px;
            }

            #feed ul li a, #feed p {
              font-size: 13px;
            }

            #feed a {
              text-decoration: none;
            }

            #feed a:hover {
              text-decoration: none;
              color: #bf311a;
            }
          </style>
                        
                        <div id="fb-root"></div>
                        
                        
                        
                        <div class="fb-like-box" data-href="https://www.facebook.com/SustainableValencia/" data-width="215" data-height="80" data-show-faces="false" data-stream="false" data-header="false"></div>
                        
                        
                        <p class="rightColumnBreaks"></p>
                        
                        
                        
                        <cfparam name="EffFeed" default="0">
                           
                           
                           <p class="center">
                              <a href="http://thegrove.valenciacollege.edu/category/sustainability/" title="The Grove - Sustainability" target="_blank">
                                 
                                 <cfif efffeed="" eq=""> ENERGY SAVING TIPS FROM
                                    
                                    <cfelse>
                                       LATEST SUSTAINABLE NEWS FROM
                                    </cfelse>
                                 </cfif><br> <em class="center">
                                    The Grove</em>
                                 </a>
                              
                           </p>
                           
                           <cfset listtype="ul">  
                              
                              <cfset loopend="2">  
                                 
                                 <cfset desclen="0">  
                                    
                                    <cfset showauthor="0">  
                                       
                                       <cfset showpublish="1">  
                                          
                                          <cfset showcontent="0">  
                                             
                                             <style>#feed .smoreIcon {
                          display: none;
                        }</style>
                                             
                                             
                                             
                                             
                                             <cfif efffeed="" eq="">
                                                
                                                <cfset urltopull="http://feeds.feedburner.com/energy-tips">
                                                   
                                                   <cfelse>
                                                      
                                                      <cfset urltopull="http://feeds.feedburner.com/valenciasustainability">
                                                         
                                                      </cfset>
                                                   </cfelse>
                                                </cfset>
                                             </cfif>
                                             
                                             
                                             <div id="feed">
                                                
                                                
                                                <cfinclude template="/includes/incl_RSS_format_new.cfm">
                                                   
                                                </cfinclude>
                                                
                                             </div>
                                             
                                             
                                             <a href="http://thegrove.valenciacollege.edu/category/energy-tips/">View All Energy Tips
                                                Articles</a>
                                             
                                             <p></p>
                                             
                                             <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All
                                                Sustainability Articles</a>
                                             
                                             
                                             
                                             
                                             <a href="http://feeds.feedburner.com/energy-tips" target="_blank">Subscribe to Energy Tips</a>&nbsp; <img src="/_resources/img/icon_rss.png" alt="RSS" border="0">
                                             
                                             <p></p>
                                             <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank"> Subscribe to Sustainability</a>&nbsp; <img src="/_resources/img/icon_rss.png" alt="RSS" border="0">
                                             
                                             
                                             
                                             
                                             
                                             <p>&nbsp;</p>
                                             
                                          </cfset>
                                       </cfset>
                                    </cfset>
                                 </cfset>
                              </cfset>
                           </cfset>
                        </cfparam>
                        
                     </div>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/energy-and-climate.pcf">©</a>
      </div>
   </body>
</html>