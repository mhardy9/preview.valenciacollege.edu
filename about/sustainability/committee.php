<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sustainability Committee | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/sustainability/committee.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/sustainability/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sustainability Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/sustainability/">Sustainability</a></li>
               <li>Sustainability Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h3>Mission Statement</h3>
                        
                        
                        <p><br>
                           <em>“The committee’s  mission is to establish a college-wide community that functions
                              sustainably now  and in future generations, uses economic and environmental resources
                              efficiently, and educates students, faculty, and staff in a way that fosters  social
                              justice in partnership with stakeholders in the wider community.”</em><br>
                           
                        </p>
                        
                        <p>The Sustainability Committee has three campus chapters (1.  West Campus/District Office
                           2. East Campus/Winter Park Campus/School of Public  Safety 3. Osceola Campus, Lake
                           Nona Campus, Advanced Manufacturing). Quarterly meetings  are held the third week
                           in January, April, July, and October to discuss  strategies and areas to advance campus
                           sustainability. The committee is open to  Valencia employees and students. <br>
                           
                        </p>
                        
                        <p>Contact Resham Shirsat, Director of Sustainability at <a href="mailto:rshirsat@valenciacollege.edu">rshirsat@valenciacollege.edu</a> or  407-582-1830 to be added to the email list for meeting invitations and agendas
                           or  to learn more. 
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/" target="_blank" title="The Grove - Sustainability">
                              
                              LATEST SUSTAINABLE NEWS FROM
                              
                              <br>
                              <em>
                                 The Grove</em>            
                              </a>
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/dxO-IIUJFCE/" target="_blank">Sustainability Tip: Let’s Stay Sustainable This Halloween</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Oct 09, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://feedproxy.google.com/~r/valenciasustainability/~3/7DMKulMpwTw/" target="_blank">Help Save the College Money and Shut Down for the Labor Day Holiday</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Aug 29, 2017</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        <p>
                           
                           <a href="http://thegrove.valenciacollege.edu/category/sustainability/">View All Articles</a>
                           
                           
                           
                        </p>
                        
                        
                        <p>
                           
                           <a href="http://feeds.feedburner.com/valenciasustainability" target="_blank">
                              Subscribe</a>&nbsp; <img alt="RSS" border="0" src="icon_rss.png">
                           
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/sustainability/committee.pcf">©</a>
      </div>
   </body>
</html>