<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Division Action Plans | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/division-plans/0708.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/division-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Division Action Plans</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/division-plans/">Division Plans</a></li>
               <li>Division Action Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>0708 Planning and Evaluation Links</h2>
                        
                        <p>To comply with SACS <i><strong>Principles of Accreditation: Foundations 
                                 for Quality Enhancement </strong></i> Comprehensive 
                           Standard 3.3.1.1 (below), each academic division will design and implement 
                           an annual plan to assess student learning outcomes. 
                        </p>
                        
                        <p>3.3.1 The institution identifies expected outcomes, assesses the extent to which it
                           achieves these outcomes, and provides evidence of improvement based on analysis of
                           the results in each of the following areas <strong>(Institutional Effectiveness)</strong>: 
                        </p>
                        
                        <blockquote>
                           
                           <blockquote>
                              
                              <blockquote>
                                 <strong>3.3.1.1 educational programs, to include student learning outcomes </strong><br>
                                 <br>
                                 3.3.1.2 administrative support services <br>
                                 <br>
                                 3.3.1.3 educational support services <br>
                                 <br>
                                 3.3.1.4 research within its educational mission, if appropriate <br>
                                 <br>
                                 3.3.1.5 community/public service within its educational mission, if appropriate 
                              </blockquote>
                              
                           </blockquote>
                           
                        </blockquote>
                        
                        <p>The <strong>Index of Plans</strong> provided below includes an informational 
                           overview including a <strong>Division Action Plan (DAP)</strong> number, academic 
                           division of origin, campus, dean and a brief topical focus for each 
                           plan. By clicking on a <strong>Division Action Plan (DAP)</strong> number 
                           you will be linked to current planning and evaluation documents.
                        </p>
                        
                        <p>
                           <strong>   <br>
                              STUDENT LEARNING OUTCOMES ASSESSMENT<br>
                              DIVISION ACTION PLANS (DAPs) FOR 2007-2008</strong></p>
                        
                        <p>
                           <strong>   Index of Plans  </strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>  <strong>DAP No.</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Academic Division</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Campus</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Dean</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Topical Focus</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-001.pdf">0708-001</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Architecture, 
                                       Engineering <br>
                                       &amp; Technology
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> N. Hedayat</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Design Studio Exploration</strong>. 
                                       
                                       
                                       </strong>To identify any performance gap occurring between students who participate in the
                                    dedicated studio space (9-213) and those who do not.&nbsp; &nbsp;The plan seeks to determine
                                    how prolonged exposure to working on Architectural Design 3 &amp; 4 assignments in a dedicated
                                    Architecture studio affects student’s success in terms of improving craft and spatial
                                    comprehension as well as overall productivity.                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-002.pdf">0708-002</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>
                                       Business, Behavioral <br>
                                       &amp; Social Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. Franceschi </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Technological Adaptation to Learning and Assessment. </strong>To identify, promote, and enhance learning by adapting technology to improve instruction
                                    and diversify assessment. Our division’s purpose statement underscores that course
                                    work should assist with the adaptation of changing technologies to both academic and
                                    work force environments (presentation materials, instructional techniques, course
                                    activities, specialized seminars, etc.). This learning assessment project will reinforce
                                    such an objective and provide an opportunity for growth to all members of the division.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-003.pdf">0708-003</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Business, IT <br>
                                       &amp; Public 
                                       Services
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J.L. Look</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Workforce Survey/Curriculum Assessment</strong>. 
                                       
                                       
                                       </strong>To determine workforce skills needed in the Central Florida business community in
                                    order to ensure that the curriculum of the AS degree programs are both relevant to
                                    learning outcomes and consistent with contemporary&nbsp;needs.&nbsp;                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-004.pdf">0708-004</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Business, IT <br>
                                       &amp; Social 
                                       Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D.Husbands</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Assessing Student Essays in Targeted Social Sciences Related Gordon Rule Classes.
                                          
                                          
                                          
                                          </strong></strong>To introduce and develop an assessment process for “Communicate” in all the Social
                                    Sciences related Gordon Rule courses that can be used to both strengthen the writing
                                    outcome for general education selections and to create a college wide rubric for writing
                                    assignments and assessments. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-005.pdf">0708-005</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D. Paul</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Improving Writing Across the Disciplines</strong>. 
                                       
                                       
                                       </strong>To design and establish a writing lab that serves the support and development needs
                                    of various students and disciplines.                 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-006.pdf">0708-006</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications    
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> K. Long</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Reading One Independent Study Experience (RISE)</strong>. 
                                       
                                       
                                       </strong>To offer high level REA 0001 students the opportunity to accelerate their Prep Reading
                                    sequence. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-007.pdf">0708-007</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications, 
                                       Humanities <br>
                                       &amp; Foreign Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> K. Mulholland</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong> <strong>Assessing Writing in Gordon Rule Humanities Classes</strong>. 
                                          
                                          
                                          </strong>To introduce and develop an assessment process for “Communicate” in all the Gordon
                                       Rule courses that can be used to both strengthen the writing outcome for humanities
                                       general education courses and to create a college wide rubric for writing assignments
                                       and assessments. <br>
                                       <u>Note</u>:&nbsp; A cooperative project between East, Oseola and West Campuses.                
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-008.pdf">0708-008</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Credit Programs
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Winter Park
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. McArdle</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Cooperative Learning Community Expansion. </strong></strong>
                                    To increase the scope and effectiveness of the campus Cooperative Learning Community
                                    through targeted curricular (learning enhancement techniques) and co-curricular (refine
                                    role of Success Coach) adjustments.<strong>  </strong>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-009.pdf">0708-009</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Fine Arts
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D. Dutkofski</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>College Wide Humanities Area 2b Gordon Rule Review</strong>.&nbsp;&nbsp;To introduce and develop an assessment process for “Communicate” in all the Gordon
                                    Rule courses that can be used to both strengthen the writing outcome for humanities
                                    general education courses and to create a college wide rubric for writing assignments
                                    and assessments. <br>
                                    <u>Note</u>:&nbsp; A cooperative project between East, Oseola and West Campuses.  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-010.pdf">0708-010</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Health Sciences
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> L. Pitts</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Assessing Student Success and Satisfaction of Computed Tomography Advanced Technical
                                       Certificate Program and/or Magnetic Resonance Imaging Advanced Technical Certificate
                                       Program</strong>. 
                                    
                                    
                                    To evaluate student success and student satisfaction of Computed Tomography Advanced
                                    Technical Certificate Program and Magnetic Resonance Imaging Advanced Technical Certificate
                                    Program and develop plan for course/program improvement based on assessment data.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-011.pdf">0708-011</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Humanities <br>
                                       &amp; Foreign 
                                       
                                       Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> A. Diaz</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Assessing Writing in Gordon Rule Humanities Classes</strong>. 
                                    
                                    
                                    To introduce and develop an assessment process for “Communicate” in all the Gordon
                                    Rule courses that can be used to both strengthen the writing outcome for humanities
                                    general education courses and to create a college wide rubric for writing assignments
                                    and assessments.  <br>
                                    <u>Note</u>:&nbsp; A cooperative project between East, Oseola and West Campuses. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-012.pdf">0708-012</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mathematics
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. Lee</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Improving Student Faculty Interaction – Habitat House</strong>. 
                                    To evaluate Community College Survey of Student Engagement (CCSSE) data and choose
                                    one outcome for the entire division to focus on related to the improvement of student
                                    engagement.&nbsp; The project selected (Habitat House) is based on the question ‘how often
                                    do students in your selected course section work with you on activities other than
                                    coursework?”
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-013.pdf">0708-013</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> Mathematics</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> West</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> L. Armour</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Division Wide Timelines for Classroom Instruction in MAT0012 and MAT0024. 
                                       
                                       
                                       </strong>To provide complete synchronization between classroom and Open Lab experiences for
                                    students of MAT0012 and MAT0024 by collaboratively devising standardized timelines
                                    for classroom instruction.   
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/0708-014.pdf">0708-014</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mathematics, Science &amp; Psychology 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> T. Grogan</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Supplemental Advising for College Prep Mathematics</strong>. 
                                       
                                       
                                       </strong>To increase student completion of prep math courses, and successful registration in
                                    the following term based on targeted information sharing, tracking of progress and
                                    supplemental advising.                 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-015.pdf">0708-015</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Science
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J. Bivins</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong> <strong>Intercampus Course Redesign in Anatomy and Physiology I. 
                                             
                                             
                                             </strong></strong>To complete and evaluate the redesign of the Anatomy and Physiology I course to include
                                       a strong online supplement to increase student engagement and performance.<br>
                                       <u>Note</u>:&nbsp; A cooperative project between East and West Campuses. <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-016.pdf">0708-016</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Science  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> F. Frierson 
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Intercampus Course Redesign in Anatomy and Physiology I. 
                                          
                                          
                                          </strong></strong>To evaluate the redesign of the Anatomy and Physiology I course to include a strong
                                    online supplement to improve student engagement and performance. <br>
                                    <u>Note</u>:&nbsp; A cooperative project between East and West Campuses. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-017.pdf">0708-017</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Social Sciences <br>
                                       &amp; 
                                       Physical Education
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       M. Villanueva <br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Four Pack Class Offerings for Business Students. </strong></strong>To determine whether packaging Financial and Managerial Accounting classes with Macro
                                    and Micro Economics classes has benefit to business students in terms of retention.
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0708-018.pdf">0708-018</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Valencia Enterprises
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Sand Lake
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J. Robertson, Director, Continuing Education and Lisa Eli, Assistant Director, Center
                                       for Global Languages 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> <strong>Curriculum and Technology Update for Advanced Conversational English Courses</strong>. 
                                       
                                       
                                       </strong>To enhance student learning by updating the curriculum of the Low and High Advanced
                                    Conversational English courses.&nbsp; Like the existing curriculum, the new technology-based
                                    curriculum will utilize a communicative approach to instruction.&nbsp; However, the new
                                    curriculum will introduce new technology (website companion, DVD, Audio CD and student
                                    CD) and updated topics.&nbsp;                
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/division-plans/0708.pcf">©</a>
      </div>
   </body>
</html>