<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Division Action Plans | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/division-plans/0607.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/division-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Division Action Plans</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/division-plans/">Division Plans</a></li>
               <li>Division Action Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>0607 Planning and Evaluation Links</h2>
                        
                        <p>To comply with SACS <i><strong>Principles of Accreditation: Foundations 
                                 for Quality Enhancement </strong></i><strong>(2007 Interim Edition)</strong> Comprehensive 
                           Standard 3.3.1 (below), each academic division will design and implement 
                           an annual plan to assess student learning outcomes. 
                        </p>
                        
                        <p><strong>3.3.1</strong> The institution identifies expected outcomes for its 
                           educational programs <em>(including student learning outcomes for 
                              educational program)</em> and its administrative and educational 
                           support services; assesses whether it achieves these outcomes; and 
                           provides evidence of improvement based on analysis of those results. 
                           <strong>(Institutional effectiveness) </strong></p>
                        
                        <p>The <strong>Index of Plans</strong> provided below includes an informational 
                           overview including a <strong>Division Action Plan (DAP)</strong> number, academic 
                           division of origin, campus, dean and a brief topical focus for each 
                           plan. By clicking on a <strong>Division Action Plan (DAP)</strong> number 
                           you will be linked to current planning and evaluation documents.
                        </p>
                        
                        <p>
                           <strong>   <br>
                              STUDENT LEARNING OUTCOMES ASSESSMENT<br>
                              DIVISION ACTION PLANS (DAPs) FOR 2006-2007</strong></p>
                        
                        <p>
                           <strong>   Index of Plans  </strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>  <strong>DAP No.</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Academic Division</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Campus</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Dean</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Topical Focus</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-001_000.pdf">0607-001</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Architecture, 
                                       Engineering <br>
                                       &amp; Technology
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> N. Hedayat</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Designing, 
                                       Installing, and Troubleshooting Packet-Voice Networks.&nbsp;
                                       </strong>
                                    To effectively train students to identify all the 
                                    network devices needed to build a packet-voice network, 
                                    understand the protocols that are required to make the 
                                    connections among those devices, perform configuration tasks and 
                                    troubleshoot any related problems.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-002_001.pdf">0607-002</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>
                                       Business, Behavioral <br>
                                       &amp; Social Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J. Graber</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Writing in the Disciplines - A Systematic 
                                       Approach to Improvement of Writing Skills.&nbsp; </strong>
                                    To plan and implement a series of "Writing in the 
                                    Disciplines" workshops and studies designed to orient, train and 
                                    evaluate division faculty members on prospective activities, 
                                    instructional methods and assessment techniques.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-003.pdf">0607-003</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Business, IT <br>
                                       &amp; Public 
                                       Services
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J.L. Look</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Business 
                                       Administration and Management - GEB1011 Introduction to Business 
                                       Online Common Course Shell Development.&nbsp;&nbsp; 
                                       </strong>To 
                                    create, maintain, and update a Common Course Shell for all 
                                    GEB1011 Introduction to Business online instructors to implement 
                                    and personalize for class instruction.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-004.pdf">0607-004</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Business, IT <br>
                                       &amp; Social 
                                       Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D.Husbands</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Alternative 
                                       Delivery Research and Development. </strong>
                                    To research and develop approaches 
                                    to alternative delivery including on-line course shells for 
                                    three courses: 
                                    CGS 2100 Microcomputer Fundamentals and 
                                    Applications, CGS 1060 Introduction to Microcomputers and SYG 
                                    2000 Introductory Sociology.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-005.pdf">0607-005</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D. Paul</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Evaluation and 
                                       Training for Improvement of Alternative Delivery Modes</strong>.&nbsp; 
                                    To improve learning through focused evaluation and enhanced 
                                    training of faculty members assigned to alternative delivery 
                                    versions of traditional teaching assignments.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-006.pdf">0607-006</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications    
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> K. Long</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Additional Method of Preparation for ENC 1101 Exit Exam.&nbsp;
                                       </strong>To increase essay writing skills by providing an additional 
                                    method of preparation (one-on-one tutoring with a writing 
                                    instructor in the lab) for students who have been unsuccessful 
                                    in completing the ENC 1101 exit exam.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-007.pdf">0607-007</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications, 
                                       Humanities <br>
                                       &amp; Foreign Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> K. Mulholland</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Assessing the 
                                       Effectiveness of Part-Time and Full-Time Faculty Teaching 
                                       Developmental Courses.&nbsp; </strong>
                                    To compare the 
                                    "success" of part-time and full-time faculty as they teach 
                                    developmental courses - specifically ENC 0012, REA 0002, EAP 
                                    1640, &amp; EAP 1620.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-008_001.pdf">0607-008</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Credit Programs
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Winter Park
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. McArdle</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> Winter Park Cooperative Learning Community 
                                       (WPCLC) Expansion.&nbsp; </strong>To solicit interested part-time 
                                    faculty to join the Winter Park Cooperative Learning Community 
                                    (WPCLC) program, and provide developmental sessions for them 
                                    to learn the philosophy and techniques of the program including 
                                    materials and workshops for part-time faculty teaching introductory 
                                    college classes.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-009_000.pdf">0607-009</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Fine Arts
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D. Dutkofski</p>
                                 </div>
                                 
                                 <div data-old-tag="td"> <strong>Adaptation of Gordon Rule Writing Requirements 
                                       to Humanities Curriculum. </strong>To engage in research and 
                                    development work that will support the adaptation of Gordon 
                                    Rule writing courses in the Humanities to new requirement language. 
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-010_000.pdf">0607-010</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Health Sciences
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> L. Pitts</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Computer-Based Test Taking 
                                       Skills Enhancement</strong>. To correlate HESI and Arnett test 
                                    results between students (experimental group) who participate 
                                    in a computer-based test-taking strategies two-hour workshop 
                                    and those who do not participate (control group) in a test-taking 
                                    strategies workshop in an effort to determine whether teaching 
                                    students specific strategies for computer test-taking and giving 
                                    them practice opportunity will increase the success rate on 
                                    both the HESI and Arnett tests. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-011_000.pdf">0607-011</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Humanities <br>
                                       &amp; Foreign 
                                       
                                       Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> A. Diaz</p>
                                 </div>
                                 
                                 <div data-old-tag="td"> <strong>Composition Writing Spanish Elementary 
                                       I and II. </strong>To develop students' writing skills in the 
                                    target language (Spanish) on the topics delineated for their 
                                    level (SPN 1100 &amp; 1101), Elementary Spanish I and II. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-012.pdf">0607-012</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mathematics
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. Lee</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Development of Course Related Materials for 
                                       the SLS 1101 College Study Skills, Math Anxiety Class.</strong>&nbsp;&nbsp; 
                                    To facilitate student development of concrete strategies that 
                                    acknowledge, understand, and effectively reduce mathematics 
                                    anxiety levels while simultaneously enhancing mathematical 
                                    self-esteem. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-013.pdf">0607-013</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> Mathematics</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> West</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> L. Armour</p>
                                 </div>
                                 
                                 <div data-old-tag="td"> <strong>Assessment of Open Lab Pilot for Sections 
                                       of MAT0012, MAT0020, and MAT0024. </strong>To perform formative 
                                    assessment of the expanded Open Lab pilot (all sections of MAT 
                                    0012C, MAT 0020C, and MAT 0024C) based on last year's initial 
                                    approach that used open lab hours to replace the scheduled lab 
                                    day and time for students in MAT 0012C.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/0607-014.pdf">0607-014</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mathematics &amp; Science
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> T. Grogan</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Intercampus Success in Chemistry.&nbsp; </strong>To assess the impact 
                                    of course design, instruction and assessment in CHM 1025C on 
                                    learning in CHM 1045C.<br>
                                    <u>Note</u>:&nbsp; A cooperative project among East, Osceola 
                                    and West Campuses.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-015.pdf">0607-015</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Science
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J. Bivins</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Intercampus Success in Chemistry.&nbsp; </strong>To assess the impact 
                                    of course design, instruction and assessment in CHM 1025C on 
                                    learning in CHM 1045C.<br>
                                    <u>Note</u>:&nbsp; A cooperative project among East, Osceola 
                                    and West Campuses.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-016_000.pdf">0607-016</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Science  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> R. Keiper</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Intercampus Success in Chemistry.&nbsp; </strong>To assess the impact 
                                    of course design, instruction and assessment in CHM 1025C on 
                                    learning in CHM 1045C.<br>
                                    <u>Note</u>:&nbsp; A cooperative project among East, Osceola 
                                    and West Campuses.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-017.pdf">0607-017</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Social Sciences <br>
                                       &amp; 
                                       Physical Education
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> S. Robinson</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Assessment of 
                                       Success of Flex Start Classes.&nbsp; </strong>
                                    To determine if 
                                    courses offered in Flex Start terms (H2, TWK, TR2, and TR3) have 
                                    success rates equal to or greater than courses offered during 
                                    the full term.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> <a href="documents/0607-018.pdf">0607-018</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Valencia Enterprises
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Sand Lake
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> C. McMorran<br>
                                       (Senior Performance Consultant)
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>
                                       Assessment 
                                       and Improvement of the Society for Human Resources Management (SHRM) 
                                       Program.&nbsp; </strong>
                                    To improve preparation and training for the 
                                    Society for Human Resources Management (SHRM) Program relative 
                                    to student success as measured by evaluation data and exam pass 
                                    rates.
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/division-plans/0607.pcf">©</a>
      </div>
   </body>
</html>