<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Division Action Plans | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/division-plans/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/division-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Division Action Plans</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Division Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>0809 Planning &amp; Evaluation Links<br>
                           (Academic Divisions and Other Instructional Areas) 
                        </h2>
                        
                        <p>To comply with SACS <i><strong>Principles of Accreditation: Foundations 
                                 for Quality Enhancement </strong></i> Comprehensive 
                           Standard 3.3.1.1 (below), each academic division will design and implement 
                           an annual plan to assess student learning outcomes. 
                        </p>
                        
                        <p>3.3.1 The institution identifies expected outcomes, assesses the extent to which it
                           achieves these outcomes, and provides evidence of improvement based on analysis of
                           the results in each of the following areas <strong>(Institutional Effectiveness)</strong>: 
                        </p>
                        
                        <blockquote>
                           
                           <blockquote>
                              
                              <blockquote>
                                 <strong>3.3.1.1 educational programs, to include student learning outcomes </strong><br>
                                 <br>
                                 3.3.1.2 administrative support services <br>
                                 <br>
                                 3.3.1.3 educational support services <br>
                                 <br>
                                 3.3.1.4 research within its educational mission, if appropriate <br>
                                 <br>
                                 3.3.1.5 community/public service within its educational mission, if appropriate 
                              </blockquote>
                              
                           </blockquote>
                           
                        </blockquote>
                        
                        <p>Links to historical <strong>Division Plans</strong> are located in the Navigate section on the left margin of this screen. 
                        </p>
                        
                        <p>Beginning with the 2008-2009 academic year, documentation of planning, evaluation
                           and improvement cycles was tranistioned to a new system called <em><strong>Planning by Valencia</strong><strong>.</strong></em> This online tool is designed to promote institutional effectiveness and demonstarte
                           compliance with SACS <i><strong>Principles of Accreditation: Foundations for Quality Enhancement</strong></i> Comprehensive Standard 3.3.1 and  Core Requirement 2.5. The full system can be accessed
                           via the <strong><a href="https://atlas.valenciacollege.edu/">Atlas</a> Employees</strong> tab. <br>
                           <br>
                           <br>
                           The <strong> Index of Plans for Academic Divisions </strong> <strong>and Other Instructional Areas</strong> below provides an informational 
                           overview including  academic 
                           division or area of origin, campus and dean (director) as well as the title, principal
                           objective and relation to the Strategic Plan. By clicking on the <strong>Academic Division</strong><strong> or Other Instructional Area</strong>              you will be linked to current planning and evaluation information.
                        </p>
                        
                        <p><br>
                           <strong>              STUDENT LEARNING OUTCOMES ASSESSMENT<br>
                              DIVISION ACTION PLANS (DAPs) FOR 2008-2009</strong></p>
                        
                        <p>
                           <strong>   Index of Plans for Academic Divisions and Other Instructional Areas </strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Academic Division or Area </strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Campus</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p>  
                                       <strong>Dean</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div><strong>Title/<br>
                                          Objective/<br>
                                          Relation to Strategic Plan 
                                          </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809ArchitectureEngineeringandTechnologyW.pdf">Architecture, 
                                          Engineering <br>
                                          &amp; Technology</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> N. Hedayat</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Assessing Effects of Game Programming Course and Certificate<br>
                                          Additions on IT Enrollment</strong><br>
                                       <br>
                                       Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience
                                       that enhances student learning. 
                                    </p>
                                    
                                    <p>This initiative will study the effectiveness of integrating game programming projects
                                       into the computer programming<br>
                                       curriculum to provide students with a more authentic, interesting learning experience.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/DAP0809ArtsandEntertainmentE.pdf">Arts &amp; Entertainment</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>East</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>W. Givoglu </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Develop Vision, Mission, and Program Outcomes for Arts &amp; Entertainment, East Campus<br>
                                          <br>
                                          </strong>Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience 
                                       that enhances student learning. 
                                    </p>
                                    
                                    <p>By defining who we are and where we plan to go as Arts &amp; Entertainment, a new Academic
                                       Division, we will be able to
                                       position ourselves to assess student learning and achievement of program outcomes
                                       as an operational unit.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>
                                       <a href="documents/DAP0809BusinessBehavioralandSocialSciencesW.pdf">Business, Behavioral <br>
                                          &amp; Social Sciences </a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. Franceschi </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Atlas LifeMap E-Portfolio Development as a Learning Tool in 
                                          EME 2040</strong><br>
                                       <br>
                                       Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience 
                                       that enhances student learning.
                                    </p>
                                    
                                    <p>Creating the portfolio as part of EME 2040 assesses student learning in terms of Florida
                                       state competencies for 
                                       educators. It assures that students see that what they do in Technology for Educators
                                       at Valencia College
                                       contributes to graduation requirements at a 4-year institution as well as to an increase
                                       of the knowledge and skills
                                       required by educators.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="documents/DAP0809BusinessITandPublicServicesE.pdf.html">Business, IT <br>
                                          &amp; Public 
                                          Services</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J.L. Look</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Principles for AS Program Outcome Development</strong></p>
                                    
                                    <p>Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience
                                       that enhances student learning.
                                    </p>
                                    
                                    <p>This is year one of a three year plan to develop assessable program outcomes for disciplines
                                       in this division to insure positive impact on learning.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809BusinessITandSocialSciencesO.pdf">Business, IT <br>
                                          &amp; Social 
                                          Sciences </a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D.Husbands</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Evaluation of Computers Based Information and Computer Skills<br>
                                          in Economics </strong></p>
                                    
                                    <p>Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience 
                                       that enhances student learning.
                                    </p>
                                    
                                    <p>This initiative will help to determine the optiomal computer and information related
                                       skills for students taking<br>
                                       Economics courses.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809CommunicationsE.pdf">Communications</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D. Paul</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong><strong>Holistic Grading of ENC 1101 Documented Essays</strong><br>
                                       (Multi-Campus:  East, West)
                                       <br>
                                       <br>
                                       </strong>To increase the percentage of students writing at the college level. <br>
                                    <br>
                                    By creating a consistent measure and understanding of college level writing amongst
                                    all faculty that teach ENC 1101, students will be assured that learning in all classrooms
                                    is assessed in the same manner. By meeting with all faculty and assessing student
                                    writing together, faculty will have very similar expectations of what constitutes
                                    college level writing. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809CommunicationsW.pdf">Communications    </a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> K. Long</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Holistic Grading of ENC 1101 Documented Essays</strong>                  <br>
                                       <strong>(Multi-Campus: East,  West)<br> 
                                          </strong><br>
                                       To increase the percentage of students writing at the college level. <br>
                                       <br>
                                       By creating a consistent measure and understanding of college level writing amongst
                                       all faculty that teach ENC 1101,
                                       students will be assured that learning in all classrooms is assessed in the same manner.
                                       By meeting with all faculty
                                       and assessing student writing together, faculty will have very similar expectations
                                       of what constitutes college level 
                                       writing.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809CommunicationsHumanitiesandForeignLanguagesO.pdf">Communications, 
                                          Humanities <br>
                                          &amp; Foreign Languages</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> K. Mulholland</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Assessing Writing in Gordon Rule Humanities Classes</strong><br>
                                       <strong>(Multi-Campus: East, Osceola, West) </strong><br>
                                       <br>
                                       To increase the percentage of students writing at the college level. <br>
                                       <br>
                                       This initiative will introduce and develop an assessment process for “Communicate”
                                       in all the Gordon Rule courses 
                                       that can be used to both strengthen the writing outcome for humanities general education
                                       courses and to create a 
                                       college wide rubric for writing assignments and ssessments. As such, it will help
                                       us find out if learning is truly 
                                       assured in our Gordon Rule humanities classes.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809CreditProgramsWP.pdf">Credit Programs</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Winter Park
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. McArdle</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Closing the Gap in Pre Algebra MAT 0012</strong><br>
                                       <strong>(Multi-Campus: East, Osceola, West, Winter Park) </strong><br>
                                       <br>
                                       Close achievement gaps among students from diverse backgrounds in completing six key
                                       courses, leading to<br>
                                       increased persistence and program completion rates. [Note: The six courses are: College
                                       Prep mathematics Pre-Algebra (MAT0012C) and Beginning Algebra (MAT 0024C), Intermediate
                                       Algebra (MAT1033), and “gateway” courses, Communications (ENC1101), Political Science
                                       (POS2041), and College Algebra (MAC1105).]
                                    </p>
                                    
                                    <p>This initiative relates to Building Pathways and Learning Assured. PreAlgebra is the
                                       first math class that these students will take at Valencia so they need assistance
                                       transitioning from the college. We are also working to close achievement gaps among
                                       students from diverse backgrounds.
                                    </p>                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809FineArtsW.pdf">Fine Arts</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D. Dutkofski</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Assessing Writing in Gordon Rule Humanities Classes</strong>                  <br>
                                       <strong>(Multi-Campus: East, Osceola, West) </strong><br>
                                       <br>
                                       Increase the percentage of students writing at the college level.
                                    </p>
                                    
                                    <p>This initiative will introduce and develop an assessment process for "Communicate"
                                       in all Gordon Rule courses that 
                                       are used to strengthen the writing outcome for humanities general education courses
                                       and to create a college wide 
                                       rubric for writing assignments and assessments. As such, it will measure the progress
                                       in writing students are 
                                       achieving in the humanities courses.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809HealthSciencesW.pdf">Health Sciences</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> L. Pitts</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Assessing Information Literacy in Advance Standing Nursing<br>
                                          Program</strong><br>
                                       <br>
                                       Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience
                                       that enhances student learning.
                                    </p>
                                    
                                    <p>This initiative will assess the ability of nursing students to locate, evaluate, and
                                       use effectively research-based<br>
                                       information found in nursing journals and approved nursing web sites to support evidence-based
                                       nursing practice. 
                                       Student learning will be evaluated in an applied context through examination of work
                                       quality related to scheduled
                                       discussions and demonstration of effective clinical-decion making.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809HumanitiesForeignLanguageE.pdf">Humanities <br>
                                          &amp; Foreign 
                                          
                                          Language</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> D. Sutton </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Assessing Writing in Gordon Rule Humanities Classes</strong>                  <br>
                                    <strong>(Multi-Campus: East, Osceola, West) </strong><br>
                                    <br>
                                    To increase the percentage of students writing at the college level. <br>
                                    <br>
                                    This initiative will introduce and develop an assessment process for “Communicate”
                                    in all the Gordon Rule courses that can be used to both strengthen the writing outcome
                                    for humanities general education courses and to create a college wide rubric for writing
                                    assignments and ssessments. As such, it will help us find out if learning is truly
                                    assured in our Gordon Rule humanities classes.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809MathematicsE.pdf">Mathematics</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. Lee</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Closing the Gap in PreAlgebra MAT0012</strong> <strong><br>
                                          (Multi-Campus: East, Osceola, West, Winter Park) </strong><br>
                                       <br>
                                       Close achievement gaps among students from diverse backgrounds in completing six key
                                       courses, leading to<br>
                                       increased persistence and program completion rates. [Note: The six courses are: College
                                       Prep mathematics Pre-Algebra (MAT0012C) and Beginning Algebra (MAT 0024C), Intermediate
                                       Algebra (MAT1033), and “gateway” courses, Communications (ENC1101), Political Science
                                       (POS2041), and College Algebra (MAC1105).]
                                    </p>
                                    
                                    <p>This initiative relates to Building Pathways and Learning Assured. PreAlgebra is the
                                       first math class that these students will take at Valencia so they need assistance
                                       transitioning from the college. We are also working to close achievement gaps among
                                       students from diverse backgrounds.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p> <a href="documents/DAP0809MathematicsW.pdf">Mathematics</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> West</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> L. Armour</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Closing the Gap in PreAlgebra MAT0012</strong><strong><br>
                                          (Multi-Campus: East, Osceola, West, Winter Park) </strong><br>
                                       <br>
                                       Close achievement gaps among students from diverse backgrounds in completing six key
                                       courses, leading to<br>
                                       increased persistence and program completion rates. [Note: The six courses are: College
                                       Prep mathematics Pre-Algebra (MAT0012C) and Beginning Algebra (MAT 0024C), Intermediate
                                       Algebra (MAT1033), and “gateway” courses, Communications (ENC1101), Political Science
                                       (POS2041), and College Algebra (MAC1105).]
                                    </p>
                                    
                                    <p>This initiative relates to Building Pathways and Learning Assured. PreAlgebra is the
                                       first math class that these students will take at Valencia so they need assistance
                                       transitioning from the college. We are also working to close achievement gaps among
                                       students from diverse backgrounds.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809MathematicsScienceandPsychologyO.pdf">Mathematics, Science &amp; Psychology</a> 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> M. Pedone </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Closing the Gap in PreAlgebra MAT0012<br>
                                          </strong><strong>(Multi-Campus: East, Osceola, West, Winter Park) </strong><br>
                                       <br>
                                       Close achievement gaps among students from diverse backgrounds in completing six key
                                       courses, leading to<br>
                                       increased persistence and program completion rates. [Note: The six courses are: College
                                       Prep mathematics
                                       Pre-Algebra (MAT0012C) and Beginning Algebra (MAT 0024C), Intermediate Algebra (MAT1033),
                                       and “gateway”
                                       courses, Communications (ENC1101), Political Science (POS2041), and College Algebra
                                       (MAC1105).]
                                    </p>
                                    
                                    <p>This initiative relates to Building Pathways and Learning Assured. PreAlgebra is the
                                       first math class that these students
                                       will take at Valencia so they need assistance transitioning from the college. We are
                                       also working to close achievement
                                       gaps among students from diverse backgrounds.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809ScienceE.pdf">Science</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J. Bivins</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p> <strong>Science E and W - Evaluating Anatomy and 
                                          Physiology I Course Prerequisites</strong><br>
                                       <strong>(Multi-Campus: East,  West) </strong><br>
                                       <br>
                                       Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience
                                       that enhances student learning.
                                    </p>
                                    
                                    <p>Quantitative and Scientific Reasoning outcomes will be measured for students completing
                                       Anatomy and Physiology I. 
                                       A comparative evaluation will be completed for students entering Anatomy and Physiology
                                       I after completing different prerequisites.                  
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809ScienceW.pdf">Science  </a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> F. Frierson 
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Evaluating Anatomy and Physiology 1 Course Prerequisites</strong><br>
                                       <strong>(Multi-Campus: East, West) </strong><br>
                                       <br>
                                       Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience
                                       that enhances student learning. 
                                    </p>
                                    
                                    <p>Quantitative and Scientific Reasoning outcomes will be measured for students completing
                                       Anatomy and Physiology 1. 
                                       A comparative evaluation process will be generated for students entering Anatomy and
                                       Physiology 1 after completing<br>
                                       different prerequisite courses.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809SocialSciencesandPhysicalEducationE.pdf">Social Sciences <br>
                                          &amp; Physical Education</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       M. Villanueva <br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Re-alignment of Program Requirements, Outcomes and<br>
                                          Assessment Measures for Sign Language</strong></p>
                                    
                                    <p>Develop, align, and review program learning outcomes to assure a cohesive curricular
                                       and co-curricular experience
                                       that enhances student learning. 
                                    </p>
                                    
                                    <p>Re-alignment of program requirements, outcomes and assessment measures should lead
                                       to students achieving the 
                                       respective competencies and enhancing transferability to a four-year degree program.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/DAP0809StudentLifeSkillsCW.pdf">Student Life Skills</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>College Wide </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>M. Allen</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Designing a Three-Term Student Success Sequence for
                                          Bridges Students</strong><br>
                                       <br>
                                       Close achievement gaps among students from diverse backgrounds in completing six key
                                       courses, leading to<br>
                                       increased persistence and program completion rates. [Note: The six courses are: College
                                       Prep mathematics
                                       Pre-Algebra (MAT0012C) and Beginning Algebra (MAT 0024C), Intermediate Algebra (MAT1033),
                                       and “gateway” courses, Communications (ENC1101), Political Science (POS2041), and
                                       College Algebra (MAC1105).]
                                    </p>
                                    
                                    <p>A sequence of Student Success courses for Bridges students that specifically support
                                       success in mathematics is
                                       intended to help close the achievement gap across race and ethnicity at Valencia.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <a href="documents/DAP0809ValenciaEnterprisesSL.pdf">Valencia Enterprises</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Sand Lake
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> J. Robertson, Director, Continuing Education and Lisa Eli, Assistant Director, Center
                                       for Global Languages 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Streamlining the F-1 Transfer-In Process from VE to VCC</strong><br>
                                       <br>
                                       Create a seamless transfer in process for CGL’s F1 international students from
                                       the non-credit Intensive English Program into the Valencia degree program.
                                    </p>
                                    
                                    <p>Valencia has made excellent strides in streamlining their admissions policies<br>
                                       to the Strategic Plan: and procedures over the last several years. This goal directly
                                       supports the Build
                                       Pathways initiative by making the entire transfer process student centered.<br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/division-plans/index.pcf">©</a>
      </div>
   </body>
</html>