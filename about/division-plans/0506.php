<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Division Action Plans | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/division-plans/0506.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/division-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Division Action Plans</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/division-plans/">Division Plans</a></li>
               <li>Division Action Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>0506 Planning and Evaluation Links</h2>
                        
                        <p>To comply with SACS <i><strong>Principles of Accreditation</strong></i> requirements, <strong>Comprehensive Standards 3.3.1</strong> and <strong>3.4.1</strong> (below), each 
                           academic division will design and implement an annual plan to 
                           assess student learning outcomes.  
                        </p>
                        
                        <p><strong>3.3.1</strong> The institution identifies expected outcomes for its 
                           educational programs and its administrative and educational support services; assesses
                           whether it achieves these outcomes; and provides evidence of improvement based on
                           analysis of those results.
                        </p>
                        
                        <p><strong>3.4.1</strong> The institution demonstrates that each educational program 
                           for which academic credit is awarded (a) is approved by the faculty and the
                           administration, and (b) establishes and evaluates program and 
                           learning outcomes.
                        </p>
                        
                        <p>The <strong>Index of Plans</strong> provided below includes an
                           informational overview including a <strong>Division Action Plan (DAP)</strong> number, academic division of origin, campus, dean and a brief
                           topical focus for each plan. By clicking on a <strong>Division Action
                              Plan (DAP)</strong> number you will be linked to current planning and
                           evaluation documents.
                        </p>
                        
                        <p>
                           <strong>   <br>
                              STUDENT LEARNING OUTCOMES ASSESSMENT<br>
                              DIVISION ACTION PLANS (DAPs) FOR 2005-2006</strong></p>
                        
                        <p>
                           <strong>  Index of Plans  </strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <strong>DAP No.</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <strong>Academic Division</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <strong>Campus</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <strong>Dean</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       <strong>Topical Focus</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-001.pdf">0506-001</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Architecture, 
                                       Engineering <br>
                                       &amp; Technology
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Hawat
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Construction Estimating Project.&nbsp; </strong>To assess the 
                                    student's technical math and blueprint reading skills, and the 
                                    effectiveness of learning estimating skills on a student's 
                                    productivity in developing a complete estimate for an actual 
                                    construction project.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-002.pdf">0506-002</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>
                                       Business, Behavioral <br>
                                       &amp; Social Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Graber
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Coordination of Student Projects with Real World Business 
                                       Activity.</strong> To enhance student learning and refine assessment 
                                    of knowledge acquired by designing and supporting connections 
                                    between assigned projects and real world business opportunities.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-003.pdf">0506-003</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Business, IT <br>
                                       &amp; Public 
                                       Services
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Look
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Outcome-Based Learning-Centered IT Labs. </strong>To improve 
                                    assessment in specific IT courses where a large percentage of 
                                    sections are taught by part time faculty through development of 
                                    learning activities and assessments that can be utilized by all 
                                    instructors teaching those courses.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-004.pdf">0506-004</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Business, IT <br>
                                       &amp; Social 
                                       Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Husbands
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Continuation of Evaluation of On-line Orientations versus 
                                       In-class Orientations.&nbsp;&nbsp; </strong>To continue to analyze 
                                    the effectiveness of online orientations versus in-class 
                                    orientations for SYG 2000 and POS 2041 courses in relations to 
                                    student completion rates in those courses.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-005.pdf">0506-005</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Paul
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> Improvement 
                                       in College Prep English and Reading</strong>.&nbsp; 
                                    To improve the rate and level of passing scores on the State 
                                    Competency Exam; assess the level of skill mastery taught and 
                                    tested in REA 0002 and ENC 0012 by tracking&nbsp; the percentage of 
                                    success in Freshman Composition I and courses such as Speech, 
                                    Humanities, and U.S. Government.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-006.pdf">0506-006</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications    
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Borglum
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Jump Start.</strong> To determine if 
                                    students who participate in the Jump START program will be more 
                                    successful than students who are not part of this learning 
                                    community.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-007.pdf">0506-007</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Communications, 
                                       Humanities <br>
                                       &amp; Foreign Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mulholland
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Assessing the Effectiveness of "Alternative Format" ENC 0012 
                                       &amp; REA 0012 Classes</strong>.&nbsp; To determine if "alternative 
                                    format" courses in developmental areas of the curriculum will 
                                    promote or hurt learning.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-008.pdf">0506-008</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Credit Programs
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Winter Pk.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       McArdle
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Cooperative Learning Community Expansion.&nbsp; </strong>To expand 
                                    the integrated curriculum to include prep English and to 
                                    continue the work with the second course in prep math and prep 
                                    reading.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-009.pdf">0506-009</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Fine Arts
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Dutkofski
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Enhancement in Humanities through Improved Reading Skills.&nbsp; </strong>To redesign the course structure and delivery so that 
                                    students will be encouraged to read assignments consistently and 
                                    develop higher level reading comprehension skills.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-010.pdf">0506-010</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Health Sciences
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       White
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Relationship of Outside Testing to Nursing Curriculum.&nbsp; </strong>To correlate HESI and Arnett test results of last semester 
                                    nursing students with the nursing program curriculum content in 
                                    an effort to determine (a) individual student &nbsp;performance in 
                                    curriculum content, (b) student group performance in curriculum 
                                    content and (c) presence of tested concepts in nursing 
                                    curriculum.  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-011.pdf">0506-011</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Humanities <br>
                                       &amp; Foreign 
                                       
                                       Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Diaz
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Assessment of Oral Skills in Selected Foreign Languages.&nbsp; 
                                       &nbsp;</strong>To promote a consistent assessment of students' oral skills 
                                    in the target foreign language (Spanish, French, German)<strong>. </strong>                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-012.pdf">0506-012</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mathematics
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Lee
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Improve 
                                       Student's Critical Thinking.&nbsp; </strong>To increase East Campus 
                                    math students' perception and development of critical thinking 
                                    skills.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-013.pdf">0506-013</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mathematics
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Armour
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Pre-Algebra Lab Initiative.&nbsp; </strong>To improve learning 
                                    support by providing students in six pilot sections with 
                                    a&nbsp; new lab design based on individual diagnostics followed 
                                    by concentration on prescribed learning outcomes.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-014.pdf">0506-014</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Mathematics &amp; Science
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Grogan
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>State Exam Performance:&nbsp; Radical 
                                       Expressions.&nbsp; </strong>To 
                                    increase student pass rate on MAT0024 state exit exam.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-015.pdf">0506-015</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Science
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Williams
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Student Biological Knowledge Base Baseline Data Survey.&nbsp; </strong>To establish a baseline estimation of what students know 
                                    about biology when they first enter our courses and to assess 
                                    changes in this baseline after completion of a course in 
                                    biology.<strong>&nbsp; </strong>  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-016.pdf">0506-016</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Science  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Keiper
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Chemistry Remediation for the Under Prepared Microbiology 
                                       Student. </strong>&nbsp;To improve student learning of basic chemical 
                                    concepts by introducing foundation principles through active and 
                                    cooperative learning, formative assessment and purposeful 
                                    mentoring.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-017.pdf">0506-017</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Social Sciences <br>
                                       &amp; 
                                       Physical Education
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Robinson
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Evaluation of Education Orientation and 
                                          Orientation Handbook.&nbsp; </strong>To evaluate and improve the 
                                       effectiveness of the East Campus Education Orientation and the 
                                       Education Student Handbook.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="DAPsnPDF/0506-018.pdf">0506-018</a></div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       Valencia Institute
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Sand Lake
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  
                                       McNamara
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Student Learning Outcomes (Accredited 
                                          Claims Adjuster Training).&nbsp; </strong>To determine if Accredited 
                                       Claims Adjuster students score sufficiently on the state exam to 
                                       become employable after completing the curriculum.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><span><a href="#top">TOP</a></span></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/division-plans/0506.pcf">©</a>
      </div>
   </body>
</html>