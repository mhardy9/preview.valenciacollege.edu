<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Division Action Plans | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/division-plans/0405.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/division-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Division Action Plans</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/division-plans/">Division Plans</a></li>
               <li>Division Action Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>0405 Planning and Evaluation Links</h2>
                        
                        <p>To comply with SACS <i><strong>Principles of Accreditation</strong></i> requirements, <strong>Comprehensive Standards 3.3.1</strong> and <strong>3.4.1</strong> (below), each 
                           academic division will design and implement an annual plan to 
                           assess student learning outcomes.  
                        </p>
                        
                        <p><strong>3.3.1</strong> The institution identifies expected outcomes for its 
                           educational programs and its administrative and educational support services; assesses
                           whether it achieves these outcomes; and provides evidence of improvement based on
                           analysis of those results.
                        </p>
                        
                        <p><strong>3.4.1</strong> The institution demonstrates that each educational program 
                           for which academic credit is awarded (a) is approved by the faculty and the
                           administration, and (b) establishes and evaluates program and 
                           learning outcomes.
                        </p>
                        
                        <p>The <strong>Index of Plans</strong> provided below includes an
                           informational overview including a <strong>Division Action Plan (DAP)</strong> number, academic division of origin, campus, dean and a brief
                           topical focus for each plan. By clicking on a <strong>Division Action
                              Plan (DAP)</strong> number you will be linked to current planning and
                           evaluation documents.
                        </p>
                        
                        <p>
                           <strong> <br>
                              STUDENT LEARNING OUTCOMES ASSESSMENT<br>
                              DIVISION ACTION PLANS (DAPs) FOR 2004-2005</strong></p>
                        
                        <p>
                           <strong> Index of Plans </strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <strong>DAP No.</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       <strong>Academic Division</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       <strong>Campus</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       <strong>Dean</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       <strong>Topical Focus</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-001.pdf">0405-001</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Architecture, 
                                       Engineering <br>
                                       &amp; Technology
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Hawat
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> Microcomputer Fundamentals Business Project.&nbsp; </strong> To assess the affects of learning Microsoft 
                                       Office skills on a student’s real-world workplace productivity.&nbsp;                 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPsinPDF/0405-002.pdf">0405-002</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Business, Behavioral <br>
                                       &amp; Social Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Graber
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Assessment of Cognitive Writing Skills 
                                          in Selected Gordon Rule and Non-Gordon Rule Courses</strong>. 
                                       To discover how effective formative assessment is in improving 
                                       student performance on high-level cognitive writing tasks in a 
                                       Gordon Rule History class and a non-Gordon Rule U.S. Government 
                                       class.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-003.pdf">0405-003</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Business, IT <br>
                                       &amp; Public 
                                       Services
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Look
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> Paralegal Skills-Based Project Incorporation into 
                                          Curriculum.&nbsp; </strong>To devise 
                                       skill-based activities for each course in paralegal program.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-004.pdf">0405-004</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Business, IT <br>
                                       &amp; Social 
                                       Sciences  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Husbands
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> Evaluation of On-line Orientations versus 
                                          In-class Orientations. </strong> To analyze the effectiveness of 
                                       online orientations versus in-class orientations for SYG 2000 
                                       and POS 2041 courses in relation to student completion rates in 
                                       those courses.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-005.pdf">0405-005</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Communications
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Paul
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Student Learning in SPC 
                                          1600.&nbsp; </strong>To explore student 
                                       learning in SPC1600, a highly enrolled front-door course
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-006.pdf">0405-006</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Communications  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Borglum
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> Assessment of Student Persuasive and Informative 
                                          Speeches.&nbsp; </strong> To provide 
                                       more consistent assessment of student persuasive and informative 
                                       speeches.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-007.pdf">0405-007</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Communications, 
                                       Humanities <br>
                                       &amp; Foreign Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Mulholland
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> Assessing the Effectiveness of Hybrid Classes.</strong>&nbsp; To 
                                       assess the effectiveness of “hybrid courses” (50% online/50% 
                                       seat time) in communications and humanities.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-008.pdf">0405-008</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Credit Programs
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Winter Pk.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       McArdle
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Start Right Project.&nbsp; </strong>To increase student pass rates and success in 
                                       sequential courses based on&nbsp; added academic and social 
                                       support.  
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-009.pdf">0405-009</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Fine Arts
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Dutkofski
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> Reading Enhancement</strong>.&nbsp; 
                                       To redesign course structure and delivery so that students will 
                                       be encouraged to read the assignments and develop higher level 
                                       comprehension skills in reading.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-010.pdf">0405-010</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Health Sciences
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       White
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> #1 
                                          Nursing Program Outcomes. </strong> To 
                                       establish satisfactory performance for NCLEX pass rates, 
                                       graduation rates, job placement rates, and student &amp; employer 
                                       satisfaction rates.<strong><br>
                                          #2 Nursing Curriculum Tracking.&nbsp; </strong> To 
                                       implement a computer-based student tracking system during the 
                                       05-06 academic year as required for information collection, 
                                       planning and program decision making.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-011.pdf">0405-011</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Humanities <br>
                                       &amp; Foreign 
                                       &nbsp;Languages
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Diaz
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Active Communications 
                                          Rubric for Basic Spanish (SPN 1000).&nbsp; </strong> To enhance the effectiveness of assessing oral 
                                       development in Basic Spanish (SPN 1000) utilizing an internally 
                                       developed rubric.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="0405-012.pdf">0405-012</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Mathematics
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Phillips
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> Assessment of Curriculum Continuity</strong>. 
                                       To measure success of students moving through the mathematics 
                                       curriculum on East Campus by comparing success rates of students 
                                       entering math classes after taking the pre-requisite course on 
                                       East Campus to the success rates of students in the same math 
                                       class overall.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-013.pdf">0405-013</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Mathematics
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Armour
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>  <strong>The Comp House Family Plan.</strong>&nbsp; To form a learning community with the elementary 
                                       algebra students and their professors to improve the passing 
                                       rates by 5% in each successive semester on the statewide 
                                       competency examination given in both the MAT0024C Beginning 
                                       Algebra and MAT0020C Prep Math Intensive courses.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-014.pdf">0405-014</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Mathematics &amp; Science
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Osceola
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Grogan
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> State Exam Performance: Exponents.&nbsp; </strong>To 
                                       increase student pass rate on MAT0024 state exit exam.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-015.pdf">0405-015</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Science
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Williams
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Student 
                                          Biological Knowledge Baseline Data Survey (SBKBDS)</strong>.
                                       To establish a baseline estimation of what 
                                       students know about biology when they first enter our courses.<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-016.pdf">0405-016</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Science  
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       West
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Keiper
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong> &nbsp;“Culture 
                                          and Sensitivity” Simulation.&nbsp; </strong> One of the 
                                       major goals in Microbiology is to identify the specific microbe 
                                       that is causing an infection and determine the correct 
                                       antibiotic to use in treating that infection through a technique 
                                       called “Culture and Sensitivity” (C&amp;S). To simulate C&amp;S, 
                                       students were required to identify 2 unknown bacteria using 19 
                                       different tests they had used in previous labs.  
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-017.pdf">0405-017</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Social Sciences <br>
                                       &amp; 
                                       Physical Education
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       East
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Robinson
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>College-Wide Exit 
                                          Examinations for American Sign Language (SPA 2612 &amp; 2613).&nbsp; </strong>To implement a college-wide American 
                                       Sign Language I (SPA 2612) &amp; American Sign Language II (SPA 
                                       2613) Exit Examination to increase student achievement rates and 
                                       preparedness for higher levels of American Sign Language.&nbsp;                 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>  <a href="DAPSinPDF/0405-018.pdf">0405-018</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Valencia Institute
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       Sand Lake
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 
                                       McNamara
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Student Learning 
                                          Outcomes.&nbsp; </strong>To determine if Animal 
                                       Control officers score sufficiently on the state exam to become 
                                       employable after completing current curriculum.&nbsp;  
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><span><a href="#top">TOP</a></span></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/division-plans/0405.pcf">©</a>
      </div>
   </body>
</html>