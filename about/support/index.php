<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Support | Valencia College</title>
      <meta name="Description" content="Valencia Support">
      <meta name="Keywords" content="college, school, educational, student, support">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/support/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/support/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li>Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <h2 data-old-tag="th">Student Support</h2>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Contact the Atlas Student Help Desk at <a href="mailto:askatlas@valenciacollege.edu">askatlas@valenciacollege.edu</a> or 407-582-5444
                                    </p>
                                    <a href="/about/support/howto/index.html">How To...</a><br> Student tutorials and guides for registration, records, graduation, Atlas and LifeMap
                                    tools.
                                    
                                    <ul>
                                       
                                       <li><a href="/academics/answer-center.html">Answer Center</a></li>
                                       
                                       <li><a href="/about/admissions-records/registration-details/atlas_access.html">Atlas Access for Registration</a></li>
                                       
                                       <li><a href="/about/student-services/atlas-access-labs.html">Atlas Access Lab Locations</a></li>
                                       
                                    </ul>
                                    
                                    <p><a href="/about/oit/learning-technology-services/student-resources/index.html">Valencia Online</a><br> Get help with online courses and more.
                                    </p>
                                    
                                    <p><a href="/students/office-for-students-with-disabilities/default.html">Office for Students with Disabilities</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit Valencia</a><br> Schedule a campus visit.
                                    </p>
                                    
                                    <p><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskwpro.P_ChooseProspectType" target="_blank">Request Information</a><br> For first time in college or transfer students.
                                    </p>
                                    
                                    <p><a href="/students/transitions-planning/index.html">Transitions<br> </a>For assisting students in making decisions about their educational paths following
                                       high school or as a career change.
                                    </p>
                                    
                                    <p><a href="/admissions/orientation/index.html">New Student Orientatation (NSO)</a><br> All new students are required to participate in an orientation session before registering
                                       for their first term. 
                                    </p>
                                    
                                    <p><a title="Florida Department of Education" href="http://www.fldoe.org/department/contact.asp" target="_blank">Florida Department of Education</a><br> For questions &amp; comments regarding education issues in Florida
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <h2 data-old-tag="th">Directories &amp; Maps</h2>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="/about/contact/index.html">Contact Us</a><br> Quickly find e-mail addresses, phone numbers and mailing addresses of Valencia faculty
                                    and staff.
                                 </div>
                                 
                                 <div data-old-tag="td">&nbsp;</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="/map/index.html">Locations &amp; Maps</a><br> Find out how to get to any of Valencia's four campuses and two centers.
                                 </div>
                                 
                                 <div data-old-tag="td">&nbsp;</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <h2 data-old-tag="th">General Information</h2>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="/about/support/search/index.html">Search Valencia Web Sites</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <h2 data-old-tag="th">Faculty &amp; Staff Support</h2>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:OITHelp@valenciacollege.edu">Email OIT Help</a><br> Technical help desk support for Valencia Faculty &amp; Staff.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="/about/OIT/index.html" target="_blank">Online Chat</a><br> Chat directly with OIT Help via the OIT Website.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/ServiceDesk" target="_blank">Online Request</a><br> Submit an online request with OIT Help.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/FAQ" target="_blank">FAQ and self-help</a><br> FAccess Frequently Asked Questions to find a solution to your issue.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="/about/oit/web/request.html" target="_blank">Web Services Development Request</a><br> Contact Web &amp; Portal Services to request a development project.
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>&nbsp;</div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/support/index.pcf">©</a>
      </div>
   </body>
</html>