<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sitemap | Valencia Support | Valencia College</title>
      <meta name="Description" content="Sitemap | Valencia Support">
      <meta name="Keywords" content="college, school, educational, student, support, sitemap">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/support/sitemap.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/support/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/support/">Support</a></li>
               <li>Sitemap</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2>Site Map</h2>
                        
                        <p>Like the index of a book, use our site map to locate any part of our Valencia Web
                           site.
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="#A">A</a>&nbsp; <a href="#B">B</a>&nbsp; <a href="#C">C</a>&nbsp; <a href="#D">D</a>&nbsp; <a href="#E">E</a>&nbsp; <a href="#F">F</a>&nbsp; <a href="#G">G</a>&nbsp; <a href="#H">H</a>&nbsp;&nbsp; <a href="#I">I</a>&nbsp; J&nbsp; K&nbsp; <a href="#L">L</a>&nbsp; M&nbsp; N&nbsp; <a href="#O">O</a>&nbsp; P&nbsp; Q&nbsp; <a href="#R">R</a>&nbsp; <a href="#S">S</a> &nbsp;<a href="#T">T</a>&nbsp; <a href="#U">U</a>&nbsp; <a href="#V">V</a>&nbsp; <a href="#W">W</a>&nbsp; X&nbsp; Y&nbsp; Z
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../index.html">Valencia College - Orlando, Florida</a></p>
                        <a name="A" id="A"></a>
                        
                        <ul>
                           
                           <li><a href="../about-valencia/index.html"><strong>About Valencia </strong></a></li>
                           
                           <li><strong><a href="../admissions-records/index.html">Admissions</a></strong></li>
                           
                           <ul>
                              
                              <li><a href="../admissions-records/forms.html">Applications &amp; Forms</a></li>
                              
                              <li><a href="../../honors/index.html">Honors Program</a></li>
                              
                              <li><a href="howto/index.html">How To/Tutorials</a></li>
                              
                              <li><a href="../../international/index.html">International Students</a></li>
                              
                              <li><a href="../../admissions/orientation/index.html">New Student Orientation</a></li>
                              
                              <li><a href="../business-office/tuition-fees.html">Tuition</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit Valencia</a></li>
                              
                           </ul>
                           
                           <li><a href="../../helpas/index.html"><strong>Advisors - Career Program </strong></a></li>
                           
                           <li><strong><a href="../../assessments/index.html">Assessment Information</a></strong></li>
                           
                           <li>
                              <a href="https://atlas.valenciacollege.edu/"><strong>Atlas Web Portal</strong></a>
                              
                              <ul>
                                 
                                 <li><a href="../admissions-records/registration-details/atlas_access.html">Atlas Access for Registration</a></li>
                                 
                              </ul>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="../student-services/atlas-access-labs.html">Atlas Access Lab Locations</a> 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <a name="B" id="B"></a><a href="../../students/locations-store/index.html"><strong>Bookstore</strong></a>
                              
                           </li>
                           
                           <li><a href="../business-office/index.html"><strong>Business Office</strong></a></li>
                           
                           <li>
                              <a name="C" id="C"></a><strong><a href="../../calendar/index.html">Calendars</a></strong>
                              
                           </li>
                           
                           <li><strong><a href="../../catalog/index.html">Catalog</a></strong></li>
                           
                           <li><a href="../studentdev/clubs2.html"><strong>Clubs &amp; Organizations</strong></a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/news/"><strong>College News &amp; Publications</strong></a></li>
                           
                           <ul>
                              
                              <li><a href="../security/index.html">Campus Crime Statistics </a></li>
                              
                              <li><a href="../about-valencia/index.html">About Valencia </a></li>
                              
                              <li><a href="http://thegrove.valenciacollege.edu" target="_blank" title="The Grove">The Grove</a></li>
                              
                              <li><a href="http://valenciavoice.com/" target="_blank"> Valencia Voice</a></li>
                              
                           </ul>
                           
                           <li>
                              <a href="../../students/courses.html"><strong>Courses Offered</strong></a>
                              
                              <ul>
                                 
                                 <li><a href="http://net5.valenciacollege.edu/schedule/">Credit Class Search</a></li>
                                 
                                 <li><a href="../public-safety/criminal-justice-institute/index.html">Criminal Justice Institute</a></li>
                                 
                                 <li><a href="../../faculty/educator-preparation-institute/index.html">Educator Preparation Institute (EPI) Alternative Certification Program</a></li>
                                 
                                 <li>
                                    <a href="../../academics/programs/index.html">Degree &amp; Career Programs</a>
                                    
                                    <ul>
                                       
                                       <li><strong><a href="../../aadegrees/index.html">University Parallel Programs</a></strong></li>
                                       
                                       <li><strong><a href="../../academics/asdegrees/index.html">Career Programs</a></strong></li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li><a href="http://www.valenciaenterprises.org/">Valencia Enterprises - Continuing Education, Training and Employee Development</a></li>
                                 
                                 <li><a href="../../west/health/cehealth/index.html">Continuing Education for Health Professions</a></li>
                                 
                              </ul>
                              
                           </li>
                           <a name="D" id="D"></a>
                           
                           <li><a href="../../departments/index.html"><strong>Departments</strong></a></li>
                           
                           <li>
                              <a name="E" id="E"></a><a href="../../east/index.html">East Campus</a>
                              
                              <ul>
                                 
                                 <li><a href="../../departments/index.html#east">Departments</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <ul>
                              
                              <li><a href="../../map/east.html">Map &amp; Address</a></li>
                              
                              <li><a href="../../east/locations-president/index.html" name="E" id="E">Campus President Office</a></li>
                              
                              <li><a href="../student-services/index.html">Student Services Office</a></li>
                              
                           </ul>
                           
                           <li><a href="../hr/index.html"><strong>Employment Opportunities</strong></a></li>
                           
                           <li>
                              <a name="F" id="F"></a><a href="http://faculty.valenciacollege.edu/"><strong>Faculty Web Sites</strong></a>
                              
                           </li>
                           
                           <ul>
                              
                              <li><a href="../../faculty/development/programs/destination/index.html" name="F" id="F">Destination</a></li>
                              
                              <li><a href="../../competencies/index.html">Student Core Competencies </a></li>
                              
                              <li><a href="../../faculty/association/index.html"> Faculty Association</a></li>
                              
                              <li><a href="../../faculty/development/index.html">Faculty &amp; Staff Development</a></li>
                              
                           </ul>
                           
                           <li><a href="../../finaid/index.html"><strong>Financial Aid</strong></a></li>
                           
                           <li>
                              <a href="../business-office/index.html"><strong>Finance Services</strong></a> (Business Office) 
                           </li>
                           
                           <li><a href="http://www.valencia.org/"><strong>Foundation</strong></a></li>
                           
                           <li>
                              <a name="G" id="G"></a><a href="../../students/graduation/index.html"><strong>Graduation</strong></a>
                              
                           </li>
                           
                           <li>
                              <a name="H" id="H"></a><a href="index.html"><strong>Help</strong></a>
                              
                           </li>
                           
                           <li>
                              <strong>High School Connection</strong>
                              
                              <ul>
                                 
                                 <li><a href="../../students/bridges-to-success/index.html">Bridges to Success</a></li>
                                 
                                 <li><a href="../../admissions/dual-enrollment/index.html">Dual &amp; Early Enrollment</a></li>
                                 
                                 <li><a href="../career-pathways/techprepoutcome.html">Tech Prep</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <a name="I" id="I"></a><a href="../../internship/index.html"><strong>Internship &amp; Workforce Services Office </strong></a>
                              
                           </li>
                           <a name="L" id="L"> </a>
                           
                           <li> <strong><a href="../oit/learning-technology-services/faculty-resources/index.html">Learning Technology Services (LTS)</a></strong>
                              
                              <ul>
                                 
                                 <li><a href="../oit/learning-technology-services/faculty-resources/blackboard/index.html">Blackboard</a></li>
                                 
                                 <li><a href="https://online.valenciacollege.edu/">Online Courses</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><a href="../../library/index.html"><strong>Libraries</strong></a></li>
                           <a name="M" id="M"> </a>
                           
                           <li><a href="../../map/index.html"><strong>Maps &amp; Addresses</strong></a></li>
                           
                           <ul>
                              
                              <li><a href="../../map/public-safety.html">CRIMINAL JUSTICE</a></li>
                              
                              <li><a href="../../map/east.html">EAST CAMPUS</a></li>
                              
                              <li><a href="../../map/osceola.html">OSCEOLA CAMPUS</a></li>
                              
                              
                              <li><a href="../../map/west.html">WEST CAMPUS</a></li>
                              
                              <li><a href="../../map/winter-park.html">WINTER PARK CAMPUS</a></li>
                              
                           </ul>
                           <a name="O" id="O"></a>
                           
                           <li><a href="../../osceola/index.html">Osceola Campus</a></li>
                           
                           <ul>
                              
                              <li><a href="../../departments/index.html#osceola">Departments</a></li>
                              
                              <li><a href="../../osceola/learningcenter/index.html">Learning Center</a></li>
                              
                              <li><a href="../../library/index.html">Library</a></li>
                              
                              <li><a href="../../map/osceola.html">Map &amp; Address</a></li>
                              
                              <li><a href="../../osceola/mainlab/index.html">Osceola Computer/Writing Labs</a></li>
                              
                              <li><a href="../../osceola/locations-president/index.html">Campus President Office</a></li>
                              
                              <li><a href="../../osceola/studentservices/index.html">Student Services</a></li>
                              
                           </ul>
                           <a name="R" id="R"></a>
                           
                           <li><a href="../admissions-records/index.html"><strong>Records / Transcripts</strong></a></li>
                           
                           <li><a href="../student-services/index.html"><strong>Student Services</strong></a></li>
                           
                           <li>
                              <a href="http://preview.valenciacollege.edu/visit/"></a><a name="T" id="T"></a><a href="../contact/directory.html"><strong>Telephone Directories</strong></a>
                              
                           </li>
                           
                           <li>
                              <a name="U" id="U"></a><a href="../../students/graduation/TransferGuarantees.html"><strong>University Connection</strong></a>
                              
                           </li>
                           
                           <ul>
                              
                              <li><a href="../../academics/asdegrees/transferagreements.html" name="U" id="U">Articulation Agreements</a></li>
                              
                           </ul>
                           
                           <li>
                              <a name="V" id="V"></a><strong><a href="../../veterans-affairs/index.html">Veteran Affairs</a></strong>
                              
                           </li>
                           
                           <li>
                              <a name="W" id="W"></a> <a href="../../west/index.html#west">West Campus</a>
                              
                           </li>
                           
                           <ul>
                              
                              <li><a href="../student-services/index.html" name="W" id="W">Advising &amp; Counseling Center</a></li>
                              
                              <li><a href="../../students/career-center/index.html">Career Center</a></li>
                              
                              <li><a href="../../library/index.html">Library</a></li>
                              
                              <li>
                                 <a href="../../departments/index.html#west">Departments</a> 
                              </li>
                              
                              <li><a href="../../map/west.html">Map &amp; Address</a></li>
                              
                              <li><a href="../../west/locations-president/index.html">Campus President Office</a></li>
                              
                           </ul>
                           
                           <li>
                              <a href="../../wp/index.html">Winter Park Campus</a>
                              
                              <ul>
                                 
                                 <li><a href="../../departments/index.html#wp">Departments</a></li>
                                 
                                 <li><a href="../../library/index.html">Library</a></li>
                                 
                                 <li><a href="../../map/winter-park.html">Map &amp; Address</a></li>
                                 
                                 <li><a href="../../east/locations-president/index.html">Campus President Office</a></li>
                                 
                                 <li><a href="../../wp/services.html">Student Services</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/support/sitemap.pcf">©</a>
      </div>
   </body>
</html>