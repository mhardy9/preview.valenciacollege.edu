<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/support/howto/atlas-advising-chat-setup.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/support/howto/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/support/">Support</a></li>
               <li><a href="/about/support/howto/">Howto</a></li>
               <li>Valencia Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        <article>
                           <article>
                              
                              <h2>How to message Advising Chat in your Atlas email account </h2>
                              
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>1.</strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Log into your Atlas account and choose "Office 365 " from the list of options available
                                                through the "O365/Email" button at the top right corner of the page. (Hover your cursor
                                                over the button to access the options.) </strong></div>
                                       </div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>2.</strong></div>
                                       
                                       <div data-old-tag="td"><strong>Click on the People icon or use the dashboard tile to access the People icon. This
                                             is how you access the Atlas email user directory. </strong></div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>3.</strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong>Type "Advising" into the search bar on the left side of your People page.</strong></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>4.</strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong> The AdvisingChat contact card should appear as an automatic result under the search
                                                results list.</strong></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>5.</strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td"><strong>Click on the AdvisingChat icon to bring up the contact card.</strong></div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>6.</strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong>When the service is available, AdvisingChat will have a green status bar next to the
                                                picture and it will say "Available" under the name. Simply click on the "Send IM "
                                                link to send a message to an advisor via instant messaging service. </strong></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </article>     
                           
                           
                        </article>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <h3>Need Assistance?</h3>
                           
                           <p>Contact the Atlas Help Desk at 407-582-5444.</p>
                           
                           
                           <p><strong>Chat Hours </strong></p>
                           
                           <p>Advising Chat service is only available during the following hours:<br>
                              <br>
                              <strong>4 p.m. - 6 p.m.</strong> Mondays<br>
                              <br>
                              <strong> 12 p.m. - 2 p.m. </strong>Wednesdays<br>
                              <br>
                              <strong>10 a.m. - 12 p.m. </strong>Thursdays
                           </p>
                           
                        </div>
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/support/howto/atlas-advising-chat-setup.pcf">©</a>
      </div>
   </body>
</html>