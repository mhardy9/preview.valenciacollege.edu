<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student How-To | Valencia Support | Valencia College</title>
      <meta name="Description" content="Student How-To | Valencia Support">
      <meta name="Keywords" content="college, school, educational, student, support, how">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/about/support/howto/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/about/support/howto/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Support</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/about/">About</a></li>
               <li><a href="/about/support/">Support</a></li>
               <li>Howto</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        					<a href="../index.html"></a>
                        
                        					
                        <h2>Student How To</h2>
                        
                        					
                        <div data-old-tag="table">
                           						
                           <div data-old-tag="tbody">
                              							
                              <div data-old-tag="tr">
                                 								
                                 <div data-old-tag="td">
                                    									
                                    <h3>Atlas Help</h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          
                                          											
                                          <li><a href="documents/2015-Atlas-Setup.mp4" target="_blank">Set up an Atlas Account</a></li>
                                          											
                                          <li><a href="../../student-services/atlas-access-labs.php">Atlas Access Lab Locations</a></li>
                                          											
                                          <li><a href="http://preview.valenciacollege.edu/atlas/documents/AtlasQuickRef.pdf">Atlas Quick Reference Guide</a></li>
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    									
                                    <h3>Atlas Overview</h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          											
                                          <li><a href="documents/Update-Address.mp4" target="_blank">How to Update Your Address</a></li>
                                          											
                                          <li><a href="documents/2015-Password-Reset.mp4" target="_blank">How to Reset Your Password</a></li>
                                          											
                                          <li><a href="https://www.youtube.com/embed/S3BjJdOVQa8" target="_blank">Understanding the My Atlas and Courses Pages</a></li>
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    
                                    									
                                    <h3>Student Records and General Information</h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          											
                                          <li><a href="../../../academics/answer-center.php">Answer Center</a></li>
                                          											
                                          <li><a href="../../admissions-records/enrollment_verification.php">Enrollment Verification</a></li>
                                          											
                                          <li><a href="../../admissions-records/degree_verification.php">Degree Verification </a></li>
                                          											
                                          <li><a href="https://www.youtube.com/embed/-shDJK2Ox_4" target="_blank">How to Select a Meta-Major </a></li>                      
                                          											
                                          <li><a href="https://www.youtube.com/embed/5_nbip6nGIk" target="_blank">How to Use the College Catalog</a></li>                      
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    									
                                    <h3>LifeMap Tools</h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          
                                          											
                                          <li>
                                             												<a href="http://v2.careersearch.net/help/tutorials.tt2">How do I use My Job Prospects?</a> 
                                          </li>
                                          
                                          											
                                          <li><a href="https://www.youtube.com/embed/C6Tm9CDuO2E" target="_blank">How to Read the MEP Checklist</a></li>
                                          											
                                          <li><a href="https://www.youtube.com/embed/n_y4HN7GeJ4" target="_blank">MEP What-If</a></li>
                                          
                                          											
                                          <li><a href="https://www.youtube.com/embed/163a99LCZ9U" target="_blank">How to Use the MEP GPA Calculators</a></li>   
                                          											
                                          <li><a href="https://www.youtube.com/embed/k4tjwGcy2-M" target="_blank">"Look Ahead" Feature</a></li>
                                          											
                                          <li><a href="https://www.youtube.com/embed/eYC64HNq9kQ" target="_blank">Saving an MEP Project</a></li>                   
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    									
                                    <h3>Valencia Alerts and Reminders<a name="alerts" id="alerts"></a>
                                       									
                                    </h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          											
                                          <li><a href="documents/valencia-alerts-how-to-opt-in-for-outreach-messages.pdf" target="_blank">How to Opt-in to Receive Outreach Messages</a></li>
                                          											
                                          <li> <a href="documents/valencia-alerts-how-to-manage-your-alerts.pdf" target="_blank">How to Manage Your Outreach Messages</a>
                                             											
                                          </li>
                                          											
                                          <li><a href="documents/valencia-alerts-how-to-opt-in-for-messages-when-registering-for-classes.pdf" target="_blank">Opt-in to Outreach Messages When Registering for Classes</a></li>
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    
                                    								
                                 </div>
                                 
                                 								
                                 <div data-old-tag="td">
                                    
                                    
                                    
                                    
                                    
                                    									
                                    <h3>Student Registration</h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          											
                                          <li><a href="documents/Student-Detail-Schedule.mp4" target="_blank">Student Detail Schedule</a></li>
                                          											
                                          <li><a href="documents/Drop-a-Class.mp4" target="_blank">Drop a Class</a></li>
                                          											
                                          <li><a href="documents/2015-Trancript-Request.mp4" target="_blank">Request Transcripts</a></li>
                                          											
                                          <li><a href="registration-planner-tutorial.html" target="_blank">Registration Planner</a></li>
                                          
                                          
                                          											
                                          <li><a href="documents/Valencia_College_Registration_Planner.pdf" target="_blank">How to Use the Registration Planner</a></li>
                                          											
                                          <li><a href="documents/Search-By-Instructor.pdf" target="_blank">Registration Planner: Search by Instructor</a></li>       
                                          											
                                          <li><a href="documents/Register-For-Classes.mp4" target="_blank">Register / Add a Class</a></li>
                                          											
                                          <li><a href="documents/2015-Final-Grades.mp4" target="_blank">Look Up Your Final Grades</a></li>
                                          											
                                          <li><a href="documents/CheckFinancialAidStatus.swf">How do I check my Financial Aid Status?</a></li>
                                          											
                                          <li><a href="documents/2015-Time-Ticket.mp4" target="_blank">When Can I Register? (Time Ticket)</a></li>
                                          											
                                          <li><a href="documents/Online-Payment.mp4" target="_blank">Online Payment Using Checking Account</a></li>
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    									
                                    <h3>Educational Enhancements</h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          											
                                          <li><a href="../../../academics/learning-in-community/index.php#">Learning in Community (LinC)</a></li>
                                          											
                                          <li><a href="documents/Valencia_Office-ProPlus-Instructions.pdf">How do I download Microsoft Student Advantage Office ProPlus?</a></li>
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    									
                                    <h3>Applying for Graduation</h3>
                                    									
                                    <div>
                                       										
                                       <ul>
                                          											
                                          <li><a href="documents/HowtoApplyforGraduation.pdf"> How to apply for Graduation</a></li>
                                          											
                                          <li><a href="documents/HowtoRequestaDegreeAudit.pdf">How to request a Degree Audit</a></li>
                                          
                                          											
                                          <li><a href="https://www.youtube.com/embed/NR7TuHc3-Ug" target="_blank">Degree Audit Tutorial</a></li>
                                          										
                                       </ul>
                                       									
                                    </div>
                                    
                                    								
                                 </div>
                                 							
                              </div>
                              						
                           </div>
                           					
                        </div>  
                        
                        
                        					
                        <p><a href="atlas-advising-chat-setup.html">Add Advising Chat to your Atlas Email Account</a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/about/support/howto/index.pcf">©</a>
      </div>
   </body>
</html>