<?php
$urls = array();  

$DomDocument = new DOMDocument();
$DomDocument->preserveWhiteSpace = false;
$DomDocument->load('sitemap.xml');
$DomNodeList = $DomDocument->getElementsByTagName('loc');

foreach($DomNodeList as $url) if (strpos($url->nodeValue, 'index.php') == true && strpos($url->nodeValue, '/blog/') == false && strpos($url->nodeValue, '/profile/') == false && strpos($url->nodeValue, '/_resources/') == false && strpos($url->nodeValue, '/blog/') == false && strpos($url->nodeValue, '/_staging/') == false && strpos($url->nodeValue, '/secure/') == false) 
{
    $urls[] = $url->nodeValue;
}

//display it
   $map = '';
   $section = '';

sort($urls);   
foreach ($urls as $url)

{
	if ( $url ) {
		//echo $url."<br>";
	if (strpos($url, 'valenciacollege.edu/about/') && $section !== '<h3>About</h3>')
	{
	$section = '<h3>About</h3>';	
	$map .= '
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_staging/_resources/img/prestige/bg-aboutus.jpg" alt="About Valencia">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>About</h3>
					</div>
					<div class="list-group">';	
	}
	if (strpos($url, 'valenciacollege.edu/academics/') && $section !== 'Academics')
	{
	$section = 'Academics';	
	$map .= '</div>
				</span>
					</div>			
						</div>
		
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_resources/img/header_bg_1_1600x800_osceola-2.jpg" alt="Academics">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>Academics</h3></div><div class="list-group">';	
	}
	if (strpos($url, 'valenciacollege.edu/admissions/') && $section !== 'Admissions')
	{
	$section = 'Admissions';	
	$map .= '</div>
				</span>
					</div>			
						</div>
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_staging/_resources/img/prestige/bg-academics-osceola.jpg" alt="Admissions">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>Admissions</h3>
		</div>
		<div class="list-group">';	
	}
	if (strpos($url, 'valenciacollege.edu/employees/') && $section !== 'Employees')
	{
	$section = 'Employees';	
	$map .= '</div>
				</span>
					</div>			
						</div>
		
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_resources/img/header_bg_1_1600x800_osceola-2.jpg" alt="Employees">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>Employees</h3></div><div class="list-group">';	
	}
	if (strpos($url, 'valenciacollege.edu/faculty/') && $section !== 'Faculty')
	{
	$section = 'Faculty';	
	$map .= '</div>
				</span>
					</div>			
						</div>
		
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_resources/img/header_bg_1_1600x800_osceola-2.jpg" alt="Faculty">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>Faculty</h3></div><div class="list-group">';	
	}
	if (strpos($url, 'valenciacollege.edu/foundation/') && $section !== 'Foundation')
	{
	$section = 'Foundation';	
	$map .= '</div>
				</span>
					</div>			
						</div>
		
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_resources/img/header_bg_1_1600x800_osceola-2.jpg" alt="Foundation">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>Foundation</h3></div><div class="list-group">';	
	}
	if (strpos($url, 'valenciacollege.edu/locations/') && $section !== 'Locations')
	{
	$section = 'Locations';	
	$map .= '</div>
				</span>
					</div>			
						</div>
		
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_resources/img/header_bg_1_1600x800_osceola-2.jpg" alt="Locations">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>Locations</h3></div><div class="list-group">';	
	}
		if (strpos($url, 'valenciacollege.edu/students/') && $section !== 'Students')
	{
	$section = 'Students';	
	$map .= '</div>
				</span>
					</div>			
						</div>
		
		<div class="card mb-5">
			<img class="card-img-top img-fluid" src="/_resources/img/header_bg_1_1600x800_osceola-2.jpg" alt="Students">
			<div class="card-body ">
				<span class="card-text">
					<div class="">
					<h3>Students</h3></div><div class="list-group">';	
	}
   $contents = file_get_contents($url);

	
	  // find the first h1 tag
   $heading1 = array();
   preg_match('/(?<=\<[Hh]1\>)(.*?)(?=\<\/[Hh]1\>)/U', $contents, $heading1);
   $heading1 = strip_tags($heading1[0]);
   // find the first h2 tag
   $heading2 = array();
   preg_match('/(?<=\<[Hh]2\>)(.*?)(?=\<\/[Hh]2\>)/U', $contents, $heading2);
   $heading2 = strip_tags($heading2[0]);
 
    if ( strlen($heading1) > 0 ) {
     $map .= '<a  class="list-group-item list-group-item-action" href="'.$url.'" title="'.trim($heading1." - ".$heading2).'">'.trim($heading1." - ".$heading2).'</a>'."\n";
    } 
	else{
     $map .= '<div  class="list-group-item list-group-item-danger">Page Not Found - '.$url.'</div>'."\n";
    };
 }
}
$map .="</span>
			</div>			
		</div>";
	 
// write output to a file
$fp = fopen('sitemap.html', "w+");
fwrite($fp,$map);
fclose($fp);
 
// print output
echo $map;

?>