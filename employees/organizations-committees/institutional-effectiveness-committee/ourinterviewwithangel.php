<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Effectiveness Committee | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-effectiveness-committee/ourinterviewwithangel.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-effectiveness-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Effectiveness Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/institutional-effectiveness-committee/">Institutional Effectiveness Committee</a></li>
               <li>Institutional Effectiveness Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Our Interview with Angel Resende </h2>
                        
                        <p><strong>IEC</strong>: "
                           
                           
                           What different ways do you use e-mail with your professors? Can you describe specific
                           examples?"
                        </p>
                        
                        <p><strong>Angel</strong>: "One major way that I use e-mail with my professors is sending out reminder e-mails
                           to them a few days before a test, saying, 'Please remember to put my test in the Testing
                           Center. I have scheduled to take my exam on this date and at this time. Is this ok
                           with you?' Fortunately, all of the professors I had were flexible with me scheduling
                           these specific test times. Most recently, I used e-mail to communicate to my online
                           math professor that I needed/have Extended Time for exams, according to my Notice
                           to Instructor; he responded, “How many hours do you need for a 45 min. test?” I replied
                           to him: “For Extended Time, I get Time and a half, so I should have 112 minutes for
                           a 45 min test.” My professor was totally receptive to my accommodation, and to be
                           completely honest, that was my very first time that I stated an accommodation of mine
                           through e-mail. It probably will not be the last time either! 
                        </p>
                        
                        <p><strong>IEC</strong>: "
                           
                           
                           Do you find using e-mail an effective tool to communicate with instructors? Why?"
                        </p>
                        
                        <p><strong>Angel</strong>: 
                           
                           
                           "Yes, I do feel e-mail is such an effective tool to communicate with professors especially
                           when professors see eye to eye on this education tool. E-mail is an excellent reminder
                           tool also, for students and professors are human after all and we sometimes do forget
                           what we have discussed. Therefore, either party can shoot out an e-mail about the
                           previous discussion and the other party can respond by, 'Oh, ok. Thanks for reminding
                           me!'"
                        </p>
                        
                        <p><strong>IEC</strong>: "What other technology tools do you use to communicate with your professors?"
                        </p>
                        
                        <p><strong>Angel</strong>: "WebCt’s Discussion Boards."
                        </p>
                        
                        <p><strong>IEC</strong>: "What role do you see technology having in your education in the future?"
                        </p>
                        
                        <p><strong>Angel</strong>: "I see technology as playing an extremely important role in my educational future,
                           for technology is forever changing our resources to gain education."
                        </p>
                        
                        
                        <h2>&nbsp;</h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-effectiveness-committee/ourinterviewwithangel.pcf">©</a>
      </div>
   </body>
</html>