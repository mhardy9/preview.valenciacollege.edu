<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Effectiveness Committee | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-effectiveness-committee/multimedia.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-effectiveness-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Effectiveness Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/institutional-effectiveness-committee/">Institutional Effectiveness Committee</a></li>
               <li>Institutional Effectiveness Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Documenting Faculty-Student Engagement</h2>
                        
                        <p>Here you can view videos and photographs documenting faculty-student engagement from
                           across the college.
                        </p>
                        
                        
                        <h2>Practicing What We Preach</h2>
                        
                        <p>When the IEC decided to create a web site, we knew exactly what to do: Get some students
                           to help us! To that end, we contacted Daissy and Reina (of Columbia and Venezuela,
                           respectively), two award-winning students enrolled in the college’s Graphic Design
                           program. They were nice enough to design  the committee’s logo and devise the organization
                           of the overall site.
                        </p>
                        
                        
                        
                        
                        <div>
                           
                           <h2>Catapult Day</h2>
                           
                           <p>Each semester, Dr. George Brooks, East Campus humanities professor, celebrates medieval
                              culture and history with Catapult Day. His students prepare medieval-era food and
                              crafts, which they bring to campus for all to enjoy while watching the event's climactic
                              showpiece: the launching of an authentic medieval trebuchet. Hand-crafted by Dr. Brooks
                              and Buzz Dawson, the Orlando Science Center's Director of New Exhibits, this intimidating
                              example of old-school shock-and-awe is used to launch a variety of ammunition--heads
                              of lettuce, cantaloupes, the errant watermelon--at a distant target: a castle wall
                              erected just for the occasion. 
                           </p>
                           
                           <p>Our video is an imaginative look at the preparations for and eventual onslaught of
                              a medieval siege: hide your children, and give a "Huzzah!" for Dr. Brooks and his
                              fearless knights!
                           </p>
                           
                           
                           
                           <p>View  <a href="catapult_pictures.html">photographs from Catapult Day</a>. 
                           </p>
                           
                        </div>
                        
                        
                        <div>
                           <br><br><br>
                           <strong>Click the Image Below to Watch Catapult Day, Fall, 2007 Video </strong><br>
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        <div>    
                           
                           <h2>Feedback Woes</h2>
                           
                           
                           <p>Assessment: every teacher's favorite past time! Of course, as a learning-centered
                              member of VCC, you know that already. However, what we sometimes forget--mired as
                              we frequently can be in piles of tests, projects, and essays to grade--is that assessing
                              our students' performance promptly is equally important. 
                           </p>
                           
                           <p>Not convinced? Then this short film, the result of a collaboration between the IEC
                              and several VCC students, will hopefully persuade you. Watch as one lucky math student
                              revels in intellectual glory after getting some invaluable feedback from his instructor
                              while his less fortunate friend is doomed to the purgatory of feedbackless confusion.
                              
                           </p>
                           
                           <p>View behind-the-scenes photographs <a href="feedback_woes.html">here</a>.  
                           </p>
                           
                        </div>
                        
                        
                        <div>
                           <br><br><br>                      
                           <strong>Click  Below to Watch  Video </strong><br>
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        <div>  
                           
                           
                           <h2>Arty Party</h2>
                           
                           <p>Hey, Moms: the kids are alright. They just need to dance sometimes! Fortunately, the
                              faculty at Valencia understand this. Each Spring, therefore, the Humanities Department
                              on East Campus helps us get in touch with our inner-Bacchus with their annual Arty
                              Party. The ingredients? Pizza, high-fructose corn syrup, art, and, this year, some
                              of that Rock n’ Roll. 
                           </p>
                           
                           <p>In the first clip below, watch local ear-drum splitters Open Windows (whose lead guitarist,
                              Mike Wheaton, is a former Valencia student and  English consultant in our Academic
                              Support Center) push those amps past 11—leading to some youthful abandon on the dance
                              floor. The second clip includes an entire song by Open Windows: if we weren’t so old,
                              we’d actually know the name of it.
                           </p>
                           
                        </div>
                        
                        <div>
                           <br><br><br>
                           <strong>Click  Below to Watch  Video </strong><br> 
                           
                           
                           
                           
                           
                           
                           <br>
                           <strong>Click  Below to Watch  Video </strong>
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        <div>            
                           
                           <h2>Choosing a Career</h2>
                           
                           <p>It's a conundrum nearly every student at VCC faces at some point: Choosing a career.
                              We know that the sooner students can figure out "what they wanna be when they grow
                              up," the better able they'll be to navigate their educational and, ultimately, professional
                              journey. 
                           </p>
                           
                           <p>To help us really appreciate the anxiety that this looming decision can create, we
                              solicited a student to help us out. Neil Bernard, a Film Production major, felt he
                              could communicate what it's like to have to pick just one  career out of so many possible
                              choices. Neil wrote, directed, edited, and starred in the following video about having
                              to choose a career and how visiting a career counseler (or even speaking with a faculty
                              member) can often help. The IEC was happy to provide the equipment and some manual
                              labor. We especially liked meeting Neil's parents (we now know where he gets those
                              creative genes!). 
                           </p>
                           
                           <p>A couple of behind-the-scenes pictues can be found <a href="behind_the_scenes.html">here</a>.
                           </p>
                           
                           <p>Wanna learn more about working with a career counselor? Click <a href="../../../students/career-center/index.html">here</a> and <a href="../../../helpas/index.html">here</a>. You won't regret it.
                           </p>
                           
                        </div>
                        
                        <div>
                           <br><br><br>
                           <strong>Click  Below to Watch  Video </strong><br><br> 
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        <div> 
                           
                           <h3><strong>Keystrokes</strong></h3>
                           
                           <p>Looking for trouble? Well, you found her. Meet Angel Resende, known among those brave
                              souls lucky enough to be on her good side as "trouble" (you can ask her why: we don't
                              want to make her mad!). Actually, Angel is as sweet as her real name suggests. She's
                              also quite the technophile, especially when it comes to school. Angel is an enthusiastic
                              user of learning technology, so we knew we could count on her to give us some insights
                              into how using e-mail in her classes has helped her learn. 
                           </p>
                           
                           
                           <p>Read our interview with Angel <a href="OurInterviewwithAngel.html">here</a> and see a couple of other pictures <a href="angel.html">here</a>. 
                           </p>
                           
                           
                           <p><a href="#top">TOP</a></p>
                           
                        </div>            
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-effectiveness-committee/multimedia.pcf">©</a>
      </div>
   </body>
</html>