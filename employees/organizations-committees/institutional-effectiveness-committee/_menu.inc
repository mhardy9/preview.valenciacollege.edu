<ul>
<li><a href="index.php">Institutional Effectiveness Committee</a></li>
<li class="submenu">
<a class="show-submenu" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
<li><a href="multimedia.php">Multimedia</a></li>
<li><a href="tips.php">Tips &amp; Resources</a></li>
<li><a href="experience.php">Share Your Experience</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/iec/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
</li>
</ul>