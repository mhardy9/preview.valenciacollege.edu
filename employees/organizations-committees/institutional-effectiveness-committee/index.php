<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Effectiveness Committee | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-effectiveness-committee/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-effectiveness-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Effectiveness Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li>Institutional Effectiveness Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Our Mission</h2>
                        
                        <p>The IEC's present objective is to promote, educate about, and ultimately improve faculty-student
                           engagement at the college. More specifically, the committee focuses on the following
                           particular types of engagement (derived from the Community College Survey of Student
                           Engagement):
                        </p>
                        
                        <ul>
                           
                           <li>Used email to communicate with an instructor</li>
                           
                           <li>Discussed grades or assignments with an instructor</li>
                           
                           <li>Talked about career plans with an instructor or advisor</li>
                           
                           <li>Discussed ideas from your readings or classes with instructors outside of class</li>
                           
                           <li>Received prompt feedback (written or oral) from instructors on your performance</li>
                           
                           <li>Worked with instructors on activities other than coursework</li>
                           
                        </ul>        
                        
                        
                        <h2>History of the IEC</h2>
                        
                        <p>Below, you can review documents that detail the history and development of the IEC
                           and its ongoing work to improve student learning:
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="documents/InstitutionalEffectivenessCommitteeCharge.pdf">Institutional Effectiveness Committee Charge</a> 
                           </li>
                           
                           <li><a href="documents/ReportoftheInstitutionalEffectivenessTaskforce.pdf">Report of the Institutional Effectiveness Taskforce</a></li>
                           
                           <li><a href="documents/Addendum.pdf">Addendum</a></li>
                           
                           <li><a href="documents/IinstitutionalEffectivenessCommitteeFacultyCouncilRecommendation.pdf">Iinstitutional Effectiveness Committee Faculty Council Recommendation</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-effectiveness-committee/index.pcf">©</a>
      </div>
   </body>
</html>