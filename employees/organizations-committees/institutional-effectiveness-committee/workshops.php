<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Effectiveness Committee | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-effectiveness-committee/workshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-effectiveness-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Effectiveness Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/institutional-effectiveness-committee/">Institutional Effectiveness Committee</a></li>
               <li>Institutional Effectiveness Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Workshops for Faculty and
                           Staff
                        </h2>
                        
                        <p>The workshops described below are offered by Valencia's Professional Development program.
                           We linked specific workshops with relevant IEC principles so that you can explore
                           ways to best improve your interaction with students. 
                        </p>
                        
                        
                        
                        <p><strong><u>IEC
                                 Principle</u></strong><strong>:
                              ï¿½Used E-Mail to Communicate with an Instructorï¿½</strong></p>
                        
                        <p><strong>LCTS3111: Who is
                              the Virtual Student?</strong></p>
                        
                        <p>Based on The Virtual Student (Palloff and Pratt), this online course will engage participants
                           in
                           discovering the characteristics and unique needs of the online learner. Participants
                           will discuss the 
                           relationships, roles, and responsibilities of learners and faculty in the virtual
                           classroom and will begin
                           the exploration of teaching and learning online. In addition, participants will review
                           the resources in
                           place at Valencia to help faculty accommodate the unique needs of virtual students.
                           Note: This workshop is only offered online.
                        </p>
                        
                        <p><strong>LCTS3115: 
                              Designing Online Student Orientation</strong><br>
                           <br>
                           Getting students off on the right foot in an online class can mean the difference
                           between success and 
                           failure. Based on The Virtual Student (Palloff and Pratt), this workshop will engage
                           participants in
                           addressing the orientation needs of the virtual student. Participants will explore
                           the essential elements
                           and develop a plan for an effective online orientation. n addition, participants will
                           review the resources
                           in place at Valencia to help faculty provide a comprehensive student orientation.
                           Note: This course is optional for Digital Professor Certification and is only offered
                           online.
                        </p>
                        
                        <p><strong>LCTS3121: 
                              Introduction to Teaching and Learning Online</strong></p>
                        
                        <p>    This online course will engage participants in an overview of web-enhanced, hybrid
                           and online courses 
                           and explore the fundamentals and key issues of teaching and learning in the online
                           environment.
                        </p>
                        
                        <p><strong>LCTS3125: 
                              Engaging the Online Learner</strong></p>
                        
                        <p>    Engagement is the critical element for student success and retention in online
                           classes. In this course,
                           participants will explore virtual student engagement and develop a variety of resources
                           and activities to
                           connect with the online learner.
                           Note: This course is optional for Digital Professor Certification and is offered only
                           online.
                        </p>
                        
                        <p><strong>LCTS3137: 
                              Facilitating Online Learning</strong></p>
                        
                        <p>    This scenario-based online course is designed for instructors who are currently
                           teaching online or are
                           planning to in the near future. This course will also be of great value to those involved
                           in designing and
                           developing online courses. It focuses on understanding the characteristics and needs
                           of the online
                           student and developing strategies to facilitate effective online discussions, assessment
                           and ultimately
                           the development of an online learning community.
                           Note: This course is optional for Digital Professor Certification.
                        </p>
                        
                        <p><strong>LCTS 3212: 
                              Boot Camp for Online Instruction</strong></p>
                        
                        <p>    This three-day intensive course addresses the pedagogy and technology essentials
                           for teaching
                           online using Valencia’s learning management system (LMS). Participants will develop
                           a framework of
                           online best practice strategies for engaging, instructing, communicating and assessing
                           students in
                           the online environment.
                        </p>
                        
                        <p><strong>LCTS3247: 
                              Maximizing Hybrid Learning</strong></p>
                        
                        <p>    Participants will investigate best practices for transforming a face-to-face class
                           into a hybrid format.
                           Participants will examine available resources and explore classroom management techniques
                           and
                           planning strategies.
                        </p>
                        
                        <p><strong>LTAD1116: 
                              Discipline-specific Topics in Learning Technology</strong></p>
                        
                        <p>    Seminars created in collaboration with deans/departments to focus on discipline
                           content and
                           learning technology. Topics vary from year to year, may be repeated for credit.<br>
                           
                        </p>
                        
                        <p><strong>LTAD2280:
                              Selected Topics for Blackboard WebCT Users</strong></p>
                        
                        <p>    Workshops on special topics concerning blackboard course tools. Faculty and staff
                           who design and
                           facilitate alternative delivery courses will benefit from this course offering.<br>
                           
                        </p>
                        
                        <p><strong>LTAD2910: 
                              Selected Topics in Learning Technology</strong></p>
                        
                        <p>    Seminars on special topics concerning learning technology. Topics include a focus
                           on best<br>
                           practices, individualized sessions for technology application, and resource-sharing
                           sessions for a
                           problem-solving approach of incorporating technology in the classroom. Topics vary
                           from year to
                           year, may be repeated for credit.
                        </p>
                        
                        <p><strong>LTAD3288: 
                              Integrating Web 2.0 Technologies</strong></p>
                        
                        <p>    Explore the world of web 2.0 and how these technologies can help enhance informal
                           learning in
                           your classroom. This workshop will showcase various applications and describe how
                           they can be
                           used to form learning communities through careful navigation, connections, evaluation,
                           integration, problem-solving and communication in the interactive web. Participants
                           will identify
                           tools to use in their discipline area and reflect on how these tools can enable collaborations
                           and
                           engage students.
                        </p>
                        
                        <p><strong>LTAD3240: 
                              Multimedia Tools for Online Courses</strong></p>
                        
                        <p>    This workshop will assist participants in identifying and incorporating multimedia
                           and instructional tools
                           to accommodate students' diverse learning styles. Note: This workshop is optional
                           for the Digital
                           Professor Certification.
                        </p>
                        
                        
                        
                        <p><strong><u>IEC
                                 Principle:</u>ï¿½Discussed Grades or Assignments with an Instructorï¿½</strong></p>
                        
                        <p><strong>LFMP 1116: 
                              Discipline-specific Topics</strong></p>
                        
                        <p>    Seminars created in collaboration with deans/departments to focus on discipline
                           content. Topics
                           vary from year to year, may be repeated for credit.
                        </p>
                        
                        <p>    <strong>LFMP1151: 
                              Virtual Reading Circle</strong></p>
                        
                        <p>    Online reading circle discussion of a selected text on LifeMap. Participants will
                           be expected to
                           engage with the text and with each other, as well as being willing to try new techniques
                           with their
                           students and report back to the discussion about their results. (Variable credits;
                           course may be
                           repeated with new content.)
                        </p>
                        
                        <p><strong>LFMP2141: LifeMap</strong></p>
                        
                        <p>This Essential Competency seminar
                           examines learning opportunities that promote
                           student life skills development while enhancing discipline learning.
                        </p>
                        
                        <p><strong>LFMP2910: 
                              Selected Topics</strong></p>
                        
                        <p>    Seminars on special topics concerning LifeMap. Topics vary from year to year,
                           may be repeated
                           for credit.
                        </p>
                        
                        <p><strong>LFMP3241: Student
                              Retention</strong></p>
                        
                        <p>This seminar examines the theory
                           and research on student retention in  college
                           and explores teaching and advising strategies that will help students persist. Emphasis
                           is on practical
                           applications for community college educators.
                        </p>
                        
                        <p><strong>LFMP3342: Student
                              Responsibility</strong></p>
                        
                        <p>This seminar hosts a dialogue on
                           the meaning of student responsibility. It explores
                           concepts of self-direction and the teaching and advising strategies that help students
                           become more
                           self-directed learners.
                        </p>
                        
                        <p><strong>LFMP3343: At-Risk
                              Students</strong></p>
                        
                        <p>This seminar examines the factors
                           that put students at risk for not succeeding
                           in college. It explores the characteristics of at-risk students and strategies for
                           teaching skills and
                           attitudes that help them meet the challenges of college life.
                        </p>
                        
                        <p>Also, check out the assessment-focused workshops below listed under <strong>"IEC Principle: Received Prompt Feedback (Written or Oral) from Instructors on Your
                              Performance."</strong> 
                        </p>
                        
                        
                        
                        <p><strong><u>IEC
                                 Principle:</u> ï¿½Talked about Career Plans with an Instructor or Advisorï¿½</strong></p>
                        
                        <p>Please see the LifeMap-focused seminars listed above under <strong>"IEC Principle: Discussed grades or assignments with an instructor."</strong> 
                        </p>
                        
                        <p>In addition, explore and familiarize yourself with the "My Career Planner" section
                           in Atlas for additional information on how to best help students should they seek
                           out advice from you on how to choose a career and/or how to structure their college
                           course work to best achieve their career goals. 
                        </p>
                        
                        <p>Having students make an appointment with a counselor, of course, is always a great
                           way for them to obtain invaluable information about their career options. 
                        </p>
                        
                        
                        
                        <p><strong><u>IEC
                                 Principle:</u> ï¿½Discussed Ideas from your Readings or Classes with Instructors
                              Outside of Classï¿½</strong></p>
                        
                        <p><strong>LCTS1111: 
                              Teaching in the Learning College</strong></p>
                        
                        <p>          This scenario-based course is designed for adjunct instructors, new instructors,
                           and instructors with
                           strong content and/or technical expertise but limited teaching experience. This curriculum
                           provides
                           faculty with tools and resources enabling them to become more effective learning-centered
                           instructors.
                           Note: This course is required for Associate Faculty Certification and is only offered
                           in a hybrid format. 
                        </p>
                        
                        <p><strong>LCTS2111: 
                              L-C Teaching Strategie</strong>s
                        </p>
                        
                        <p>          This Essential Competency seminar examines diverse teaching strategies that
                           promote student
                           learning.
                        </p>
                        
                        <p><strong>LCTS2213: 
                              Active Learning Techniques</strong></p>
                        
                        <p>          In this hands-on workshop, participants learn a variety of ways to actively
                           engage students in learning.
                           Faculty members from all disciplines will find this session useful.
                        </p>
                        
                        <p>The workshops listed under <strong>IEC Principle: “Received Prompt Feedback (Written or Oral) from Instructors on Your
                              Performance” </strong>as well as <strong>IEC Principle:”Discussed Grades or Assignments with an Instructor”</strong> would prove beneficial here as well. 
                        </p>
                        
                        
                        
                        <p><strong><u>IEC
                                 Principle: </u>ï¿½Received Prompt Feedback (Written or Oral) from Instructors on
                              Your </strong></p>
                        
                        <p><strong>Performanceï¿½</strong></p>
                        
                        <p><strong>ASMT1116 Discipline-specific Topics</strong>:        
                        </p>
                        
                        <p>Seminars created in collaboration with deans/departments to focus on discipline content.
                           Topics vary
                           from year to year, may be repeated for credit.
                        </p>
                        
                        <p><strong>ASMT1151: 
                              Virtual Reading Circle</strong></p>
                        
                        <p>          Online reading circle discussion of a selected text on assessment issues
                           in college. Participants will be
                           expected to engage with the text and with each other, as well as being willing to
                           try new techniques with
                           their students and report back to the discussion about their results. (Variable credits;
                           course may be 
                           repeated with new content.)
                        </p>
                        
                        <p><strong>ASMT2121:
                              Assessment as a Tool for Learning</strong></p>
                        
                        <p>This Essential Competency seminar
                           examines strategies that promote student
                           growth through consistent, timely, formative measures, and promote studentsï¿½
                           ability to self-assess. Also examined will be assessment practices that invite student
                           feedback on the teaching and learning process, as well
                           as on student achievement.
                        </p>
                        
                        <p><strong>ASMT2224: Course
                              Design: From Learning Outcomes to Assessment</strong></p>
                        
                        <p>Course design can be one of the
                           most interesting and challenging areas of a faculty
                           memberï¿½s work. Drawing from research in the field, this session focuses on understanding
                           the
                           characteristics of learning-centered course construction
                           from learning outcomes to assessment.
                        </p>
                        
                        <p><strong>ASMT2226: Online
                              Course Summative Assessment</strong></p>
                        
                        <p>ï¿½This course is designed to
                           introduce the topic of assessment in online courses to those who are thinking
                           about teaching online or who are in the initial stages of teaching online. It is designed
                           to acquaint participants withï¿½ best practices in online assessment,
                           concentrating on summativeï¿½ assessment, and to have
                           participants apply what they have learned in anï¿½ online environment. Throughout
                           the course, participants will play the dual roles of student and designer,
                           giving them experience in both sides of online learning and will have the
                           benefit of collegial conversations about assessment.
                        </p>
                        
                        <p><strong>ASMT2227: Understanding and Designing Rubrics </strong></p>
                        
                        <p>In this online, interactive course, participants will learn the elements of rubric
                           construction and examine
                           a variety of models to use in creating their own discipline-specific rubrics.
                        </p>
                        
                        <p><strong>ASMT2228: Authentic
                              Assessment</strong></p>
                        
                        <p>In this online course, participants will examine assessment methods that require students
                           to perform
                           real-world tasks that demonstrate meaningful application of essential knowledge and
                           skills. Instructors
                           of online and face-to-face course will benefit from this course.
                        </p>
                        
                        <p><strong>ASMT2122: Classroom
                              Assessment Techniques: Part 1 </strong></p>
                        
                        <p>This introductory course examines classroom assessment techniques that promote student
                           learning
                           through consistent, timely, formative measures. Also examined will be assessment practices
                           that invite
                           student feedback on the teaching and learning process, as well as on student achievement.
                           Note: This course is only offered online.
                        </p>
                        
                        <p><strong>ASMT2123 10: 
                              Classroom Assessment Techniques: Part 2</strong></p>
                        
                        <p>This advanced course builds upon Part 1 to examine classroom assessment techniques
                           that promote
                           students’ higher-order thinking through formative measures, including students’ abilities
                           to self-assess.
                           Also examined will be assessment practices that invite student feedback on the teaching
                           and learning
                           process. Note: This course is only offered online.
                        </p>
                        
                        <p><strong>ASMT2910: 
                              Selected Topics</strong></p>
                        
                        <p>          Seminars on special topics concerning assessment. Topics vary from year
                           to year, may be repeated
                           for credit.
                        </p>
                        
                        <p><strong>ASMT3221: 
                              Course Outcome Assessment</strong></p>
                        
                        <p>          Assessment techniques for course level outcomes. Includes design of authentic
                           assessment tasks
                           and methods of rating student work.
                        </p>
                        
                        <p><strong>ASMT3232: 
                              QM: Peer Reviewer Training</strong></p>
                        
                        <p>          This workshop will explore the Quality Matters project and processes and
                           will prepare the participant to
                           be part of an initiative that impacts the design of online courses and ultimately,
                           student success.
                           Participants should be experienced online instructors. After successfully completing
                           this workshop,
                           participants will be eligible to serve on a Valencia Quality Matters peer course review.
                           Note: This workshop is required for Digital Professor Certification.
                        </p>
                        
                        <p><strong>ASMT3233: 
                              Evidence of Learning</strong></p>
                        
                        <p>          Evidence of Learning is designed for faculty and staff who wish to understand
                           and participate in the
                           college-wide assessment of learning discussions. This online scenarios-based course
                           is intended to
                           foster a more positive attitude about evidence and the collection and use of data
                           as well as a
                           willingness to be involved in constructive dialogues about data.
                        </p>
                        
                        <p><strong>ASMT3323: 
                              Program Outcome Assessment</strong></p>
                        
                        <p>          Assessment techniques for program level outcomes. Includes design triangulated
                           methods of
                           measuring student learning. Both direct and indirect methods are included.
                        </p>
                        
                        
                        
                        <p><strong><u>IEC
                                 Principle: </u>ï¿½Worked with Instructors on Activities other than Courseworkï¿½</strong></p>
                        
                        <p>In addition to taking the LifeMap workshops described above, faculty can also discover
                           new ways to get involved with students outside of class by sponsoring student clubs
                           through SGA, attending campus events such as Matador Day, artistic performances, and
                           visiting lectureres, or traveling--either domestically or abroad--with students.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-effectiveness-committee/workshops.pcf">©</a>
      </div>
   </body>
</html>