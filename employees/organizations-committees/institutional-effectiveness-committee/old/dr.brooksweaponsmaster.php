<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dr. Brooks, Weapons Master | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-effectiveness-committee/old/dr.brooksweaponsmaster.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-effectiveness-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/institutional-effectiveness-committee/">Institutional Effectiveness Committee</a></li>
               <li><a href="/employees/organizations-committees/institutional-effectiveness-committee/old/">Old</a></li>
               <li>Dr. Brooks, Weapons Master</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div align="center" class="mainCenter">
                  
                  <table class="table ">
                     
                     <tr>
                        
                        <td align="center">
                           
                           <table class="table ">
                              
                              <tr>
                                 
                                 <td height="2"><img src="http://valenciacollege.edu/iec/images/spacer.gif" width="18" height="2"></td>
                                 
                                 <td rowspan="2">
                                    <table class="table ">
                                       
                                       <tr>
                                          
                                          <td colspan="2" rowspan="2">
                                             
                                             <table class="table ">
                                                
                                                <tr>
                                                   
                                                   <td colspan="2"><img src="http://valenciacollege.edu/iec/images/Dr.BrooksWeaponsMaster_000.jpg" width="450" height="299" border="0" alt="Dr. Brooks, Weapons Master"></td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td>
                                                      <span class="image">Dr. George Brooks: Master Destroyer</span><img src="http://valenciacollege.edu/iec/images/spacer.gif" width="2" height="2">
                                                      
                                                   </td>
                                                   
                                                   <td align="right"><span class="image"></span></td>
                                                   
                                                </tr>
                                                
                                             </table>
                                             
                                          </td>
                                          
                                          <td bgcolor="#999999" valign="top"><img src="http://valenciacollege.edu/iec/images/shadowTop.gif" width="3" height="3"></td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td bgcolor="#999999"><img src="http://valenciacollege.edu/iec/images/spacer.gif" width="3" height="3"></td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td bgcolor="#999999" align="left"><img src="http://valenciacollege.edu/iec/images/shadowBottom.gif" width="3" height="3"></td>
                                          
                                          <td bgcolor="#999999"><img src="http://valenciacollege.edu/iec/images/spacer.gif" width="3" height="3"></td>
                                          
                                          <td bgcolor="#999999"><img src="http://valenciacollege.edu/iec/images/spacer.gif" width="3" height="3"></td>
                                          
                                       </tr>
                                       
                                    </table>
                                 </td>
                                 
                                 <td height="2"><img src="http://valenciacollege.edu/iec/images/spacer.gif" width="18" height="2"></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td align="center" height="100%">
                                    <a href="BuzzDawsonofTheO20002.html"><img src="http://valenciacollege.edu/iec/images/previousArrow.gif" height="28" width="18" border="0" alt="Previous"></a>
                                    
                                 </td>
                                 
                                 <td align="center" height="100%">
                                    <a href="GuardingtheAmmunition.html"><img src="http://valenciacollege.edu/iec/images/nextArrow.gif" height="28" width="18" border="0" alt="Next"></a>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </table>
                           
                        </td>
                        
                     </tr>
                     
                  </table>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-effectiveness-committee/old/dr.brooksweaponsmaster.pcf">©</a>
      </div>
   </body>
</html>