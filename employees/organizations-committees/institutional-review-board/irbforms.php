<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Review Board | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-review-board/irbforms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-review-board/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Review Board</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/institutional-review-board/">Institutional Review Board</a></li>
               <li>Institutional Review Board</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>IRB Forms</h2>
                        
                        
                        <p>Submission of IRB applications and other forms is a two-part process involving submission
                           of both electronic and hard copy versions of the completed forms. 
                        </p>
                        
                        <p>For IRB applications from <u>internal researchers</u> (for example, from Valencia employees) Step 1 involves following this link to take
                           the IRB screening survey to determine if you need to submit an IRB application <a href="https://jfe.qualtrics.com/form/SV_8cXteuEKgs2LWok" target="_blank"><strong>http://tiny.cc/Valencia_IRB</strong></a>. 
                           
                           
                           Using this tool you will find out whether or not you need to submit an IRB review
                           application. If you are still unsure after step one then go directly to step two.
                        </p>
                        
                        <hr>
                        
                        <p> For IRB applications from <u>external researchers</u> please follow the <a href="documents/IRB-Steps-for-External-Researchers.pdf" target="_blank">steps needed to get approval for the study</a> if you have not already. Then, please use the interactive flowchart process (via
                           the "Getting Started" link to the left) to determine if you need to submit an application
                           for review, and if so, which type of review is most appropriate for your study. Using
                           this tool you will find out whether or not you need to submit an IRB review application.
                           If you are still unsure after step one then go directly to step two.
                        </p>
                        
                        <p>Step 2 is for people who have followed Step 1 and, based on their answers to the questions
                           asked, are required to submit an IRB review application. 
                        </p>
                        
                        <p>a. After reading the instructions please complete the IRB application package on your
                           computer. The forms are located at the bottom of this page. 
                        </p>
                        
                        
                        <p>b. Please send your application as an attachment via email to the IRB Chair at <a href="mailto:irb@valenciacollege.edu">irb@valenciacollege.edu</a>. Required additional documents for the IRB application (e.g., informed consent forms,
                           recruitment flyers, survey questions, pre- and post-tests) should be included as attachments
                           in the same email transmission. These additional documents may be submitted in Word,
                           Excel, or Adobe Acrobat (pdf) format. The subject line for the email should say "IRB
                           application [insert last name of Principal Investigator]." Signatures of the Principal
                           Investigator (PI), any Co-Principal Investigators, and the PI Supervisor/Administrator
                           are not required on the emailed version of the application form, however the review
                           process will not officially begin until the original signed hard copy of the IRB application
                           and all appropriate additional documents are received by the IRB Chair.
                        </p>
                        
                        <p>Other forms shown below are for use by PIs operating already-approved projects. </p>
                        
                        <p>Internal Investigators should send hard copies of documents to Dr. Laura Blasi, IRB
                           Chair at Mail Code DO-339.
                        </p>
                        
                        <p>External Investigators should mail hard copies to Dr. Laura Blasi, IRB Chair, Valencia
                           College, P.O. Box 3028, Mail Code DO-339, Orlando, FL 32802.
                        </p>
                        
                        <ul>
                           
                           <li><a href="documents/IRBReviewProcess_030108-2.pdf">IRB Review Process</a></li>
                           
                        </ul>
                        
                        
                        <p><span>If you are having trouble with<br>
                              the PDF form you will want to use the Word .doc version. <br>
                              These Forms are the same.</span></p>
                        
                        <ul>
                           
                           <li>
                              <a href="documents/IRB-Application-Package-internal.doc" target="_blank">IRB Application Package</a> (for Internal Investigators) - form and instructions
                           </li>
                           
                           <li>
                              <a href="documents/IRB-Application-Package-external.doc" target="_blank">IRB Application Package</a> (for External Investigators) - form and instructions<br>
                              
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <u><a href="documents/IRBAddendumRequestForm_020312.pdf">IRB Addendum/Modification Request Form</a></u> <span>(for Internal &amp; External Investigators) - for applications already approved under
                                 full or expedited review</span>
                              
                           </li>
                           
                           <li>
                              <a href="documents/IRBAdverseEventForm_030108.pdf">IRB Adverse Event/Unanticipated Problem/Increased Risk Report Form</a> <span>(for Internal and External Investigators) - for applications already approved under
                                 full or expedited review</span>
                              
                           </li>
                           
                           <li>
                              <a href="documents/IRBContReviewTerminationForm_030108.pdf">IRB Continuing Review/Termination Form</a> <span>(for Internal and External Investigators) - for applications already approved under
                                 fulll or expedited review</span>
                              
                           </li>
                           
                           <li>
                              <a href="documents/ParticipantWithdrawalComplaintForm_030108.doc">Participant Withdrawal/Complaint Report Form</a> <span>Internal and External Investigators) - for applications already approved under full
                                 or expidited review</span>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-review-board/irbforms.pcf">©</a>
      </div>
   </body>
</html>