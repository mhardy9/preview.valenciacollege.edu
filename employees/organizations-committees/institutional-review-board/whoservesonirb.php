<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Review Board | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-review-board/whoservesonirb.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-review-board/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Review Board</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/institutional-review-board/">Institutional Review Board</a></li>
               <li>Institutional Review Board</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Who serves on the IRB?</h2>
                        
                        <p>The IRB is composed of full-time employees
                           (with the exception of the Community Representative) with varying backgrounds
                           and expertise to comprehend the nature of the research, as well as other
                           competencies necessary to interpret regulations, relevant law, ethical
                           standards, and standards of professional practice. 
                        </p>
                        
                        
                        <p>Following Valencia’s current collaborative approach
                           to operations, interested applicants  submitted their IRB Membership Application
                           Forms to the appropriate nominating individual/departments, and the following individuals
                           were selected to serve:
                        </p>
                        
                        
                        <ul>
                           
                           <li>
                              <strong>Laura Blasi </strong> - Chair
                           </li>
                           
                           <li>
                              <strong>Jeff Hogan </strong> - Professional Staff Representative
                           </li>
                           
                           <li>
                              <strong>Esther Coombes</strong> - Academic Program Faculty Representative
                           </li>
                           
                           <li>
                              <strong>Risë</strong> <strong>Sandrowitz</strong>- Dean, Nursing
                           </li>
                           
                           <li>
                              <strong>Mary Beck</strong> - Academic Program Representative
                           </li>
                           
                           <li>
                              <strong>Linda Herlocker </strong> - Asst VP, Admissions &amp; Records 
                           </li>
                           
                           <li>
                              <strong>Linda Speranza</strong> - Career/Technical Program Faculty Representative
                           </li>
                           
                           <li>
                              <strong>Marcy Porter </strong> - Community Representative
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-review-board/whoservesonirb.pcf">©</a>
      </div>
   </body>
</html>