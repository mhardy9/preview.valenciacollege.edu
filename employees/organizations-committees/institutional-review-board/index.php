<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Review Board | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-review-board/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-review-board/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Review Board</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li>Institutional Review Board</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2> IRB Welcome</h2>
                        
                        <p>The Institutional Review Board (IRB) at Valencia College is a representative group
                           of faculty and staff members who review research proposals on a regular basis when
                           the person proposing the study aims to obtain any information about Valencia students
                           or employees and plans to share that information (for example, through publications
                           or presentations). Details and definitions are provided on this website along with
                           the steps necessary to apply for IRB review. 
                        </p>
                        
                        <p>As the Chair of the IRB Board I am here to help answer questions, provide the materials
                           that you need to apply for IRB review, and assist with training related to the ethical
                           conduct of research. The IRB Board and review process is required of institutions
                           like Valencia College by order of a federal mandate. This review is required by law
                           in order to protect the rights of participants and researchers and to identify the
                           risks that are assumed by institutions that support research intitatives. 
                        </p>
                        
                        <p>We are here to help you to understand the required steps and to participate in the
                           process. 
                        </p>
                        
                        <p><em>Laura Blasi. Ph.D.<br>
                              </em><em>Director, Institutional Assessment </em></p>
                        
                        <h3>Overview</h3>
                        
                        
                        <p>It is the investigator
                           who self-determines whether his/her project qualifies
                           as human subjects research, and whether an application
                           should be submitted to the Valencia IRB. However, it
                           is important to be aware that when conducting human
                           subjects research involving Valencia students or employees, <strong>any
                              investigator who elects not to apply for and receive
                              a formal IRB determination from Valencia’s IRB is non-compliant
                              with Valencia procedures. This means that the investigator’s
                              research will not be protected from any risk or liability
                              that IRB approval might have provided. </strong>Any individual <em>(inside or outside the College, including administrators, faculty, staff, or students)</em> who desires to conduct any systematic investigation that involves obtaining any information
                           about Valencia students or employees and plans to use that information as generalizable*
                           knowledge must obtain official Valencia Institutional Review Board <em>(IRB)</em> clearance. There are three types of IRB review processes – exempt from review, expedited
                           review, and full review – where research protocols are subjected to different degrees
                           of scrutiny. 
                        </p>
                        
                        <p>*generalizable - research
                           data gathered by systematic investigation to be used
                           in materials including but not limited to dissertations
                           and theses, articles for publication and conference presentations
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-review-board/index.pcf">©</a>
      </div>
   </body>
</html>