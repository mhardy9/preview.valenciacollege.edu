<ul>
<li><a href="default.php">Institutional Review Board</a></li>
<li class="submenu">
<a class="show-submenu" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
<li><a href="whoservesonirb.php">Who serves on the IRB?</a></li>
<li><a href="irbforms.php">Steps to Follow and Forms</a></li>
<li><a href="gettingstarted.php">Getting Started</a></li>
<li><a href="faq.php">FAQs</a></li>
<li><a href="links.php">External Links</a></li>
</ul>
</li>
</ul>