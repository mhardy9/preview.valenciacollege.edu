<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Review Board | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/institutional-review-board/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/institutional-review-board/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Institutional Review Board</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/institutional-review-board/">Institutional Review Board</a></li>
               <li>Institutional Review Board</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <p>The IRB is authorized to review, approve,
                           require modifications in, or disapprove human subjects research activities
                           conducted by or through the College. The following is a list of answers to common
                           questions, however please keep in mind that these responses are general in nature,
                           and each potential research activity has unique features that could impact the answer
                           shown. 
                        </p>
                        
                        
                        <p><strong>Q: How do I know if I should submit an application to the Valencia IRB?</strong></p>
                        
                        <p>A: You should thoroughly read and understand the Valencia IRB process, including the
                           Federal IRB regulations, particularly focusing on whether the proposed project meets
                           the Federal definition of research. The Federal definition of research is: a systematic
                           investigation which is intended to develop or contribute to generalizable knowledge.
                           If your activity meets this defintion, then an IRB application should be submitted.
                        </p>
                        
                        <p><strong>Q: What are some examples of intending "to develop or contribute to generalizable
                              knowledge"?</strong></p>
                        
                        <p>A: Valencia interprets this to be when you intend to use the outcomes of your activity
                           to contribute to an action that is broad in scope. Examples include: submitting an
                           article for publication (including dissertations), developing and delivering a conference
                           or workshop presentation, or contributing to a process that ultimately would impact
                           a decision made by Valencia regarding major programs and services. It does not include
                           routine classroom assignments or the performance of regular job duties. 
                        </p>
                        
                        <p><strong>Q: Who decides whether an IRB application should be submitted?</strong></p>
                        
                        <p>A: The individual self-determines whether to submit an application to Valencia's IRB
                           based on his/her understanding of IRB regulations and the Valencia IRB process. The
                           IRB Chair and the IRB members may offer guidance, but the individual makes the final
                           decision to submit an application.
                        </p>
                        
                        <p><strong>Q: If I determine that I don't need to submit an IRB application, may I submit one
                              anyway?</strong></p>
                        
                        <p>A: Yes. Even if their current intent does not involve contributing to generalizable
                           knowledge, sometimes individuals elect to submit applications so that they receive
                           an official IRB determination "just in case" they decide in the future that they would
                           like to include their activities and data in a project that meets the Federal definition
                           of research. 
                        </p>
                        
                        <p><strong>Q: Do my students need to submit IRB applications if I assign them to do research
                              projects as part of my class?</strong></p>
                        
                        <p>A: Yes, but only if the projects meet the Federal definition of research. </p>
                        
                        <p><strong>Q: I think my research
                              qualifies for “exempt” status – do
                              I still need to submit an IRB application?</strong><br>
                           A: Yes. Only the Valencia IRB can officially issue an “exempt” determination.
                        </p>
                        
                        
                        <p><strong>Q: I don’t know how to answer some of the questions on the
                              IRB application but I am in a hurry to get started – can I submit
                              it anyway to begin the process and take care of the details later?</strong><br>
                           A: No. The application is designed to ensure that the IRB has
                           sufficient information to make a judgment regarding compliance with
                           Federal HRP regulations. Investigators who submit incomplete applications
                           will be informed that their applications will not be reviewed until
                           all the information is received. Investigators are encouraged to build
                           sufficient time for IRB review into their research timetable.
                        </p>
                        
                        <p><strong>Q: May I start on my research before receiving an official IRB determination?</strong></p>
                        
                        <p>A: Yes, there are certain steps in the research process that you may complete prior
                           to submitting an application to the Valencia IRB, but you cannot begin recruiting
                           participants or collecting data until you receive the official IRB determination for
                           your application. 
                        </p>
                        
                        <p><strong>Q: Do all survey projects have to be submitted to the IRB for review?</strong></p>
                        
                        <p>A: No. Whether an IRB application needs to be submitted is based on whether the proposed
                           project meets the Federal definition of research; it is not based on the research
                           process.  
                        </p>
                        
                        <p><strong>Q: Must I submit an IRB application if my survey will be anonymous or if the data
                              I'm collecting will be aggregated?</strong></p>
                        
                        <p>A: Not necessarily. Whether an IRB application needs to be submitted is based on whether
                           the proposed project meets the Federal definition of research; it is not based on
                           the research process. 
                        </p>
                        
                        <p><strong>Q: What happens if I submit my application but the IRB requests changes?</strong><br>
                           A: The IRB chair, on behalf of the IRB, conducts an initial review
                           of all applications to determine if the application: (1) is complete;
                           (2) is eligible for exemption; (3) is eligible for expedited review;
                           or (4) must receive full IRB review. Based on the Federal regulations,
                           changes to the application may be requested in any of these situations.
                           If changes are needed to meet compliance requirements, a detailed
                           explanation is provided; a determination cannot be issued until the
                           investigator submits additional information and/or materials that satisfy the changes
                           requested.
                        </p>
                        
                        <p><strong>Q: Does receiving an “exempt” determination
                              mean that I do not have to worry about informed consent?</strong><br>
                           A: No. All research projects that receive a determination of “exempt” or “approval”
                           (expedited
                           or full) must still comply with informed consent requirements. Projects
                           that do not fall under the Federal definition of research involving
                           human subjects are not required to comply with Federal informed consent
                           regulations, although compliance is encouraged.
                        </p>
                        
                        <p><strong>Q: What is a waiver of informed consent?</strong><br>
                           A: A waiver of documentation of informed consent may be requested at
                           the time of application if the research project meets certain conditions
                           (see Section 2 of the Informed Consent section of the IRB application
                           package). However, this waiver only means that the participant’s
                           signature is not required on the informed
                           consent form – informed consent is still required.
                        </p>
                        
                        <p><strong>Q: How long is IRB approval good for?</strong><br>
                           A: When you submit your application, you can request any period of
                           time up to 10 years. Based on Federal regulations, projects longer
                           than 12 months that receive approval under the expedited or full
                           review process will be required to submit periodic reports (minimum:
                           annually) as a condition of approval.
                        </p>
                        
                        <p><strong>Q: Someone has asked me to participate in his/her research. How do
                              I know it has been through IRB review?</strong><br>
                           A: IRB review is documented by a stamp placed on informed
                           consent forms submitted at the time of application. The stamp contains the IRB application
                           number assigned to the project and the determination given. If no stamp is evident
                           on the informed consent document that you receive, you can send an email to irb@valenciacollege.edu
                           and ask if an IRB application has been submitted for the project in
                           question.<br>
                           <br>
                           <span><strong>Q: Who should complete IRB training?</strong><br>
                              </span>A: Regulations require that investigators administering research that
                           has been approved via either expedited or full review must complete
                           training in a timely manner as a condition of the approved research.
                           However, individuals may complete training even if no research is
                           currently planned.
                           <strong>Q: What will I learn in Valencia’s
                              IRB training?</strong><br>
                           A: Protecting Human Research Participants online training consists of seven modules;
                           each addressing the principles used to define ethical research using humans and the
                           regulations, policies, and guidance that describe the implementation of those principles.
                           Four of these modules are followed by a quiz. The entire course will take approximately
                           3 hours to complete. Register for free to take this assessment at:<a href="http://phrp.nihtraining.com/users/login.php" target="_blank"> http://phrp.nihtraining.com/users/login.php</a> if you have not taken it within the past three years.
                        </p>
                        
                        <p><strong>Q: Do I have to take Valencia’s
                              IRB training if I completed IRB training elsewhere?</strong><br>
                           A: Federal regulations indicate that other formal IRB training may
                           satisfy the training requirement imposed on investigators of approved
                           research protocols as long as the training was completed within the
                           past three years.
                        </p>
                        
                        <p><strong>Q: What happens if my application is disapproved?</strong></p>
                        
                        <p>A: Before a determination is issued, the IRB strives to communicate with PIs to request
                           or suggest modifications that will bring the application into compliance as needed.
                           Should an application be subject to full review by the IRB, and the Board issues a
                           disapproval, the determination cannot be overidden based on Federal regulations. Based
                           on the pre-determination communication process with PIs, Valencia's IRB does not have
                           an appeals process. 
                        </p>
                        
                        <p><strong>Q: Can I still try to do my research if my application is disapproved?</strong><br>
                           A: Technically, yes, but not without consequences. The IRB process
                           is designed to place the burden on the investigator – just
                           as with any regulation or policy, ultimately it is the individual’s
                           responsibility to comply. Investigators are expected to submit an
                           IRB application, to comply with the regulations when the application
                           is approved, or to not proceed if the application is disapproved.
                           Receiving a disapproval from the IRB cannot physically stop an investigator
                           from moving forward if he/she so chooses; the IRB can only exempt,
                           approve, disapprove, or terminate IRB approval. However, it is important
                           to be aware that a disapproval or the absence of a formal IRB determination
                           from Valencia’s IRB means that the researcher will not be protected
                           from any individual risk or liability that IRB approval provides.
                           In addition, the investigator will be at risk for not having their
                           study accepted for publication or presentation. Also, by having a
                           process whereby a summary of IRB applications and determinations
                           is shared, the Valencia community is informed and thus may take any
                           action that they believe is appropriate separate from the IRB application
                           process.
                        </p>
                        
                        <p><strong>Q: I have agreed to serve as a project director on a Valencia grant.
                              Do I need to submit an IRB application?</strong><br>
                           A: IRB review of research conducted as part of institutional grant
                           proposals is now integrated into the final review process before grants
                           are submitted to the 17 Federal agencies that have agreed to follow
                           the Common Rule regarding Human Research Protection. As a result, project
                           directors should follow the direction given to them by the Office of
                           Resource Development.
                        </p>
                        
                        <p><strong>Q: How long does it take to receive a determination after I submit my application?</strong></p>
                        
                        <p>A: We request that applications be submitted at least 3 weeks prior to the start date
                           indicated in the application. The timing of the determination will vary based upon
                           the completeness of the application, current workload, whether the application is
                           subject to full board review, and other factors. Determinations for some applications
                           involving complex protocol, needing input from special resources, and/or going to
                           the full Board for review could take several months. PIs should not begin the participant
                           recruitment process or data collection until they receive their official IRB determination,
                           but they may work on other aspects of their research while waiting. 
                        </p>
                        
                        <p><strong>Q: Does Leadership Valencia IRB training count towards the Professional Development
                              component of the faculty compensation plan?</strong></p>
                        
                        <p>A: Yes. The faculty member must obtain proof of attendance/completion from Leadership
                           Valencia. Full-time faculty must upload that evidence into their online Professional
                           Development planner under Faculty Tools in Atlas. Part-time faculty must send the
                           proof to Jenelle Conner, Coordinator, Faculty &amp; Inst Development, at Mailcode DO-23,
                           phone (407) 582-2526. 
                        </p>
                        
                        <p><strong>Q: What is our human subjects Federal-wide Assurance (FWA) number?</strong></p>
                        
                        <p>A: FWA00022007  
                           FWA - The Department of Health and Human Services
                           Office for Human Research Protections assigns an FWA
                           approval number to each campus of a university.
                        </p>
                        
                        <p><strong>Q: What is our human subjects Institutional Review Board (IRB) HRRC (Human Research
                              Review Committee) registration number?</strong></p>
                        
                        <p>A: IRB00006823 - 
                           Every IRB on a campus is registered upon filing for 
                           a federal-wide assurance number and each IRB is assigned an IRB (HRRC) number. 
                        </p>
                        
                        <p><strong>Q: What is our human subjects IORG Identifier number?</strong></p>
                        
                        <p>A: IORG0005669 - 
                           The IRB Organization (IORG) number identifies the
                           organization when it files or updates its Federal-wide Assurances (FWA).
                        </p>
                        
                        <p><strong>Q: What's the difference between a human subjects IORG (IRB Organization number),
                              an FWA (Federal-wide Assurance) number and an IRB (Institutional Review Board)<br>
                              HRRC (Human Research Review Committee) registration number?</strong></p>
                        
                        <p>A: The IORG number identifies the institution. The university has only one IORG number.
                           FWA (Federalwide 
                           Assurance) numbers identify the branch campuses that have filed assurances that they
                           will
                           comply with the federal regulations covering the protection of human subjects. Each
                           branch campus
                           2
                           has an FWA number. The IRB HRRC number is the registration number identifying each
                           IRB on
                           campus. Institutions may have multiple IRBs.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/institutional-review-board/faq.pcf">©</a>
      </div>
   </body>
</html>