<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Association of Valencia Women | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/association-of-valencia-women/committees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/association-of-valencia-women/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Association of Valencia Women</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/association-of-valencia-women/">Association Of Valencia Women</a></li>
               <li>Association of Valencia Women</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h3>Association of Valencia Women Executive Committee </h3>
                        
                        <p>July 2016- June 2017                      </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>President:</strong></div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=lmacon.html">Lisa Macon</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>President Elect:</strong></div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=jrodriguez237.html">Jeannie Rodriguez</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Treasurer:</strong></div>
                                 
                                 <div data-old-tag="td">TBA</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Secretary:</strong></div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=ngumbs.html">Nikkia Gumbs</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Marketing Coordinator:</strong></div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=ghall13.html">Genevieve Hall</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Past-President:</strong></div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=jmezquita.html">Jennifer Mezquita</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <h3>Association of Valencia Women Committee Chairs </h3>
                        
                        <p>Please e-mail the Association of Valencia Women at <a href="mailto:avw@valenciacollege.edu">avw@valenciacollege.edu</a> or use the<a href="http://net4.valenciacollege.edu/forms/avw/contact.cfm" target="_blank"> Contact Us</a> form if you would like to be a part of the following committess.
                        </p>
                        
                        <p><strong>Membership Committee &nbsp;&nbsp;</strong></p>
                        
                        <ul>
                           
                           <li> Maintain membership 
                              records
                           </li>
                           
                           <li>Share updated membership roster with board on 
                              regular basis
                           </li>
                           
                           <li>Contact members whose renewal is due to encourage 
                              continued participation
                           </li>
                           
                           <li> Inform potential and current members of the opportunity for 
                              payroll deduction of membership fees
                           </li>
                           
                        </ul>
                        
                        <p><strong>Marketing and Social Media Committee</strong></p>
                        
                        <ul>
                           
                           <li>Develop marketing 
                              strategies to increase membership
                           </li>
                           
                           <li>Create a webpage for AVW</li>
                           
                           <li> Create promotional materials for upcoming events</li>
                           
                           <li>Support membership and professional development efforts through member communication
                              
                           </li>
                           
                           <li>Maintain social media outlets</li>
                           
                           <li>Announce scholarship opportunity to students and appropriate college-wide offices
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Community Service Committee &nbsp;</strong></p>
                        
                        <ul>
                           
                           <li>Coordinate all aspects 
                              of the AVW scholarship 
                           </li>
                           
                           <li>Determine scholarship amount and selection criteria</li>
                           
                           <li>Collect and distribute essays and rating sheets 
                              to board
                           </li>
                           
                           <li>Notify each applicant with final outcome and 
                              invite recipient to AVW event
                           </li>
                           
                           <li>National Institute for Leadership Development</li>
                           
                           <li>Announce opportunity, recruit applicants, manage 
                              selection process
                           </li>
                           
                           <li>Maintain Junior League/Bargain Box relationship 
                              and coordinate clothing drive
                           </li>
                           
                        </ul>
                        
                        <p><strong>Professional Development Committee &nbsp;&nbsp;</strong></p>
                        
                        <ul>
                           
                           <li>Develop, coordinate 
                              and assist with marketing of Leadership Valencia workshops
                           </li>
                           
                           <li>Develop, coordinate and assist with marketing 
                              of reading circles
                           </li>
                           
                           <li>Develop, coordinate and assist with marketing 
                              of Path to Leadership presentations (College and Local Leaders)
                           </li>
                           
                           <li>Seek new opportunities to expand our current 
                              offerings 
                           </li>
                           
                        </ul>
                        
                        <p><strong>Social Events and Fundraising Committee &nbsp;</strong></p>
                        
                        <ul>
                           
                           <li>Coordinates Wine and Cheese Social and Silent Auction </li>
                           
                           <li> Coordinates Thanksgiving Banquet </li>
                           
                           <li> Coordinates End-of-Year event </li>
                           
                           <li> Seek interesting, timely and stimulating social 
                              events where we can encourage donations to our scholarship 
                           </li>
                           
                           <li> Fine tune and market the payroll deduction option for scholarship 
                              donation.
                           </li>
                           
                        </ul>
                        
                        <p><strong>Mentoring Committee &nbsp;</strong></p>
                        
                        <ul>
                           
                           <li>Create guidelines 
                              for mentoring 
                           </li>
                           
                           <li> Plan, coordinate and nurture a program for mentor-mentee 
                              relationships 
                           </li>
                           
                        </ul>
                        
                        <p><strong>Professional Development Day Committee &nbsp;</strong></p>
                        
                        <ul>
                           
                           <li>Plan, coordinate and implement a one-day program that emphasizes 
                              business skills and develops professionalism in a selected group 
                              of female students. 
                           </li>
                           
                           <li> Assess the value of the program and make recommendations for 
                              future events
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/association-of-valencia-women/committees.pcf">©</a>
      </div>
   </body>
</html>