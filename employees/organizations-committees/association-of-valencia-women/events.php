<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Association of Valencia Women | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/association-of-valencia-women/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/association-of-valencia-women/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Association of Valencia Women</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/association-of-valencia-women/">Association Of Valencia Women</a></li>
               <li>Association of Valencia Women</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h3>Networking Activities and Training Events</h3>
                        
                        
                        <div>
                           
                           <p><strong>Upcoming Events </strong></p>
                           
                           <strong>The Association of Valencia Women is pleased to announce three reading circles for
                              the 2015-2106 Academic year, NEW "ebook" options, and NEW ï¿½Skype for Businessï¿½
                              remote sessions.  Below are the selected titles based on the results from the survey
                              sent to all AVW members (and those that attended the ï¿½Meet-and-Greetï¿½ sessions).</strong>
                           
                           
                           
                           
                           
                           <div>
                              
                              <h3>Spring Reading Circle</h3>
                              
                              
                              <div>
                                 
                                 <p><img alt="Spring Reading - Develop Exemplary Performance One Person at a Time by Michael Sabbag" border="0" height="164" hspace="5" src="Events-Spring16Reading-DEP.jpg" vspace="5" width="108"><a href="http://www.amazon.com/Developing-Exemplary-Performance-Person-Time/dp/0891062491/ref=sr_1_1?s=books&amp;ie=UTF8&amp;qid=1445217908&amp;sr=1-1&amp;keywords=Developing+Exemplary+Performance+One+Person+at+a+Time"><br>
                                       <br><strong>Spring Reading Circle: “Developing Exemplary Performance One Person at a Time” by
                                          Michael Sabbag</strong></a> <br>
                                    <strong>Description</strong>: Spring Reading Circle: "Developing Exemplary Performance One Person at a Time" by
                                    Michael Sabbag. <span>*Note: this book is available through Valencia College's "e-brary" program with unlimited
                                       access, meaning as many people that choose to download the book may do so and read
                                       at the same time.</span><br>
                                    <strong>Instructor/Facilitator or moderator/co-facilitators:</strong> Karen Reilly and Karen Cowden<br>
                                    <strong>Who to call if there are question ï¿½ aka ï¿½ the training contact:</strong> Karen Cowden x1960<br>
                                    <strong>Cap (max number):</strong> 20<br>
                                    <strong>Registration deadline (when to lock the roster):</strong> April 5 at noon<br>
                                    <strong>Date:</strong> April 5, 2016<br>
                                    <strong>Time:</strong> 1 ï¿½ 2:30pm<br>
                                    <strong>Duration:</strong> 90 minutes<br>
                                    <strong>Who needs to get the enrollment report:</strong> Karen Cowden<br>
                                    <strong>Location:</strong> West 9-140b<br>
                                    
                                    
                                 </p>
                                 
                              </div>                    
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Summer Reading Circle</h3>
                              
                              <div>
                                 
                                 <p><img alt="Nudge: Improving Decisions About Health, Wealth, and Happiness - Richard H. Thaler" border="0" height="164" hspace="5" src="Events-Summer16Reading-Nudge.jpg" vspace="5" width="108"><a href="http://www.amazon.com/Nudge-Improving-Decisions-Health-Happiness/dp/014311526X/ref=sr_1_1?s=books&amp;ie=UTF8&amp;qid=1445218003&amp;sr=1-1&amp;keywords=Nudge%3A+Improving+Decisions+About+Health%2C+Wealth%2C+and+Happiness"><br>
                                       <br><strong>Summer Reading Circle: “Nudge: Improving Decisions About Health, Wealth, and Happiness”
                                          by Richard H. Thaler</strong></a> <br>
                                    
                                    <strong>Instructor/Facilitator or moderator/co-facilitators:</strong> Karen Reilly and Karen Cowden<br>
                                    <strong>Who to call if there are question ï¿½ aka ï¿½ the training contact:</strong> Karen Cowden x1960<br>
                                    <strong>Cap (max number):</strong> 20<br>
                                    <strong>Registration deadline (when to lock the roster):</strong> June 23 at noon<br>
                                    
                                    
                                    <strong>Date:</strong> Thursday, June 23rd, 2016<br>
                                    <strong>Time:</strong> 1-2:30pm<br>
                                    <strong>Duration:</strong> 1.5 hours<br>
                                    <strong>Who needs to get the enrollment report:</strong> Karen Cowden<br>
                                    
                                    <strong>Location: </strong>West Campus, Room 9-140
                                    ï¿½Skype for Businessï¿½ (formerly Lync) online option
                                    <br><br><br><br></p>
                                 
                                 
                                 <p>*Note: this book is NOT available through Valencia College's "e-brary" program but
                                    it is available to read for free online. Please contact Lisa Macon (<a href="mailto:lmacon@valenciacollege.edu">lmacon@valenciacollege.edu</a>) for more information.
                                 </p>                  
                                 
                              </div>                    
                              
                           </div>                                
                           
                           <div>
                              
                              <h3>Path to Leadership:  Dr. Lisa Macon</h3>
                              
                              <div>
                                 
                                 <p>
                                    
                                    <strong>Description:</strong>Dr. Karen Reilly will interview Lisa Macon, Ph.D., dean of engineering, computer programming,
                                    and technology, regarding her path from computer software developer to Valencia dean.<br>
                                    <strong>Date:</strong> Wednesday, April 6 <br>
                                    <strong>Time:</strong> 3:30 - 5:00pm<br>
                                    <strong>Location:</strong> West 6-202<br>
                                    
                                    <strong><span>Arrive early to enjoy refreshments and interact with fellow AVW members!</span></strong>
                                    
                                    
                                 </p>
                                 
                              </div>                    
                              
                           </div>  
                           
                           <div>
                              
                              <h3>Annual Meeting and Prospective Officer Information Session</h3>
                              
                              <div>
                                 
                                 <p>
                                    
                                    <strong>Description:</strong> AVW's annual membership update meeting.  Interested in joining our board? Come out
                                    and meet the current officers and learn more about the positions and leadership within
                                    AVW.<br>
                                    <strong>Instructor/Facilitator or moderator/co-facilitators:</strong> Karen Reilly <br>
                                    <strong>Who to call if there are question ï¿½ aka ï¿½ the training contact:</strong> Karen Reilly x1810<br>
                                    <strong>Date:</strong> April 21, 2016<br>
                                    <strong>Time:</strong> 11:00am - 12:30pm and light lunch will be served.<br>
                                    <strong>Location:</strong> West 11-106<br>
                                    
                                    
                                    
                                    
                                 </p>
                                 
                              </div>                    
                              
                           </div>  
                           
                           
                           
                           
                           
                           <div>
                              
                              <h3>Second Harvest Food for Thought Tour</h3>
                              
                              <div>
                                 
                                 <p>
                                    
                                    <strong>Description:</strong> During this one-hour information session, you will learn more about who is hungry
                                    in our community, and then take a walking tour of the food bank to see how they operate.
                                    <br>
                                    <strong>Instructor/Facilitator or moderator/co-facilitators:</strong> Carol Millenson<br>
                                    <strong>Who to call if there are question ï¿½ aka ï¿½ the training contact:</strong> Carol Millenson x1870<br>
                                    <strong>Date:</strong> April 22, 2016<br>
                                    <strong>Time:</strong> 11:15 ï¿½ 12:45pm<br>
                                    
                                 </p>
                                 
                              </div>                    
                              
                           </div>                       
                           
                           <div>
                              
                              <h3>AVW Speed Mentoring</h3>
                              
                              <div>
                                 
                                 <p>
                                    
                                    <strong>Description:</strong> Join us for this fun and exciting activity that will allow you to meet a variety
                                    of potential mentors across Valencia in a ï¿½speed-datingï¿½ format.<br>
                                    <strong>Instructor/Facilitator or moderator/co-facilitators:</strong> Karen Reilly<br>
                                    <strong>Who to call if there are question ï¿½ aka ï¿½ the training contact:</strong> Karen Reilly x1810<br>
                                    <strong>Date:</strong> May 17, 2016<br>
                                    <strong>Time:</strong> 1 ï¿½ 2:30 pm<br>
                                    <strong>Location:</strong> West 11-106<br>
                                    
                                    
                                 </p>
                                 
                              </div>                    
                              
                           </div>    
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       To sign up for one (or more) reading circles, choose from the following two options:
                                       
                                       
                                       <ol>
                                          
                                          <li>Join the "Facebook" event on the Association of Valencia Women page (listed by title).</li>
                                          
                                          <li>Contact Karen Cowden at kcowden@valenciacollege.edu.  Be sure to state which circles
                                             you plan to attend.
                                          </li>
                                          
                                       </ol>
                                       
                                       <p>
                                          To obtain a book you have three options:
                                          
                                       </p>
                                       
                                       <ol>
                                          
                                          <li>Download the e-book through Valencia College's library lending program directly to
                                             your device, computer, phone, tablet, or reader (see directions and video, below).
                                          </li>
                                          
                                          <li>Check out the book(s) placed in the "Lending Library" for AVW (two books are on reserve
                                             for West Campus, and one is on reserve for East Campus).
                                          </li>
                                          
                                          <li>E-mail Karen Cowden (kcowden@valenciacollege.edu) to request a possible paper copy.
                                             
                                             
                                             <ul>
                                                
                                                <li>NOTE: we will have a limited supply of 10 books available for anyone who sends an
                                                   email (first come first served) that they are not able to use the ebook.  After the
                                                   10 are gone you are most welcome to rent the book from local library or buy one at
                                                   your chosen vendor.
                                                </li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ol>
                                       
                                       <ul>
                                          
                                          <li>
                                             <strong>Please note that the summer book, Nudge: Improving Decisions About Health, Wealth,
                                                and Happiness is not available as an e-book through Valencia's library.</strong>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       Download Directions: 
                                       
                                       The following two books are available as e-books with unlimited simultaneous user
                                       access through Valencia College's library ebrary collection, from Proquest. Here are
                                       instructions for quick access to each:
                                       
                                       <p>
                                          <strong>To access Help Them Grow or Watch Them Go:</strong>
                                          
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Login to Atlas.</li>
                                          
                                          <li>Go to Employees tab.</li>
                                          
                                          <li>Click on Search the Library.</li>
                                          
                                          <li>Open a new tab and paste the following URL: <a href="http://site.ebrary.com.db29.linccweb.org/lib/valencia/detail.action?docID=10597156&amp;p00=help+grow" target="_blank">Help Them Grow or Watch Them Go</a>
                                             
                                          </li>
                                          
                                          <li>Click read online or full download.</li>
                                          
                                       </ul>
                                       <strong>To access Developing Exemplary Performance One Person at a Time:</strong>
                                       
                                       <ul>
                                          
                                          <li>Login to Atlas.</li>
                                          
                                          <li>Go to Employees tab.</li>
                                          
                                          <li>Click on Search the Library.</li>
                                          
                                          <li>Open a new tab and paste the following URL: <a href="http://site.ebrary.com.db29.linccweb.org/lib/valencia/detail.action?docID=10356684&amp;p00=%E2%80%9Cdeveloping+exemplary+performance+one+person+time" target="_blank">Developing Exemplary Performance One Person at a Time</a>
                                             
                                          </li>
                                          
                                          <li>Click read online or full download.</li>
                                          
                                       </ul>
                                       
                                       <p>
                                          
                                          For help opening these e-books in the online reader or downloading them, please contact
                                          a Valencia librarian OR use the help guides provided by Proquest:
                                          
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>
                                             <a href="http://proquest.libguides.com/ebrary/new-reader" target="_blank">http://proquest.libguides.com/ebrary/new-reader (using the online reader)</a>
                                             
                                          </li>
                                          
                                          <li>
                                             <a href="http://proquest.libguides.com/ebrary/download-desk" target="_blank">http://proquest.libguides.com/ebrary/download-desk (downloading to a desktop)</a>
                                             
                                          </li>
                                          
                                          <li>
                                             <a href="http://proquest.libguides.com/ebrary/download-mobile" target="_blank">http://proquest.libguides.com/ebrary/download-mobile (downloading to a mobile device)</a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>                
                           
                           
                           
                           
                        </div>
                        
                        
                        <div>
                           
                           <p><strong>Activities</strong></p>
                           
                           <div>
                              
                              <h3>Scholarships</h3>
                              
                              <p>AVW Valencia Chapter provides two scholarships annually. A student scholarship is
                                 awarded in the spring and an employee scholarship is awarded to an AVW chapter member.
                              </p>
                              
                              <h3>Community Projects</h3>
                              
                              <p>AVW Valencia Chapter hosts many community projects; from donating clothing, food,
                                 gift certificates to multiple organizations throughout Central Florida. Some of the
                                 organizations we've been involved with include Chlidren's Home Society of Florida,
                                 Dress for success, the Harbor House, and many more.
                              </p>
                              
                              <h3>Member Socials</h3>
                              
                              <p>AVW members are invited to gather together for an evening full of fun and fellowship.
                                 Some of the past events include a Wine &amp; Cheese Social &amp; Silent Auction, Fellowship
                                 Line Dancing, Festival of Trees Social, Albin Polasek Museum and Sculpture Garden
                                 Tour, An Evening at the Enzian and in Summer 2012, <em>CreativeTeam Building with Chocolate</em>.
                              </p>
                              
                              <p> <span>We are always looking for suggestions on these type of events, please <a href="mailto:aacww@valenciacollege.edu">contact us</a> if you have any! </span></p>
                              
                              <h3>Professional Workshops </h3>
                              
                              <p>One of AVW's main focus is your professional and personal growth, so many times during
                                 the year we host multiple workshops to help maximize our members potential. Workshops
                                 such as <em>Creating Relationships in our Community, Successful Living: Five Steps to Achieving
                                    Everything You Want in Life</em> , <em>Forum for Financial Questions / Personal Finance Workshop.</em></p>
                              
                           </div>
                           
                        </div>        
                        
                        
                        
                        <div>
                           
                           <p><strong>Past Events </strong></p>
                           
                           
                           <div>
                              
                              <h3>Summer Reading Circle</h3>
                              
                              <p>To sign up for one or more reading circles and receive your book(s), please  contact
                                 Sherri Dixon at <a href="mailto:sdixon@valenciacollege.edu">sdixon@valenciacollege.edu</a>.  &nbsp;Be sure to state which circles you plan to attend.
                              </p>
                              
                              <div>
                                 
                                 <p><img alt="Summer Reading - Speed of Trust by Stephen Covey" border="0" height="164" hspace="5" src="Events-SummerReading-SpeedOfTrust.png" vspace="5" width="108"><a href="http://www.myspeedoftrust.com/How-The-Speed-of-Trust-works/book"><br>
                                       <br><strong>Summer  Reading Circle: “The Speed of Trust” by Stephen Covey</strong></a> <br>
                                    <strong>Date:</strong> Tuesday, July 14, 2015<br>
                                    <strong>Time: </strong>1-2:15pm<br>
                                    <strong>Location: </strong>East Campus Room 3-113<br><br><br><br></p>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           <div>
                              
                              <h3>Path to Leadership Series</h3>
                              
                              <div>
                                 
                                 <p><img alt="Path to Leadership - Dr. Falecia Williams" border="0" height="150" hspace="5" src="events-faleciawilliams.jpg" vspace="5" width="120"><strong> Dr. Falecia Williams, West Campus President</strong><br>
                                    Wednesday, April 30, 2014<br>
                                    West Campus, Special Events Center<br>
                                    Networking &amp; Refreshments before the program<br><br><br><br></p>
                                 
                                 <p>Falecia Williams was tapped to lead the college's flagship West Campus in 2011.  The
                                    campus has more than 26,000 students. Prior to that appointment, she was the Assistant
                                    Vice President for Workforce Development, leading the college's more than 100 Associate
                                    in Science degree (career) programs. Graduates of those programs enjoy a 96 percent
                                    placement rate.  Dr. Williams also led the team that created the first Bachelor's
                                    degree programs in 2012.
                                 </p>
                                 
                                 <p>In 2014, she was named one of five "Women of Distinction" an award sponsored by the
                                    City of Orlando. Dr. Williams has worked at Valencia for more than 15 years, first
                                    as an adjunct professor, then in leadership roles in conjunction with dual enrollment,
                                    high school outreach programs and workforce development.
                                 </p>
                                 
                              </div>           
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Painting With a Twist</h3>
                              
                              <div>
                                 
                                 <p> <a href="http://www.paintingwithatwist.com/orlando/"><img alt="Paiting with a Twist" border="0" height="124" src="events-painting-with-a-twist.jpg" width="200"></a><strong> Painting with a Twist!  Get your fun on! Be inspired!</strong><br>
                                    Friday, May 31, 2013<br>
                                    6700 Conroy Windermere Rd, Ste 125, Orlando, FL 32835<br>
                                    Networking &amp; Refreshments before the program<br><br><br><br></p>
                                 
                                 <p>Bring your creative energy and together we will learn new skills and techniques for
                                    an inspirational painting experience with AVW friends! Share in creative camaraderie
                                    and recreate a masterpiece in your own special style.
                                 </p>
                                 
                                 <p>Each member will take their work of art home to flaunt those new talents. Cost was
                                    $35 per person.
                                 </p>
                                 
                              </div>           
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Reading Circles</h3>
                              
                              <p>These events were open to all Employees: Read a great book and share your insights!
                                 The reading circles are a casual fun way to share knowledge and make new connections
                                 with your colleagues around the college.
                              </p>
                              
                              
                              <div>
                                 
                                 <p><img alt="Reading Circle - The Four Hour Work Week" border="0" height="159" hspace="5" src="events-readingcircle-fourhour.png" vspace="5" width="106"><a href="http://fourhourworkweek.com/overview/" target="_blank">The 4-Hour Work Week by Timothy Ferriss</a><br>
                                    <strong>Date:</strong> Friday, March 20, 2015<br>
                                    <strong>Time:</strong> 1-2:15pm<br>
                                    <strong>Location:</strong> Winter Park Campus Room 107<br><br><br><br></p>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <p><img alt="Reading Circle - Thinking Fast and Slow" border="0" height="159" hspace="5" src="events-readingcircle-thinking.png" vspace="5" width="106"><a href="http://www.summary.com/book-reviews/_/Thinking-Fast-and-Slow/" target="_blank">Thinking Fast and Slow by Daniel Kahneman</a><br>
                                    <strong>Date:</strong> Tuesday, October 21, 2014<br>
                                    <strong>Time:</strong> 1-2:15pm<br>
                                    <strong>Location: </strong>West Campus Room 6-326<br><br><br><br></p>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <p><img alt="Reading Circle - The Power of Habit" border="0" height="159" hspace="5" src="events-readingcircle-powerhabit.jpg" vspace="5" width="106"><a href="http://www.amazon.com/The-Power-Habit-Business-Hardcover/dp/B00CNWO0U4/ref=sr_1_2?ie=UTF8&amp;qid=1397439635&amp;sr=8-2&amp;keywords=the+power+of+habit+why+we+do+what+we+do+in+life+and+business" target="_blank">The Power of Habit: Why We Do What We Do in Life and Business by Charles Duhigg</a><br>In The Power of Habit, award-winning New York Times business reporter Charles Duhigg
                                    takes us to the thrilling edge of scientific discoveries that explain why habits exist
                                    and how they can be changed. With penetrating intelligence and an ability to distill
                                    vast amounts of information into engrossing narratives, Duhigg brings to life a whole
                                    new understanding of human nature and its potential for transformation.<br><br><br></p>
                                 
                                 <p><strong>Leader:</strong>  Kera Coyer, 2012-2013 AVW Valencia Chapter Secretary<br>
                                    <strong>Date:</strong> Tuesday, June 3, 2014<br>
                                    <strong>Time:</strong> 3:30-5:00 pm<br>
                                    <strong>Location:</strong> Winter Park Campus, Room 246
                                 </p>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <p><img alt="Reading Circle - Women Leaders At Work" border="0" height="159" hspace="5" src="events-readingcircle-womenleaders.jpg" vspace="5" width="106"><a href="http://www.amazon.com/Women-Leaders-Work-Achieving-Ambitions-ebook/dp/B006LPJZIQ/ref=sr_1_1?ie=UTF8&amp;qid=1397438438&amp;sr=8-1&amp;keywords=Women+Leaders+at+work" target="_blank">Women Leaders at Work: Untold Tales of Women Achieving Their Ambitions by Elizabeth
                                       Ghaffari</a><br>Women Leaders at Work traces the personal life decisions taken by women who found
                                    ways to achieve greatness in their work. Each story is intriguing. But, collectively,
                                    the stories provide inspiration. They illustrate how real women of varied talents
                                    from varied backgrounds traversed quite different paths, seized opportunities presented
                                    in many guises, and found ways to achieve and to contribute to society.<br><br><br></p>
                                 
                                 <p><strong>Leader:</strong>  Grace Acevedo, 2014-2015 AVW Valencia Chapter President<br>
                                    <strong>Date:</strong> May 22, 2014<br>
                                    <strong>Time:</strong> 3:30-5:00 pm<br>
                                    <strong>Location:</strong> East Campus, 5-112
                                 </p>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <p><img alt="Reading Circle - Net Smart: How to Thrive Online" border="0" height="159" hspace="5" src="events-readingcircle-NetSmart.jpg" vspace="5" width="106"><a href="http://www.amazon.com/Net-Smart-How-Thrive-Online-ebook/dp/B007D5UP9G" target="_blank">Net Smart: How to Thrive Online by Howard Rhinegold &amp; Anthony Weeks</a><br>
                                    <strong>Date:</strong> Thursday, June 27, 2013<br>
                                    <strong>Time:</strong>  3:00 - 4:30pm<br>
                                    <strong>Location:</strong> Winter Park Campus, Room 108<br><br><br><br></p>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <p><img alt="Reading Circle - The Art and Spirit of Leadership" border="0" height="159" hspace="5" src="events-readingcircle-ArtSprirt.jpg" vspace="5" width="106"><a href="http://www.amazon.com/The-Spirit-Leadership-Judy-Brown/dp/1466910488" target="_blank"> The Art and Spirit of Leadership by Judy Brown</a><br>
                                    <strong>Date:</strong> Tuesday, May 28, 2013<br>
                                    <strong>Time:</strong> 1:00pmï¿½ 2:30pm<br>
                                    <strong>Location:</strong> West Campus, AHS 218<br><br><br><br></p>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/association-of-valencia-women/events.pcf">©</a>
      </div>
   </body>
</html>