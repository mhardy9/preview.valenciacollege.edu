<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Association of Valencia Women | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/association-of-valencia-women/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/association-of-valencia-women/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Association of Valencia Women</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/association-of-valencia-women/">Association Of Valencia Women</a></li>
               <li>Association of Valencia Women</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Resources</h3>
                        
                        
                        <div>
                           <p>
                              <a href="http://valencia.org" target="_blank"><img alt="Valencia Foundation" border="0" height="62" src="resources-vf_logo.png" width="200"></a><br>
                              <a href="http://valencia.org" target="_blank">Valencia Foundation</a> -  The Valencia Foundation offers <a href="documents/AVWBookScholarshipApplication.docx">AVW Valencia Chapter Book Scholarships</a> - Your AVW Executive Committee is excited to announce the Valencia Chapter AVW Book
                              Scholarship through the <a href="http://www.valencia.org">Valencia College Foundation</a>! Both current classes and upcoming semester courses are eligible for reimbursement
                              through the scholarship. <a href="documents/AVWBookScholarshipApplication.docx">Download your scholarship application here</a> or email <a href="mailto:aacww@valenciacollege.edu">aacww@valenciacollege.edu</a> &nbsp;with “Book Scholarship Application” in the subject line. An application will be
                              sent to you upon verification of your AVW Valencia Chapter membership. <br><br>
                              
                           </p>
                        </div>
                        
                        
                        <div>
                           <p>
                              <a href="http://www.league.org/league/conferences/nild.htm" target="_blank"><img alt="National Institute for Leadership Development" border="0" height="46" src="resources-NILD.jpg" width="350"></a><br>
                              <a href="http://www.league.org/league/conferences/nild.htm" target="_blank">The National Institute for Leadership Development</a> provides leadership development institutes, forums, conferences and workshops for
                              women community college executives, administrators, and faculty. Almost 5,000 people
                              have completed NILD programs since 1981. Ninety-seven percent of the participants
                              have advanced in their administrative careers. NILD has identified almost 400 women
                              CEOS and 94% of those have gone through one of the NILD programs. NILD leadership
                              philosophy ia 1) Leaders excel through collaborative, team efforts; 2) Many valuable
                              leadership styles exist and can co-exist; 3) Leadership must flourish and be actively
                              supported at every level within an organization; 4) Leaders are more effective when
                              they support &amp; encourage each other; and 5) A balance of intellectual, physical, social
                              &amp; spiritual attributes creates authentic successful leaders.<br><br></p>
                        </div>
                        
                        
                        <div>
                           <p> <a href="http://www.aacc.nche.edu/"><img alt="American Association of Community Colleges" border="0" height="85" src="resources-aacc.jpg" width="285"></a><br>
                              <a href="http://www.aacc.nche.edu/">The American Association of Community Colleges</a> is the primary advocacy organization for the nation's community colleges. The Association
                              represents more than 1,100 associate degree-granting institutions and some 10 million
                              students. Formed in 1920, AACC is a national voice for community colleges, which marked
                              their 100th year of service  to the nation in 2001. AACC is leading the celebration
                              of the colleges as they provide learning opportunities to their students and communities
                              and the nation.<br><br></p>
                        </div>
                        
                        
                        
                        <div>
                           
                           <p><a href="http://www.league.org/welcome.htm" target="_blank"><img alt="League for Innovation" border="0" height="70" src="resources-leagueinnovation.jpg" width="172"></a><br>
                              <a href="http://www.league.org/welcome.htm" target="_blank">The League for Innovation</a></p> is an international organization dedicated to catalyzing the community college movement.
                           They host conferences and institutes, develop Web resources, conduct efforts to make
                           a positive difference for students and  communities.<br><br>
                           
                        </div>
                        
                        
                        <div>
                           <p> <a href="http://www.myafchome.org/"><img alt="AFC Association of Florida Colleges" height="124" src="resources-afc-logo.gif" width="125"></a><br>
                              <a href="http://www.myafchome.org/">The Association of Florida Colleges</a> is the professional Association for <a href="http://www.fldoe.org/schools/higher-ed">Florida's 28 public community colleges</a>, their Boards, employees, retirees, and associates. The mission of the Association
                              is to actively promote, democratically represent, support, and serve the individual
                              <a href="http://www.myafchome.org/membership-information">members</a> and institutions in their endeavors to provide their students and the citizens of
                              Florida with the best possible comprehensive community college educational system
                              <br><br></p>
                        </div>
                        
                        
                        <div>
                           <p> <a href="http://www.affordablecolleges.com/resources/scholarships-for-women/"><img alt="AffordableColleges.com Logo" height="49" src="resources-affordable_colleges.png" width="251"></a><br>
                              <a href="http://www.affordablecolleges.com/resources/scholarships-for-women/">AffordableColleges.com</a> provides a scholarship guide for women. <br><br></p>
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/association-of-valencia-women/resources.pcf">©</a>
      </div>
   </body>
</html>