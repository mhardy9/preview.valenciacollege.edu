<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Association of Valencia Women | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/association-of-valencia-women/bylaws.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/association-of-valencia-women/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Association of Valencia Women</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/association-of-valencia-women/">Association Of Valencia Women</a></li>
               <li>Association of Valencia Women</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h3>By Laws</h3>
                        
                        <p>Please click on the ARTICLE below for details.</p>
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <h3>ARTICLE I: NAME</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The name of this chapter shall be the Association of Valencia Women known as AVW.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>ARTICLE II: PURPOSE</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The purpose of the Association shall be: </p>
                                 
                                 <ul>
                                    
                                    <li>To facilitate the development of comprehensive educational, career, and lifelong learning
                                       opportunities at Valencia
                                    </li>
                                    
                                    <li>To provide information, assistance, and support for Valencia educators and staff as
                                       they serve our students 
                                       
                                    </li>
                                    
                                    <li>To encourage and support professional development and advancement of Valencia women
                                       
                                    </li>
                                    
                                    <li>To support the mission, purpose and core values of Valencia College</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>  
                           
                           
                           
                           
                           <h3>ARTICLE III: MEMBERSHIP </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Section 1.</strong> &nbsp;&nbsp;Active membership is open to any person employed by or formerly employed by Valencia
                                    or who actively supports the mission of Valencia College. 
                                 </p>
                                 
                                 <p><strong>Section 2.</strong> &nbsp;Participation in AVW activities is limited to those making application and payment
                                    in full of any required annual dues. 
                                 </p>
                                 
                                 <p><strong>Section 3.</strong> Section 3. Each member shall be entitled to the rights and privileges of the Association
                                    of Valencia Women and shall be entitled to one vote on matters submitted to a vote
                                    of the membership.  A majority vote shall prevail. 
                                 </p>            
                                 
                              </div>
                              
                           </div>           
                           
                           
                           
                           
                           <h3>ARTICLE IV: MEETINGS OF MEMBERS</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Section 1.</strong> &nbsp;&nbsp;Meetings and/or activities for members shall be held no less frequently than three
                                    times per year. 
                                 </p>
                                 
                                 <p><strong>Section 2.</strong> &nbsp;&nbsp;One meeting shall be designated as the annual meeting. 
                                 </p>
                                 
                                 <p><strong>Section 3.</strong> &nbsp;&nbsp;The Marketing Coordinator shall inform members of meetings. 
                                 </p>
                                 
                              </div>
                              
                           </div>         
                           
                           
                           
                           
                           <h3>ARTICLE V: RECORDS</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Section 1.</strong> &nbsp;&nbsp;The Secretary shall keep meeting minutes. 
                                 </p>
                                 
                                 <p><strong>Section 2.</strong> &nbsp;&nbsp;The Treasurer shall keep the membership roll. 
                                 </p>
                                 
                                 <p><strong>Section 3.</strong> &nbsp;&nbsp;Any records of the organization may be inspected by any member at any time. 
                                 </p>
                                 
                              </div>
                              
                           </div>        
                           
                           
                           
                           
                           <h3>ARTICLE VI: FISCAL YEAR</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The fiscal year of this organization shall be July 1 through June 30. </p>
                                 
                              </div>
                              
                           </div>    
                           
                           
                           
                           
                           <h3>ARTICLE VII: OFFICERS</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Section 1.</strong> &nbsp;&nbsp;The officers of this organization, each of whom must be a member in good standing
                                    and a full time employee of the college, shall be President, President-Elect, Secretary,
                                    Treasurer, and Marketing Coordinator.
                                 </p>
                                 
                                 <p><strong>Section 2.</strong> &nbsp;&nbsp;Officers shall serve for a term of one year or until their successors have been
                                    duly elected and installed. No officers may serve more than two consecutive terms
                                    in the same office. 
                                 </p> 
                                 <p><strong>Section 3.</strong> &nbsp;&nbsp;The officers shall be elected at the annual meeting or by paper/electronic ballot
                                    as deemed appropriate by the officers of the organization. The term will begin on
                                    July 1. 
                                 </p> 
                                 
                                 <p><strong>Section 4.</strong> &nbsp;&nbsp;In the event that the office of President becomes vacant, the President-Elect shall
                                    succeed to the office. If a vacancy occurs in any other office, the President shall
                                    have the power to make an appointment to fill the remaining term. 
                                 </p> 
                                 
                                 <p><strong>Section 5.</strong> &nbsp;&nbsp;Attempts shall be made to establish diversity among the officers including diverse
                                    campus representation and categorical employment. 
                                 </p>     
                                 
                                 <p><strong>Section 6.</strong> &nbsp;&nbsp;The duties of the officers of the organization shall be as follows: 
                                 </p>
                                 
                                 <p><strong>The Past-President shall:</strong><br>
                                    .&nbsp; serve as an advisor to the President and President Elect; <br>
                                    .&nbsp; provide advice and leadership to the Board in governing the organization; <br>
                                    .&nbsp; serve as tie breaking vote for the Board. <br>         
                                    
                                 </p>
                                 
                                 <p><strong>The President shall:</strong><br>
                                    .&nbsp; serve as the presiding officer of the organization; <br>
                                    .&nbsp; act as the primary representative of the organization to the public; <br>
                                    .&nbsp; make appointments if necessary to fill vacancies in offices; <br>
                                    .&nbsp; call special meetings when necessary.<br>
                                    <br>
                                    <strong>The President-Elect shall:</strong><br>
                                    .&nbsp; assist the President in matters; <br>
                                    .&nbsp; succeed to the office of President; <br>
                                    .&nbsp; preside in the absence of the President;<br>
                                    .&nbsp; serve as Chair of Nominating Committee; <br>
                                    .&nbsp; serve as Parliamentarian.<br>
                                    <br>
                                    <strong>The Secretary shall:</strong><br>
                                    .&nbsp; record and distribute the minutes for the executive and membership meetings; <br>
                                    .&nbsp; maintain all necessary organizational records to include, but not limited to, meeting
                                    attendance, financial reports, minutes and annual reports.<br>
                                    <br>
                                    <strong>The Treasurer shall:</strong><br>
                                    .&nbsp; be responsible for the collection of applications and any required dues; <br>
                                    .&nbsp; be responsible for maintaining financials; <br>
                                    .&nbsp; prepare the annual report; <br>
                                    .&nbsp; keep the membership roll.<br>
                                    <br>
                                    <strong>The Marketing Coordinator shall:</strong><br>
                                    .&nbsp; market events to all members; <br>
                                    .&nbsp; communicate programs and services to the members; <br>
                                    .&nbsp; maintain the web presence and publications; <br>
                                    .&nbsp; coordinate AVW presence at College Events; <br>
                                    .&nbsp; manage outreach; <br>
                                    .&nbsp; notify membership of meetings and events at least 7 days prior; <br>
                                    .&nbsp; communicate with committee chairs. <br> 
                                 </p>
                                 
                              </div>
                              
                           </div>    
                           
                           
                           
                           
                           <h3>ARTICLE VIII: COMMITTEES</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Section 1.</strong> &nbsp;&nbsp;The Executive Board shall be a standing committee and shall: 
                                 </p>
                                 
                                 <p>.&nbsp; consist of the current officers and the immediate Past-President; <br>
                                    .&nbsp; meet no fewer than two times per year; <br>
                                    .&nbsp; recommend policies and procedures for consideration by the membership; <br>
                                    .&nbsp; establish procedures for budget development and approval;<br>
                                    .&nbsp; examine, modify if necessary, and adopt the organization's budget; <br>
                                    .&nbsp; make decisions on matters that do not significantly impact the organizationâ€™s
                                    membership.
                                 </p>
                                 
                                 <p><strong>Section 2.</strong> &nbsp;&nbsp;The Nominating Committee shall be an ad hoc committee and shall: 
                                 </p>
                                 
                                 <p>.&nbsp; be chaired by the President Elect; <br>
                                    .&nbsp; make its report to the President no later than 30 days prior to the election of
                                    officers. 
                                 </p>
                                 
                                 <p><strong>Section 3.</strong> &nbsp;&nbsp;Other committees shall be established as needed. 
                                 </p>
                                 
                              </div>
                              
                           </div>    
                           
                           
                           
                           
                           <h3>ARTICLE IX: ELECTIONS</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Section 1.</strong> &nbsp;&nbsp;Candidates nominated for office shall be elected by a majority of votes of the
                                    members who submit a written or electronic ballot. <br>
                                    <br>
                                    <strong>Section 2.</strong> &nbsp;&nbsp;Elections occur no later than May 30. 
                                 </p>
                                 
                                 <p><strong>Section 3.</strong> &nbsp;&nbsp;The results of the election shall be announced to the membership within 30 days
                                    of the election. 
                                 </p>
                                 
                                 <p><strong>Section 4.</strong> &nbsp;&nbsp;Elected officers shall assume their duties on July 1. 
                                 </p>
                                 
                              </div>
                              
                           </div>    
                           
                           
                           
                           
                           <h3>ARTICLE X: PARLIAMENTARY AUTHORITY</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>These bylaws shall govern the affairs of this organization, and ROBERT'S RULES OF
                                    ORDER, NEWLY REVISED (current edition) shall govern the proceedings of AVW in all
                                    cases. 
                                 </p>
                                 
                              </div>
                              
                           </div>    
                           
                           
                           
                           
                           <h3>ARTICLE XI: AMENDMENTS OF BYLAWS</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Section 1.</strong> &nbsp;&nbsp;The amendment or repeal of the bylaws will be decided by two-thirds (2/3) vote
                                    of the members submitting a written or electronic ballot. 
                                 </p>
                                 
                                 <p><strong>Section 2.</strong> &nbsp;&nbsp;Amendments of the bylaws or repeal of the bylaws may be proposed by individual
                                    members of the organization. 
                                 </p>
                                 
                                 <p><strong>Section 3.</strong> &nbsp;&nbsp;The proposed amendment or revision must be written and disseminated to the membership
                                    and include two full weeks for the collection of the vote. 
                                 </p>
                                 
                              </div>
                              
                           </div>            
                           
                           
                           
                           
                        </div>    
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/association-of-valencia-women/bylaws.pcf">©</a>
      </div>
   </body>
</html>