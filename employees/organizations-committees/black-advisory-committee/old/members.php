<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Membership  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/black-advisory-committee/old/members.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/black-advisory-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/black-advisory-committee/">Black Advisory Committee</a></li>
               <li><a href="/employees/organizations-committees/black-advisory-committee/old/">Old</a></li>
               <li>Membership </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>
                              Community Members
                              
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/05.jpg" alt="Deloris Batson" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Deloris Batson
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/06.jpg" alt="Vernard Batson" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Vernard Batson
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Dr. Felecia Boyd" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Dr. Felecia Boyd
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/07.jpg" alt="Vicki Brooks" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Vicki Brooks
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/12.jpg" alt="Leona Burns" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Leona Burns
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/09.jpg" alt="Joe Caldwell" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Joe Caldwell
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/14.jpg" alt="Marion Campbell" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Marion Campbell
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/15.jpg" alt="Marva Carter" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Marva Carter
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Steve Currie, III" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Steve Currie, III
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/11.jpg" alt="Kevin Gaston" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Kevin Gaston
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/03.jpg" alt="Chester Glover" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Chester Glover
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Stephen Graham" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Stephen Graham
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/01.jpg" alt="Arthur Jarvis" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Arthur Jarvis
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/18.jpg" alt="Monica May" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Monica May
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Reginald McGill" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Reginald McGill
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/13.jpg" alt="Denise Mose" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Denise Mose
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Shanti Persaud" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Shanti Persaud
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Darryl Smith, Esq." class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Darryl Smith, Esq.
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Eric Smith" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Eric Smith
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/10.jpg" alt="John Stover" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             John Stover
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/04.jpg" alt="Maggie Wells" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Maggie Wells
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Dr. E. Laverne Williams" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Dr. E. Laverne Williams
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>
                              Valencia College Representatives
                              
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Dr. Amy Bosley" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Dr. Amy Bosley
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/16.jpg" alt="Dr. Mike Bosley" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Mike Bosley
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Charles Davis" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Charles Davis
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Kevin Rushing" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Kevin Rushing
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Barbara Shell" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Barbara Shell
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/teacher_4_thumb.jpg" alt="Lisa Shrestha" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Lisa Shrestha
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/17.jpg" alt="Dr. Sanford Shugart" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Dr. Sanford Shugart
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                    <li><a href="#0">
                                          
                                          <figure>
                                             <img src="/_resources/img/employees/organizations-committees/black-advisory-committee/02.jpg" alt="Deborah Xavier" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>
                                             Deborah Xavier
                                             
                                          </h5>
                                          <i class="pe-7s-angle-right-circle"></i></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               
               
               
               
               
               <main role="main">
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
               </main>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/black-advisory-committee/old/members.pcf">©</a>
      </div>
   </body>
</html>