<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Meeting Schedule  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/black-advisory-committee/old/meeting-schedule.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/black-advisory-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/black-advisory-committee/">Black Advisory Committee</a></li>
               <li><a href="/employees/organizations-committees/black-advisory-committee/old/">Old</a></li>
               <li>Meeting Schedule </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>
                              Meeting Schedule
                              
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              The full membership of the BAC holds five meetings annually and the Annual Incentive
                              Awards during the
                              month of December.
                              
                           </p>
                           
                           <p align="center">
                              <strong>September 2016</strong><br> Regular meeting-Wednesday, September 28, 2016 6:00pm<br> District
                              Office 2nd floor-1768 Park Center Drive, Orlando FL 32835
                              
                           </p>
                           
                           <p align="center">
                              <strong>November 2016</strong><br> Regular meeting-Wednesday, November 16, 2016 6:00pm<br> District
                              Office 5th floor-1768 Park Center Drive, Orlando FL 32835
                              
                           </p>
                           
                           <p align="center">
                              <strong>December 2016</strong><br> 40th Annual Incentive Awards Reception<br> Friday, December 2, 2016,
                              West Campus, SEC bldg.8-111
                              
                           </p>
                           
                           <p align="center">
                              <strong>February 2017</strong><br> Regular meeting-Wednesday, February 22, 2017, 6:00pm<br> District
                              Office 5th floor-1768 Park Center Drive, Orlando FL 32835
                              
                           </p>
                           
                           <p align="center">
                              <strong>April 2017</strong><br> Regular meeting-Wednesday, April 19, 2017, 6:00pm<br> District Office
                              5th floor-1768 Park Center Drive, Orlando FL 32835
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               
               
               
               
               
               <main role="main">
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
               </main>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/black-advisory-committee/old/meeting-schedule.pcf">©</a>
      </div>
   </body>
</html>