<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sub-Committees  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/black-advisory-committee/old/sub-committees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/black-advisory-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/black-advisory-committee/">Black Advisory Committee</a></li>
               <li><a href="/employees/organizations-committees/black-advisory-committee/old/">Old</a></li>
               <li>Sub-Committees </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>
                              Sub-Committees
                              
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>
                                       
                                       <h4>
                                          Incentive Awards Committee
                                          
                                       </h4>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td><strong>Chair:</strong></td>
                                    
                                    <td>Marva Carter</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Dr. Amy Bosley</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Leona Burns</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Joe Caldwell</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Margie Wells</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>
                              &nbsp;
                              
                           </p>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>
                                       
                                       <h4>
                                          Bylaws Committee
                                          
                                       </h4>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td><strong>Chair:</strong></td>
                                    
                                    <td>Darryl Smith Esq.</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>
                              &nbsp;
                              
                           </p>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>
                                       
                                       <h4>
                                          Membership Committee
                                          
                                       </h4>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td><strong>Chair:</strong></td>
                                    
                                    <td>Marva Carter</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Joe Caldwell</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Marion Campbell</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Reginald McGill</td>
                                    
                                 </tr>
                                 
                                 <tr align="left" valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>
                                       
                                       <p>
                                          Margie Wells
                                          
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/black-advisory-committee/old/sub-committees.pcf">©</a>
      </div>
   </body>
</html>