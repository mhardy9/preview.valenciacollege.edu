<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>The Black Advisory Committee | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/black-advisory-committee/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/black-advisory-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>The Black Advisory Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li>Black Advisory Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <p>The Black Advisory Committee (BAC) was established in 1970 and 
                           exists to support the overall goals of Valencia College. 
                        </p>
                        
                        <p><strong> </strong><strong>BAC Purpose Statement </strong></p>
                        
                        <p>The Black Advisory Committee serves as a viable liaison between 
                           Valencia College and the Black community by providing 
                           advice and direction to Valencia 's Board of Trustees and President 
                           on issues impacting Black students, administration, faculty, staff 
                           and businesses to fulfill the College's learning-centered mission. 
                        </p>
                        
                        <p>To provide financial support to the BAC's efforts to brighten the 
                           lives of students at Valencia College, please make your 
                           contribution payable to:
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">    
                                    
                                    <h4>Valencia Foundation / Black Advisory Committee </h4>
                                    
                                    <p>To make a donation, click on the link below and  select Eula P. Jenkins </p>
                                    
                                    <p><a href="../finaid/Scholarship_bulletin.cfm-utm_source=RECRUITMENT-BROCHURE&amp;utm_medium=PRINT&amp;utm_content=16EMC011&amp;utm_campaign=EMC.html">http://valenciacollege.org/scholarships</a></p>
                                    
                                    <p>or</p>
                                    
                                    <p><em>mail a check to: </em></p>
                                    
                                    <p>Valencia  College <br>
                                       P. O. Box 3028 <br>
                                       Orlando , FL 32802 
                                    </p>
                                    
                                    <p><a href="mailto:Stephen.Graham@orlandohealth.com">Contact The Black Advisory Committee</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    
                                    <div> 
                                       
                                       
                                       <div>
                                          
                                          
                                          
                                          <div>       
                                             
                                             <div>
                                                <img alt="Arthur Jarvis" height="310" src="01.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Arthur Jarvis</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Chester Glover" height="324" src="03.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Chester Glover</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Deloris Batson" height="323" src="05.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Deloris Batson</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Joe Caldwell" height="295" src="09.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Joe Caldwell</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="John Stover" height="324" src="10.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>John Stover</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Vernard Batson" height="324" src="Rachel-Luce-Hitt.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Vernard Batson</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Kevin Gaston" height="324" src="11.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Kevin Gaston</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Leona Burns" height="324" src="12.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Leona Burns</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Marion Campbell" height="324" src="14.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Marion Campbell</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Marva Carter" height="324" src="15.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Marva Carter</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Name Caption" height="324" src="16.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Mike Bosley</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Dr. Sandford Shugart" height="320" src="17.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Dr. Sandford Shugart</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Monica May" height="321" src="24.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Monica May</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Denise Mose" height="321" src="25.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Denise Mose</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Maggie Wells" height="300" src="MaggieWells5.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Maggie Wells</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <img alt="Vicki Brooks" height="299" src="VivkiBrooks6.jpg" width="215">
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <h3>Vicki Brooks</h3>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       <div> Use the arrows to scroll the photos.</div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/black-advisory-committee/index.pcf">©</a>
      </div>
   </body>
</html>