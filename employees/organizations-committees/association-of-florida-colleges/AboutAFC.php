<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Association of Florida Colleges | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/association-of-florida-colleges/AboutAFC.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/association-of-florida-colleges/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Association of Florida Colleges</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/association-of-florida-colleges/">Association Of Florida Colleges</a></li>
               <li>Association of Florida Colleges</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>  
                        
                        
                        <h2>About AFC</h2>
                        
                        <h3><strong><font color="#000066">Mission</font></strong></h3>
                        
                        <p>The  Association of Florida  Colleges is the professional Association for <a href="http://www.fldoe.org/cc/colleges.asp">Florida's 28 public community colleges</a>, their Boards, employees, retirees, and associates. The mission of the Association
                           is to actively promote, democratically represent, support, and serve the individual
                           <a href="http://www.myafchome.org/membership-information">members</a> and institutions in their endeavors to provide their students and the citizens of
                           Florida with the best possible comprehensive community college educational system.
                           
                        </p>
                        
                        <h3><strong><font color="#000066">History</font></strong></h3>
                        
                        <p>The Association was organized in 1949 as the Florida Association of Public Junior
                           Colleges by the presidents of Florida's first four public junior colleges. Its mission
                           was to unite the colleges for the purposes of helping the Florida Legislature understand
                           the junior college mission and to advocate for Florida's public junior colleges in
                           the development of the state's long-range plan for higher education.
                        </p>
                        
                        <p>Since 1949, the Association's mission and purposes have grown in scope as has the
                           Association itself. In 1971, the Association became the Florida Association of Community
                           Colleges. &nbsp;The Association changed its name to the Association of Florida Colleges
                           in 2010. &nbsp;It has since become the most inclusive higher education organization serving
                           any college system in the nation. The AFC is the only such Association which represents
                           all employees, presidents, and trustees associated with a college system.&nbsp;Today, all
                           28 of the state's colleges support the work of the Association through institutional
                           dues as do nearly 8500 regular and retired members through the sharing of their talents,
                           time and energy.
                        </p>
                        
                        <h3><strong><font color="#000066">Organization</font></strong></h3>
                        
                        <p>The Association is guided by Bylaws and organized through an network of College&nbsp;<a href="http://www.myafchome.org/chapters">Chapters</a>, <a href="http://www.myafchome.org/commissions">Commissions</a>, <a href="http://www.myafchome.org/regions">Regions</a>. &nbsp;The AFC is overseen by an&nbsp;Executive&nbsp;Committee&nbsp;and a <a href="http://www.myafchome.org/board-of-directors">Board of Directors</a>. A full-time <a href="http://www.myafchome.org/staff">staff</a>&nbsp;of six administers the day-to-day operations, maintains membership records and coordinates
                           <a href="http://www.myafchome.org/publications--communiations">communications</a> and <a href="http://www.myafchome.org/professional-development">professional development</a> opportunities.
                        </p>            
                        
                        
                        <h3><strong><font color="#000066">What are the benefits of joining AFC? </font></strong></h3>
                        
                        <div>
                           
                           <p><strong>Advocacy</strong> –a full-time staff representing your interests in Tallahassee. 
                           </p>
                           
                           <p><strong>Recognition</strong> - exmplary practice programs, awards, and honors. 
                           </p>
                           
                           <p><strong>Information</strong> - publications, web sites, and e-mail updates to keep you informed. 
                           </p>
                           
                           <p><strong>Professional Development</strong> - workshops, seminars, and an annual convention geared toward your interests. 
                           </p>
                           
                           <p><strong>Networking</strong> - sharing ideas, making contacts, and building partnerships with peers across the
                              state. 
                           </p>
                           
                           <p><strong>Benefits and Services</strong> - Discount programs "for members only."
                           </p>
                           
                           <p><strong>Fellowship</strong> - camaraderie and friendships that last a lifetime. 
                           </p>
                           
                           <p><strong>For More Information visit <a href="http://www.myafchome.org" target="_blank">www.myafchome.org </a></strong></p>
                           
                           <p><strong><a href="documents/AFCChapterppt2013.pptx.html">Why Join AFC at Valencia? </a></strong></p>
                           
                           <p><a href="#top">TOP</a></p>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/association-of-florida-colleges/AboutAFC.pcf">©</a>
      </div>
   </body>
</html>