<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Association of Florida Colleges | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/association-of-florida-colleges/AFCBoard.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/association-of-florida-colleges/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Association of Florida Colleges</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/association-of-florida-colleges/">Association Of Florida Colleges</a></li>
               <li>Association of Florida Colleges</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>  
                        
                        
                        <h2>AFC Valencia Chapter Board Members</h2>
                        
                        
                        <p>President – Vacant </p>
                        
                        <p>President - Elect – TBA </p>
                        
                        <p>Secretary – Patty Nicholas, <a href="mailto:pnicholas@valenciacollege.edu">pnicholas@valenciacollege.edu</a>, DO, x-3465
                        </p>
                        
                        <p>Treasurer – Sue Fagan, <a href="mailto:patlee@valenciacollege.edu">kfagan4@valenciacollege.edu</a>, DO, x-3309
                        </p>
                        
                        <p>Membership - Vacant </p>
                        
                        <p>Historian – Andrea Bealler, <a href="mailto:alove8@valenciacollege.edu">abealler@valenciacollege.edu</a>, East, x-2932
                        </p>
                        
                        <p>Marketing &amp; Social Media Coordinator - Grenka Fletcher, <a href="mailto:sjackson59@valenciacollege.edu">gbajramoski@valenciacollege.edu</a> , WPC, x-6814
                        </p>
                        
                        <p>District Office – Joe Nunes, <a href="mailto:jnunes@valenciacollege.edu">jnunes@valenciacollege.edu</a>, DO, x-8007 
                        </p>
                        
                        <p>Osceola Campus Representative – Sarah Dockray, <a href="mailto:adelneky@valenciacollege.edu">sdockray@valenciacollege.edu</a> x-4156
                        </p>
                        
                        <p>West Campus Representative – Marsha Clarke-Ferguson, <a href="mailto:lnelson@valenciacollege.edu">mclarkeferguson@valenciacollege.edu</a>, x-1069
                        </p>
                        
                        <p>Winter Park Representative – vacant </p>
                        
                        <p>East Campus Representative – Johnny Aplin, <a href="mailto:sboles-melvin@valenciacollege.edu">japlin@valenciacollege.edu</a> , x-2019
                        </p>
                        
                        <p>District Office Representative - vacant </p>
                        
                        <p>Member at large Representative - Chaz Davis, <a href="mailto:cdavis73@valenciacollege.edu">cdavis73@valenciacollege.edu</a>, x-5607 
                        </p>
                        
                        <p>Lake Nona Representative - vacant </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/association-of-florida-colleges/AFCBoard.pcf">©</a>
      </div>
   </body>
</html>