<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Operations Leadership Team | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/operational-leadership-team/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/operational-leadership-team/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Operations Leadership Team</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li>Operational Leadership Team</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <link href="../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           <div> 
                              
                              <div>
                                 <img alt="Featured 1" border="0" height="260" src="olt-banner-770x260.jpg" width="770">
                                 
                              </div>
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2>Charge</h2>
                        
                        <p><u></u>The<strong> Operations Leadership Team (OLT)</strong> meets to research, design, and implement solutions to address major operational changes
                           that impact how the college manages its day to day functions, communication regarding
                           operations changes that affect the college, and planning/training for operational
                           changes that cross impact college locations.
                        </p>
                        
                        <p><u><strong>Leadership 2017-18<br>
                                 </strong></u>      
                        </p>
                        
                        <p>Loren Bender, Chair/Convener<br>
                           VP, Business Operations &amp; Finance
                        </p>
                        
                        <p>Michelle Sever, Co-Chair<br>
                           Director of HR Policy and Compliance Programs 
                        </p>
                        
                        <p>Terry Allcorn, Co-Chair<br>
                           Dean of Business &amp; Hospitality
                        </p>
                        
                        <p>Kari Makepeace, Council Coordinator<br>
                           Academic Planning &amp; Support
                        </p>
                        
                        <p><u><strong>Past Co-Chairs<br>
                                 </strong></u><br>
                           2016-2017: Jennifer Page, Director of Employment &amp; Onboarding <br>
                           2015-2016: Jackie Lasch, AVP Financial Services<br>
                           2014-2015: Roger Corriveau, Manager of Campus Operations
                        </p>
                        
                        
                        <p><strong>Better decisions. Greater trust.</strong></p>   
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/operational-leadership-team/index.pcf">©</a>
      </div>
   </body>
</html>