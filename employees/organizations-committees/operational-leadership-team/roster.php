<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>2017-2018 Membership | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/operational-leadership-team/roster.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/operational-leadership-team/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>2017-2018 Membership</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/operational-leadership-team/">Operational Leadership Team</a></li>
               <li>2017-2018 Membership</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <link href="../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           <div> 
                              
                              <div>
                                 <img alt="Featured 1" border="0" height="260" src="olt-banner-770x260.jpg" width="770">
                                 
                              </div>
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>                      
                                    <p>Member Name </p>
                                    
                                 </div>
                                 
                                 <div>                      
                                    <p>Job Title </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Terry Allcorn</div>
                                 
                                 <div>Co-Chair &amp; Dean, Business &amp; Hospitality - District Office</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Michelle Sever</div>
                                 
                                 <div>Co-Chair &amp; Director, HR Policy &amp; Compliance - District Office</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Loren Bender </div>
                                 
                                 <div>Convener &amp; VP, Business Operations &amp; Finance  - District Office</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Daniel Barkowitz </div>
                                 
                                 <div>AVP Financial Aid &amp; Vet Affairs</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Joe Livingston </div>
                                 
                                 <div>AVP, Human Resources - District Office</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Traci Thornton </div>
                                 
                                 <div>AVP, Marketing &amp; Strategic Communication  - District Office</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Paul Rooney </div>
                                 
                                 <div>AVP, Operations - District Office</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Sonya Joseph </div>
                                 
                                 <div>AVP, Student Affairs - Lake Nona Campus</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Judy Jackson </div>
                                 
                                 <div>Director, Business Operations - Continuing Education - West Campus </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jamie Rost</div>
                                 
                                 <div>Interim AVP, Enterprise Solutions - West Campus</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Rhonda Ulmer </div>
                                 
                                 <div>Managing Director, Procurement - District Office</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Drew Smith </div>
                                 
                                 <div>Operations Manager - Continuing Education - West Campus </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Roger Corriveau</div>
                                 
                                 <div>Operations Manager - East Campus </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jonathan Hernandez</div>
                                 
                                 <div> Manager - Lake Nona Campus </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jasmin Cruz </div>
                                 
                                 <div> Operations Manager - Osceola Campus </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Neisa Farley </div>
                                 
                                 <div>Operations Manager - West Campus</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Mike Lergier</div>
                                 
                                 <div>Operations Manager- Winter Park Campus </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Joe Lopez </div>
                                 
                                 <div>Public Safety Facilities Specialist - School of Public Safety </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>Better decisions. Greater trust.</strong></p>  
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/operational-leadership-team/roster.pcf">©</a>
      </div>
   </body>
</html>