<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Meeting Summaries | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/operational-leadership-team/summaries.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/operational-leadership-team/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Meeting Summaries</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/operational-leadership-team/">Operational Leadership Team</a></li>
               <li>Meeting Summaries</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <link href="../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           <div> 
                              
                              <div>
                                 <img alt="Featured 1" border="0" height="260" src="olt-banner-770x260.jpg" width="770">
                                 
                              </div>
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>2017</p>
                        
                        <p><a href="documents/OLT-Meeting-Summary-6-6-17.pdf" target="_blank">Operations Leadership Team Meeting Summary - 6/6/17</a></p>
                        
                        <p><a href="documents/OLTMeetingSummary4417.pdf" target="_blank">Operations Leadership Team Meeting Summary - 4/4/17 </a></p>
                        
                        <p><a href="documents/OLTMeetingSummary2717.pdf" target="_blank">Operations Leadership Team Meeting Summary - 2/7/17</a> 
                        </p>
                        
                        <p>2016</p>
                        
                        <p><a href="documents/OLTMeetingSummary11116.pdf" target="_blank">Operations Leadership Team Meeting Summary - 11/1/16</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary9616.pdf" target="_blank">Operations Leadership Team Meeting Summary - 9/6/16</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary8216.pdf" target="_blank">Operations Leadership Team Meeting Summary - 8/2/16</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary6716.pdf" target="_blank">Operations Leadership Team Meeting Summary - 6/7/16</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary5316.pdf" target="_blank">Operations Leadership Team Meeting Summary - 5/3/16 </a></p>
                        
                        <p><a href="documents/OLTMeetingSummary2216.pdf" target="_blank">Operations Leadership Team Meeting Summary - 2/2/16</a></p>
                        
                        <p>2015</p>
                        
                        <p><a href="documents/OLTMeetingSummary12.1.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 12/1/15</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary10615.pdf" target="_blank">Operations Leadership Team Meeting Summary - 10/6/15</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary9.1.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 9/1/15 </a></p>
                        
                        <p><a href="documents/OLTMeetingSummary8415.pdf" target="_blank">Operations Leadership Team Meeting Summary - 8/4/15</a></p>
                        
                        <p><a href="documents/OLTMeetingSummary6215.pdf" target="_blank">Operations Leadership Team Meeting Summary - 6/2/15</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary4.7.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 4/7/15 </a></p>
                        
                        <p><a href="documents/OLTMeetingSummary2.3.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 2/3/15 </a></p>
                        
                        <p><a href="documents/OLTMeetingSummary1.6.15.pdf" target="_blank">Operations Leadership Team Meeting Summary - 1/6/15</a> 
                        </p>
                        
                        <p>2014</p>
                        
                        <p><a href="documents/OLTMeetingSummary11.4.14.pdf" target="_blank">Operations Leadership Team Meeting Summary - 11/4/14</a> 
                        </p>
                        
                        <p><a href="documents/OLTMeetingSummary10.7.14.pdf" target="_blank">Operations Leadership Team Meeting Summary - 10/7/14</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/operational-leadership-team/summaries.pcf">©</a>
      </div>
   </body>
</html>