<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Voter Lists  | Valencia College</title>
      <meta name="Description" content="About the College Curriculum Committee">
      <meta name="Keywords" content="committee, curriculum, voters, schedules, meetings, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/voter-lists.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>Voter Lists </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        Voter Lists
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <div class="indent_title_in">
                        
                        <table class="table table table-striped cart-list add_bottom_30">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col"> Group</th>
                                 
                                 <th scope="col"> Prefixes</th>
                                 
                                 <th scope="col"> Division</th>
                                 
                                 <th scope="col"> Year</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Radiography-DONE.doc&amp;">A.S. Radiography</a></th>
                                 
                                 <td>HSA, RTE, RADI</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/RespiratoryCare.doc&amp;">A.S. Respiratory Care</a></th>
                                 
                                 <td>RET, RESP</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/CardiopulmonaryScienceBS-Done.doc&amp;">B. S. Cardiopulmonary
                                       Sciences</a></th>
                                 
                                 <td>BSCS</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="documents/CardiovascularTechnology-Done.doc&amp;">Cardiovascular Technology</a>
                                    
                                 </th>
                                 
                                 <td>CVT, CARV</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/DentalHygiene-1617.doc&amp;">Dental Hygiene</a></th>
                                 
                                 <td>DEH, DES, DENT</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/DiagnosticMedicalSonography-1617.doc&amp;">Diagnostic Medical
                                       Sonography</a></th>
                                 
                                 <td>SON, SONO</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/EMSParamedic-1617.doc&amp;">EMS Paramedic-1617</a></th>
                                 
                                 <td>EMS, EMER</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/HealthInformationTechnology-1617.doc&amp;">Health Information
                                       Technology</a></th>
                                 
                                 <td>HIM, HITE</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/RadiologicandImagingSciencesBS-DONE.doc&amp;">Radiologic and Imaging
                                       Sciences (A.S. to B.S. Degree)</a></th>
                                 
                                 <td>HSA, RTE, RADI</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Nursing-1617.doc&amp;">Nursing</a></th>
                                 
                                 <td>NUR, NURS</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Nutrition-done.doc&amp;">Nutrition</a></th>
                                 
                                 <td>HUN, NUTR</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="file:///S%7C/Academic%20Affairs-Common/Curriculum%20&amp;%20Assessment/Voter%20Lists/Voter%20Lists%202016-17/Business/Accounting%201617.doc&amp;">Accounting</a>
                                    
                                 </th>
                                 
                                 <td>ACG, APA, TAX, ACCT</td>
                                 
                                 <td>Business</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Business2016updatesdone.doc&amp;">Business</a></th>
                                 
                                 <td>GEB, MAN, MAR, MKA, MNA, RMI, SBM, BUSA</td>
                                 
                                 <td>Business</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="file:///S%7C/Academic%20Affairs-Common/Curriculum%20&amp;%20Assessment/Voter%20Lists/Voter%20Lists%202016-17/Business/Culinary%20Management-done.doc&amp;">Culinary
                                       Management</a></th>
                                 
                                 <td>FOS, FSS, CULI</td>
                                 
                                 <td>Business</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="file:///S%7C/Academic%20Affairs-Common/Curriculum%20&amp;%20Assessment/Voter%20Lists/Voter%20Lists%202016-17/Business/Finance%201617.doc&amp;">Finance</a>
                                    
                                 </th>
                                 
                                 <td>FIN, FINA</td>
                                 
                                 <td>Business</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Hospitality-done.doc&amp;">Hospitality</a></th>
                                 
                                 <td>HFT, HOSP</td>
                                 
                                 <td>Business</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/OfficeAdministration-DONE.doc&amp;">Office Administration</a></th>
                                 
                                 <td>OST, OFST</td>
                                 
                                 <td>Business</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/RealEstate-1617.doc&amp;">Real Estate</a></th>
                                 
                                 <td>REE, REAL</td>
                                 
                                 <td>Business</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/EAP-1617.doc&amp;">English for Academic Purposes</a></th>
                                 
                                 <td>EAP, ENGP</td>
                                 
                                 <td>Communications</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/English-1617.doc&amp;">English</a></th>
                                 
                                 <td>AML, CRW, ENC, ENG, ENL, LIT, ENGL</td>
                                 
                                 <td>Communications</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Journalism-1617.doc&amp;">Journalism</a></th>
                                 
                                 <td>JOU, MMC, PUR, JOUR</td>
                                 
                                 <td>Communications</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Reading-1617.doc&amp;">Reading</a></th>
                                 
                                 <td>REA, READ</td>
                                 
                                 <td>Communications</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Speech-1617.doc&amp;">Speech</a></th>
                                 
                                 <td>SPC, SPCH</td>
                                 
                                 <td>Communications</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="&amp;">Criminal Justice Institutue</a></th>
                                 
                                 <td>CJD, CJK, CRJI</td>
                                 
                                 <td>Criminal Justice Institutue</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="file:///S%7C/Academic%20Affairs-Common/Curriculum/Voter%20Lists/Voter%20Lists%202015-16/Engineering/Architecture-.doc&amp;">Architecture</a>
                                    
                                 </th>
                                 
                                 <td>ARC, ARCH</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/BuildingConstructionTechnology-done.doc&amp;">Building Construction
                                       Technology</a></th>
                                 
                                 <td>BCN, BCT, BLDG</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/CivilSurveyingEngTechnology-done.doc&amp;">Civil/Surveying
                                       Engineering Technology</a></th>
                                 
                                 <td>SUR, CIVL</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/DraftingandDesignTechnology-done.doc&amp;">Drafting &amp; Design
                                       Technology</a></th>
                                 
                                 <td>ETD, MTB (except MTB 1329 and MTB 2321), DRAF</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="documents/CET2112CET2113CET21231617.doc&amp;">CET 2112, CET 2113, CET 2123</a>
                                    
                                 </th>
                                 
                                 <td>ELEN</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/CETLowerLevelCourses1617.doc&amp;">CET Lower Level Courses</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/CETUpperLevelCourses1617.doc&amp;">CET Upper Level Courses</a></th>
                                 
                                 <td>ALL CET 3000 &amp; 4000, ECET</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/CTS2361C.doc&amp;">CTS 2361C</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/EETLowerLevelCourses1617.doc&amp;">EET Lower Level Courses</a></th>
                                 
                                 <td>ALL EET 1000 &amp; 2000, ELEN</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/EETUpperLevelCourses1617.doc&amp;">EET Upper Level Courses</a></th>
                                 
                                 <td>ALL EET 3000 &amp; 4000, ECET</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/EGNLowerLevelCourses.doc&amp;">EGN Lower Level Courses</a></th>
                                 
                                 <td>ALL EGN 1000&amp; 2000, ENGE</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/EGNUpperLevelCourses1617.doc&amp;">EGN Upper Level Courses</a></th>
                                 
                                 <td>ALL EGN 3000 &amp; 4000, ECET</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ESTLowerLevelCourses.doc&amp;">EST Lower Level Courses</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ESTUpperLevelCourses.doc&amp;">EST Upper Level Courses</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ETIUpperLevelCourses1617.doc&amp;">ETI Upper Level Courses</a></th>
                                 
                                 <td>ALL ETI 3000 &amp; 4000, ECET</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ETPLowerLevelCourses1617.doc&amp;">ETP Lower Level Courses</a></th>
                                 
                                 <td>ALL ETP 1000 &amp; 2000, ELEN</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ETPUpperLevelCourses1617.doc&amp;">ETP Upper Level Courses</a></th>
                                 
                                 <td>ALL ETP 3000 &amp; 4000, ECET</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ETSLowerLevelCourses1617.doc&amp;">ETS Lower Level Courses</a></th>
                                 
                                 <td>ALL ETS 1000 &amp; 2000, ELEN</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ETSUpperLevelCourses1617.doc&amp;">ETS Upper Level Courses</a></th>
                                 
                                 <td>ALL ETS 3000 &amp; 4000, ECET</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/MTB1329C.doc&amp;">MTB 1329</a></th>
                                 
                                 <td>ELEN</td>
                                 
                                 <td>Engineering</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/MTB2321.doc&amp;">MTB 2321C</a></th>
                                 
                                 <td></td>
                                 
                                 <td></td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Arabic-DONE.docx&amp;">Arabic</a></th>
                                 
                                 <td>ARA, ARAB</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Chinese-done.docx&amp;">Chinese</a></th>
                                 
                                 <td>CHI, CHIN</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/French-done.doc&amp;">French</a></th>
                                 
                                 <td>FRE, FREN</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/German-done.doc&amp;">German</a></th>
                                 
                                 <td>GER, GERM</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Italian-done.doc&amp;">Italian</a></th>
                                 
                                 <td>ITA, ITAL</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Japanese-done.docx&amp;">Japanese</a></th>
                                 
                                 <td>JPN, JAPA</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Latin-done.doc&amp;">Latin</a></th>
                                 
                                 <td>LAT, LATN</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Portuguese-done.doc&amp;">Portuguese</a></th>
                                 
                                 <td>POR, PORT</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Spanish-done.doc&amp;">Spanish</a></th>
                                 
                                 <td>SPN, SPAN</td>
                                 
                                 <td>Foreign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/HealthFitnessandPE-1617.doc&amp;">Health, Health &amp; Fitness, and
                                       PE</a></th>
                                 
                                 <td>HLP, PEL, PEM, PEN, PEO, PET, HEFT</td>
                                 
                                 <td>Health, Health and Fitness, and PE</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/HSC1100HSC1180HSC1400HSC2100HSC2400CHEFT1617done.doc&amp;">HSC 1100,
                                       HSC 1180, HSC 1400, HSC 2100, HSC 2400C HEFT</a></th>
                                 
                                 <td>HEFT</td>
                                 
                                 <td>Health, Health and Fitness, and PE</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="file:///S%7C/Academic%20Affairs-Common/Curriculum%20&amp;%20Assessment/Voter%20Lists/Voter%20Lists%202016-17/Health%20Sciences/Health%20Sciences1617.doc&amp;">Health
                                       Sciences</a></th>
                                 
                                 <td>HSC (except 1100, 1180, 1400, 2100, 2400C), HELR</td>
                                 
                                 <td>Health Related</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/AfricanAmericanHumanities1617done.doc&amp;">African American
                                       Humanities</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ArtHistory1617.doc&amp;">Art History</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/AsianHumanities1617done.docx&amp;">Asian Humanities</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Humanities-1617.doc&amp;">Humanities</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/LatinAmericanHumanities1617done.doc&amp;">Latin American
                                       Humanities</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/MiddleEasternHumanities1617done.docx&amp;">Humanities - Middle
                                       Eastern</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Philosophy-1617.doc&amp;">Philosophy</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Religion-1617.doc&amp;">Religion</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/IDHVoterList1617.doc&amp;">Interdisciplinary Studies Honors</a></th>
                                 
                                 <td>IDH, INTE</td>
                                 
                                 <td>Humanities</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/CompInfoTechCompProgandAnalysis.doc&amp;">Comp Info Tech &amp; Comp Prog
                                       and Analysis</a></th>
                                 
                                 <td>CGS, CIS, COP 1000 AND 2000 Level Course, COT, CTS, COMP</td>
                                 
                                 <td>Information Technology</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/COPUpperLevelCourses.doc&amp;">COP Upper Level Courses</a></th>
                                 
                                 <td></td>
                                 
                                 <td>Information Technology</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/LibraryScience1617.doc&amp;">Library Science</a></th>
                                 
                                 <td>LIS, LIBS</td>
                                 
                                 <td>Library Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Mathematics-1617.docx&amp;">Mathematics</a></th>
                                 
                                 <td>MAC, MAP, MAS, MAT, MGF, MHF, STA, MATH</td>
                                 
                                 <td>Math and Statistics</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/MilitaryScience.doc&amp;">Military Science</a></th>
                                 
                                 <td>MSL, MLSC</td>
                                 
                                 <td>Military Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Psychology1617.doc&amp;">Psychology</a></th>
                                 
                                 <td>CLP, DEP, EDP INP, PSY, SOP, PSYC</td>
                                 
                                 <td>Psychology</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/CriminalJusticeTechnology1617.doc&amp;">Criminal Justice
                                       Technology</a></th>
                                 
                                 <td>CCJ, CJC, CJE, CJJ, CJL, DSC, CRJT</td>
                                 
                                 <td>Public Service</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ParalegalStudies-1617.doc&amp;">Paralegal Studies</a></th>
                                 
                                 <td>BUL, PLA, PARA</td>
                                 
                                 <td>Public Service</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/FireScienceTechnology1617.doc&amp;">Fire ScienceTechnology</a></th>
                                 
                                 <td>FFP, FIRE</td>
                                 
                                 <td>Public Service</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Astronomy1617.doc&amp;">Astronomy</a></th>
                                 
                                 <td>AST, ASTR</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Biology1617.doc&amp;">BiologyBiology</a></th>
                                 
                                 <td>BOT, BSC, EVS, MCB, OCB, PCB, ZOO, BIOL</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Chemistry-1617.doc&amp;">Chemistry</a></th>
                                 
                                 <td>CHM, CHEM</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Geology-1617.doc&amp;">Geology</a></th>
                                 
                                 <td>ESC, GLY, GEOL</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Horticulture-1617.doc&amp;">HorticultureHorticulture-1617</a></th>
                                 
                                 <td>AOM, ENY, HOS, IPM, LDE, ORH, PLP, PLS, SWS, HORT</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Meteorology1617.doc&amp;">MeteorologMeteorology</a></th>
                                 
                                 <td>MET, METE</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Oceanography1617.doc&amp;">Oceanography</a></th>
                                 
                                 <td>OCE, OCEA</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/PhysicalScience1617.docx&amp;">Physical Science</a></th>
                                 
                                 <td>PSC, PHYC</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Physics1617.doc&amp;">Physics</a></th>
                                 
                                 <td>PHY, PHYS</td>
                                 
                                 <td>Science Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/SignLanguage1617.doc&amp;">Sign Language</a></th>
                                 
                                 <td>ASL, SIGN</td>
                                 
                                 <td>Sign Language</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Anthropology1617.doc&amp;">Anthropology</a></th>
                                 
                                 <td>ANT, ANTH</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Economics-1617.doc&amp;">Economics</a></th>
                                 
                                 <td>ECO, ECON</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Education-1617done.doc&amp;">Education</a></th>
                                 
                                 <td>ARE, EDF, EDG, EEC, EEX, EME, EDUC</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/MusicMUE-done.doc&amp;">Early Childhood Music and Movement</a></th>
                                 
                                 <td>MUE 2211, EDUC</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Geography-1617.doc&amp;">Geography</a></th>
                                 
                                 <td>GEA, GEOG</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/History1617.doc&amp;">History</a></th>
                                 
                                 <td>AMH, EUH, HIS, WOH, HIST</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/PoliticalScience1617.doc&amp;">Political Science</a></th>
                                 
                                 <td>INR, POS, POLI</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/SocialScience1617.doc&amp;">Social Science</a></th>
                                 
                                 <td>AFA, ISS, SOCI</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Sociology1617.doc&amp;">Sociology</a></th>
                                 
                                 <td>SYG, SOCL</td>
                                 
                                 <td>Social Science</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/SLS1617.doc&amp;">Student Life Skills</a></th>
                                 
                                 <td>SLS, STSU</td>
                                 
                                 <td>Student Success Division</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/ArtHistory-done.doc&amp;">Art</a></th>
                                 
                                 <td>ART, ARTS</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;16-17
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Dance-done.doc&amp;">Dance</a></th>
                                 
                                 <td>DAA, DANC</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="documents/DigitalMediaTechnology-done.doc&amp;">Digital Media Technology</a>
                                    
                                 </th>
                                 
                                 <td>DIG, DIGM</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/EntertainmentDesignandTechnology.doc&amp;">Entertainment Design and
                                       Technology</a></th>
                                 
                                 <td>TPA</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Film-done.doc&amp;">Film</a></th>
                                 
                                 <td>FIL, FILM</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Graphics-1617.doc&amp;">Graphics</a></th>
                                 
                                 <td>GRA, GRPH</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/MusicMUC.docx&amp;">Music</a></th>
                                 
                                 <td>MUC, MUSC</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/MusicMUM.doc&amp;">Music-</a></th>
                                 
                                 <td>MUM, MUS, MUSC</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="file:///S%7C/Academic%20Affairs-Common/Curriculum%20&amp;%20Assessment/Voter%20Lists/Voter%20Lists%202015-16/Arts%20and%20Entertainment/Music%20Sound%20and%20Music%20Tech%20MVK%20MUS%20MUN%20MUT.doc&amp;">Music</a>
                                    
                                 </th>
                                 
                                 <td>MUL, MUN, MUO, MUT, MVB, MVK, MVP, MVS, MVV, MVW, MUSC</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    <a href="file:///S%7C/Academic%20Affairs-Common/Curriculum%20&amp;%20Assessment/Voter%20Lists/Voter%20Lists%202015-16/Arts%20and%20Entertainment/Photography.doc&amp;">Photography</a>
                                    
                                 </th>
                                 
                                 <td>PGY, PHOT</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;15-16
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Theater.doc&amp;">Theater</a></th>
                                 
                                 <td>THE, TPP, THTR</td>
                                 
                                 <td>Visual and Performing Arts</td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/InformationTechnology.doc&amp;">Information Technology</a></th>
                                 
                                 <td></td>
                                 
                                 <td></td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Internships.docx&amp;">Internships - Changes that affect all/most
                                       internships only </a></th>
                                 
                                 <td></td>
                                 
                                 <td></td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/Internships.docx&amp;">Internships-Global changes</a></th>
                                 
                                 <td></td>
                                 
                                 <td></td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"><a href="documents/NetworkEngineeringTech-done.doc&amp;">Network Engineering
                                       Technology</a></th>
                                 
                                 <td></td>
                                 
                                 <td></td>
                                 
                                 <td>
                                    <i class="far fa-calendar"></i>&nbsp;
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/voter-lists.pcf">©</a>
      </div>
   </body>
</html>