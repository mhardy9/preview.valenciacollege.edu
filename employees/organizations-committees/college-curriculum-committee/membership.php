<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Curriculum Committee | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/membership.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>College Curriculum Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>College Curriculum Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <h2>Committee Membership<br>
                     2016-2017
                  </h2>
                  
                  <p><img alt="Curriculum Committee in Session" height="200" src="/images/employees/organizations-committees/college-curriculum-committee/membership-co-chairs.jpg" width="300"></p>
                  
                  
                  <p>Karen Borglum - AVP for Curriculum and Articulation - Co-Chair<br>
                     John Niss - Mathematics Representative - Co-Chair 
                  </p>
                  
                  
                  <div class="row">
                     
                     <h3>
                        Area Representatives for 2016-2017
                        
                     </h3>
                     
                     <div class="col-md-4">
                        
                        <ul class="list_staff">
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Karen Borglum, AVP, Co-Chair
                                 
                              </h5>
                              
                              <p>
                                 Curriculum and Articulation
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-male-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 John Niss, Co-Chair
                                 
                              </h5>
                              
                              <p>
                                 Mathematics
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Ana Caldero Figuero, Dean
                                 
                              </h5>
                              
                              <p>
                                 Associate in Arts
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Lisa Macon, Dean
                                 
                              </h5>
                              
                              <p>
                                 Associate of Science
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Beverly Bond
                                 
                              </h5>
                              
                              <p>
                                 Allied Health Programs
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Bonnie Oliver
                                 
                              </h5>
                              
                              <p>
                                 At-Large Representative
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Valencia image description" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Sonia Casablanca
                                 
                              </h5>
                              
                              <p>
                                 Business
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-male-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Nelson Placa
                                 
                              </h5>
                              
                              <p>
                                 District Office Representative
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Esther Coombs
                                 
                              </h5>
                              
                              <p>
                                 Communications
                                 
                              </p>
                              
                           </li>
                           
                           
                        </ul>
                        
                     </div>
                     				 
                     <div class="col-md-4">
                        
                        <ul class="list_staff">
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-male-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Chris Klinger
                                 
                              </h5>
                              
                              <p>
                                 Counselor
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-male-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Masood Ejaz
                                 
                              </h5>
                              
                              <p>
                                 Electrical and Computer Engineering, Bachelor of Science
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-male-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Ravi Varma
                                 
                              </h5>
                              
                              <p>
                                 Engineering
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-male-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Al Groccia
                                 
                              </h5>
                              
                              <p>
                                 Faculty Council
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-male-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Raul Valery
                                 
                              </h5>
                              
                              <p>
                                 Fine Arts
                                 
                              </p>
                              
                           </li>
                           				  
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Yolanda Gonzalez
                                 
                              </h5>
                              
                              <p>
                                 Foreign Language
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Julia Ribley
                                 
                              </h5>
                              
                              <p>
                                 Health Sciences,
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Lisa Lippitt
                                 
                              </h5>
                              
                              <p>
                                 Humanities
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Cheryl Robinson
                                 
                              </h5>
                              
                              <p>
                                 Honors
                                 
                              </p>
                              
                           </li>
                           					 
                        </ul>
                        					 
                     </div>
                     
                     <div class="col-md-4">
                        
                        <ul class="list_staff">
                           
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Joan Alexander
                                 
                              </h5>
                              
                              <p>
                                 Information Technology
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Regina Seguin
                                 
                              </h5>
                              
                              <p>
                                 Library
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Melody Boeringer
                                 
                              </h5>
                              
                              <p>
                                 Natural Science
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Leann Hudson
                                 
                              </h5>
                              
                              <p>
                                 Nursing
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Terry Miller/Debra Jacobs
                                 
                              </h5>
                              
                              <p>
                                 Public Service
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Yasmeen Qadri
                                 
                              </h5>
                              
                              <p>
                                 Social Science
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Betty Wanielista(EC)/(WC)
                                 
                              </h5>
                              
                              <p>
                                 Technical Education, East Campus
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure>
                                 <img src="/_resources/img/no-photo-female-thumb.png" alt="Blank User Picture" class="img-circle">
                                 
                              </figure>
                              
                              <h5>
                                 Marie Howard
                                 
                              </h5>
                              
                              <p>
                                 Technical Education, West Campus
                                 
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/membership.pcf">©</a>
      </div>
   </body>
</html>