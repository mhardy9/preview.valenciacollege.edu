<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Meeting Minutes | College Curriculum Committee | Valencia College | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/minutes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>College Curriculum Committee</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>Meeting Minutes | College Curriculum Committee | Valencia College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        Meeting Minutes
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Draft Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           			   
                           <li><a href="documents/employees/organizations-committees/college-curriculum-committee/minutes-2017-06-14.pdf">June 14, 2017</a></li>
                           			  
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Approved Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           				 
                           <li><a href="documents/employees/organizations-committees/college-curriculum-committee/minutes-2017-05-10.pdf">May 10, 2017</a></li>
                           				 
                           <li><a href="documents/employees/organizations-committees/college-curriculum-committee/minutes-2017-04-08.pdf">April 8, 2017</a></li>
                           				 
                           <li><a href="documents/employees/organizations-committees/college-curriculum-committee/minutes-2017-03-08.pdf">March 8, 2017</a></li>
                           				 
                           <li><a href="documents/employees/organizations-committees/college-curriculum-committee/minutes-2017-02-14.pdf">February 15, 2017</a></li>
                           	 
                           <li><a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2017-01-11.pdf">January 11, 2017</a></li>
                           
                        </ul>
                        
                     </div>
                     			 
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           2016 Archived Minutes
                           
                        </h3>
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-11-09.pdf">November
                                 9, 2016</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-10-12.pdf">October
                                 12, 2016</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-09-14.pdf">September
                                 14 2016 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-07-13.pdf">July
                                 13, 2016</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-05-11.pdf">May
                                 11, 2016 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-04-13.pdf">April
                                 13, 2016</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-03-16.pdf">March
                                 16, 2016</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-02-10.pdf">February
                                 10, 2016 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2016-01-13.pdf">January
                                 13, 2016 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-11-11.pdf">November
                                 11, 2015</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-10-14.pdf">October
                                 14, 2015 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-09-09.pdf">September
                                 9, 2015 (Skype Meeting) </a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        			  
                        			  
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           2015 Archived Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-07-08.pdf">July
                                 8, 2015 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-06-10.pdf">June
                                 10, 2015</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-05-13.pdf">May
                                 13, 2015 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-04-08.pdf">April
                                 8, 2015 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-03-08.pdf">March
                                 8, 2015</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2015-01-14.pdf">January
                                 14, 2015 </a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div>
                        
                        <h3>
                           2014 Archived Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-11-12.pdf">November
                                 12, 2014 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-10-08.pdf">October
                                 8, 2014 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-09-10.pdf">September
                                 10, 2014 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-07-09.pdf">July
                                 9, 2014 (E-Meeting) </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-06-11.pdf">June
                                 11, 2014 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-03-19.pdf">March
                                 19, 2014</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-02-12.pdf">February
                                 12, 2014 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2014-01-15.pdf">January
                                 15, 2014 </a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div>
                        
                        <h3>
                           2013 Archived Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-11-13.pdf">November
                                 13, 2013</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-10-16.pdf">October
                                 16, 2013</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-09-11.pdf">September
                                 11, 2013</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-07-10.pdf">July
                                 10, 2013 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-06-12.pdf">June
                                 12, 2013 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-04-10.pdf">April
                                 10, 2013 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-03-20.pdf">March
                                 20, 2013</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-02-13.pdf">February
                                 13, 2013</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2013-01-09.pdf"><strong>January
                                    9, 2013</strong></a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div>
                        
                        <h3>
                           2012 Archived Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-11-14.pdf">November
                                 14, 2012</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-10-10.pdf">October
                                 10, 2012 </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-07-11.pdf">July
                                 11, 2012 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-06-13.pdf">June
                                 13, 2012 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-05-09.pdf">May
                                 9, 2012</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-04-11.pdf">April
                                 11, 2012 (E-Meeting)</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-02-08.pdf">February
                                 8, 2012</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2012-01-11.pdf">January
                                 11, 2012 (Revised)</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div>
                        
                        <h3>
                           2011 Archived Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2011-11-09.pdf">November
                                 9, 2011</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2011-10-12.pdf">October
                                 12, 2011</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2011-09-14.pdf">September
                                 14, 2011</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2011-07-13.pdf">July
                                 13, 2011</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2011-04-13.pdf">April
                                 13, 2011</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2011-03-16.pdf">March
                                 16, 2011</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2011-02-09.pdf">February
                                 9, 2011</a> <em>(Revised)</em></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2011-01-12.pdf">January
                                 12, 2011</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div>
                        
                        <h3>
                           2010 Archived Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-11-10.pdf">November
                                 10, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-10-13.pdf">October
                                 13, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-09-08.pdf">September
                                 8, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-07-14.pdf">July
                                 14, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-06-09.pdf">June
                                 9, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-05-12.pdf">May
                                 12, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-04-14.pdf">April
                                 14, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-03-17.pdf">March
                                 17, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-02-10.pdf">February
                                 10, 2010</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2010-01-13.pdf">January
                                 13, 2010</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div>
                        
                        <h3>
                           2009 Archived Minutes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2009-11-11.pdf">November
                                 11, 2009</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2009-10-14.pdf">October
                                 14, 2009</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2009-09-09.pdf">September
                                 9 , 2009</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2009-07-08.pdf">July
                                 8, 2009</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2009-06-10.pdf">June
                                 10, 2009</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2009-05-13.pdf">May
                                 13, 2009</a></li>
                           
                           <li>
                              <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/minutes-2009-04-08.pdf">April
                                 8, 2009</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2009-03-18.pdf">March
                                 18, 2009</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2009-02-11.pdf">February
                                 11, 2009</a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-2009-01-14.pdf">January
                                 14, 2009</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/minutes.pcf">©</a>
      </div>
   </body>
</html>