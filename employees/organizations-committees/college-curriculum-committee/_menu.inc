<ul>
	<li><a href="/employees/organizations-committees/college-curriculum-committee/index.php">College Curriculum Committee</a></li>
	<li><a href="/employees/organizations-committees/college-curriculum-committee/about.php">About the Committee</a></li>
	<li><a href="/employees/organizations-committees/college-curriculum-committee/membership.php">Membership</a></li>
<li class="submenu"><a href="javascript:void(0);" class="show-submenu">Meeting Information <i class="fa fa-angle-down" aria-hidden="true"></i></a>
	<ul>
		
	<li><a href="/employees/organizations-committees/college-curriculum-committee/meetings.php">Meeting Schedule</a></li>
	<li><a href="/employees/organizations-committees/college-curriculum-committee/agenda.php">Agendas</a></li>
	<li><a href="/employees/organizations-committees/college-curriculum-committee/minutes.php">Meeting Minutes</a></li>
	</ul>
</li>

<li class="submenu"><a href="javascript:void(0);" class="show-submenu">Documentation <i class="fa fa-angle-down" aria-hidden="true"></i></a>
	<ul>
		<li><a href="/employees/organizations-committees/college-curriculum-committee/course-information-management.php">Course Information Management System</a></li>
		<li><a href="/employees/organizations-committees/college-curriculum-committee/general-education.php">General Education</a></li>
		<li><a href="/employees/organizations-committees/college-curriculum-committee/legislative.php">Legislative Topics</a></li>
		<li><a href="/employees/organizations-committees/college-curriculum-committee/process.php">Curriculum Process</a></li>
	</ul>
</li>
<li><a href="http://net4.valenciacollege.edu/forms/curriculumcommittee/contact.cfm">Contact Us</a></li>
</ul>
