<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Meeting Schedule  | Valencia College</title>
      <meta name="Description" content="Meeeting Schedule for College Curriculum Committee">
      <meta name="Keywords" content="committee, curriculum, schedules, meetings, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/meetings.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>Meeting Schedule </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        Scheduled Meetings 2017 - 2018
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <p>
                        Please note that all meetings begin at 2:00 pm
                        
                     </p>
                     
                     <table class="table table table-striped cart-list add_bottom_30">
                        
                        <thead>
                           
                           <tr>
                              
                              <th scope="col"> Date</th>
                              
                              <th scope="col"> Location</th>
                              
                              <th scope="col"> Agenda Deadline</th>
                              
                           </tr>
                           
                        </thead>
                        
                        <tbody>
                           
                           <tr>
                              
                              <td>August, 2017</td>
                              
                              <td>No Meeting Scheduled</td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>September 13, 2017</td>
                              
                              <td>West Campus, Room 6-202 </td>
                              
                              <td>August 30, 2017</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>October 11, 2017</td>
                              
                              <td>West Campus, Room 6-202</td>
                              
                              <td>September 27 , 2017</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>November 8, 2017</td>
                              
                              <td>West Campus, Room 6-202</td>
                              
                              <td>October 25, 2017</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>December 20, 2017</td>
                              
                              <td>No Meeting Scheduled</td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>January 17, 2018</td>
                              
                              <td>East Campus, Room 3-113</td>
                              
                              <td>December 20, 2017</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>February 14, 2018 <strong>CATALOG MEETING </strong></td>
                              
                              <td>East Campus, Room 3-113</td>
                              
                              <td>January 17, 2018*</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>March 7, 2018** </td>
                              
                              <td>East Campus, Room 3-113</td>
                              
                              <td>February 21, 2018</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>April 11, 2018</td>
                              
                              <td>East Campus, Room 3-113</td>
                              
                              <td>March 28, 2018</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>May 9, 2018</td>
                              
                              <td>East Campus, Room 3-113</td>
                              
                              <td>April 25, 2018</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>June 13, 2018</td>
                              
                              <td>West Campus, Room 6-202</td>
                              
                              <td>May 30, 2018</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>July 11, 2018</td>
                              
                              <td>West Campus, Room 6-202</td>
                              
                              <td>June 27, 2018</td>
                              
                           </tr>
                           
                        </tbody>
                        
                     </table>
                     
                     <p>
                        <strong>*</strong> This date has been moved back one week due to the heavy volume for the February Catalog
                        meeting. Agenda materials will not be accepted after this date (January 17, 2018).
                        ** Note: Spring Break is March 12-18, 2018
                        
                     </p>
                     
                     <p><a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/17-18-Curriculum-Committee-Meeting-Schedule.pdf">2017-2018 Meeting Schedule (Printable)</a></p>
                     
                     <hr class="styled_2">
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/meetings.pcf">©</a>
      </div>
   </body>
</html>