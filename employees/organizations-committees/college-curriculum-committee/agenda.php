<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Agendas &amp; Related Materials | College Curriculum Committee | Valencia College | Valencia College</title>
      <meta name="Description" content="Agendas &amp;amp; Related Materials for the College Curriculum Committee">
      <meta name="Keywords" content="committee, curriculum, schedules, meetings, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/agenda.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>College Curriculum Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>Agendas &amp; Related Materials </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        Agendas &amp; Related Materials
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Current Agenda
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/agenda-apr-8-2017.pdf">April 8, 2017 Agenda</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Consent Agenda
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/Y1617-agenda-items-193-221.pdf">Proposals 1617-193 to 1617-221</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Two-Year Program Review Cycle
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>No Outlines being presented</p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Regular Agenda
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>There are no Regular Agenda Items</p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Discussion Items
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>
                           There are no discussion items attachments.
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Information Items
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           			  
                           <li><a href="/documents/employees/organizations-committees/college-curriculum-committee/2017-2018-lab-fee-revisions.xlsx">2017-2018 Lab Fee Revisions</a></li>
                           			   
                           <li><a href="/documents/employees/organizations-committees/college-curriculum-committee/valencia-st-pete-appendix-update.docx">Valencia College and St. Petersburg COllege Appendix Update</a></li>
                           			  
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/agenda.pcf">©</a>
      </div>
   </body>
</html>