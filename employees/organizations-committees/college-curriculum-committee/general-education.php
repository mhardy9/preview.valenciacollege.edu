<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>General Education | College Curriculum Committee | Valencia College  | Valencia College</title>
      <meta name="Description" content="General Education for the College Curriculum Committee">
      <meta name="Keywords" content="CIM, committee, curriculum, schedules, meetings, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/general-education.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>College Curriculum Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>General Education </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        General Education
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Download
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-template.docx">General
                                 Education Opt-In Template</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-strategic-work-plan.pdf" target="_blank" title="Valencia General Education Planning and Communication Team">General Education
                                 Strategic Work Plan</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <h3>
                        General Education Voting Process, Principles &amp; Procedures
                        
                     </h3>
                     
                     <p>
                        In keeping with SACS Comprehensive Standard 3.4.10, <em>the institution places primary responsibility for
                           the content, quality, and effectiveness of the curriculum with its faculty,</em> the Curriculum Committee
                        will serve as the eligible voters for changes to the General Education program.&nbsp; However,
                        effective
                        collaboration and communication will be a part of all General Education decisions
                        made at the college.
                        
                     </p>
                     
                     <ul>
                        
                        <li>All stakeholders will be included in the process</li>
                        
                        <li>Stakeholders will be aware of their roles in the process</li>
                        
                        <li>The process will be inclusive of concerned faculty</li>
                        
                        <li>There will be clear and open paths for communication</li>
                        
                        <li>Results will be student centered addressing-
                           
                           <ul>
                              
                              <li>Program integrity</li>
                              
                           </ul>
                           
                        </li>
                        
                        <li>Data will inform the process
                           
                           <ul>
                              
                              <li>Effects on AS programs and pre-majors</li>
                              
                              <li>Transferability</li>
                              
                              <li>Course section offerings</li>
                              
                           </ul>
                           
                        </li>
                        
                     </ul>
                     
                     <p>
                        For a faculty member or dean to initiate a proposal to include a course in general
                        education institutional
                        hours
                        
                     </p>
                     
                     <ol>
                        
                        <li>The initiator will notify the co-chairs of the College-wide Curriculum Committee (CCC)
                           of the proposal
                           to include the course in a general education subject area.
                           
                        </li>
                        
                        <li>A co-chair will help organize a work team including at minimum the initiator, her/his
                           dean, and the
                           general education dean for the subject area. This group will prepare a portfolio for
                           the proposed course
                           that includes
                           
                           <ul>
                              
                              <li>A list of stakeholders that should be notified</li>
                              
                              <li>Course outline</li>
                              
                              <li>Assessment plan</li>
                              
                              <li>Gen Ed template</li>
                              
                              <li>Transferability data</li>
                              
                              <li>Impact assessment (enrollment, program integrity, etc)</li>
                              
                              <li>Communication plan</li>
                              
                           </ul>
                           
                        </li>
                        
                        <li>Once the portfolio is complete, a co-chair will schedule a face-to-face forum at an
                           appropriate time and
                           location that gives stakeholders an opportunity to learn about the proposal and provide
                           feedback.
                           Stakeholders will be notified both about the forum and that comments on the CCC blog
                           will be considered in
                           the decision process.
                           
                        </li>
                        
                        <li>If the initiator chooses to move the proposal forward it will be considered by the
                           CCC at the November
                           meeting. The decision will be based on the college’s general education principles,
                           the content of
                           the portfolio and the feedback collected at the forum and through the blog.
                           
                        </li>
                        
                     </ol>
                     
                     <h3>
                        Principles
                        
                     </h3>
                     
                     <p>
                        Courses within the General Education Program will:
                        
                     </p>
                     
                     <ol>
                        
                        <li>be able to meet the A.A. and A.S. degree requirements;</li>
                        
                        <li>significantly contribute to Valencia's general education outcomes;</li>
                        
                        <li>not narrowly focus on those skills, techniques, and procedures specific to a particular
                           occupation or
                           profession;
                           
                        </li>
                        
                        <li>be transferrable for all programs</li>
                        
                        <li>contribute significantly to breadth of knowledge</li>
                        
                     </ol>
                     
                     <p>
                        <em>This was Approved December 6, 2007; Reaffirmed Learning Day, February 2013</em>
                        
                     </p>
                     
                     <h3>
                        Procedures
                        
                     </h3>
                     
                     <p>
                        In keeping with SACS Comprehensive Standard 3.4.10, the institution places primary
                        responsibility for the
                        content, quality, and effectiveness of the curriculum with its faculty, the Curriculum
                        Committee will serve
                        as the eligible voters for changes to the General Education program. However, effective
                        collaboration and
                        communication will be a part of all General Education decisions made at the college.
                        
                     </p>
                     
                     <ol>
                        
                        <li>When deciding course additions to or deletions from the General Education program,
                           the
                           discipline-specific voter eligibility list will serve as a means of communication
                           and collaboration.
                           
                        </li>
                        
                        <li>In matters which relate to major changes across the five General Education areas,
                           there will be
                           communication from the faculty Co-Chair of the Curriculum Committee to all tenured
                           and tenure-track
                           faculty at the college. This communication will include the proposed change, the timetable
                           for
                           decision-making, access to resources for decision input, identification of opportunities
                           to provide input,
                           and required date of response.
                           
                        </li>
                        
                     </ol>
                     
                     <h3>
                        The Issue of Breadth
                        
                     </h3>
                     
                     <p>
                        In order to ensure consistency with decisions regarding the addition of courses to
                        the General Education
                        program, the following questions will be addressed:
                        
                     </p>
                     
                     <ol>
                        
                        <li>Does the course contribute significantly to satisfying the General Education Outcomes?
                           (Yes)
                        </li>
                        
                        <li>Does this course, when added to the General Education program, satisfy the&nbsp;mission
                           of Valencia
                           College (Yes)
                           
                        </li>
                        
                        <li>Is this course specific to a particular faculty member? (No)</li>
                        
                        <li>Does this course focus on a specific occupation? (No)</li>
                        
                        <li>Will this course, if added, be transferable to upper division programs? (Yes)</li>
                        
                        <li>Does this course have prerequisites that are not General Education courses? (No)</li>
                        
                     </ol>
                     
                     <hr class="styled_2">
                     
                     <h3>
                        General Education Outcomes
                        
                     </h3>
                     
                     <p>
                        Approved December 2007
                        
                     </p>
                     
                     <ul>
                        
                        <li>Cultural and Historical Understanding: Demonstrate understanding of the diverse traditions
                           of the world,
                           and an individual's place in it
                           
                        </li>
                        
                        <li>Quantitative and Scientific Reasoning: Use processes, procedures, data, or evidence
                           to solve problems
                           and make effective decisions
                           
                        </li>
                        
                        <li>Communication Skills: Engage in effective interpersonal, oral, written communication</li>
                        
                        <li>Ethical Responsibility: Demonstrate awareness of personal responsibility in one's
                           civic, social, and
                           academic life
                           
                        </li>
                        
                        <li>Information Literacy: Locate, evaluate, and effectively use information from diverse
                           sources
                        </li>
                        
                        <li>Critical Thinking: Effectively analyze, evaluate, synthesize, and apply information
                           and ideas from
                           diverse sources and disciplines
                           
                        </li>
                        
                     </ul>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           General Education Outcome Discussions: Campus-Based
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-outcome-discussion-winter-park.pdf">October
                                 19, 2012: Winter Park Campus</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-outcome-discussion-east.pdf">October
                                 26, 2012: East Campus</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-outcome-discussion-osceola.pdf">November
                                 5, 2012: Osceola Campus </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-outcome-discussion-west.pdf">November
                                 13, 2012: West Campus</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-outcome-discussion-themes.pdf">Themes
                                 from the General Education Discussion</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           A.A./General Education Program Review
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>
                           Document Repository
                           
                        </p>
                        
                        <ul>
                           
                           <li>3.5.1 - The institution identifies college-level competencies within the general education
                              core and
                              provides evidence that graduates have attained those competencies.
                              
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-sacs-visiting-team.pdf">Response
                                       to the Recommendations of the SACS Visiting Team</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>3.8.3 - The institution provides a sufficient number of qualified staff-with appropriate
                              education or
                              experiences in library and other learning/information resources to accomplish the
                              mission of the
                              institution
                              
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li><a href="http://valenciacollege.edu/library/about/departments-staff.cfm">Library Staff</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>3.9.3 - The institution employs qualified personnel to ensure the quality and effectiveness
                              of its
                              student affairs programs
                              
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li><a href="http://valenciacollege.edu/studentaffairs/organizationChart.cfm">Student Affairs
                                       Staff</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>4.1 - The institution evaluates success with respect to student achievement, including
                              as appropriate,
                              consideration of course completion, state licensing examinations, and job placement
                              rates.
                              
                              <ul>
                                 
                                 <li>
                                    <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-SIR-student-progression.pdf">SIR
                                       Student Progression</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-SIR-student-overview.pdf">SIR
                                       Online Students Overview</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-gateway-gap.pdf">Gateway
                                       Gap Model FTIC Students</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/aa-program-review-final-2012.pdf">2012
                                 AA Program Review FINAL RESULTS</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <h3>
                        Valencia General Education Planning and Communication Team
                        
                     </h3>
                     
                     <div>
                        
                        <h3>
                           Meeting Notes
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-gen-ed-planning-team-2012-08-24.pdf">August
                                 24, 2012</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-gen-ed-planning-team-2012-09-10.pdf">September
                                 10, 2012 </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-gen-ed-planning-team-2012-08-08.pdf">October
                                 8, 2012</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/minutes-gen-ed-planning-team-2012-11-06.pdf">November
                                 6, 2012 </a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           General Education Articles
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/article-university-education.pdf">A
                                 University Education</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/article-gen-ed-history.pdf">General
                                 Education History </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/article-gen-ed-new-global-century.pdf">College
                                 Learning for the New Global Century</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/article-gen-ed-for-global-century.pdf">General
                                 Education for a Global Century</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/general-education.pcf">©</a>
      </div>
   </body>
</html>