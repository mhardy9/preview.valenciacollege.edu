<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Curriculum Process  | Valencia College</title>
      <meta name="Description" content="College Curriculum Committee Process">
      <meta name="Keywords" content="committee, curriculum, schedules, meetings, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/process.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>College Curriculum Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>Curriculum Process </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        Preparing a Curriculum Proposal
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Step 1: Determine what kind of proposal you are preparing.
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>Are you <strong>adding</strong> a new <strong>course</strong>?
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li>Use Credit Course Addition - <strong>CCA</strong> - Form
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Are you <strong>modifying</strong> an existing <strong>course</strong>?
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li> Use Credit Course Modification - <strong>CCM</strong> - Form
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Are you <strong>deleting</strong> a <strong>course</strong>?
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li>Use Credit Course Deletion - <strong>CCD</strong> - Form
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Are you <strong>adding</strong> a new <strong>program</strong> (i.e., technical certificate, A.S.
                              degree, or pre-major)?
                              
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li>Use Credit Program Addition - <strong>CPA</strong> - Form; and a <strong>mock-up of the Program
                                       Sheet</strong> for the Catalog
                                    
                                 </li>
                                 
                                 <li>Note: The Senior Team must approve new programs prior to development.</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Are you <strong>modifying</strong> an existing <strong>program</strong>?
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li>Use Credit Program Modification - <strong>CPM</strong> - Form and a red-line of the current
                                    Program Sheet
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Are you <strong>deleting</strong> a <strong>program</strong>?
                           </li>
                           
                           <li class="list-unstyled">
                              
                              <ul>
                                 
                                 <li>Use Credit Program Deletion - <strong>CPD</strong> - Form and follow the deletion timeline
                                    guidelines in the College Catalog.
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p>
                           If you need additional guidance, please see your Division or Department representative.
                           If you still have
                           questions, please contact either Krissy Brissett or Karen Borglum in the Office for
                           Curriculum and
                           Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Step 2: Preparing Your Outline and Proposal Forms.
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              
                              <p>
                                 For a <strong>CCA</strong>, make sure to create an outline in the Course Information Management System
                                 and have it voted on prior to completing the <strong>CCA</strong> form. As soon as the basic course
                                 information (Catalog information) is available, notify Krissy Brissett so that she
                                 can submit the
                                 course to the State Course Numbering System (SCNS) for an official number. The course
                                 cannot be taken
                                 before the <strong>CCC</strong> without an approved number. The voting only occurs through the Course
                                 Information Management System process; no additional voting needs to occur with the
                                 CCA form. Be sure
                                 to attach a course syllabus in the <strong>CCA</strong> form (required). If the course is intended to
                                 be included in the General Education Program, a <strong>CPM</strong> form will be needed. Notify your
                                 voting list, and the <strong>CCC</strong> will serve as the voting body for the General Education
                                 Program Modification.
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>
                                 For a <strong>CCM</strong>, <strong>CCD</strong>, <strong>CPM</strong>, or <strong>CPD</strong>
                                 proposal, complete the appropriate form in the builder and vote (and encourage your
                                 colleagues to
                                 vote). Note: All Course forms and all Program forms are now located in Course Information
                                 Management
                                 System.
                                 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>
                                 For a <strong>CPA</strong> form, take the Program Proposal Generator to your dean and provost for
                                 approval, then take the form to IAC for feedback. Once all appropriate stakeholders
                                 have been given an
                                 opportunity to review the proposal, voting can occur.
                                 
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Step 3: Submit the completed proposal.
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>
                           Submit the completed proposal through Course Information Management System (including,
                           as appropriate, a
                           course outline, course syllabus, Program Proposal Generator Form, mock-up sheet, mark-up)
                           at least two to
                           three weeks prior to the Curriculum Committee meeting. <a href="meetings.html">View the Meeting
                              Schedule</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Step 4: Attend the Curriculum Committee meeting.
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>
                           Be prepared to answer the following questions
                           
                        </p>
                        
                        <ul>
                           
                           <li>How does the course that is being approved fit into the program? (Context)</li>
                           
                           <li>For proposals where the initiator is requesting that a course be put into General
                              Education - what
                              does the Committee look for? (see questions in the <a href="/documents/employees/organizations-committees/college-curriculum-committee/committee-manual.pdf">Curriculum
                                 Manual</a> - Pages 4 and 5)
                              
                           </li>
                           
                           <li>Is the course description language consistent and appropriate for the Catalog?</li>
                           
                           <li>Is the course modification changing hours? If so, please check with other disciplines
                              to ensure that
                              the credit hour changes do not impact another program.
                              
                           </li>
                           
                           <li>Does the program include the appropriate General Education requirements? At least
                              15 hours for an A.S.
                              degree and 18 hours if you plan on articulating to a BAS or BS degree. Remember, there
                              needs to be an
                              ENC 1101 course, a humanities course, a math or science course, and a social science
                              course.
                              
                           </li>
                           
                           <li>If you are deleting a course, is it impacting other program areas or General Education?</li>
                           
                           <li>If you are changing program hours, does this impact the total number of program hours?
                              Remember, this
                              is assigned by the State and cannot change.
                              
                           </li>
                           
                           <li>If you are considering a course for General Education, does your course description
                              state that it is a
                              General Education course? There should be no language in the Catalog Course Description
                              that refers to
                              the course as being a "General Education Course."
                              
                           </li>
                           
                           <li>If your course is repeatable for credit, did you make sure that you put a credit hour
                              limit on the
                              course?
                              
                           </li>
                           
                           <li>If the course is an honors course, did you follow the current Honors course description?</li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Important things to remember
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>The Curriculum Committee is here to help you. We ask questions from a broad perspective,
                              so that we
                              can cover all issues.
                              
                           </li>
                           
                           <li>You know your proposal better than anyone; you should attend the Curriculum Committee
                              meeting to
                              explain it.
                              
                           </li>
                           
                           <li>Make sure your dean knows that you have a proposal.</li>
                           
                           <li>If your proposal is not approved, you may be able to go back to your department, re-work
                              the proposal,
                              and present it again.
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/process.pcf">©</a>
      </div>
   </body>
</html>