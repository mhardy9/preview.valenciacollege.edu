<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Legislative Topics  | Valencia College</title>
      <meta name="Description" content="Legislative Topics for the College Curriculum Committee">
      <meta name="Keywords" content="committee, curriculum, schedules, meetings, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/legislative.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>Legislative Topics </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        Legislative Topics
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           House Bill 7135 - Updates
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/house-bill-7135-timeline.pdf">Work
                                 Timeline</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/house-bill-7135-final-approval.pdf">Final
                                 Approval March 2, 2012</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/house-bill-7135-summary.pdf" target="_blank">Summary August 20012 </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/house-bill-7135-faq-gen-ed-implementation.pdf">Provisions
                                 Related to General Education Implementation</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-timeline.pdf">General
                                 Education Timeline</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-faculty-committee-2012-10-25.pdf">Gen
                                 Ed Project: Faculty Committee Mtgs. (Oct. 25, 2012) </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-steering-initial-recs.pdf">Gen
                                 Ed Steering Committee Initial Draft Recommendations (December 2012) </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-faculty-committee-revised-recs.pdf">Gen
                                 Ed Faculty Committee Revised Recommedations (May 2013) </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-core-recs.pdf">Gen
                                 Ed Core Recommendations (August 2013)</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           House Bill 7135 - Learning Day Materials
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/house-bill-7135-gen-ed-themes.pdf">Themes
                                 from the General Education Discussions</a><br>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-faculty-committee-initial-recs.pdf">General
                                 Education Faculty Committee Initial Draft Recommendations</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/house-bill-7135-gen-ed-principles.pdf">General
                                 Education Principles and Procedures</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-tp-degree-analysis.xlsx">Gen
                                 Ed Analysis Within Transfer Plans and Degree Programs</a>
                              
                           </li>
                           
                           <li><a href="/documents/employees/organizations-committees/college-curriculum-committee/gen-ed-map.pdf">Current
                                 Map of General Education</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           House Bill 7135 - Related Documents
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>Florida Department of Education Website - Gen Ed Core Course Options:<br> <a href="http://www.fldoe.org/articulation/hb7135gep.asp" target="_blank">Office of Articulation Readiness
                                 for College Report</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Senate Bill 1720 - Updates
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/senate-bill-1720-full-legislation.pdf">Senate
                                 Bill 1720-Full Legislation</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/senate-bill-1720-legislative-action-2013-05-01.pdf">Legislative
                                 Action-5/1/2013</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/senate-bill-1720-webinar.pdf">Senate
                                 Bill 1720 Webinar</a>
                              
                           </li>
                           
                           <li><a href="http://prezi.com/f5-3tao30yoe/">Institutional Research </a></li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/senate-bill-1720-meta-majors-draft.pdf">Meta
                                 Majors-Draft </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/senate-bill-1720-meta-majors-revised.pdf">Meta
                                 Majors-Revised-6/20/2013</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/senate-bill-1720-faq.pdf">SB
                                 1720 FAQ's</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Developmental Education Work Team Meetings
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/dev-ed-workgroup-plan.pdf">Developmental
                                 Education Work Group Plan</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/dev-ed-meeting-2013-06-03.pdf">June
                                 3, 2013</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/dev-ed-meeting-2013-06-17.pdf">June
                                 17, 2013</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/dev-ed-meeting-2013-07-01.pdf">July
                                 1, 2013 </a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/dev-ed-meeting-2013-09-20.pdf">September
                                 20, 2013</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/dev-ed-meeting-2013-10-11.pdf">October
                                 11, 2013</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/dev-ed-reform-timeline.pdf">Developmental
                                 Education Reform Implementation Timeline</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Developmental Education Reading Material
                           
                        </h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul>
                           
                           <li>
                              <a href="/documents/employees/organizations-committees/college-curriculum-committee/essay-an-avalanche-is-coming.pdf" class="icon_for_pdf">Higher Education and the Revolution Ahead</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/legislative.pcf">©</a>
      </div>
   </body>
</html>