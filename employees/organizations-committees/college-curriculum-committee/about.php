<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>About  | Valencia College</title>
      <meta name="Description" content="About the College Curriculum Committee">
      <meta name="Keywords" content="committee, curriculum, schedules, meetings, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/about.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>College Curriculum Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li><a href="/employees/organizations-committees/college-curriculum-committee/">College Curriculum Committee</a></li>
               <li>About </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               				
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h2>
                        About the College Curriculum Committee
                        
                     </h2>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Charge
                           
                        </h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>
                           The College Curriculum Committee is responsible for ensuring that all courses and
                           programs have
                           instructional integrity, address appropriate learning outcomes, fit into a sequential
                           framework that leads
                           to students achieving the respective competencies, and meet the college's standards
                           of excellence. The
                           Committee reviews and must approve all additions, deletions, and major modifications
                           to credit courses and
                           programs and as such may assume leadership when there are no tenured faculty in a
                           particular discipline or
                           to suggest a change (addition, deletion, modification) where one may be necessary.
                           The College Learning
                           Council and the president receive the recommendations of this Committee.
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>
                           Membership
                           
                        </h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>
                           The committee consists of twenty one members: one Associate in Arts dean, one Associate
                           in Science dean,
                           and nineteen tenured/tenure-track professors [one each from the following disciplines:
                           communications,
                           arts and entertainment, foreign languages, allied health, humanities, mathematics,
                           natural sciences,
                           physical education, public service, business, social sciences, technical education,
                           counselor, librarian,
                           engineering/architecture programs, nursing, IT, one at-large member, and the Faculty
                           Council
                           President-Elect]. Each member shall have an alternate. Ex-officio members are a campus
                           provost, the AVP
                           for Workforce Development, the catalog coordinator, and representatives for Banner,
                           Dual Enrollment,
                           Financial Aid, SACS, Institutional Research, Graduation, Atlas, and Assessment. The
                           committee is
                           co-chaired by a faculty member and the AVP for Curriculum and Articulation. The faculty
                           co-chair will be a
                           voting member, will serve a one year term as co-chair during the second year or later
                           of membership on the
                           committee, and may be re-elected to the position. The faculty co-chair will be elected
                           by the voting
                           members of the committee during the March meeting. The VP for Academic Affairs/CLO
                           or his/her designee may
                           only cast a ballot to break a tie vote.
                           
                        </p>
                        
                        <p>
                           A quorum shall be considered to be more than half of the voting membership.
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/about.pcf">©</a>
      </div>
   </body>
</html>