<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Curriculum Committee | Employees | Valencia College | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/organizations-committees/college-curriculum-committee/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>College Curriculum Committee</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/organizations-committees/">Organizations Committees</a></li>
               <li>College Curriculum Committee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60">
                  
                  <div class="box_style_1">
                     
                     <h3>
                        Curriculum Committee Blog
                        
                     </h3>
                     
                     <p>
                        To create a post for the Curriculum Committee Blog, please <a href="http://wp.valenciacollege.edu/college-curriculum-committee/wp-login.php">login here</a> using your
                        Atlas Credentials. If you would like to review the Blog articles and post comments,
                        you can <a href="http://wp.valenciacollege.edu/college-curriculum-committee/">visit the site </a>.
                        
                     </p>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="box_style_1">
                     
                     <h3>
                        Social Sciences General Education Opt-In Request Criminal Justice
                        
                     </h3>
                     
                     <p>
                        James McDonald has withdrawn his proposal for this course.
                        
                     </p>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="box_style_1">
                     
                     <h3>
                        Humanities General Education Opt-In Request- Middle Eastern and African American Humanities
                        
                     </h3>
                     
                     <p>
                        These documents are here to assist you in discussing the opt-in request for Humanities.
                        Please review the
                        information, and post a comment in the above link.
                        
                     </p>
                     
                     <ul>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/humanities-template-HUM2403.docx">Gen
                              Ed Opt-In Template for HUM 2403</a></li>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/humanities-template-HUM2454.docx">Gen
                              Ed Opt-In Template for HUM 2454</a></li>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/humanities-rationale-for-inclusion.pdf">Rationale
                              for Inclusion</a></li>
                        
                     </ul>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="box_style_1">
                     
                     <h3>
                        Science General Education Opt in Request-Intro to Biotechnology
                        
                     </h3>
                     
                     
                     <p>
                        These documents are here to assist you in discussing the opt-in request for Humanities.
                        Please review the
                        information, and post a comment in the above link.
                        
                     </p>
                     
                     <ul>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/science-template-BSC1421C.docx">Gen
                              Ed Opt-In Template for BSC 1421C</a></li>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/science-final-portfolio-document.pdf">Final
                              Portfolio Document</a></li>
                        
                     </ul>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="box_style_1">
                     
                     <h3>
                        Helpful Documents
                        
                     </h3>
                     
                     <ul>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/committee-manual.pdf">2016-2017
                              Curriculum Committee Manual</a></li>
                        
                        <li><a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/lab-manual.pdf">Lab
                              Manual</a></li>
                        
                        <li>
                           <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/health-sciences-lab-hours.pdf">Lab
                              Manual, Appendix B - Health Sciences</a></li>
                        
                        <li>
                           <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/lab-manual-appendix-d-fees.pdf">Lab
                              Manual, Appendix D - Lab Fee Reports</a></li>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/cip-program-codes.xlsx">Program
                              CIP Codes</a></li>
                        
                     </ul>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="box_style_1">
                     
                     <h3>
                        				Secured Website Logins
                     </h3>
                     
                     
                     <p>
                        <a href="https://ban-apex-prod.valenciacollege.edu:8070/apex/f?p=LAB_FEES" class="button-outline" target="_blank">Lab Fees</a> 
                        				<a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=http%3A%2F%2Fgcp.valenciacollege.edu%2FCPIP%2F%3Fsys%3DCIM" class="button-outline" target="_blank">Course info Management</a> 
                        				<a href="http://wp.valenciacollege.edu/college-curriculum-committee/wp-login.php" class="button-outline" target="_blank">Committee Blog Login</a>
                        
                     </p>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="box_style_1">
                     
                     <h3>
                        Related Links
                        
                     </h3>
                     
                     
                     <ul>
                        
                        <li><a href="http://valenciacollege.edu/faculty/forms/voterlists/" target="_blank">Voter Lists</a></li>
                        
                        <li><a href="http://catalog.valenciacollege.edu" target="_blank">College Catalog</a></li>
                        
                        <li>
                           <a href="/documents/employees/organizations-committees/college-curriculum-committee/CIM-approval-process.pdf" target="_blank">Course Outline Approval Flow Chart</a></li>
                        
                        <li>
                           <a href="/documents/EMPLOYEES/organizations-committees/college-curriculum-committee/Provisional-Course-List-2013.pdf" target="_blank">Provisional Course List (2013)</a></li>
                        
                        <li><a href="http://scns.fldoe.org/scns/public/pb_index.jsp" target="_blank">State Course Numbering System
                              (SCNS)</a></li>
                        
                     </ul>
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/organizations-committees/college-curriculum-committee/index.pcf">©</a>
      </div>
   </body>
</html>