<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Frequently Asked Questions | Word Processing Services | Valencia College">
      <meta name="Keywords" content="college, school, educational, word, processing, frequently, asked, questions, faq">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/word-processing/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/word-processing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Word Processing Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/word-processing/">Word Processing</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <p><strong>Who is eligible to use Word Processing Services?</strong><br>
                           Services are available for faculty and staff of Valencia. 
                        </p>
                        
                        <p><strong>Who is charged for the service?</strong><br>
                           Your departmental budget will be charged for services.
                        </p>
                        
                        <p><strong> Which formats are acceptable for submitting materials 
                              to be printed?</strong><br>
                           Digital files and printed camera ready materials.
                        </p>
                        
                        <p><strong>What are some typical projects that your service has produced?</strong><br>
                           Newsletters, flyers, letters, tests, test banks, labels, certificates, 
                           annual reports, quizzes, books, copies, book covers, transparencies, 
                           color scans, color laser copies to name a few projects.
                        </p>
                        
                        <p><strong>Will I be provided an opportunity to proof the work prior 
                              to copying?</strong><br>
                           While we proof read all documents prepared, we encourage you to 
                           proof if you wish.
                        </p>
                        
                        <p><strong>Can I submit my own work for copying?</strong><br>
                           Yes, please be sure it is "camera-ready"..
                        </p>
                        
                        <p><strong>What special finishing services are available?</strong><br>
                           We can provide binding services and three-hole punch for your copy 
                           jobs. 
                        </p>
                        
                        <p><strong>What about color copies?</strong><br>
                           East Campus offers color laser prints @ .35 ea for limited quantities. 
                           Color copies can be obtained through Printing Services
                        </p>
                        
                        <p><strong>Do you archive jobs for reprinting?</strong><br>
                           Yes, if file is prepared by the Word Processing Center. 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/word-processing/faqs.pcf">©</a>
      </div>
   </body>
</html>