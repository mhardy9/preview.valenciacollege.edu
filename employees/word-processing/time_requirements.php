<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Time Requirements  | Valencia College</title>
      <meta name="Description" content="Time Requirements | Word Processing Services | Valencia College">
      <meta name="Keywords" content="college, school, educational, word, processing, time, requirements">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/word-processing/time_requirements.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/word-processing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Word Processing Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/word-processing/">Word Processing</a></li>
               <li>Time Requirements </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Time Requirements</h2>
                        
                        <h3><strong>East Campus</strong></h3>
                        Letters &amp; Memos: Same day service<br>
                        Typing: Two day service<br>
                        Copying: Two day service<br>
                        Type &amp; Copy: Four day service<br>
                        Rush projects: Contact Supervisor<br>
                        
                        
                        <p>*Please remember to add a day to the turn-around time if you would like your document
                           mailed to your box. 
                        </p>
                        
                        <p>Contact the East Campus Word Processing Office by telephone at 
                           ext. 2258 or by <a href="mailto:wordprocessing@valenciacollege.edu">email</a>. 
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>West Campus</strong></h3>
                        Letters &amp; Memos: Same day service<br>
                        Typing: Two day service<br>
                        Copying: Two day service<br>
                        Type &amp; Copy: Four day service<br>
                        Rush projects: Contact Supervisor
                        
                        
                        <p>Contact the West Campus Word Processing Office by telephone at 
                           ext.1266 or by <a href="mailto:westwordprocessing@valenciacollege.edu">email</a>. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/word-processing/time_requirements.pcf">©</a>
      </div>
   </body>
</html>