<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Word Processing Services  | Valencia College</title>
      <meta name="Description" content="Word Processing Services | Valencia College">
      <meta name="Keywords" content="college, school, educational, employees, word, processing">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/word-processing/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/word-processing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Word Processing Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li>Word Processing</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <p><img alt="Graphic" border="0" height="163" hspace="10" align="right" name="grphic" src="images/boxes.gif" vspace="10" width="90" id="grphic">Valencia provides professional word processing and copying services to our faculty
                           and staff. We provide typing and/or copying of letters, memos, books, syllabi, tests
                           &amp; quizzes, transparencies, flyers, classroom handouts and programs.
                        </p>
                        
                        <p>Orders can be submitted to the campus offices (East or West Campus locations) or by
                           completing the online form in this Web site. 
                        </p>
                        
                        <p>Materials can also be submitted by interoffice mail, fax or email attachment. Drop
                           box available on West Campus only.
                        </p>
                        
                        
                        <p><strong><a href="documents/WORDPROCESSINGCENTEROVERVIEWWestJune2014.pdf" target="new_tab">West Campus Information </a> <i class="far fa-file-pdf-o" aria-hidden="true"></i></strong></p>
                        
                        
                        <p><strong><a href="documents/WordProcessingEastInfoMarch2015.pdf">East Campus Information </a> <i class="far fa-file-pdf-o" aria-hidden="true"></i></strong></p>
                        
                        
                        <p><strong><a href="documents/PosterPrinterSupport.pdf" target="new_tab"><img alt="Poster Printer Graphic" border="0" height="146" src="images/PosterPrintingGraphic_001.jpg" width="150"></a></strong></p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        Bldg 1, Rm 255<br>
                        407-582-2258<br><br>
                        <strong>Fall/Spring:</strong> <br>Monday - Friday: 6:30am to 7pm <br> Friday: 6:30am to 5pm <br><br> 
                        <strong>Summer:</strong> <br> Monday - Thursday: 6:30am to 7pm <br> Friday: 6:30am to Noon
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/word-processing/index.pcf">©</a>
      </div>
   </body>
</html>