
<ul>
	<li><a href="/employees/word-processing/index.php">Word Processing Services</a></li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Order Forms <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/employees/word-processing/east/index.php">East Order Form</a></li>
			<li><a href="/employees/word-processing/orderform_west1.php">West Order Form</a></li>
		</ul>
	</li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Resources <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/employees/word-processing/time_requirements.php">Time Requirements</a></li>
			<li><a href="/employees/word-processing/hours.php">Hours</a></li>
			<li><a href="/employees/word-processing/faqs.php">FAQs</a></li>
		</ul>
	</li>
</ul>
