<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus  | Valencia College</title>
      <meta name="Description" content="East Campus | Word Processing Services | Valencia College">
      <meta name="Keywords" content="college, school, educational, word, processing, east, campus, print, poster, book">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/word-processing/east/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/word-processing/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Word Processing Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/word-processing/">Word Processing</a></li>
               <li>East</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>East Campus</h2>
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>The Word Processing Center is a support facility for East Campus faculty. All classroom
                              typing and printing support are provided in this area. The Center utilizes both Macintosh
                              and Dell Platforms and use a variety of software: Microsoft Word, Excel, PowerPoint,
                              Adobe InDesign, Photoshop, Illustrator, and Acrobat. There is a computer and printer
                              available for faculty use along with a scanner for grading tests. Training on the
                              use of the scanner is available. 
                           </p>
                           
                           
                           <p>The Word Processing Center is located in Building 1, Room 255. For questions or special
                              requests, call 407-582-2258.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Operating Hours</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <h4>Fall &amp; Spring Terms</h4>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <strong>Monday - Thursday:</strong>&nbsp;6:30am - 7:00pm*
                                    </li>
                                    
                                    <li>
                                       <strong>Friday:</strong>&nbsp;6:30am - 5:00pm
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>Summer Terms</h4>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <strong>Monday - Thursday:</strong>&nbsp;6:30am - 5:00pm**
                                    </li>
                                    
                                    <li>
                                       <strong>Friday:</strong>&nbsp;6:30am - 12:00pm
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>* Word Processing closes at 5pm when classes do not meet.</p>
                                 
                                 <p>** The East Campus Word Processing Center will be open the first two Saturdays of
                                    every term.
                                 </p>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <h4>Turnaround Time</h4>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <strong>Letters &amp; Memos:</strong>&nbsp;Same day service
                                    </li>
                                    
                                    <li>
                                       <strong>Typing:</strong>&nbsp;Two day service
                                    </li>
                                    
                                    <li>
                                       <strong>Copying:</strong>&nbsp;Two day service
                                    </li>
                                    
                                    <li>
                                       <strong>Type &amp; Copy:</strong>&nbsp;Four day service
                                    </li>
                                    
                                    <li>
                                       <strong>Rush projects:</strong>&nbsp;Contact Supervisor
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Turnaround times may vary depending on the extent of the job.</p>
                                 
                                 <p>Please remember to add a day to the turnaround time if you would like your document
                                    sent by interoffice mail.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in" id="east-forms">
                           
                           <h3>Online Requests</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The easiest way to request services from Word Processing is using the online forms.</p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://net4.valenciacollege.edu/forms/wordprocessing/east/print-request.cfm" target="_blank">Print Request</a> 
                              </li>
                              
                              <li>
                                 <a href="http://net4.valenciacollege.edu/forms/wordprocessing/east/paper-request.cfm" target="_blank">Paper Request</a> 
                              </li>
                              
                              <li>
                                 <a href="http://net4.valenciacollege.edu/forms/wordprocessing/east/book-request.cfm" target="_blank">Book Request</a> 
                              </li>
                              
                              <li><a href="http://net4.valenciacollege.edu/forms/wordprocessing/east/poster-request.cfm" target="_blank">Poster Request</a></li>
                              
                              <li>
                                 <a href="/documents/employees/word-processing/east-word-processing-design-request.pdf">Design Request Downloadable Form</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>
                                 
                              </li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Requesting Typing/Printing</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>In-Person Requests</h4>
                           
                           <p>Bring your work to the Word Processing Center, Building 1, Room 255, and fill in a
                              "Typing/Printing" Instruction Slip. Be sure to include your full name, telephone number,
                              mail code, and the name of the account to which your printing should be charged. The
                              name of the account can be obtained through your department secretary. Also, be sure
                              to tell us how many copies you want printed and where they should be held for you.
                              We prefer to hold tests in locked cabinets in the Copy Center for you to pick up since
                              this is the most secure. Other documents can be sent to your mailbox.
                           </p>
                           
                           
                           <h4>Rush Items</h4>
                           
                           <p>Occasionally you will need to have rush handling of an otherwise routine document.
                              Please call the Center at (407) 582-2258.
                           </p>
                           
                           
                           <h4>Color Printing</h4>
                           
                           <p>Color is offered. After supervisor/departmental approval, you can request transparencies,
                              flyers, invitations, certificates, nametags, etc. produced in color. Also, you can
                              bring in photographs and other non-copyrighted material to be scanned and saved for
                              use on the internet. 
                           </p>
                           
                           
                           <h4>Large Poster Printing</h4>
                           
                           <p>Word Processing Center can print large format color posters. Please call the Center
                              at (407) 582-2258 for more information. 
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Copying Services</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Copy Center</h4>
                           
                           <p>The East Campus Copy Center is available for all classroom printing. Two-day turnaround
                              is provided for regular printing jobs. Books, manuals, and final exams have special
                              deadlines posted each term. Please call Juan Montalvo at (407) 582-2278 for more information.
                              
                           </p>
                           
                           
                           <h4>Copyright</h4>
                           
                           <p>Typing and copying of materials must be in keeping with copyright laws. Written permission
                              from the publisher of copyrighted documents should be obtained prior to requesting
                              that the document be printed.
                           </p>
                           
                           
                           <h4>Emergency Copying</h4>
                           
                           <p>A copier is provided for your use in Room 4-101, the Academic Success Center. A code
                              can be obtained from your department.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Design Services</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Flyers, Posters, Programs, Invitatons, &amp; Other Graphic Materials</h4>
                           
                           <p>Various programs are utilized to create detailed flyers, posters, programs, invitations
                              and other graphic materials you may need. We can print your material in color and
                              have large format (color) poster printers. If you have any questions regarding design,
                              please contact Donna DeLong or Whitly Charles.
                           </p>
                           
                           
                           <p>Please fill out the <a href="/documents/employees/word-processing/east-word-processing-design-request.pdf">Design Request Form</a> to request service.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Other Services</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Document Version</h4>
                           
                           <p>Staff can convert documents to the PDF format and can also convert PDF documents to
                              Microsoft Word. Please contact Donna DeLong or Whitly Charles for more information.
                              
                           </p>
                           
                           
                           <h4>Test Grading</h4>
                           
                           <p>There is a scanner to grade multiple choice tests. Please see Word Processing Staff
                              for computer answer sheets and training in the use of the scanner. A scanner is also
                              available in the Academic Success Center (ASC) in Building 4, Room 101. Please check
                              with the ASC for their operating hours.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Staff</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <ul class="list_style_1">
                              
                              <li>Darla Brown, Staff Assistant II — ext. 2258</li>
                              
                              <li>Whitly Charles, Senior Word Processing — Specialist — ext. 2262</li>
                              
                              <li>Craig Oberg, Lead Reprographics Specialist — ext. 2278</li>
                              
                              <li>Ozelle Sabado, Senior Word Processing Specialist — ext. 2375</li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/word-processing/east/index.pcf">©</a>
      </div>
   </body>
</html>