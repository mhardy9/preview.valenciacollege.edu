<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus  | Valencia College</title>
      <meta name="Description" content="The Word Processing Center is a support facility for faculty and staff of the West Campus, Osceola Campus and District Office. Typing and printing of classroom material is provided in this area. We have software for Microsoft Word, Excel, Power Point, Office Publisher, Test Generator and Adobe Reader. The Copy Center which is housed within the Word Processing Center supports most all printing requests, large or small, and many other services including color printing, lamination and binding.">
      <meta name="Keywords" content="west, campus, word, processing, printing, copying, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/word-processing/west.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/word-processing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/word-processing/">Word Processing</a></li>
               <li>West Campus </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <h2>West Campus</h2>
                     
                     <hr class="styled_2">
                     
                     
                     
                     <div class="wrapper_indent">
                        
                        <p>The Word Processing Center is a support facility for faculty and staff of the West
                           Campus, Osceola Campus and District Office. Typing and printing of classroom material
                           is provided in this area. We have software for Microsoft Word, Excel, Power Point,
                           Office Publisher, Test Generator and Adobe Reader. The Copy Center which is housed
                           within the Word Processing Center supports most all printing requests, large or small,
                           and many other services including color printing, lamination and binding.
                        </p>
                        
                        
                        <p>The Word Processing Center is located in Building 1, Room 251. For questions or special
                           requests, call 407-582-1266.
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Operating Hours</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <h4>Fall &amp; Spring Terms</h4>
                              
                              <ul class="list-unstyled">
                                 
                                 <li>
                                    <strong>Monday - Thursday:</strong>&nbsp;7:30am - 6:00pm
                                 </li>
                                 
                                 <li>
                                    <strong>Friday:</strong>&nbsp;7:30am - 5:00pm
                                 </li>
                                 
                              </ul>
                              
                              
                              <h4>Summer Terms</h4>
                              
                              <ul class="list-unstyled">
                                 
                                 <li>
                                    <strong>Monday - Thursday:</strong>&nbsp;7:30am - 5:00pm*
                                 </li>
                                 
                                 <li>
                                    <strong>Friday:</strong>&nbsp;7:30am - 12:00pm
                                 </li>
                                 
                              </ul>
                              
                              <p>* Word Processing will be open extended hours during the first week of the Summer
                                 term.
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <h4>Turnaround Time</h4>
                              
                              <ul class="list-unstyled">
                                 
                                 <li>
                                    <strong>Letters &amp; Memos:</strong>&nbsp;Same day service
                                 </li>
                                 
                                 <li>
                                    <strong>Typing:</strong>&nbsp;Two day service
                                 </li>
                                 
                                 <li>
                                    <strong>Copying:</strong>&nbsp;Two day service
                                 </li>
                                 
                                 <li>
                                    <strong>Type &amp; Copy:</strong>&nbsp;Four day service
                                 </li>
                                 
                                 <li>
                                    <strong>Rush projects:</strong>&nbsp;Contact Supervisor
                                 </li>
                                 
                              </ul>
                              
                              <p>Turnaround times may vary depending on the extent of the job.</p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>Please note: Department deliveries are typically made once, starting midday on the
                           date requested.
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Requesting Typing/Printing</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="documents/west-word-processing-request.xlsx">Download the Excel order form</a> to your computer.
                           </li>
                           
                           <li>Complete your specific information in the top section of the page; name, account number,
                              mail code, etc. and save.
                           </li>
                           
                           <li>Each time you have a request, just complete the job information by placing an "X",
                              selecting from a drop-down list, entering a number and/or a date in the pertinent
                              boxes.
                           </li>
                           
                           <li>Once you have completed the form, attach the completed order form XLS file and the
                              original document(s) to an e-mail, and send them to <a href="mailto:WestWordProcessing@valenciacollege.edu">WestWordProcessing@valenciacollege.edu</a>.
                           </li>
                           
                           <li>If you prefer, you may print the form, attach it to your original document(s), and
                              submit your request via Interoffice Mail  , or in person at the Word Processing office.
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Copyright</h4>
                        
                        <p>Typing and copying of materials must be in keeping with copyright laws. Written permission
                           from the publisher of copyrighted documents should be obtained prior to requesting
                           that the document be printed.
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Staff</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        <ul class="list_style_1">
                           
                           <li>Debbie Jefferson, Word Processing Specialist — Ext. 1228</li>
                           
                           <li>Melanie Thomas, Word Processing Specialist — Ext. 1231</li>
                           
                           <li>Candy Davis, Reprographic Equipment Operator — Ext. 1282</li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/word-processing/west.pcf">©</a>
      </div>
   </body>
</html>