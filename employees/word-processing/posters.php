<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Poster Printing  | Valencia College</title>
      <meta name="Description" content="The East Campus Word Processing Center provides large format poster printing services for research projects, student government activities, and presentations.">
      <meta name="Keywords" content="poster, east, campus, word, processing, printing, copying, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/word-processing/posters.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/word-processing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/word-processing/">Word Processing</a></li>
               <li>Poster Printing </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <h2>Poster Printing</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="wrapper_indent">
                        
                        <p>To support Valencia faculty and student research collaborations, we provide large
                           format poster printing services for research projects, student government activities,
                           and presentations. 
                        </p>
                        
                        <p>To submit your file for printing, upload the file using the <a href="http://net4.valenciacollege.edu/forms/wordprocessing/east/poster-request.cfm">East Campus Poster Printing Request Form.</a></p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Printing Guidelines</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <li>Posters will be printed Monday-Friday from 8am to 4pm, excluding holidays when the
                           Word Processing Center is closed.
                        </li>
                        
                        <li>The poster will be printed within 4 business days, unless there is a problem with
                           the poster equipment, supplies, or we experience a high number of requests. Forms
                           submitted Saturday or Sunday will be printed before Thursday evening.
                        </li>
                        
                        <li>Posters can be printed on either 42 inch wide paper. Backgrounds, large photos and
                           significantly large areas of a single solid color or are strongly discouraged
                        </li>
                        
                        <li>Only faculty, staff, and students (with Student Development permission) may request
                           posters. We will verify your status.
                        </li>
                        
                        <li>The poster must be in one of the following formats: Illustrator, PDF, Photoshop, or
                           Word.
                        </li>
                        
                        <li>We are not responsible for editing or formatting posters that were not originally
                           created in the Word Processing Center.
                        </li>
                        
                        <li>We will review the poster to ensure it is for Valencia academic purposes, personal
                           posters will not be printed.
                        </li>
                        
                        <li>After the poster has been printed, you will receive an email notifying you to pick
                           up the poster in the Word Processing Center. If you do not receive an email within
                           one business day, please contact Word Processing, Ext. 2258.
                        </li>
                        
                        <li>We are not a full-service professional print shop. If you require very large (long)
                           posters, professional imaging, color separation or poster printing from other applications,
                           you may need to research another third party service not offered by the college.
                        </li>
                        
                        <li>We reserve the right to refuse requests that do not follow the guidelines above. </li>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Frequently Asked Questions</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>What software should I use when preparing my poster?</h4>
                        
                        <p>The most accessible and universal software for preparing your poster is in Adobe Acrobat’s
                           PDF format or Adobe Illustrator.
                        </p>
                        
                        
                        <h4>How long does production take and where do I pick up my poster?</h4>
                        
                        <p>Typically it takes four to five working days for your poster to be printed. You will
                           pick up your printed poster at the front Desk in the Word Processing Department, Building
                           1, Room 255.
                        </p>
                        
                        
                        <h4>Does anyone proof my work before they print?</h4>
                        
                        <p>If a poster is created in the Word Processing Center, it will be proofed. If you created
                           the poster and submit it for printing only, it will not be proofed. Proofing is solely
                           your responsibility. Please proof your work carefully before submitting for printing.
                        </p>
                        
                        
                        <h4>What is the biggest size my poster will be if the original is 8.5" by 11"?</h4>
                        
                        <p>It will be 42" by 54.35".</p> 
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/word-processing/posters.pcf">©</a>
      </div>
   </body>
</html>