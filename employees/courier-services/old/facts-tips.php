<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Courier Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/courier-services/old/facts-tips.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/courier-services/old/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Courier Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/courier-services/">Courier Services</a></li>
               <li><a href="/employees/courier-services/old/">Old</a></li>
               <li>Courier Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Facts and Tips</h2>
                        
                        <p><strong>Reminder!! </strong><br>
                           Please send back any unused interoffice envelopes that each department can spare to
                           Courier Services. Please put them in your outgoing mailbox so the couriers may pick
                           them up when completing their mail route. This will help us save budget dollars by
                           limiting the amount of interoffice envelopes and rubber bands we need to purchase
                           for the college. Thank you!!
                        </p>
                        
                        <h3>Interoffice Mail </h3>
                        
                        <ul>
                           
                           <li><strong><img alt="envelope" height="200" src="photo-envelope-275x200.jpg" width="275">If I don't know the mailcode for interoffice mail, may I only put the name of the
                                 recipient?</strong></li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>No. Always check the phone directory or microsoft   outlook when you have a quesiton
                              on what the  mail code of your   receipient is prior to placing any interoffice envelope
                              or package out   for delivery. Always include the name of the receipient, their mail
                              code, your name, and mail code so we are able to return the envelope or   package
                              to you in the event we are unable to complete the delivery. This   will allow for
                              your item to reach the end user in a more timely manner.
                           </p>
                           
                           <p>Refrain from checking Banner for mail codes. Banner is   not up to date with current
                              mail codes. The phone directory and   Microsoft Outlook are your best resources. 
                           </p>
                           
                        </blockquote>
                        
                        <ul>
                           
                           <li><strong>What if I only know the office and building number?</strong></li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>Use this as the last resort, but please include the campus (East, West, OSC, DO, SPS).</p>
                           
                        </blockquote>
                        
                        
                        <h3>College-Related Mail / Inter-Office Packages </h3>
                        
                        <ul>
                           
                           <li><strong><span><img alt="Label machine" height="200" src="photo-label-machine-275x200.jpg" width="275"></span>What about college-related mail and Inter-Office Packages? </strong></li>
                           
                           <li>All college-related mail should have a return address and mail code   on the envelope.
                              
                           </li>
                           
                           <li>Oversized or thick-packaged envelopes should be sealed since our mail meter will be
                              unable to seal them on your behalf. 
                           </li>
                           
                           <li>Complete the Internal Package Movement Request form   located under Valencia Forms
                              or click on the tab located on our web page   for this form.
                           </li>
                           
                           <li>Please follow the instructions provided. We will be unable to transport your items
                              without this form.
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><strong>How should I  send International Mail?</strong></li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>First, double check address and make sure address is complete and legible (Phone,
                              City, Country and code if available).
                           </p>
                           
                           <p>Separate all international mail by putting a rubber band and a note stating <em>INTERNATIONAL, </em>even if it is one piece.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Personal Mail </h3>
                        
                        <ul>
                           
                           <li><strong>Do you sell stamps?</strong></li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p> We do not sell stamps in the Mailroom, though they can   be purchased at the Bookstore.
                              We are only authorized to apply postage   on college related business  mailing. 
                           </p>
                           
                        </blockquote>
                        
                        <ul>
                           
                           <li><strong>What about personal mail?</strong></li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>All personal mail must be stamped prior to placing it inside your outgoing mail bin.
                              We will only apply postage onto mail that is marked on approved Valencia   College
                              stationary. If you have any questions on this, please contact   Courier Services.
                              
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Copy Paper / Toner Request </h3>
                        
                        <p>For toner, please e-mail Courier Services with your 5 digit system id number. </p>
                        For copy paper , please e-mail the Courier Service office that takes care of your
                        campus. 
                        
                        <blockquote>&nbsp;
                           
                        </blockquote>
                        
                        <p><a href="facts-tips.html#top">TOP</a></p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="https://net4.valenciacollege.edu/forms/courier-services/package-form.cfm" target="_blank"><img alt="Internal Package Movement Request Form" height="88" src="internal-package-movement-form-sm.png" width="75"></a>
                        
                        
                        <h3>Internal Package Movement Request Form</h3>
                        
                        
                        <a href="https://net4.valenciacollege.edu/forms/courier-services/package-form.cfm" target="_blank">Get Printable Form</a> <br>
                        
                        
                        
                        
                        
                        
                        
                        
                        <div> 
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-1755</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>7:30am - 4:00pm Monday - Friday</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Plant operations building Mailroom/Shipping/Receiving</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-2685</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:East_CourierServices@valenciacollege.edu">Email</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Central Energy Plant Building</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-4268</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:Osceola_CourierServices@valenciacollege.edu">Email</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Building 14, Room 132</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-1447 or 407-582-1750</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:West_CourierServices@valenciacollege.edu">Email</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/courier-services/old/facts-tips.pcf">©</a>
      </div>
   </body>
</html>