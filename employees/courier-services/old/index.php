<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Courier Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/courier-services/old/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/courier-services/old/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Courier Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/courier-services/">Courier Services</a></li>
               <li>Old</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>You have a delivery!</h2>
                        
                        <p><strong><img alt="envelope" height="200" src="photo-courier-services-sign-275x200.jpg" width="275"></strong>Thank you for visiting our web site. The Courier Services Department was formed in
                           the fall of 2008 by combining both Shipping &amp; Receiving and the mail rooms making
                           this a college wide service comprised of one department. Courier Services is committed
                           to providing a seamless delivery service of all packages, USPS mail, interoffice mail,
                           interoffice packages, stationary, toner, office supply orders, and copy paper delivery
                           throughout all locations in order to support the overall mission of the college. 
                        </p>
                        
                        <p>We  have offices at West,  East, and Osceola. 
                           The hours of operation are all  the same: 7:30 am - 4:00 pm. 
                           West campus is the main shipping  / receiving campus 
                           and West covers District  Office. 
                           East covers SPS, Lake Nona, and  Winter Park. Osceola covers CFFI and Advanced Manufacturing,
                           and Poinciana. 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="https://net4.valenciacollege.edu/forms/courier-services/package-form.cfm" target="_blank"><img alt="Internal Package Movement Request Form" height="88" src="internal-package-movement-form-sm.png" width="75"></a>
                        
                        
                        <h3>Internal Package Movement Request Form</h3>
                        
                        
                        <a href="https://net4.valenciacollege.edu/forms/courier-services/package-form.cfm" target="_blank">Get Printable Form</a> <br>
                        
                        
                        
                        
                        
                        
                        
                        
                        <div> 
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-1755</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>7:30am - 4:00pm Monday - Friday</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Plant operations building Mailroom/Shipping/Receiving</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-2685</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:East_CourierServices@valenciacollege.edu">Email</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Central Energy Plant Building</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-4268</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:Osceola_CourierServices@valenciacollege.edu">Email</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Building 14, Room 132</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-1447 or 407-582-1750</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:West_CourierServices@valenciacollege.edu">Email</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/courier-services/old/index.pcf">©</a>
      </div>
   </body>
</html>