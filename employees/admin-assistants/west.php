<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Administrative Assistants' Tool Box | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/admin-assistants/west.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/admin-assistants/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Administrative Assistants' Tool Box</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/admin-assistants/">Admin Assistants</a></li>
               <li>Administrative Assistants' Tool Box</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>West Campus</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4>Important Info</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/ImportantDates2016-17.pdf">Important Dates 2016-17</a></li>
                                       
                                       
                                       <li><a href="documents/Important-Dates-2017-18.pdf">Important Dates 2017-18</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h4>Notes</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/EPAF073008.doc">Administrative Assistants Workshop - EPAFs</a></li>
                                       
                                       <li>
                                          <a href="documents/AnswerCenter090308.doc">Administrative Assistants Workshop - Answer Center</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <h4>Forms</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <p>NOTE: The Request for Class to Meet Off-Campus form can now be found in the Valencia
                                    Forms in Atlas. 
                                    
                                 </p>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h4>Department Checklists for New Faculty </h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/admin-assistants/west.pcf">©</a>
      </div>
   </body>
</html>