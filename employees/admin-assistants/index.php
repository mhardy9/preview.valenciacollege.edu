<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Administrative Assistants' Tool Box | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/admin-assistants/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/admin-assistants/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Administrative Assistants' Tool Box</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li>Admin Assistants</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>College Wide Information </h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4>Payroll/EPAF Information</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/4-and8-MonthContractStatus.pdf">4- and 8-Month Faculty Contract Status</a></li>
                                       
                                       <li>
                                          <a href="ProfessionalDevelopmentHR26Info.html">Professional Development HR26 Instructions</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <h4>Contact Information</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/CollegewideDeanRoster-January2017.pdf">Collegewide Dean Roster</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h4>Notes</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/CalculatingExcessMileage.pdf">In-District Travel - Calculating Excess Miles </a></li>
                                       
                                       <li>Banner 8
                                          
                                          <ul class="list_style_1">
                                             
                                             <li><a href="documents/Banner8AccessInstructions.pdf">Access Instructions</a></li>
                                             
                                             <li><a href="documents/Banner8Faculty.pdf">Faculty</a></li>
                                             
                                             <li><a href="documents/Banner8SectionCreation.pdf">Section Creation</a></li>
                                             
                                             <li><a href="documents/Banner8Waitlist.pdf">Waitlist</a></li>
                                             
                                             <li><a href="documents/Registraion.pdf">Registration</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li><a href="documents/InstructionsforusingBannerXtender.pdf">Banner Xtender Instructions</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h4>Forms</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> 
                                    <ul class="list_style_1">
                                       
                                       <li>
                                          <a href="../office-of-information-technology/tss/helpdesk/Request_account.html">OIT Account Request Forms</a><a href="http://www.admin-pro.com/"></a>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <h4>Cool Grammar Tips</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul class="list_style_1">
                                       
                                       <li><a href="http://grammar.quickanddirtytips.com/">Quick Tips</a></li>
                                       
                                       <li><a href="http://www.webgrammar.com/">Webgrammar</a></li>
                                       
                                       <li><a href="https://owl.english.purdue.edu/owl/">Grammar, Punctuation, &amp; Spelling Tips</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h4>Great Resources</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul class="list_style_1">
                                       
                                       <li><a href="http://www.apomonline.org/members/member_main.asp">Association of Professional Office Managers </a></li>
                                       
                                       <li><a href="http://www.iaap-hq.org/">International Association of Administrative Professionals</a></li>
                                       
                                       <li><a href="http://www.admin-pro.com/">Personal Report for the Administrative Professional  </a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/admin-assistants/index.pcf">©</a>
      </div>
   </body>
</html>