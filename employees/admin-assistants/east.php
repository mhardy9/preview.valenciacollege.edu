<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Administrative Assistants' Tool Box | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/admin-assistants/east.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/admin-assistants/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Administrative Assistants' Tool Box</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/admin-assistants/">Admin Assistants</a></li>
               <li>Administrative Assistants' Tool Box</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>East Campus</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4>Important Info</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/FallSupportHours201810.pdf" target="_blank" class="icon_for_pdf">East Campus Fall Support Hours (201810)</a></li>
                                       
                                       <li><a href="documents/EastCampusDeptContactAugust2017.pdf" target="_blank" class="icon_for_pdf">East Campus Departmental Contact List </a></li>
                                       
                                       <li><a href="documents/ImportantDatesEastCampus17-18.pdf" target="_blank" class="icon_for_pdf">East Campus Important Dates 2017-2018 </a></li>
                                       
                                       
                                       <li><a href="documents/Faculty-Traditional-and-Alternative-Credentialing-Process.pdf" target="_blank">Faculty Traditional and Alternative Credentialing Process</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h4>FLAC Training Information</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/FLACPresentationtoAdmins.pdf" target="_blank">FLAC PowerPoint Presentation from March 2, 2016 </a></li>
                                       
                                       <li><a href="documents/FLACInstructions-PREPAREDBYADMINS.pdf" target="_blank">FLAC Instructions Prepared by the Administrative Assistants</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h4>Forms</h4>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li><a href="documents/CredittoAuditForm.doc">Credit to Audit Form </a></li>
                                       
                                       <li><a href="documents/SignInSheet.doc">Generic Sign-in Sheet</a></li>
                                       
                                       <li><a href="documents/TravelChecklist.doc">Travel Checklist</a></li>
                                       
                                       <li>
                                          <a href="documents/RequestforGuestSpeakerForm.doc">Request for Guest Speakers </a>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <p>NOTE: The <em><strong>Request for Class to Meet Off-Campus</strong></em> form can now be found in the Valencia Forms in Atlas.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/admin-assistants/east.pcf">©</a>
      </div>
   </body>
</html>