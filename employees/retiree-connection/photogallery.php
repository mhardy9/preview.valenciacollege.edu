<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Photo Gallery  | Valencia College</title>
      <meta name="Description" content="Photo Gallery | Valencia Retiree Connection">
      <meta name="Keywords" content="college, school, educational, retiree, connection, photo, gallery">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/retiree-connection/photogallery.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/retiree-connection/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Retiree Connection</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/retiree-connection/">Retiree Connection</a></li>
               <li>Photo Gallery </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h3><strong>Photo Gallery</strong></h3>
                        
                        <p><a href="https://www.flickr.com/photos/myvalencia/albums/72157657793194383">2015 Valencia Retiree Connection Luncheon</a></p>
                        
                        <p><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157654550644543">Alumni and Friends Wine and Cheese Reception</a> &amp; <a href="https://www.youtube.com/watch?v=VyEENnV0VRE&amp;feature=youtu.be">Video</a> 
                        </p>
                        
                        <p><a href="https://www.flickr.com/photos/valenciafoundation/sets/72157652073170798/">2014 Valencia Retiree Connection Luncheon</a><br>
                           
                        </p>
                        
                        <p><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157637448035575/" target="_blank">2013 Valencia Retiree Connection Luncheon</a><br>
                           
                        </p>
                        
                        <p><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631880520285/" target="_blank">2012 Valencia Retiree Connection Workshop </a> 
                        </p>
                        
                        <p><a href="http://www.flickr.com/photos/valenciafoundation/sets/72157631739930860/" target="_blank">2012 Valencia Retiree Connection Luncheon</a>    
                        </p>
                        
                        <p><a href="https://picasaweb.google.com/ValenciaAlumni/RetireeConnectionLuncheon2011" target="_blank">2011 Valencia Retiree Connection Luncheon</a></p>
                        
                        <p><a href="https://picasaweb.google.com/ValenciaAlumni/ValenciaAlumniFriendsDayAstrosSpringTraining" target="_blank">2011 Valencia Alumni and Friends Astro's Spring Training</a></p>
                        
                        <p><a href="https://picasaweb.google.com/ValenciaAlumni/ValenciaAlumniFriendsDayAstrosSpringTraining" target="_blank">2010 Valencia Retiree Connection Luncheon</a> 
                        </p>
                        
                        <p><a href="http://picasaweb.google.com/ValenciaAlumni/ValenciaOrlandoMagicNightDec162009" target="_blank">2009 Valencia Orlando Magic Night </a></p>
                        
                        <p><a href="http://picasaweb.google.com/ValenciaAlumni/RetireeConnectionLuncheon2009" target="_blank">2009 Valencia Retiree Connection Luncheon</a></p>
                        
                        <p><a href="http://picasaweb.google.com/ValenciaAlumni/ValenciaRetireeConnectionLunchOctober302008" target="_blank">2008 Valencia Retiree Connection Luncheon</a></p>
                        
                        <p><a href="http://picasaweb.google.com/ValenciaAlumni/RetireePhotosPastEvents?authkey=Gv1sRgCK3ImJz9p7bTnwE&amp;feat=directlink" target="_blank">Previous Retiree Events</a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/retiree-connection/photogallery.pcf">©</a>
      </div>
   </body>
</html>