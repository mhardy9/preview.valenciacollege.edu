<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Retiree Connection | Valencia College</title>
      <meta name="Description" content="Valencia Retiree Connection">
      <meta name="Keywords" content="college, school, educational, retiree, connection">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/retiree-connection/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/retiree-connection/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Retiree Connection</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li>Retiree Connection</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3><strong>Coming Attractions</strong></h3>
                        
                        <p> Keep updated on events of interest for retirees that take place at Valencia as well
                           as in the community. If your contact information has changed or you have moved, please
                           take a moment to update your<a href="http://net4.valenciacollege.edu/forms/retireeconnection/update-notes.cfm" target="_blank"> current contact information. </a></p>
                        
                        
                        <p><strong>For more information, contact Alumni Relations at <br>
                              <a href="mailto:retireeconnection@valenciacollege.edu">retireeconnection@valenciacollege.edu</a> or call 407-582-3426/3417. </strong></p>
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3><strong>Retiree Past Events</strong></h3>
                        
                        
                        <hr class="styled_2">
                        
                        <h3><strong>Additional Resources</strong></h3>
                        
                        <a href="http://www.vitaeonline.com/"><strong>View Current Vitae Issues</strong></a><p></p>
                        <a href="http://valenciacollege.edu/alumni/vitae.cfm"><strong>View Previous Vitae Issues</strong></a><p></p>
                        
                        
                        <a href="https://www.facebook.com/ValenciaCollege" target="_blank" title="Facebook"><i class="far fa-facebook-official" aria-hidden="true"></i> </a>Follow us on Facebook
                        
                     </div>
                     
                     
                  </div>  
                  
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/retiree-connection/index.pcf">©</a>
      </div>
   </body>
</html>