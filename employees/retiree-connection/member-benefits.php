<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Member Benefits  | Valencia College</title>
      <meta name="Description" content="Member Benefits | Valencia Retiree Connection">
      <meta name="Keywords" content="college, school, educational, retiree, connection, member, benefits">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/retiree-connection/member-benefits.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/retiree-connection/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Retiree Connection</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/retiree-connection/">Retiree Connection</a></li>
               <li>Member Benefits </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        <h3><strong>Member Benefits</strong></h3>
                        
                        <ul>
                           
                           <li>Discounted Valencia Theater tickets<br>
                              <a href="../artsandentertainment/theater/schedule.html">Theater - Valencia Character Company<br>
                                 <br>
                                 </a><strong>*</strong>Check out the new <strong><a href="http://valenciacollege.edu/arts/tickets.cfm">Valencia Retiree Flex Pass</a>. </strong>This allows retirees 2 FREE  tickets to each of the theater and dance events in the
                              2015-16 season! <a href="http://valenciacollege.edu/arts/tickets.cfm"><br>
                                 </a>
                              
                           </li>
                           
                           <li>Admission to alumni sponsored events<br>
                              <a href="../alumni/events.html">Alumni Calendar and News</a><br>
                              
                           </li>
                           
                           <li>10% discount on <a href="http://preview.valenciacollege.edu/continuing-education">Valencia Continuing Education</a> classes and  programs<br>
                              
                           </li>
                           
                           <li>Discounted <a href="https://runsignup.com/Race/FL/Kissimmee/ValenciaCollegeFamilyRunWalkforHeroes">Valencia 5K registration</a><br>
                              
                           </li>
                           
                           <li>Visit  <a href="http://www.vitaeonline.com/">Vitae</a>, the college's bi-annual  magazine
                           </li>
                           
                        </ul>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Free Membership in Discount Programs</strong></h3>
                        
                        <a href="https://valenciaalumrewards.abenity.com/perks/"><img src="/images/EMPLOYEES/retiree-connection/abenity_000.jpg" alt="aben" width="400" height="103" hspace="0" vspace="0" border="0" align="default"></a>
                        
                        
                        <p>Abenity Customer Service: <a href="https://valenciaalumrewards.abenity.com/perks/support/login">https://valenciaalumrewards.abenity.com/perks/support/login</a></p>
                        
                        
                        
                        <ol>
                           
                           <li>Go to <a href="http://www.workingadvantage.com/register">www.workingadvantage.com/register</a>
                              
                           </li>
                           
                           <li> Click on Register at top of page </li>
                           
                           <li>Click Employees Click Here</li>
                           
                           <li>Enter Valencia's Member ID #278897230 to create your FREE  account. </li>
                           
                        </ol>
                        
                        <p>Working Advantage Customer Service: <a href="mailto:customerservice@workingadvantage.com">customerservice@workingadvantage.com</a> or<br>
                           call (800) 565- 3712
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Interested in becoming a Vendor?</strong></h3>
                        <a href="http://www.youtube.com/watch?v=FyIfsZNOX_8&amp;feature=youtu.be"><img alt="ab" border="0" height="219" src="/images/EMPLOYEES/retiree-connection/aben.jpg" width="328"></a> <br>
                        <a href="http://valenciaalumrewards.abenity.com/vendorregistration">valenciaalumrewards.abenity.com/vendorregistration</a>  <br> 
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/retiree-connection/member-benefits.pcf">©</a>
      </div>
   </body>
</html>