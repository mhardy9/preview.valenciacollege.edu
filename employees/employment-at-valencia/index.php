<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Employment at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/employment-at-valencia/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/employment-at-valencia/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Employment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li>Employment At Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <h2>Employment At Valencia</h2>
                        					
                        					
                        <div>
                           
                           						
                           <h3>Explore Our Career Opportunities</h3>
                           						
                           <p>Achieve your full potential by helping others do the same.</p>
                           
                           						
                           <div><a href="https://valenciacollege.csod.com/ats/careersite/search.aspx?site=4&amp;c=valenciacollege&amp;sid=%5e%5e%5eT8tbpwkvmO0tTFe2OTKQJw%3d%3d" title="Search Faculty Jobs">Faculty Jobs</a></div>
                           						<br>
                           
                           						
                           <div><a href="https://valenciacollege.csod.com/ats/careersite/search.aspx?site=5&amp;c=valenciacollege&amp;sid=%5e%5e%5eT8tbpwkvmO0tTFe2OTKQJw%3d%3d" title="Search Staff Jobs">Staff Jobs</a></div>
                           						<br>
                           
                           						
                           <div><a href="https://valenciacollege.csod.com/ats/careersite/search.aspx?site=1&amp;c=valenciacollege" title="Search All Jobs">All Jobs</a></div>
                           					
                        </div>
                        
                        
                        					
                        <div>
                           
                           
                           						
                           <h3>Learn More About Valencia</h3>
                           						
                           <p>Why people love working here.</p>
                           						
                           <div>
                              							
                              <p><a href="/employees/human-resources/aboutValencia.php">About Valencia</a></p>
                              						
                           </div>
                           						
                           <div>
                              							
                              <p><a href="https://www.youtube.com/valenciacollege">Get to Know Us</a></p>
                              						
                           </div>
                           						
                           <div>
                              							
                              <p><a href="http://news.valenciacollege.edu/">Valencia News</a></p>
                              						
                           </div>
                           						
                           <div>
                              							
                              <p><a href="/locations/map/">Locations</a></p>
                              						
                           </div>
                           
                           					
                        </div>
                        
                        
                        					
                        <div>
                           						
                           <h3>Connect with Us</h3>
                           						
                           <p>We'd love to connect and welcome  you to our family. 
                              							Engage, chat, follow our news.
                           </p>
                           						
                           <div><a href="mailto:jobs@valenciacollege.edu"><i aria-hidden="true"></i> jobs@valenciacollege.edu</a></div>
                           						<br>
                           
                           						
                           <div><a href="https://www.facebook.com/valenciacollegejobs/"><i aria-hidden="true"></i> facebook.com/valenciacollegejobs</a></div>
                           						<br>
                           
                           						
                           <div><a href="https://twitter.com/valenciajobs/"><i aria-hidden="true"></i> twitter/valenciajobs</a></div>
                           						<br>
                           					
                        </div>
                        
                        
                        
                        
                        				
                     </div>          
                     
                     			
                  </div>
                  		
               </div>
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/employment-at-valencia/index.pcf">©</a>
      </div>
   </body>
</html>