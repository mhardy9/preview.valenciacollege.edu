<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>SharePoint | Valencia College</title>
      <meta name="Description" content="SharePoint sites allow Valencia employees to work together on documents, tasks, contacts, events, and other information.">
      <meta name="Keywords" content="sharepoint, sharepoint help, surveys, microsoft office, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web-and-portal-services/sharepoint.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web-and-portal-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web and Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/web-and-portal-services/">Web And Portal Services</a></li>
               <li>SharePoint</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h3>SharePoint</h3>
                        				
                        <p>With SharePoint, you can find out how easy it is for teams and individuals to create
                           Web sites for information sharing and document collaboration. SharePoint sites allow
                           Valencia employees to work together on documents, tasks, contacts, events, and other
                           information. Unfortunately, at this time, sharing outside of the Valencia network
                           is not an option.
                        </p>
                        				
                        <p>A "<strong>Team Site</strong>" has a number of very powerful built-in features including a shared calendar, announcements,
                           task list, contacts list, web links, document libraries, photo libraries, discussion
                           groups, and surveys. The beauty of this kind of site is that anyone who has access
                           to the site can be given permissions to post information and it is as easy as saving
                           a file or completing a form on a web site. Any team member with permissions and Internet
                           access can post documents to the shared document libraries. Anyone (with permissions)
                           can participate in discussion groups and access the other shared Team Site features.
                           To get started with SharePoint, all you need is a Web Browser and Internet access.
                        </p>
                        				
                        <p>A "<strong>My Site</strong>" is a Web space that provides each SharePoint member an environment to share information
                           about them as well as create a social center where other SharePoint members may connect
                           to them. This area is really the heart of the social world within SharePoint as from
                           this page one can manage their Professional Profile, Personal Site (Web space), Colleagues
                           and even create a Blog.
                        </p>
                        				
                        <p><a href="http://mysite.valenciacollege.edu/" target="_blank">http://mysite.valenciacollege.edu/</a></p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Do you need to request a new SharePoint site?</h3>
                        				
                        <p>Please submit your request well in advance of your required deadline. Some Web projects
                           can take longer than others to complete.
                        </p>
                        				
                        <p></p>
                        				
                        <ul>
                           					
                           <li>
                              						<a href="http://net4.valenciacollege.edu/forms/oit/web/request.cfm" target="_blank">Sharepoint - Web Development Request form</a>
                              					
                           </li>
                           				
                        </ul>
                        				
                        <p></p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Valencia SharePoint Getting Started Guides.</h3>
                        				
                        <p>SharePoint Help</p>
                        				
                        <p>We provide support to all faculty, staff, and classroom computers across the college.
                           If you are experiencing difficulties gaining access to any software or operating hardware,
                           please feel free contact our help desk. We can assist you in identifying solutions
                           and scheduling service calls with our technicians.
                        </p>
                        				
                        <p></p>
                        				
                        <ul>
                           					
                           <li>
                              						<a href="/documents/employees/office-of-information-technology/SharePoint-End-User-Guide.pdf" target="_blank" title="Site Owner Guide">Team Site End User Guide</a>
                              					
                           </li>
                           					
                           <li style="list-style: none">This guide is intended for SharePoint Members and My Site Owners that will utilize
                              SharePoint.
                           </li>
                           					
                           <li>
                              						<a href="/documents/employees/office-of-information-technology/SharePoint-Team-Site-Owner-Guide.pdf" target="_blank" title="Site Owner Guide">Team Site Owner Guide</a>
                              					
                           </li>
                           					
                           <li style="list-style: none">This guide is intended for SharePoint Site Owners that will administer and utilize
                              SharePoint.
                           </li>
                           				
                        </ul>
                        				
                        <p></p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Do you need training materials for Sharepoint?</h3>
                        				
                        <p>SharePoint Help</p>
                        				
                        <p></p>
                        				
                        <ul>
                           					
                           <li>
                              						<a href="https://support.office.com/en-us/article/SharePoint-Online-help-83c1c11b-3d7c-4852-b597-46309e0892b3?ui=en-US&amp;rs=en-US&amp;ad=US" target="_blank" title="Sharepoint 2010: Product Information and Featured Galleries">Sharepoint Online Help</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="http://office.microsoft.com/en-us/sharepoint-help/training-courses-for-sharepoint-2013-HA104030990.aspx" target="_blank">Training Videos</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="https://valenciacollege.zendesk.com/hc/en-us/sections/200557974-MS-SharePoint" target="_blank">Sharepoint FAQ</a>
                              					
                           </li>
                           				
                        </ul>
                        				
                        <p></p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Valencia O365 SharePoint Sites Getting Started Guide</h3>
                        				
                        <p>SharePoint Help for Office 365 Sites (SharePoint Online)</p>
                        				
                        <p></p>
                        				
                        <ul>
                           					
                           <li>
                              						<a href="/documents/employees/office-of-information-technology/SharePoint-O365-Site-Set-Up.pdf" target="_blank" title="Site Owner Guide">O365 SharePoint Site Set Up Guide</a>
                              					
                           </li>
                           					
                           <li style="list-style: none">This guide is intended for SharePoint Site Owners that will administer and utilize
                              SharePoint.
                           </li>
                           				
                        </ul>
                        				
                        <hr class="styled_2">
                        				
                        <h3>O365 Sites Information and Help Links</h3>
                        				
                        <p>SharePoint Help for Office 365 Sites (SharePoint Online)</p>
                        				
                        <p></p>
                        				
                        <ul>
                           					
                           <li>
                              						<a href="https://products.office.com/en-us/business/explore-office-365-for-business" target="_blank">What are Office 365 SharePoint Sites?</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="http://office.microsoft.com/en-us/business/microsoft-office-365-for-business-faq-FX103030232.aspx" target="_blank" title="Get to the Point: SharePoint Training Blog">Office 365 FAQ</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="http://office.microsoft.com/en-us/office365-suite-help/training-courses-for-office-365-for-business-HA104031723.aspx" target="_blank">Training Videos</a><br>
                              					
                           </li>
                           					
                           <li>
                              						<a href="http://office.microsoft.com/en-us/office365-sharepoint-online-enterprise-help/public-website-help-for-office-365-HA102891740.aspx?hcs=28b19708-5e6a-4f7c-955b-79377dedc8f1" target="_blank">Public Website help</a>
                              					
                           </li>
                           				
                        </ul>
                        				
                        <p></p>
                        				
                        <p></p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web-and-portal-services/sharepoint.pcf">©</a>
      </div>
   </body>
</html>