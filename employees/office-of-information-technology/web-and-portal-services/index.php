<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web and Portal Services (WPS) | Valencia College</title>
      <meta name="Description" content="Web and Portal services builds and maintains Web &amp;amp; Portal sites for the college departments. ">
      <meta name="Keywords" content="web and portal, web design, cms, web department, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web-and-portal-services/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web-and-portal-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web and Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Web And Portal Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Web and Portal Services (WPS)</h2>
                        				
                        <p>Web and Portal services builds and maintains Web and Portal sites for the college
                           departments. We also maintain Atlas, which is the college's Luminis portal.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Faculty Web Space</h3>
                        				
                        <p></p>
                        				
                        <p>Web space is available for faculty and adjuncts. The Web team has designed a place
                           on the Valencia Web site for faculty to maintain a Web presence. It is called "Faculty
                           Frontdoor." This is a user friendly, form-based application that allow faculty to
                           post announcements, syllabi, documents, images and even video. To get started, follow
                           the steps below:
                        </p>
                        				
                        <p>Faculty Frontdoor</p>
                        				
                        <ul>
                           					
                           <li>Sign in to <a href="https://atlas.valenciacollege.edu/">Atlas</a>
                              					
                           </li>
                           					
                           <li>Faculty tab &amp;gt; Faculty Tools channel &amp;gt; Faculty Frontdoor</li>
                           					
                           <li>Sample Web Address: http://frontdoor.valenciacollege.edu/?aSmith</li>
                           				
                        </ul>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Adding information to the college site</h3>
                        				
                        <p></p>
                        				
                        <p>If you have anything you would like to add or change on the college Web site, please
                           send your information and suggestions to <a href="mailto:webservices@valenciacollege.edu">Web Services</a>.
                        </p>
                        				
                        <p>Files in the following formats may be sent as attachments: PDF, Word, Excel, &amp; PowerPoint,
                           JPEG, GIF and PNG. Please check with Web Services if you have any other file type.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Timelines</h3>
                        				
                        <p></p>
                        				
                        <p>We aim to add new information to the college Web site quickly. However, at certain
                           times this may not be possible. We will do everything we can to assist if you need
                           to add material to the site urgently, but please speak to us in-person or by telephone
                           first (ext. 5529).
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Online Courses</h3>
                        				
                        <p></p>
                        				
                        <p>Online Courses are available for any member of staff who wishes to create their own
                           course pages. It is currently managed by the <a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web-and-portal-services/index.pcf">©</a>
      </div>
   </body>
</html>