<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>GoTo Meeting &amp; GoTo Webinar | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/goto/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/goto/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>GoTo Meeting &amp; GoTo Webinar</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Goto</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a> 
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       
                                       <div>
                                          
                                          <div> 
                                             
                                             <div>
                                                
                                                <p><font face="Arial, Helvetica, sans-serif" size="5">Join An Online Meeting</font></p>
                                                
                                                <p><font face="Arial, Helvetica, sans-serif" size="2">To attend a demonstration, training course, or collaboration meeting: enter your meeting
                                                      number in the field below and click "Meet Now".</font></p>
                                                
                                                
                                                <p></p>
                                                
                                                <p><font face="Arial, Helvetica, sans-serif" size="2">For meeting questions or assistance, please contact your meeting organizer.</font><br>
                                                   
                                                   
                                                </p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <h2>Welcome!</h2>
                        
                        <p>There  are two  buzzwords floating around campus - GoToMeeting and GoToWebinar. If
                           you have been asked to participate or present using these new tools, please take advantage
                           of the resources hosted here. 
                        </p>
                        
                        <p>To find out more, browse through the links here and the resources located on the left
                           hand side of your screen. 
                        </p>
                        
                        <h3>What is GoToMeeting? </h3>
                        
                        <p> GoToMeeting is a Web conferencing tool that allows you to meet online rather than
                           in a conference room. It’s the easiest and most cost-effective way to organize and
                           attend online meetings (max: 25 participants in one meeting) 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <img alt="PDF" height="16" src="icon_pdf.gif.html" width="16"> <a href="https://www1.gotomeeting.com/default/downloads/pdf/p/GoToMeeting_Fact_Sheet.pdf" target="_blank">GoToMeeting Fact Sheet</a>
                              
                           </li>
                           
                           <li><a href="https://www.gotomeeting.com/en_US/pre/faq.tmpl" target="_blank">GoToMeeting FAQs</a></li>
                           
                        </ul>
                        
                        <h3>What is GoToWebinar? </h3>
                        
                        <p> A Webinar, or "Web seminar," is an online event designed to facilitate communication
                           between a small number of presenters and a large remote audience using the Internet.
                           GoToWebinar helps you deliver your message to hundreds of people at the same time,
                           eliminating the need for costly travel or expensive marketing promotions (Max: 1,000
                           participants in one webinar). 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <img alt="PDF" height="16" src="icon_pdf.gif.html" width="16"> <a href="https://www1.gotomeeting.com/default/downloads/pdf/p/GoToWebinar_Fact_Sheet.pdf" target="_blank">GoToWebinar Fact Sheet</a>
                              
                           </li>
                           
                           <li><a href="https://www.gotowebinar.com/en_US/webinar/pre/faq.tmpl" target="_blank">GoToWebinar FAQs</a></li>
                           
                        </ul>
                        
                        <h3>Best Practice Documentation <img alt="PDF" height="13" src="icon_lock.gif" width="10">
                           
                        </h3>
                        
                        <blockquote>
                           
                           <p><img alt="PDF" height="16" src="icon_pdf.gif.html" width="16"> <a href="http://www.citrixonline.com/downloads/bp/g2a/Citrix_Online_Best_Practices_Program_Brochure.pdf" target="_blank">Citrix Online Best Practices Program Overview</a></p>
                           
                           <p><img alt="PDF" height="13" src="icon_lock.gif" width="10"> <a href="https://www.citrixonline.com/expertcity/bp/g2m/entry.tmpl?_sid=19337%3A7A953DA1B2FF7E8&amp;Action=rgoto&amp;_sf=1" target="_blank">Best Practices Web Site</a></p>
                           
                           <p><img alt="PDF" height="16" src="icon_pdf.gif.html" width="16"> <a href="documents/UserGuide.pdf" target="_blank">User Guide</a></p>
                           
                        </blockquote>
                        
                        <h3>Tutorials</h3>
                        
                        <p><img alt="PDF" height="20" src="icon_winmedia.gif.html" width="20"> <a href="https://www.gotomeeting.com/default/help/viewlets/gotomeeting_organizer_training.wmv">Organizer   Training Video</a> (40 minutes)
                        </p>
                        
                        <p><strong>Quick-Start Attendee Tutorials</strong></p>
                        
                        <ul type="disc">
                           
                           <li>
                              
                              <p><img alt="Flash Video" height="16" src="icon_flash.gif" width="16"> <a href="https://www.gotomeeting.com/default/help/viewlets/g2m_att_join_from_invite/g2m_att_join_from_invite_viewlet_swf.html" target="_blank">Attend a Meeting from an Invitation</a> (1.5 minutes)
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p><img alt="Flash Video" height="16" src="icon_flash.gif" width="16"> <a href="https://www.gotomeeting.com/default/help/viewlets/g2m_att_join_from_website/g2m_att_join_from_website_viewlet_swf.html" target="_blank">Attend a Meeting from the GoToMeeting Web site</a> (2 minutes)
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p><a href="index.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/goto/index.pcf">©</a>
      </div>
   </body>
</html>