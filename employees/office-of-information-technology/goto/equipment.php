<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>GoTo Meeting &amp; GoTo Webinar | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/goto/equipment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/goto/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>GoTo Meeting &amp; GoTo Webinar</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/goto/">Goto</a></li>
               <li>GoTo Meeting &amp; GoTo Webinar</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>Recommended Equipment for use with GoTOMeeting:</h2>
                        
                        <p>Remember, you only need an audio device to use GoToMeeting.</p>
                        
                        <h3><strong> Speakerphone currently available in the learning technology centers:</strong></h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <br>
                                    <strong>Polycom Speakerphone </strong><br>
                                    Model: Direct Connect Sound Station 2 
                                    
                                    <p><img alt="polycom.jpg" height="91" src="equipment_clip_image002.jpg" width="181"></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>The Polycom system is a very sensitive speakerphone – great addition    to a phone
                                       conference OR a go to meeting held in a large room where the    existing phone makes
                                       the interaction a bit difficult to hear (for example    East Campus 5-112).
                                    </p>
                                    
                                    <p><strong>NOTE:</strong> You only need this    speakerphone if&nbsp; you are meeting in an    large room.&nbsp; If you
                                       are in your office,    simply use your phone to conference into the GoToMeeting Session.
                                    </p>
                                    
                                    <h3>Where can you check this    system out?</h3>
                                    <br>
                                    Osceola 2-116 (Gary Kokaisel x4812)<br>
                                    West Campus 3-326A (Daniel Charriez x1792)<br>
                                    East Campus 4-133C (Migdalia Otero x2425)<br>
                                    Winter    Park Campus RM 142(Brian Bellisimo x6819)<br>
                                    DownTown Center (Lisa Yenke x3401)
                                    
                                    <p><strong>Resources:</strong><br>
                                       <a href="documents/polycomDirections_1.pdf">Quickstart Guide – Link (polycom.pdf)</a><br>
                                       <a href="documents/UserGuide_000.pdf">UserManual – Link </a><a href="documents/Polycom_SoundStation2_Manual_for_Nortel.pdf">(Polycom_SoundStation2_Manual_for_Nortel.pdf)</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../office-of-information-technology/goto/equipment.html#top">TOP</a></p>
                        
                        <h3>Ready to Order Equipment?</h3>
                        
                        <p>If you are interested in using the “Microphone and Speakers”  option with GoToMeeting,
                           please consider the following list of recommended equipment – choose what fits your
                           budget and presenting needs. When you are ready to order - contact <strong>Jeff Kurtz</strong> at x5541 or via email at 
                           
                           
                           <a href="mailto:jkurtz@valenciacollege.edu">jkurtz@valenciacollege.edu</a> <br clear="all">
                           
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Equipment Recommendations</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Headset with microphone</strong> (USB connection Preferred)
                                    </p>
                                    
                                    <p>        *Recommended*</p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><img alt="6536.1.0.jpg" height="120" src="equipment_clip_image004.jpg" width="120"></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Any Logitech Heaset would work    – </strong><br>
                                       Great sound quality and conservatively priced.<br>
                                       Windows and Mac Compatible<br>
                                       Some are cordless/ wireless<br>
                                       Great option to reduce ambient noise during a web-conferencing    meeting!
                                    </p>
                                    
                                    <p>$50-100 price range.<br>
                                       Click <a href="http://www.logitech.com/index.cfm/webcam_communications/internet_headsets_phones/&amp;cl=us,en" target="_blank">here</a> for a list of options
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>High-Quality Omnidirectional&nbsp;    Microphone</strong></p>
                                    
                                    <p>*Recommended*</p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><img alt="Planet Headset USB Microphone" height="100" src="headset.jpg" width="159"></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Planet Headset’s USB PRO    microphone.</strong><br>
                                       Simple Plug and Play use<br>
                                       Easy to transport (it is the size of a jumpdrive)<br>
                                       High Quality digital audio.<br>
                                       Windows , Vista, and Mac Supported<br>
                                       $79.99 – <a href="http://planetheadset.com/usb-microphone.php" target="_blank">Link    for details</a></p>
                                    
                                    <p>Great range! 50ft+ in every direction and superior quality.&nbsp; Great option if you have
                                       multiple people    meeting in one office location.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Simple Microphone</strong> (USB connection preferred).
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p><img alt="microphone.jpg" border="0" height="96" src="equipment_clip_image008.jpg" width="96"></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Logitech’s USB Desktop    Microphone.</strong><br>
                                       Simple Plug and Play use<br>
                                       Noise Cancelling, USB Compatible<br>
                                       Windows Vista and Mac Supported<br>
                                       $29.99 – <a href="http://www.logitech.com/index.cfm/webcam_communications/microphones/devices/221&amp;cl=us,en" target="_blank">Link</a></p>
                                    
                                    <p>Great Idea for desktop to desktop conferencing but limited in range    (distance between
                                       you and the microphone).
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Any Webcam</strong> (to use microphone capabilities)
                                    </p>
                                    
                                    <p>*least preferred method*</p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><img alt="logitech_cam.jpg" border="0" height="128" src="equipment_clip_image010.jpg" width="64"></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Logitech’s Quickcam Orbit AF.</strong><br>
                                       Simple Plug and Play use<br>
                                       Integrated Microphone with some built-in noise reduction.<br>
                                       Windows only<br>
                                       $129.99 – <a href="http://www.logitech.com/index.cfm/webcam_communications/webcams/devices/3480&amp;cl=us,en" target="_blank">Link    for details</a></p>
                                    
                                    <p>Great range! 50ft+ in every direction and superior quality.&nbsp; Great option if you have
                                       multiple people    meeting in one office location.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                        </div>
                        
                        <p><a href="../../office-of-information-technology/goto/equipment.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/goto/equipment.pcf">©</a>
      </div>
   </body>
</html>