<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>About OIT | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/about/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/about/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>About OIT</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/about/">About</a></li>
               <li>About OIT</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2> OIT Staff </h2>
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 <div>
                                    
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#OIT">OIT Management and Office Support</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#NET">Network &amp; Infrastructure Services</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#APS">Enterprise Application Services</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#DWD">Enterprise Application Services - Data Warehouse Development</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#IIT">Web &amp; Portal Services</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#ITC">Learning Technology &amp;  Project Management Services</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#TSE">Campus Technology Services - East &amp; Winter Park</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#TSO">Campus Technology Services - Osceola &amp; Lake Nona</a></li>
                                    
                                    
                                    <li><a href="../../office-of-information-technology/about/staff.html#TSW">Campus Technology Services - West &amp; District Center</a></li>
                                    
                                    
                                    <br><br>
                                    
                                    
                                    
                                 </div>
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="OIT" id="OIT"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">OIT Management and Office Support</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=288.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=288.html">John Slot</a>, VP, Info Technology and CIO<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=295.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=295.html">Emily Blinn</a>, Executive Assistant, Sr<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="NET" id="NET"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Network &amp; Infrastructure Services</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=292.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg1413321971244709250.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=292.html">Ken Rivera</a>, Dir, Network &amp; Infrastructure<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=210.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-1666813394281763837.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=210.html">Michael OLoughlin</a>, Network Administrator, Sr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=127.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg1222904084144100485.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=127.html">Ed Delgado</a>, Systems Administrator<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=185.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=185.html">Marc Moseley</a>, Systems Administrator<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=7.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg3707120578334848357.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=7.html">Kevin Matis</a>, Server Administrator, Sr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=282.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg2164505771523392627.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=282.html">Dennis Roehrich</a>, Server Administrator, Sr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=283.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-6001017479966499620.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=283.html">Rickardo Stupart</a>, Server Administrator<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=284.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=284.html">Earl Hoffman</a>, Network Administrator, Sr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=10.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-3743067055270554044.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=10.html">Josh Focht</a>, Network Administrator<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="APS" id="APS"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Enterprise Application Services</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=161.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-5063304223890409133.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=161.html">Seshagiri Parupalli</a>, IT Database Administrator, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=118.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=118.html">Lee White</a>, IT Mgr, Enterprise App Svcs<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=213.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=213.html">Dwayne Smoot</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=293.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=293.html">Liz Gangemi</a>, Functional IS Support Spec<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=286.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=286.html">Bernstein Stephen</a>, Functional IS Support Spec<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=248.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-8714410435407291581.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=248.html">Lymari Maldonado</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=266.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=266.html">Edwin Cruz</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=268.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-6482285164309833428.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=268.html">Irving Ortega Rodriguez</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=274.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg7007574699123477797.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=274.html">Carlos Pena</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=171.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg2980074010503461491.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=171.html">Andy Mobley</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=126.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg8177568503406814044.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=126.html">Nathan Nguyen</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=195.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg5046269512645349974.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=195.html">John Reilly</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=133.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg7420713406081034669.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=133.html">Steve Sheikh</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=193.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg5224811995464472532.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=193.html">Troy Timberman</a>, IT Database Administrator<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="DWD" id="DWD"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Enterprise Application Services - Data Warehouse Development</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=156.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-6611825704536642561.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=156.html">Juan Olvera</a>, IT Mgr, Decision Support Svcs<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=253.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg4049220333659686325.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=253.html">Latife Erdem</a>, State Reporting Analyst/Prgrmr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=254.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg7980516230331255670.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=254.html">Peter Wen</a>, DS Architect/Developer, Sr.<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="IIT" id="IIT"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Web &amp; Portal Services</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=281.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg8204922679192881081.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=281.html">James Flanagan</a>, Dir, Web and Portal Services<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=50.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-3373911936190115648.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=50.html">Jeff Danser</a>, Web/Portal Developer<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=164.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg6868968819874924308.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=164.html">Melanie Hardy</a>, Web/Portal Developer<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=39.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg7031564923450806190.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=39.html">Doug Johnson</a>, Systems Administrator<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=278.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg5150908889298422296.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=278.html">James Jordan</a>, Web/Portal Developer<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=264.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-8750068306403315706.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=264.html">Scott Hood</a>, Web/Portal Developer<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=285.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg4747363759941278516.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=285.html">Jeffrey Reed</a>, Web/Portal Developer<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="ITC" id="ITC"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Learning Technology &amp;  Project Management Services</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=160.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg5441459660440809367.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=160.html">Todd Treece</a>, Dir, Learn Tech/Proj Mgmt Svcs<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=124.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg3530537803181255452.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=124.html">David Renteria</a>, Dir, Enterprise App Services<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=280.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-5799730041557356148.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=280.html">Jessica Bourne</a>, Instructional Design/Developer<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=196.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-3770964911180438967.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=196.html">Josh Murdock</a>, IT Mgr, Inst Design Svcs<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=167.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg6512040638459260677.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=167.html">Michael Maguire</a>, Ops Mgr, Video Productions<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=289.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-5172691310222109276.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=289.html">Nicholas Croft</a>, IT Mgr, Classroom Tech Svcs<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=275.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=275.html">Scott Smith</a>, Producer, Multimedia<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="TSE" id="TSE"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Campus Technology Services - East &amp; Winter Park</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=211.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg2995835552032726243.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=211.html">Jamie Rost</a>, Asst VP, Application<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=249.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg6271245183804134013.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=249.html">Keith Hill</a>, Dir, Campus Technology Svcs E<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=250.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg6508081081300736739.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=250.html">Mortiman Moodie</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=277.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg7452291099851896258.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=277.html">Steve Rukstalis</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=247.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-6893986461201678354.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=247.html">Fernando Gutierrez</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=215.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg4902656906646585964.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=215.html">Steve Suarez</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=216.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg645870269085718446.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=216.html">Joshua Chapkin</a>, Technical Support Specialist<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="TSO" id="TSO"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Campus Technology Services - Osceola &amp; Lake Nona</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=237.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg2662467954307590972.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=237.html">Daniel Charriez</a>, Dir, Campus Technology Svcs O<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=221.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-7951785287891540636.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=221.html">Jorge Soto</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=222.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg20554232639062951.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=222.html">Rooney LaPlante</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=245.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=245.html">Andrew Salaz</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=252.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg3317270296557748216.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=252.html">Mohamed Aleem</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=258.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg2871873899141876750.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=258.html">Jose Diaz</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=259.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=259.html">Ron Ginn</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=279.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg1725077372682509137.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=279.html">Noelia Vasquez</a>, Technical Support Specialist<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div><a name="TSW" id="TSW"></a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong><font size="+1">Campus Technology Services - West &amp; District Center</font></strong> 
                                    
                                    <br><br>
                                    
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=228.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg5067386773727923505.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=228.html">Carmine La Pietra</a>, Dir, Campus Technology Svcs W<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=229.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-8729147613299272986.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=229.html">Mehdi Khoumassi</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=224.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-8519776037625006363.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=224.html">Vinh Nguyen</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=225.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-2764517578339645035.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=225.html">Sean Coamey</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=226.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-8655572338019355131.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=226.html">Edwin Lopez</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=227.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg4045942660506923699.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=227.html">Lorri Saldivar</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=272.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg3040850037185377702.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=272.html">Orin Monrose</a>, IT Mgr, Business &amp; Support Ops<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=251.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg5159341968207009891.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=251.html">David Lazarus</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=230.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-1562738207950930035.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=230.html">John Watson</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=231.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-6347971552372886741.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=231.html">Wes Sondermann</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=260.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg1151205037367488485.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=260.html">Shevon Antoine</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=290.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=290.html">Mike Otero</a>, Tech Support Spec Working Supv<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=287.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=287.html"></a>, <br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=296.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=296.html">Rob Aponte</a>, Technical Support Specialist<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=294.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=294.html">Aaron Ramlochan</a>, Technical Support Specialist<br>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><a href="../../office-of-information-technology/about/staff.html#top">TOP</a></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/about/staff.pcf">©</a>
      </div>
   </body>
</html>