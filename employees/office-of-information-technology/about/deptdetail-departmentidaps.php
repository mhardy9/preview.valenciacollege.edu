<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>About OIT | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/about/deptdetail-departmentidaps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/about/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>About OIT</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/about/">About</a></li>
               <li>About OIT</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>OIT Staff Detail</h2>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    <strong><font size="+1">Enterprise Application Services</font></strong><br><br>      
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    The Enterprise Application Services area of OIT provides maintenance,
                                    support, and development of various PC, network-based, applications
                                    and systems at the college.<br><br>      
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <ul>
                                       
                                       <li>Develop, support, and maintain various PC, network, and
                                          web-based software applications, including: 
                                          <ul>
                                             
                                             <li>Florida FACTS
                                                programs and interfaces         (transcript, degree audit, degree
                                                shopping, admission application, degree search, etc.)
                                             </li>
                                             
                                             <li>Financial Aid software systems (file transfer, Title IV refunding,
                                                short-term loans, Department of  Education financial aid
                                                software)
                                             </li>
                                             
                                             <li>Application database systems (Security Vehicle
                                                Tracking, Gear Up Grant, Upward Bound Grant, Career Center)
                                                
                                             </li>
                                             
                                             <li>Programming Request System (PRS - a internally developed
                                                system for allowing users to submit programming requests and track
                                                them through completion and user-signoff)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Provide
                                          training and customized application development as  needed
                                       </li>
                                       
                                       <li>Manage all PC/network programming requests in accordance with
                                          user-driven priorities as defined in monthly user meetings and hrough
                                          the automated PRS system
                                       </li>
                                       
                                       <li>Support, maintain, and develop the
                                          student information system in accordance with college, state, and
                                          federal regulations [Modules supported include: admissions, records,
                                          registration, master scheduling, degree audit, financial aid, accounts
                                          receivable, cash receipts, and advising]
                                       </li>
                                       
                                       <li>Support college,
                                          state, and federal reporting     requirements, including Florida state
                                          databases for student, personnel, and facilities
                                          information
                                       </li>
                                       
                                       <li>Support, maintain, and develop college mainframe
                                          applications and interfaces for the Florida Academic Counseling and
                                          Tracking for Students (FACTS) system
                                       </li>
                                       
                                    </ul>      
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    <strong>Location:&nbsp;&nbsp;</strong>West Campus SSB 234<br><br>      
                                 </div>
                                 
                              </div>
                              
                              
                              <div> 
                                 
                                 <div> 
                                    <strong>Web Site :&nbsp;&nbsp;</strong><a href="../index.html">http://valenciacollege.edu/office-of-information-technology/</a><br><br>      
                                 </div>
                                 
                              </div> 
                              
                              <div> 
                                 
                                 <div> 
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Staff</div>
                                 
                                 <div>Hours</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=161.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg6295908767630538800.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=161.html">Seshagiri Parupalli</a>, IT Database Administrator, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=118.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=118.html">Lee White</a>, IT Mgr, Enterprise App Svcs<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=213.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=213.html">Dwayne Smoot</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=248.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-5370877557384336018.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=248.html">Lymari Maldonado</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=266.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=266.html">Edwin Cruz</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=268.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg4057767113809088786.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=268.html">Irving Ortega Rodriguez</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=274.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg3869900231212121526.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=274.html">Carlos Pena</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=286.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=286.html">Bernstein Stephen</a>, Functional IS Support Spec<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=293.html">
                                       <img alt="Employee Photo" border="0" height="30" src="default_profile.jpg" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=293.html">Liz Gangemi</a>, Functional IS Support Spec<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=171.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-7827527533932600263.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=171.html">Andy Mobley</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=126.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-5417733451843639104.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=126.html">Nathan Nguyen</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=195.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-4533834412617414787.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=195.html">John Reilly</a>, Programmer Analyst<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=133.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-377135388854908941.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=133.html">Steve Sheikh</a>, Programmer Analyst, Sr.<br>
                                    
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=193.html">
                                       <img alt="Employee Photo" border="0" height="30" src="_cfimg-7533556567397214514.PNG.html" width="30">
                                       </a>&nbsp;
                                    
                                    <a href="../../office-of-information-technology/about/staffDetail.cfm-EmployeeID=193.html">Troy Timberman</a>, IT Database Administrator<br>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>Monday</div>
                                             
                                             <div>7:00 AM - 5:30 PM&nbsp;</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Tuesday</div>
                                             
                                             <div>7:00 AM - 5:30 PM&nbsp;</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Wednesday</div>
                                             
                                             <div>7:00 AM - 5:30 PM&nbsp;</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Thursday</div>
                                             
                                             <div>7:00 AM - 5:30 PM&nbsp;</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Friday</div>
                                             
                                             <div>7:00 AM - 5:30 PM&nbsp;</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Saturday</div>
                                             
                                             <div>Closed&nbsp;</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Sunday</div>
                                             
                                             <div>Closed&nbsp;</div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        
                        
                        
                        
                        
                        
                        <p><a href="../../office-of-information-technology/about/deptDetail.cfm-DepartmentID=APS.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/about/deptdetail-departmentidaps.pcf">©</a>
      </div>
   </body>
</html>