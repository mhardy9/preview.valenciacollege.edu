<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../index.php">Technology Support Services</a></li>
<li><a href="../helpdesk/index.php">Help Desk Services</a></li>
<li><a href="../../../office-of-information-technology/tss/helpdesk/Request_account.php">Account Request Forms</a></li>
<li><a href="../../../office-of-information-technology/tss/helpdesk/Request_DW.php">Data Warehouse Request</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/tss/helpdesk/deptMoves.cfm" target="_blank">Department Moves</a></li>
<li><a href="../helpdesk/index.php">Request of Tech Support</a></li>
<li><a href="index.php">Technology Support &amp; Refresh Services</a></li>
<li><a href="../../../office-of-information-technology/tss/technology/standards.php">Computer Hardware &amp; Software Standards</a></li>
<li><a href="../../../office-of-information-technology/tss/technology/TechnologyPurchasing.php">Standard Configurations</a></li>
<li><a href="../../../office-of-information-technology/tss/technology/techRefresh.php">Technology Refresh</a></li>
<li><a href="../computerlab/index.php">Computer Lab Technology Service</a></li>
<li><a href="../../../office-of-information-technology/tss/computerlab/Software_Purchase_Procedure.php">Software Procurement</a></li>
<li><a href="../../../office-of-information-technology/tss/computerlab/Software_Licenses.php">Available Software &amp; Licensing</a></li>
<li><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.php">Lab Manager Contacts</a></li>
<li><a href="../cts/index.php">Classroom Technology Services</a></li>
<li><a href="../cts/tierclass/index-SmartClassroom=true.php">Tier Classrooms</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/purpose.php">Mission and Purpose</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/equipment.php">Equipment</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/rooms.php">Classrooms</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/brightroom.php">Classroom TiER Levels</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/panorama.php">360 Panorama</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/training.php">Training</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/policies.php">Policies</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/procedures.php">Procedures</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/troubleshooting.php">Troubleshooting</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/tierclass/contact.php">Contact Us</a></li>
<li><a href="../cts/videoconferencing/index.php">Video Conferencing</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/videoconferencing/missionAndPurpose.php">Mission and Purpose</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/videoconferencing/rooms.php">Rooms</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/videoconferencing/training.php">Training</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/videoconferencing/etiquette.php">V-Etiquette</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/videoconferencing/schedulingProcedure.php">Scheduling Procedure</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/videoconferencing/desktopSoftware.php">Desktop Software</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/videoconferencing/contactUs.php">Contact Us</a></li>
<li><a href="../cts/digitaltv/index.php">Digital TV</a></li>
<li><a href="../../../office-of-information-technology/tss/cts/digitaltv/contactUs.php">Contact Us</a></li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
