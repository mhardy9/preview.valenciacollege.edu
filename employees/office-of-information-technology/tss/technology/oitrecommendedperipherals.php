<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/technology/oitrecommendedperipherals.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/technology/">Technology</a></li>
               <li>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <h2>OIT Recommended Peripherals</h2>
                                 </div>
                                 
                                 <div>
                                    <h2>
                                       <a href="javascript:%20window.print()">Print Page</a> 
                                    </h2>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <p><strong>Headset</strong> <br>
                                             Microsoft LifeChat LX-4000 for Business<br>
                                             Part #:7YF-00001
                                          </p>
                                          
                                       </div>
                                       
                                       <p><img alt="Microsoft LifeChat LX-4000" height="250" hspace="0" src="Microsoft_LifeChat_000.jpg" vspace="0" width="185"></p>
                                       
                                       <p>Skype for Business Certified<br>
                                          Noise canecelling microphone<br>
                                          Inline Volume and Microphone Controls<br>
                                          USB Interface
                                       </p>                      
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          
                                          <p><strong>Webcam</strong><br>
                                             Microsoft LifeCam Cinema <br>
                                             Part #: H5D-00013
                                          </p>
                                          
                                          <p><img alt="Microsoft LifeCam Cinema" height="145" src="Microsoft_LifeCam_Cinema_000.jpg" width="129"></p>
                                          
                                          <p>Skype for Business Certified<br>
                                             720p HD Video Chat<br>
                                             Auto Focus<br>
                                             360-degree rotation<br>
                                             Wideband microphone for premium sound recording<br>
                                             Wide-angle lens
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><strong>Wireless Keyboard and Mouse</strong><br>
                                             Wireless Desktop 2000<br>
                                             Part #:
                                             
                                             
                                             M7J-00001 <img alt="Microsoft Wireless KBM" height="189" hspace="0" src="MicrosoftKBM.jpg" vspace="0" width="343"></p>
                                          
                                          <p>Up to 15 months of battery<br>
                                             USB Receiver 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>**If you feel that these devices do not suit your needs, you can <a href="mailto:quoterequest@valenciacollege.edu">request a quote</a>.
                        </p>
                        
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/OITRecommendedPeripherals.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/technology/oitrecommendedperipherals.pcf">©</a>
      </div>
   </body>
</html>