<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/technology/standards.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/technology/">Technology</a></li>
               <li>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <h2>Computer Hardware &amp; Software Standards </h2>
                        
                        <p><strong>I. Supported  Computer Hardware Desktop Platforms</strong>&nbsp;<br>Valencia College will provide, via the Office of Information Technology, official
                           support for Intel/Windows-based and Apple (Mac OS) computer systems. This policy respects
                           situations where non-standard computer systems may need to be purchased for specialized
                           functions and/or applications. Official support for non-standard systems will only
                           be provided when such hardware environments have been approved in advance by the Chief
                           Information Officer (CIO) or his designee.<br>
                           <br>
                           Computers supplied to faculty and staff for office work and administrative applications
                           must possess certain functionality and should be compatible across all networks, departments
                           and campuses in order to facilitate ease of communication and use of college-wide
                           applications. Academic system requirements should be based on the required functionality
                           of the academic area or academic program that those systems will support. In some
                           cases, conflicts may occur between required academic and administrative software on
                           computer systems. In those cases, the College will attempt to provide a solution to
                           the conflict and/or suggest alternative configurations. 
                        </p>
                        
                        
                        <p><strong>II.&nbsp; Supported File and Application Server  Platforms</strong> <br>
                           The Office of Information Technology must approve all file and application servers
                           that connect to Valencia’s network infrastructure.&nbsp; All such file and application
                           servers will be subject to network and security configurations as required by the
                           Office of Information Technology. This requirement is necessary to ensure the integrity
                           and security of Valencia’s overall network infrastructure. Only file and application
                           servers that have been previously approved by The Office of Information Technology
                           will be supported by The Office of Information Technology.
                        </p>
                        
                        
                        <p><strong>III. Method  and Level of Hardware Support</strong>&nbsp;<br>
                           Faculty and staff computers will be supported through the <a href="../helpdesk/index.html">Office of Information Technology Service Desk </a>. Computers in academic labs, classrooms, and LRCs will be primarily supported by
                           their respective department or lab personnel. Additional support may be requested
                           from the <a href="../helpdesk/index.html">Office of Information Technology Service Desk</a> and will be provided based on availability. 
                        </p>
                        
                        <p>The Office of Information Technology will provide primary support for all file and
                           application servers that it manages. File and application servers managed by other
                           areas are primarily the responsibility of their respective department or lab personnel.
                           Additional support may be requested from the <a href="../helpdesk/index.html">Office of Information Technology Service Desk</a> and will be provided based on availability.
                        </p>
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p><strong>IV. 
                              
                              
                              <strong>Guidelines for Computer Hardware and Software Purchasing and Configurations</strong> </strong><br>To ensure proper hardware configuration, software compatibility, and the ability to
                           provide technical support, all requests for acquisitions of computer and network equipment
                           (including servers and printers) must adhere to the following procedures:
                        </p>
                        
                        <blockquote>
                           
                           <p><br>
                              A. Any desktop or laptop computer system configuration that meets all of the ACCEPTABLE
                              criteria in the <a href="../../../office-of-information-technology/tss/technology/standards.html">OIT Computer Hardware and Software Standards</a> may be requested without contacting the Office of Information Technology or the CIO
                              for review/approval.<br>
                              <br>
                              B. Any desktop or laptop computer system configuration that meets any of the EXCEPTIONS
                              criteria in the <a href="../../../office-of-information-technology/tss/technology/standards.html">OIT Computer Hardware and Software Standards</a> must first be reviewed and approved by the Office of Information Technology or the
                              CIO. The listing of an item in the EXCEPTIONS list does not necessarily imply that
                              the item cannot be ordered/purchased. Rather, this designation notes that the item
                              must be reviewed to determine user needs, support issues, etc.
                           </p>
                           
                           <p><br>
                              C. Any desktop or laptop computer system configuration request must be coordinated
                              with the Office of Information Technology to determine if OIT support will be required
                              for installation and setup and related scheduling/delivery issues.<br>
                              <br>
                              
                           </p>
                           
                        </blockquote>            
                        
                        <p><strong><u>Hardware/Software Configuration Guidelines Last Update: &nbsp;December 15, 2014</u></strong></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>ACCEPTABLE </strong>(no review/approval needed)
                                       </p>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong>EXCEPTIONS</strong>(review/approval necessary)
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong><u>Hardware</u></strong><br>
                                          Please see the current hardware technology standards by following the link below:
                                       </p>
                                       
                                       <p><a href="../../../office-of-information-technology/tss/technology/TechnologyPurchasing.html">Technology Standard Configurations</a></p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong><br>
                                             </strong></p>
                                       
                                       <p>All hardware that does not conform to Valencia OIT Standards</p>
                                       
                                       <p><strong><br>
                                             </strong>Apple/Macintosh® desktops and laptops<br>
                                          
                                       </p>
                                       
                                       <p>All file, terminal, remote access, and application servers</p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong><u>Desktop/LaptopOperating Systems Software</u> &nbsp;<br>
                                             </strong>Microsoft Windows 8 64-bit
                                       </p>
                                       
                                       <p>Microsoft Windows 7 64-bit </p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><br>
                                          
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Apple Macintosh® OS&nbsp; X.x (pending review/approval on hardware configuration)</p>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p><strong>V. Computer Peripheral  Guidelines</strong> <br>
                           Each department should contact the Office of Information Technology for purchasing
                           guidelines for peripheral items such as printers, scanners, etc. Requests for assistance
                           specifying or obtaining a quote for computer-related items can be sent to <a href="mailto:quoterequest@valenciacollege.edu">quoterequest@valenciacollege.edu</a></p>
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p><strong>VI. Network vs.  Stand-alone Printers</strong>&nbsp;<br>
                           Volume of printing should determine printer needs. A network printer in a central
                           location is usually the most cost efficient configuration and recommended whenever
                           possible. If a centralized printer cannot be used, a standalone inkjet or laser printer
                           should be purchased depending on usage requirements by the particular staff member.
                           Hewlett-Packard (HP) printers are recommended for most printing applications.
                        </p>
                        
                        <p><br>
                           
                        </p>
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p><strong>VII. Computer Labs and  Computer Classrooms</strong>&nbsp;<br>
                           The following steps outline the process for building a new computer lab or classroom.
                           It is critical that each of the following points be addressed prior to the ordering
                           any equipment. Planning will assist in efficient allocation of assets and lessen delays
                           in the process.
                        </p>
                        
                        <ol>
                           
                           <ol>
                              
                              <li>
                                 <strong> Power</strong> <br>
                                 Determine if the existing power can support the needs of the workstations that will
                                 be installed. If it does not, then new power must be installed. Also, consider whether
                                 the placement of power outlets suits the design of the lab.
                              </li>
                              
                              <li>
                                 <strong>Air Conditioning and Lighting</strong> <br>
                                 Can the air conditioning handle the additional load? Is the location to bright/dark
                                 for computers?
                              </li>
                              
                              <li>
                                 <strong>Communications</strong> - Does the location of the new lab have pre-existing network wiring and network equipment?
                              </li>
                              
                              <li>
                                 <strong>Design</strong> <br>
                                 Does the design meet the state of Florida and ADA requirements for computer labs?
                                 All labs built at Valencia College will provide for and comply with all current laws
                                 and regulations outlining support for disabled students. This includes, but is not
                                 limited to, floor plans, furniture, and computer equipment.
                              </li>
                              
                              <li>
                                 <strong>Workstations</strong> <br>
                                 How many workstations can be safely installed in the location of the new lab? Do not
                                 attempt installation of 30 computers in a room that can only hold 20. Do the workstations
                                 meet the requirements of the software to be used?
                              </li>
                              
                              <li>
                                 <strong>Server</strong><br>
                                 Is a file/application server needed? Who will support the server? Where will it be
                                 housed? Is an existing server going to be utilized or does one needed to be purchased?
                                 If using an existing server, does it have enough hard disk space and RAM to handle
                                 the new software? If a server must be purchased, determine the requirements of the
                                 software for a server. The College strongly recommends the consolidation of servers
                                 when at all possible.
                              </li>
                              
                              <li>
                                 <strong>Software</strong> <br>
                                 If using existing software, do additional software licensing agreements and concurrent
                                 user licenses need to be purchased? Also consider the hardware and OS requirements.
                                 Does the College support the operating system? What type of administrative support
                                 is needed for the software or end user?
                              </li>
                              
                              <li>
                                 
                                 <div>                    <strong>Furniture</strong> <br>
                                    Does the furniture meet OSHA Safety Regulations? Does it comply with ADA requirements?
                                    
                                 </div>
                                 <br>
                                 
                              </li>
                              
                           </ol>
                           
                        </ol>
                        
                        <p>The design, power and communications for a lab or classroom are strongly dependent
                           on one another. In a typical environment, if power needs to be installed, it is completed
                           first, followed by communications. The furniture is installed before the communications
                           is finished, giving the ability to install network cables. Finally, the workstations
                           are installed. Before submitting general requisitions for hardware and software purchases,
                           power and communications should be budgeted and installed. Also, it must be determined
                           whether the requirements of the software can be met. Often, more than half of the
                           cost of a lab or classroom is in the power and networking of the room/area. 
                        </p>            
                        
                        <blockquote>&nbsp;
                           
                        </blockquote>            
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p><strong>VIII. Software Standards and  Support</strong>&nbsp;<br>
                           The table below entitled Supported Software Packages lists the software packages that
                           are officially supported by the Office of Information Technology. Items listed under
                           LIMITED SUPPORT may include new software systems being pilot tested and/or software
                           which is being phased-out. In some cases, it may be necessary to upgrade a system
                           to a “FULL SUPPORT” software version to resolve issues/problems. <br>
                           &nbsp;
                        </p>
                        
                        <p><strong><u>Supported Software Packages Last Update: December 15, 2014 </u></strong></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>FULL SUPPORT<u></u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p><strong>LIMITED SUPPORT</strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong><u>Operating Systems</u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Microsoft Windows® 8 64-bit </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Microsoft Windows® 7 64-bit</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Microsoft Windows® Server 2008 (OIT managed servers only)</p>
                                    </div>
                                    
                                    <div>
                                       <p>Microsoft Windows® Server 2003 (OIT managed servers only)</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Apple ® OS, X.x</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong><u>Office Suites</u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Microsoft Office® 2013, 2010 (Windows)<strong><u></u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p>OSX (Apple OS)<strong><u></u></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Microsoft Office® 2011 (Apple OS)</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong><u>Anti-Virus Software</u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Microsoft ForeFront </p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong><u>Internet Browsers</u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Microsoft® Internet Explorer v9.0 and above</p>
                                       
                                    </div>
                                    
                                    <div>
                                       <p>Safari&nbsp; (Apple OS version only)<br>
                                          
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Mozilla® Firefox v3.6 and above </div>
                                    
                                    <div>
                                       <p>Google Chrome </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong><u>Email Software</u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Outlook 2013</div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Outlook 2010</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Atlas E-mail</p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong><u>Other Software</u></strong></p>
                                    </div>
                                    
                                    <div>
                                       <p><strong></strong></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Microsoft VPN Client </div>
                                    
                                    <div>
                                       <p>Faronics® Deep Freeze Enterprise Edition<br>
                                          
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>Microsoft® SharePoint Designer<u></u></p>
                                    </div>
                                    
                                    <div>
                                       <p>Microsoft® FrontPage 2002, 2003</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>GoToMeeting</div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>TurnItin</div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Respondus</div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>StudyMate</div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Micrograde (WEC only) </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Contribute</div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>AutoCAD 2009 and above </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SkillsTutor</div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Schoolvue</div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><br>
                           
                        </p>
                        
                        <p><strong><u>Notes:</u></strong></p>
                        
                        <p>1. This table will be reviewed and updated regularly.</p>
                        
                        <p>2. When combined with non-supported software applications the above software packages
                           may not function as intended. The College will attempt to provide support for such
                           configurations on a limited basis. Support for all other software applications will
                           be the primary responsibility of the department that purchased the software unless
                           an agreement has been made with the Office of Information Technology for centralized
                           support.
                           
                        </p>
                        
                        <p><br>
                           <strong>IX.&nbsp; Required Software Applications</strong>&nbsp;<br>Certain software applications are required to be installed and operating on all college-owned
                           computers and file servers unless permission has been received from the Office of
                           Information Technology to remove or disable these applications. The required software
                           applications are:
                        </p>
                        
                        <p>1.&nbsp; Anti-Virus Software (Microsoft ForeFront or approved alternative)</p>
                        
                        
                        <p>It is a violation of college policy to remove, disable, or tamper with the above</p>
                        
                        <p>software systems on any college-owned computer unless prior permission has</p>
                        
                        <p>been granted.</p>
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p><strong>X.&nbsp; Preparing for your new computer</strong><br>
                           While OIT recommends that users save their  important files on the “I” network drive,
                           many users still save files to the  hard drive of their computers.&nbsp; In the  event
                           the hard drive files are not backed up and there is a hard drive failure,  the user
                           could lose their files, coursework, College documentation, which may  be unretrievable.&nbsp;
                           In addition, each user  is responsible for the files and data in their possession.&nbsp;
                           Considering this, OIT requires users to  consider the following:
                        </p>
                        
                        <p><strong>Once a new computer is  installed all documents/files on the hard drive(s) of the
                              old computer will be  erased</strong> so before an OIT technician arrives to set up your new computer, you will need  to:
                        </p>
                        
                        <p>1.&nbsp;  Move any documents (typically Excel, Word, Power Point, Access, etc.)  you have
                           stored on your computer to your personal network drive (the “I” drive,  which is backed
                           up regularly).&nbsp; If you  require assistance with this please contact the Service Desk
                           or advise the  Technical Support Specialist when you are contacted about the installation.<strong>&nbsp; We will not be able to set up your new  computer until the files have been removed
                              from the old computer.</strong></p>
                        
                        <p>2.&nbsp; Record any software license keys or similar information that may be necessary
                           when OIT installs your new computer 
                        </p>
                        
                        <p>Other items of importance:</p>
                        
                        <ol>
                           
                           <li>Only properly licensed software may be installed on college owned  systems.&nbsp; For any
                              additional software  needs please have the original installation media and licensing
                              information  available.&nbsp; (Note:&nbsp; All computer system configurations must  conform
                              to the guidelines as set forth in the College’s IT Policies. These  policies are available
                              online at <u><a href="../../../office-of-information-technology/about/policiesProcedures.html">http://valenciacollege.edu/itpolicies</a></u>).
                           </li>
                           
                           <li>Desktop  wallpaper, screen savers, music/MP3 files and such are typically not business
                              or mission critical and are generally unsupported, nor are these settings  preserved.
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p><strong>XII.&nbsp; Personal Home Computers</strong><br>
                           <br>
                           OIT does not support personally  purchased computers.&nbsp; However, we will  attempt to
                           answer any questions over the phone that relate to connectivity to  College services,
                           such as Atlas.&nbsp; If the  user reports use of a firewall or popup blocker, the user
                           will need to contact  outside support for assistance.
                        </p>
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/standards.html#top">TOP</a></p>
                        
                        <p>-Last Updated on April 16, 2015- </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/technology/standards.pcf">©</a>
      </div>
   </body>
</html>