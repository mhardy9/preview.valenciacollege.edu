<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/technology/technologypurchasing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/technology/">Technology</a></li>
               <li>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>
                           <a name="content" id="content"></a>
                           <a href="index.html">
                              
                              </a>
                           Technology Purchasing
                           
                        </h2>
                        
                        <h4>We have adjusted the purchasing of technology items into 2  steps:</h4>
                        
                        <p><strong>STEP 1</strong>:
                        </p>
                        
                        <ul>
                           
                           <li>Select your item(s) from the list of approved devices below, then email <a href="mailto:quoterequest@valenciacollege.edu">Quote Request</a> with the items and quantities. 
                           </li>
                           
                        </ul>
                        
                        <p>If you do not see a specific item or you require  a non-standard device please email
                           <a href="mailto:quoterequest@valenciacollege.edu">Quote Request</a> with specific details of the requested item(s). 
                        </p>
                        
                        <p><br>
                           <strong>STEP 2</strong>:
                        </p>
                        
                        <ul>
                           
                           <li> Create a Requisition,<span> making sure the ship to address is:<strong> OIT - West Campus 4-12, 1800 S. Kirkman Rd</strong></span><strong>.</strong>
                              
                           </li>
                           
                           <li>Once the item arrives, OIT will have it tagged, processed and  sent to the requester.<br>
                              
                           </li>
                           
                           <li>Other peripherals such as webcams, keyboards, and headsets can be ordered by your
                              department. Recommended peripherals can be found <a href="../../../office-of-information-technology/tss/technology/OITRecommendedPeripherals.html">here.</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p>List of OIT Approved Devices</p>
                        
                        <h3>LAPTOPS</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Dell Latitude 5289</h3>
                                    
                                    <p><img alt="Dell Latitude e5450" height="113" hspace="0" src="laptop-latitude-5289.jpg" vspace="10" width="245"></p>
                                    
                                    <p><strong>Total Price: $1399.99</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Intel® Core™ i7-7600U Processor</li>
                                       
                                       <li>16GB Memory</li>
                                       
                                       <li>Intel® Core i7-7600U vPro CPU</li>
                                       
                                       <li>256GB Solid State Drive</li>
                                       
                                       <li>FHD (1920 x 1080) Touch LCD with Mic/Camera</li>
                                       
                                       <li>Intel® 8265 wi-fi + BT 4.2 Wireless Card (2x2)</li>
                                       
                                       <li>60 WHr Polymer Primary Battery</li>
                                       
                                       <li>Dell Active Pen</li>
                                       
                                       <li>4-Year Accidental Damage Protection</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Apple MacBook Pro 13</h3>
                                    
                                    <p><img alt="Apple Macbook Air 13" height="166" hspace="0" src="macbookpro-above.jfif" vspace="0" width="180"> 
                                    </p>
                                    
                                    <p><strong>Total Price: $1778.00</strong></p>
                                    
                                    <ul>
                                       
                                       <li>2.3GHz Processor&nbsp;</li>
                                       
                                       <li>256GB Storage</li>
                                       
                                       <li>2.3GHz dual-core 7th-generation Intel Core i5 processor</li>
                                       
                                       <li>Turbo Boost up to 3.6GHz</li>
                                       
                                       <li>8GB 2133MHz LPDDR3 memory</li>
                                       
                                       <li>256GB SSD storage1</li>
                                       
                                       <li> Intel Iris Plus Graphics 640</li>
                                       
                                       <li> Two Thunderbolt 3 ports</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>TABLETS</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Apple iPad  Wi-Fi 32GB</h3>
                                    
                                    <p><img alt="Apple iPad Air " height="171" src="AppleIpadMini2_000.png" width="150"></p>
                                    
                                    <p><strong>Total Price: $398.00</strong></p>
                                    
                                    <ul>
                                       
                                       <li>        9.7" Screen (2048x1536)</li>
                                       
                                       <li> 32GB of flash storage</li>
                                       
                                       <li> Wi-Fi (802.11 a/b/g/n/ac)</li>
                                       
                                       <li> 3-Year AppleCare+</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Apple iPad Mini 4 128GB</h3>
                                    
                                    <p><img alt="Apple iPad Mini 2" height="171" src="AppleIpadMini2_001.png" width="150"></p>
                                    
                                    <p><strong>Total Price: $478.00</strong></p>
                                    
                                    <ul>
                                       
                                       <li>        7.9" Screen (2048x1536)</li>
                                       
                                       <li>128GB of flash storage </li>
                                       
                                       <li> Wi‑Fi (802.11a/​b/​g/​n/​ac)</li>
                                       
                                       <li>3-Year AppleCare+</li>
                                       
                                    </ul>      
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>PRINTERS</h3>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Dell E310dw Monochrome Laser Printer</h3>
                                    
                                    <p><img alt="Dell E310dw" height="170" hspace="0" src="Dell_E310dw_000.png" vspace="0" width="280"></p>
                                    
                                    <p><strong>Total Price: $108.74</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Print resolution: 600 x 600 dpi (Up to 2400 x 600 dpi)</li>
                                       
                                       <li>Recommended Monthly Print Volume: 250 - 1,000 pages</li>
                                       
                                       <li>250-sheet input tray</li>
                                       
                                       <li>Interfaces: USB 2.0 High Speed, Ethernet        </li>
                                       
                                    </ul>
                                    
                                    <p><a href="http://accessories.us.dell.com/sna/productdetail.aspx?c=us&amp;cs=04&amp;l=en&amp;s=bsd&amp;sku=210-AEHL&amp;redirect=1">Dell Specifications Link </a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Dell S2830dn Smart Monochrome Laster Printer </h3>
                                    
                                    <p><img alt="Dell Smart Printer | S2830dn" height="184" src="dell-s2830dn-printer-hero_000.jpg" width="252"> <br>
                                       <strong>Total Price: $190.61</strong></p>
                                    
                                    <ul>
                                       
                                       <li>  Print Resolution: 1200x1200 dpi </li>
                                       
                                       <li>Recommended Monthly Print Volume: 750 - 2,500 pages</li>
                                       
                                       <li>150-sheet Standard tray</li>
                                       
                                       <li>Interfaces: USB 2.0 High Speed, Ethernet        </li>
                                       
                                    </ul>
                                    
                                    <p><a href="http://accessories.us.dell.com/sna/productdetail.aspx?c=us&amp;cs=04&amp;l=en&amp;sku=225-4035">Dell Specifications Link</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Dell C1760nw Color Laser Printer</h3>
                                    
                                    <p><img alt="Dell C1765nw" height="180" hspace="0" src="Dell_C1765nfw_001.png" vspace="0" width="242"></p>
                                    
                                    <p><strong>Total Price: $195.74</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Print resolution: 600 x 600 dpi</li>
                                       
                                       <li>150-sheet input tray</li>
                                       
                                       <li>Recommended Monthly Print Volume: 250 - 1,000 pages</li>
                                       
                                       <li>Interfaces: USB 2.0 High Speed, Ethernet</li>
                                       
                                    </ul>
                                    
                                    <p><a href="http://accessories.us.dell.com/sna/productdetail.aspx?c=us&amp;cs=04&amp;l=en&amp;sku=225-4111">Dell Specifications Link</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Dell C2660dn Color Laser Printer</h3>
                                    
                                    <p><img alt="Dell C2660dn" height="180" hspace="0" src="Dell_C2660dn_001.png" vspace="0" width="217"></p>
                                    
                                    <p><strong>Total Price: $204.74</strong></p>
                                    
                                    <ul>
                                       
                                       <li>Print resolution: 600 x 600 dpi</li>
                                       
                                       <li>250-sheet input tray</li>
                                       
                                       <li>Recommended Monthly Print Volume: 750 to 3,500 pages</li>
                                       
                                       <li>Interfaces: USB 2.0 High Speed, Ethernet</li>
                                       
                                    </ul>
                                    
                                    <p><a href="http://accessories.us.dell.com/sna/productdetail.aspx?c=us&amp;cs=19&amp;l=en&amp;sku=210-ABPD">Dell Specifications Link</a> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Dell E514dw Multifunction Printer </h3>
                                    
                                    <p><img alt="Dell Multifunction Printer – E514dw" height="202" src="dell-e514dw-printer-hero_000.jpg" width="202"> 
                                    </p>
                                    
                                    <p><strong>Total Price: $144.99</strong></p>
                                    
                                    <ul>
                                       
                                       <li>  Print resolution: 600 x 600 dpi</li>
                                       
                                       <li>Standard: 250-sheet input tray</li>
                                       
                                       <li>Recommended Monthly Print Volume: 300 - 1,000 pages</li>
                                       
                                       <li>Interfaces: USB 2.0 High Speed, Ethernet</li>
                                       
                                    </ul>
                                    
                                    <p><a href="http://accessories.dell.com/sna/productdetail.aspx?c=us&amp;l=en&amp;s=dhs&amp;cs=19&amp;sku=210-AEHJ">Dell Specifications Link</a> 
                                    </p> 
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Dell E525w Color Multifunction Printer </h3>
                                    
                                    <p><img alt="Dell Color Multifunction Printer — E525w" height="205" src="dell-e525w-printer-hero.jpg" width="205"></p>
                                    
                                    <p><strong>Total Price: $260.99</strong></p>
                                    
                                    <ul>
                                       
                                       <li>  Print resolution: 600 x 600 dpi</li>
                                       
                                       <li>Standard: 150-sheet</li>
                                       
                                       <li>Recommended Monthly Print Volume: 250 - 700 pages</li>
                                       
                                       <li>Interfaces: USB 2.0 High Speed, Ethernet</li>
                                       
                                    </ul>
                                    
                                    <p><a href="http://accessories.dell.com/sna/productdetail.aspx?c=us&amp;l=en&amp;s=dhs&amp;cs=19&amp;sku=210-AEDL">Dell Specifications Link</a> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>PERIPHERALS</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Dell P2017 20" LCD</h3>
                                    
                                    <p><img alt="Dell P2016" height="180" hspace="0" src="P2016_000.png" vspace="0" width="223"></p>
                                    
                                    <p><strong>Total Price: $89.99</strong></p>
                                    
                                    <ul>
                                       
                                       <li>          Viewable Size: 19.5"</li>
                                       
                                       <li>Resolution: 1440x900 @ 60Hz</li>
                                       
                                       <li>Connectivity: DP 1.2, VGA </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Dell UltraSharp U2417HJ 24" LCD</h3>
                                    
                                    <p><img alt="Dell Ultrasharp U2414h" height="180" hspace="0" src="U2414h_000.jpeg" vspace="0" width="199"></p>
                                    
                                    <p><strong>Total Price: $254.99</strong></p>
                                    
                                    <ul>
                                       
                                       <li>  Viewable Size: 23.8"</li>
                                       
                                       <li>Resolution: 1920x1080 @ 60Hz</li>
                                       
                                       <li>Connectivity: (2) HDMI, DP, mDP</li>
                                       
                                    </ul>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/technology/technologypurchasing.pcf">©</a>
      </div>
   </body>
</html>