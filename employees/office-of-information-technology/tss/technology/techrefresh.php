<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/technology/techrefresh.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/technology/">Technology</a></li>
               <li>Technology Support &amp; Refresh ServicesA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <h2>Technology Refresh </h2>
                        
                        <p>The Technology Refresh Program in the Office of Information Technology is responsible
                           for maintaining appropriate lifecycles of existing computers at Valencia College.
                        </p>
                        
                        <ul>
                           
                           <li>Maintain  inventory of all Valencia computers to include location, user, and hardware
                              information
                           </li>
                           
                           <li>Use inventory to address highest priority computer replacement needs at Valencia</li>
                           
                           <li>Oversee procurement, delivery, and installation of new computers to ensure College
                              needs are met
                           </li>
                           
                           <li>Coordinate the  re-use/disposal of older computers that are replaced via the Technology
                              Refresh Program
                           </li>
                           
                        </ul>
                        
                        <p>Computer Refresh Cycle:</p>
                        
                        <p>                Computers are scheduled to be refreshed every four (4) years, beginning
                           with the oldest computers first. When your computer is eligible for refresh you will
                           be contacted by a Technical Services Specialist, with at least a one month notice.<br>
                           <br>
                           Please contact <a href="mailto:refreshrequest@valenciacollege.edu">Refresh Request</a> with the computer Model Number and the Valencia Property Tag number. 
                        </p>
                        
                        <p>Technology Refresh  Links</p>
                        
                        <ul>
                           
                           <li>
                              <img alt="PDF" height="16" src="icon_pdf.gif.html" width="16"> <a href="../../../office-of-information-technology/documents/Instruction%20forCopyingFilesServer.pdf">Instructions for Copying Files to the Server</a>
                              
                           </li>
                           
                        </ul>
                        
                        <p><a href="../../../office-of-information-technology/tss/technology/techRefresh.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/technology/techrefresh.pcf">©</a>
      </div>
   </body>
</html>