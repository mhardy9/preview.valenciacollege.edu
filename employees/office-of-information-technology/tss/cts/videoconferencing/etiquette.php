<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Video ConferencingA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/videoconferencing/etiquette.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/videoconferencing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Video ConferencingA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/videoconferencing/">Videoconferencing</a></li>
               <li>Video ConferencingA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a> 
                        
                        <h2>Proper v-etiquette During Video Conferencing (Desktop)</h2>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                    <ul>
                                       
                                       <li> When using the directory from your desktop software you can only make a point to
                                          point call.
                                       </li>
                                       
                                       <li> For multi point conferences, the conference must be scheduled through Tandberg Management
                                          Suite (TMS). Please reference the procedure document.
                                       </li>
                                       
                                       <li>Before the conference begins, reduce any sources for background noise that may interrupt
                                          your call (i.e. any telephone, TV, music from your PC/laptop, fans, etc.).
                                       </li>
                                       
                                       <li>Try to avoid lighting that points at the camera lens.</li>
                                       
                                       <li>After the connection has been established, check to make sure your audio, video and
                                          camera angle are OK.
                                       </li>
                                       
                                       <li>When speaking look at the camera and speak clearly; do not shout</li>
                                       
                                       <li>Remember you are always on camera, even when you are not speaking</li>
                                       
                                       <li>Wear conservative clothing such as blue or black; avoid white, red, or patterns such
                                          as plaids and stripes. This can be too “busy” for the camera.
                                       </li>
                                       
                                       <li>Be yourself. Try to create a comfortable atmosphere</li>
                                       
                                       <li>Have the phone number for the campus AV department in the event support is needed.</li>
                                       
                                    </ul>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h2>Proper v-etiquette During Video Conferencing (Multi-Point)</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    
                                    
                                    <ul>
                                       
                                       <li>Before conference begins, hand out v-etiquette papers and discuss each item.</li>
                                       
                                       <li>When the conference begins, have each site introduce the participants</li>
                                       
                                       <li>Remote sites should mute their microphones</li>
                                       
                                       <li>Do not speak until properly addressed</li>
                                       
                                       <li>Reduce or eliminate any background noise (i.e.fans, shuffling papers, turn cell phones
                                          to vibrate, etc.)
                                       </li>
                                       
                                       <li>When speaking look into the appropriate camera and speak clearly, do not shout</li>
                                       
                                       <li>Have an agenda and stick to it</li>
                                       
                                       <li>Appoint a meeting facilitator, he or she would be responsible for muting and un-muting
                                          the microphone and changing camera angles
                                       </li>
                                       
                                       <li>After addressing the audience, allow time for remote sites to respond one at a time</li>
                                       
                                       <li>Based on the size of the audience, have a seating arrangement in mind before the conference
                                          begins. (i.e., auditorium seating, town hall, horseshoe table, etc.) This way the
                                          camera presets can be made ahead of time.
                                       </li>
                                       
                                       <li>For multipoint conferences have a sign identifying your ampus/location</li>
                                       
                                       <li>Assume you are always on camera, even when you are not speaking</li>
                                       
                                       <li>Wear conservative clothing such as blue or black; avoid white, red, or patterns such
                                          as plaids and stripes. This can be too “busy” for the camera
                                       </li>
                                       
                                       <li>Be yourself. Try to create a comfortable atmosphere</li>
                                       
                                       <li>Have the phone number for the campus AV department in the event support is needed.</li>
                                       
                                    </ul>
                                    
                                    
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><strong>DOs and DON'TS</strong></div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO pre-plan</div>
                                             
                                             <div> DON'T make distracting sounds</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO speak clearly </div>
                                             
                                             <div>DON'T make distracting movements</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO keep body movements to a minimum</div>
                                             
                                             <div>DON'T cover the microphone</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO move and gesture naturally</div>
                                             
                                             <div>DON'T interrupt other speakers</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO maintain eye contact</div>
                                             
                                             <div>DON'T carry on side conversations</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO dress appropriately</div>
                                             
                                             <div>DON'T wear noisy jewelry</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO apply cosmetics subtly </div>
                                             
                                             <div>DON'T arrive late</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>DO mute your microphone when not speaking</div>
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/videoconferencing/etiquette.pcf">©</a>
      </div>
   </body>
</html>