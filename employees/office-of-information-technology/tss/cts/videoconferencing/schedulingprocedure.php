<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Video ConferencingA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/videoconferencing/schedulingprocedure.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/videoconferencing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Video ConferencingA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/videoconferencing/">Videoconferencing</a></li>
               <li>Video ConferencingA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a> 
                        
                        <h4>PLEASE TRY TO SCHEDULE THE CONFERENCE AT LEAST ONE WEEK IN ADVANCE.</h4>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <br>
                                    <font face="Verdana">To schedule 
                                       a video conference between campuses, please perform the 
                                       following steps:<br>
                                       &nbsp;<br>
                                       (PLEASE NOTE: TMS IS INTERNAL TO VALENCIA ONLY. IT CANNOT BE 
                                       ACCESSED OFF CAMPUS UNLESS YOU USE YOUR VPN ACCOUNT). </font> 
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><font face="Verdana">1.&nbsp; Contact the appropriate personnel on each campus to schedule the room: </font></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <br>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>West Campus 6-202:</div>
                                             
                                             <div>Vivian Martin, ext 1370</div> 
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>West Campus 6-326A:</div> 
                                             
                                             <div>Daniel Charriez, ext 1792</div>
                                             
                                          </div> 
                                          
                                          <div>
                                             
                                             <div>East Campus 3-108E:</div> 
                                             
                                             <div>Samantha Pabon , ext 2007</div> 
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>East Campus 4-149:</div>
                                             
                                             <div>Jackie Melley, ext 2320</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Winter Park 143:</div> 
                                             
                                             <div>Lois Brennan, ext 6800 or Amanda Body, 6874</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div><font face="Verdana">Osceola 2-101:  </font></div>
                                             
                                             <div>Judi Velazquez, ext 4104</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Osceola 2-219B: </div>
                                             
                                             <div>Judi Velazquez, ext 4104 </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Downtown Center 106: </div>
                                             
                                             <div>Patty Nicholas, ext 3465</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Criminal Justice Instituteï¿½1-218:</div> 
                                             
                                             <div> Vicki Pipkin, ext 8017</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <br>
                                    &nbsp;&nbsp;&nbsp; When speaking to the contact person have the following information ready: 
                                    date, &nbsp;&nbsp;&nbsp; start time, duration and subject of the conference.
                                    
                                 </div>
                                 
                              </div>         
                              
                              
                              <div>
                                 
                                 <div>
                                    <font face="Verdana">
                                       <br> 
                                       2.&nbsp; <strong>If you have a TMS user account,</strong> schedule the 
                                       conference in TMS. Click on the<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;link for detailed instructions:&nbsp;&nbsp;
                                       <a href="images/TMSprocedure.pdf">TMS procedure</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <br>
                                    
                                    3.<span>
                                       <strong>If you do not have a TMS account </strong>and would like to request 
                                       one, please fill &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;following fields and select the submit 
                                       button. This information will be forwarded to&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the 
                                       appropriate OIT personnel and the conference will be 
                                       scheduled based on the &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;information provided.&nbsp; You will 
                                       receive an email to confirm the conference has been 
                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;scheduled on the appropriate date, time and locations.&nbsp; 
                                       Please review that the &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;information is correct and forward it 
                                       to all the participants.&nbsp; </span>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>           
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/videoconferencing/schedulingprocedure.pcf">©</a>
      </div>
   </body>
</html>