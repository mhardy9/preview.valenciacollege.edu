<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Video ConferencingA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/videoconferencing/rooms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/videoconferencing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Video ConferencingA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/videoconferencing/">Videoconferencing</a></li>
               <li>Video ConferencingA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a> 
                        
                        <h2>High-end Video tier classrooms (Video Conferencing Capabilities)</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Campus</strong></div>
                                 
                                 <div><strong>Location</strong></div>
                                 
                                 <div><strong>Phone</strong></div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>East </div>
                                 
                                 <div>Bldg. 3, Rm. 108E </div>
                                 
                                 <div>ext. 8851 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Bldg. 4, Rm. 149</div>
                                 
                                 <div>ext. 8969</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Osceola</div>
                                 
                                 <div>Bldg. 2, Rm. 101 (Auditorium)</div>
                                 
                                 <div>ext. 4550</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Bldg. 2, Rm. 219B </div>
                                 
                                 <div> None </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>West </div>
                                 
                                 <div>Bldg. 6, Rm. 202 </div>
                                 
                                 <div>ext. 5900</div>   
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Bldg. 6, Rm. 326A</div>
                                 
                                 <div>ext. 5405</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Bldg. SSB, Rm. 171J</div>
                                 
                                 <div>ext. 1187 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Winter Park </div>
                                 
                                 <div>Rm. 143 </div>
                                 
                                 <div>ext. 6051</div>   
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Downtown Center </div>
                                 
                                 <div>Rm. 106</div>
                                 
                                 <div>ext. 3115</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Criminal Justice Institute </div>
                                 
                                 <div>Bldg. 1, Rm. 218</div>
                                 
                                 <div>None</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>  
                        <br>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <img border="1" height="165" src="west(Rm326a)220(b).jpg" width="220">
                                    
                                 </div>
                                 
                                 <div>
                                    <img border="1" height="165" src="west(Rm202)220(b).jpg" width="220">
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img border="1" height="165" src="winterPark220(b).jpg" width="220">
                                    
                                 </div>
                                 
                                 <div>
                                    <img border="1" height="165" src="osceola220(b).jpg" width="220">
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img border="1" height="165" src="east220(b).jpg" width="220">
                                    
                                 </div>
                                 
                                 <div>
                                    <img border="1" height="165" src="crimJustice220(b).jpg" width="220">
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/videoconferencing/rooms.pcf">©</a>
      </div>
   </body>
</html>