<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Video ConferencingA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/videoconferencing/contactus.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/videoconferencing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Video ConferencingA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/videoconferencing/">Videoconferencing</a></li>
               <li>Video ConferencingA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a> 
                        
                        <h2>Contact Us</h2>
                        
                        &nbsp;
                        <div>
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>East Campus</div>
                                                         
                                                         <div>Osceola Campus</div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <strong>Keith Hill </strong><br>
                                                            <a href="mailto:khill43@valenciacollege.edu">
                                                               khill43@valenciacollege.edu</a><br>
                                                            <strong>Ext: 2392</strong>
                                                            
                                                         </div>
                                                         
                                                         <div>
                                                            <strong>Jorge Soto </strong><br>
                                                            <a href="mailto:jsoto@valenciacollege.edu">
                                                               jsoto@valenciacollege.edu</a><strong><br>
                                                               Ext: 4888 </strong>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>West Campus</div>
                                                         
                                                         <div>Winter Park Campus</div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <strong>Medhi Khoumassi </strong><br>
                                                            <a href="mailto:mkhoumassi@valenciacollege.edu">
                                                               mkhoumassi@valenciacollege.edu</a>&nbsp; <br>
                                                            <strong>Ext: 1577 </strong>
                                                            
                                                            <p>Manager of Classroom Technology<br>
                                                               <strong>
                                                                  <a href="mailto:ccall2@valenciacollege.edu">
                                                                     Chad Call </a><br>
                                                                  Ext: 1230 </strong><br>
                                                               Cell: 407-448-1063
                                                            </p>
                                                            
                                                            <p>Asst to Manager of Classroom Technology<br>
                                                               <strong>
                                                                  <a href="mailto:jcwalker@valenciacollege.edu">JC Walker </a> <br>
                                                                  Ext: 1078 </strong><br>
                                                               Cell: 407-448-1065<br>
                                                               <br>
                                                               
                                                            </p>
                                                            
                                                         </div>
                                                         
                                                         <div>
                                                            <strong>Keith Hill </strong><br>
                                                            <a href="mailto:khill43@valenciacollege.edu">
                                                               khill43@valenciacollege.edu</a>&nbsp;<strong><br>
                                                               Ext: 2392 </strong>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                        </div>           
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/videoconferencing/contactus.pcf">©</a>
      </div>
   </body>
</html>