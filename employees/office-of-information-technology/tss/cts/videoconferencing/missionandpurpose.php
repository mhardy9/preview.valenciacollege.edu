<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Video ConferencingA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/videoconferencing/missionandpurpose.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/videoconferencing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Video ConferencingA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/videoconferencing/">Videoconferencing</a></li>
               <li>Video ConferencingA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a> 
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <h2>Mission Statement</h2>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>The vision statement for Valencia 
                                       Community College is to be an extraordinary learning 
                                       community.&nbsp; Some of the key values to help accomplish this 
                                       are:
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>We value the on-going personal and professional development of learning leaders throughout
                                          the college community
                                       </li>
                                       <br>
                                       
                                       <li>We value community partnerships for workforce development</li>
                                       <br>
                                       
                                       <li>We value the thoughtful use of technology to enhance learning</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    <img border="1" height="199" src="DSC01497small(c).jpg" width="265">
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Video conferencing capabilities at 
                                       Valencia College are an integral component in 
                                       realizing and achieving the mission and values of the 
                                       college.&nbsp; Video conferencing, much like the tier classrooms 
                                       at Valencia, is part of the Learning Technology Alternative 
                                       Delivery group whose mission is to facilitate the 
                                       introduction, development, and use of media and 
                                       communication technologies and services within the college.&nbsp; 
                                       As the college grows and the technology advances, video 
                                       conferencing will greatly benefit those who employ these 
                                       technologies and services in the teaching and learning 
                                       environment as well as the administrative activities of the 
                                       college.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <h2>Purpose and Goals</h2>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <img border="1" height="165" src="DSC01502small.jpg" width="220">
                                    
                                 </div>
                                 
                                 <div>
                                    Central to the goal of video 
                                    conferencing at Valencia College is a cultural 
                                    shift towards a more collaborative and flexible method by 
                                    which disparate departments or groups are able to work 
                                    together toward a common goal and do so expeditiously. The 
                                    video conferencing will, in the long term, allow for desktop 
                                    to desktop communication along with the current capability 
                                    of conferencing between the designated “Video Conference” 
                                    rooms on each of Valencia’s campuses.&nbsp; 
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Our goal is to make the entire system 
                                    as easy to use as possible; as easy as simply placing a 
                                    phone call.&nbsp; To that end, we have provided instructions to 
                                    assist you in scheduling the video conference rooms and have 
                                    outlined some important etiquette rules.&nbsp; Please read 
                                    through all of them and make a mental note of them when you 
                                    are in a video conference.&nbsp; If you have received the tier classroom training, then
                                    the video conferencing element is 
                                    simply the next step in the process and is also very easy.&nbsp; 
                                    It is also very reassuring to know that the video conference 
                                    rooms have the same excellent support system that the tier classrooms have. 
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/videoconferencing/missionandpurpose.pcf">©</a>
      </div>
   </body>
</html>