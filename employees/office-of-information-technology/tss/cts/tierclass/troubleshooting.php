<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tier ClassroomsA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/tierclass/troubleshooting.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/tierclass/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Tier ClassroomsA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/tierclass/">Tierclass</a></li>
               <li>Tier ClassroomsA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <h2>Troubleshooting</h2>
                        
                        <p> <img alt="" border="1" height="173" hspace="10" src="projector_8082.jpg" vspace="20" width="260">Responsibilities include…
                        </p>
                        
                        <ul>
                           
                           <li>    <strong>Classroom support</strong> is available from 7:00 a.m.
                              to 10:00 p.m. Monday
                              through Friday, by calling the AV department on your campus: 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <ul>
                              
                              <li> East - 2419</li>
                              
                              <li>CJI - 8272 </li>
                              
                              <li> West - 1488</li>
                              
                              <li> Winter Park - 6835</li>
                              
                              <li> Osceola - 4150</li>
                              
                           </ul>
                           
                           <blockquote>
                              
                              
                           </blockquote>
                           
                           <li>Routine maintenance matters are reported to the AV Department</li>
                           
                        </ul>
                        
                        
                        <ul>
                           
                           <li><a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/tss/cts/tierclass/contact.cfm" target="_blank">Contact Us</a></li>
                           
                        </ul>
                        
                        
                        <p><span>Multimedia Classroom Troubleshooting Tips</span></p>
                        
                        <p>  Always ask:</p>
                        
                        <ul>
                           
                           <li>Has the “System On” button on the touchpanel
                              been pressed?                                                                    
                              
                           </li>
                           
                           <li>Has the ceiling mounted projector turned on and
                              is the screen down?                                                              
                              
                           </li>
                           
                           <li>Are all the components in the cabinet turned
                              on?                                                                              
                              <em>Look for lights</em>
                              
                           </li>
                           
                           <li> Is the power switch for the
                              monitor at the instructor’s desk turned
                              on? 
                           </li>
                           
                        </ul>
                        <p>  <strong><img border="1" height="300" hspace="10" src="IMG_8101.jpg" vspace="10" width="199"></strong>Problems with Equipment:
                        </p>
                        
                        <p><strong>Ceiling Mounted LCD Projector</strong></p>
                        
                        <ul>
                           
                           <li>Press the “System On” button, if projector is not on. </li>
                           
                           <li>Note:
                              If the screen is raised the projector will automatically shut –off,
                              lowering the screen will not automatically turn the projector back on.
                              You must press the “System On” button.
                              Also, if the projector does not receive a signal for one hour
                              it will shut off.
                           </li>
                           
                        </ul>
                        <p><strong>Document Camera</strong></p>
                        
                        <ul>
                           
                           <li>Has the “System On” button been pressed? Projector
                              on and screen down?
                           </li>
                           
                           <li>Is DOC CAM selected on the touch panel on
                              the Source Page?
                           </li>
                           
                           <li>Did you push the “Send to Screen” button?</li>
                           
                           <li>If the document
                              camera will not stay in focus, press the focus in or out button
                              and focus on the object manually.
                           </li>
                           
                        </ul>
                        <p><strong>VCR</strong></p>
                        
                        <ul>
                           
                           <li>Has the “System On” button been pressed? Projector
                              on and screen down?                                                              
                              
                           </li>
                           
                           <li>Is VCR selected on the touch panel in the
                              Source Page?                                                                     
                              
                           </li>
                           
                           <li>Is the power on? Are there lights on the front panel
                              of the VCR?                                                                      
                              
                           </li>
                           
                           <li>Did you press the “Play” button on the touchpanel?</li>
                           
                           <li>Did you
                              push the                                                                         
                              “Send to Screen” button?
                           </li>
                           
                           <li>Is the tape in?</li>
                           
                           <li>Have you tried another tape? </li>
                           
                        </ul>
                        <p><br>
                           <strong>DVD</strong></p>
                        
                        <ul>
                           
                           <li>Has the “System On” button been pressed? Projector
                              on and screen down?                                                              
                              
                           </li>
                           
                           <li>Is DVD selected on the touch panel in the
                              Source Page?                                                                     
                              
                           </li>
                           
                           <li>Is the power on; are there lights on the front panel
                              of the DVD?                                                                      
                              
                           </li>
                           
                           <li>Did you press the “Play” button?</li>
                           
                           <li>Did you push the “Send
                              to Screen                                                                        
                              ” button?
                           </li>
                           
                           <li>Is the disc in?</li>
                           
                           <li> Have you tried another disc?</li>
                           
                        </ul>
                        <p><br>
                           <strong>Audio Cassette Player</strong></p>
                        
                        <ul>
                           
                           <li> Has the “System On” button been pressed? Projector
                              on and screen down?                                                              
                              
                           </li>
                           
                           <li> Is CASSETTE selected on the touch panel in
                              the Source Page?                                                                 
                              
                           </li>
                           
                           <li> Is the power on; are there lights on the front
                              panel of the cassette player?                                                    
                              
                           </li>
                           
                           <li> Did you press the “Play” button?</li>
                           
                           <li>Did you push the “Send
                              to Speakers                                                                      
                              ” button?
                           </li>
                           
                           <li> Is the tape in?</li>
                           
                           <li> Have you tried another tape?</li>
                           
                        </ul>
                        <p><strong>PC</strong></p>
                        
                        <ul>
                           
                           <li> Has the “System On” button been pressed? Projector
                              on and screen down?
                           </li>
                           
                           <li>Is PC selected on the touch panel in the Source
                              Page?
                           </li>
                           
                           <li> Is the power on; is there a green light on the front panel
                              of the PC?
                           </li>
                           
                           <li> Press any key on the keyboard to get the PC out of
                              sleep mode.
                           </li>
                           
                           <li> Is the monitor turned on?</li>
                           
                           <li>Did you push the “Send to Screen” button?</li>
                           
                        </ul>
                        <p><br>
                           <strong>MAC</strong></p>
                        
                        <ul>
                           
                           <li>Has the “System On” button been pressed? Projector
                              on and screen down?
                           </li>
                           
                           <li>Is MAC selected on the touch panel in the
                              Source Page?
                           </li>
                           
                           <li>Is the power on; is there a green light on the front
                              panel of the MAC?
                           </li>
                           
                           <li>Is the monitor turned on?</li>
                           
                           <li> Press any key on the keyboard to get the MAC
                              out of sleep mode.
                           </li>
                           
                           <li> Did you push the “Send to Screen” button?</li>
                           
                        </ul>
                        <p><br>
                           <strong>LAPTOP</strong></p>
                        
                        <ul>
                           
                           <li>Has the “System On” button been pressed? Projector
                              on and screen down?                                                              
                              
                           </li>
                           
                           <li> Is LAPTOP selected on the touch panel in the Source Page?</li>
                           
                           <li> Is the laptop plugged in to the appropriate cable on the instructor’s
                              desk?
                           </li>
                           
                           <li>Is the laptop turned on?</li>
                           
                           <li> If the image is not showing, press Fn/F8 button on the laptop.
                              You may need to do this two or three times to project the
                              image to the monitor and the screen.
                           </li>
                           
                           <li>If trying to connect to the network/internet
                              
                              <ul>
                                 
                                 <li>Is it using an Ethernet card?</li>
                                 
                                 <li>Is it connected to the correct cable on the instructor’s
                                    desk? 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        <p><strong>LCD TOUCH PANEL</strong></p>
                        
                        <ul>
                           
                           <li> If the touchpanel will not come on</li>
                           
                           <li>Press the screen with your fingertips, fingernails will not turn
                              on the panel. 
                           </li>
                           
                           <li>Is the cable securely plugged in the back of the touchpanel?</li>
                           
                        </ul>
                        
                        <div><a href="../../../../office-of-information-technology/tss/cts/tierclass/troubleshooting.html#top">TOP</a></div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/tierclass/troubleshooting.pcf">©</a>
      </div>
   </body>
</html>