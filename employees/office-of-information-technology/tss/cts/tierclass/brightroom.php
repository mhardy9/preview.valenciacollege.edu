<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tier ClassroomsA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/tierclass/brightroom.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/tierclass/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Tier ClassroomsA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/tierclass/">Tierclass</a></li>
               <li>Tier ClassroomsA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <h2>Smart Room Nomenclature</h2>
                        
                        <h3><u>PURPOSE</u></h3>
                        
                        <p>In order to better define terminology used to describe and  classify technology-enhanced
                           rooms at the college, a new classification system  has been developed. Under this
                           new system, all technology-enhanced rooms will  now be referred to generally as Smart
                           Rooms with an added level of definition  as outlined below.&nbsp; Smart Rooms will be 
                           sub-classified by TiERs (<u>T</u>echnology-<u>E</u>nhanced <u>R</u>oom) which will  define and identify exactly what level of technology exists in the
                           room.&nbsp; 
                        </p>
                        
                        
                        <h3><u>SMART ROOM TiER DEFINITIONS</u></h3>
                        
                        <p><strong><em><u>Smart Room – TiER I</u></em></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (formerly  known as pre-Bright Room)
                        </p>
                        
                        <p>The level of technology in the room is very limited in  scope.&nbsp; The room contains
                           at least a  ceiling-mounted projector and at least one multimedia source to display
                           but is  otherwise severely limited in technology and infrastructure.&nbsp; The room can
                           be upgraded to another TiER at  considerable cost.
                        </p>
                        
                        <p><strong><em><u>Smart Room – TiER II</u></em></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (formerly  known as Bright Room)
                        </p>
                        
                        <p>The standard compliment of technology is in place as is the  infrastructure to accommodate
                           the necessary cabling and data ports.&nbsp; This room can be easily upgraded with minimal
                           expenditure in funds and time. 
                        </p>
                        
                        <p><strong><em><u>Smart Room – TiER III</u></em></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (formerly  known as Smart Room)
                        </p>
                        
                        <p>The standard compliment of equipment exists as does the Crestron  controlling features.&nbsp;
                           The infrastructure  is in place with the cabling properly installed.
                        </p>
                        
                        <p><strong><em><u>Smart Room – TiER IV</u></em></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (formerly  known as Tandberg Video Conference Room)
                        </p>
                        
                        <p>This is a high-end video-conferencing room built with the standard  TiER III compliment
                           of equipment. 
                        </p>
                        
                        <h2>&nbsp;</h2>
                        
                        <p><a href="../../../../office-of-information-technology/tss/cts/tierclass/brightroom.html#top">TOP</a> 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/tierclass/brightroom.pcf">©</a>
      </div>
   </body>
</html>