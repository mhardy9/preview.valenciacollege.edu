<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tier ClassroomsA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/tierclass/purpose.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/tierclass/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Tier ClassroomsA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/tierclass/">Tierclass</a></li>
               <li>Tier ClassroomsA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <h2>Mission Statement &amp; Purpose</h2>
                        
                        <p><strong><img border="1" height="209" hspace="10" src="Document-Camera.jpg" vspace="10" width="200">MISSION STATEMENT</strong><br>
                           The Purpose of the tier classrooms at Valencia College
                           is to provide a state-of-the-art technology enhanced teaching
                           and learning environment. Faculty members are provided a stable and reliable
                           teaching
                           environment that is fully supported and maintained by the Office
                           of Information Technology. Students are provided an enhanced learning
                           opportunity through
                           the incorporation of various instructional media into the regular
                           classroom lecture.
                        </p>
                        
                        <p> To view a general description of the Multimedia Classrooms and
                           Lecture, <a href="../../../../office-of-information-technology/tss/cts/tierclass/equipment.html">click here.</a></p>
                        
                        <p>      <strong>PURPOSE</strong><br>
                           tier classrooms need to provide a functional and user friendly
                           teaching and learning environment. It is important to include
                           faculty input during the design phase; this will give the faculty a sense
                           of ownership
                           and provide the technology that will enhance their teaching style.
                           Consistent standard designs among all the classrooms will reduce confusion
                           and reluctance
                           to teach in the classrooms. All classrooms will have the same
                           layout, equipment and operating sequence. 
                        </p>
                        
                        <p><img border="1" height="156" hspace="10" src="Laptop1.jpg" vspace="10" width="200">Training is essential for successful use of the classrooms. Each
                           faculty member must go through an hour long hands-on training session
                           before given the privilege to use the room. The training will introduce
                           them to the environment and how all the equipment operates, which
                           should reduce the number of tech support calls. 
                        </p>
                        
                        <p>A system must be in place to provide technical assistance as needed.
                           The faculty must have a sense of security in the event there is a
                           problem someone can be available to help. Weekly operational/maintenance
                           checks will provide a stable, predictable and reliable environment.
                           Faculty need to be assured when they enter the classroom everything
                           will be operational. The support staff for the classrooms must be
                           professional, friendly, confident, and knowledgeable. They must treat
                           the faculty with respect when addressing their needs or concerns.
                           Each support technician shall go through a different and more intense
                           level of training then the faculty. The technician must have a high
                           level of confidence when called upon for assistance by a faculty member.
                        </p>
                        
                        <p>tier classrooms can be very successful; it will involve the support
                           of several different areas working together for a common goal.                   
                           
                        </p>
                        
                        <div>
                           <a href="../../../../office-of-information-technology/tss/cts/tierclass/purpose.html#top">TOP</a> 
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/tierclass/purpose.pcf">©</a>
      </div>
   </body>
</html>