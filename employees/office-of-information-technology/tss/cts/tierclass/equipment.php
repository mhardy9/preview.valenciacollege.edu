<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tier ClassroomsA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/cts/tierclass/equipment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/cts/tierclass/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Tier ClassroomsA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/">Cts</a></li>
               <li><a href="/employees/office-of-information-technology/tss/cts/tierclass/">Tierclass</a></li>
               <li>Tier ClassroomsA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           </a>
                        
                        <h2>
                           <span><img border="1" height="300" hspace="10" src="IMG_8157.jpg" width="199"></span>Equipment
                        </h2>
                        
                        <p>Audio System:</p>
                        
                        <ul>
                           
                           <li> Acoustic ceiling tile</li>
                           
                           <li>Acoustic sound panels (opt.)</li>
                           
                           <li>Carpeting</li>
                           
                           <li> Audio Sources (see below for list) </li>
                           
                           <li> Stereo Amplifier</li>
                           
                           <li> Ceiling speakers </li>
                           
                        </ul>
                        
                        <p>Video System:</p>
                        
                        <ul>
                           
                           <li>Zoned lighting/controls</li>
                           
                           <li> Window treatments (optional)</li>
                           
                           <li> Video sources  (see below for list)</li>
                           
                           <li> Ceiling projector
                              
                              
                           </li>
                           
                        </ul>  
                        
                        <p>Control System:</p>
                        
                        <ul>
                           
                           <li><span><img border="1" height="173" hspace="10" src="IMG_8092.jpg" vspace="10" width="260"></span>Crestron touch panel
                              
                              
                           </li>
                           
                        </ul>  
                        <p>Network System:</p>
                        
                        <ul>
                           
                           <li> Instructor’s Dell and/or MAC w wireless keyboard and mouse</li>
                           
                           <li> Laptop system and network connections</li>
                           
                           <li> Campus server
                              
                              
                           </li>
                           
                        </ul>    
                        
                        <p>Faculty Support System:</p>
                        
                        <ul>
                           
                           <li>Design input</li>
                           
                           <li> Security and stability</li>
                           
                           <li> Training</li>
                           
                           <li> Telephone in room</li>
                           
                           <li>
                              <span><img border="1" height="191" hspace="10" src="IMG_8162.jpg" vspace="20" width="260"></span> End
                              of semester open forums (feedback sessions)
                           </li>
                           
                           <li> Response to problems
                              via phone and Help Desk
                           </li>
                           
                        </ul>
                        
                        
                        
                        <ul>
                           
                           
                           
                           
                           <p><br>
                              
                           </p>
                           
                        </ul>        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><font size="3"><strong>Visual Equipment</strong></font></div>
                                 
                                 <div> <font size="3"><strong>Audio Equipment</strong></font>
                                    
                                 </div>
                                 
                                 <div><font size="3"><strong>Teacher's Aides </strong></font></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Video cassette recorder</li>
                                       
                                       <li>DVD player</li>
                                       
                                       <li>Computers (Dell, Mac, laptop)</li>
                                       
                                       <li>Document camera/visual presenter</li>
                                       
                                       <li>Projection screen and/or<br>
                                          Digital tablet
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Audio cassette player</li>
                                       
                                       <li>Video cassette player</li>
                                       
                                       <li>DVD player/audio CD</li>
                                       
                                       <li>Computers (Dell, Mac, laptop)</li>
                                       
                                       <li>Wireless Microphones<br>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Crestron Touch Panel </li>
                                       
                                       <li>Portable Mouse/Keyboard</li>
                                       
                                       <li>Wall Phone </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><font size="3"><strong>Computer Equipment</strong></font></div>
                                 
                                 <div><font size="3"><strong>Presentation Equipment</strong></font></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>                  Faculty Laptop Connection </li>
                                       
                                       <li>Macintosh
                                          G4 
                                       </li>
                                       
                                       <li>Dell</li>
                                       
                                       <li>ZIP Drives (MAC/PC) <br>              
                                          <br>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Document Camera</li>
                                       
                                       <li>LCD Projector </li>
                                       
                                    </ul>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="../../../../office-of-information-technology/tss/cts/tierclass/equipment.html#top">TOP</a> 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/cts/tierclass/equipment.pcf">©</a>
      </div>
   </body>
</html>