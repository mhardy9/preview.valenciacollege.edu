<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Computer Lab Technology ServicesA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/computerlab/software_licenses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/computerlab/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Computer Lab Technology ServicesA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/computerlab/">Computerlab</a></li>
               <li>Computer Lab Technology ServicesA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Software Licenses at Valencia</h2>
                        
                        <p>Software Consolidation efforts are underway at the college.&nbsp;Valencia has saved thousands
                           of dollars by consolidating individual 
                           user licenses into college-wide licenses. Additionally, the 
                           college has an Adobe CLP licenses which provides us discounted 
                           volume pricing. Below is a list of site-licenses offered through the 
                           Office of Information Technology:<br>
                           &nbsp;
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Software Title</strong></div>
                                 
                                 <div><strong>Platform</strong></div>
                                 
                                 <div><strong>License</strong></div>
                                 
                                 <div><strong>Contact</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>MS Office Suite</div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:OITServiceDesk@valenciacollege.edu">OIT Sevice Desk </a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>MS Windows</div>
                                 
                                 <div>PC</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:OITServiceDesk@valenciacollege.edu">OIT Sevice Desk </a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Deepfreeze</div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:OITServiceDesk@valenciacollege.edu">OIT Sevice Desk </a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>AutoCad</div>
                                 
                                 <div>PC</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div>
                                    <a href="mailto:AngSmith@valenciacollege.edu?subject=Response%20from%20Software%20Website"> </a><a href="mailto:OITServiceDesk@valenciacollege.edu">OIT Sevice Desk </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Micrograde</div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>300 Users</div>
                                 
                                 <div><a href="mailto:webcthelp@valenciacollege.edu?subject=Response%20from%20Software%20Website"> LTC Managers</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Respondus</div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:onlinehelp@valenciacollege.edu"> onlinehelp@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Respondus Lock Down Browser</div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:onlinehelp@valenciacollege.edu"> onlinehelp@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>StudyMate</div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:onlinehelp@valenciacollege.edu"> onlinehelp@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>TutorTrac</div>
                                 
                                 <div>PC</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:OITServiceDesk@valenciacollege.edu">OIT Sevice Desk </a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>SchoolVue</div>
                                 
                                 <div>PC</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:OITServiceDesk@valenciacollege.edu">OIT Sevice Desk </a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Qualtrics</div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:kewen@valenciacollege.edu">Kurt Ewen</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>SoftChalk Lesson Builder </div>
                                 
                                 <div>PC &amp; Mac</div>
                                 
                                 <div>College-wide</div>
                                 
                                 <div><a href="mailto:onlinehelp@valenciacollege.edu">onlinehelp@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../../office-of-information-technology/tss/computerlab/Software_Licenses.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/computerlab/software_licenses.pcf">©</a>
      </div>
   </body>
</html>