<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Computer Lab Technology ServicesA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/computerlab/software_purchase_procedure.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/computerlab/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Computer Lab Technology ServicesA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/computerlab/">Computerlab</a></li>
               <li>Computer Lab Technology ServicesA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Software Procurement Procedure</h2>
                        
                        <h3>Purpose</h3>
                        
                        <p>The purpose of this document is to provide a standardized procedure 
                           for processing requests for the procurement of software for student 
                           labs and academic areas. This endeavor seeks to:
                        </p>
                        
                        <ol>
                           
                           <li>Give us a better understanding of the software the college 
                              currently uses 
                           </li>
                           
                           <li>Help us  manage and maintain the software used by the 
                              college 
                           </li>
                           
                           <li>Categorize the software by departments for better 
                              distribution 
                           </li>
                           
                           <li>Give us the ability to coordinate the renewal/procurement of 
                              software
                           </li>
                           
                        </ol>
                        
                        <h3>General</h3>
                        
                        <p>All requests for the procurement of  academic software 
                           should be submitted to the Manager of Computer Lab  
                           Technology Services in the Office of Information Technology 
                           (OIT) for review prior to purchase. To submit software for review, please download,
                           complete, and submit the form below via email to the <a href="mailto:OITServiceDesk@valenciacollege.edu">OIT Sevice Desk </a>. During the review process the Academic Lab Manager will check for best pricing and
                           forward results to the requesting party. 
                        </p>
                        
                        <p>Note: Unless the software being purchased is used college-wide, the department of
                           the requesting party will be responsible for funding and procurement of the software.
                           
                        </p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                <h2><a href="documents/OIT_Procurement_Form.doc">Download Form</a></h2>
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="../../../office-of-information-technology/tss/computerlab/Software_Purchase_Procedure.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/computerlab/software_purchase_procedure.pcf">©</a>
      </div>
   </body>
</html>