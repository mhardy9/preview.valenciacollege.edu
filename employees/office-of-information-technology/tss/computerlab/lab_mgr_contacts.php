<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Computer Lab Technology ServicesA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/computerlab/lab_mgr_contacts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/computerlab/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Computer Lab Technology ServicesA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li><a href="/employees/office-of-information-technology/tss/computerlab/">Computerlab</a></li>
               <li>Computer Lab Technology ServicesA Division of the Technology Support Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Lab Manager Contacts</h2>
                        
                        <p><strong>Computer Lab Managers:</strong></p>
                        
                        <ul>
                           
                           <li> <a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#east">East Campus</a>
                              
                           </li>
                           
                           <li><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#west">West Campus</a></li>
                           
                           <li><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#winterpark">Winter Park Campus</a></li>
                           
                           <li><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#osceola">Osceola Campus</a></li>
                           
                        </ul>
                        
                        <p><a name="east" id="east"></a></p>
                        
                        <h3>Computer Lab Managers East Campus </h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>IT Liason </div>
                                    
                                    <div>Division POC </div>
                                    
                                    <div> Lab or Computerized Classrooms </div>
                                    
                                    <div>Bldg</div>
                                    
                                    <div>Room</div>
                                    
                                    <div>Phone</div>
                                    
                                    <div>E-mail Address</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis</div>
                                    
                                    <div>Joe Bivens </div>
                                    
                                    <div>Physics Lab </div>
                                    
                                    <div>1</div>
                                    
                                    <div>110</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade  Lewis</div>
                                    
                                    <div>Joe Bivens </div>
                                    
                                    <div>Science</div>
                                    
                                    <div>1</div>
                                    
                                    <div>122</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Ismael Velazquez</div>
                                    
                                    <div>Ismael Velazquez</div>
                                    
                                    <div>Digital Media Technology </div>
                                    
                                    <div>1</div>
                                    
                                    <div>152 A</div>
                                    
                                    <div>2570</div>
                                    
                                    <div><a href="mailto:ivelazquez@valenciacollege.edu" target="_parent">ivelazquez@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Joe Bivins </div>
                                    
                                    <div>Science</div>
                                    
                                    <div>1</div>
                                    
                                    <div>217</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Dennis Santspree</div>
                                    
                                    <div>Dennis Santspree</div>
                                    
                                    <div>Graphic Technology </div>
                                    
                                    <div>1</div>
                                    
                                    <div>213 / 214</div>
                                    
                                    <div>2762</div>
                                    
                                    <div><a href="mailto:dsantspree@valenciacollege.edu" target="_parent">dsantspree@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Joseph McBride </div>
                                    
                                    <div>Sound Recording Technology </div>
                                    
                                    <div>1</div>
                                    
                                    <div>256</div>
                                    
                                    <div>2882</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis</div>
                                    
                                    <div>Social Sciences &amp; Physical Education</div>
                                    
                                    <div>Social Sciences Computer Lab </div>
                                    
                                    <div>1</div>
                                    
                                    <div>377</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Joe Bivens </div>
                                    
                                    <div>Science</div>
                                    
                                    <div>1</div>
                                    
                                    <div>323</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Kristen Abel </div>
                                    
                                    <div>Sound Recording technology </div>
                                    
                                    <div>2</div>
                                    
                                    <div>123</div>
                                    
                                    <div>2882</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis</div>
                                    
                                    <div> Kristen Abel </div>
                                    
                                    <div>Theatre Tech </div>
                                    
                                    <div>2</div>
                                    
                                    <div>164C</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Shannon Mason </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>2</div>
                                    
                                    <div>203</div>
                                    
                                    <div>2384</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>2</div>
                                    
                                    <div>206</div>
                                    
                                    <div>2350</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Shannon Mason </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>2</div>
                                    
                                    <div>207</div>
                                    
                                    <div>2384</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Shannon Mason </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>2</div>
                                    
                                    <div>305</div>
                                    
                                    <div>2384</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Shannon Mason </div>
                                    
                                    <div>Business, IT &amp; Public Services </div>
                                    
                                    <div>2</div>
                                    
                                    <div>305A</div>
                                    
                                    <div>2384</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Kristen Abel </div>
                                    
                                    <div>Theatre Technology </div>
                                    
                                    <div>3</div>
                                    
                                    <div>101</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost </div>
                                    
                                    <div>Aziz Said </div>
                                    
                                    <div>Microcomputer Assistance Center </div>
                                    
                                    <div>4</div>
                                    
                                    <div>101</div>
                                    
                                    <div>2424</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Lindi Kourtellis</div>
                                    
                                    <div>EAP</div>
                                    
                                    <div>4</div>
                                    
                                    <div>102</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>OIT</div>
                                    
                                    <div>Foreign Lanaguage</div>
                                    
                                    <div>4</div>
                                    
                                    <div>104</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost </div>
                                    
                                    <div>Richard Weinsier</div>
                                    
                                    <div>Mathematics-SPA</div>
                                    
                                    <div>4</div>
                                    
                                    <div>108</div>
                                    
                                    <div>2854</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost </div>
                                    
                                    <div>Jamie Rost </div>
                                    
                                    <div>Honors Resource Lab</div>
                                    
                                    <div>4</div>
                                    
                                    <div>
                                       
                                       <p>117 / 117A</p>
                                       
                                    </div>
                                    
                                    <div>2854</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost </div>
                                    
                                    <div>John Edwards </div>
                                    
                                    <div>Communication Center </div>
                                    
                                    <div>4</div>
                                    
                                    <div>120</div>
                                    
                                    <div>2854</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost </div>
                                    
                                    <div>Alison Langevin </div>
                                    
                                    <div>Learning Resources Center</div>
                                    
                                    <div>4</div>
                                    
                                    <div>
                                       
                                       <p>122 / 123 / 123A-B</p>
                                       
                                    </div>
                                    
                                    <div>2854</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Migdalia Otero-Olan</div>
                                    
                                    <div>OIT</div>
                                    
                                    <div>Technology Center </div>
                                    
                                    <div>4</div>
                                    
                                    <div>133</div>
                                    
                                    <div>2815</div>
                                    
                                    <div><a href="mailto:motero@valenciacollege.edu">motero@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost</div>
                                    
                                    <div>Jamie Rost</div>
                                    
                                    <div>Faculty Break Room </div>
                                    
                                    <div>4</div>
                                    
                                    <div>134</div>
                                    
                                    <div>2854</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost</div>
                                    
                                    <div>Josh Chapkin </div>
                                    
                                    <div>Learning Resource Center </div>
                                    
                                    <div>4</div>
                                    
                                    <div>201</div>
                                    
                                    <div>2854</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams </div>
                                    
                                    <div>Business/IT Political Science </div>
                                    
                                    <div>4</div>
                                    
                                    <div>201</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost</div>
                                    
                                    <div>Josh Chapkin </div>
                                    
                                    <div>Library Instruction </div>
                                    
                                    <div>4</div>
                                    
                                    <div>203 / 210</div>
                                    
                                    <div>2854</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Joe Shuro </div>
                                    
                                    <div>OIT</div>
                                    
                                    <div>Career Development Services </div>
                                    
                                    <div>5</div>
                                    
                                    <div>230</div>
                                    
                                    <div>2259</div>
                                    
                                    <div>
                                       <a href="mailto:lvance@valenciacollege.edu"> </a><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jamie Rost </div>
                                    
                                    <div>OIT</div>
                                    
                                    <div>Assessment Testing </div>
                                    
                                    <div>5</div>
                                    
                                    <div>237A</div>
                                    
                                    <div>2815</div>
                                    
                                    <div><a href="mailto:jwells@valenciacollege.edu" target="_parent">jrost@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>OIT</div>
                                    
                                    <div>Johnny Aplin</div>
                                    
                                    <div>Atlas </div>
                                    
                                    <div>5</div>
                                    
                                    <div>213</div>
                                    
                                    <div>2019</div>
                                    
                                    <div><a href="mailto:japlin@valenciacollege.edu%C2%A0">japlin@valenciacollege.eduï¿½</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>6</div>
                                    
                                    <div>217</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>6</div>
                                    
                                    <div>217A</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Shannon Mason </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>6</div>
                                    
                                    <div>226</div>
                                    
                                    <div>2384</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Willams </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>6</div>
                                    
                                    <div>230</div>
                                    
                                    <div>2350</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Comm. Dept </div>
                                    
                                    <div>Writing lab </div>
                                    
                                    <div>7</div>
                                    
                                    <div>110</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Jade Lewis</div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>7</div>
                                    
                                    <div>112</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>7</div>
                                    
                                    <div>113</div>
                                    
                                    <div>2350</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Jade Lewis</div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>7</div>
                                    
                                    <div>116</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>7</div>
                                    
                                    <div>117</div>
                                    
                                    <div>2350</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>8</div>
                                    
                                    <div>Lobby</div>
                                    
                                    <div>2769</div>
                                    
                                    <div>
                                       
                                       <p><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Joe Bivins </div>
                                    
                                    <div> Science Lab </div>
                                    
                                    <div>8</div>
                                    
                                    <div>209</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>8</div>
                                    
                                    <div>213</div>
                                    
                                    <div>2350</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>8</div>
                                    
                                    <div>218</div>
                                    
                                    <div>2350</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Shannon Mason </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>8</div>
                                    
                                    <div>221</div>
                                    
                                    <div>2384</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Shannon Mason </div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>8</div>
                                    
                                    <div>226</div>
                                    
                                    <div>2384</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams</div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>8</div>
                                    
                                    <div>243</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Jade Lewis </div>
                                    
                                    <div>Richard Williams</div>
                                    
                                    <div> Business, IT &amp; Public Services </div>
                                    
                                    <div>8</div>
                                    
                                    <div>244</div>
                                    
                                    <div>2769</div>
                                    
                                    <div><a href="mailto:jjenkinslewis@valenciacollege.edu" target="_parent">jjenkinslewis@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Ed Livingston</div>
                                    
                                    <div>Terry Johnson </div>
                                    
                                    <div>Criminal Justice Institute </div>
                                    
                                    <div>2</div>
                                    
                                    <div>228</div>
                                    
                                    <div>8272</div>
                                    
                                    <div><a href="mailto:elivingston1@valenciacollege.edu">elivingston1@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a name="west" id="west"></a><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#top">TOP</a>            
                        </p>
                        
                        <h3>Computer Lab Managers West Campus</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>IT Liason </div>
                                 
                                 <div>Division POC  </div>
                                 
                                 <div> 
                                    <div>Lab or Computerized Classrooms </div>
                                    
                                 </div>
                                 
                                 <div>Bldg</div>
                                 
                                 <div>Room</div>
                                 
                                 <div>Phone</div>
                                 
                                 <div>E-mail Address</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Shelby Fiorentino</div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Atlas lab </div>
                                 
                                 <div>SSB</div>
                                 
                                 <div>142</div>
                                 
                                 <div>1619</div>
                                 
                                 <div><a href="mailto:mballenger@valenciacollege.edu" target="_parent">mballenger@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Tyron Johnson</div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Career Development Services </div>
                                 
                                 <div>SSB</div>
                                 
                                 <div>206</div>
                                 
                                 <div>1464</div>
                                 
                                 <div><a href="mailto:tjohnson@valenciacollege.edu">tjohnson@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Charles Chreptak </div>
                                 
                                 <div>Molly McIntire </div>
                                 
                                 <div>Business &amp; Information Technology Dept.</div>
                                 
                                 <div>7</div>
                                 
                                 <div>142</div>
                                 
                                 <div>1730</div>
                                 
                                 <div><a href="mailto:ccherptak@valenciacollege.edu" target="_parent">cchreptak@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Luis Stevanovich </div>
                                 
                                 <div>Montez Bates </div>
                                 
                                 <div>Testing Center</div>
                                 
                                 <div>11</div>
                                 
                                 <div>142</div>
                                 
                                 <div>1757</div>
                                 
                                 <div><a href="mailto:sfenton@valenciacollege.edu" target="_parent">lstevano@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ian O'Toole </div>
                                 
                                 <div>
                                    
                                    <p>Lisa Macon</p>
                                    
                                 </div>
                                 
                                 <div>Computer Programming &amp; Analysis</div>
                                 
                                 <div>7</div>
                                 
                                 <div>122A</div>
                                 
                                 <div>1484</div>
                                 
                                 <div><a href="mailto:iotoole@valenciacollege.edu" target="_parent">iotoole@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Charles Chreptak </div>
                                 
                                 
                                 <div>Hospitality &amp; Tourism</div>
                                 
                                 <div>9</div>
                                 
                                 <div>109-C</div>
                                 
                                 <div>1491</div>
                                 
                                 <div><a href="mailto:ccherptak@valenciacollege.edu" target="_parent">cchreptak@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Wes Sondermann </div>
                                 
                                 <div>Carmine LaPietra </div>
                                 
                                 <div>Library</div>
                                 
                                 <div>6</div>
                                 
                                 <div>204</div>
                                 
                                 <div>1504</div>
                                 
                                 <div><a href="mailto:Slutz@valenciacollege.edu" target="_parent">wsondermann@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Angel Melendez</div>
                                 
                                 <div>Lisa Macon </div>
                                 
                                 <div>Architecture &amp; Engineering &amp; Technology</div>
                                 
                                 <div>11</div>
                                 
                                 <div>223</div>
                                 
                                 <div>1983</div>
                                 
                                 <div><a href="mailto:amelendez@valenciacollege.edu" target="_parent">amelendez@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Patricia Lynch </div>
                                 
                                 <div>Lisa Macon</div>
                                 
                                 <div>Architecture &amp; Engineering &amp; Technology</div>
                                 
                                 <div>9</div>
                                 
                                 <div>209</div>
                                 
                                 <div>1599</div>
                                 
                                 <div><a href="mailto:plynch5@valenciacollege,edu">plynch5@valenciacollege,edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Gordon Andrews </div>
                                 
                                 <div>Lisa Macon </div>
                                 
                                 <div>Architecture &amp; Engineering &amp; Technology</div>
                                 
                                 <div>9</div>
                                 
                                 <div>210/211</div>
                                 
                                 <div>1360</div>
                                 
                                 <div><a href="mailto:rbunea@valenciacollege.edu">gandrews@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Wes Sondermann </div>
                                 
                                 <div>Karen Blondeau </div>
                                 
                                 <div>Student Computer</div>
                                 
                                 <div>6</div>
                                 
                                 <div>101</div>
                                 
                                 <div>1329</div>
                                 
                                 <div><a href="mailto:Slutz@valenciacollege.edu" target="_parent">wsondermann@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Wes Sondermann </div>
                                 
                                 <div>Carmine LaPietra </div>
                                 
                                 <div>Honors Resource </div>
                                 
                                 <div>6</div>
                                 
                                 <div>201b</div>
                                 
                                 <div>1504</div>
                                 
                                 <div><a href="mailto:Slutz@valenciacollege.edu" target="_parent">wsondermann@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ken Carpenter </div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Journalism </div>
                                 
                                 <div>3</div>
                                 
                                 <div>151</div>
                                 
                                 <div>1170</div>
                                 
                                 <div><a href="mailto:kcarpenter@valenciacollege.edu">kcarpenter2@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>William Stillwell </div>
                                 
                                 <div>William Stillwell </div>
                                 
                                 <div>Physics</div>
                                 
                                 <div>2</div>
                                 
                                 <div>209A</div>
                                 
                                 <div>
                                    <div>1767 </div>
                                 </div>
                                 
                                 <div><a href="mailto:wstillwell@valenciacollege.edu">wstillwell@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Luis Stevanovich</div>
                                 
                                 <div>Montez Bates</div>
                                 
                                 <div>Math Center </div>
                                 
                                 <div>7</div>
                                 
                                 <div>240</div>
                                 
                                 <div>
                                    <div>1757</div>
                                 </div>
                                 
                                 <div><a href="mailto:cthompson39@valenciacollege.edu">lstevano@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Luis Stevanovich</div>
                                 
                                 <div>Montez Bates</div>
                                 
                                 <div>Learning Support Services </div>
                                 
                                 <div>4</div>
                                 
                                 <div>240B</div>
                                 
                                 <div>
                                    <div>1757</div>
                                 </div>
                                 
                                 <div><a href="mailto:jmurdock@valenciacollege.edu">lstevano@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Luis Stevanovich</div>
                                 
                                 <div>Elizabeth Renn </div>
                                 
                                 <div> Communication Center </div>
                                 
                                 <div>5</div>
                                 
                                 <div>155</div>
                                 
                                 <div>1757</div>
                                 
                                 <div><a href="mailto:randry@valenciacollege.edu">lstevano@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Lee Lucia </div>
                                 
                                 <div>Wendi Dew </div>
                                 
                                 <div>Learning Technology Center </div>
                                 
                                 <div>6</div>
                                 
                                 <div>326A</div>
                                 
                                 <div>1792</div>
                                 
                                 <div><a href="mailto:bbritt@valenciacollege.edu" target="_parent">llucia1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>John Stover </div>
                                 
                                 <div>Bridges to Success </div>
                                 
                                 <div>SSB</div>
                                 
                                 <div>248</div>
                                 
                                 <div>1395</div>
                                 
                                 <div><a href="mailto:jstover@valenciacollege.edu">jstover@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Molly McIntire</div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Education Preparation Institute </div>
                                 
                                 <div>2</div>
                                 
                                 <div>112</div>
                                 
                                 <div>5473</div>
                                 
                                 <div><a href="mailto:mmcintire1@valenciacollege.edu">mmcintire1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Leon Finkler Kahan</div>
                                 
                                 <div>Assessment </div>
                                 
                                 <div>SSB</div>
                                 
                                 <div>235</div>
                                 
                                 <div>1102</div>
                                 
                                 <div><a href="mailto:lfinklerkahn@valenciacollege.edu">lfinklerkahn@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jeff Hogan</div>
                                 
                                 <div>Dr. Paula Pritchard</div>
                                 
                                 <div>Nursing</div>
                                 
                                 <div>HSB</div>
                                 
                                 <div>225</div>
                                 
                                 <div>5564</div>
                                 
                                 <div><a href="mailto:jhogan1@valenciacollege.edu">jhogan1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dr. Paula Pritchard</div>
                                 
                                 <div>Nursing</div>
                                 
                                 <div>HSB</div>
                                 
                                 <div>228</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Penny Conners</div>
                                 
                                 <div>CVT</div>
                                 
                                 <div>AHS</div>
                                 
                                 <div>126</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dental Hygiene</div>
                                 
                                 <div>AHS</div>
                                 
                                 <div>139</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Respiratory</div>
                                 
                                 <div>AHS</div>
                                 
                                 <div>226</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Radiography</div>
                                 
                                 <div>AHS</div>
                                 
                                 <div>246</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Diagnostic Medical Sonography</div>
                                 
                                 <div>AHS</div>
                                 
                                 <div>242</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Polysomnography</div>
                                 
                                 <div>AHS</div>
                                 
                                 <div>239</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>EMT</div>
                                 
                                 <div>3</div>
                                 
                                 <div>242</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Paramedic Lab</div>
                                 
                                 <div>3</div>
                                 
                                 <div>246</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Andy Hamer</div>
                                 
                                 <div>Graphic &amp; Interactive Design</div>
                                 
                                 <div>3</div>
                                 
                                 <div>151/151a</div>
                                 
                                 <div>1592</div>
                                 
                                 <div><a href="mailto:rhamer@valenciacollege.edu" rel="noreferrer">rhamer@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a name="winterpark" id="winterpark"></a></p>
                        
                        <h3>Computer Lab Managers Winter Park Campus</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>IT Liason </div>
                                 
                                 <div>Division POC </div>
                                 
                                 <div>Lab or Computerized Classrooms </div>
                                 
                                 <div>Bldg</div>
                                 
                                 <div>Room</div>
                                 
                                 <div>Phone</div>
                                 
                                 <div>E-mail Address</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Nadege Bastien</div>
                                 
                                 <div>Katherine Moore</div>
                                 
                                 <div>Computer Services</div>
                                 
                                 <div> 1</div>
                                 
                                 <div>130</div>
                                 
                                 <div>6831</div>
                                 
                                 <div><a href="mailto:nbastien@valenciacollege.edu">nbastien@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Nadege Bastien</div>
                                 
                                 <div>Linda Hidek </div>
                                 
                                 <div>Math Center</div>
                                 
                                 <div> 1</div>
                                 
                                 <div>138</div>
                                 
                                 <div>6831</div>
                                 
                                 <div><a href="mailto:nbastien@valenciacollege.edu">nbastien@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Nadege Bastien</div>
                                 
                                 <div>Stacy Diliberto </div>
                                 
                                 <div>Communication Center</div>
                                 
                                 <div>1</div>
                                 
                                 <div>136</div>
                                 
                                 <div>6831</div>
                                 
                                 <div><a href="mailto:nbastien@valenciacollege.edu">nbastien@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Nadege Bastien</div>
                                 
                                 <div>Katherine Moore</div>
                                 
                                 <div>Library&nbsp;</div>
                                 
                                 <div>1</div>
                                 
                                 <div>140</div>
                                 
                                 <div>6831</div>
                                 
                                 <div><a href="mailto:nbastien@valenciacollege.edu">nbastien@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dr, Cheryl Robinson</div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Career Development Services </div>
                                 
                                 <div>1</div>
                                 
                                 <div>104</div>
                                 
                                 <div>6882</div>
                                 
                                 <div><a href="mailto:crobinson@valenciacollege.edu">crobinson@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Julie Corderman </div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Atlas</div>
                                 
                                 <div> 1<span>&nbsp;&nbsp;&nbsp; 1</span>
                                    
                                 </div>
                                 
                                 <div>104 A</div>
                                 
                                 <div>6851</div>
                                 
                                 <div><a href="mailto:jcorderman@valenciacollege.edu" target="_parent">jcorderman@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>OIT</div>
                                 
                                 
                                 <div>Learning Technology Center </div>
                                 
                                 <div>1</div>
                                 
                                 <div>126</div>
                                 
                                 <div>6919</div>
                                 
                                 <div><a href="mailto:llucia1@valenciacollege.edu">llucia1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a name="osceola" id="osceola"></a><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#top">TOP</a>            
                        </p>
                        
                        <h3>Computer Lab Managers Osceola Campus</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>IT Liason </div>
                                 
                                 <div>Division POC </div>
                                 
                                 <div>Lab or Computerized Classrooms </div>
                                 
                                 <div>Bldg</div>
                                 
                                 <div>Room</div>
                                 
                                 <div>Phone</div>
                                 
                                 <div>E-mail Address</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jorge Soto</div>
                                 
                                 <div>Akos Delneky </div>
                                 
                                 <div>Library</div>
                                 
                                 <div> 1 </div>
                                 
                                 <div>104</div>
                                 
                                 <div>4888</div>
                                 
                                 <div><a href="mailto:jsoto@valenciacollege.edu" target="_parent">jsoto@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jill Szentmiklosi </div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Career Development Services </div>
                                 
                                 <div> 1 </div>
                                 
                                 <div>151</div>
                                 
                                 <div>4897</div>
                                 
                                 <div><a href="mailto:cklinger@valenciacollege.edu">jszentmiklosi@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Heidi Shugg </div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>Atlas lab </div>
                                 
                                 <div> 1 </div>
                                 
                                 <div>140-C</div>
                                 
                                 <div>4821</div>
                                 
                                 <div><a href="mailto:HShugg1@valenciacollege.edu">HShugg1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Andrew Becker</div>
                                 
                                 <div>OIT</div>
                                 
                                 <div>CPT Computer Assessment Lab (also part of Student Services)</div>
                                 
                                 <div> 1 </div>
                                 
                                 <div>127</div>
                                 
                                 <div>4860</div>
                                 
                                 <div><a href="mailto:abecker@valenciacollege.edu" target="_parent">abecker@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jorge Soto</div>
                                 
                                 <div>Kimberly Foster </div>
                                 
                                 <div>Testing Center </div>
                                 
                                 <div> 1 </div>
                                 
                                 <div>125</div>
                                 
                                 <div>4815</div>
                                 
                                 <div><a href="mailto:jsoto@valenciacollege.edu" target="_parent">jsoto@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Tanyi Colon </div>
                                 
                                 <div>
                                    
                                    <p>Jenni Campbell</p>
                                    
                                 </div>
                                 
                                 <div>Language Lab</div>
                                 
                                 <div> 2 </div>
                                 
                                 <div>244</div>
                                 
                                 <div>4903</div>
                                 
                                 <div><a href="mailto:storres@valenciacollege.edu" target="_parent">tcolon3@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Andrew Salaz </div>
                                 
                                 <div>Dale Husbands </div>
                                 
                                 <div>Graphics</div>
                                 
                                 <div> 2 </div>
                                 
                                 <div>115</div>
                                 
                                 <div>4896</div>
                                 
                                 <div><a href="mailto:asalaz@valenciacollege.edu">asalaz@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Andi Berry</div>
                                 
                                 <div>Todd Ribardo</div>
                                 
                                 <div>Academic Systems (Math) Lab</div>
                                 
                                 <div> 2 </div>
                                 
                                 <div>143</div>
                                 
                                 <div>4835</div>
                                 
                                 <div><a href="mailto:aberry@valenciacollege.edu" target="_parent">aberry@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Todd Ribardo</div>
                                 
                                 <div>Dale Husbands </div>
                                 
                                 <div>Comp I Lab (classroom use only)</div>
                                 
                                 <div> 3 </div>
                                 
                                 <div>218</div>
                                 
                                 <div>4147</div>
                                 
                                 <div><a href="mailto:tribardo@valenciacollege.edu" target="_parent">tribardo@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Javiar Charriez </div>
                                 
                                 <div>Dale Husbands </div>
                                 
                                 <div>Learning Center</div>
                                 
                                 <div> 3</div>
                                 
                                 <div>100</div>
                                 
                                 <div>4112</div>
                                 
                                 <div><a href="mailto:sguevara@valenciacollege.edu" target="_parent">JCharriez@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Carol Nevels</div>
                                 
                                 <div>Dale Husbands </div>
                                 
                                 <div>OST Lab (classroom use mainly)</div>
                                 
                                 <div> 3</div>
                                 
                                 <div>116</div>
                                 
                                 <div>4836</div>
                                 
                                 <div><a href="mailto:CNevels@valenciacollege.edu" target="_parent">CNevels@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Gary Kokaisel </div>
                                 
                                 <div>Angelique Smith </div>
                                 
                                 <div>Learning Technology Center </div>
                                 
                                 <div>2</div>
                                 
                                 <div>116</div>
                                 
                                 <div>4812</div>
                                 
                                 <div><a href="mailto:sguevara@valenciacollege.edu" target="_parent">gkokaisel@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#top">TOP</a></p>
                        
                        <h3>Computer Lab Managers Lake Nona Campus</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>IT Liason </div>
                                 
                                 <div>Division POC </div>
                                 
                                 <div>Lab or Computerized Classrooms </div>
                                 
                                 <div>Bldg</div>
                                 
                                 <div>Room</div>
                                 
                                 <div>Phone</div>
                                 
                                 <div>E-mail Address</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../../office-of-information-technology/tss/computerlab/lab_mgr_contacts.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/computerlab/lab_mgr_contacts.pcf">©</a>
      </div>
   </body>
</html>