<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Service DeskA Division of the Technology Support Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/tss/helpdesk/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/tss/helpdesk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service DeskA Division of the Technology Support Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/tss/">Tss</a></li>
               <li>Helpdesk</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           
                           
                           </a><br>
                        
                        <p>The OIT Service Desk is responsible for process of ensuring accurate and timely technology
                           support and problem resolution to Valencia Employees. They are also responsible for
                           resolving complex and everyday service requests relating to PC, web services , server
                           operations, college enterprise resource system and hardware and software problems
                           and malfunctions; interacting and consulting with clients to manage client satisfaction;
                           providing guidance, assistance, and follow-up on client inquiries; and assisting in
                           the implementation of desktop hardware and software.
                        </p>
                        
                        
                        <p><strong>Hours</strong>:
                        </p>
                        
                        <p>The OIT Service Desk is staffed Monday - Friday -- 7AM to 6 PM<br>
                           After hours Voice Mail, E-Mail, and Self Service Requests are  periodically checked.
                           
                        </p>
                        
                        <ul>
                           
                           <li><a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new">Submit a new request.</a></li>
                           
                           <li><a href="https://valenciacollege.zendesk.com/hc/en-us/requests">Check on the status of a current request. </a></li>
                           
                           <li><a href="../../../office-of-information-technology/tss/helpdesk/Request_account.html"> Account Request Forms</a></li>
                           
                           <li>Email <a href="mailto:support@valenciacollege.zendesk.com">support@valenciacollege.zendesk.com</a> to start a self-service request
                           </li>
                           
                        </ul>
                        
                        
                        
                        <h2>Account Request Forms </h2>
                        
                        <p>All Banner Security, Account &amp; VPN request must now be made through Atlas<br>  
                           Please <a href="https://atlas.valenciacollege.edu">Log in to Atlas</a> and click on the <a href="http://valenciacollege.edu/office-of-information-technology/tss/helpdesk/images/EmployeeSuppportChannel.png" target="_blank">Banner &amp; Network Access Request</a> link in the Employee Support Channel. (only the approving manager has access to this).
                           A brief walkthrough of this procedure can be found <a href="https://valenciacollege.zendesk.com/hc/en-us/articles/204536580">here</a>.
                           
                           
                        </p>
                        
                        <p><em>Questions?</em> E-mail <a href="mailto:OITServiceDesk@valenciacollege.edu">OITServiceDesk@valenciacollege.edu.</a></p>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <hr size="2" width="100%">
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><img alt="PDF Icon" border="0" height="31" src="pdfmidsz.gif" width="28"></div>
                                 
                                 <div>In order to view PDF files, you will need Adobe Reader which is available 
                                    for free from the <a href="http://get.adobe.com/reader/" target="_blank">Adobe</a> 
                                    web site. 
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <div><a href="http://get.adobe.com/reader/" target="_blank">
                                          <img alt="Get Adobe Acrobat Reader" border="0" height="31" src="getacro.gif" width="88"></a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/tss/helpdesk/index.pcf">©</a>
      </div>
   </body>
</html>