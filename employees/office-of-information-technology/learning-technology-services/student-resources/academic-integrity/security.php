<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/security.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/">Academic Integrity</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Passwords</h2>
                        
                        <h3>Password Security Update! </h3>
                        
                        <p>Because of the increase in security awareness everywhere it has been determined that
                           Valencia faculty, staff and students will now be required to change their network
                           password every 180 days. There is a certain complexity to the password that you pick
                           to make sure it is a secure password and not easily guessed by others. Here is what
                           will be required: 
                        </p>
                        
                        
                        <ul>
                           
                           <li>All passwords are required to meet the <strong>“strong password”</strong> definition.
                           </li>
                           
                           <li>Passwords must be at least eight characters in length.</li>
                           
                           <li>Passwords must consist of a combination of three (3) of the following four (4) categories:
                              Upper case letters, Lower case letters, Numerals and special characters. 
                           </li>
                           
                           <li>Passwords cannot contain a dictionary word.</li>
                           
                           <li>Passwords cannot contain all or part of your user id.</li>
                           
                           <li>Passwords cannot match any of your four previous passwords.</li>
                           
                        </ul>
                        
                        
                        <p> This will only be required every 180 days (6 months). You will start to get on Friday,
                           June 25, 2010 warnings prompting you to change your password 14 days before it is
                           due and each day until it does expire. If you do not change the password before it
                           expires then you will be forced to change it before logging in. Should you need assistance
                           please contact the Helpdesk for assistance at x5555 
                        </p>
                        
                        <h3><strong>Password Procedure</strong></h3>
                        
                        <p><strong></strong> 
                        </p>
                        
                        <p><strong>PROCEDURE &nbsp;STATEMENT</strong></p>
                        
                        <p>This procedure establishes a standard for creation of strong passwords and protection
                           of those passwords within the Microsoft Active Directory/Exchange email system. This
                           procedure applies to all persons who have, or are responsible for, an account on any
                           system accessed on the Valencia College network or computer systems.
                        </p>
                        
                        
                        <p><strong>DEFINITIONS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
                        
                        <p><u>Expiration:</u>Date at which password for access to College systems is required to be changed meeting
                           strong password standards.
                        </p>
                        
                        <p><strong>PROCEDURES and RESPONSIBILITIES</strong></p>
                        
                        <p>Users are responsible for assisting in the protection of the network and computer
                           systems they use. The integrity and secrecy of an individual's password is a key element
                           of that responsibility. Each individual has the responsibility for creating and securing
                           an acceptable password per this procedure. Failure to conform to these restrictions
                           may lead to the suspension of rights to College systems or other action as provided
                           by College Policy, State or Federal law.
                        </p>
                        
                        <p><strong>Password Creation Rules</strong>:
                        </p>
                        
                        <p>Passwords are initially assigned when a new account is created. Upon initial logon
                           users have the right and the ability to change passwords on their Active Directory/Exchange
                           accounts at any time. Users will be prompted to change their password the first time.
                        </p>
                        
                        <ul>
                           
                           <li>All passwords are required to meet the “strong password” definition.</li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>Passwords must be at least eight characters in length.</li>
                           
                           <li>Passwords must consist of a combination of three (3) of the following four (4) categories:
                              Upper case letters, Lower case letters, Numerals and special characters.
                           </li>
                           
                           <li>Passwords cannot contain a dictionary word.</li>
                           
                           <li>Passwords cannot contain all or part of your user id.</li>
                           
                           <li>Passwords cannot match any of your four previous passwords.</li>
                           
                        </ul>
                        
                        <p><strong>Password Expiration:</strong></p>
                        
                        <p>Passwords will expire on a 180-day cycle (6 months). Advance warnings of upcoming
                           password expiration will be sent to the designated account holder via campus e-mail
                           beginning 30 days prior to expiration, with repeated reminders several times thereafter
                           until the expiration date.
                        </p>
                        
                        <p>A student  may change his or her password at any time through their Atlas account.
                           It is not necessary to wait for expiration.
                        </p>
                        
                        <p>Passwords should be changed immediately and the Office of Information Technology notified
                           whenever there is a belief that the password has been compromised.
                        </p>
                        
                        
                        <p><strong>Desktop/Laptop&nbsp; Password Security</strong></p>
                        
                        <p>All Desktops and Laptops must have a password enabled screensaver &nbsp;and timeout set
                           to no more than 5 minutes.
                        </p>
                        
                        <p>NOTE: Password changes will also affect Smartphone’s and PDA’s that check Outlook
                           emails. User will need to change this manually.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/security.pcf">©</a>
      </div>
   </body>
</html>