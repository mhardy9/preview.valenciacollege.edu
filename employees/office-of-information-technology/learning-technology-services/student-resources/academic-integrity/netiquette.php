<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/netiquette.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/">Academic Integrity</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Netiquette</h2>
                        
                        <p>The term  <span>"netiquette"</span> refers to the awareness of the need for a certain code of  behavior (etiquette) in
                           electronic environments (the net) ... Net + Etiquette =  netiquette. Netiquette, while
                           a general term, is complex at specific levels  because there are so many different
                           kinds of electronic environments, and so  many different situations in which we may
                           find ourselves in those environments. <br>
                           Since this class will  be focusing on areas where electronic communication play a
                           key, if as yet  undefined role in technical and professional communication, it is
                           our  responsibility to be aware of our textual presence as it reflects upon our  professional
                           responsibilities, to ourselves and to our parents and students. <br>
                           In order to maintain  a positive online environment for our class, we all need to
                           follow the  netiquette guidelines summarized below. <br>
                           All students are  expected to: 
                        </p>
                        
                        <ul type="disc">
                           
                           <li>show       respect for the instructor and for other students in the class </li>
                           
                           <li>respect       the privacy of other students </li>
                           
                           <li>express       differences of opinion in a polite and rational way </li>
                           
                           <li>maintain       an environment of constructive criticism when commenting on the work
                              of       other students 
                           </li>
                           
                           <li>avoid       bringing up irrelevant topics when involved in group discussions or other
                              collaborative activities 
                           </li>
                           
                        </ul>
                        
                        <p>The following list  summarizes the kind of behavior that is not acceptable. Each item
                           listed below is  grounds for removal from the class. <br>
                           
                        </p>
                        
                        <h3><strong>Students should not: </strong></h3>
                        
                        <ol start="1" type="1">
                           
                           <li>Show       disrespect for the instructor or for other students in the class </li>
                           
                           <li>Send       messages or comments that are threatening, harassing, or offensive </li>
                           
                           <li>Use       inappropriate or offensive language </li>
                           
                           <li>Convey       a hostile or confrontational tone when communicating or working     
                              collaboratively with other students 
                           </li>
                           
                           <li>USE       ALL UPPERCASE IN THEIR MESSAGES -- THIS IS THE EQUIVALENT OF SHOUTING!!!
                              
                           </li>
                           
                        </ol>
                        
                        <p>If a faculty members  feels that a student is violating any of the above guidelines,
                           They will  contact that student to discuss the situation in person. If you feel that
                           a  student is behaving inappropriately, please send your instructor a private  e-mail
                           message explaining the situation as soon as possible. 
                        </p>
                        
                        <p>Most netiquette is  simple common sense. Simply put, the rules of netiquette outlined
                           by the NCTE  are  those you should know and understand as an individual "netizen"
                           ... a  citizen of cyberspace. In this class, however, you also <em>represent</em>&nbsp;  Valencia College in those public spaces, and you should keep in mind  that those
                           spaces are, practically speaking, an extension of the classroom  itself.<br>
                           If you are engaging  electronic environments <em>as a member of this class</em>&nbsp; then you are  subject to the same expectations and rules of conduct any teacher
                           or  administrator might expect of you in a face-to-face (f2f) environment.  Additionally,
                           whether or not you are entering an electronic environment as a  member of the class,
                           if you are doing so via a WebCT or Atlas account, you are  legally responsible to
                           the Valencia's policies and expectations. 
                        </p>
                        
                        <h3>
                           <strong>The Top Ten of Computer Ethics</strong><br>
                           <em>From the Computer Ethics Institute</em> 
                        </h3>
                        
                        <ol start="1" type="1">
                           
                           <li>Do not  use a computer to harm other people. </li>
                           
                           <li>Do not interfere with other people's computer work. </li>
                           
                           <li>Do  not snoop around in other people's files. </li>
                           
                           <li>Do not use a computer to steal. </li>
                           
                           <li>Do not use a computer to bear false witness. </li>
                           
                           <li>Do not use or copy software for which you have not paid. </li>
                           
                           <li>Do not use other people's computer resources without authorization. </li>
                           
                           <li>Do not appropriate other people's intellectual output. </li>
                           
                           <li>Do think about the social consequences of the program you write. </li>
                           
                           <li>Do use a computer in ways that show consideration and respect. </li>
                           
                        </ol>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/netiquette.pcf">©</a>
      </div>
   </body>
</html>