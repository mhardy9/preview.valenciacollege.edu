<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/plagiarism.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/">Academic Integrity</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Plagiarism Resources</h2>
                        
                        <p><img alt="Two Students Cheating" height="208" hspace="10" src="CheatingCompressed_000.jpg" vspace="3" width="324"></p>
                        
                        <h3>What is Plagiarism?</h3>
                        
                        <p>Many people think of plagiarism as copying another's work, or borrowing someone else's
                           original ideas. But terms like "copying" and "borrowing" can disguise the seriousness
                           of the offense:
                        </p>
                        
                        <h3>According to the Merriam-Webster Online Dictionary, to "plagiarize" means</h3>
                        
                        <ul>
                           
                           <li>to steal and pass off (the ideas or words of another) as one's own</li>
                           
                           <li>to use (another's production) without crediting the source</li>
                           
                           <li>to commit literary theft</li>
                           
                           <li>to present as new and original an idea or product derived from an existing source.</li>
                           
                        </ul>
                        
                        <p>In other words, plagiarism is an act of fraud. It involves both stealing someone else's
                           work and lying about it afterward. 
                        </p>
                        
                        <h3>But can words and ideas really be stolen?</h3>
                        
                        <p>According to U.S. law, the answer is yes. The expression of original ideas is considered
                           intellectual property, and is protected by copyright laws, just like original inventions.
                           Almost all forms of expression fall under copyright protection as long as they are
                           recorded in some way (such as a book or a computer file). 
                        </p>
                        
                        <h3>All of the following are considered plagiarism:</h3>
                        
                        <ul>
                           
                           <li>turning in someone else's work as your own</li>
                           
                           <li>copying words or ideas from someone else without giving credit</li>
                           
                           <li>failing to put a quotation in quotation marks</li>
                           
                           <li>giving incorrect information about the source of a quotation</li>
                           
                           <li>changing words but copying the sentence structure of a source without giving credit</li>
                           
                           <li>copying so many words or ideas from a source that it makes up the majority of your
                              work, whether you give credit or not (see our section on "fair use" rules)
                           </li>
                           
                        </ul>
                        
                        <p>Most cases of plagiarism can be avoided, however, by citing sources. Simply acknowledging
                           that certain material has been borrowed, and providing your audience with the information
                           necessary to find that source, is usually enough to prevent plagiarism. See our section
                           on <a href="http://www.plagiarism.org/plag_article_what_is_citation.html">citation</a> for more information on how to cite sources properly. 
                        </p>
                        
                        
                        <p><em>Source: <a href="http://www.plagiarism.org/">http://www.plagiarism.org/</a> </em></p>
                        
                        <p>One of the tools the college currently subscribes to is the SafeAssign plagiarism
                           prevention tool to help with any paper submissions in your course. This will tool
                           is intergrated into your Valencia Online courses. 
                        </p>
                        
                        <ul>
                           
                           <li><a href="../../../../faculty/development/centers/learn-it/SafeAssign.html" target="_blank">Safe Assign</a></li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/academic-integrity/plagiarism.pcf">©</a>
      </div>
   </body>
</html>