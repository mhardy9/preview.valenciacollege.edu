<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/online-101.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/">Getting Started Online</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <a href="http://learn.valenciacollege.edu/webapps/blackboard/execute/courseMain?course_id=_1310_1" target="_blank"><img alt="Valencia Online 101 - The Student Experince Click Here to Enter Course with Altas User Name and Password" border="0" height="100" src="ValenciaOnline101Banner.jpg" width="708"></a>
                        
                        
                        <h2>Valencia Online 101- The Student Experience</h2>
                        
                        <p><span>Blackboard Learn</span> is our new management system for online learning, community building, and knowledge
                           sharing. It is open, flexible, and centered on enhancing your academic  achievement.
                           Below find interactive video tutorials on how to perform essential tasks in Blackboard
                           Learn.
                        </p>
                        
                        <p>Visit <a href="http://learn.valenciacollege.edu/webapps/blackboard/execute/courseMain?course_id=_1310_1" target="_blank"><strong>Valencia Online 101 for Blackboard </strong></a>  - Will require you to enter Atlas Username and Password to enter course 
                        </p>
                        
                        
                        <h3> Video Tutorials - Click on icons below to view</h3>
                        
                        <a href="http://ondemand.blackboard.com/r91/movies/bb91_student_creating_discussion_board_post.htm" target="_blank"><img alt="Drawing of student working on Laptop" border="1" height="107" hspace="3" src="ThumbBB_Student_Movie_Red_001.jpg" vspace="3" width="200"></a><p>How to create a discussion board post, reply directly to a post and reply to a thread</p>
                        <br>
                        <a href="http://ondemand.blackboard.com/r91/movies/bb91_student_taking_test_online.htm" target="_blank"><img alt="Drawing Student working on Laptop" border="1" height="107" hspace="3" src="ThumbBB_Student_Movie_000.png" vspace="3" width="200"></a>
                        
                        
                        <p>How to take a test online and save answers for submission</p>
                        <br>
                        <a href="http://ondemand.blackboard.com/r91/movies/bb91_student_submit_assignment.htm" target="_blank"><img alt="Drawing of student working on Laptop" border="1" height="107" hspace="3" src="ThumbBB_Student_Movie_001.png" vspace="3" width="200"></a>
                        
                        
                        <p>How to download assignment, add file content and upload assignment</p>
                        <br>
                        <a href="http://ondemand.blackboard.com/r91/movies/bb91_student_checking_grades.htm" target="_blank"><img alt="Drawing of student working on laptop" border="1" height="107" hspace="3" src="ThumbBB_Student_Movie_Red_000.jpg" vspace="3" width="200"></a>
                        
                        
                        <p>How to view grades and instructor feedback.</p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/online-101.pcf">©</a>
      </div>
   </body>
</html>