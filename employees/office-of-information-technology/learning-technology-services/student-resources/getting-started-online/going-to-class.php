<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/going-to-class.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/">Getting Started Online</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Going to Class</h2>
                        
                        <p><strong>Tips for Online Success</strong><br>
                           <img alt="Young woman holds books as she walks" height="375" hspace="3" src="Young_Woman_walks_books_small.jpg" vspace="3" width="250">An online course is much like a traditional, face-to-face course in that it follows
                           a schedule and syllabus and that assignments and quizzes are expected, usually due
                           by certain dates. You are responsible for reading the textbooks and handouts and completing
                           all class learning activities on their own, but the big difference in an online course
                           is that you can do the work any time of day and, typically, any day of the week. Instead
                           of attending lectures, you will access course content (PowerPoint presentations, video
                           demonstrations, assignments, papers, other web sites, etc.) via the internet. The
                           instructor can usually be reached by Blackboard discussion, e-mail, and/or chat.
                        </p>
                        
                        
                        <p>Learning online can require some new strategies. Generally, you should:</p>
                        
                        <p><strong>Familiarize yourself with the course’s online delivery system (e.g., Blackboard</strong>).<br>
                           Learn what modules and links you have access to and how can you use them to get to
                           where you want to go. Find the course syllabus, schedule, calendar, requirements,
                           methods of communication, where you go for help, etc. <br>
                           <br>
                           <strong>Take full advantage of online chat and discussions</strong>. <br>
                           Whatever you can do to avoid feeling isolated is extremely important, and participating
                           in online chat and discussions will give you access to other students who are taking
                           the same course as you at the same time. <br>
                           <br>
                           <strong>Participate!</strong><br>
                           Whether you are working alone, or in a group, contribute your ideas, perspective and
                           comments on the subject you are studying, and read about those of your classmates.
                           Your instructor is not the only source of information in your course—you can gain
                           great insight from your peers and they can learn from you as well.<br>
                           <br>
                           <strong>Take the courses and yourself seriously.</strong> <br>
                           Elicit the support of your colleagues, family and friends before you start out on
                           your online adventure. This built-in support system will help you tremendously since
                           there will be times when you will have to sit at your computer for hours at a stretch
                           in the evenings and on weekends. When most people are through with work and want to
                           relax is most likely when you will be bearing down on your course work. It helps to
                           surround yourself with people who understand and respect what you are trying to do.<br>
                           <br>
                           <strong>Make sure you have a private space where you can study.</strong><br>
                           This will help lend importance to what you are doing as well. Your own space where
                           you can shut the door, leave papers everywhere, and work in peace is necessary. If
                           you try to share study space with the dining room or bedroom, food or sleep will take
                           priority over studying.<br>
                           <br>
                           <strong>Become a true advocate of distance learning.</strong><br>
                           Discuss the merits of the process with whoever will listen. In order to be successful
                           in this new educational environment, you must truly believe in its potential to provide
                           quality education which is equal to, if not better than the traditional face-to-face
                           environment. In discussing the value of online learning, you will reinforce it’s merits
                           for yourself.<br>
                           <br>
                           <strong>Log on to your course every single day</strong>.<br>
                           ….or a minimum of 5-6 days a week. Once you get into the online conferencing system,
                           you will be eager to see who has commented on your postings and read the feedback
                           of your instructor and peers. You will also be curious to see who has posted something
                           new that you can comment on. If you let too many days go by without logging on to
                           your course discussion group, you will get behind and find it very difficult to catch
                           up. You are expected to give to and take at least as much from an online class as
                           you would from a traditional face-to-face class.<br>
                           <br>
                           <strong>Take advantage of your anonymity.</strong><br>
                           One of the biggest advantages of the online format is that you can pursue your studies
                           without the judgments typical in a traditional classroom. Unless you are using video
                           conferencing, no one can see you – there are no stereotypes, and you don’t have to
                           be affected by raised eyebrows, rolled eyeballs, other students stealing your thunder,
                           or people making other non verbal reactions to your contributions. You don’t have
                           to feel intimidated or upstaged by students who can speak faster than you because
                           you can take all of the time you need to think your ideas through and compose a response
                           before posting your comments to your class.<br>
                           <br>
                           <strong>Be polite and respectful</strong>.<br>
                           Just because you are anonymous, doesn’t mean you should let yourself go. Remember,
                           you are dealing with real people on the other end of your modem. Being polite and
                           respectful is not only common sense, it is absolutely obligatory for a productive
                           and supportive online environment. In a positive online environment, you will feel
                           valued by your instructor, valued by your classmates and your own work will have greater
                           value as well.<br>
                           <br>
                           <strong>Speak up if you are having problems.</strong><br>
                           Your instructor will advise you regarding your avenues for help but, generally, you
                           have a number of options. First, look around the program to try and find the answers
                           to your questions. When you enrolled in an online course you indicated your understanding
                           that you could work in a self-directed environment. You can also post or send questions
                           to your classmates. If you have a question it’s not unlikely that someone else has
                           already had that same question. One of the strengths of an online class is that it
                           can promote working cooperatively. If another student is able to help you, he/she
                           probably will, and if you are able to explain something to your classmates in need,
                           you will not only help them out, you will reinforce your own knowledge about the subject.
                           Finally, you may contact your instructor with any unanswered questions. <br>
                           <br>
                           Remember that your instructor cannot see you so you must be absolutely explicit with
                           your comments and requests. If you are having technical difficulties, or problems
                           understanding something about the course, you MUST speak up otherwise there is no
                           way that anyone will know that something is wrong. <br>
                           <br>
                           <strong>Apply what you learn.</strong><br>
                           Apply everything you learn as you learn it and you will remember it more readily.
                           If it is possible, take the things you learn in your online course today and use them
                           in your workplace tomorrow. Also, try to make connections between what you are learning
                           and what you do or will do in your job. Contributing advice or ideas about the real-world
                           as it applies to the subject matter you are studying helps you to internalize what
                           you are learning, and gives valuable insight to your classmates who will benefit from
                           your experience. 
                        </p>
                        
                        <p><em>Source: Illinois Online Network (ION)
                              </em><br>
                           
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/going-to-class.pcf">©</a>
      </div>
   </body>
</html>