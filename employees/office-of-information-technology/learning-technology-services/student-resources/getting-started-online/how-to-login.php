<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/how-to-login.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/">Getting Started Online</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>How to Login</h2>
                        
                        <p>As of Summer 2011 there is one Learning Management Systems available for you to take
                           your online, hybrid and web-enhanced courses. Students will be using the new system
                           Blackboard Learn.
                        </p>
                        
                        
                        <h3>Automatically Log in via Atlas </h3>
                        
                        <p>You can access your courses using the My Courses page in Atlas. You will be automatically
                           logged in to Blackboard Learn. 
                        </p>
                        
                        <ol> 
                           
                           <li>Log in to Atlas at <a href="https://atlas.valenciacollege.edu" target="_blank">https://atlas.valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>Click on the Courses Tab in Atlas</li>
                           
                           <li>Click on the My Courses Banner to be logged into Blackboard Learn</li>
                           
                        </ol>
                        
                        
                        <p><img alt="Courses menu in Atlas" src="Atlas-Menu.png"></p>
                        
                        <p><img alt="My Courses Portlet in Atlas" src="Atlas-MyCourses-Page.png"></p>
                        
                        
                        <h3>Using the Online Courses Quick Link - Valencia Homepage </h3>
                        
                        <p>You can also access your classes via the Blackboard Learn Login Page. The link is
                           in Quick Links. 
                        </p>
                        
                        <ol>
                           
                           <li>Click on Quick Links on the <a href="../../../../index.html">Valencia College homepage</a>
                              
                           </li>
                           
                           <li>Click on Blackboard Learn</li>
                           
                           <li>Log into Blackboard using your Atlas Username and Password</li>
                           
                        </ol>
                        
                        <p><img alt="Quick Link for Online Courses" src="Quick-Links.png"></p>
                        
                        <p><img alt="Blackboard Learn Login Page" src="BB-Learn-Login-Page.png"></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/getting-started-online/how-to-login.pcf">©</a>
      </div>
   </body>
</html>