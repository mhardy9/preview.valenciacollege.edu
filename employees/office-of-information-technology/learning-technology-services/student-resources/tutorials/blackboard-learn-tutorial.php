<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/tutorials/blackboard-learn-tutorial.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/tutorials/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/tutorials/">Tutorials</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Blackboard Learn 9.1 </h2>
                        
                        
                        <h3>Blackboard Mobile Learn App-NEW! </h3>
                        
                        <ul>
                           
                           <li><span><a href="../../../../faculty/development/centers/learn-it/BlackboardMobileLearn.html" target="_blank">Access and Features </a></span></li>
                           
                        </ul>
                        
                        <h3>Blackboard Help for Students</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Student">Blackboard Help for Students 9.1 SP 14 </a>(Great resources and easily search)
                           </li>
                           
                           <li>
                              <a href="http://learn.valenciacollege.edu/webapps/blackboard/execute/courseMain?course_id=_1310_1">Valencia Online 101: The Student Experince </a>- <span>Atlas Username &amp; Password Required! </span>
                              
                           </li>
                           
                           <li>
                              <a href="../getting-help/index.html">Technical Support</a> - <span>24/7 Phone Support </span><strong>407-582-5600 </strong>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Blackboard On Demand Videos </h3>
                        
                        <h2><strong><a href="http://www.youtube.com/playlist?list=PLontYaReEU1uzR5405Nhi_-y5qNCjrK71">Working in Your Course</a></strong></h2>
                        
                        <p>This set of videos helps you understand how to access your course content, take tests,
                           pass in assignments and other course related activities.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://youtu.be/J_aZCVQYSj0">Global Navigation and My Blackboard</a></li>
                           
                           <li><a href="http://youtu.be/36kDE4lvRmI">Taking a Test Online</a></li>
                           
                           <li><a href="http://youtu.be/7ZuZW9-KAjY">Submitting an Assignment</a></li>
                           
                           <li><a href="http://youtu.be/S5H_eguCKU8">Checking Your Grades</a></li>
                           
                           <li><a href="http://youtu.be/_RrfRzWLw7M">Using the Content Editor</a></li>
                           
                           <li><a href="http://youtu.be/zR5OH5SIR1g">Working in Groups</a></li>
                           
                           <li><a href="http://youtu.be/OBTsn60Qm-I">Creating and Editing a Blog Entry</a></li>
                           
                           <li><a href="http://youtu.be/UxjOU5sXmns">How to Use the Discussion Board</a></li>
                           
                           <li><a href="http://youtu.be/vvy7LgC4MXA">Creating and Editing Wiki Pages</a></li>
                           
                           <li><a href="http://youtu.be/IvrzuPqYrkE">Creating and Commenting on Journal Entries</a></li>
                           
                           <li><a href="http://youtu.be/lTBdyzkCHqM">How to Quickly Jump from One Course to Another</a></li>
                           
                        </ul>            
                        <h2><strong><a href="http://www.youtube.com/playlist?list=PLontYaReEU1ub17f3GEdWuAZE1Dz-27QM">Staying Organized and Communicating</a></strong></h2>
                        
                        <p>This set of videos helps you learn how to use the course tools to communicate with
                           others in your course and manage your course work.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://youtu.be/znPR4uzEM7M">How to Send Email from Your Course</a></li>
                           
                           <li><a href="http://youtu.be/Qm7J9zHMBIk">How to Create and Manage Tasks</a></li>
                           
                           <li><a href="http://youtu.be/OaZBBDKvSMc">How to Use the Calendar</a></li>
                           
                           <li><a href="http://youtu.be/OUJ6YXjcdhc">How to Send and Receive Blackboard Course Messages</a></li>
                           
                        </ul>            
                        <h2><strong><a href="http://www.youtube.com/playlist?list=PLontYaReEU1tRg4HseWUMhosPEr8dwEKR">Setting Your Preferences and Controlling Your Environment</a></strong></h2>
                        
                        <p>This set of videos helps you customize your Blackboard experience and set your preferences.</p>
                        
                        <ul>
                           
                           <li><a href="https://www.youtube.com/watch?v=rIsVvOutniI">My Blackboard Profiles</a></li>
                           
                           <li><a href="http://youtu.be/rIsVvOutniI">How to Create, Import, or Reconcile a MyEdu Profile</a></li>
                           
                           <li><a href="http://youtu.be/2ZIdCqhc-8E">Setting Your Notification Options</a></li>
                           
                           <li><a href="http://youtu.be/llncdgZ9YrE">Editing Personal Information and Privacy Options</a></li>
                           
                           <li><a href="http://youtu.be/A2JD2yekQvQ">How to Change Your Password</a></li>
                           
                           <li><a href="http://youtu.be/MjAE7eQNmK8">Adding an Avatar</a></li>
                           
                           <li><a href="http://youtu.be/qvhPFBKHik0">Customizing Your My Institution Page</a></li>
                           
                           <li><a href="http://youtu.be/Mvak9JG7a0c">How to Customize Your Group Home Page</a></li>
                           
                        </ul>            
                        <h2><strong><a href="http://www.youtube.com/playlist?list=PLontYaReEU1seUE3ACG3sEc3zR7Br7URU">All Student Videos</a></strong></h2>
                        
                        <p>The complete series of short video tutorials to help students learn how to use the
                           various Blackboard Learn tools to be successful online.
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/tutorials/blackboard-learn-tutorial.pcf">©</a>
      </div>
   </body>
</html>