<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../../index.php">Home</a></li>
<li><a href="../../services/index.php">Services</a></li>
<li><a href="../../services/instructional-design/index.php">Instructional Design Services</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/services/instructional-design/faculty-training.php">Faculty Training</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/services/instructional-design/emerging-technologies.php">Emerging Technologies</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/services/instructional-design/instructional-advice.php">Instructional Advice</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/services/instructional-design/online-accessibility.php">Online Accessibilty</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/services/instructional-design/copyright.php">Copyright</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/services/instructional-design/plagrism.php">Plagrism</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/services/instructional-design/testing.php">Testing</a></li>
<li><a href="../../services/valencia-productions/index.php">Valencia productions</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/valencia-productions/the-productions-process.php">The productions Process</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/valencia-productions/streaming-media-server.php">Streaming Media Server</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/valencia-productions/samples.php">Samples</a></li>
<li><a href="../../services/classroom-technology/index.php">Classroom Technology</a></li>
<li><a href="../../services/classroom-technology/tier-class/index.php">Tier Classrooms</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/purpose.php">Mission and Purpose</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/equipment.php">Equipment</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/rooms.php">Classrooms</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/brightroom.php">Classrooms TiER Levels</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/panorama.php">360 Panorama</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/training.php">Training</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/policies.php">Policies</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/procedures.php">Procedures</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/tier-class/troubleshooting.php">Troubleshooting</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/learning-technology-services/services/classroom-technology/tier-class/contact.cfm" target="_blank">Contact Us</a></li>
<li><a href="../../services/classroom-technology/digital-tv/index.php">Digital TV</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/Services/classroom-technology/digital-tv/contactUs.php">Contact Us</a></li>
<li><a href="../../faculty-resources/index.php">Faculty Resources</a></li>
<li><a href="../../faculty-resources/blackboard/index.php">Blackboard Learn 9.1</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-mobile.php">Blackboard Learn Mobile</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-resources.php">Blackboard Resources</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-plugins-software.php">Plugins &amp; Software</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/faculty-resources/blackboard/BlackboardIssues.php">Blackboard Issues</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/faculty-resources/getting-help.php">Getting Help</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/faculty-resources/kaltura.php">Kaltura</a></li>
<li><a href="../index.php">Student Resources</a></li>
<li><a href="index.php">Am I Ready for Online?</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/amiready/course-preview.php">What does an online course look like?</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/amiready/skill-requirements.php">Am I ready for Online Learning? </a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/amiready/definition-methods.php">Instructional Method Defintions</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/amiready/how-to-register.php">How to Register</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/amiready/courses-offered.php">Courses Offered</a></li>
<li><a href="../getting-started-online/index.php">Getting Started Online</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/getting-started-online/online-101.php">Valencia Online 101</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/getting-started-online/how-to-login.php">How to Login</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/getting-started-online/going-to-class.php">Going to Class</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/getting-started-online/system-requirements.php">System Requirements</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/getting-started-online/browser-requirements.php">Browser Requirements</a></li>
<li><a href="../academic-integrity/index.php">Academic Integrity</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/academic-integrity/plagiarism.php">Plagiarism</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/academic-integrity/netiquette.php">Netiquette</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/academic-integrity/security.php">Security</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/academic-integrity/copyright.php">Copyright</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/academic-integrity/policies.php">Policies and Responsibilities</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/academic-integrity/academic-research.php">Academic Research</a></li>
<li><a href="../tutorials/index.php">Tutorials</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/tutorials/blackboard-learn-tutorial.php">Blackboard Learn</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/tutorials/respondus-tutorial.php">Respondus LockDown Browser</a></li>
<li><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/tutorials/safeassign-tutorials.php">Using SafeAssign</a></li>
<li><a href="../getting-help/index.php">Getting Help</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/learning-technology-services/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
