<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>What does an online course look like? | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/BBLearn_Course_Demo_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/amiready/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/">Amiready</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/">Bblearn Course Demo</a></li>
               <li>What does an online course look like?</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>What does an online course look like?</strong><br>Blackboard Learn 9.1
               </p>
               
               
               <p>&nbsp;&nbsp;<img class="resizable" hspace="30" align="right" alt="Woman_computer.jpg" height="274" border="0" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/Woman_computer.jpg" vspace="10" width="163"></p>
               
               <p>This tutorial will introduce you to the essential look and feel of the Blackboard
                  Learn 9.1 Management system. At the end of this overview you will be able to recognize:
               </p>
               
               <ul>
                  
                  <li>The course menu</li>
                  
                  <li>Main areas of the course</li>
                  
                  <li>Folder view of course content</li>
                  
                  <li>Learning Module view of course content</li>
                  
               </ul>
               
               <p>You will also be able to preview tutorials covering how to perform essential student
                  tasks in Blackboard Learn 9.1:
               </p>
               
               <ul>
                  
                  <li>Creating a discussion board thread</li>
                  
                  <li>View and submit assignments</li>
                  
                  <li>How to take tests online</li>
                  
                  <li>How to view grades</li>
                  
               </ul>
               
               <p align="center"></p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td valign="top" width="190px">
                        
                        <p align="center"><img class="resizable" hspace="30" height="332" alt="Course_menu.jpg" border="1" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/Course_menu.jpg" vspace="10" width="182"></p>
                        
                        <p align="center"><strong><font size="5">COURSE MENU</font></strong></p>
                        
                     </td>
                     
                     <td valign="top" width="854px">
                        
                        <p><strong>What does an online course look like in Blackboard Learn 9.1?</strong></p>
                        
                        <p>Once you have entered a Blackboard course, you will always be able to see&nbsp;a&nbsp;list of&nbsp;buttons
                           or links&nbsp;down the left hand side of the screen. This is known as the Course Menu.&nbsp;Use
                           these buttons or text links to access and view various areas of the course.&nbsp;Each&nbsp;Course
                           Menu&nbsp;may vary slightly&nbsp;depending on your teacher's preferences. Keep in mind your
                           courses may look a bit different from each other and have different sets of tools
                           available.
                        </p>
                        
                        <p>Instructors customarily will group related items together under a particular navigation
                           link.&nbsp; For example,&nbsp;the syllabus, course policies and schedule might be found in a
                           section called Course Overview.&nbsp;Handouts, assignments, and lecture notes might be
                           found under a link titled Lesson One, Chapter Materials, or Course Documents. Within
                           these areas, you will typically find&nbsp;your assigned learning materials in the form
                           of&nbsp;text, graphics, and files.&nbsp;<img class="resizable" hspace="30" align="right" alt="ThumbBB_Student_Movie .png" height="107" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/ThumbBB_Student_Movie%20.png" border="0" vspace="10" metadata="obj3" width="184"></p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td valign="top">
                        
                        <p><img class="resizable" hspace="30" align="left" alt="content-frame.jpg" height="377" border="0" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/content-frame.jpg" vspace="10" width="500"></p>
                        
                        <p>&nbsp;</p>
                        
                        <p><strong>Main Areas</strong></p>
                        
                        <p>Upon entering a course, you will see something similar to the illustration. Notice
                           that even within a course, the top area will remain constant.&nbsp;<br>
                           We refer to the tabs and buttons&nbsp;in that area as the <strong>Tab Area</strong> and they stay with you no matter where you are in Blackboard.&nbsp;
                        </p>
                        
                        <p>&nbsp;The <strong>Breadcrumbs</strong> allow you to navigate backwards to the previous area you visited in your course.
                        </p>
                        
                        <p>To the left will always be your <strong>Course Menu</strong> and to the right you will always find the course content and tool areas.&nbsp;When you
                           click on a menu item on the left, the area to right changes.&nbsp;We refer to this area
                           on the right as the <strong>Content Frame</strong></p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td>
                        
                        <p><strong>Folders</strong></p>
                        
                        <p>Your instructor has the option of organizing course content into folders. Inside folders
                           you will find various kinds of items. Files such as PowerPoints, Lesson Plans, Tests
                           and Assignments, Items containing text, images and weblinks.You will also find links
                           to other areas of your course such as Discussion Boards and Tests.
                        </p>
                        
                        <p><img class="resizable" hspace="30" height="516" alt="Folders.jpg" border="0" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/Folders.jpg" vspace="10" width="687"></p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p align="center"></p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td>
                        
                        <p><strong>Learning Modules</strong><br>
                           Your instructor may make use of a Learning Module, which&nbsp;automatically sets a linear
                           path for progressing through the content and learning activities.&nbsp;As&nbsp;opposed to&nbsp;scrolling&nbsp;down
                           to see the information, you would proceed through a&nbsp;sequence of screens using the
                           links on the Table of Contents or the forward and back arrows to move from one page
                           to the next. <img class="resizable" hspace="30" height="294" alt="Learning_Module.jpg" border="0" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/Learning_Module.jpg" vspace="10" width="700"></p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>Blackboard Learn 9.1 is open, flexible, and centered on enhancing your academic achievement.
                  Below find links to interactive video tutorials on how to perform essential tasks
                  in Blackboard Learn 9.1.
               </p>
               
               <p>
                  <img class="resizable" hspace="30" align="left" alt="distance_learn-copy.jpg" height="140" border="1" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/distance_learn-copy.jpg" vspace="10" width="300">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <ul>
                  
                  <li>Creating a discussion board thread</li>
                  
                  <li>Viewing and submitting assignments</li>
                  
                  <li>How to take tests online</li>
                  
                  <li>How to view grades</li>
                  
               </ul>
               
               <p>For more information on becoming and succeeding as an online student continue exploring:<br>
                  Valencia Online Student Resources
               </p>
               
               <p>&nbsp;</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/BBLearn_Course_Demo_print.pcf">©</a>
      </div>
   </body>
</html>