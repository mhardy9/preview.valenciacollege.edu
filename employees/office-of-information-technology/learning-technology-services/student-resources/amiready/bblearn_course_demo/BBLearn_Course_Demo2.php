<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 2 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/BBLearn_Course_Demo2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/amiready/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/">Amiready</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/">Bblearn Course Demo</a></li>
               <li>Page 2</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <table class="table ">
                  
                  <tr>
                     
                     <td valign="top" width="190px">
                        
                        <p align="center"><img class="resizable" hspace="30" height="332" alt="Course_menu.jpg" border="1" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/Course_menu.jpg" vspace="10" width="182"></p>
                        
                        <p align="center"><strong><font size="5" style="line-height: 1">COURSE MENU</font></strong></p>
                        
                     </td>
                     
                     <td valign="top" width="854px">
                        
                        <p><strong>What does an online course look like in Blackboard Learn 9.1?</strong></p>
                        
                        <p>Once you have entered a Blackboard course, you will always be able to see&nbsp;a&nbsp;list of&nbsp;buttons
                           or links&nbsp;down the left hand side of the screen. This is known as the Course Menu.&nbsp;Use
                           these buttons or text links to access and view various areas of the course.&nbsp;Each&nbsp;Course
                           Menu&nbsp;may vary slightly&nbsp;depending on your teacher's preferences. Keep in mind your
                           courses may look a bit different from each other and have different sets of tools
                           available.
                        </p>
                        
                        <p>Instructors customarily will group related items together under a particular navigation
                           link.&nbsp; For example,&nbsp;the syllabus, course policies and schedule might be found in a
                           section called Course Overview.&nbsp;Handouts, assignments, and lecture notes might be
                           found under a link titled Lesson One, Chapter Materials, or Course Documents. Within
                           these areas, you will typically find&nbsp;your assigned learning materials in the form
                           of&nbsp;text, graphics, and files.&nbsp;<img class="resizable" hspace="30" align="right" alt="ThumbBB_Student_Movie .png" height="107" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/ThumbBB_Student_Movie%20.png" border="0" vspace="10" metadata="obj3" width="184"></p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="BBLearn_Course_Demo2.html#">return to top</a> | <a href="BBLearn_Course_Demo.html">previous page</a> | <a href="BBLearn_Course_Demo3.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/BBLearn_Course_Demo2.pcf">©</a>
      </div>
   </body>
</html>