<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 6 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/BBLearn_Course_Demo6.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/amiready/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/">Amiready</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/">Bblearn Course Demo</a></li>
               <li>Page 6</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>Blackboard Learn 9.1 is open, flexible, and centered on enhancing your academic achievement.
                  Below find links to interactive video tutorials on how to perform essential tasks
                  in Blackboard Learn 9.1.
               </p>
               
               <p>
                  <img class="resizable" hspace="30" align="left" alt="distance_learn-copy.jpg" height="140" border="1" src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/student-resources/amiready/BBLearn_Course_Demo/distance_learn-copy.jpg" vspace="10" width="300">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <ul>
                  
                  <li><a href="http://ondemand.blackboard.com/r91/movies/bb91_student_creating_discussion_board_post.htm/" target="_blank">Creating a discussion board thread</a></li>
                  
                  <li><a href="http://ondemand.blackboard.com/r91/movies/bb91_student_submit_assignment.htm" target="_blank">Viewing and submitting assignments</a></li>
                  
                  <li><a href="http://ondemand.blackboard.com/r91/movies/bb91_student_taking_test_online.htm" target="_blank">How to take tests online</a></li>
                  
                  <li><a href="http://ondemand.blackboard.com/r91/movies/bb91_student_checking_grades.htm" target="_blank">How to view grades</a></li>
                  
               </ul>
               
               <p>For more information on becoming and succeeding as an online student continue exploring:<br>
                  <a href="../../index.html" target="_blank">Valencia Online Student Resources</a></p>
               
               <p>&nbsp;</p>
               
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="BBLearn_Course_Demo6.html#">return to top</a> | <a href="BBLearn_Course_Demo5.html">previous page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/bblearn_course_demo/BBLearn_Course_Demo6.pcf">©</a>
      </div>
   </body>
</html>