<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/how-to-register.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/amiready/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/">Amiready</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>How to Register- Steps to Enrollment </h2>
                        
                        <h3>Step 1: Apply for Admissions and Financial Aid </h3>
                        
                        <p>Apply via the online&nbsp;<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm"><u>Application for Admission</u></a>.
                        </p>
                        
                        <ul>
                           
                           <li>Be sure to complete the&nbsp;<a href="../../../../admissions-records/forms.html"><u>Statement of Florida Residency for Tuition Purposes</u></a>&nbsp;form in order to be considered for in-state&nbsp;<a href="../../../../business-office/tuition-fees.html"><u>Tuition &amp; Fee</u></a>&nbsp;rates.
                           </li>
                           
                        </ul>
                        
                        <p>Financial Aid Services</p>
                        
                        <p>It is suggested that you complete your financial aid application prior to the Financial
                           Aid Priority Deadline (deadlines can be found in our online&nbsp;<a href="../../../../calendar/index.html"><u>Important Dates &amp; Deadlines</u></a>calendar). You can still apply for financial aid after the deadline, but you are not
                           guaranteed to receive aid in time to pay for classes. Please see the&nbsp;<a href="../../../../finaid/index.html"><u>Financial Aid Services</u></a>web site for more information about financial aid.
                        </p>
                        
                        <ul>
                           
                           <li>Complete the&nbsp;<a href="http://www.fafsa.ed.gov/"><u>Free Application for Federal Student Aid (FAFSA)</u></a>&nbsp;online. Allow at least 6 weeks for your FAFSA to be processed. Valencia's school
                              code is&nbsp;006750.
                           </li>
                           
                           <li>To receive financial aid, you must be pursuing an Associate in Arts (A.A.) degree,
                              an Associate in Science (A.S.) degree, a Bachelor of Science (B.S.) degree, or an
                              eligible&nbsp;<a href="../../../../finaid/gettingstarted/training_cert.html"><u>Vocational Training Certificate</u></a>&nbsp;program approved for financial aid.
                           </li>
                           
                           <li>Submit all official transcripts (high school and/or college) to Valencia College.</li>
                           
                        </ul>
                        
                        <p>First Time in College Students: Request your official final high school transcript
                           showing your date of graduation from high school or proof of your GED. You will be
                           classified as a non-degree-seeking student until your official final high school transcript
                           has been received.&nbsp;<br>
                           <br>
                           Transfer Students: Request official transcripts from all colleges that you ever attended.
                           It is suggested that you also have your official final high school transcript or proof
                           of your GED sent to Valencia. College transcripts must be received&nbsp;and&nbsp;evaluated before
                           aid will be awarded.
                        </p>            
                        <h3>2. Create Your Atlas Account</h3>
                        
                        
                        <p>Atlas is Valencia's online learning community that connects faculty, students, and
                           staff to the resources they need to succeed at Valencia. Students can obtain very
                           important information regarding registration, financial aid, and course planning in
                           their Atlas account. Students will also have an Atlas email account which is Valencia's
                           official means of communication with students.
                        </p>
                        
                        <p>Click here for&nbsp;<a href="https://atlas.valenciacollege.edu/"><u>Atlas</u></a>. Once at the Atlas log-in page, click on the "Sign up for an account" link and follow
                           the prompts to create your account. Your Atlas account can be created&nbsp;3 - 5 business
                           days after your application has been submitted. The only information you need in order
                           to create your Atlas account is your name, Valencia Identification Number (VID) or
                           Social Security Number (SSN), and your date of birth. Be sure to save your Atlas username
                           and password in a safe place. Atlas will be very important for the&nbsp;<a href="../../../../student-services/new-student-orientation.html"><u>New Student Orientation</u></a>&nbsp;and registration steps that follow.&nbsp;<br>
                           <br>
                           If you need assistance with your Atlas account please visit one of our&nbsp;<a href="../../../../student-services/atlas-access-labs.html"><u>Atlas Access Labs</u></a>.
                        </p>
                        
                        <p><br>
                           
                        </p>
                        
                        <h3>Step 3: Take Entry Testing &amp; Attend New Student Orientation</h3>
                        
                        <p>Assessment</p>
                        
                        <ul>
                           
                           <li>Degree seeking students are required to complete an Entry Placement Test. Proof of
                              completed college-level courses in math and English may be substituted for portions
                              of the assessment. Or, if you have taken the ACT, CPT, PERT, and/or SAT within the
                              last two years and obtained&nbsp;<a href="../../../../assessments/placement-chart.html"><u>satisfactory scores</u></a>&nbsp;you may waive assessment in those skill areas.
                           </li>
                           
                           <li>You are strongly encouraged to&nbsp;<a href="../../../../assessments/testing-rules.html"><u>prepare</u></a>&nbsp;for the Entry Placement Test.&nbsp;<br>
                              
                           </li>
                           
                        </ul>
                        
                        <p>New Student Orientation</p>
                        
                        <ul>
                           
                           <li>After&nbsp;you have completed your assessment, sign up for a mandatory&nbsp;<a href="../../../../student-services/new-student-orientation.html"><u>New Student Orientation</u></a>&nbsp;session via your&nbsp;<a href="https://atlas.valenciacollege.edu/"><u>Atlas</u></a>&nbsp;account.
                           </li>
                           
                           <li>Attend New Student Orientation. At orientation we will review Valencia's degree programs,
                              degree requirements, policies and procedures, and how your placement test impacts
                              your first term course selection. If your official college transcripts have not yet
                              been received by Valencia, to ensure proper advisement, you will need to bring an
                              unofficial copy of your transcript(s) with you to your orientation session. During
                              orientation you will meet with an&nbsp;<a href="../../../../advising-counseling/index.html"><u>Academic Advisor</u></a>&nbsp;to develop an education plan and select classes.
                           </li>
                           
                        </ul>  
                        
                        <h3>Step 4: Register for Classes &amp; Pay Tuition and Fees</h3>
                        
                        <p>Register</p>
                        
                        <ul>
                           
                           <li>You register for classes online through your&nbsp;<a href="https://atlas.valenciacollege.edu/"><u>Atlas</u></a>&nbsp;account by clicking on theRegistration&nbsp;tab and then selecting&nbsp;Register for Classes.
                           </li>
                           
                           <li>Check your Florida Residency status in&nbsp;<a href="https://atlas.valenciacollege.edu/"><u>Atlas</u></a>&nbsp;by clicking on the&nbsp;Registration&nbsp;tab, selecting&nbsp;Transcripts, Grades &amp; Holds, and then
                              selecting&nbsp;Student Information. All new and readmit students (you are a readmit student
                              if you have not attended classes at Valencia in two or more years) are classified
                              as non-Florida Residents at the time of admission/re-admission to the College. To
                              be considered for Florida Residency and thus be eligible for in-state tuition &amp; fees,
                              be sure to complete the<a href="../../../../admissions-records/forms.html"><u>Statement of Florida Residency for Tuition Purposes</u></a>&nbsp;form.
                           </li>
                           
                           <li>Due to state and federal regulations, Valencia College requires all students registered
                              in an online course to reside in Florida and to provide a mailing address in the state
                              of Florida. If you do not have a mailing address in Florida, you will be deleted from
                              any online course(s) in which you register. You do not need to be a Florida Resident
                              for Tuition Purposes to register for online courses but the active mailing address
                              on your Valencia College student record must be in the state of Florida.
                           </li>
                           
                        </ul>
                        
                        <p>Pay Tuition</p>
                        
                        <ul>
                           
                           <li>After registering for classes, you may also pay for your classes through your Atlas
                              account. If you wish to pay in person, visit the&nbsp;<a href="../../../../business-office/index.html"><u>Business Office</u></a>. Credit card (American Express, Discover, MasterCard, and Visa) and cash, check,
                              or money order payments are accepted in a drop box after business hours.
                           </li>
                           
                           <li>A&nbsp;<a href="../../../../business-office/tuition-installment-plan/index.html"><u>Tuition Installment Plan (TIP)</u></a>&nbsp;is available for students who need assistance with managing their education expenses.&nbsp;
                              &nbsp;
                           </li>
                           
                           <li>IMPORTANT: &nbsp;If you have applied for financial aid and are awaiting word on your eligibility,
                              you will need to pay for classes yourself by the Fee Payment Deadline (see the&nbsp;<a href="../../../../calendar/index.html"><u>Important Dates &amp; Deadlines</u></a>&nbsp;calendar for Fee Payment Deadlines) or your classes will be dropped for non-payment.
                              If you are currently receiving financial aid, check with the Answer Center to ensure
                              that all paperwork has been received and that you will not be dropped from your classes.
                           </li>
                           
                           <li><a href="../../../../business-office/tuition-fees.html"><u>Tuition &amp; Fee Schedule</u></a></li>
                           
                        </ul>    
                        
                        <h3>Step 5: Get Your Student Identification Card and Parking Decal</h3>
                        
                        <ul>
                           
                           <li>You must show a paid receipt of classes and a photo I.D. in order to receive your
                              student identification card. To obtain your student I.D., go to Student Development.
                           </li>
                           
                           <li>Parking decals are free and required for all students. The parking decal can be requested
                              via your Atlas account. &nbsp;
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Step 6: Buy Your Books &amp; Go To Class</h3>
                        
                        <p>Purchase Your Books</p>
                        
                        <ul>
                           
                           <li>Print your&nbsp;Student Detail Schedule&nbsp;from your&nbsp;<a href="https://atlas.valenciacollege.edu/"><u>Atlas</u></a>&nbsp;account and take it to the<a href="../../../../locations-store/index.html"><u>Bookstore</u></a>&nbsp;or purchase your books&nbsp;<a href="../../../../locations-store/faq.html"><u>online</u></a>.
                           </li>
                           
                        </ul>
                        
                        <p>Go to Class(es)</p>
                        
                        <p>See the online&nbsp;<a href="../../../../calendar/index.html"><u>Important Dates &amp; Deadlines</u></a>&nbsp;calendar for the dates that classes begin.
                        </p>
                        
                        <ul>
                           
                           <li>Your&nbsp;Student Detail Schedule&nbsp;from your&nbsp;<a href="https://atlas.valenciacollege.edu/"><u>Atlas</u></a>&nbsp;account includes the day(s), time, and location of your class(es).
                           </li>
                           
                           <li>Note:&nbsp;Students are required to attend the first day that each class meets or they
                              may be withdrawn as a "no show" and will still be responsible for the cost of the
                              class(es).
                           </li>
                           
                        </ul>  
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/amiready/how-to-register.pcf">©</a>
      </div>
   </body>
</html>