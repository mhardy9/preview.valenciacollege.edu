<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/student-resources/getting-help/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/student-resources/getting-help/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources/">Student Resources</a></li>
               <li>Getting Help</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Getting Help - Students</h2>
                        
                        <p><img alt="Student frustrated " height="200" src="reading_asiancopy.jpg" width="485"></p>
                        
                        <h3>Getting help is just a click away!</h3>
                        
                        <p>                Under this section you will find links to wide variety of campus services
                           to facilitate navigating through your academic life.
                        </p>
                        <a href="../../../../students/labs/index.html" target="_blank">
                           <h3>Academic Computer Labs</h3>
                           </a>
                        
                        <p> What are campus lab hours?</p>            
                        
                        <h3><a href="../../../../students/student-services/index.html" target="_blank">Student Services</a></h3>
                        
                        <p> Need to talk to a 
                           Financial Aid Specialist, Academic Advisor, or Counselor?
                        </p>            
                        
                        <h3><a href="../../../../calendar/index.html" target="_blank">Calendar</a></h3>
                        
                        <p> When the semester starts or ends?</p>            
                        
                        <h3><a href="../../../../library/index.html" target="_blank">Libraries</a></h3>
                        
                        <p> Writing a paper, where to find e-books, videos and/or audios?</p>            
                        
                        <h3><a href="../../../../office-for-students-with-disabilities/default.html">OSD</a></h3>
                        
                        <p> Need special accommodations for a class or assessments?</p>            
                        
                        <h3><a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home" target="_blank">Technical Support</a></h3>
                        
                        <p> Need help?</p>            
                        
                        <h3><a href="../../../../office-of-information-technology/learning-technology-services/student-resources/getting-help/test_proctoring.html">Test Proctoring</a></h3>
                        
                        <p>Where do I go? How is it arranged?</p>            
                        
                        <p>Remember that assistance is only a click away. Your instructors are here to help you
                           become a successful learner. We also offer a variety of technical assistance through
                           our Online Help Desk.
                        </p>            
                        
                        <p>Valencia students have several ways of getting assistance with courses that use the
                           WebCT/Blackboard online course development tool. For technical assistance with your
                           course, contact the helpdesk by: 
                        </p>
                        
                        <h3>Phone 407-582-5600 (24/7 Phone Support)</h3>            
                        
                        <p>You may also chat live with a support team member, submit a ticket with your issue
                           or concern, or review our <a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home">knowledge-base of resources.</a></p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                <a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home">Submit a Ticket</a><br>
                                                Submit a trouble ticket to our service representatives. You can track the status of
                                                your ticket in My Support.
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                <a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home">Live Chat</a><br>
                                                Chat with a service representative live over the web.
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/student-resources/getting-help/index.pcf">©</a>
      </div>
   </body>
</html>