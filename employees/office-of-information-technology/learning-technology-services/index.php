<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia College | Valencia College</title>
      <meta name="Description" content="A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/learning-technology-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div id="containerHeaderAlert">
                  
                  
                  
                  
                  
                  
                  
                  
               </div>
               
               
               <div id="containerTop">
                  
                  
                  <header><a name="top" id="top"></a>
                     
                     <div id="accessibility">
                        <a href="index.html#navigate" class="trkHeader">Skip to Local Navigation</a> | <a href="index.html#content" class="trkHeader">Skip to Content</a>
                        
                     </div>
                     
                     <div id="outContainerTopBar">
                        
                        <div id="topBar">
                           
                           <div id="logoandlogin">
                              
                              <div id="logo">
                                 
                                 
                                 <a href="../../index.html" class="trkHeader" aria-label="Valencia College"><img src="https://valenciacollege.edu/images/logos/valencia-college-logo.svg" alt="Valencia College" width="275" height="42" role="img"></a>
                                 
                                 
                              </div>
                              
                              <div class="AtlasLoginContainer"> 
                                 <a href="https://atlas.valenciacollege.edu/" title="Atlas Login" style="text-decoration:none;" class="trkHeader"><span class="AtlasIcon"></span> <span class="AtlasText"><span class="kern-2">A</span>T<span class="kern-1">LAS</span>&nbsp;<span class="kern-2">L</span><span class="kern-1">OGIN</span></span></a>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div id="navContainer" role="navigation" aria-label="Sitewide Navigation">
                           
                           <div id="nav">
                              
                              <ul class="navMenu">
                                 
                                 <li class="navLinks"><a href="http://preview.valenciacollege.edu/future-students/" id="nav-future" class="trkHeader">Future Students</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../../students/index.html" id="nav-current" class="trkHeader">Current Students</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../../faculty.html" id="nav-faculty" class="trkHeader">Faculty &amp; Staff</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../../visitors.html" id="nav-visitors" class="trkHeader">Visitors &amp; Friends</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="blank">
                                    
                                    <div class="util">
                                       
                                       <ul class="x_level1">
                                          
                                          <li class="x_quicklinks">
                                             <a href="../../includes/quicklinks.html" style="text-decoration:none;" class="trkHeader">Quick Links</a>
                                             
                                             <ul class="x_level2">
                                                
                                                <li class="x_quicklinks2"><a href="../../calendar/index.html" class="trkHeader">Academic Calendar</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../students/admissions-records/index.html" class="trkHeader">Admissions</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://net4.valenciacollege.edu/promos/internal/blackboard.cfm" class="trkHeader">Blackboard Learn</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://www.valenciabookstores.com/valencc/" class="trkHeader">Bookstore</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../business-office/index.html" class="trkHeader">Business Office</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../calendar/index.html" class="trkHeader">Calendars</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../map/index.html" class="trkHeader">Campuses</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../students/catalog/index.html" class="trkHeader">Catalog</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/schedule/" class="trkHeader">Class Schedule</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/continuing-education/" class="trkHeader">Continuing Education</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../programs/index.html" class="trkHeader">Degree &amp; Career Programs</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../departments/index.html" class="trkHeader">Departments</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/economicdevelopment/" class="trkHeader">Economic Development</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../human-resources/index.html" class="trkHeader">Employment</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../finaid/index.html" class="trkHeader">Financial Aid Services</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../international/index.html" class="trkHeader">International</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../library/index.html" class="trkHeader">Libraries</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../map/index.html" class="trkHeader">Locations</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../contact/directory.html" class="trkHeader">Phone Directory</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../students/admissions-records/index.html" class="trkHeader">Records/Transcripts</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../security/index.html" class="trkHeader">Security/Parking</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../../support/index.html" class="trkHeader">Support</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/events/" class="trkHeader">Valencia Events</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="blank">
                                    
                                    <div class="util">
                                       
                                       <div class="x_search" id="Search">
                                          
                                          <form label="search" class="relative" action="https://search.valenciacollege.edu/search" name="seek" id="seek" method="get">
                                             <label for="q"><span style="display:none;">Search</span></label>
                                             <input type="text" name="q" id="q" alt="Search" value="" aria-label="Search">
                                             <button style="position:absolute; top:0; right:12;" type="submit" name="submitMe" value="seek" onmouseover="clearTextSubmit(document.seek.q)" onfocus="clearTextSubmit(document.seek.q)" onmouseout="clearTextSubmitOut(document.seek.q)" onblur="clearTextSubmitOut(document.seek.q)">go</button>
                                             <input type="hidden" name="site" value="default_collection">
                                             <input type="hidden" name="client" value="default_frontend">
                                             <input type="hidden" name="proxystylesheet" value="default_frontend">
                                             <input type="hidden" name="output" value="xml_no_dtd">
                                             
                                          </form>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </li>
                                 
                              </ul>
                              
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </header>
                  
               </div>
               
               <div id="containerMain">
                  
                  
                  <div id="containerContent" role="main">
                     
                     <div id="wrapper">
                        
                        <div id="crumb">
                           <a href="../../default.html">Home</a>
                           <img src="http://valenciacollege.edu/images/arrow.gif" width="8" height="8" alt=""> <a href="../index.html">OIT</a>
                           <img src="http://valenciacollege.edu/images/arrow.gif" width="8" height="8" alt=""> <a href="index.html">Learning Technology Services</a>
                           
                           
                        </div>
                        
                        
                        
                        <div class="container_12">
                           
                           
                           
                           <div class="grid_12">
                              
                              
                              <div id="SPslideshow">
                                 
                                 
                                 
                                 <div id="SPmainImage"> 
                                    
                                    <div class="SPmainImage">
                                       <img src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/images/featured-lts.jpg" width="930" height="314" alt="" border="0">
                                       
                                       
                                       <div id="transtextbox" class="transtext"><strong>LEARNING TECHNOLOGY SERVICES</strong></div>
                                       
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;" class="SPmainImage">
                                       <img src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/images/24-7Support.jpg" width="930" height="314" alt="24-7 Support" border="0">
                                       
                                       <div id="transHide">
                                          
                                          <div id="transtextbox" class="transtext">Valencia students and faculty have several ways of getting assistance with online
                                             courses. For technical assistance with your course, <span class="transtextBold">contact 407-582-5600 for 24/7 phone support.</span>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;" class="SPmainImage">
                                       <img src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/images/1on1Support.jpg" width="930" height="314" alt="1 on 1 Support" border="0">
                                       
                                       <div id="transHide">
                                          
                                          <div id="transtextbox" class="transtext">Faculty one-to-one consultations are available to support all faculty in a broad range
                                             of topics. Contact the instructional designer on your campus to arrange an one-to-one
                                             consultation.
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="mainImage" style="display:none;">
                                       <img src="http://valenciacollege.edu/office-of-information-technology/learning-technology-services/images/Valencia%20Productions.jpg" width="930" height="314" alt="Valencia Productions" border="0">
                                       
                                       <div id="transHide">
                                          
                                          <div id="transtextbox" class="transtext"><span class="transtextBold"> Valencia Productions provides support to faculty wishing to enhance their courses
                                                with multimedia and instructional video. Our team will help you determine the right
                                                tools to support your educational objectives and walk you through the process of creating
                                                materials for your classes.</span></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              
                              
                           </div>
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                           <div class="grid_12">&nbsp;</div>
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                           
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:360px;">
                                 
                                 
                                 <h1>QUICK LINKS</h1>
                                 
                                 <div class="button-color1-245"><a href="services/index.html" title=" ">SERVICES</a></div>
                                 <br>
                                 
                                 <div class="button-color1-245"><a href="faculty-resources/index.html" title=" ">FACULTY RESOURCES</a></div>
                                 <br>
                                 
                                 <div class="button-color1-245"><a href="student-resources/index.html" title=" ">STUDENT RESOURCES </a></div>
                                 <br>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:360px;">
                                 
                                 
                                 <h1>BLOG</h1>
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       <strong><a href="http://wp.valenciacollege.edu/lts-blog/2017/01/05/security-warning-malicious-oneclass-chrome-extension/" target="_blank" title="Security Warning – Malicious OneClass Chrome Extension">Security Warning – Malicious OneClass Chrome Extension</a></strong>
                                       <br> A malicious OneClass Chrome Extension is part of a current phishing scam which, if
                                       installed, may send  ...
                                       
                                       <br><br>
                                       
                                    </div>
                                    
                                    
                                    <div>
                                       <strong><a href="http://wp.valenciacollege.edu/lts-blog/2017/01/04/discussion-board-forum-access-denied-error/" target="_blank" title="Discussion Board Forum “Access Denied” Error">Discussion Board Forum “Access Denied” Error</a></strong>
                                       <br> The latest update to Blackboard has produced a bug that will cause an “Access Denied”
                                       error message  ...
                                       
                                       <br><br>
                                       
                                    </div>
                                    
                                    
                                    <div>
                                       <strong><a href="http://wp.valenciacollege.edu/lts-blog/2017/01/04/blackboard-gamegogy-leaderboard/" target="_blank" title="Blackboard Gamegogy Leaderboard">Blackboard Gamegogy Leaderboard</a></strong>
                                       <br> Want to add some gamification to your Blackboard course?&nbsp; The&nbsp;Gamegogy&nbsp;Leaderboard
                                       module displays a  ...
                                       
                                       <br><br>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:360px;">
                                 
                                 
                                 <h1>GETTING HELP</h1>
                                 
                                 <div style="width: 270px;"> Faculty can visit a<a href="../../faculty/development/centers/locations.html" target="_blank"> Center for Teaching/Learning Innovation</a> or make an appointment with one of our <a href="../../office-of-information-technology/learning-technology-services/services/instructional-design/instructional-advice.html" target="_blank">Instructional Designers</a> for assistance. <br>
                                    <br>
                                    Valencia students and faculty have several ways of getting assistance with courses
                                    that use the Blackboard online course development tool. For technical assistance with
                                    your course, contact the online helpdesk:<br>
                                    <br>
                                    
                                    <strong>Phone 407-582-5600 (24/7 Phone Support)</strong><br><br>
                                    
                                    You may also <a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home" target="_blank">chat live</a> with a support team member, <a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home" target="_blank">submit a ticket</a> with your issue or concern, or review our <a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home" target="_blank">knowledge-base of resources</a>.
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                           <div class="grid_12">&nbsp;</div>
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                           
                        </div>          
                        
                        
                     </div>
                     
                  </div>
                  
                  
               </div> 
               
               <div id="containerBtm">
                  
                  
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/index.pcf">©</a>
      </div>
   </body>
</html>