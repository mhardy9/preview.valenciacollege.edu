<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/services/valencia-productions/samples.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/services/valencia-productions/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/">Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/valencia-productions/">Valencia Productions</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Samples</h3>
                        
                        
                        <h3>Online Audiovisual Course Introductions </h3>
                        <img alt="Orientation Video Sample Still Shot" height="166" hspace="8" src="OrientationVideoLTAD.jpg" vspace="8" width="300"><p>With the aid of Camtasia software, our television studio, and other audiovisual creating
                           tools, Valencia faculty members are able to create a personal presence in the online
                           learning environment.
                           Options are plenty. Faculty can select to create a: high-end video class orientation,
                           video lectures with embedded Power Point slides including navigation through webpages,
                           audiovisual presentations with sound and screen captures including course introduction
                           podcasts for learners on the go. Faculty is able to deliver audiovisual interaction,
                           until now, only possible in the classroom.
                        </p> 
                        
                        <h4>Faculty Experience with Online Video Orientation</h4> 
                        Prof. Angelique Smith: "Having the opportunity to provide a face to face interaction
                        with my students in the online environment has brought a sense of connection and intimacy
                        only thought possible in the traditional classroom."
                        
                        
                        <h3>Sample videos </h3>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <h4>Sample Course Introduction</h4>
                              <a href="http://websflash.valenciacollege.edu/videoPlayer.html?appl=Computer_Info_Tech&amp;inst=CTS1155&amp;vid=mp4%3A090825OrientationVideoforCTS1155" target="_blank">CTS 1155 </a>
                              
                           </div>
                           
                           
                           <div>
                              
                              <h4>Sample Module Introduction</h4>
                              
                              
                           </div>
                           
                           
                           
                           <br>
                           
                           
                           <div>
                              
                              <h4>Audio/Video Script Template: </h4>
                              <a href="documents/Online_Video_Template.doc">Audio/Video Script Template</a>
                              
                           </div>
                           
                           
                           <div>
                              
                              <h4>Generic Script for Online Video Orientation</h4> <a href="documents/Generic_Online_Video_Orientation.doc">Generic Script</a>
                              
                           </div>
                           
                           <br>
                           
                           
                           <div>
                              
                              <h4>Sample Script:</h4>
                              <a href="documents/WelcometoEME2040.pdf">EME2040 Script by Angelique Smith</a>
                              
                           </div>
                           
                           
                           <div>
                              
                              <h4>&nbsp;</h4>
                              
                           </div>
                           
                        </div>
                        
                        <br>
                        
                        <h3>Sample Multimedia</h3>
                        <br>
                        
                        
                        <h4>Key Points to include:</h4>
                        
                        <ul>
                           
                           <li>Introduction of the subject area and personal interest in it.</li>
                           
                           <li>Content of course: 
                              
                              <ul>
                                 
                                 <li>What to expect</li>
                                 
                                 <li>Syllabus</li>
                                 
                                 <li>Calendar</li>
                                 
                                 <li>Grading</li>
                                 
                                 <li>How to communicate with each other and instructor.</li>
                                 
                              </ul>
                              
                              
                           </li>
                           
                           
                        </ul>
                        
                        
                        <h3>More... </h3>
                        
                        
                        <h4>Tutorial : How to plan your script</h4>
                        <a href="documents/Planning_Your_Script.pdf" target="_blank">Planning_Your_Script.pdf</a>
                        
                        
                        
                        
                        
                        <h4>Online Audiovisual Orientation: Getting Started: </h4>
                        <a href="documents/ValenciaProductionsRequest.pdf" target="_blank">Request Form</a>                    
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/services/valencia-productions/samples.pcf">©</a>
      </div>
   </body>
</html>