<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/services/instructional-design/testing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/services/instructional-design/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/">Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/instructional-design/">Instructional Design</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Online Testing: Best Practices</h3>
                        
                        <p>At first glance, online testing seems              problematic. How can cheating be
                           prevented? How do you stop students              from using books and other materials
                           to answer questions? Here are eleven ideas to make online assessment work:
                        </p>
                        
                        <dl>
                           
                           <ol>
                              
                              <li>Bring a new mindset to online assessment.
                                 
                                 <ol type="a">
                                    
                                    <li>Traditional approaches to testing don't work well online. A new                mindset
                                       is needed. That's not bad; most education experts agree                that rote memory
                                       testing is not the best measure of learning in                any environment. 
                                    </li>
                                    
                                    <li>In online courses, treat every test as if it                were "open book." Use
                                       questions that challenge students                even if they use resources when forming
                                       their answers. This                practice is more like our real life tests anyway.
                                       
                                    </li>
                                    
                                    <li>Treat online testing as a means, not an end.                Assessments should not
                                       only measure learning, but serve as part of                the learning process.<br>
                                       <br>
                                       
                                    </li>
                                    
                                 </ol>
                                 
                              </li>
                              
                              <li>Publicize content, format, rules, and                honor codes to students in advance.
                                 
                                 <ol type="a">
                                    
                                    <li>Students are less prone to cheating if they understand what to                expect
                                       on tests. Ensure that they are appropriately prepared. 
                                    </li>
                                    
                                    <li>Post the question formats, test length, and                time limitations well in
                                       advance. Post study guides that students                can use to prepare. In addition,
                                       announce rules for the test,                especially limitations on the resources
                                       students can use. 
                                    </li>
                                    
                                    <li>Finally,                post or link to the college policies so that students are
                                       aware of the                implications of cheating. <br>
                                       <br>
                                       
                                    </li>
                                    
                                 </ol>
                                 
                              </li>
                              
                              <li>Ask questions that require application of                knowledge.<br>
                                 The most important way to overcome online cheating (and                      realistically
                                 assess student understanding) is to use application                      level questions.
                                 Essays, case studies, and other complex question                      types can be
                                 challenging to answer even if you are looking at the                      book. <br>
                                 <br>
                                 
                              </li>
                              
                              <li>Only use memory-testing questions to                facilitate student progress. <br>
                                 There is room for some rote memory questions in online courses.                These
                                 questions confirm student understanding and establish common                context
                                 for learning. Instructors can use memory questions to                gauge the pace
                                 of the course and identify students who are lost. <br>
                                 <br>
                                 
                              </li>
                              
                              <li>Use software with test administration                features.<br>
                                 There are many kinds of online testing software. These packages                produce
                                 consistent test formatting and grade most questions. Use                administration
                                 features in these programs to enhance the practice                of online testing.<br>
                                 For instance, Blackboard CourseInfo has                mechanisms to not only grade
                                 tests, but also provide feedback so                students can learn from the questions
                                 they miss. For this to work,                test writers must include quality feedback
                                 and turn on the                appropriate options. Other common options include timers,
                                 the                choice to allow one try or multiple tries at the test, and    
                                 password protection.<br>
                                 <br>
                                 
                              </li>
                              
                              <li>Design alternate forms of the test. In                CourseInfo, use question pools.<br>
                                 One simple but important rule for test security is to make                different
                                 forms of the test available. Alter question order or                write tests with
                                 slightly different questions. Once a test is                online, it only takes
                                 a minute to create alternate forms with cut                and paste features.<br>
                                 <br>
                                 In CourseInfo, build question pools. When                constructing tests from pools,
                                 you can randomize sequence or pull                a different set of questions (but
                                 the same number of questions)                from the pool for each student. Once
                                 a question is in a pool, it                can also be pulled onto other tests in
                                 the course.<br>
                                 <br>
                                 
                              </li>
                              
                              <li>Learn the writing style of students before                testing.<br>
                                 A great deal of written communication passes between online                students
                                 and the instructor. Pay attention to the writing style of                students
                                 and save samples. Online instructors report that with a                little awareness,
                                 it is easy to recognize work that is not the                student's own.<br>
                                 <br>
                                 
                              </li>
                              
                              <li>Use questions that require personal input                from students.<br>
                                 Require some personal opinions from students in answers. Ask                students
                                 to provide examples from their own lives. These kinds of                personal details
                                 are difficult to fake.<br>
                                 <br>
                                 
                              </li>
                              
                              <li>Set a reasonable time limit for                completion.<br>
                                 Limiting the amount of time for which students can access a test                makes
                                 any form of cheating difficult to implement. Don't be too                restrictive,
                                 but don't give students forever either. In CourseInfo,                do not make
                                 the test available until the day you want students to                begin taking
                                 it.<br>
                                 <br>
                                 
                              </li>
                              
                              <li>If security is critical, consider local                proctoring.<br>
                                 Some colleges use local proctoring. When a test is given, students               
                                 must either visit the college or find a local proctor to monitor                their
                                 test. For instance, the fire prevention program at Coastal                Carolina
                                 CC uses firehouses around the state to monitor the tests                of online
                                 students. Valencia's Testing Centers are able to proctor your tests in a secure environment,
                                 visit <a href="../../../../testing-center/index.html" target="_blank">http://valenciacollege.edu/testingcenter/</a> to find information on the Testing Center on each campus. <br>
                                 This approach works if you have willing                proctors and time to mail tests
                                 to them, but can also create                problems. Many students take online classes
                                 because they cannot                physically attend a course at specified times.
                                 Requiring them to                test at a particular location precludes the very
                                 reason they                signed up for the course. So please, use this approach
                                 with care. <br>
                                 <br>
                                 
                              </li>
                              
                              <li>Don't worry too much.<br>
                                 Finally, online educators emphasize that online testing is more                problematic
                                 in theory than in practice. They report that the range                of scores is
                                 similar for online students and classroom students.                Test achievement
                                 by individual students is consistent with                performance in other course
                                 assignments. Incidence of cheating is                no more frequent than in face-to-face
                                 courses. <br>
                                 Design online tests with care, prepare                students fairly, and take a
                                 few reasonable anti-cheating                precautions. Success is likely to follow!
                                 
                              </li>
                              
                           </ol>
                           
                        </dl>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/services/instructional-design/testing.pcf">©</a>
      </div>
   </body>
</html>