<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/services/instructional-design/faculty-training.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/services/instructional-design/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/">Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/instructional-design/">Instructional Design</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Instructional Design Services-Faculty Training</h3>
                        
                        <p>We provide faculty training in the integration of   learning technologies into the
                           curriculum. Training includes a wide   array of instructional enhancements: from audio,
                           multimedia, interactive   learning objects and automated quiz applications, to innovative
                           approaches   such as social networking and mobile learning. 
                        </p>
                        
                        <p>                      We also provide you with the essential knowledge on how to navigate,
                           create and manage online teaching in our learning management system,   Blackboard
                           Learn.                    
                        </p>
                        
                        <p>We offer:</p>
                        
                        <ul>
                           
                           <li>LTAD 3182 Maximizing Groupwork in Online Courses </li>
                           
                           <li>LTAD 3183 Assessment Tools in Online Courses </li>
                           
                           <li>LTAD 3185 Using the Online Courses Grade Center </li>
                           
                           <li>LTAD 3186 Communication Tools in Online Courses </li>
                           
                           <li>LTAD 3282 Blackboard Essentials </li>
                           
                           <li>LTAD 3240 Multimedia Tools for All Courses </li>
                           
                           <li>LTAD  3285 Respondus and StudyMate </li>
                           
                           <li>LTAD 3270 SoftChalk Lesson Builder </li>
                           
                           <li>LTAD 3287 Online Video Orientations </li>
                           
                           <li>LTAD 3284 Mobile Learning </li>
                           
                           <li>LTAD 3288 Emerging Web 2.0 Technologies </li>
                           
                           <li>LTAD 3286 Tools for Plagiarism Prevention </li>
                           
                           <li>LTAD 3383 Teaching and Learning Social Media 2                    </li>
                           
                        </ul>
                        
                        <p><a href="../../../../faculty/development/howToRegisterForCourses.html">How to Register for Courses</a></p>
                        
                        <p><a href="http://events.valenciacollege.edu/group/facultydevelopment">Faculty Development Calendar</a></p>
                        
                        <p><a href="../../../../faculty/development/courseSearch.html" target="_blank">Search Faculty Development Courses</a></p>
                        
                        <p><a href="../../../../faculty/development/programs/online/index.html">Online Teaching &amp; Learning - Faculty Development</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/services/instructional-design/faculty-training.pcf">©</a>
      </div>
   </body>
</html>