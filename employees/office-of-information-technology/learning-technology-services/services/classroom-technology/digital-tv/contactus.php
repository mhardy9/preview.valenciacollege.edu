<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/services/classroom-technology/digital-tv/contactus.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/services/classroom-technology/digital-tv/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/">Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/classroom-technology/">Classroom Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/services/classroom-technology/digital-tv/">Digital Tv</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h3>Digital TV - Contact Us </h3>
                        
                        
                        &nbsp;
                        <div>
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                <h2>Contact Us</h2>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>CJI Campus <i><font size="1">(Bright House)</font></i>
                                                            
                                                         </div>
                                                         
                                                         <div>Downtown Campus <i><font size="1">(Dish Network)</font></i>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <strong>Room 1-206<br>
                                                               Ed Livingston</strong><br>
                                                            <a href="mailto:elivingston1@valenciacollege.edu">elivingston1@valenciacollege.edu</a><br>
                                                            <strong>Ext: 8272</strong>
                                                            
                                                         </div>
                                                         
                                                         <div>
                                                            <strong>Rooms 106, 401<br>
                                                               Lorrisa Saldivar</strong><br>
                                                            <a href="mailto:lsaldivar@valenciacollege.edu">jsoto@valenciacollege.edu</a><strong><br>
                                                               <strong>Ext: 3444 </strong>
                                                               
                                                               </strong>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         <div>East Campus<i><font size="1"> (Bright House)</font></i>
                                                            
                                                         </div>
                                                         
                                                         <div>Osceola Campus<i><font size="1"> (Bright House)</font></i>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <strong>Room 4-149<br>
                                                               James Wells</strong><br>
                                                            <a href="mailto:jwells@valenciacollege.edu">jwells@valenciacollege.edu</a><br>
                                                            <strong>Ext: 2424</strong>
                                                            
                                                         </div>
                                                         
                                                         <div>
                                                            <strong>Room 1-105<br>
                                                               Jorge Soto</strong><br>
                                                            <a href="mailto:jsoto@valenciacollege.edu">jsoto@valenciacollege.edu</a><strong><br>
                                                               <strong>Ext: 4888 </strong>
                                                               </strong>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         <div>West Campus<i><font size="1"> (Direct TV)</font></i>
                                                            
                                                         </div>
                                                         
                                                         <div>Winter Park Campus<i><font size="1"> (Bright House)</font></i>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <strong>Room 3-101A<br>
                                                               Ken Carpenter</strong><br>
                                                            <a href="mailto:kcarpenter2@valenciacollege.edu">kcarpenter2@valenciacollege.edu</a><br>
                                                            <strong>Ext: 1170</strong>
                                                            <br><br>
                                                            
                                                            <strong>SSB-171A, SSB-171J, SSB-180<br>
                                                               JC Walker</strong><br>
                                                            <a href="mailto:jcwalker@valenciacollege.edu">jcwalker@valenciacollege.edu</a><br>
                                                            <strong>Ext: 1078</strong>
                                                            <br><br>
                                                            
                                                            <strong>Room 6-104<br>
                                                               John Watson</strong><br>
                                                            <a href="mailto:jwatson1@valenciacollege.edu">jwatson1@valenciacollege.edu</a><br>
                                                            <strong>Ext: 5464</strong>
                                                            <br><br>
                                                            
                                                            <strong>Room SSB-170<br>
                                                               Tom Lopez</strong><br>
                                                            <a href="mailto:tlopez@valenciacollege.edu">tlopez@valenciacollege.edu</a><br>
                                                            <strong>Ext: 1100</strong>
                                                            
                                                            
                                                         </div>
                                                         
                                                         <div>
                                                            <strong>Room 1-142<br>
                                                               Brian Bellissimo</strong><br>
                                                            <a href="mailto:bbellissimo@valenciacollege.edu">bbellissimo@valenciacollege.edu</a><strong><br>
                                                               <strong>Ext: 6819 </strong>
                                                               </strong>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>Service / Repair</div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <strong>Stellar Satellite Solutions<br>
                                                               Dan Harms (Project Manager)</strong><br>
                                                            <a href="http://www.stellarsatellite.com">www.stellarsatellite.com</a><br>
                                                            <a href="mailto:dan@stellarsatellite.com">dan@stellarsatellite.com</a><br>
                                                            <strong>(407) 529-6589</strong> (phone)<br><strong>(407) 277-5669</strong> (fax)
                                                            <br><br>
                                                            
                                                            
                                                         </div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                        </div>           
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/services/classroom-technology/digital-tv/contactus.pcf">©</a>
      </div>
   </body>
</html>