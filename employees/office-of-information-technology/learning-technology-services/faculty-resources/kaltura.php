<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/faculty-resources/kaltura.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/faculty-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/">Faculty Resources</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h3>Kaltura - Video Delivery Platform</h3>
                        
                        
                        <p>The College has recently been working to find a better video delivery solution. Kaltura
                           was choosen to replace the Adobe Media Server. 
                        </p>
                        
                        <p><strong>There are many new benefits to the Kaltura solution, here are just a few:</strong></p>
                        
                        <ul>
                           
                           <li>Integration with Blackboard, including My Media, Media Gallery, and Mashup tools.</li>
                           
                           <li>Self-service option for uploading videos for both faculty and students.</li>
                           
                           <li>Ability to display captions</li>
                           
                           <li>Ability to upload videos, edit videos, record webcam, or record screencast utilizing
                              a single solution.
                           </li>
                           
                           <li>Ability to view video analytics within Blackboard </li>
                           
                        </ul>
                        
                        <p>              <strong>Kaltura Implementation Timeline: </strong></p>
                        
                        <p><img alt="Kaltura Timeline" height="370" src="KalturaTimeline_007.jpg" width="659"></p>
                        
                        <p>Watch this video to learn more about Kaltura's integration into Blackboard</p>
                        
                        <p><iframe allowfullscreen="" frameborder="0" height="285" mozallowfullscreen="" src="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_dqf8tsjf&amp;flashvars%5BstreamerType%5D=auto&amp;flashvars%5BleadWithHTML5%5D=true&amp;flashvars%5BsideBarContainer.plugin%5D=true&amp;flashvars%5BsideBarContainer.position%5D=left&amp;flashvars%5BsideBarContainer.clickToClose%5D=true&amp;flashvars%5Bchapters.plugin%5D=true&amp;flashvars%5Bchapters.layout%5D=vertical&amp;flashvars%5Bchapters.thumbnailRotator%5D=false&amp;flashvars%5BstreamSelector.plugin%5D=true&amp;flashvars%5BEmbedPlayer.SpinnerTarget%5D=videoHolder&amp;flashvars%5BdualScreen.plugin%5D=true&amp;&amp;wid=1_q4tz3qf5" webkitallowfullscreen="" width="400"></iframe></p>
                        
                        <p><strong>Together, Blackboard and Kaltura help you engage more students by offering:</strong></p>
                        
                        <ul>
                           
                           <li>Seamless authoring, uploading, recording, searching and streaming of campus media
                              content, including lecture capture
                           </li>
                           
                           <li>Optimal playback experience on desktop and mobile devices (including iPhone, iPad,
                              and Android)
                           </li>
                           
                           <li>Rich media ingestion, transcoding, editing, content and metadata management, and analytics</li>
                           
                           <li>Advanced user access control and rights management features</li>
                           
                        </ul>            
                        
                        <blockquote>
                           
                           <p> <strong>Easy-to-use tools such as:</strong><br>
                              <br>
                              <a href="http://www.blackboard.com/resources/partnerships/img/featured-partners/kaltura/my-media.png"></a><a href="http://www.blackboard.com/resources/partnerships/img/featured-partners/kaltura/my-media.png"><strong>                </strong></a><strong>My Media Tool
                                 </strong> 
                           </p>
                           
                           <p>Provides each user with a personal media library where they can upload, edit and manage
                              content, as well as share content.
                           </p>
                           
                           <p><a href="http://www.blackboard.com/resources/partnerships/img/featured-partners/kaltura/my-media.png"><strong><img alt="Kaltura My Media Tool" height="190" src="kaltura-my-media_000.jpg" width="455"></strong></a></p>
                           
                           <p>                <strong><br>
                                 Media Gallery
                                 </strong>
                              
                           </p>
                           
                           <p>Allows members to search and view rich media assigned to a course.</p>
                           
                           <p><img alt="Kaltura Media Gallery inside a Blackboard Course" height="295" src="KalturaMediaGalleryValencia_000.jpg" width="457"><br>
                              <br>
                              <a href="http://www.blackboard.com/resources/partnerships/img/featured-partners/kaltura/my-media.png"></a><br>
                              <strong>Mashup Tool Integration</strong><br>
                              <br>
                              Allows users to easily embed and share videos anywhere within Blackboard Learn.
                           </p>
                           
                           <p><a href="http://www.blackboard.com/resources/partnerships/img/featured-partners/kaltura/my-media.png"><img alt="Kaltura Mashup Tool in Blackboard" height="286" src="KalturaMashupTool.jpg" width="450"></a><br>
                              <br>
                              
                           </p>
                           
                        </blockquote>            
                        
                        <p><strong>Tutorials:</strong></p>
                        
                        <ul>
                           
                           <li>What are Kaltura's Blackboard features: My Media, Media Gallery, and Mashup Tool<span> (<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_hmw81wkf&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="documents/WhatareKalturaBlackboardTools_000.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Mashup Tool for Kaltura in Blackboard <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_uh4ptjnd&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="documents/KalturaMediaMashupTool.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding a Video to Kaltura using My Media in Blackboard <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_5ptnalsn&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="documents/AddingaVideotoMyMediaUsingBlackboard.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding a Media Gallery to your Blackboard Course<span> (<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_ysbpf855&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="documents/AddingaMediaGallerytoyourBlackboardCourse.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding Captions to a Kaltura Video <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_l792oqb6&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="documents/AddingCaptionstoaKalturaVideoinBlackboard.pdf">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding a Co-Publisher to a Kaltura Video <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/30648821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_3h3lyasx&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="documents/AddingaCo-PublisherinKaltura.pdf" target="_blank">PDF)</a></span>
                              
                           </li>
                           
                           <li>Creating a Webcam Recording in Kaltura <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_lgmbmme8&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="documents/HowdoIaddawebcamvideotoBlackboard.pdf" target="_blank">PDF</a><a href="documents/AddingaCo-PublisherinKaltura.pdf" target="_blank">)</a></span>
                              
                           </li>
                           
                           <li>How to Download a Video You Own in Kaltura<span> (<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_3iyffxy4&amp;flashvars" target="_blank">Video</a>) </span>
                              
                           </li>
                           
                           <li> Student Instructions for adding media using Kaltura in Blackboard using the Mashup
                              Tool (<a href="blackboard/documents/StudentInstructionsKalturaMediaMashupToolinBlackboard.pdf">PDF</a>) 
                           </li>
                           
                           <li>Kaltura User Manual for Blackboard <span>(<a href="http://knowledge.kaltura.com/sites/default/files/Kaltura_Video_Building_Block_5_for_Blackboard_9_x_User_Manual.pdf" target="_blank">PDF</a>) </span>
                              
                           </li>
                           
                           <li><span>CaptureSpace Walkthrough (<a href="http://videos.kaltura.com/media/CaptureSpace+Walkthrough/1_x8mvlw8o/4061861">Video</a>) </span></li>
                           
                           <li>
                              <span><a href="https://videos.kaltura.com/category/Interactive+Video+Quiz/40511741">Interactive Video Quiz Videos</a></span><span><br>
                                 </span>
                              
                           </li>
                           
                           <li>If you would like to take advantage of Kaltura’s capabilities and currently have videos
                              in YouTube, follow these instructions to download your videos. <a href="https://support.google.com/youtube/answer/56100?hl=en">https://support.google.com/youtube/answer/56100?hl=en</a>
                              . Once you have the files on your computer, you may upload your videos to Kaltura
                              as shown in the tutorials above.
                           </li>
                           
                        </ul>  
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/faculty-resources/kaltura.pcf">©</a>
      </div>
   </body>
</html>