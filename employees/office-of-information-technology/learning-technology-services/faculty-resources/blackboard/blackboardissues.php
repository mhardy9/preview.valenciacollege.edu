<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboardissues.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/">Faculty Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/">Blackboard</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Blackboard Learn 9.1 Bugs </h3>
                        
                        <h3>Blackboard Learn - SP11 Issues </h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Case</div>
                                 </div>
                                 
                                 <div>
                                    <div>Description</div>
                                 </div>
                                 
                                 <div>
                                    <div>Permanent Fix</div>
                                 </div>
                                 
                                 <div>Target Release </div>
                                 
                                 <div>Temporary Fix </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1746408</div>
                                 
                                 <div>Order Columns for Students </div>
                                 
                                 <div>
                                    
                                    <p>Building Block Update </p>
                                    
                                    <p>Two  weeks to updated Building Block (July 15, 2013)</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Enhancement to Building Block. Issue resolved by PD. Finding out the release date
                                       and if it is compatible for SP11.<br>
                                       Option 1: Release learn Patch in addition to a Building Block compatible with SP11.<br>
                                       Option 2: Implement new feature in SP11 not as a Building Block, at least 2 weeks
                                       development time. (July 1, 2013) 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>NA</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1756979</div>
                                 
                                 <div>Issue with grading discussion on certain course themes </div>
                                 
                                 <div>This is bug LRN-66514. It is currently scheduled for SP15.</div>
                                 
                                 <div>Targeted to SP14. August 1st or 2nd week release. (July 1, 2013) </div>
                                 
                                 <div>
                                    
                                    <p>NA</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> 1779686 </div>
                                 
                                 <div> All faculty/students using Mac OS X are unable to copy/paste in Blackboard. </div>
                                 
                                 <div> This is a “known issue”. Requested a patch. </div>
                                 
                                 <div> Being developed with several weeks to be delivered as patch. (July 3, 2013) </div>
                                 
                                 <div>Use latest version of Safari</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1749321</div>
                                 
                                 <div>Cursor Issues with new Text Editor </div>
                                 
                                 <div>This is a “known issue”. Part of maintance in development. </div>
                                 
                                 <div>Part of PMO-1589 maintenance. Pending release. (July 3, 2013) </div>
                                 
                                 <div>NA</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1732285</div>
                                 
                                 <div>Certain tests can not be exported. </div>
                                 
                                 <div>This is a “known issue”. Requested a patch. </div>
                                 
                                 <div>
                                    
                                    <p>Targeted for CPatch 16 (July 22, 2013) </p>
                                    
                                    <p>Patch in QA (August 12, 2013 </p>
                                    
                                 </div>
                                 
                                 <div>NA</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1806083</div>
                                 
                                 <div>Events from the calendar created by due dates won't display in the My Calendar module
                                    on the Blackboard homepage or course Dashboard. 
                                 </div>
                                 
                                 <div>This is a "known issue". A patch is currently in development. </div>
                                 
                                 <div>Patch is in development - Several weeks but set release date. (August 16, 2013) </div>
                                 
                                 <div>Students can click the more calendar events link or add the calendar tool into the
                                    course menu. . 
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>             
                        
                        <p>CPatch = Cumulative  Patch<br>
                           SP = Service Pack 
                        </p>
                        
                        <p>For additional questions or support please contact us at <a href="mailto:onlinehelp@valenciacollege.edu">onlinehelp@valenciacollege.edu</a></p>
                        
                        <h3>Fixed </h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Case</div>
                                 
                                 <div>Description</div>
                                 
                                 <div>Fix Released </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1752360</div>
                                 
                                 <div>Missing Files During Course Copy </div>
                                 
                                 <div>CPatch 8 June 2013 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1749650</div>
                                 
                                 <div>No Text Wrapping in Internet Explorer </div>
                                 
                                 <div>CPatch 10 June 2013 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1749144</div>
                                 
                                 <div>Discussion grading doesn't show all posts. </div>
                                 
                                 <div>Fixed in Building Block Update June 2013 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1755510</div>
                                 
                                 <div>Grading Issues with Assignment that have an apostrophe in the file name</div>
                                 
                                 <div>Fixed in Building Block Update July 22, 2013 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1780384</div>
                                 
                                 <div>Students unable to see their grades. When clicking on My Grades, students receive
                                    the following error: "Not Found. The specified resource was not found or you do not
                                    have permission access it." 
                                 </div>
                                 
                                 <div>Fixed in CPatch 14 and Patch to Update Datebase on August 8, 2013. </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1756972</div>
                                 
                                 <div>Unable to grade assignment submissions with video everywhere</div>
                                 
                                 <div>Assignment Building Block update August 2013. </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1747035</div>
                                 
                                 <div>Text from reply does not appear. It is however visible when clicking EDIT for that
                                    post due to HTML formatting..&nbsp;
                                 </div>
                                 
                                 <div>Discussion Building Block update August 2013. </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1747194</div>
                                 
                                 <div>Discussions not allowing replies to student posts when using video everywhere tool</div>
                                 
                                 <div>Discussion Building Block update August 2013. </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1748303</div>
                                 
                                 <div>Generated URL has extra text</div>
                                 
                                 <div>CPatch 15 August 19, 2013. </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1754193</div>
                                 
                                 <div>Point value not showing up when grading by question </div>
                                 
                                 <div>CPatch 15 August 19, 2013. </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboardissues.pcf">©</a>
      </div>
   </body>
</html>