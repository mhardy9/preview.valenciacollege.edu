<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/">Faculty Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/">Blackboard</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h3>Learn It! - Blackboard Learn 9.1 </h3>
                        
                        <h3>Blackboard Learn - On Demand Learning Center</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.youtube.com/playlist?list=PLontYaReEU1sfvUGB7SgZb770TkBJtsGe">Understanding &amp; Building Your Course</a> (Building Content, Premissions, and Best Pratices) 
                           </li>
                           
                           <li>
                              <a href="http://www.youtube.com/playlist?list=PLontYaReEU1tXMa2qtvz6zY-QiD7gNUKJ">Communicating &amp; Collaborating</a> (Discussions, Blogs, Journal, WIkis, Groups, and Announcments)
                           </li>
                           
                           <li>
                              <a href="http://www.youtube.com/playlist?list=PLontYaReEU1shWKvIgLyoKl79H_5q3TOB">Assessing Learners</a> (Grade Center, Tests, Surveys, Pools, Assignments, Student Performance) 
                           </li>
                           
                           <li>
                              <a href="https://www.youtube.com/playlist?list=PLontYaReEU1u8Z-VKx45P-JkVSTUOUD13">Date, Reporting, and Reuse in Blackboard</a> (course data, run reports, retention center, item analysis) 
                           </li>
                           
                           <li><a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Instructor/160_Blackboard_Videos">All Instructor Videos</a></li>
                           
                           <li>
                              <a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Student/120_Blackboard_Videos">For Students</a> (Working in a Course, Navigating, Communicating, and Staying Organized) 
                           </li>
                           
                        </ul>
                        
                        <h3>Moving from Atlas Course Studio to Blackboard </h3>
                        
                        <ul>
                           
                           <li><a href="https://atlas.valenciacollege.edu/content/faculty/progress.cfm" target="_blank">Progress Report Email Templates</a></li>
                           
                           <li><a href="http://youtu.be/hDshTC7vnBY">Webinar Recording - Blackboard Basics for Atlas Course Studio Users </a></li>
                           
                           <li><a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Instructor/060_Communication/010_Email" target="_blank"> Email in Blackboard</a></li>
                           
                           <li><a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Instructor/060_Communication/030_Announcements" target="_blank">Announcements in Blackboard</a></li>
                           
                           <li><a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Instructor/060_Communication/060_Course_Messages" target="_blank">Course Messages in Blackboard</a></li>
                           
                           <li><a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Instructor/090_Course_Content/050_Course_Files/020_Add_Files_to_Course_Files" target="_blank">Add Files to Course Files in Blackboard</a></li>
                           
                           <li><a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Instructor/090_Course_Content/010_Create_Content" target="_blank">Creating Content in Blackboard</a></li>
                           
                           <li><a href="https://help.blackboard.com/en-us/Learn/9.1_SP_14/Instructor/040_In_Your_Course/030_File_Attachments/020_Best_Practice_Attach_Files" target="_blank">Best Practices Attaching Files in Blackboard </a></li>
                           
                        </ul>                  
                        
                        <h3>Get Help</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="https://help.edusupportcenter.com/shplite/ValenciaCollege/home">Technical Support</a> (407-582-5600) 
                           </li>
                           
                           <li><a href="../../../../faculty/development/centers/locations.html">Centers for Teaching/Learning Innovation </a></li>
                           
                           <li>Email: <a href="mailto:onlinehelp@valenciacollege.edu">onlinehelp@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li><a href="https://en-us.help.blackboard.com/Learn/9.1_2014_04" target="_blank">Blackboard Help Instructor Guide October 2014 Release </a></li>
                           
                           <li>
                              <strong>Supported Browsers</strong><br>
                              Most issues with Blackboard arise from browser incompatibly issues. Valencia recommends
                              an up to date and stable release of Mozilla Firefox.<br>
                              <a href="https://en-us.help.blackboard.com/Learn/9.1_2014_04/Student/015_Browser_Support/013_Browser_Support_for_October_2014">Blackboard's official browser support information</a>
                              <br>
                              <a href="https://en-us.help.blackboard.com/Learn/9.1_2014_04/Administrator/020_Browser_Support/Browser_Checker">Browser compatibility checker                    </a><br>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>LTS YouTube Videos </h3>
                        
                        <ul>
                           
                           <li><a href="http://youtu.be/6LwULETZXIE" target="_blank">Testing Setting Options in SP14</a></li>
                           
                           <li><a href="http://youtu.be/D9XvI-7nhdQ" target="_blank">Date Management in SP14</a></li>
                           
                           <li>
                              <a href="http://www.youtube.com/playlist?list=PLGl2sKbrcx1N1YLq3ErWkoRe4XXGslzie">Adding Library Links to Blackboard</a> - Playlist 
                           </li>
                           
                           <li><a href="http://youtu.be/QB_Y0cwp1mQ">ABC's of Blackboard - Getting Started</a></li>
                           
                           <li><a href="http://youtu.be/MBjRMKLcSi0">ABC's of Blackboard - Adding Content and Files</a></li>
                           
                           <li><a href="http://youtu.be/PHRjE4MyyV8">ABC's of Blackboard - Grade Center 101 </a></li>
                           
                           <li>
                              <a href="http://www.youtube.com/watch?v=nYC4gIMCd_4">How to migrate your course from WebCT CE8 to Blackboard Learn 9.1</a> 
                           </li>
                           
                           <li> <a href="http://www.youtube.com/watch?v=cbUG_VagzOM">Customizing Your Course Menu in Bb 9.1</a> 
                           </li>
                           
                           <li>UPDATED <a href="http://youtu.be/H50nqDm73EU">Blackboard Learn 9.1 Course Copy</a> 
                           </li>
                           
                           <li> <a href="http://www.youtube.com/watch?v=u9SX1vZJmMw">How To Copy Course Content in Bb 9.1</a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.youtube.com/watch?v=SiP0mqUCXzA">Bulk Delete Function in Blackboard Learn 9.1</a> 
                           </li>
                           
                           <li> <a href="http://www.youtube.com/watch?v=6TabefLTnHw">Setting up Respondus for Bb Learn</a> 
                           </li>
                           
                           <li> <a href="http://www.youtube.com/watch?v=6bs3tWlW5gM">Setting Up Respondus Lockdown Browser</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.youtube.com/watch?v=0qnAYHETX90">WebDav via Mac OS</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.youtube.com/watch?v=CzOJPegBCjY">How to Search for Publisher Content in Blackboard Learn 9.1</a> 
                           </li>
                           
                           <li> <a href="http://www.youtube.com/watch?v=MkDaWn-uvxE">Embedding Video in Blackboard</a> 
                           </li>
                           
                           <li> <a href="http://www.youtube.com/watch?v=b9mahtJqLVA">Mashups in Blackboard</a> 
                           </li>
                           
                           <li>Check out our YouTube Channel <a href="https://www.youtube.com/user/ValenciaLTAD">https://www.youtube.com/user/ValenciaLTAD</a> 
                           </li>
                           
                        </ul>
                        
                        <h3>Blackboard Blogs </h3>
                        
                        <ul>
                           
                           <li>Additonal Updates - Visit our LTS Blog<a href="http://wp.valenciacollege.edu/lts-blog/"> http://wp.valenciacollege.edu/lts-blog/</a> 
                           </li>
                           
                           <li>NEW! <a href="http://wp.valenciacollege.edu/lts-blog/2017/01/04/discussion-board-forum-access-denied-error/">Discussion Board Forum "Access Denied" Error </a>
                              
                           </li>
                           
                           <li>NEW! <a href="http://wp.valenciacollege.edu/lts-blog/2017/01/04/blackboard-gamegogy-leaderboard/">Blackboard Gamegogy Leaderboard </a>
                              
                           </li>
                           
                           <li>NEW! <a href="http://wp.valenciacollege.edu/lts-blog/2016/10/18/kalturas-newest-features-capturespace-and-interactive-online-video-quizzing/">Kaltura's Newest Features: CaptureSpace and Interactive Online Video Quizzing </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://wp.valenciacollege.edu/lts-blog/2016/05/02/blackboard-video-migration-deadline-may-2nd-2016/">Blackboard Video Migration Deadline May 2, 2017</a> 
                           </li>
                           
                           <li>
                              <a href="http://wp.valenciacollege.edu/lts-blog/2015/11/16/kaltura-valencias-new-video-delivery-platform/" target="_blank">Kaltura Video Delivery Platform in Blackboard</a> 
                           </li>
                           
                           <li>
                              <a href="http://wp.valenciacollege.edu/lts-blog/2015/10/09/photo-roster-now-available-in-blackboard/" target="_blank">Photo Roster</a> 
                           </li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2015/06/25/easily-add-smarthinking-to-your-course-menu/">Add Smarthinking to Course Menu</a></li>
                           
                           <li> <a href="http://wp.valenciacollege.edu/lts-blog/2015/01/09/new-course-copy-option-for-file-attachments-in-blackboard/" target="_blank">Course Copy Option for File Attachements </a>
                              
                           </li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2014/04/30/adding-sms-text-message-notifications-to-blackboard/" target="_blank">Adding SMS Text Message Notifications to Blackboard </a></li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2014/04/21/new-safeassign-originality-reports-coming-soon/" target="_blank">New SafeAssign Originality Reports </a></li>
                           
                           <li>
                              <a href="http://wp.valenciacollege.edu/lts-blog/2014/12/18/what-is-the-new-student-preview-mode-in-blackboard/" target="_blank">Student Preview Mode</a> (Formerly Adding a Test Student)
                           </li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2014/01/14/test-availability-exception-for-extended-time/" target="_blank">Test Availability Exceptions for Extended Time on Assessments (Tests) </a></li>
                           
                           <li>
                              <a href="http://wp.valenciacollege.edu/lts-blog/2014/01/14/insecure-content-or-mixed-content-http-doesnt-display-in-blackboards-secure-page-https/" target="_blank">Insecure Content or Mixed Content (http) Doesn't Display in Blackboard's Secure Page
                                 (https)</a> - Usually embedded content from Youtube, Screencast, or other sites. 
                           </li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2013/12/09/upcoming-blackboard-downtime-new-features-coming/" target="_blank">Features in Blackboard SP14 for Spring 2014 </a></li>
                           
                           <li>
                              <a href="http://wp.valenciacollege.edu/lts-blog/2013/04/29/blackboard-sp11-features-webinar-recording/">Blackboard SP11 Features Webinar Recording</a> 
                           </li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2013/04/10/blackboard-learn-9-1-maintenance-window-and-new-features-for-sp11/"> Blackboard Learn 9.1: Maintenance Window and New Features for SP11 </a></li>
                           
                           <li><a href="http://wp.me/p3rFO4-k6"> Library Links in Blackboard Made Simple </a></li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2012/08/21/updates-to-course-copy-for-blackboard-learn-9-1-sp9/">Updates to Course Copy for Blackboard Learn 9.1 SP9</a></li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2012/08/14/respondus-software-and-connection-update/">Respondus Software and Connection Update </a></li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2012/02/20/find-last-date-of-attendance-in-blackboard-based-on-a-graded-activity/">Finding Last Date of Attendance in Blackboard Based on a Graded Activity</a></li>
                           
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2011/08/11/message-notifications-just-got-a-lot-easier-in-blackboard/">My Messages</a></li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2011/06/09/add-a-facebook-widget-to-a-blackboard-course/">Add a Facebook Widget to your Course</a></li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2011/05/17/my-courses-settings-in-blackboard-hide-rearrange-courses/"> My Courses Settings - Hide &amp; Rearrange Courses</a></li>
                           
                           <li><a href="http://wp.valenciacollege.edu/lts-blog/2011/05/06/update-all-file-permissions-in-blackboard-learn/">Updated All File Premissions</a></li>
                           
                        </ul>
                        
                        <h3>Blackboard Training</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="http://learn.valenciacollege.edu/webapps/blackboard/execute/courseMain?course_id=_1036_1">Blackboard Essentials Self-Paced</a> - Atlas User Name &amp; Password Required
                           </li>
                           
                           <li><a href="../../../../faculty/development/programs/online/IntroductiontoOnlineTeaching.html">Blackboard Essentials LTAD 3282</a></li>
                           
                           <li><a href="../../../../faculty/development/programs/online/IntroductiontoOnlineTeaching.html">Selected Topics in Blackboard LTAD 3180-3186 </a></li>
                           
                           
                        </ul>
                        
                        <h3>Blackboard Mobile App Information:</h3>
                        
                        <ul>
                           
                           <li><a href="../../../../office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-mobile.html"> Access and features </a></li>
                           
                        </ul>
                        
                        <h3>Kaltura Media Delivery Platform </h3>
                        
                        <ul>
                           
                           <li>What are Kaltura's Blackboard features: My Media, Media Gallery, and Mashup Tool<span> (<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_hmw81wkf&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="../documents/WhatareKalturaBlackboardTools_000.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Mashup Tool for Kaltura in Blackboard <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_uh4ptjnd&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="../documents/KalturaMediaMashupTool.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding a Video to Kaltura using My Media in Blackboard <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_5ptnalsn&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="../documents/AddingaVideotoMyMediaUsingBlackboard.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding a Media Gallery to your Blackboard Course<span> (<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_ysbpf855&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="../documents/AddingaMediaGallerytoyourBlackboardCourse.pdf" target="_blank">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding Captions to a Kaltura Video <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_l792oqb6&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="../documents/AddingCaptionstoaKalturaVideoinBlackboard.pdf">PDF</a>)</span> 
                           </li>
                           
                           <li>Adding a Co-Publisher to a Kaltura Video <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/30648821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_3h3lyasx&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="../documents/AddingaCo-PublisherinKaltura.pdf" target="_blank">PDF)</a></span>
                              
                           </li>
                           
                           <li>Creating a Webcam Recording in Kaltura <span>(<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_lgmbmme8&amp;flashvars" target="_blank">Video</a>)</span> <span>(<a href="../documents/HowdoIaddawebcamvideotoBlackboard.pdf" target="_blank">PDF</a><a href="../documents/AddingaCo-PublisherinKaltura.pdf" target="_blank">)</a></span>
                              
                           </li>
                           
                           <li>How to Download a Video You Own in Kaltura<span> (<a href="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/28065821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player&amp;entry_id=1_3iyffxy4&amp;flashvars" target="_blank">Video</a>) </span>
                              
                           </li>
                           
                           <li>Student Instructions for adding media using Kaltura in Blackboard using the Mashup
                              Tool <span>(<a href="documents/StudentInstructionsKalturaMediaMashupToolinBlackboard.pdf">PDF</a>) </span>
                              
                           </li>
                           
                           <li>Kaltura User Manual for Blackboard <span>(<a href="http://knowledge.kaltura.com/sites/default/files/Kaltura_Video_Building_Block_5_for_Blackboard_9_x_User_Manual.pdf" target="_blank">PDF</a>) </span>
                              
                           </li>
                           
                           <li><span>CaptureSpace Walkthrough (<a href="http://videos.kaltura.com/media/CaptureSpace+Walkthrough/1_x8mvlw8o/4061861">Video</a>) </span></li>
                           
                           <li><span><a href="https://videos.kaltura.com/category/Interactive+Video+Quiz/40511741">Interactive Video Quiz Videos</a><br>
                                 </span></li>
                           
                           <li>If you would like to take advantage of Kaltura’s capabilities and currently have videos
                              in YouTube, follow these instructions to download your videos. <a href="https://support.google.com/youtube/answer/56100?hl=en">https://support.google.com/youtube/answer/56100?hl=en</a> . Once you have the files on your computer, you may upload your videos to Kaltura
                              as shown in the tutorials above.
                           </li>
                           
                        </ul>                  
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-resources.pcf">©</a>
      </div>
   </body>
</html>