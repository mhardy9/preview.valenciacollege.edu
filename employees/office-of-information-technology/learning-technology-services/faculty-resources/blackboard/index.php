<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/">Faculty Resources</a></li>
               <li>Blackboard</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h3>Blackboard 9.1 Learn at Valencia College</h3>
                        
                        <p> Welcome to the Blackboard Learn site for Valencia College. This page will keep you
                           updated about issues, updates, and information regarding Blackboard Learn. Please
                           visit this page regularly to see change, watch video tutorials, and download helpful
                           resources for this learning environment. 
                        </p>
                        
                        <h3>Be Prepared!</h3>
                        
                        <p>Valencia began our migration journey from Blackboard/WebCT CE 8 to Blackboard Learn
                           9.1 on June 1, 2010. In Blackboard Learn 9.1 you have see major changes to the user
                           interface, new useful features, and some improvements to existing features. 
                        </p>
                        
                        <p>We are currently on the <strong><em>Blackboard Learn 9.1 October 2014 Release </em></strong></p>
                        
                        <p>Plenty of resources available to assist you along the way. Visit <a href="../../../../faculty/development/centers/learn-it/BlackboardLearn.html">Blackboard Learn It! </a></p>
                        
                        <h3>Why Did We Upgrade?</h3>
                        
                        <p>Let's face it . . . change is hard! We all want our computer technologies to be able
                           to do more without having to change the way we work! We don't want to have to "re-learn"
                           how to do what we're already familiar with - because none of us has the time!
                        </p>
                        
                        <p>Fortunately (or unfortunately, depending on your point of view), the reality is that
                           technology is constantly changing very quickly, demanding that we keep up with it.
                           
                        </p>
                        
                        <ul>
                           
                           <li>
                              <strong>Improved accessibility/usability
                                 </strong>
                              
                              <ul>
                                 
                                 <li> Users with disabilities - particularly those with visual disabilities - have often
                                    encountered a number of accessibility barriers when using previous versions of Blackboard.
                                    Blackboard 9.1 goes a long way toward removing those barriers. 
                                 </li>
                                 
                                 <li> Blackboard has incorporated a range of enhancements that significantly improve its
                                    usability for everyone. <br>
                                    <br>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><strong>21st-century learners expect modern Web 2.0 technologies </strong></li>
                           
                           <ul>
                              
                              <li> Blackboard/WebCT was first used at Valencia in the early 2000's. Since then hardware,
                                 operating systems, browsers, programming languages and techniques, and especially
                                 the Web, have evolved and changed dramatically. 
                              </li>
                              
                              <li> The Web 1.0 programming used in our current version -- Blackboard/WebCT CE8 -- just
                                 does not play well with the newer Web 2.0 technologies. 
                              </li>
                              
                              <li> Students use these Web 2.0 technologies in other applications daily -- Blackboard
                                 9.1 has an intuitive, easy-to-use interface students will quickly be comfortable with!<br>
                                 <br>
                                 
                              </li>
                              
                           </ul>
                           
                           <li>
                              <strong>Powerful new features and improvements to existing features </strong> 
                           </li>
                           
                           <ul>
                              
                              <li>Even though some people use just the basic features in Blackboard, others need (and
                                 request) additional tools and more features that enhance their teaching with Blackboard
                                 and make course management easier. 
                              </li>
                              
                              <li> Blackboard 9.1 offers many of these additional features users have requested, as
                                 well as a more intuitive and effective user interface. 
                              </li>
                              
                           </ul>
                           
                           <li>
                              <strong> Major improvements in communication, information sharing, collaboration, usability,
                                 and security require more advanced web development techniques </strong>
                              
                              <ul>
                                 
                                 <li>                    Blackboard has been able to take advantage of new technologies
                                    in this upgrade to improve the teaching and learning experience for instructors and
                                    students alike. <br>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong> Continued support for the product </strong><br>
                              Like most software vendors, Blackboard will discontinue support for Blackboard 8 (our
                              current version) at some point in the near future. <br>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>How Can We Prepare?</h3>
                        
                        <p>To learn more about Blackboard Learn 9.1, navigate through the main areas listed on
                           the left hand side (menu) or check out our blog for updates. You can also connect
                           with us through any of our social media channels shown below!
                        </p>
                        
                        <p> <a href="https://www.facebook.com/LTSValencia"><img alt="Find us on Facebook" border="0" height="32" src="icon_facebook_32.png" width="32"></a> <a href="https://twitter.com/LTSValencia"> <img alt="Follow us on Twitter" border="0" height="32" src="icon_twitter_32.png" width="32"></a> <a href="http://wp.valenciacollege.edu/lts-blog/"> <img alt="Read our blog on Wordpress" border="0" height="32" src="icon_wordpress_32.png" width="32"></a> <a href="http://www.youtube.com/user/ValenciaLTAD"> <img alt="LTS YouTube Channel" border="0" height="24" src="icon_youtube_24_dark.png" width="66"></a> 
                        </p>
                        
                        <p>Plenty of resources available to assist you along the way. Visit <a href="../../../../faculty/development/centers/learn-it/BlackboardLearn.html">Blackboard Learn It! </a></p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/index.pcf">©</a>
      </div>
   </body>
</html>