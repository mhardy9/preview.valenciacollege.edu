<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-mobile.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/">Faculty Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/">Blackboard</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h3>Blackboard Mobile Learn </h3>
                        
                        <p> Blackboard app is now available for ALL iOS and Android users! You  can use your
                           iPhone®, iPad®, iPod touch®, and Android® devices on any carrier or network to access
                           and update announcements, discussions, content, and much more with the Blackboard
                           Mobile Learn app.&nbsp;&nbsp;                                  
                           
                        </p>
                        
                        <p>Blackboard Mobile Learn is now free to all users at Valencia College. </p>
                        
                        <p>DOWNLOAD INSTRUCTIONS</p>
                        
                        <p>                              1. Visit the:</p>
                        
                        <ul>
                           
                           <li>App Store on your iPhone®, iPad®, or iPod touch®</li>
                           
                           <li>Visit the Google Play Store (formerly Android Marketplace) on your Android®</li>
                           
                        </ul>                              
                        
                        <p>                                2. Search for “Blackboard Mobile Learn”<br>
                           3. Install the app<br>
                           4. Search for your school<br>
                           5. Login with your personal Blackboard credentials<br>
                           
                        </p>
                        
                        <p>Below you can find  the app by clicking on the App Store or Google Play store buttons
                           or by scanning the QR codes:
                        </p>                              
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div> <a href="https://play.google.com/store/apps/details?id=com.blackboard.android&amp;feature=nav_result#%3Ft=W251bGwsMSwxLDMsImNvbS5ibGFja2JvYXJkLmFuZHJvaWQiearn/id376413870%3Fmt=8%23?t=W251bGwsMSwxLDMsImNvbS5ibGFja2JvYXJkLmFuZHJvaWQiearn/id376413870?mt=8" target="_blank"><img alt="Google Play Store" border="0" height="56" src="GooglePlay160x600.png" width="160"></a>
                                    
                                 </div>
                                 
                                 <div>Supported Devices</div>
                                 
                                 <div><a href="http://itunes.apple.com/us/app/blackboard-mobile-learn/id376413870?mt=8" target="_blank"><img alt="Apple App Store" border="0" height="56" src="iPhone-store.png" width="160"></a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Google Play Blackboard Mobile App Download" height="150" hspace="3" src="AndroidQRCode_000.png" vspace="3" width="150"></div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p>Android</p>
                                       
                                       <p>iPhone<br>
                                          iPod Touch<br>
                                          iPad
                                       </p>
                                       
                                       <p><img alt="Blackboard Mobile App" height="85" src="Mobile_Learn_512_000.jpg" width="85"></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div><img alt="Apple App Store Blackboard Mobile App Download" height="150" hspace="3" src="iOSQRCode.png" vspace="3" width="150"></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                              
                        
                        
                        <h3>Features</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><img alt="Tests" height="61" src="icon_test.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Mobile Tests</strong></p>
                                    
                                    <p>Your students are able to take tests and exams delivered through Blackboard Learn
                                       on their Android and iOS devices. Any test you created that includes compatible question
                                       types (Multiple Choice, Hot Spot, Fill in the Blank, etc) can be taken from Blackboard
                                       Mobile Learn<strong><br>
                                          </strong></p>
                                    
                                 </div>
                                 
                                 <div><img alt="Content" height="61" src="icon_media.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Content</strong></p>
                                    
                                    <p>Your students can access content you uploaded and interact with it on their device,
                                       using Blackboard Mobile Learn or any other application that supports those documents.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Push Notifications" height="61" src="icon_push.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Push Notifications</strong></p>
                                    
                                    <p>Your students can now elect to receive automatic, personalized notifications delivered
                                       straight to their mobile devices to help them stay informed. 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>New announcements</li>
                                       
                                       <li> New graded items</li>
                                       
                                       <li> Test being posted</li>
                                       
                                       <li>Other course activities</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div><img alt="Blog" height="61" src="icon_blog.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Blogs</strong></p>
                                    
                                    <p>Classmates can read blog posts and interact with each other by posting comments and
                                       uploading media as attachments on blogs (Android and iOS) as well as uploading non-media
                                       files (Android).
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Announcements" height="61" src="icon_announcements.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Announcements</strong></p>
                                    
                                    <p>You can post<br>
                                       announcements on-the-go
                                    </p>
                                    
                                 </div>
                                 
                                 <div><img alt="Journals" height="61" src="icon_journal.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Journals</strong></p>
                                    
                                    <p>Your students can reflect on their course Journals through Blackboard Mobile Learn,
                                       as well as comment on peer Journals. You can use this tool to comment on student journals.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Grades" height="61" src="icon_grades.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Grades</strong></p>
                                    
                                    <p>Your students can find out how they did on their last midterm or homework assignment
                                       by using the Grades tool. They'll receive a Push Notification as soon as an item in
                                       their course has been graded. 
                                    </p>
                                    
                                 </div>
                                 
                                 <div><img alt="Roster" height="61" src="icon_roster.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Roster</strong></p>
                                    
                                    <p>With the class roster,your students can quickly view their entire class list, making
                                       organizing study groups a whole lot easier.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="Discussions" height="61" src="icon_discussions.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Discussions</strong></p>
                                    
                                    <p>You and your students can easily read-up and contribute to Discussions from Blackboard
                                       Mobile Learn. Students can even upload media from their mobile device as part of a
                                       Discussion attachment
                                    </p>
                                    
                                 </div>
                                 
                                 <div><img alt="Tasks" height="61" src="icon_tasks.jpg" width="61"></div>
                                 
                                 <div>
                                    
                                    <p><strong>Tasks</strong></p>
                                    
                                    <p>You can help students track and manage the progress of various tasks, from turning
                                       in homework assignments to midterm reminders to purchasing textbooks. The tasks tool
                                       allow students to mark when they've started a task and when it's complete.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                              
                        
                        <h3>Blackboard Mobile Learn Resources </h3>
                        
                        <p>If  you’re looking to really engage your students with mobile learning, check out
                           Blackboard’s<br> 
                           <a href="http://www.blackboard.com/getdoc/cffda5e3-d0cc-443e-931f-a5ec2c80b14b/New-Best-Practices-for-Mobile-Friendly-Courses.aspx">Best Practices for Mobile-Friendly Courses</a>.
                        </p>
                        
                        <p>Demo video tutorials</p>
                        
                        <p><a href="https://www.blackboard.com/Platforms/Mobile/Resources/Blackboard-Mobile-Learn-Demos.aspx">Blackboard Official Mobile Learn Device demos</a>  
                        </p>
                        
                        <p><a href="http://www.youtube.com/watch?v=NZuMOdHad2Y" target="_blank"> Youtube Blackboard Mobile learn for the iPad  </a></p>
                        
                        <p><a href="http://youtu.be/OfTKhX8VSj8" target="_blank">Youtube Blackboard Mobile Learn for Android</a></p>
                        
                        <h3>Get Help</h3>
                        
                        <p><a href="http://help.blackboardmobile.com/" target="_blank">Blackboard Mobile Learn Help</a> 
                        </p>                              
                        
                        <h3>Important Notice </h3>
                        
                        <p><em>The use and purchase of this application is optional and not required. It offers another
                              way to connect to Valencia's Blackboard server. You may still access Blackboard via
                              your device's mobile browser by going to <a href="https://learn.valenciacollege.edu">https://learn.valenciacollege.edu</a> (functionality will depend on your browser's technical limitations.)</em></p>                              
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-mobile.pcf">©</a>
      </div>
   </body>
</html>