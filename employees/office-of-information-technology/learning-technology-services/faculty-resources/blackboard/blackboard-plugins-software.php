<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-plugins-software.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/">Learning Technology Services</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/">Faculty Resources</a></li>
               <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/">Blackboard</a></li>
               <li>Learning Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h3>Plugins</h3>
                        
                        <h2>Blackboard Browser Check - Did you pass? <a href="../../../../office-of-information-technology/learning-technology-services/student-resources/getting-started-online/browser-requirements.html">CHECK HERE</a> 
                        </h2>
                        
                        <h2>&nbsp;</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Software Plug-Ins </strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Some online courses use specific applications and/or plug-ins. You may need one or
                                       more of the plug-ins below. Internet Explorer will generally automatically download
                                       any plug-in required by a particular page.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <h3>Browsers</h3>
                                 </div>
                                 
                                 <div>
                                    <h3><strong>Download</strong></h3>
                                 </div>
                                 
                                 <div>
                                    <h3><strong>Resource Links </strong></h3>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Internet Explorer </strong></div>
                                 
                                 <div>
                                    <div><a href="http://www.microsoft.com/downloads/details.aspx?FamilyID=9ae91ebe-3385-447c-8a30-081805b2f90b&amp;DisplayLang=en">Download</a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="http://www.microsoft.com/enable/products/">Accessibility</a> 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Firefox</strong></div>
                                 
                                 <div>
                                    <div><a href="http://www.mozilla.com/en-US/firefox/">Download</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Safari</strong></div>
                                 
                                 <div>
                                    <div><a href="http://www.apple.com/safari/download/">Download</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Browser Plug-ins</strong>                              
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Windows Media Player .</strong> Some online courses may stream video in Windows Format.
                                 </div>
                                 
                                 <div>&nbsp;
                                    
                                    <div><a href="http://www.microsoft.com/downloads/details.aspx?FamilyID=1d224714-e238-4e45-8668-5166114010ca&amp;displaylang=en">Download</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><a href="http://www.microsoft.com/enable/products/">Accessibility</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Windows Media Player Components for Quicktime </strong>Some online courses may stream video in Windows Format this will let you play those
                                    files directly in Quicktime Player
                                 </div>
                                 
                                 <div><a href="http://www.microsoft.com/downloads/en/details.aspx?FamilyId=915D874D-D747-4180-A400-5F06B1B5E559&amp;displaylang=en" target="_blank">Download</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>RealPlayer. </strong>Some courses use streaming video. 
                                       To view the video within a web page, you should 
                                       download RealOne plug-in, which is free.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://forms.real.com/netzip/getrde5_new_look.html?h=207.188.7.150&amp;f=windows/RealOnePlayerGold.exe&amp;p=RealOne+Player&amp;j2re=false&amp;tagtype=ie&amp;type=dl">Download </a></p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>QuickTime. </strong>Some courses use Quicktime video to deliver multimedia content. <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><a href="http://www.apple.com/itunes/download/">Download iTunes with Quicktime </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Shockwave</strong>. Shockwave is used in some online courses 
                                       for interactive quizzes, animations, and activities.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><a href="http://www.macromedia.com/shockwave/download/download.cgi" target="new">Download </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Flash. </strong>Flash is used in some online courses 
                                       for interactive quizzes, animations, and activities.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><a href="http://get.adobe.com/flashplayer/" target="new">Download</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Acrobat Reader.</strong>
                                       &nbsp; Adobe PDF documents are used in some 
                                       online courses and websites at Valencia College. 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><a href="http://www.adobe.com/products/acrobat/readstep.html" target="new"> Download</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Microsoft Word Viewer</strong><br>
                                       View, navigate, and print Microsoft Word 
                                       Documents.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><a href="http://www.microsoft.com/downloads/details.aspx?FamilyID=3657CE88-7CFA-457A-9AEC-F4F827F20CAC&amp;DisplayLang=en">Download </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Software Commonly Used in Online Courses</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Microsoft Office Professional 2007</strong> (Word, PowerPoint, Excel, Access) 
                                 </div>
                                 
                                 <div>
                                    <div><a href="http://us1.trymicrosoftoffice.com/product.aspx?sku=3203819&amp;culture=en-US">Download Trial </a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><a href="http://office.microsoft.com/en-us/training/default.aspx?ofcresset=1">Learn It</a></p>
                                       
                                       <p><a href="http://www.microsoft.com/enable/products/">Accessibility</a> 
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Open Office</strong> (Free Office Productivity Suite) 
                                 </div>
                                 
                                 <div>
                                    <div><a href="http://www.openoffice.org/">Download</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>         
                        
                        <p>Sometimes, you may need additional software to view and&nbsp;listen to&nbsp;instructional content
                           within your course. For instance, your instructor may upload an audio file for you
                           to listen to, or a video clip for you to watch. The browser you currently use to access
                           Blackboard may not always have the required software, or "plug-in", to properly play
                           such audio and video files.
                        </p>
                        
                        <p>This list contains the most commonly used&nbsp;plug-ins, and they are free to download.&nbsp;Click
                           the links to access the plug-ins. Before downloading any of the plug-ins provided,&nbsp;please
                           check with your instructor&nbsp;or your&nbsp;institution's technology coordinator&nbsp;to see which&nbsp;plug-ins
                           they may recommend.
                        </p>
                        
                        <p><a href="http://www.adobe.com/products/acrobat/readstep2.html"> Adobe Acrobat Reader</a>&nbsp;allows you to view and print PDF (Portable Document Format) files.Click on the link
                           to begin the download and install process. A new browser window will open. Be sure
                           to uncheck any unwanted tool bars or other add-ons before you download the plug-in.
                        </p>
                        
                        <p><a href="http://www.adobe.com/downloads/">Adobe Shockwave Player</a> for Windows or Mac OS allows you to view dynamic and animated content on the Internet
                           through your browser. Click on the link to begin the download and install process.
                           A new browser window will open.
                        </p>
                        
                        <p><a href="http://get.adobe.com/flashplayer/">Adobe Flash Player</a> for <strong>Windows</strong> or <strong>Mac OS</strong> allows you to view video or interactive content through your browser.&nbsp; Click on the
                           link to begin the download and install process. A new browser window will open
                        </p>
                        
                        <p><a href="http://www.apple.com/quicktime/download">Apple QuickTime Player</a> for Windows allows you to play audio and video files online through your browser.&nbsp;Click
                           the link to begin the download and install process.&nbsp;A new browser window will open.
                           Be sure to select the player you want. The default selection includes iTunes. Make
                           sure to uncheck any unwanted email sign-up lists.
                        </p>
                        
                        <p><a href="http://www.microsoft.com/downloads/en/default.aspx">Microsoft Windows Media Player</a> for Windows allows you to play audio and video files online through your browser.&nbsp;
                           Click the link to begin the download and install process.&nbsp; A new browser window will
                           open.
                        </p>
                        
                        <p><a href="http://www.real.com/">Real Networks RealPlayer</a> for <strong>Windows</strong> allows you to play audio and video files online through your browser. Click the link
                           to begin the download and install process. A new browser window will open.
                        </p>
                        
                        <p><a href="http://www.real.com/mac/?pageid=broadBandHomePage">Real Networks RealPlayer</a> for Mac OS allows you to play audio and video files online through your browser.&nbsp;Click
                           the link to begin the download and install process.&nbsp;A new browser window will open.
                        </p>
                        
                        <p><a href="http://www.java.com/en/download/manual.jsp">Sun Java Runtime Environment</a> (all platforms) is required for the use of Blackboard's Web&nbsp;Equation&nbsp;Editor as well
                           as the Virtual Classroom&nbsp;and Text Chat. Click the link to choose the appropriate plug-in
                           for your computer and to begin the download and install process. A new browser window
                           will open.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p>Connect</p>
                        
                        <p>                            <a href="https://www.facebook.com/LTSValencia" target="_blank"><img alt="Find us on Facebook" border="0" height="32" hspace="5" src="icon_facebook_32.png" vspace="3" width="32"></a> 
                           <a href="https://twitter.com/LTSValencia" target="_blank"><img alt="Follow us on Twitter" border="0" height="32" hspace="5" src="icon_twitter_32.png" vspace="3" width="32"></a> 
                           
                           <a href="http://wp.valenciacollege.edu/lts-blog" target="_blank"><img alt="Read our blog on Wordpress" border="0" height="32" hspace="5" src="icon_wordpress_32.png" vspace="3" width="32"></a>                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </p>
                        
                        <p>Featured Video</p>
                        
                        <p>How To Add An Avatar</p>
                        
                        <iframe allowfullscreen="" frameborder="0" height="151" src="http://www.youtube.com/embed/MVLAYGowf9A?rel=0" width="240"></iframe>
                        
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLTSValencia&amp;send=false&amp;layout=standard&amp;width=240&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=242649042425164"></iframe>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/learning-technology-services/faculty-resources/blackboard/blackboard-plugins-software.pcf">©</a>
      </div>
   </body>
</html>