<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Staff | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Staff</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Staff</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		  
               <h2> OIT Staff </h2>
               			
               
               
               
               
               
               
               
               <table class="table ">
                  
                  <tr>
                     <td style="padding-left:15px;">
                        
                        
                        	
                        <li><a href="#OIT">OIT Management and Office Support</a></li>
                        
                        	
                        <li><a href="#NET">Network &amp; Infrastructure Services</a></li>
                        
                        	
                        <li><a href="#APS">Enterprise Application Services</a></li>
                        
                        	
                        <li><a href="#DWD">Enterprise Application Services - Data Warehouse Development</a></li>
                        
                        	
                        <li><a href="#IIT">Web &amp; Portal Services</a></li>
                        
                        	
                        <li><a href="#ITC">Learning Technology &amp;  Project Management Services</a></li>
                        
                        	
                        <li><a href="#TSE">Campus Technology Services - East &amp; Winter Park</a></li>
                        
                        	
                        <li><a href="#TSO">Campus Technology Services - Osceola &amp; Lake Nona</a></li>
                        
                        	
                        <li><a href="#TSW">Campus Technology Services - West &amp; District Center</a></li>
                        
                        
                        <br><br>
                        
                        
                        
                     </td>
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="OIT"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">OIT Management and Office Support</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=297">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=297">John Knights</a>, Dir, Information Security<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=288">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=288">John Slot</a>, VP, Info Technology and CIO<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=295">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=295">Emily Blinn</a>, Executive Assistant, Sr<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="NET"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Network &amp; Infrastructure Services</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=292">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-3285063697720662375.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=292">Ken Rivera</a>, Dir, Network &amp; Infrastructure<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=210">
                           	<img src="/CFFileServlet/_cf_image/_cfimg6298165531992575934.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=210">Michael OLoughlin</a>, Network Administrator, Sr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=127">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-2057979986878032157.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=127">Ed Delgado</a>, Systems Administrator<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=185">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=185">Marc Moseley</a>, Systems Administrator<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=7">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-4247449636879181520.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=7">Kevin Matis</a>, Server Administrator, Sr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=282">
                           	<img src="/CFFileServlet/_cf_image/_cfimg4117372602644322686.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=282">Dennis Roehrich</a>, Server Administrator, Sr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=283">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-2324298474826897360.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=283">Rickardo Stupart</a>, Server Administrator<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=284">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=284">Earl Hoffman</a>, Network Administrator, Sr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=10">
                           	<img src="/CFFileServlet/_cf_image/_cfimg8468996158455730314.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=10">Josh Focht</a>, Network Administrator<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="APS"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Enterprise Application Services</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=161">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-8829799222290776413.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=161">Seshagiri Parupalli</a>, IT Database Administrator, Sr.<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=118">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=118">Lee White</a>, IT Mgr, Enterprise App Svcs<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=213">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=213">Dwayne Smoot</a>, Programmer Analyst<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=293">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-7526206909411022488.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=293">Liz Gangemi</a>, Functional IS Support Spec<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=286">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=286">Bernstein Stephen</a>, Functional IS Support Spec<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=248">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-8040275133808771513.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=248">Lymari Maldonado</a>, Programmer Analyst, Sr.<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=253">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-2371187501388535684.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=253">Latife Erdem</a>, State Reporting Analyst/Prgrmr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=266">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=266">Edwin Cruz</a>, Programmer Analyst<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=268">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-3446131380590074856.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=268">Irving Ortega Rodriguez</a>, IT Mgr, Enterprise App Svcs<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=274">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-4085537856550015692.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=274">Carlos Pena</a>, Programmer Analyst, Sr.<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=171">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-8853782621256198667.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=171">Andy Mobley</a>, Programmer Analyst, Sr.<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=126">
                           	<img src="/CFFileServlet/_cf_image/_cfimg4259376246423695945.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=126">Nathan Nguyen</a>, Programmer Analyst<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=195">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-4932965642276034941.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=195">John Reilly</a>, Programmer Analyst<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=133">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-7194531102913868219.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=133">Steve Sheikh</a>, Programmer Analyst, Sr.<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=193">
                           	<img src="/CFFileServlet/_cf_image/_cfimg8202838015838565201.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=193">Troy Timberman</a>, IT Database Administrator<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="DWD"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Enterprise Application Services - Data Warehouse Development</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=156">
                           	<img src="/CFFileServlet/_cf_image/_cfimg2011496164002035780.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=156">Juan Olvera</a>, IT Mgr, Decision Support Svcs<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=254">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-7356576254402674371.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=254">Peter Wen</a>, DS Architect/Developer, Sr.<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=298">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-6696319041018129154.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=298">Vidal Baez Fortunato</a>, DS Architect/Developer<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="IIT"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Web &amp; Portal Services</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=281">
                           	<img src="/CFFileServlet/_cf_image/_cfimg2438207126237461269.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=281">James Flanagan</a>, Dir, Web and Portal Services<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=50">
                           	<img src="/CFFileServlet/_cf_image/_cfimg5268503981550604292.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=50">Jeff Danser</a>, Web/Portal Developer<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=164">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-7122747543107012992.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=164">Melanie Hardy</a>, Web/Portal Developer<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=39">
                           	<img src="/CFFileServlet/_cf_image/_cfimg8599690573278315849.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=39">Doug Johnson</a>, Systems Administrator<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=278">
                           	<img src="/CFFileServlet/_cf_image/_cfimg8035029626712417055.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=278">James Jordan</a>, Web/Portal Developer<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=264">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-2611174630138937629.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=264">Scott Hood</a>, Web/Portal Developer<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=285">
                           	<img src="/CFFileServlet/_cf_image/_cfimg8941169555231934447.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=285">Jeffrey Reed</a>, Web/Portal Developer<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="ITC"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Learning Technology &amp;  Project Management Services</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=160">
                           	<img src="/CFFileServlet/_cf_image/_cfimg8897627534336445203.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=160">Todd Treece</a>, Dir, Learn Tech/Proj Mgmt Svcs<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=124">
                           	<img src="/CFFileServlet/_cf_image/_cfimg931345351237628656.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=124">David Renteria</a>, Dir, Enterprise App Services<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=280">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-8792461978390024574.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=280">Jessica Bourne</a>, Instructional Design/Developer<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=196">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-8468000001520926965.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=196">Josh Murdock</a>, IT Mgr, Inst Design Svcs<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=167">
                           	<img src="/CFFileServlet/_cf_image/_cfimg2058597583672833741.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=167">Michael Maguire</a>, Ops Mgr, Video Productions<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=289">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-8018287782797896656.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=289">Nicholas Croft</a>, IT Mgr, Classroom Tech Svcs<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=275">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=275">Scott Smith</a>, Producer, Multimedia<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="TSE"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Campus Technology Services - East &amp; Winter Park</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=211">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-9185389783918202557.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=211">Jamie Rost</a>, Asst VP, Application<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=249">
                           	<img src="/CFFileServlet/_cf_image/_cfimg4888493971949569470.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=249">Keith Hill</a>, Dir, Campus Technology Svcs E<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=250">
                           	<img src="/CFFileServlet/_cf_image/_cfimg601381258161490413.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=250">Mortiman Moodie</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=277">
                           	<img src="/CFFileServlet/_cf_image/_cfimg9072517654112269096.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=277">Steve Rukstalis</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=247">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-5906381245002475826.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=247">Fernando Gutierrez</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=215">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-9217706512153489708.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=215">Steve Suarez</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=216">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-1785458245404355733.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=216">Joshua Chapkin</a>, Technical Support Specialist<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="TSO"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Campus Technology Services - Osceola &amp; Lake Nona</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=237">
                           	<img src="/CFFileServlet/_cf_image/_cfimg7528672184312033094.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=237">Daniel Charriez</a>, Dir, Campus Technology Svcs O<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=221">
                           	<img src="/CFFileServlet/_cf_image/_cfimg62517939406033037.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=221">Jorge Soto</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=222">
                           	<img src="/CFFileServlet/_cf_image/_cfimg7549942183766281853.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=222">Rooney LaPlante</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=245">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=245">Andrew Salaz</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=252">
                           	<img src="/CFFileServlet/_cf_image/_cfimg4390107152150715460.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=252">Mohamed Aleem</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=258">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-1432907243535443043.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=258">Jose Diaz</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=259">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=259">Ron Ginn</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=279">
                           	<img src="/CFFileServlet/_cf_image/_cfimg1577234777163408943.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=279">Noelia Vasquez</a>, Technical Support Specialist<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
                  <tr> 
                     	
                     <td><a name="TSW"></a></td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td>
                        <b><font size="+1">Campus Technology Services - West &amp; District Center</font></b> 
                        	
                        <br><br>
                        
                        
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=228">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-3694853150181883187.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=228">Carmine La Pietra</a>, Dir, Campus Technology Svcs W<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=229">
                           	<img src="/CFFileServlet/_cf_image/_cfimg835163097824764923.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=229">Mehdi Khoumassi</a>, IT Mgr, Campus Tech Svcs Ptnr<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=224">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-1931106978332946590.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=224">Vinh Nguyen</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=225">
                           	<img src="/CFFileServlet/_cf_image/_cfimg4865546938406320680.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=225">Sean Coamey</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=226">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-7003628273887015875.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=226">Edwin Lopez</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=227">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-5303429762051707629.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=227">Lorri Saldivar</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=272">
                           	<img src="/CFFileServlet/_cf_image/_cfimg4613096505919964784.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=272">Orin Monrose</a>, IT Mgr, Business &amp; Support Ops<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=251">
                           	<img src="/CFFileServlet/_cf_image/_cfimg1291930369838063152.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=251">David Lazarus</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=230">
                           	<img src="/CFFileServlet/_cf_image/_cfimg4938045042198140455.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=230">John Watson</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=231">
                           	<img src="/CFFileServlet/_cf_image/_cfimg7647474049251498390.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=231">Wes Sondermann</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=260">
                           	<img src="/CFFileServlet/_cf_image/_cfimg-4835592446519356987.PNG" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=260">Shevon Antoine</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=290">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=290">Mike Otero</a>, Tech Support Spec Working Supv<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=287">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=287"></a>, <br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=296">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=296">Rob Aponte</a>, Technical Support Specialist<br>
                        	
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=294">
                           	<img src="/oit/about/images/default_profile.jpg" border="0" alt="Employee Photo" width="30" height="30" align="absmiddle">
                           </a>&amp;nbsp;
                        
                        <a href="/oit/about/staffDetail.cfm?EmployeeID=294">Aaron Ramlochan</a>, Technical Support Specialist<br>
                        	 
                        	
                     </td>
                     
                  </tr>
                  
                  <tr> 
                     	
                     <td> 
                        
                        
                        <p>&amp;nbsp;</p>
                        	
                     </td>
                     
                  </tr>
                  
                  
               </table>
               			
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/staff.pcf">©</a>
      </div>
   </body>
</html>