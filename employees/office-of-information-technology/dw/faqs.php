<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Data Warehouse | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/dw/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/dw/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Data Warehouse</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/dw/">Dw</a></li>
               <li>Data Warehouse</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Data Warehouse 
                           
                           
                           Frequently Asked Questions (FAQs)
                        </h2>
                        
                        <h3>Switcher Utility: </h3>
                        
                        
                        
                        Download and unzip the script below.  Then double-click to run.  
                        
                        <ul>
                           
                           <li> <a href="http://valenciacollege.edu/office-of-information-technology/dw/documents/switch4_3.zip">ZIP File with switch4_3.bat</a> (New add-in version) 
                           </li>
                           
                        </ul>
                        
                        
                        <ul>
                           
                           <li> <a href="http://valenciacollege.edu/office-of-information-technology/dw/documents/switch2_1.zip">ZIP File with switch2_1.bat</a> (Old add-in version) 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>Microsoft Office Add-in: </h3>
                        
                        <ul>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#01">How do I get the Microsoft Add-ins installed?</a></li>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#a02">How do I change the output format of a report to CSV or HTML?</a></li>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#a03">Are "patches" available for the Michrosoft Add-in software?</a></li>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#a04">The "SAS" tab in Excel disappeared, how do I get it back?</a></li>
                           
                        </ul>
                        
                        <h3>Training:</h3>
                        
                        <ul>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#b01">I attended a training class and want to know where can I get a copy of the material?
                                 </a></li>
                           
                           <li>
                              <a href="../../office-of-information-technology/dw/FAQs.html#b02">Some coworkers are being trained on how to access the Data Warehouse, how can I receive
                                 training?</a> 
                           </li>
                           
                        </ul>
                        
                        <h3>Access:</h3>
                        
                        <ul>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#c01">I need access to the Data Warehouse to perform my job, how do get access? </a></li>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#c02">Why am I getting a message that my logon is incorrect when I enter my Atlas ID and
                                 password to access the Data Warehouse?</a></li>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#c03">Why can't I run reports in Excel after changing my password in Atlas?</a></li>
                           
                           <li>
                              <a href="../../office-of-information-technology/dw/FAQs.html#c04">Why do I get an error message when I try to access the Data Warehouse Portal from
                                 outside of Valencia?</a> 
                           </li>
                           
                        </ul>
                        
                        <p><span>Other:</span></p>
                        
                        <ul>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#d01">The question I have isn't addressed in the FAQ, what can I do?</a></li>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#d02">What are reports DQ0001 and DQ0002 telling me? </a></li>
                           
                           <li><a href="../../office-of-information-technology/dw/FAQs.html#last">How do I request a change to an existing report or to request a new report?</a></li>
                           
                        </ul>
                        
                        <p><a href="../../office-of-information-technology/dw/FAQs.html#top">TOP</a></p>
                        
                        <hr size="1" width="80%">
                        <a name="01" id="01"></a><br>
                        <span>How do I get the Microsoft Add-ins installed?<br>
                           </span>  The Microsoft Add-in is used to access the Data Warehouse through the Microsoft
                        Office suite of tools. This provides the ability to run reports and return the results
                        into Excel, Word, or Power Point. 
                        
                        <ul>
                           
                           <li>The OIT Help Desk can install Microsoft Add-ins for the Data Warehouse. Contact them
                              (X-5555 or <a href="mailto:oithelp@valenciacollege.edu">oithelp@valenciacollege.edu</a>) to schedule the install.  
                           </li>
                           
                        </ul>
                        
                        <p name="a02" id="a02"><span><a name="a02" id="a02"></a><br>
                              </span><span>How do I change the output format of a report to CSV or HTML?</span><br>
                           The default output format when the Microsoft Add-in is installed is a proprietary
                           SAS format. Depending on your needs and what you are trying to accomplish you may
                           want to change the format. You can change the format to csv or HTML by doing the following:
                        </p>
                        
                        <p name="a02" id="a02"> <img alt="How to change output format for Microsoft Add-in" height="649" src="ChangingOutputFormat-SASAdd-in-Excel2007.jpg" width="500"> 
                        </p>
                        
                        <p name="a02" id="a02"><a name="a03" id="a03"></a><br>
                           
                           
                           
                        </p>
                        
                        <p><span><a name="a04" id="a04"></a><br>
                              The "SAS" tab in Excel disappeared, how do I get it back?</span><br>
                           Under certain conditions the SAS Add-in may be deactivated, causing the "SAS" tab
                           to disappear. You can call the help desk (x-5555) and they can walk you through the
                           steps to reactivate the SAS Add-in, or you can follow the link below for directions:
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/office-of-information-technology/dw/Re-activatingTheSASAdd-in-Excel2007.docx">How to reactivate the SAS Add-in (Excel 2007).</a></li>
                           
                        </ul>
                        
                        <p><a href="../../office-of-information-technology/dw/FAQs.html#top">TOP</a></p>
                        
                        <hr size="1" width="80%">
                        <a name="b01" id="b01"></a><br>
                        <span>I attended a training class and want to know where can I get a copy of the material?</span><br>
                        The material is available for download from the following links:
                        
                        <ul>
                           
                           <li><a href="documents/GettingtotheDWPortal.doc">Data Warehouse Portal Training Manual</a></li>
                           
                           <li><a href="documents/Add-inforMSOffice2007.doc">Add-in for Microsoft Office 2007 Training Manual</a></li>
                           
                        </ul>
                        
                        <p><span><a name="b02" id="b02"></a><br>
                              Some coworkers are being trained on how to access the Data Warehouse, how can I receive
                              training?<br>
                              </span>Training sessions are scheduled through Leadership Valencia. Check the Leadership
                           Valencia calendar for the next available session. 
                        </p>
                        
                        <p><a href="../../office-of-information-technology/dw/FAQs.html#top">TOP</a></p>
                        
                        <hr size="1" width="80%">
                        
                        <p><a name="c01" id="c01"></a> <br>
                           <span>I need access to the Data Warehouse to perform my job, how do get access?</span><br>
                           An online form is available to request access to the Data Warehouse. Supervisor approval
                           is required for general access and AVP approval is needed for access to secure areas.
                        </p>
                        
                        <p><a href="../../office-of-information-technology/tss/helpdesk/Request_DW.html">Data Warehouse access request. </a></p>
                        
                        <p>  <a name="c02" id="c02"></a><br>
                           <span>Why am I getting a message that my logon is incorrect when I enter my Atlas ID and
                              password to access the Data Warehouse?</span><br>
                           The first possible cause is that you may not have been granted access. Before you
                           can access the Data Warehouse for the first time you need to be authorized and your
                           ID needs to be setup. To request access to the Data Warehouse, use the following <a href="../../office-of-information-technology/tss/helpdesk/Request_DW.html">form</a>. 
                        </p>
                        
                        <p>If  you have previously been able to access the Data Warehouse and you are able to
                           access Atlas, your account may need to be refreshed. The OIT Help Desk (X-5555) is
                           able to perform the necessary refresh.
                        </p>
                        
                        <p>Or you can try <a href="http://cpip.valenciacollege.edu/iTools/?Run=iTools&amp;Fun=ShowRefreshUser&amp;Id=03053558" target="_blank">"Refreshing"</a> your Atlas account yourself. Enter your Atlas ID or VID and wait about 10 minutes
                           before trying to logon to the Data Warehouse. 
                        </p>
                        
                        <p>If you are still having problems, contact the Data Warehouse team using the <a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/dw/contact.cfm" target="_blank">Contact Us</a> link or send an email directly to <a href="mailto:DWHELP@valenciacollege.edu">DWHELP@valenciacollege.edu</a>.
                        </p>
                        
                        <p><a name="c03" id="c03"></a><br>
                           <span>Why can't I run reports in Excel after changing my password in Atlas?</span><br>
                           Your Data Warehouse logon ID/Password is tied to your Atlas logon ID/Password. If,
                           when you run a report in Excel, you are prompted for an ID/Password, use your new
                           password immediately after changing it in Atlas.
                        </p>
                        
                        <p>If you <strong>are not </strong>prompted for a password when you run a report in Excel then your password is being
                           stored and it will need to be updated if you change your password in Atlas.
                        </p>
                        
                        <p>Directions on how to update your password in the Excel add-in are available by following
                           the link below:
                        </p>
                        
                        <ul>
                           
                           <li><a href="documents/ChangePasswordInAMO-2007.pdf">How to update your password in the Excel add-in (2007).</a></li>
                           
                        </ul>
                        
                        <p><span><a name="c04" id="c04"></a> <br>
                              Why do I get an error message when I try to access the Data Warehouse Portal from
                              outside of Valencia?<br>
                              </span>The Data Warehouse is only available while connected to the Valencia network. When
                           on campus, this can be accomplished by either plugging directly into the network (like
                           the PC in your office or a laptop that is plugged into a network outlet via a network
                           cable) or connecting to the Valencia Wireless Network (WiFi). Off campus if you establish
                           a VPN (Virtual Private Network) connection you will be able to access the Data Warehouse
                           portal. After establishing the VPN connection simply open a browser and connect to
                           the Data Warehouse portal page. 
                        </p>
                        
                        <p><a href="../../office-of-information-technology/dw/FAQs.html#top">TOP</a></p>
                        
                        <hr size="1" width="80%">
                        
                        <p><span>        <a name="d01" id="d01"></a><br>
                              </span><span>The question I have isn't addressed in the FAQ, what can I do?</span><br>
                           You can send your question to the Data Warehouse team using the <a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/dw/contact.cfm" target="_blank">Contact Us</a> link or send an email directly to <a href="mailto:DWHELP@valenciacollege.edu">DWHELP@valenciacollege.edu</a>. Who knows, you question may show up on the FAQ page. 
                        </p>
                        
                        <p><span><a name="d02" id="d02"></a><br>
                              What are reports DQ0001 and DQ0002 telling me?</span><br>
                           These reports are used for Data Quality purposes to ensure Credit Hours are properly
                           assigned at the Course, Section, and Meet Time levels. For a complete explanation
                           read <a href="documents/DQ0001andDQ0002Explained.pdf">DQ0001 and DQ0002 Explained</a>. 
                        </p>
                        
                        <p><span><a name="last" id="last"></a><br>
                              <strong>How do I request a change to an existing report or to request a new report?</strong></span><br>
                           The "Project Request System" (PRS) has been setup to receive and track Data Warehouse
                           requests. Requests for changes should be entered into the <a href="http://oit.valenciacollege.edu/prs?Type=DWR&amp;User=User&amp;Show=Submit" target="_blank">PRS</a>.
                        </p>
                        
                        
                        <p><a href="../../office-of-information-technology/dw/FAQs.html#top">TOP</a></p>
                        
                        <hr size="1" width="80%">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/dw/faqs.pcf">©</a>
      </div>
   </body>
</html>