<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Data Warehouse | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/dw/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/dw/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Data Warehouse</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Dw</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           
                           <a href="https://sasprod-web.valenciacollege.edu:8443/SASPortal"><img alt="DW Portal" src="button-dw-login.png"></a>
                           
                           
                           
                           
                           
                           
                           
                        </div> 
                        
                        
                        
                        <p><strong>Future System Maintenance Windows </strong></p>
                        
                        <p>For details regarding future system maintenance windows, please <a href="../../calendar/calendar.cfm-catid=40.html">click here</a> to see our <a href="../../calendar/calendar.cfm-catid=40.html">maintenance calendar</a>.
                        </p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><a href="http://answers.valenciacollege.edu" target="_blank"><img alt="" border="0" height="50" src="TopFAQ.gif" width="240"></a></div>
                                 
                              </div>
                              
                              <div>
                                 <div>
                                    
                                    
                                    <ol>
                                       
                                       
                                       <li>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <a href="http://answers.valenciacollege.edu/ics/support/default.asp?deptID=5572&amp;task=knowledge&amp;questionID=100" target="_blank">Data Warehouse Update Schedule</a> <br>
                                          
                                          
                                          
                                          
                                          
                                          
                                       </li>
                                       
                                       
                                       <li>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <a href="http://answers.valenciacollege.edu/ics/support/default.asp?deptID=5572&amp;task=knowledge&amp;questionID=86" target="_blank">How do I log into the DW Portal?</a> <br>
                                          
                                          
                                          
                                          
                                          
                                          
                                       </li>
                                       
                                       
                                    </ol>
                                    
                                 </div>
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/dw/index.pcf">©</a>
      </div>
   </body>
</html>