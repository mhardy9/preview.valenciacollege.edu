<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Technology Services (CTS) | Valencia College</title>
      <meta name="Description" content="CTS - Campus Technology Services provides technical service &amp;amp; support for our classrooms, libraries, common learning areas, and faculty &amp;amp; staff offices. ">
      <meta name="Keywords" content="digital signage, cctv, support for our classrooms, computer technology, desktop support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/campus-technology-services/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/campus-technology-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office of Information Technology (OIT)</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Campus Technology Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>
                           Campus Technology Services (CTS)
                           
                        </h2>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           The Campus Technology Services Department provides technical service &amp; support for
                           our classrooms,
                           libraries, common learning areas, and faculty &amp; staff offices. We also provide training
                           in the use of
                           educational &amp; classroom technology. The Campus Technology Services is represented
                           on each campus, and is a
                           division of the Office for Information Technology.
                           
                        </p>
                        
                        <p>
                           If you encounter any difficulty with the classroom technology please notify us and
                           we will resolve the
                           issue promptly.
                           
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           Desktop Support
                           
                        </h4>
                        
                        <p>
                           We provide support to all faculty, staff, and classroom computers across the college.
                           If you are
                           experiencing difficulties gaining access to any software or operating hardware, please
                           feel free contact
                           our help desk. We can assist you in identifying solutions and scheduling service calls
                           with our
                           technicians.
                           
                        </p>
                        
                        
                        <ul class="list_style_1">
                           
                           <li>Purchasing support</li>
                           
                           <li>Quotes</li>
                           
                           <li>Refresh status and deployment</li>
                           
                           <li>Problem resolution</li>
                           
                           <li>Software installation and support</li>
                           
                           <li>Equipment moves</li>
                           
                           <li>Printer support</li>
                           
                        </ul>
                        
                        <p>
                           <a href="http://net4.valenciacollege.edu/forms/oit/locations-technology-services/equipment-request.cfm">
                              Equipment request Form </a>
                           
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           Digital Signage and CCTV
                           
                        </h4>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           We support digital signage equipment used to display campus news, information, and
                           events across the
                           college. We also manage the broadcasting of content through closed circuit television
                           at each campus which
                           may include local news, weather alerts, or special events.
                           
                        </p>
                        
                        <p>
                           If you are interested in submitting content, please contact Student Development on
                           your campus.
                           
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           Faculty Staff Services
                           
                        </h4>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           In addition to our standard support services we partner with faculty and staff to
                           deliver customized
                           technology solutions. We work collaboratively with each department to help facilitate
                           the use of
                           technology resources. We also assist with the exploration of new ideas and ways to
                           implement technology
                           solutions to enhance the services we deliver to the classroom and college support
                           areas.
                           
                        </p>
                        
                        <p>
                           We also support a number of initiatives to support faculty and staff. Our goal is
                           to ensure a seamless
                           delivery of technical services to compliment each division of the college.
                           
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           Special Events
                           
                        </h4>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           We provide equipment and support for campus events, meetings, conferences &amp; functions.
                           
                        </p>
                        
                        <p>
                           We ask that you contact your campus representative to review event specifications,
                           support requirements,
                           and any specific concerns. Typically we require 48 hour notice for most events, with
                           more lead time
                           required for larger events to accommodate scheduling as well as making sure the necessary
                           resources are in
                           place.
                           
                        </p>
                        
                        <p>
                           Some examples of event services:
                           
                        </p>
                        
                        
                        <ul class="list_style_1">
                           
                           <li>Laptop &amp; printer setup</li>
                           
                           <li>PA system setup</li>
                           
                           <li>Wireless Microphone setup</li>
                           
                           <li>Video Recording (basic setup)</li>
                           
                           <li>Video Streaming</li>
                           
                           <li>Online Meeting Support</li>
                           
                           <li>Assistance with presentations</li>
                           
                        </ul>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>Training</h4>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           The Campus Technology Services team is always available to provide training on the
                           use of any of the
                           technologies we provide. Our department provides training in classroom equipment use,
                           best practices, and
                           procedures. We can schedule individualized sessions tailored to your skill level.
                           We also help facilitate
                           the use of your devices and equipment in the classroom along with installation of
                           college-licensed
                           software.
                           
                        </p>
                        
                        <p>
                           Tier classrooms operation &amp; educational technologies
                           
                        </p>
                        
                        <p>
                           We offer Smart (Tier) Classroom training on request, and in addition can provide training
                           on any specific
                           gear as needed.
                           
                        </p>
                        
                        <p>
                           Please visit the contact us page to arrange for a Smart Classroom training session,
                           or to just review any
                           training requirements you may have.
                           
                        </p>
                        
                        <p>
                           As a reminder, the LTS team is readily available on each campus for software training
                           requirements.
                           
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/campus-technology-services/index.pcf">©</a>
      </div>
   </body>
</html>