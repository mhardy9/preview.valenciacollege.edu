<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Technology Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/campus-technology-services/services/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/technology/office-of-information-technology/campus-technology-services/services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Technology Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/campus-technology-services/">Campus Technology Services</a></li>
               <li>Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h3>Services</h3>
                        
                        <p>For complete overview of the services we provide please visit each section for additional
                           detaill. 
                        </p>
                        
                        <ul>
                           
                           <li><a href="../../../office-of-information-technology/locations-technology-services/services/classroom-technology.html">Classroom Technology</a></li>
                           
                           <li><a href="../../../office-of-information-technology/locations-technology-services/services/desktop-support.html">Desktop Support</a></li>
                           
                           <li><a href="../../../office-of-information-technology/locations-technology-services/services/digital-signage-cctv.html">Digital Signage and CCTV </a></li>
                           
                           <li><a href="../../../office-of-information-technology/locations-technology-services/services/faculty-staff-services.html">Faculty Staff Services</a></li>
                           
                           <li><a href="../../../office-of-information-technology/locations-technology-services/services/special-events.html">Special Events</a></li>
                           
                           <li><a href="../../../office-of-information-technology/locations-technology-services/services/training.html">Training</a></li>
                           
                        </ul>            
                        
                        
                        <p>Be sure to visit the campus specific tabs for services that are unique to each campus.
                           
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><span> Technical Support</span> - 407-582-5555
                        </p>
                        
                        <p><strong>Email</strong> - OITHelp@valenciacollege.edu
                        </p>
                        
                        <p><strong>East &amp; Winter Park </strong></p>
                        
                        <p>Jamie Rost - 407-582-2854</p>
                        
                        <p><strong>Osceola &amp; Lake Nona </strong></p>
                        
                        <p>Daniel Charriez - 321-682- 4164 </p>
                        
                        <p><strong>District Office </strong></p>
                        
                        <p>Carmine La Pietra - 407-582-1577 </p>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/campus-technology-services/services/index.pcf">©</a>
      </div>
   </body>
</html>