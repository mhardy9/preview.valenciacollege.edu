<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>TiER Classrooms - Valencia College | Valencia College</title>
      <meta name="Description" content="TiERs (Technology-Enhanced Room) which will define and identify exactly what level of technology exists in the room. ">
      <meta name="Keywords" content="tiers, tier classrooms, technology-enhanced room, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/campus-technology-services/tier-classrooms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/campus-technology-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/campus-technology-services/">Campus Technology Services</a></li>
               <li>TiER Classrooms - Valencia College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>
                           TiER Classroom
                           
                        </h2>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           In order to better define terminology used to describe and classify technology-enhanced
                           rooms at the
                           college, a new classification system has been developed. Under this new system, all
                           technology-enhanced
                           rooms will now be referred to generally as Smart Rooms with an added level of definition
                           as outlined
                           below.&nbsp; Smart Rooms will be sub-classified by TiERs (<u>T</u>echnology-<u>E</u>nhanced <u>R</u>oom)
                           which will define and identify exactly what level of technology exists in the room.&nbsp;
                           
                        </p>
                        
                        <p>  All TiER rooms available can be found here : <a href="/documents/employees/office-of-information-technology/Technology_Enhanced_Rooms.pdf">Technology_Enhanced_Rooms.pdf</a></p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           TiER 1 Classroom
                           
                        </h4>
                        
                        <p>
                           The level of technology in the room is very limited in scope. The room contains a
                           ceiling-mounted
                           projector and at least one multimedia source to display but is otherwise severely
                           limited in technology
                           and infrastructure. The room can be upgraded to another TiER at considerable cost.
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           TiER 2 Classroom
                           
                        </h4>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           The standard compliment of technology is in place as is the infrastructure to accommodate
                           the necessary
                           cabling and data ports. This room can be easily upgraded with minimal expenditure
                           in funds and time.
                           
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           TiER 3 Classroom
                           
                        </h4>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           The standard compliment of equipment exists as does the Crestron controlling features.
                           The infrastructure
                           is in place with the cabling properly installed. A list of rooms that fall under this
                           tier can be found
                           here:
                           
                        </p>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h4>
                           TiER 4 Classroom
                           
                        </h4>
                        
                        <p>
                           
                        </p>
                        
                        
                        <p>
                           This is a high-end video-conferencing room built with the standard TiER III compliment
                           of equipment.
                           
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/campus-technology-services/tier-classrooms.pcf">©</a>
      </div>
   </body>
</html>