<ul>
<li><a href="/employees/office-of-information-technology/index.php">Office of Information Technology (OIT)</a></li>
<li><a href="/employees/office-of-information-technology/services.php">Services</a></li>
<li><a href="/employees/office-of-information-technology/staff.php">Staff</a></li>


   <li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);">Departments <span class="fas fa-chevron-down" aria-hidden="true"></span></a>
              <div class="menu-wrapper">
                <div class="col-md-12">
                  <h3>Campus Technology Services (CTS)</h3>
                  <ul>
                    <li><a href="/employees/office-of-information-technology/locations-technology-services/index.php">Campus Technology Services (CTS)</a></li>
                    <li><a href="/employees/office-of-information-technology/locations-technology-services/tier-classrooms.php">TiER Classrooms</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/oit/locations-technology-services/equipment-request.cfm" target="_blank">Equipment Request</a></li>
                  </ul>
                  <h3>Enterprises Application Services (EAS) </h3>
                  <ul>
                 <li><a href="/employees/office-of-information-technology/enterprises-application-services/">Enterprises Application Services</a></li>
                <li><a href="/employees/office-of-information-technology/enterprises-application-services/frequently-asked-questions.php">FAQ</a></li>
                  </ul>
                 <h3>Learning Technology Services (LTS) </h3>
                  <ul>
                <li><a href="/employees/office-of-information-technology/learning-technology-services/index.php">Learning Technology Services</a></li>
                <li><a href="/employees/office-of-information-technology/learning-technology-services/valencia-productions.php">Valencia Productions</a></li>
                <li><a href="/employees/office-of-information-technology/learning-technology-services/faculty-resources.php">Faculty Resources</a></li>
                <li><a href="/employees/office-of-information-technology/learning-technology-services/student-resources.php">Student Resources</a></li> 
            </ul>
                 <h3>Network and Infrastructure Services (NIS) </h3>
                  <ul>
                <li><a href="/employees/office-of-information-technology/networking/">Network and Infrastructure Services</a></li>
                <li><a href="/employees/office-of-information-technology/networking/resources.php">Networking Resources</a></li>
                <li><a href="/employees/office-of-information-technology/networking/wifi.php">Wifi</a></li>
                 <h3>Web and Portal Services (WPS) </h3>
                  <ul>
                <li><a href="/employees/office-of-information-technology/web-and-portal-services">Web and Portal Services</a></li>  
                <li><a href="/employees/office-of-information-technology/web-and-portal-services/web-standards.php">Web Standards</a></li>
                <li><a href="http://net4.valenciacollege.edu/forms/oit/web/request.cfm" target="_blank">Web Request</a></li>
                <li><a href="/employees/office-of-information-technology/web-and-portal-services/sharepoint.php">SharePoint</a></li>
            </ul>
                    </div>
              </div>
              <!-- End menu-wrapper --></li>
              

<li><a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new" target="_blank">Help Desk</a></li>
<li><a href="tel://4075825555">Need OIT Support: 407-582-5555</a></li>

</ul>