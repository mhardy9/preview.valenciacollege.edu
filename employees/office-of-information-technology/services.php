<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="office of information technology, oit, eas, wps, nis, web and portal services, enterprise data solutionscollege, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/employees/office-of-information-technology/locations-technology-services/index.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          CTS - Campus Technology Services
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    CTS - Campus Technology Services
                                    
                                 </h3>
                                 
                                 <p>
                                    The Campus Technology Services Department provides technical service &amp; support for
                                    our classrooms,
                                    libraries, common learning areas, and faculty &amp; staff offices. We also provide training
                                    in the use
                                    of educational &amp; classroom technology. The Campus Technology Services is represented
                                    on each campus,
                                    and is a division of the Office for Information Technology
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/employees/office-of-information-technology/locations-technology-services/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/employees/office-of-information-technology/enterprises-application-services/index.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          EAS - Enterprise Application Services
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    EAS - Enterprise Application Services
                                    
                                 </h3>
                                 
                                 <p>
                                    The Enterprise Application Services area of the Office of Information Technology provides
                                    maintenance, support, and development of various PC, network-based, applications and
                                    systems at the
                                    college.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/employees/office-of-information-technology/enterprises-application-services/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/employees/office-of-information-technology/learning-technology-services/index.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          LTS - Learning Technologies Services
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    LTS - Enterprise Application Services
                                    
                                 </h3>
                                 
                                 <p>
                                    The Instructional Technology Support area of the Office of Information Technology
                                    provides
                                    technology support to faculty and manages the centralized online course delivery software
                                    used by
                                    the college.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/employees/office-of-information-technology/learning-technology-services/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/employees/office-of-information-technology/networking/index.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          NIS - Network and Infrastructure Services
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    NIS - Network and Infrastructure Services
                                    
                                 </h3>
                                 
                                 <p>
                                    Networking and Computer Services area of the Office of Information Technology is responsible
                                    for
                                    ongoing operational support, maintenance and development of Valencia’s IT infrastructure
                                    and network
                                    applications.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/employees/office-of-information-technology/networking/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/employees/office-of-information-technology/web-and-portal-services/index.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          WPS - Web and Portal Services
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    WPS - Web and Portal Services
                                    
                                 </h3>
                                 
                                 <p>
                                    The Web &amp; Portal Services area of the Office of Information Technology is responsible
                                    for
                                    maintenance, support and development of the college's Internet presence.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/employees/office-of-information-technology/web-and-portal-services/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new" target="_blank"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          HELPDESK
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    HELPDESK
                                    
                                 </h3>
                                 
                                 <p>
                                    The Service Desk and Technical Support area of the Office of Information Technology
                                    is responsible
                                    for monitoring and responding to college-wide technical support issues.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new" class="button-outline" target="_blank">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        
                        <h3>
                           HELP DESK SUPPORT
                           
                        </h3>
                        
                        <p>
                           Contact the Help Desk to place a Support Ticket
                           
                        </p>
                        <a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new" class="banner_bt">Click here</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>
                           Need OIT Support?
                           
                        </h5>
                        <i class="icon-phone"></i>
                        
                        <p>
                           <a href="tel://4075825555">Call Extension 5555</a><br>
                           <small>Monday to Friday 9.00am - 6.00pm</small>
                           
                        </p>
                        
                     </div>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/services.pcf">©</a>
      </div>
   </body>
</html>