<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web &amp; Portal Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web/departmentweb.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web &amp; Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/web/">Web</a></li>
               <li>Web &amp; Portal Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>Create a Department Web Site</h2>
                        
                        <p>To set up a Web site for your department,  please take the following steps:</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>Collect or write all copy in Microsoft Word. Make sure to include the title
                                 of your page. <br>
                                 <br>
                                 <strong>Typical Web layout with page titles </strong><br>
                                 <img height="150" src="webLayout.gif" width="514"> 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Collect or create graphics and/or photos to be placed on pages. Make sure
                                 to indicate where the images will be placed in your copy (use filenames). <br>
                                 <br>
                                 <img height="300" src="webPageSample.gif" width="431"> 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Contact the office of <a href="mailto:msc@valenciacollege.edu">Marketing &amp; Strategic Communications</a> (407-582-1779) to coordinate any current promotions or themed
                                 graphics for your site. If you have any printed
                                 material
                                 your
                                 department
                                 uses,
                                 send via inter-office mail to your assigned designer from  Web Services. 
                              </p>
                              
                              <ol type="a">
                                 
                                 <li> MAIL CODE 4-12 -- ATTN: Jeff Danser. (example:
                                    brochures or postcards).<br>
                                    <br>
                                    <img height="360" src="webPageSample_Alumni.jpg" width="576"> 
                                 </li>
                                 
                                 
                                 <p>**Please Note, the Right Side-Bar is only for advertisments, hours, events or other
                                    items as such.  Navigation will not be place into this section.
                                 </p>
                                 
                              </ol>
                              
                           </li>
                           
                           <li>
                              
                              <p>If you have not already done so, <a href="http://oit.valenciacollege.edu/prs?User=User&amp;Show=Submit&amp;Type=WWR">place
                                    a Web request</a>. You will be contacted
                                 by an assigned Web developer. You will be asked for the materials mentioned
                                 above.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>A Web developer will create your site with the your materials you have sent.</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>You may request a Contribute key to edit your site. <a href="../../office-of-information-technology/web/updates.html">For more information about Contribute content management, click here.</a></p>
                              
                           </li>
                           
                        </ol>
                        
                        
                        <p><a href="../../office-of-information-technology/web/departmentWeb.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web/departmentweb.pcf">©</a>
      </div>
   </body>
</html>