<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web &amp; Portal Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web/standards.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web &amp; Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/web/">Web</a></li>
               <li>Web &amp; Portal Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>Web Standards  </h2>
                        
                        <h3>Guiding Principles</h3>
                        <br>
                        Valencia College recognizes the value of publishing on the World
                        Wide Web (WWW) and encourages the development of WWW content that supports
                        the mission of the college and approved strategic planning directions. 
                        
                        <p> The quality of information published by the college plays an important role
                           in presenting the college to the community and maintaining its reputation
                           and image as a major educational institution in the Orlando area. This document
                           sets forth minimum standards that are meant to ensure that information published
                           electronically is visually appealing, well-written and follows the same high
                           standards as other forms of published information. The standards are necessary
                           to give
                           the Valencia Web site uniformity so it represents one institution, while at the
                           same time allowing the site to be both interesting and an expression of our
                           multiple initiatives. 
                        </p>
                        
                        
                        <h3>Web Content Placement</h3>
                        <br>
                        The Valencia College Web site contains a lot of information  and must be organized
                        in a way to help
                        our audiences find what they are looking for. To do this we have set up a table that
                        defines four
                        types of content and pairs them with the content delivery tools available at Valencia.
                        These tools can
                        be used for each type of content by asking two questions: Who is the audience? And
                        what type of
                        access is required? Once these two answers are determined, we can direct clients to
                        place content into
                        one of these four areas or tools.            
                        
                        <blockquote>
                           
                           <p><a href="documents/Web-Content-Placement-Guidelines.pdf" target="_blank">Web Content Placement Guidelines</a></p>
                           
                        </blockquote>
                        
                        
                        
                        
                        <h3>The Valencia “Look &amp; Feel”</h3>
                        <br>
                        The  look of the Valencia Web site has been designed with
                        the end user in mind. It is easy to navigate to find what
                        you need quickly. 
                        If your area were to stray from this look &amp; feel, you
                        could confuse the user and they may think that they are
                        not 
                        at a Valencia site at first glance. The user could also think
                        your web pages are out of date. We want to keep the Valencia
                        
                        web areas consistent in their look &amp; layout so the user
                        gets used to the interface and gets comfortable enough to
                        easily 
                        find their way around to the information they need.
                        
                        <p><strong>Navigation</strong><br>
                           Valencia has four key areas present in each 
                           web area that have links to get to other areas and areas within 
                           your web. They are the header, the footer, the breadcrumbs 
                           and the local navigation. Some sites may have a sub-local 
                           navigation due to deeper levels of information in that area. 
                        </p>
                        
                        <p>The header is at the top of every page and has links to
                           the  main areas of the Valencia site: the homepage, the future students
                           
                           page, the current students
                           
                           page, the faculty &amp; staff page and the visitors &amp; friends
                           page. These pages are key pages that
                           
                           act as directory listings with links to related areas of
                           the  Valencia Web site. The header also has support links
                           for added 
                           usability. 
                        </p>
                        
                        <p>The breadcrumbs are located just below the header of every page. These are links that
                           form a trail of where the user is located. This allows the user to back track if necessary.
                           
                        </p>
                        
                        <p>The footer is located at the bottom of every page. These links mirror some of the
                           links in the header and also contains typical links found in all Web site footers
                           such as location and contact information. 
                        </p>
                        
                        <p>Each site's local navigation is found on the left side with links diplayed vertically.
                           Below each local navigation are related links. The related links are links that are
                           external to the local department site. These links can connect to other internal Valencia
                           sites or external sites. 
                        </p>
                        
                        <p><strong>Styles &amp; Colors</strong><br>
                           Valencia Web site styles and colors are created and maintained by the office of Marketing
                           &amp; Strategic Communications. These  are implemented site wide so every page is using
                           a consistent look. To ensure that your site is updated with the rest of the site,
                           you can use the styles that are included in every page. You can also contact  Web
                           services or the marketing offices for custom graphics. 
                        </p>
                        
                        <p><strong>Logos</strong><br>
                           Valencia has a logo that is specifically designed by our marketing office. This logo
                           must be used according to specification. Please read the usage guide before you post
                           the logo on your Web site. <a href="http://valenciacollege.edu/brand/">Brand Standards and Guidelines</a></p>
                        
                        <h3>Web Site Statistics / Page Counters</h3>
                        <br>
                        Valencia uses a stats program called SmarterStats. SmarterStats is a powerful and
                        easy-to-use web site analysis
                        solution. In order to view the stats on your department site,  you must <a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/web/request.cfm" target="_blank">request a report</a>.
                        Once your reports are generated you will be emailed your reports. These reports are
                        generated weekly &amp; monthly
                        for the life of your site or until you tell us to take
                        it down. 
                        
                        <p>Valencia does not use page counters. We insist on back end
                           stats to keep the front end professional.
                        </p>
                        
                        <h3>Valencia Search Engine</h3>
                        <br>
                        To help your placement in the Valencia search engine, your page title and heading
                        at the top of your page play an important role in finding your page. Be sure all of
                        your pages in your Web site have the appropriate title. The page heading can repeat
                        the page title or be more specific. Please send us keywords that will help users find
                        your site. We will place them in the back-end code of your page. 
                        
                        <h3>Campus Web Sites</h3> - East, West, Osceola and Winter Park <br> 
                        Campus sites should have a reference to their campus on every page of each department
                        site so the user can easily see which campus the deparment belongs.  The front page
                        of the campus site should drill down to their own internal departments before linking
                        back to college-wide pages. 
                        
                        
                        <p><a href="../../office-of-information-technology/web/standards.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web/standards.pcf">©</a>
      </div>
   </body>
</html>