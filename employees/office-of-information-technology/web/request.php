<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web &amp; Portal Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web/request.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web &amp; Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/web/">Web</a></li>
               <li>Web &amp; Portal Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>Web Requests</h2>
                        
                        <p>Please submit all requests for Web &amp; Portal Services through the <a href="https://valenciacollege.edu/servicedesk">OIT Service Desk</a>.  All requests will be reviewed and prioritized once they have been submitted through
                           the Service Desk form.
                        </p>
                        
                        <h3>Types of Issues &amp; needed information</h3>
                        
                        <ul>
                           
                           <li>Updates to Departmental Websites
                              
                              <ul>
                                 
                                 <li>Please see the detailed instructions for these type of Updates on the <a href="../../office-of-information-technology/web/updates.html">Updates to Departmental Websites</a> page.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>SharePoint Issue
                              
                              <ul>
                                 
                                 <li>Include the URL (link) for the SharePoint site you are having issues with.</li>
                                 
                                 <li>Give a detailed description for the issue.</li>
                                 
                                 <li>List all users who were having this specific issue</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Atlas Issue
                              
                              <ul>
                                 
                                 <li>Include the URL (link) for the Atlas Page you are having issues with</li>
                                 
                                 <li>Give a detailed description of the issue.</li>
                                 
                                 <li>Give specific VIDs or UserNames for users that have had this issue.</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Web Page or Web Application Issue
                              
                              <ul>
                                 
                                 <li>Include the URL (link) for the specific page or pages you are having issues with</li>
                                 
                                 <li>Give a detailed description of the issue.</li>
                                 
                                 <li>List all users who have had this issue.</li>
                                 
                                 <li>Give an approximate date/time for when this issue happened.</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Vanity URL or Page Redirect
                              
                              <ul>
                                 
                                 <li>What is requested Vanity URL or the URL of the page that has been removed/renamed?</li>
                                 
                                 <li>What URL should this link be redirected to?</li>
                                 
                                 <li>How long should this URL be in place?</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Video Upload/Embed/Link
                              
                              <ul>
                                 
                                 <li>If you have a video you would like to embed in a site or link to, WPS can upload it
                                    to Kaltura for you.
                                 </li>
                                 
                                 <li>You will be asked to upload the video to your OneDrive &amp; share with the developer
                                    who takes this ticket
                                 </li>
                                 
                                 <li>Make sure you include the URL (link) to the page you'd like the video embedded on
                                    (if needed).
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>New Site (ex: http://valenciacollege.edu/NewSite)
                              
                              <ul>
                                 
                                 <li>New site requests will be handled as a project in the WPS system. Please submit a
                                    <a href="https://valenciacollege.edu/servicedesk">Service Desk Ticket</a> to start the process.
                                 </li>
                                 
                                 <li>You will be asked to have ALL content ready before a New Site project can be started.</li>
                                 
                                 <li>Please be aware that new site projects may be put on hold until the new Content Management
                                    System is in place.
                                 </li>
                                 
                                 <li>In the Service Desk Ticket, please give as much detail as you have about what you
                                    want on the new site
                                 </li>
                                 
                                 <li>WPS may set up a meeting to discuss your New Site request.</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Web Form (new or updating)
                              
                              <ul>
                                 
                                 <li>Web Form requests will be handled as a project in the WPS system.  Please submit a
                                    <a href="https://valenciacollege.edu/servicedesk">Service Desk Ticket</a> to start the process.
                                 </li>
                                 
                                 <li>Please detail the form &amp; its requirements when submitting.  A Word Document is typically
                                    the easiest way to submit this information
                                 </li>
                                 
                                 <li>Be sure to include information about
                                    
                                    <ul>
                                       
                                       <li>Required Fields</li>
                                       
                                       <li>Distribution (email addresses)</li>
                                       
                                       <li>Accessing Information collected (email only, save as CSV)</li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Web Applications
                              
                              <ul>
                                 
                                 <li>New or changes to web applications will be handled as a project in the WPS system.
                                    Please submit a <a href="https://valenciacollege.edu/servicedesk">Service Desk Ticket</a> to start the process.
                                 </li>
                                 
                                 <li>Include as much detail about the changes or new Web Application as possible.  This
                                    will help us prioritize the project
                                 </li>
                                 
                                 <li>WPS will set up a meeting to discuss your Web Application request</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web/request.pcf">©</a>
      </div>
   </body>
</html>