<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web &amp; Portal Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web/searchengines.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web &amp; Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/web/">Web</a></li>
               <li>Web &amp; Portal Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>Search Engines</h2>
                        
                        <h3>Potential Design/Set Up Problems</h3>
                        <br>
                        If your site does not use Frames, Flash, Javascript, Image Maps, or Dynamic URLs
                        you can skip to the next tip.
                        
                        <p>Unfortunately there are some site design/set up issues
                           that can make it very difficult for the search engines to list your web pages
                           no matter how well you optimize and submit them. What follows is a starting list of
                           elements to take into consideration when trying to get a site listed. Almost everything
                           can be fixed or optimized before you submit. The most
                           common problems are:
                        </p>
                        
                        <ol>
                           
                           <li>Sites that use Frames. </li>
                           
                           <li>Sites that use Dynamic URLs. </li>
                           
                           <li>Sites that use Flash. </li>
                           
                           <li>Sites that use Image Maps for navigation. </li>
                           
                           <li>Sites that use Javascript for
                              navigation. 
                           </li>
                           
                           <li>Sites that contain many broken links.</li>
                           
                        </ol>
                        
                        <p><strong>Frames</strong><br>
                           Framed sites can be invisible to search engines. The crawlers that cannot
                           follow frame links never see the content pages that human visitors see. They
                           only see the frameset page itself. The best answer to this problem is to
                           redesign your site without using frames. If that is not possible you can
                           have your Webmaster add information to the frameset page to make it more
                           search engine friendly. Since the frameset is treated just like an HTML page
                           your Webmaster should add meta tags and a title to the frameset page. Additionally,
                           descriptive text can be included in a "No Frames" tag. This tag
                           is intended for older browsers that do not support frames, but it is also
                           visible to crawlers when they are confronted with a frameset.
                        </p>
                        
                        <p><strong>Dynamically Generated Pages</strong><br>
                           A problem with many sites managed by a CMS (Content Management System) is that their
                           pages are dynamically
                           generated. Most search engines do not recognize the "?" and "&amp;amp;" characters
                           that separate CGI variables. This means that some individual pages
                           will not be crawled by some search engines. One way to prevent this problem
                           is to create static links to yuor site's most important  dynamic pages for search
                           engines
                           to crawl. Another option is to use the "dynamic page optimization solutions" available
                           for ASP, ColdFusion, CGI/Perl, or the Apache Web Server. If you have to include dynamic
                           pages, make sure the parameters are short and the number of them few.
                        </p>
                        
                        <p><strong>Flash Animation and Graphic Text</strong><br>
                           Search engines use the text on your page for their ranking determinations. If
                           your site is filled with Flash animation or if your text appears as a graphic
                           the search engine crawler will not see it. Rich media enhancements like flash
                           look good to human visitors but they are invisible to search engines.
                           To improve your chances for good ranking stick with simpler pages without animation.
                           If your site has fancy graphics in place of text be sure
                           to have your Webmaster put another version on the wording in the "Alt
                           Image" tag.
                        </p>
                        
                        <p><strong>Image Maps for navigation</strong><br>
                           If you use image maps for your main site navigation you should consider switching
                           to standard HTML hyperlinks or your site will most likely not get crawled.
                           If you want to keep the image maps you can, but you should add another navigation
                           scheme to your site that uses only standard HTML hyperlinks so that your site
                           will get crawled. Try to use text instead of or as well as images to display important
                           names, content or links. An easy way to do this is to create a site map page uses
                           standard HTML links to link to every page on your site. Then add a standard
                           HTML link on each page of your site that links to the site map. 
                        </p>
                        
                        <p><strong>JavaScript for navigation</strong><br>
                           Search engines can't follow links within Javascript, so your site
                           will not get crawled unless you also have some form standard HTML hyperlinks
                           that they can follow. You should add some standard HTML hyperlinks to
                           all of your pages  so that your site will be crawled
                           properly. An easy way to do this is to create a site map page uses standard
                           HTML links to link to every page on your site. Then add a standard HTML link
                           on each page of your site that links to the site map. 
                        </p>
                        
                        <p><strong>Broken Links<br>
                              </strong>Check for broken links and correct HTML often. Broken links keep search engines from
                           being able to optimally crawl your site. There are several tools available as browser
                           plug-ins (such as <a href="https://addons.mozilla.org/en-US/firefox/addon/532">LinkChecker</a> for Firefox) or web pages (such as <a href="http://validator.w3.org/checklink">W3's Link Checker</a>) that will scan your page for broken links and return a report. Make sure you keep
                           the links on your site fresh. You could also develop a reporting tool for users to
                           notify you when they find a broken link.
                        </p>
                        
                        
                        <h3>Search Engine Optimization</h3>
                        <br>
                        To help your placement in search engines, place your keywords, keyword phrases
                        and 25 word or less description of your site in the code (META tags) on your
                        front page. And make sure the title of the pages in your site are descriptive and
                        accurate.
                        Keep the links on a given page to a reasonable number (fewer than 100).
                        
                        <p>Beyond the Valencia Web Site, you must either spend the
                           time at each search engine web site you would like to be
                           a part
                           of
                           and
                           follow
                           their placement processes, or you can hire a service. There
                           are quite a few services out there that offer search engine
                           placement. The prices vary from $20/yr to $7000/mth.
                        </p>
                        
                        <p><strong>Sitemaps</strong><br>
                           Sitemaps, in general, are a textual representation of your site.  More specifically,
                           a Sitemap is a document that can be used to show the structure of a website to web
                           crawlers or users.  Sitemaps can improve SEO (search engine optimization) by making
                           sure that all pages can be found.  Google Sitemaps are used to help developers publish
                           lists of links from their site to make the site more easily crawlable.  Google recommends
                           that Sitemaps are helpful if: 
                        </p>
                        
                        <ul>
                           
                           <li>Your site has dynamic content.</li>
                           
                           <li>Your site has pages that aren't easily discovered by Googlebot during the crawl processï¿½for
                              example, pages featuring rich AJAX or images.
                           </li>
                           
                           <li>Your site is new and has few links to it. (Googlebot crawls the web by following links
                              from one page to another, so if your site isn't well linked, it may be hard for us
                              to discover it.)
                           </li>
                           
                           <li>Your site has a large archive of content pages that are not well linked to each other,
                              or are not linked at all.
                           </li>
                           
                        </ul>
                        
                        <p><strong>Inbound Links</strong><br>
                           Your site's PageRank is based on how many other sites link to your own. Therefore,
                           the more links there are to your site, the higher your page will be ranked in Search
                           Engine results. So, it is a good idea to get other sites to link to your site. You
                           can also pay to have sites link to your own site, but you should be careful when employing
                           this technique. It's important to avoid links to/from web spammers. Your own page
                           ranking may be adversely affected by links to and from known spammers. You can also
                           increase your page ranks by linking to your site from social media sites such as Blogs,
                           Social Networking or Social Bookmarking sites. 
                        </p>
                        
                        <p><a href="../../office-of-information-technology/web/searchEngines.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web/searchengines.pcf">©</a>
      </div>
   </body>
</html>