<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web &amp; Portal Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web/updates.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web &amp; Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/web/">Web</a></li>
               <li>Web &amp; Portal Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>Updates to Departmental Websites</h2>
                        
                        <p>In an earlier series of communications regarding Contribute and  future website updates,
                           OIT has presented an outline of our migration plan to a  new content management platform.
                           This involves the discontinuation of updates  using Contribute to ensure that the
                           most current information is migrated to the  new platform. As a result, and in keeping
                           with prior announcements, access to  Contribute has been discontinued as planned as
                           of May 1st 2017.
                        </p>
                        
                        <p>Updates &nbsp;to departmental  websites are still available, and can be requested by submitting
                           a service  request (<a href="http://valenciacollege.edu/ServiceDesk">http://valenciacollege.edu/ServiceDesk</a> &nbsp;(most requests will be completed within  48 hours)
                        </p>
                        
                        <p>If you used Contribute daily or heavily during the week, contact  the Web &amp; Portal
                           Services team at&nbsp;<a href="mailto:webservices@valenciacollege.edu">webservices@valenciacollege.edu</a>&nbsp;so  that we may provide additional assistance.
                        </p>
                        
                        <p>Should you have any questions about this or require assistance,  please contact the
                           OIT Service Desk at&nbsp;<u><a href="mailto:OITServiceDesk@valenciacollege.edu">OITServiceDesk@valenciacollege.edu</a></u>,  or at extension 5555.
                        </p>
                        
                        <h2>Information Required</h2>
                        
                        <p>When you submit a ticket requesting changes to your website,  please make sure you
                           follow these steps to help us complete your request as  quickly as possible.&nbsp; Any
                           request lacking  the needed information will be returned to the requestor until the
                           information  is submitted.
                        </p>
                        
                        <h2>Updating/Adding a Document</h2>
                        
                        <ul>
                           
                           <li>URL of the page this document is linked from.&nbsp; </li>
                           
                           <li>URL of the current Document (if not new)</li>
                           
                           <li>Attach the new document</li>
                           
                           <ul>
                              
                              <li>Document file must be in the correct format (ie do  not attach a word document if
                                 you would like it posted as a PDF)
                              </li>
                              
                              <li>If you are replacing an existing document, you  should ONLY change the name of the
                                 file if the content of the file necessitates  the change (ie.&nbsp; Academic-Calendar-2016-2017.pdf
                                 is being replaced by Academic-Calendar-2017-2018).
                              </li>
                              
                              <li>Document file name should be only letters,  numbers &amp; dashes (-).&nbsp; Do not use  spaces
                                 in documents being posted to the web.
                              </li>
                              
                           </ul>
                           
                           <li>Title of the link (the words that are clicked on  the webpage) to the document.&nbsp; If
                              this  hasn’t changed, please tell us the current title of the link.
                           </li>
                           
                        </ul>
                        
                        <h2>Updating Page Content</h2>
                        
                        <ul>
                           
                           <li>
                              <strong>The most  important information you can give us is the URL of the page to be updated.</strong>&nbsp; Please do not describe where the page is or  refer to it as “my site” or “math site”.&nbsp;
                              Vague descriptions will increase the time to complete the request.&nbsp; Copy and paste
                              the URL from the web browser  when you are on the page you would like edited.&nbsp; <a href="../../%7Bsite%7D/%7Bpage.cfm%7D.html">http://valenciacollege.edu/{site}/{page.cfm}</a>
                              
                           </li>
                           
                           <li>If there is a time constraint, please let us  know when you submit the request.&nbsp; For
                              example, if you have information posted for a meeting happening today &amp; the  location
                              has been changed, let us know this up front.&nbsp; We will do our best to get time sensitive
                              updates done immediately.&nbsp; 
                           </li>
                           
                           <li><strong>Adding New Content</strong></li>
                           
                           <ul>
                              
                              <li>If you would like a new page added, you will  need to submit all the following</li>
                              
                              <ul>
                                 
                                 <li>URL of the site where the page should be added.</li>
                                 
                                 <li>Title of New Page</li>
                                 
                                 <li>Where the new page should be linked in the  Navigate section of your site.</li>
                                 
                                 <li>The fully edited content of your new page.&nbsp; Any changes to the new page will have
                                    to be  submitted as a new request and will go into the queue in the order in which
                                    it  was received.
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <li><strong>Updating Existing Content</strong></li>
                           
                           <ul>
                              
                              <li>
                                 <strong>Please make sure you submit ALL changes to the  page at once.</strong>&nbsp; If you make additional or  separate requests, they will be reprioritized or made
                                 into a new request.&nbsp; 
                              </li>
                              
                              <li>If you are updating part of a paragraph, copy  the whole paragraph, make your changes,
                                 then give instructions for updating.
                              </li>
                              
                              <ul>
                                 
                                 <li>Example:&nbsp; <br>
                                    Please update the 1st Paragraph on the page to read “Testing Centers  are available
                                    on each campus for you to take exams. Students are welcome to  visit any location
                                    they wish, IF their professor has allowed them the option to  test at any campus.”&nbsp;
                                    
                                 </li>
                                 
                              </ul>
                              
                              <li>If you are requesting added links or removed  links, please be specific </li>
                              
                              <ul>
                                 
                                 <li>Example:&nbsp; <br>
                                    Please Add a link to the page.&nbsp; Title: Select  Blinds Scholarship Link: <a href="http://www.selectblinds.com/scholarship.html">http://www.selectblinds.com/scholarship.html</a>
                                    
                                 </li>
                                 
                                 <li>Example:<br>
                                    Please remove the link on this page.&nbsp;  Title: Long Island Attorney Ronald D. Weiss
                                    Scholarship Contest Link: http://www.ny-bankruptcy.com/scholarship/
                                 </li>
                                 
                              </ul>
                              
                              <li>Changes to the header image will only be done if  there is an issue with the current
                                 picture. 
                              </li>
                              
                              <ul>
                                 
                                 <li>Example:<br>
                                    Please update the picture in the header.&nbsp; <em>Attach picture you would like to  use.</em>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <li>If you would like to remove content from the  site, please be specific.</li>
                              
                              <ul>
                                 
                                 <li>Example:<br>
                                    Please remove the text reading “Applications are open until January 15, 2017”
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <li><strong>Updating Hours/Location information</strong></li>
                           
                           <ul>
                              
                              <li>All Hour/Location information should be updated  through the “Update your Department’s
                                 Information” link in Atlas (on the  Employee page).
                              </li>
                              
                              <li>If your department’s Hour/Location information  does not exist, please make a request
                                 to create a new department.
                              </li>
                              
                              <ul>
                                 
                                 <li>Include the following</li>
                                 
                                 <ul>
                                    
                                    <li>Collegewide Name for the department</li>
                                    
                                    <li>Collegewide Hours (if they exist)</li>
                                    
                                    <li>Collegewide Phone Number (if it exists)</li>
                                    
                                    <li>Collegewide Email (if it exists.</li>
                                    
                                    <li>URL of your Website</li>
                                    
                                    <li>Keywords</li>
                                    
                                    <li>Campus</li>
                                    
                                    <li>Campus Location</li>
                                    
                                    <li>Campus Phone Numbers</li>
                                    
                                    <li>Campus Hours</li>
                                    
                                    <li>Campus Department Email</li>
                                    
                                    <li>Campus Department Website URL.</li>
                                    
                                 </ul>
                                 
                                 <li>The developer will then work with you to get  hour department/office Hours &amp; Location
                                    information on your web page(s).
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web/updates.pcf">©</a>
      </div>
   </body>
</html>