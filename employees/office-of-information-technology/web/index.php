<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web &amp; Portal Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/web/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/web/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Web &amp; Portal Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Web</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        
                        <p>Web &amp; Portal services builds and maintains Web &amp; Portal sites for the college departments.
                           We also maintain Atlas, which is the college's Luminis portal. 
                        </p>
                        
                        <h3>Faculty Web Space </h3>
                        Web space is available for faculty and adjuncts.            The Web team has designed
                        a place on the Valencia Web site for faculty to maintain a Web presence. It is called
                        "Faculty Frontdoor." This is a user friendly, form-based application that allow faculty
                        to post announcements, syllabi, documents, images and even video. To get started,
                        follow the steps below:
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Faculty Frontdoor</h3>
                                    
                                    
                                    <ul>
                                       
                                       <li>Sign in to <a href="https://atlas.valenciacollege.edu/">Atlas</a>
                                          
                                       </li>
                                       
                                       <li>Faculty tab &amp;gt; Faculty Tools channel &amp;gt; Faculty Frontdoor</li>
                                       
                                       <li>Sample Web Address:<br>
                                          http://frontdoor.valenciacollege.edu/?aSmith
                                       </li>
                                       
                                    </ul>
                                    
                                    
                                    <p><a href="../../faculty/development/centers/learn-it/frontdoor.html">Help with Faculty Frontdoor</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Adding information to the college site </h3> <br>
                        If you have anything you would like to add or change on the college Web site, please
                        send your information and suggestions to <a href="mailto:webservices@valenciacollege.edu">Web Services</a>.
                        
                        <p>Files in the following formats may be sent as attachments: PDF, Word,  Excel, &amp; PowerPoint,
                           JPEG, GIF and PNG. Please check with Web Services if you have any                
                           other file type. 
                        </p>
                        
                        <h3>Timelines</h3>
                        <br>
                        We  aim to add new information to the college Web site  quickly. However, at certain
                        times this may not be possible. We will do  everything we can to assist if you need
                        to add material to the  site  urgently, but please speak to  us in-person or by telephone
                        first (ext.  5529).
                        
                        <h3>Web Standards</h3>
                        <em> <br>
                           </em>Web pages  which do not conform to the current college visual identity cannot be 
                        displayed on the Web site or the portal.
                        
                        <h3>Online Courses</h3>
                        <br>
                        
                        Online Courses are available for any member of staff who wishes to create their own
                        course pages. It is currently managed by the <a href="../learning-technology-services/index.html">Learning Technology Services department</a>. 
                        
                        <p><a href="index.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/web/index.pcf">©</a>
      </div>
   </body>
</html>