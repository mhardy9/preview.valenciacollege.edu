<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office of Information Technology (OIT) | Valencia College</title>
      <meta name="Description" content="OIT employs technology to create, communicate, and apply knowledge that facilitates learning and supports the mission and goals of Valencia College.">
      <meta name="Keywords" content="office of information technology, oit, eas, wps, nis, web and portal services, enterprise data solutions, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office of Information Technology (OIT)</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li>Office Of Information Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="row">
                        
                        <div class="col-md-9">
                           
                           <div class="box_style_1">
                              
                              <div class="indent_title_in">
                                 
                                 <h3>
                                    Office of Information Technology (OIT)
                                    
                                 </h3>
                                 
                                 <p>
                                    Welcome to the Valencia College Office of Technology
                                    
                                 </p>
                                 
                              </div>
                              
                              <div class="wrapper_indent">
                                 
                                 <p>
                                    OIT employs technology to create, communicate, and apply knowledge that facilitates
                                    learning and supports
                                    the mission and goals of Valencia College.
                                    
                                 </p>
                                 
                                 <p>
                                    Principles:
                                    
                                 </p>
                                 
                                 <p>
                                    
                                 </p>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Enhance learning activities and instructional support through the effective use of
                                       technology
                                    </li>
                                    
                                    <li>Maintain a reliable, robust and secure technology environment</li>
                                    
                                    <li>Balance innovation, manageability and use of college resources through careful planning
                                       and
                                       stewardship
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                              <hr class="styled_2">
                              
                              <div class="indent_title_in">
                                 
                                 <h3>
                                    IT Policies &amp; Procedures
                                    
                                 </h3>
                                 
                                 <p>
                                    Valencia College Office of Policy and General Counsel
                                    
                                 </p>
                                 
                              </div>
                              
                              <div class="wrapper_indent">
                                 
                                 <p>
                                    
                                 </p>
                                 
                                 <div class="row">
                                    
                                    <div class="col-md-6">
                                       
                                       <ul class="list_style_1">
                                          
                                          <li><a href="/about/general-counsel/policy/">Information
                                                Technology Resource Policies</a></li>
                                          
                                          <li>
                                             <a href="/about/general-counsel/policy/documents/7A-02-Acceptable-Use-of-Information-Technology-Resources.pdf">Acceptable
                                                Use of Information Technology Resources</a>
                                             
                                          </li>
                                          
                                          <li>
                                             <a href="/about/general-counsel/policy/documents/7A-03-Computer-Hardware-and-Software-Standards.pdf">Computer
                                                Hardware and Software Standards</a>
                                             
                                          </li>
                                          
                                          <li>
                                             <a href="/about/general-counsel/policy/documents/7A-04-Online-Privacy-Access-and-Security.pdf">Online
                                                Privacy, Access and Security</a>
                                             
                                          </li>
                                          
                                          <li>
                                             <a href="/about/general-counsel/policy/documents/7A-05-User-Authentication-Requirements.pdf">User
                                                Authentication Requirements</a>
                                             
                                          </li>
                                          
                                          <li>
                                             <a href="/employees/office-of-information-technology/documents/ITP01A1-PASS-022502.pdf">Guidelines for Selecting a Secure Password</a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    <div class="col-md-6">
                                       
                                       <ul class="list_style_1">
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <hr class="styled_2">
                              
                              <div class="indent_title_in">
                                 
                                 <h3>
                                    Secure Links
                                    
                                 </h3>
                                 
                                 <p>
                                    Faculty &amp; Staff
                                    
                                 </p>
                                 
                              </div>
                              
                              <div class="wrapper_indent">
                                 
                                 <p>
                                    
                                 </p>
                                 
                                 <div class="row">
                                    
                                    <div class="col-md-6">
                                       
                                       <ul class="list_style_1">
                                          
                                          <li><a href="https://mailvalenciacc.sharepoint.com/banner/SitePages/OPROD.aspx">Banner Login</a></li>
                                          
                                          <li><a href="http://webmail.valenciacollege.edu/" target="_blank">Email: Outlook</a></li>
                                          
                                          <li><a href="http://oit.valenciacollege.edu/prs/" target="_blank">Project Request System</a></li>
                                          
                                          <li><a href="/employees/office-of-information-technology/techgov/documents/Valencia_Technology_Governance.pdf" class="icon_for_pdf">Technology
                                                Governance Strucuture Graphic</a></li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    <div class="col-md-6">
                                       
                                       <ul class="list_style_1">
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <aside class="col-md-3">
                           
                           <div class="banner">
                              
                              <h3>
                                 HELP DESK SUPPORT
                                 
                              </h3>
                              
                              <p>
                                 Contact the Help Desk to place a Support Ticket
                                 
                              </p>
                              <a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new" class="banner_bt">Click here</a>
                              
                           </div>
                           
                           <hr class="styled">
                           
                           <div class="box_side">
                              
                              <h5>
                                 Need OIT Support?
                                 
                              </h5>
                              <i class="icon-phone"></i>
                              
                              <p>
                                 Call Extension <a href="tel://4075825555">5555</a><br>
                                 <small>Monday to Friday 9.00am - 6.00pm</small>
                                 
                              </p>
                              
                           </div>
                           
                        </aside>
                        
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/index.pcf">©</a>
      </div>
   </body>
</html>