<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Networking | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/mail.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Networking</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/networking/">Networking</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2><img alt="O365 Logo" height="63" src="LogoOffice365-2_000.jpg" width="250"></h2>
                        
                        <h2>
                           <a href="https://atlas.valenciacollege.edu/">Email Login</a> 
                        </h2>
                        
                        
                        <blockquote>
                           
                           <blockquote>
                              
                              <blockquote>
                                 
                                 <blockquote>
                                    
                                    <p><img alt="Collaborate" height="234" hspace="0" src="LogoCollaborate.jpg" vspace="0" width="500"></p>
                                    
                                 </blockquote>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                        </blockquote>            
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <h3>Atlas Email</h3>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong></strong></p>
                        
                        <p>The Atlas email account (yourname@mail.valenciacollege.edu) provides faculty, staff,
                           &amp; students  with a  mailbox, calendaring, collaboration, blogging, photo sharing,
                           event planning, instant messaging tools and much more! 
                        </p>
                        
                        
                        <p><strong>Top Features</strong></p>
                        
                        <blockquote>
                           
                           <p><strong><img alt="O365 features" height="149" src="Logoall-2.jpg" width="620"></strong></p>
                           
                        </blockquote>            
                        
                        
                        <h3>Basic Client Settings for  Student Email Account</h3>
                        
                        <p><strong></strong></p>
                        
                        <p><strong>For the Outlook Client (2007, 2010 &amp; 2013) or Outlook Express, use the control panel
                              in Windows to add a new “Profile”</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>OS includes, but not limited to: Windows 8, Windows 7, Windows XP.</strong><strong> </strong>
                              
                              <ul>
                                 
                                 <li>
                                    <strong>Enter you email address,&nbsp; </strong><a href="mailto:username@mail.valenciacollege.edu">username@mail.valenciacollege.edu</a><strong> </strong>
                                    
                                 </li>
                                 
                                 <li><strong>Enter your Atlas password</strong></li>
                                 
                                 <li><strong>Auto-discovery should find and connect to your account</strong></li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Spam Portal</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><br>
                           
                        </p>
                        
                        <p><a href="https://admin.protection.outlook.com/quarantine"><img alt="Spam-Logo" border="0" height="80" src="LogoOffice365-1_000.jpg" width="300"></a></p>
                        
                        
                        
                        <p><a name="ForwardEmail" id="ForwardEmail"></a></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <h3>Forwarding Office 365 email </h3>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3><span>Settings for Redirect Office 365 Atlas email to Outlook.</span></h3>
                        
                        
                        <p> <img alt="Forward1a" height="706" src="ForwardSetting1_000.jpg" width="530"></p>
                        
                        
                        <p><img alt="FormatEmail3b" height="758" src="ForwardSetting2_000.jpg" width="530"></p>
                        
                        <p><strong>Download the PDF document </strong> <a href="documents/LoginintoyourAtlasemail.pdf"><img alt="Forward Doc" border="0" height="38" src="LogoOffice2013-2.jpg" width="35"></a></p>
                        
                        
                        <p><a name="FilterEmail" id="FilterEmail"></a></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <h3>Filter Office 365 email </h3>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3><span>Settings for filtering Office 365 Atlas in the Deleted Items folder.</span></h3>
                        
                        
                        <p> <img alt="Forwarding3" height="801" src="ForwardSetting3.jpg" width="475"></p>
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <h3>SPAM</h3>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>
                           <span>What is OIT doing about 
                              spam?</span><img alt="Barracuda Server" height="62" src="Barracudaserver.jpg" width="91">
                           
                        </h3>
                        
                        <ul>
                           
                           <li><a href="http://netview.valenciacollege.edu/spamstats/combined_barracuda_stats.php">Real 
                                 Time SPAM statistics for Valencia </a></li>
                           
                        </ul>
                        
                        
                        <p><strong>Block 
                              Known Spammers
                              </strong><br>
                           We block known spammers from 
                           sending email to Valencia. We reject connections from mail servers 
                           that are listed on a particular set of "Remote Block Lists" (RBLs). 
                           This list is maintained by a widely respected non-profit organization. 
                           Anyone sending from a RBL listed server will not be able to sent 
                           to Valencia and will receive a "failed delivery" message. This message 
                           also includes instructions about how to get off the list. If someone 
                           has been improperly listed, he will be made aware of the problem 
                           and have an opportunity to fix it. 
                        </p>
                        
                        <p><strong>Greylisting</strong><br>
                           One technique which has seen 
                           great success is known as "greylisting". This mechanism causes senders 
                           to delay their messages in a manner which complies with accepted 
                           email standards. Spammers generally are unable to cooperate, and 
                           therefore their messages are not accepted. 
                        </p>
                        
                        <p>As a result of our greylisting 
                           efforts, you may notice some delay in receiving expected messages, 
                           particularly in the period immediately following activation. Most 
                           of the time, these delays should be minimal, and they should only 
                           apply to the first message you receive from a particular correspondent. 
                           Please let us know if you experience any lengthy delays in received 
                           expected messages. 
                        </p>
                        
                        <p><strong>Spam Filtering</strong><br>
                           OIT checks incoming email 
                           to try to determine if it is spam. We have two methods to help you 
                           filter spam from your Inbox into a separate folder. No automated 
                           technique can determine with 100% accuracy if a message is spam. 
                           Check your spam folder regularly for legitimate messages that were 
                           incorrectly marked as spam, as well as to delete the actual spam.            
                        </p>
                        
                        
                        <p><a href="../../office-of-information-technology/networking/email.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/mail.pcf">©</a>
      </div>
   </body>
</html>