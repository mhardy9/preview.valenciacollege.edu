<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Networking | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/network-security.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Networking</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/networking/">Networking</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Network Security</h2>
                        
                        <p> <img alt="" border="0" height="60" src="dshield_live_banner.png" usemap="#DSMap0" width="468"> 
                        </p>
                        
                        
                        <p>Do you have a question about IT security at Valencia 
                           Community College?&nbsp; Email <a href="mailto:oit-security@valenciacollege.edu"> oit-security@valenciacollege.edu</a> with your question, and 
                           we'll answer it.
                        </p>
                        
                        <h3>Introduction</h3>
                        
                        <p>Computer security is the process of preventing and detecting 
                           unauthorized use of your computer or our computer resources.&nbsp; 
                           Prevention measures help you to stop unauthorized users (also 
                           known as “intruders”) from accessing any part of your computer 
                           system.&nbsp; Detection helps you to determine whether or not someone 
                           attempted to break into your system, if they were successful, 
                           and what they may have done.
                        </p>
                        
                        <p>We use computers for everything from banking and investing to 
                           shopping and communicating with others through email or chat 
                           programs.&nbsp; Although you may not consider your communications 
                           “top secret,” you probably do not want strangers reading your 
                           email, using your computer to attack other systems, sending 
                           forged email from your computer, or examining personal 
                           information stored on your computer.&nbsp;
                        </p>
                        
                        <p>The resources and articles either on this site, or linked to 
                           by this site, will allow you to do your part in keeping our 
                           systems from being compromised, or used in an inappropriate 
                           manner.&nbsp; 
                        </p>
                        
                        <p>Latest Microsoft Alerts: <a href="http://seclists.org/rss/microsoft.rss">RSS feed</a> 
                        </p>
                        
                        <p>Latest from BugTRAQ: <a href="http://seclists.org/rss/bugtraq.rss">RSS feed</a> <br>Latest from CERT-US: <a href="http://seclists.org/rss/cert.rss">RSS feed</a> 
                        </p>
                        
                        
                        <h3>Patching your Operating System and Updating Virus Definitions at least once a week
                           is the best protection against becoming a victim 
                        </h3>
                        
                        
                        <p><strong>Click here to access the <a href="../../office-of-information-technology/networking/rulesofRoad.html"> Valencia Rules of the Road</a> and important Information Security Links</strong></p>
                        
                        <p><a href="../../office-of-information-technology/networking/security.html#top"></a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p> 
                           
                           
                        </p>
                        
                        <noscript>
                           
                        </noscript>
                        
                        
                        <noscript>
                           
                        </noscript>
                        
                        
                        <noscript>
                           
                        </noscript>
                        
                        
                        
                        
                        <p><a href="../../office-of-information-technology/networking/security.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/network-security.pcf">©</a>
      </div>
   </body>
</html>