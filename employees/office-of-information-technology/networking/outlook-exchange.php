<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Networking | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/outlook-exchange.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Networking</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/networking/">Networking</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Outlook on Exchange</h2>
                        
                        <div>
                           <img alt="Outlook 2007" height="150" hspace="0" src="logooutlook-2016.jpg" vspace="0" width="200"><br>
                           
                        </div>
                        
                        <p>Welcome to the Microsoft 
                           Outlook installation and Support page. Microsoft Outlook includes many features 
                           (calendar, contacts, notes, tasks) and integrations with other applications (Word,
                           Excel). 
                           In addition, Outlook also has improved security, spam handling and virus 
                           scanning.
                        </p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Outlook Self-Help</p>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><strong> <a href="https://support.office.com/en-us/article/Office-Training-Center-b8f02f81-ec85-4493-a39b-4c48e6bc4bfb">Office Training Center - Click Now</a> </strong></li>
                                          
                                          <li>Setting up an Out of Office message couldn’t be easier! And, the really good news
                                             is that by using the date and start feature (just click – chose the dates – click
                                             again!) you do not need to remember to turn it off when you come back in. 
                                          </li>
                                          
                                          <li><a href="https://support.office.com/en-us/article/Send-automatic-Out-of-Office-replies-from-Outlook-2013-or-Outlook-2016-2b77364d-21ed-4149-b913-4f150b738047?ui=en-US&amp;rs=en-US&amp;ad=US">Click here to learn how to use the Out of Office Assistant </a></li>
                                          
                                          <li>
                                             
                                          </li>
                                          <li>
                                             <u>** Voice Mail is now in Outlook </u><u>**</u>
                                             
                                          </li>
                                          
                                          <li> <strong>Microsoft Unified Messaging</strong> is a subcomponent of Microsoft Outlook Email and provides several new features and
                                             enhancements, such as the ability to receive and listen to voicemails directly from
                                             your Outlook email inbox. See the QuickStart guide below. 
                                          </li>
                                          
                                          <li>                          
                                             <div>                            
                                                <hr>
                                                <strong>Guides and FAQs:</strong>
                                                
                                             </div>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> Outlook</strong> PDF guide click <a href="documents/OUTLOOK2016QUICKSTARTGUIDE_000.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart<u> 2016</u> Outlook Mac</strong> PDF guide click <a href="documents/OUTLOOK2016FORMACQUICKSTARTGUIDE_000.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>
                                             <strong>QuickStart<u> 2016</u> OneNote</strong> PDF guide click <a href="documents/ONENOTE2016QUICKSTARTGUIDE_001.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> OneNote Mac </strong> PDF guide click <a href="documents/ONENOTE2016FORMACQUICKSTARTGUIDE_000.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> Word</strong> PDF guide click <a href="documents/WORD2016QUICKSTARTGUIDE_000.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> Word Mac</strong> PDF guide click <a href="documents/WORD2016FORMACQUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> Excel</strong> PDF guide click <a href="documents/EXCEL2016QUICKSTARTGUIDE_000.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> Excel Mac</strong> PDF guide click <a href="documents/EXCEL2016FORMACQUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> PowerPoint</strong> PDF guide click <a href="documents/POWERPOINT2016QUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> PowerPoint Mac</strong> PDF guide click <a href="documents/POWERPOINT2016FORMACQUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> OneNote Mobile</strong> PDF guide click <a href="documents/ONENOTEMOBILEQUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> Word Mobile</strong> PDF guide click <a href="documents/WORDMOBILEQUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> Excel Mobile</strong> PDF guide click <a href="documents/EXCELMOBILEQUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>2016</u> PowerPoint Mobile</strong> PDF guide click <a href="documents/POWERPOINTMOBILEQUICKSTARTGUIDE.PDF" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart Unified Messaging Outlook </strong> PDF guide click <a href="documents/UnifiedMessagingQuickStart.pdf"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>
                                             <br>
                                             
                                             <hr>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    <p>Training and Info</p>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <strong>Training Information: </strong> 
                                          
                                          <li>Look for upcoming monthly webinars on selected Outlook "how to" topics. </li>
                                          
                                          <li>Find out more at the <a href="../../employees/development/edge/index.html" target="_blank"><strong> Valencia EDGE </strong></a> website!
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Outlook Client </p>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><strong>To install:</strong></li>
                                          
                                          <li><strong>Login to Atlas and connect to Office365, on the Home Page click on "Install Now" </strong></li>
                                          
                                          <li>
                                             
                                          </li>
                                          <li>
                                             
                                             <hr>
                                             
                                          </li>
                                          
                                          <li>To access <strong>Outlook off campus</strong> browse to <a href="https://webmail.valenciacollege.edu/" target="_blank">https://webmail.valenciacollege.edu</a> and access Outlook Web Access (OWA).
                                          </li>
                                          
                                          <li><em>Archives are NOT accessible in OWA</em></li>
                                          
                                          <li>If you need to access a Resource Account via OWA click on the down arrow next to your
                                             name (upper right) and type in the Resource Account name. 
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    <p>Skype for Business</p>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li>
                                             <strong>QuickStart <u>Audio</u></strong> PDF guide click <a href="documents/SkypeforBusinessQuickStartGuide-AudioSetupandMakingCalls.pdf" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>
                                             <strong>QuickStart <u>Contacts</u></strong> PDF guide click <a href="documents/SkypeforBusinessQuickStartGuide-ContactsPresenceandIM.pdf" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>Meetings</u></strong> PDF guide click <a href="documents/SkypeforBusinessQuickStartGuide-Meetings.pdf" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>Sharing</u></strong> PDF guide click <a href="documents/SkypeforBusinessQuickStartGuide-SharingandCollaboration.pdf" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             <strong>QuickStart <u>Video</u></strong> PDF guide click <a href="documents/SkypeforBusinessQuickStartGuide-Video.pdf" target="_blank"><strong>here</strong></a>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                          </li>
                                       </ul>
                                       
                                       
                                       <ul>
                                          
                                          <li>
                                             
                                          </li>
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <hr size="2" width="100%">
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><img alt="PDF Icon" border="0" height="31" src="pdfmidsz.gif" width="28"></div>
                                 
                                 <div>In order to view PDF files, you will need Adobe Reader which is available 
                                    for free from the <a href="http://get.adobe.com/reader/" target="_blank">Adobe</a> 
                                    web site. 
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <div><a href="http://get.adobe.com/reader/" target="_blank">
                                          <img alt="Get Adobe Acrobat Reader" border="0" height="31" src="getacro.gif" width="88"></a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="../../office-of-information-technology/networking/outlookexchange.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/outlook-exchange.pcf">©</a>
      </div>
   </body>
</html>