<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Networking | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/rules-of-the-road.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Networking</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/networking/">Networking</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Rules 
                           of the Road
                        </h2>
                        
                        <p><strong>Access to Information 
                              Resources</strong><br>
                           All personnel (faculty, staff, and students) must acquire a user 
                           id and pass word before accessing Valencia College information 
                           resources. This access is approved by representatives of the information 
                           resource owner and processed through the Office of Information Technology. 
                           This access is reviewed at least annually to ensure that it is still 
                           valid.&nbsp; When the access is no longer valid, the Office of Information 
                           Technology terminates it.
                        </p>
                        
                        <p><strong>Access to Confidential/Sensitive 
                              Information</strong><br>
                           Access to confidential and sensitive information is restricted to 
                           individuals who are authorized by owners on a strict "need 
                           to know" basis. Owners of confidential and /or sensitive information 
                           are responsible for classifying the data. Resources containing this 
                           information should be clearly marked. Managers should be aware of 
                           which personnel are in positions of special trust.
                        </p>
                        
                        <p><strong>Use of Valencia Community 
                              College Information Resources</strong><br>
                           Valencia College information resources provided to faculty, 
                           staff, and students are owned by Valencia College and 
                           are to be used for College related activities only. Resources are 
                           not to be used for commercial purposes and/or personal financial 
                           gain or for any other non-university-related activities. When off-site 
                           Valencia College terminals are used to access University 
                           information resources, their operation must be only for university 
                           business. At no time should Valencia College information 
                           resources be used to disrupt, damage, degrade, or obstruct systems/networks. 
                           University resources should not be used to gain unauthorized access 
                           to systems, subvert restrictions associated with computer accounts, 
                           or obtain additional resources. Users are responsible for their 
                           own individual accounts and should never make them available to 
                           others. User IDs and passwords exist for account protection and 
                           should not be compromised. 
                        </p>
                        
                        <p><strong>Email</strong><br>
                           Email should be considered the same as printed communication and 
                           should meet the same standards of taste, professionalism, accuracy 
                           and legality that are expected in printed communication. Fraudulent, 
                           harassing, threatening or libelous messages should not be transmitted 
                           and personal or sensitive information about individuals should not 
                           be posted without their consent. Junk mail (e.g. random mail, chain 
                           letters, etc.) is unacceptable. All messages must correctly identify 
                           the sender. If <u>you</u> are being harassed via email, immediately 
                           notify the Office of Information Technology. It is not the intent 
                           of the University to monitor email in the absence of policy or law 
                           violations. Email <u>is</u> subject to disclosure to third parties 
                           under the Florida Sunshine Law or through subpoena or other processes.
                        </p>
                        
                        <p><strong>Copyright</strong><br>
                           Copyrighted material includes, but is not limited to, software, 
                           audio recordings, video recordings, photographs and written material. 
                           Copyright policy can be complex and fairly confusing but the following 
                           general guidelines apply to Valencia College:
                        </p>
                        
                        <p>Software licensed to the 
                           university may be copied only if explicitly authorized by the software 
                           developer.
                        </p>
                        
                        <p>Copying software licensed 
                           to the University onto a <u> personal</u> home computer must be 
                           expressly authorized in the license agreement. 
                        </p>
                        
                        <p>Multiple use over a network 
                           must also be specifically authorized in the license agreement. 
                        </p>
                        
                        <p>Users must have permission 
                           to access, copy, or transport data maintained by other University 
                           users. 
                        </p>
                        
                        <p>Faculty, staff, and students 
                           may not use unlicensed or unauthorized copies of software on University-owned 
                           computers. 
                        </p>
                        
                        <p>Please refer to the <a href="../../office-of-information-technology/about/policiesProcedures.html">
                              Valencia College IT policies</a> for more information.
                        </p>
                        
                        <p> <span> 
                              <a href="../../office-of-information-technology/networking/rulesofRoad.html#top">TOP</a></span></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/rules-of-the-road.pcf">©</a>
      </div>
   </body>
</html>