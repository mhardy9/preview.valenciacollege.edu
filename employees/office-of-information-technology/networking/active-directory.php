<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Networking | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/active-directory.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Networking</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/networking/">Networking</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Active 
                           Directory &amp; Exchange Outlook Network Services FAQ's 
                        </h2>
                        
                        
                        <p><strong><span><strong>What is Office 365?</strong></span></strong></p>
                        
                        <p>Microsoft Office 365 for Education is a cloud-based communication system which provides
                           not only internet email, but also 25 GB email storage, personal and shared calendars,
                           plus a collaboration tool called Lync which gives you real-time chat capabilities
                           to communicate with students. You will see the following changes in Atlas: 
                        </p>
                        
                        <ul>
                           
                           <li>Clicking on the Email icon in Atlas will sign you in to your Office 365 account. Sign
                              into Atlas <a href="https://atlas.valenciacollege.edu/">here</a>
                              
                           </li>
                           
                        </ul>                                    
                        
                        
                        <ul>
                           
                           <li>You can also login directly to Office 365 with your username@mail.valenciacollege.edu
                              account here <a href="https://login.microsoftonline.com/"><img alt="O365 Logo" border="0" height="38" src="LogoOffice365-2.jpg" width="150"></a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        <p><strong><span><strong>What is Microsoft Unified Messaging?</strong></span></strong></p>
                        
                        <p> A subcomponent of the Microsoft Outlook Email Client and provides several features
                           and enhancements, such as the ability to receive and listen to voicemails directly
                           from your Outlook email inbox.
                        </p>
                        
                        <blockquote>
                           
                           <p><u></u><strong>You will need to record your personal voicemail greeting.<br>
                                 </strong>A system generated generic message will be available until you have recorded your
                              new personal greeting and instructions will be provided to you on how to record a
                              new greeting.
                           </p>
                           
                           <p><strong>There will be new numeric keypad options to access your voicemail.<br>
                                 </strong>Click this Valencia Answers link: <a href="http://answers.valenciacollege.edu/ics/support/default.asp?deptID=5572&amp;task=knowledge&amp;questionID=1315">http://answers.valenciacollege.edu/ics/support/default.asp?deptID=5572&amp;task=knowledge&amp;questionID=1315</a> to get a head start on learning the new commands.
                           </p>
                           
                        </blockquote>                
                        
                        
                        <p><strong>What is Active Directory?</strong></p>
                        
                        <p> Active Directory 
                           (AD) is the centralized database of user accounts, groups, computers, 
                           and other resources that comprise a Windows Server based network 
                           environment.
                        </p>
                        
                        <p> The purpose 
                           of this project is to create a centrally managed infrastructure 
                           to maintain and secure Microsoft Windows servers and desktops throughout 
                           all campuses. This infrastructure will provide centralized authentication, 
                           authorization, and management services for Windows-based computers 
                           and applications.
                        </p>
                        
                        <p> <strong>What is my I: Drive?</strong></p>
                        
                        <p>Every full 
                           time faculty and staff member has what is called the "I: Drive", 
                           also known as a home drive or home folder. This is space allotted 
                           to you on the Valencia AD Network for you to store business related 
                           files and documents. Anytime you login to a MS AD Window-based computer, 
                           your personal I: Drive will automatically be available for you to 
                           access.
                        </p>
                        
                        
                        <p><strong>What is the S: Drive?</strong></p>
                        
                        <p>Every full 
                           time faculty and staff member has what is called the"S: Drive", 
                           also known as the common drive, shared drive or shared folders. There are departmental
                           common folders designated as [Department]-Common" which everyone in that department
                           group has access to these shared files and Global Shares which only the people specified
                           or requested has access to these files allowing for secured file sharing, these are
                           designated as GS-[shared name].
                        </p>
                        
                        <p>To make data sharing 
                           easier and more cohesive, all shared folders will be on the S: Drive 
                           along with the common folders, [department]-Common 
                           and global shares GS-[share name].
                        </p>
                        
                        <p> This is 
                           space allotted to you on the Valencia AD Network for you to share 
                           business related files and documents with your department or groups 
                           throughout the college. Anytime you login to a MS AD Window-based 
                           computer, the S: Drive will automatically be available for you to 
                           access. 
                        </p>
                        
                        
                        
                        
                        
                        <p><a href="../../office-of-information-technology/networking/activedirectory.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/active-directory.pcf">©</a>
      </div>
   </body>
</html>