<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Networking | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/passwords.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Networking</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/networking/">Networking</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2><strong><font color="#3333FF" size="4">Password Security Update</font></strong></h2>
                        
                        
                        <p>Because of the increase in security awareness everywhere it has been determined that
                           Valencia faculty, staff and students are required to change their network password
                           every 180 days. There is a certain complexity to the password that you pick to make
                           sure it is a secure password and not easily guessed by others. Here is what will be
                           required: 
                        </p>
                        
                        
                        <ul>
                           
                           <li>All passwords are required to meet the “strong password” definition.</li>
                           
                           <li>Passwords must be at least eight characters in length.</li>
                           
                           <li>Passwords must consist of a combination of three (3) of the following four (4) categories:
                              Upper case letters, Lower case letters, Numerals and special characters. 
                           </li>
                           
                           <li>Passwords cannot contain a dictionary word.</li>
                           
                           <li>Passwords cannot contain all or part of your user id.</li>
                           
                           <li>Passwords cannot match any of your ten previous passwords.</li>
                           
                        </ul>
                        
                        
                        <p> This will only be required every 180 days (6 months). You will start to get  warnings
                           prompting you to change your password 14 days before it is due and each day until
                           it does expire. If you do not change the password before it expires then you will
                           be forced to change it before logging in. Should you need assistance please contact
                           the Helpdesk for assistance at x5555 or by email <a href="mailto:oithelp@valenciacollege.edu">oithelp@valenciacollege.edu</a> 
                        </p>
                        
                        
                        
                        
                        <p>*********************************************************************************</p>
                        
                        
                        
                        <p><strong>Password Procedure</strong></p>
                        
                        <p><strong></strong> 
                        </p>
                        
                        <p><a name="Policy_Statement" id="Policy_Statement"></a><strong>PROCEDURE &nbsp;STATEMENT</strong> 
                        </p>
                        
                        <p>This procedure establishes a standard for creation of strong passwords and protection
                           of those passwords within the Microsoft Active Directory/Exchange email system. This
                           procedure applies to all persons who have, or are responsible for, an account on any
                           system accessed on the Valencia College network or computer systems. 
                        </p>
                        
                        
                        <p><a name="Definitions" id="Definitions"></a><strong>DEFINITIONS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong> 
                        </p>
                        
                        <p><u>Expiration:</u> Date at which password for access to College systems is required to be changed meeting
                           strong password standards.
                        </p>
                        
                        <p><a name="PROCEDURES_and_RESPONSIBILITIES" id="PROCEDURES_and_RESPONSIBILITIES"></a><strong>PROCEDURES and RESPONSIBILITIES</strong> 
                        </p>
                        
                        <p>Users are responsible for assisting in the protection of the network and computer
                           systems they use. The integrity and secrecy of an individual's password is a key element
                           of that responsibility. Each individual has the responsibility for creating and securing
                           an acceptable password per this procedure. Failure to conform to these restrictions
                           may lead to the suspension of rights to College systems or other action as provided
                           by College Policy, State or Federal law. 
                        </p>
                        
                        <p><strong>Password Creation Rules</strong>: 
                        </p>
                        
                        <p>Passwords are initially assigned when a new account is created. Upon initial logon
                           users have the right and the ability to change passwords on their Active Directory/Exchange
                           accounts at any time. Users will be prompted to change their password the first time.
                           
                        </p>
                        
                        <ul>
                           
                           <li>All passwords are required to meet the “strong password” definition. </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>Passwords must be at least eight characters in length.</li>
                           
                           <li>Passwords must consist of a combination of three (3) of the following four (4) categories:
                              Upper case letters, Lower case letters, Numerals and special characters. 
                           </li>
                           
                           <li>Passwords cannot contain a dictionary word.</li>
                           
                           <li>Passwords cannot contain all or part of your user id.</li>
                           
                           <li>Passwords cannot match any of your ten previous passwords.</li>
                           
                        </ul>
                        
                        <p><strong>Password Expiration:</strong> 
                        </p>
                        
                        <p>Passwords will expire on a 180-day cycle (6 months). Advance warnings of upcoming
                           password expiration will be sent to the designated account holder via campus e-mail
                           beginning 30 days prior to expiration, with repeated reminders several times thereafter
                           until the expiration date. 
                        </p>
                        
                        <p>Any account holder may change his or her password at any time through their Active
                           Directory Account or <a href="https://adpass.valenciacollege.edu">https://adpass.valenciacollege.edu</a> (an online password reset utility). It is not necessary to wait for expiration. 
                        </p>
                        
                        <p>Passwords should be changed immediately and the Office of Information Technology notified
                           whenever there is a belief that the password has been compromised. 
                        </p>
                        
                        
                        <p><strong>Desktop/Laptop&nbsp; Password Security</strong></p>
                        
                        <p>All Desktops and Laptops must have a password enabled screensaver &nbsp;and timeout set
                           to no more than 5 minutes.
                        </p>
                        
                        <p>NOTE: Password changes will also affect Smartphone’s and PDA’s that check Outlook
                           emails. User will need to change this manually.
                        </p>
                        
                        
                        
                        
                        
                        
                        <p>Version: sec-100603.07</p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/passwords.pcf">©</a>
      </div>
   </body>
</html>