<ul><li><a href="/employees/office-of-information-technology/networking/">Network and Infrastructure Services</a></li>
      <li class="submenu">
<a href="#">Networking Resources <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
              <ul>
                 <li><a href="/employees/office-of-information-technology/networking/outlook-exchange.php">Outlook on Exchange</a></li>
                <li><a href="/employees/office-of-information-technology/networking/active-directory.php">Active Directory</a></li>
                <li><a href="/employees/office-of-information-technology/networking/mail.php">Mail</a></li>
                  <li><a href="/employees/office-of-information-technology/networking/network-security.php">Network Security</a></li>
                    <li><a href="/employees/office-of-information-technology/networking/passwords.php">Passwords</a></li>
                      <li><a href="/employees/office-of-information-technology/networking/rules-of-the-road.php">Rules of the Road</a></li>
                        <li><a href="/employees/office-of-information-technology/networking/telecommunications.php">Telecommunications</a></li>
                    
              </ul>
            </li>	
	
	
<li><a href="/employees/office-of-information-technology/networking/wifi.php">Wifi</a></li>
</ul>