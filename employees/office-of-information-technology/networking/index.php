<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>(NIS) Network and Infrastructure Services - Valencia College | Valencia College</title>
      <meta name="Description" content="add">
      <meta name="Keywords" content="office of information technology, oit, eas, wps, nis, web and portal services, enterprise data solutionscollege, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="networking.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Networking
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Networking
                                    
                                 </h3>
                                 
                                 <p>
                                    Valencia is upgrading their Exchange On-Premise Storage to Exchange Online. This new
                                    storage will
                                    give each user 50 gb of mail quota, which removes the need to archive email or run
                                    out of space.
                                    There are a couple of changes you'll need to be aware of after the migration
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="networking.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="outlook-exchange.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Outlook on Exchange
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Outlook on Exchange
                                    
                                 </h3>
                                 
                                 <p>
                                    Microsoft Outlook installation and Support page. Microsoft Outlook includes many features
                                    (calendar,
                                    contacts, notes, tasks) and integrations with other applications (Word, Excel.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="outlook-exchange.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="active-directory.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          
                                          						  
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Active Directory
                                    
                                 </h3>
                                 
                                 <p>
                                    Active Directory &amp; Exchange Outlook Network Services FAQ's
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="active-directory.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="email.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Email
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Email
                                    
                                 </h3>
                                 
                                 <p>
                                    The Atlas email account (yourname@mail.valenciacollege.edu) provides faculty, staff,
                                    &amp; students with
                                    a mailbox, calendaring, collaboration, blogging, photo sharing, event planning, instant
                                    messaging
                                    tools and much more!
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="mail.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="network-security.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Network Security
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Network Security
                                    
                                 </h3>
                                 
                                 <p>
                                    Computer security is the process of preventing and detecting unauthorized use of your
                                    computer or
                                    our computer resources. Prevention measures help you to stop unauthorized users (also
                                    known as
                                    “intruders”) from accessing any part of your computer system.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="network-security.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/wifi/index.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Wifi
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Wifi
                                    
                                 </h3>
                                 
                                 <p>
                                    Wireless networking (wifi) is a vitally important and growing section of Valencia's
                                    computer network
                                    technology, providing support for education, research, and administration.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/wifi/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="passwords.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Passwords
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Passwords
                                    
                                 </h3>
                                 
                                 <p>
                                    There is a certain complexity to the password that you pick to make sure it is a secure
                                    password and
                                    not easily guessed by others.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="passwords.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="rules-of-the-road.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Rules of the Road
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Rules of the Road
                                    
                                 </h3>
                                 
                                 <p>
                                    All personnel (faculty, staff, and students) must acquire a user id and pass word
                                    before accessing
                                    Valencia College information resources. This access is approved by representatives
                                    of the
                                    information resource owner and processed through the Office of Information Technology.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="rules-of-the-road.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="telecommunications.php"><img src="/_resources/img/course_1.jpg" alt="Valencia image description">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Telecommunications
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>
                                    Telecommunications
                                    
                                 </h3>
                                 
                                 <p>
                                    Find faculty and staff phone numbers, extensions, mail codes
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="telecommunications.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        
                        <h3>
                           HELP DESK SUPPORT
                           
                        </h3>
                        
                        <p>
                           Contact the Help Desk to place a Support Ticket
                           
                        </p>
                        <a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new" class="banner_bt">Click here</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>
                           Need OIT Support?
                           
                        </h5>
                        <i class="icon-phone"></i>
                        
                        <p>
                           <a href="tel://4075825555">Call Extension 5555</a><br>
                           <small>Monday to Friday 9.00am - 6.00pm</small>
                           
                        </p>
                        
                     </div>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/index.pcf">©</a>
      </div>
   </body>
</html>