<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Networking | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/office-of-information-technology/networking/default-formstrue.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/office-of-information-technology/networking/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Networking</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/office-of-information-technology/">Office Of Information Technology</a></li>
               <li><a href="/employees/office-of-information-technology/networking/">Networking</a></li>
               <li>Networking</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2><strong><font color="#3333FF" size="4">OIT Network &amp; Infrastructure Services</font></strong></h2>
                        
                        <div>
                           
                           
                           
                           <p><strong><img alt="Exchange" border="0" height="73" src="exchange.jpg" width="217"></strong></p>
                           
                           
                           <p>Valencia is upgrading their Exchange On-Premise Storage to Exchange Online. This new
                              storage will give each user 50 gb of mail quota, which removes the need to archive
                              email or run out of space.
                           </p>
                           
                           <p>There are a couple of  changes you'll need to be aware of after the migration:</p>
                           
                           <p>Once your account is migrated, you'll need to close the  Desktop Outlook Client and
                              reopen it. The Unified Messaging (UM) Voice Mail  portion will prompt you to reenter
                              you Voice Mail PIN and Windows Security will  prompt for a password.
                           </p>
                           
                           
                           <p>Your mobile device will prompt you for your password because  the Server information
                              has changed. All you need is your full email name,  password and the Server is <strong><u>outlook.office365.com</u></strong></p>
                           
                           
                           <p>If you use the "Outlook" app you'll need to delete your account and add it back and
                              it will  resync.
                           </p>
                           
                           
                           <p>Here are 2 screen shots examples of how the new settings  should look for common Apple
                              and Android phones.
                           </p>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>Apple </strong></div>
                                    
                                    <div><strong>Andriod</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <img height="369" src="mobile3.jpg" width="294"> 
                                    </div>
                                    
                                    <div><img height="366" src="mobile1.jpg" width="201"></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <p>Also, webmail.valenciacollege.edu will redirect you to login  to <a href="http://outlook.com/owa/valenciacollege.edu">http://outlook.com/owa/valenciacollege.edu</a> This is the new Server hosted by Ofice365 and Outlook.com and will need  to authenticate
                              to Valencia's ADFS Server a second time if you are off Campus.
                           </p>
                           
                           
                           <p><img alt="Outlook" height="242" src="outlook.jpg" width="382"></p>
                           
                           
                           <p><img alt="Link" height="141" src="link.jpg" width="435"></p>
                           
                           
                           <p><img alt="Login Process" height="332" src="loginprocess.jpg" width="422"></p>
                           
                           
                           <p><img alt="356 View" height="105" src="365view.jpg" width="544"></p>
                           
                           
                           <p>If you are on Campus, you can update the Internet Options on IE so it doesn't ask
                              for the ADFS credentials because you are already logged into the computer, these are
                              below with the action steps highlighted.
                           </p>
                           
                           <p><img alt="Update Local Intranet" height="428" src="update-local-intranet.jpg" width="397"></p>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/office-of-information-technology/networking/default-formstrue.pcf">©</a>
      </div>
   </body>
</html>