
<ul>
	<li><a href="/employees/valencia-promise/index.php">Valencia Promise</a></li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Participate <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/employees/valencia-promise/mentor.php">Become a Mentor</a></li>
			<li><a href="/employees/valencia-promise/invest.php">Invest in a Child</a></li>
		</ul>
	</li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Contributors <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/employees/valencia-promise/sponsors.php">Our Sponsors</a></li>
			<li><a href="/employees/valencia-promise/schools.php">Participating Schools</a></li>
			<li><a href="/employees/valencia-promise/advisory.php">Advisory Council</a></li>
		</ul>
	</li>
	<li><a href="/employees/valencia-promise/events.php">Events</a></li>
	<li><a href="http://net4.valenciacollege.edu/forms/valencia-promise/contact.cfm" target="_blank">Contact Us</a></li>
</ul>

