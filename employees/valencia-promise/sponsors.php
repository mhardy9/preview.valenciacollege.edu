<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Our Sponsors  | Valencia College</title>
      <meta name="Description" content="Our Sponsors | Valencia Promise">
      <meta name="Keywords" content="college, school, educational, valencia, promise, sponsors">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/valencia-promise/sponsors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/valencia-promise/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Promise</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/valencia-promise/">Valencia Promise</a></li>
               <li>Our Sponsors </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <h2>Our Sponsors</h2>
                        
                        <p>Community support is crucial to the success of Valencia Promise. Our corporate sponsors
                           are committed to expanding educational opportunities for students in Orange County.
                        </p>
                        
                        
                        <h3>Founding Partner</h3>
                        <strong>Steve Hogan, CEO, Florida Citrus Sports</strong><br> <br>
                        <strong>"You young people are our MVP's. We can't wait to celebrate your successes in life."</strong> <br>
                        ~<span>quote at the Recognition Ceremony, Feb. 9th.</span><br> <br>
                        
                        
                        <p><img src="/_resources/img/EMPLOYEES/valencia-promise/FCS-foundation-logo-jpg-4c.jpg" height="250" alt="FCS logo"></p>
                        
                        
                        <h3>Corporate Sponsors</h3>
                        
                        
                        
                        <p>
                           
                           
                        </p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td style="background-color:#f0efef;">
                                    
                                    
                                    <table class="table ">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <td width="180" valign="middle" align="center"><a href="http://www.mearstransportation.com/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-1.png" alt="mears logo" width="175" height="175" border="0"></a></td>
                                             
                                             <td width="180" valign="middle" align="center"><a href="http://www.mccormickfoundation.org/page.aspx?pid=687" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-2.png" width="175" height="175" alt="omyf" border="0"></a></td>
                                             
                                             <td width="180" valign="middle" align="center"><a href="http://www.fullsail.edu/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-3.png" alt="full sail logo" width="175" height="175" border="0"></a></td>
                                             
                                             <td width="180" valign="middle" align="center"><a href="http://www.tavistock.com/?id=89" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-4.png" alt="tavistock logo" width="175" height="175" border="0"></a></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td valign="middle" align="center"> <a href="http://www.ocps.net" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-5.png" alt="ocps logo" width="175" height="175" border="0"></a>
                                                
                                             </td>
                                             
                                             <td valign="middle" align="center"><a href="http://orlandohealth.com/orlandohealth/index.aspx" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-6.png" width="175" height="175" border="0" align="middle" alt=""></a></td>
                                             
                                             <td valign="middle" align="center"><a href="http://www.statefarm.com/about/part_spos/grants/grants.asp" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-7.png" alt="state farm logo" width="175" height="175" border="0"></a></td>
                                             
                                             <td valign="middle" align="center"><a href="http://www.lakenona.com/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-8.png" alt="lake nona logo" width="175" height="175" border="0"></a></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td valign="middle" align="center">
                                                <a href="/bac/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-9.png" alt="BAC logo" width="175" height="175" border="0"></a> 
                                             </td>
                                             
                                             <td valign="middle" align="center"><a href="http://kif.kiwanis.org/KIF/home.aspx" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-10.png" alt="Picture" width="175" height="175" border="0"></a></td>
                                             
                                             <td valign="middle" align="center"><a href="http://walmartstores.com/communitygiving/203.aspx" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-11.png" alt="Picture" width="175" height="175" border="0"></a></td>
                                             
                                             <td valign="middle" align="center"><a href="http://www.ucf.edu" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-12.png" alt="UCF" width="175" height="175" border="0"></a></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td valign="middle" align="center"><a href="http://www.takestockinchildren.org/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-13.png" width="175" height="175" alt="tsic logo" border="0"></a></td>
                                             
                                             <td valign="middle" align="center"><img src="/_resources/img/EMPLOYEES/valencia-promise/sponsor-14.png" width="175" height="175" alt="tsic logo" border="0"></td>
                                             
                                             <td valign="middle" align="center"><img src="/_resources/img/EMPLOYEES/valencia-promise/aio-wireless.png" width="175" height="175" alt="aio wireless" border="0"></td>
                                             
                                             <td valign="middle" align="center" style="border:none; background-color:inherit;"><img src="/_resources/img/EMPLOYEES/valencia-promise/Logo-Wells-Fargo.jpg" width="175" height="175" alt="Wells Fargo"></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td valign="middle" align="center"><a href="https://www.duke-energy.com/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/duke-energy.png" width="175" height="175" alt="tsic logo" border="0"></a></td>   
                                             
                                             <td valign="middle" align="center">
                                                <a href="http://www.myfloridaprepaid.com/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/florida-prepaid.jpg" width="150" height="150" alt="tsic logo" border="0"></a>
                                                
                                             </td>    
                                             
                                             <td valign="middle" align="center"><a href="http://www.foundationforocps.org/" target="blank"><img src="/_resources/img/EMPLOYEES/valencia-promise/ocpsfoundationlogo.jpg" width="175" height="175" alt="tsic logo" border="0"></a></td>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        
                        
                        <h3>Our Success - Great Results</h3>
                        
                        <p>Valencia Promise has provided demonstrable results bridging high school graduation,
                           college enrollment, and college graduation.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/valencia-promise/sponsors.pcf">©</a>
      </div>
   </body>
</html>