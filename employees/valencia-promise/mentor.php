<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Become a Mentor  | Valencia College</title>
      <meta name="Description" content="Become a Mentor | Valencia Promise">
      <meta name="Keywords" content="college, school, educational, valencia, promise, mentor, apply">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/valencia-promise/mentor.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/valencia-promise/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Promise</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/valencia-promise/">Valencia Promise</a></li>
               <li>Become a Mentor </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <h2>Apply to Volunteer</h2>
                        
                        <p>A mentor is a caring, compassionate adult who chooses to devote their time to a young
                           person, offering them support, guidance, and encouragement. If you can donate one
                           hour per week, you can change a life by helping a child build a solid foundation of
                           values, establish goals, improve their academic and life skills, while developing
                           their self-esteem and confidence.
                        </p>
                        
                        <p>Mentoring sessions range from 45-60 minutes and occur at a school near your home or
                           workplace, during school hours. The days and times of mentoring sessions are arranged
                           between the mentor and mentee depending on their individual schedules and classes.
                           As a result, mentoring is convenient for both students and mentors.
                        </p>
                        
                        <p>Mentoring is the key to success for Valencia Promise students. Nationwide research
                           shows that if a young person is mentored they are 52 percent less likely to skip school,
                           46 percent less likely to begin using illegal drugs, and 27 percent less likely to
                           begin drinking alcohol.
                        </p>
                        
                        <p>More and more people want to have a lasting, profound impact in their community. They
                           understand that the commitment of becoming a mentor can change a child’s life while
                           helping them grow personally. Can you remember adults who paid attention to you; who
                           believed in you; and who guided you during your childhood? These individuals served
                           as mentors. Wouldn’t you like to have someone think of you as their life-changing,
                           trusted mentor?
                        </p>
                        
                        <p>For more information about the Valencia Promise program, please contact 407-582-3008
                           or <a href="mailto:valenciapromise@valenciacollege.edu">valenciapromise@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <p align="center"><img src="/_resources/img/EMPLOYEES/valencia-promise/IMG_0283.JPG" width="400" height="301" alt="Student and mentor"></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/valencia-promise/mentor.pcf">©</a>
      </div>
   </body>
</html>