<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advisory Board  | Valencia College</title>
      <meta name="Description" content="Valencia Promise Advisory Board">
      <meta name="Keywords" content="college, school, educational, valencia, promise, advisory, board">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/valencia-promise/old/advisory-board.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/valencia-promise/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/valencia-promise/">Valencia Promise</a></li>
               <li><a href="/employees/valencia-promise/old/">Old</a></li>
               <li>Advisory Board </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>
                              Advisory Board
                              
                           </h3>
                           
                           <p>
                              2015-2016
                              
                           </p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              The following community leaders serve on the advisory board for the Valencia Promise.
                              
                           </p>
                           
                           <p>
                              <strong>Ed Bustos</strong><br> Director of International Admission<br> Rollins College
                              
                           </p>
                           
                           <p>
                              <strong>Angela Chestang</strong><br> Manager for the Citizens Commission for Children<br> Orange County
                              Government
                              
                           </p>
                           
                           <p>
                              <strong>David Dawkins</strong><br> Senior Vice President<br> Wells Fargo Advisors, LLC
                              
                           </p>
                           
                           <p>
                              <strong>Nicole Dupre</strong><br> Director of Community Engagement<br> Opera Orlando
                              
                           </p>
                           
                           <p>
                              <strong>Jovanna Heavener</strong><br> Director of Philanthropy<br> Heavener Foundation
                              
                           </p>
                           
                           <p>
                              <strong>Rachael Kobb</strong><br> Government Relations Specialist<br> Orlando Regional Medical Center
                              
                           </p>
                           
                           <p>
                              <strong>Maritza Martinez</strong><br> Associate Vice President, Division of Community<br> University of
                              Central Florida<br>
                              
                           </p>
                           
                           <p>
                              <strong>Brian Orth</strong><br> First Vice President<br> Sun Trust Bank
                              
                           </p>
                           
                           <p>
                              <strong>Alexis Rosenberg</strong><br> Premium Activation Manager<br> Orlando Magic
                              
                           </p>
                           
                           <p>
                              <strong>Augusto Sanabria</strong><br> President and CEO<br> Hispanic Business Initiative Fund
                              
                           </p>
                           
                           <p>
                              <strong>Ronda Wilson</strong><br> Financial Advisor<br> Waddell &amp; Reed
                              
                           </p>
                           
                           <p>
                              &nbsp;
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side">
                        <a href="https://valenciacollege.edu/valencia-promise/mentor_app.cfm"><img src="/_resources/img/employees/valencia-promise/button-mentor.png" alt="Apply to be a Valencia College Take Stock in Children mentor" width="245" height="70" border="0" data-pin-nopin="true"></a>
                        
                        <p>
                           &nbsp;
                           
                        </p>
                        <a href="https://donate.valencia.org/promise"><img src="/_resources/img/employees/valencia-promise/button-donate.png" alt="Donate to the Valencia College Take Stock in Children program" width="245" height="70" border="0" data-pin-nopin="true"></a>
                        
                        <p>
                           &nbsp;
                           
                        </p>
                        <a href="https://www.ocps.net/es/cr/resources/Pages/ADDitions.aspx"><img src="/_resources/img/employees/valencia-promise/button-additions.png" alt="Fill out the online OCPS ADDitions form" width="245" height="85" border="0" data-pin-nopin="true"></a>
                        
                        <p>
                           &nbsp;
                           
                        </p>
                        
                        <p align="center">
                           To volunteer as a mentor, provide a tax-deductible contribution or to obtain more
                           information about Valencia
                           Promise of Orange County, please contact Orange County Valencia Promise, at
                           
                        </p>
                        
                        <p align="center">
                           407-582-3008 or&nbsp;<a href="mailto:valenciapromise@valenciacollege.edu">valenciapromise@valenciacollege.edu</a>
                           
                        </p>
                        
                        <p align="center">
                           <a href="http://www.takestockinchildren.org/"><img src="/_resources/img/employees/valencia-promise/take-stock-in-children.jpg" alt="Take Stock in Children" width="245" height="127" border="0"></a>
                           
                        </p>
                        
                        <h4 align="center">
                           Valencia College Partner
                           
                        </h4>
                        
                     </div>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/valencia-promise/old/advisory-board.pcf">©</a>
      </div>
   </body>
</html>