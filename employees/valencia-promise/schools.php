<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Participating Schools  | Valencia College</title>
      <meta name="Description" content="Participating Schools | Valencia Promise">
      <meta name="Keywords" content="college, school, educational, valencia, promise, participating, schools">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/valencia-promise/schools.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/valencia-promise/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Promise</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/valencia-promise/">Valencia Promise</a></li>
               <li>Participating Schools </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Participating Schools</h2>
                        
                        
                        
                        <p><a href="http://www.ocps.net"><img src="/_resources/img/EMPLOYEES/valencia-promise/ocpslogocolor.png" alt="Orange County Public School's Logo" align="right" width="169" height="112" border="0"></a>Valencia Promise of Orange County began in 2008 with 50 student participants from
                           the three Orange County middle schools considered to be the most in need. As the program
                           grows, Valencia anticipates working with children from additional middle schools.
                        </p>
                        
                        
                        
                        
                        <ul>
                           
                           <li>
                              Apopka High School
                              
                           </li>
                           
                           <li>
                              Arbor Ridge Middle School
                              
                           </li>
                           
                           <li>Boone High School
                              
                           </li>
                           
                           <li>Colonial High School 
                              
                           </li>
                           
                           <li>Cypress Creek High School
                              
                           </li>
                           
                           <li>Dr. Phillips High School
                              
                           </li>
                           
                           <li>East River High School
                              
                           </li>
                           
                           <li>Edgewater High School
                              
                           </li>
                           
                           <li>
                              Evans High School 
                              
                           </li>
                           
                           <li>Howard Middle School 
                              
                           </li>
                           
                           <li>Jones High School
                              
                           </li>
                           
                           <li>Memorial Middle School
                              
                           </li>
                           
                           <li>Oak Ridge High School
                              
                           </li>
                           
                           <li>
                              Olympia High School
                              
                           </li>
                           
                           <li>
                              University High School
                              
                           </li>
                           
                           <li>Wekiva High School
                              
                           </li>
                           
                           <li>Winter Park High School 
                              
                           </li>
                           
                        </ul>
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/valencia-promise/schools.pcf">©</a>
      </div>
   </body>
</html>