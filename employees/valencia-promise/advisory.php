<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advisory Board  | Valencia College</title>
      <meta name="Description" content="Advisory Board | Valencia Promise">
      <meta name="Keywords" content="college, school, educational, valencia, promise, advisory, council, board">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/valencia-promise/advisory.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/valencia-promise/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Promise</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/valencia-promise/">Valencia Promise</a></li>
               <li>Advisory Board </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Advisory Board 2015-2016</h2>
                        
                        <p>The following community leaders serve on the advisory board for the Valencia Promise.</p>            
                        
                        
                        
                        <p><strong>Ed Bustos </strong></p>
                        
                        <p>Director of International Admission<br> 
                           Rollins College
                        </p>
                        
                        <p><strong>Angela Chestang </strong></p>
                        
                        <p>Manager for the Citizens Commission for Children<br>
                           Orange County Government
                        </p>
                        
                        <p><strong>David Dawkins </strong></p>
                        
                        <p>Senior Vice President<br> 
                           Wells Fargo Advisors, LLC 
                        </p>
                        
                        <p><strong>Nicole Dupre </strong></p>
                        
                        <p>Director of Community Engagement <br>
                           Opera Orlando 
                        </p>
                        
                        <p><strong>Jovanna Heavener </strong></p>
                        
                        <p>Director of Philanthropy<br>
                           Heavener Foundation 
                        </p>
                        
                        <p><strong>Rachael Kobb </strong></p>
                        
                        <p>Government Relations Specialist <br>
                           Orlando Regional Medical Center
                        </p>
                        
                        <p><strong>Maritza Martinez </strong></p>
                        
                        <p>Associate Vice President, Division of Community <br>
                           
                           University of Central Florida
                        </p>
                        
                        <p><strong>Brian Orth</strong></p>
                        
                        <p>First Vice President</p>
                        
                        <p>Sun Trust Bank </p>
                        
                        <p><strong>Alexis Rosenberg </strong></p>
                        
                        <p>Premium Activation Manager <br>
                           Orlando Magic 
                        </p>
                        
                        <p><strong>Augusto Sanabria </strong></p>
                        
                        <p>President and CEO <br>
                           Hispanic Business Initiative Fund 
                        </p>
                        
                        <p><strong>Ronda Wilson</strong></p>
                        
                        <p>Financial Advisor<br>
                           Waddell &amp; Reed 
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/valencia-promise/advisory.pcf">©</a>
      </div>
   </body>
</html>