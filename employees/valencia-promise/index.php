<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Promise | Valencia College</title>
      <meta name="Description" content="Valencia Promise">
      <meta name="Keywords" content="college, school, educational, valencia, promise">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/valencia-promise/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/valencia-promise/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Promise</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li>Valencia Promise</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Overview </h2>
                        
                        <p>Since 2007, Valencia College has worked closely with Take Stock in Children to enrich
                           the lives of young people in Orange County. Take Stock in Children (TSIC) is a non-profit
                           organization that provides low-income students in grades 8-12 with the resources to
                           break the cycle of poverty through education. Serving every county in the state of
                           Florida, Take Stock in Children assists students in making a successful transition
                           from high school to college by offering funding, mentoring, and advocacy services.
                           
                        </p>
                        
                        <p>Partnering with Take Stock in Children, the Valencia Promise Program endeavors to
                           involve various members of the community – including state and local government representatives,
                           businesses, colleges/universities, and school districts - in creating unique educational
                           opportunities for low-income and minority students to help them realize the dream
                           of a college education. Valencia College invests in these students by providing a
                           multi-year program model of mentorship, college preparation, civic engagement, and
                           a college scholarship. Collectively, Valencia and our community partners invest in
                           the next generation of leaders which directly impacts the social and economic development
                           of Central Florida.<br>
                           
                        </p>
                        
                        
                        <p align="center"><img src="/_resources/img/EMPLOYEES/valencia-promise/13490701_114724422289561_7539874507221658292_o.jpg" width="500" height="333" alt="Student Graduates of the Valencia Promise Program"></p>
                        
                        
                        <h3><strong>Program Model</strong></h3>
                        
                        <p><strong>The Valencia Promise – Take Stock in Children Program model consists of four “Pillars
                              of Promise”</strong></p>
                        
                        <p><br>
                           <u><strong>Pillar 1: </strong></u>Scholarship. Students and their parents/guardians agree to specific academic performance
                           standards. In order to receive a college scholarship, students must stay in school,
                           maintain an acceptable GPA, and remain drug and crime free. Additionally, all of our
                           student must take the SAT and/or ACT during their junior and senior years.
                        </p>
                        
                        <p><u><strong>Pillar 2:</strong></u> Mentorship. Each student who is enrolled in the Valencia Promise program is matched
                           with a mentor who provides guidance and academic motivation. Students meet with their
                           mentors a minimum of 15 times per academic year. Students will also meet with their
                           College Success Coordinator at least 4 times per academic year.
                        </p>
                        
                        <p><u><strong>Pillar 3:</strong></u> Leadership Development. The Valencia Promise program hosts workshops for our students
                           on topics including financial literacy, funding opportunities, career planning, community
                           awareness and civic engagement, self-care, and conflict resolution. Students must
                           attend at least 4 workshops per academic year.
                        </p>
                        
                        <p><u><strong>Pillar 4:</strong></u> Civic Engagement. We endeavor to help our students become active learners and engaged
                           citizens. To this end, we require our students to complete at least 25 hours of service
                           to the local community each academic year.<br>
                           
                        </p>
                        
                        
                        
                        
                        <p>
                           <img src="/_resources/img/EMPLOYEES/valencia-promise/Sandy.jpg" width="192" height="192" align="left" style="margin-right:10px" alt="Dr. Sandy Shugart">
                           
                           
                           Dr. Sanford Shugart<br>
                           President, Valencia College
                        </p>
                        
                        <h4>"We have many children in our county that are dreaming of going to college, and we
                           are committed to making that dream a reality. This program will provide our children
                           with a chance to obtain a higher education and will help them get there with the guidance
                           of a caring mentor through middle and high school."
                        </h4>
                        
                        
                     </div>
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/valencia-promise/index.pcf">©</a>
      </div>
   </body>
</html>