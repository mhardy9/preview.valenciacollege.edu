<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Property Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/property-management/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/property-management/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Property Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li>Property Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-8">
                        
                        <p>The Property Management Office is chartered to verify the location, custodian, condition
                           and cost of all applicable Fixed Assets the  College purchases. An annual inventory
                           of all items, i.e. furniture and equipment, that have been assigned a Valencia College
                           asset number and are valued at  $1,000 or more, is conducted at the beginning of the
                           calendar year and is ongoing. All College owned equipment is inventoried including
                           those items located off campus. <strong>Important:</strong> No item may be  relocated, disposed, or traded in without proper notification to
                           the Property Management Office in advance. 
                        </p>
                        
                     </div>
                     
                     <div class="col-md-4"> <img src="images/propertycode.gif"> 
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4"> <a class="box_feat" href="documents/FunFacts.pdf"> <i class="far fa-question-circle"></i>
                           
                           <h3> Property Control Info </h3>
                           
                           <p> Good to Know and Fun Facts about Property Control at Valencia.<br>
                              <br><br>
                              
                           </p>
                           </a> 
                     </div>
                     
                     <div class="col-md-4"> <a class="box_feat" href="documents/propertycontrol.pdf"> <i class="far fa-download"></i>
                           
                           <h3> Property Control Form </h3>
                           
                           <p> Submit this form when property belonging to Valencia is to be relocated, removed
                              for disposal, temporarily off-campus. 
                           </p>
                           </a> 
                     </div>
                     
                     <div class="col-md-4"> <a class="box_feat" href="documents/badge.pdf"> <i class="far fa-diamond"></i>
                           
                           <h3> Badges </h3>
                           
                           <p> Property Management Recognizes its Colleagues with a Badge.  See how to do this in
                              Valencia EDGE.<br><br> 
                           </p>
                           </a> 
                     </div>
                     
                  </div>
                  
                  <div class="row add_bottom_30">
                     
                     <ul class="list_staff">
                        
                        <li>
                           
                           <figure> <img src="/_resources/img/no-photo-male-thumb.png" alt="Valencia image description" class="img-circle"> 
                           </figure>
                           
                           <h4> <a href="mailto:wlong@valenciacollege.edu">David Long</a> 
                           </h4>
                           
                           <p> <span title="Title"><i class="far fa-globe fa-fw"></i>Property Management Coordinator</span><br>
                              <span title="Phone"><i class="far fa-phone fa-fw"></i>407-582-1267</span><br>
                              <span title="Office"><i class="far fa-building fa-fw"></i>DO, 1-343</span>&nbsp;&nbsp; <span title="Mail Code"><i class="far fa-envelope-o fa-fw"></i>MC: DO-333</span> 
                           </p>
                           
                        </li>
                        
                        <li>
                           
                           <figure> <img src="/_resources/img/no-photo-female-thumb.png" alt="Valencia image description" class="img-circle"> 
                           </figure>
                           
                           <h4> <a href="mailto:fbrown@valenciacollege.edu">Fanita Brown</a> 
                           </h4>
                           
                           <p> <span title="Title"><i class="far fa-globe fa-fw"></i>Property Records Specialist</span><br>
                              <span title="Phone"><i class="far fa-phone fa-fw"></i>407-582-1263</span><br>
                              <span title="Office"><i class="far fa-building fa-fw"></i>DO, 1-343</span>&nbsp;&nbsp; <span title="Mail Code"><i class="far fa-envelope-o fa-fw"></i>MC: DO-333</span> 
                           </p>
                           
                        </li>
                        
                        <li>
                           
                           <figure> <img src="/_resources/img/no-photo-female-thumb.png" alt="Valencia image description" class="img-circle"> 
                           </figure>
                           
                           <h4> <a href="mailto:kkelley5@valenciacollege.edu">Karen Kelley</a> 
                           </h4>
                           
                           <p> <span title="Title"><i class="far fa-globe fa-fw"></i>Property Records Specialist</span><br>
                              <span title="Phone"><i class="far fa-phone fa-fw"></i>407-582-1331</span><br>
                              <span title="Office"><i class="far fa-building fa-fw"></i>DO, 1-343</span>&nbsp;&nbsp; <span title="Mail Code"><i class="far fa-envelope-o fa-fw"></i>MC: DO-333</span> 
                           </p>
                           
                        </li>
                        
                        <li>
                           
                           <figure> <img src="/_resources/img/no-photo-male-thumb.png" alt="Valencia image description" class="img-circle"> 
                           </figure>
                           
                           <h4></h4>
                           
                           <p> <span title="Title"><i class="far fa-globe fa-fw"></i>Property Support Specialist</span><br>
                              <span title="Phone"><i class="far fa-phone fa-fw"></i>407-582-3114</span><br>
                              <span title="Office"><i class="far fa-building fa-fw"></i>DO, 1-343</span>&nbsp;&nbsp; <span title="Mail Code"><i class="far fa-envelope-o fa-fw"></i>MC: DO-333</span> 
                           </p>
                           
                        </li>
                        
                     </ul>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="row">
                     
                     <h3>College Policies</h3>
                     
                     <ul>
                        
                        <li>
                           <a href="/ABOUT/generalcounsel/policy/documents/Volume5/5-11-College-Property-Control.pdf">6Hx28: 5-11</a> College Property Control
                        </li>
                        
                        <li>
                           <a href="/ABOUT/generalcounsel/policy/documents/Volume9/9-02-Gifts-to-the-College.pdf">6Hx28: 9-02</a> Gifts to the College
                        </li>
                        
                     </ul>
                     
                     <h3>Forms</h3>
                     
                     <ul>
                        
                        <li><a href="documents/GiftstotheCollege.pdf">Gifts to the College Form</a></li>
                        
                     </ul>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/property-management/index.pcf">©</a>
      </div>
   </body>
</html>