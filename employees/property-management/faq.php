<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Property Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/property-management/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/property-management/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Property Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/property-management/">Property Management</a></li>
               <li>Property Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>FAQs</h2>
                        
                        <div> <strong>Download FAQ Document:</strong><br>
                           <a href="doc/faq.doc">FAQ document</a> 
                        </div>
                        
                        <p><strong>Question:  Our office has some obsolete equipment, such as a printer, that we no longer
                              use and would like to have it removed from our office.  What do we need to do?</strong></p>
                        
                        <p><strong>Answer:</strong> Submit a Property Control form to Property Management, and once the appropriate approvals
                           are received, Plant Operations will remove the item.
                        </p>
                        
                        <p><strong>Question:  Where does old property go?</strong></p>
                        
                        <p><strong>Answer:</strong> When old, unwanted equipment has been approved for deletion from the property records,
                           it can go to a variety of places.  Generally, the equipment is relocated to a surplus
                           warehouse to wait for final disposition.  If the equipment is in usable condition,
                           the first preference is for it to go to a Valencia-approved non-profit organization.
                           If a non-profit organization cannot be found, then we have an approved, certified
                           recycler that will pick up the equipment and recycle or dispose of the equipment in
                           a safe, environmentally-responsible way.  If the equipment is suitable for a landfill
                           (i.e., furniture, metal cabinets, wood desks) and it is more economical to dispose
                           of than to store, then the equipment may be readily disposed.  Electronics should
                           never be thrown into a dumpster, no matter how small, and nothing should ever be disposed
                           of without first checking with Property Management.
                        </p>
                        
                        <p><strong>Question:  Can I just throw this ol' thing in the trash?</strong></p>
                        
                        <p><strong>Answer:</strong> Depending on what the ol' thing is, it probably should not be thrown into the trash.
                           Electronics should never be thrown into the trash, since they may contain harmful
                           chemicals, leads, inks or radioactive materials.  Many things may still be tracked
                           by Property Management, even though they are very old; therefore, Property Management
                           should always be consulted before disposal of any items.  If you see a "Property of
                           Valencia College" barcode on the item, then a Property Control form must be completed
                           prior to any removal or disposal.
                        </p>
                        
                        <p><strong>Question:  What if we want to give some of our equipment, such as a laptop, to another
                              person/department either at the same campus or at another Valencia campus?</strong></p>
                        
                        <p><strong>Answer:</strong> You must complete a Property Control form to relocate an item, not only for our tracking
                           purposes, but also to have Plant Ops move the item for you.  If the item is small,
                           like a laptop, and doesn't need to be moved by Plant Ops, be sure to complete a Property
                           Control form for the item prior to relocation.   We need to be able to track all equipment
                           once-a-year; therefore, we need to know where the item is located and whose name should
                           be listed as the new custodian of the item.
                        </p>
                        
                        <p><strong>Question:  Can employees purchase or donate old computers?</strong></p>
                        
                        <p><strong>Answer:</strong> Neither employees nor students are allowed to purchase or donate computers or any
                           other type of equipment belonging to Valencia College.  New computers can be purchased
                           directly from Dell Computer Corporation generally at a discount to employees of Valencia
                           College.
                        </p>
                        
                        <p><strong>Question:  Why did our office receive an "Unlocated Inventory List" from the Property
                              Management office and what should we do now?</strong></p>
                        
                        <p><strong>Answer:</strong> The Property Management office scans all inventory &amp; equipment belonging to Valencia
                           College once-a-year per Florida law.  It is difficult to locate every item, particularly
                           laptops and equipment that is checked-out to students/staff, during our beginning-of-the
                           year inventory scans.  Our office will then send a list to department heads notifying
                           them of Unlocated Inventory.  Please assist us by locating the items on the list and
                           then emailing or calling Property Management to let us know where the items are located.
                           If you cannot locate the items, and the items have been missing for at least two years,
                           complete a Property Control form noting that the item is missing and have the Security
                           Office complete a Security report for the item.  This process will remove the item
                           from our inventory records so that we do not keep asking your office to locate it.
                        </p>
                        
                        <p><a href="#top">TOP</a></p>            
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/property-management/faq.pcf">©</a>
      </div>
   </body>
</html>