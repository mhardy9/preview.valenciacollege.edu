<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Organizational Development &amp; Human Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/employmentverifications-employeesmain.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Organizational Development &amp; Human Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Organizational Development &amp; Human Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <p><strong>Employees </strong></p>
                        
                        <p>The Work Number provides the following benefits to Employees and Verifiers:</p>
                        
                        <p><strong>-</strong><strong>Access Control</strong></p>
                        
                        <p>The Work Number will provide consistent system reports containing the most factual
                           and objective data available.&nbsp; &nbsp;&nbsp;
                        </p>
                        
                        <p>Employees can receive a detailed report <strong>(Employee Data Report)</strong> showing the information contained on The Work Number and the verifiers who have accessed
                           that information.&nbsp; Employees can generate a single-use six-digit code <strong>(Salary Key)</strong> which they may provide as consent to verifiers in order to access income information.
                        </p>
                        
                        <p><strong>Equifax Workforce Solutions is able to provide the following:&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
                        
                        <ul>
                           
                           <li>Employee Name</li>
                           
                           <li>Most recent hire date</li>
                           
                           <li>Termination date (if no longer employed)</li>
                           
                           <li>Total time with the college </li>
                           
                           <li>Job Title</li>
                           
                           <li>Rate of Pay</li>
                           
                           <li>Gross earnings for current year-to-date including base pay and overtime. </li>
                           
                           <li>Gross earnings for last year and two years past, including base pay and  overtime.</li>
                           
                        </ul>
                        
                        <p><strong>-Instant Availability of records </strong></p>
                        
                        <p>Current, former employees and verifiers can log on to The Work Number website anytime
                           24 hours a day, 7 days a week.&nbsp; This means a faster turnaround for Verifiers thereby
                           speeding up the processing of mortgage, auto and other loan applications, rental/lease
                           agreements, government aid/Social Service applications and reference checks.
                        </p>
                        
                        <p><strong>Instructions</strong></p>
                        
                        <p>Go to<a href="http://www.theworknumber.com" target="_blank"> www.theworknumber.com</a> or click below for additional information.
                        </p>
                        
                        <p><strong>-<a href="EmploymentVerifications-EmployeesAccessTWN_login.pdf" target="_blank">How to access your account</a></strong></p>
                        
                        <p><strong>-<a href="EmploymentVerifications-EmployeesEDRandSalKey.pdf" target="_blank">Viewing your Employee Data Report and/or generating a Salary Key </a></strong></p>
                        
                        <p>The Employee Data Report is a record of your information contained in The Work Number
                           database and a list of the verifiers who have accessed that information.&nbsp; A Salary
                           Key is a single-use, six digit code you can provide to a verifier as consent to access
                           your income information. 
                        </p>
                        
                        <p>-<strong><a href="EmploymentVerifications-EmployeesWalletCards.pdf" target="_blank">Wallet Card</a> </strong></p>
                        
                        <p>A reference card you may provide to verifiers or public assistance agencies.</p>
                        
                        <p>-<a href="EmploymentVerifications-EmployeesFAQs.pdf" target="_blank"><strong>FAQ</strong></a></p>
                        
                        <p>For additional questions or assistance over the phone, please call:</p>
                        
                        <p><strong>The Work Number Client Service Center</strong></p>
                        
                        <p><strong>1-800-996-7566</strong></p>
                        
                        <p><strong>1-800-660-3399 (Social Service Agencies Only)</strong></p>
                        
                        <p><strong>1-800-424-0253 (TTY-Deaf)</strong></p>
                        
                        <p><strong>Monday-Friday 7:00am -8:00pm (CT)</strong></p>
                        *Instructional/Educational Experience Verifications will continue to be handled by
                        Human Resources.&nbsp; These forms or requests can be directed to HR Records at hrrecords@valenciacollege.edu.&nbsp;
                        Due to the extensive research and detail required, we have a 5 business day turnaround
                        time to complete these requests.<strong> </strong>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <p>HOURS &amp; LOCATION</p>
                           
                           
                           
                           <p>
                              <strong>Hours</strong><br>
                              <span>Monday - Friday: 8 AM to 5 PM</span>
                              
                           </p>
                           
                           <p>
                              <strong>Location</strong>
                              
                           </p>
                           
                           <p>District Office<br>
                              1768 Park Center Drive<br> Orlando, FL 32825
                           </p>
                           
                           <p><a href="http://maps.google.com/maps?q=1768+Park+Center+Drive,+Orlando,+FL+32825&amp;iwloc=A&amp;hl=en" target="_blank" title="Driving Directions">Driving Directions</a></p>
                           
                           
                           <p>
                              <strong>Phone</strong><br>
                              <span>407-582-8033</span>
                              
                           </p>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/employmentverifications-employeesmain.pcf">©</a>
      </div>
   </body>
</html>