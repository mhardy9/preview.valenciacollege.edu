<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Organizational Development &amp; Human Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/about-valencia.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Organizational Development &amp; Human Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Organizational Development &amp; Human Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2>About Valencia</h2>
                        
                        <h3>Create a better tomorrow. Actually, more like 63,000 better tomorrows.</h3>
                        
                        
                        <p>The winner of the inaugural <a href="http://news.valenciacollege.edu/academic-issues/valencia-named-top-community-college-in-nation" target="_blank" title="Aspen Prize">Aspen Prize for Community College Excellence</a>, Valencia is one of the largest community colleges in
                           Florida, teaching more than 63,000 students annually. But it's our quality of education,
                           not size, that truly makes us great. Our <a href="../honors/index.html" program="" title="">Honors program</a> is viewed by the National Collegiate and Southern Regional Honors Council as one
                           of the exemplary programs in the nation. The Orlando Sentinel dubbed our <a href="../artsandentertainment/index.html" title="Fine-arts Faculty">fine-arts faculty </a> one of the best in the region, and director Steven Spielberg called our film program
                           one of the best anywhere. But the opinions that matter most are those of our students,
                           and 92% say we met or exceeded their expectations on overall quality of education.
                        </p>
                        
                        <p>Valencia College provides <a href="EqualAccessEqualOpportunity.html">equal opportunity for employment</a> to all, without regard to race, ethnicity, color, national origin, age, religion,
                           disability, marital status, gender, genetic information, sexual orientation and any
                           other factor protected under applicable federal, state, and civil rights laws, rules
                           and regulations.
                        </p>
                        
                        <div><a href="../about-valencia/vision.html" target="_blank">VISION, VALUES, MISSION</a></div>
                        <br>
                        
                        <div><a href="documents/admin-ORG-CHART.pdf" target="_blank">ORGANIZATION CHART</a></div>
                        
                        
                        
                        
                        <h2>About Central Florida</h2>
                        
                        <h3>Be inspired daily. By your students and your surroundings.</h3>
                        
                        <p>Valencia College has five area locations as well as a Downtown Center and Criminal
                           Justice Institute, all conveniently located
                           throughout Central Florida and easily accessible by local freeways, expressways, and
                           surface streets. But the convenient commute
                           is just part of the appeal. With beautiful year-round weather, endless entertainment
                           and dining options, and world-famous attractions,
                           there's no shortage of incredible ways to spend your spare time.
                        </p>
                        
                        <p>Enjoy vibrant nightlife in downtown Orlando, breathtaking thrill rides at the theme
                           parks, or simply relax and recharge just a short
                           drive away at some of Florida's most acclaimed beaches. There are also plenty of museums,
                           art galleries, live music venues and
                           theaters to get your cultural fix. And, being in the Sunshine State, you'll have access
                           to an abundance of outdoor recreation including
                           countless golf courses, parks, bike paths, nature trails, collegiate and professional
                           sporting events, as well as Florida's signature
                           water sports like surfing, wakeboarding, and waterskiing.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <p>HOURS &amp; LOCATION</p>
                           
                           
                           
                           <p>
                              <strong>Hours</strong><br>
                              <span>Monday - Friday: 8 AM to 5 PM</span>
                              
                           </p>
                           
                           <p>
                              <strong>Location</strong>
                              
                           </p>
                           
                           <p>District Office<br>
                              1768 Park Center Drive<br> Orlando, FL 32825
                           </p>
                           
                           <p><a href="http://maps.google.com/maps?q=1768+Park+Center+Drive,+Orlando,+FL+32825&amp;iwloc=A&amp;hl=en" target="_blank" title="Driving Directions">Driving Directions</a></p>
                           
                           
                           <p>
                              <strong>Phone</strong><br>
                              <span>407-582-8033</span>
                              
                           </p>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/about-valencia.pcf">©</a>
      </div>
   </body>
</html>