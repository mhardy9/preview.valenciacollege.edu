<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Organizational Development &amp; Human Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/recognition.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Organizational Development &amp; Human Resources</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Organizational Development &amp; Human Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				<a name="content" id="content"></a>
                     
                     
                     				
                     <h2>Valencia's Formal Recognition Programs </h2>
                     				
                     <h3>Career Anniversary </h3>
                     				
                     <p>Who is Eligible? Full-Time and Part-Time Faculty &amp; Staff<br>
                        					<a href="Career_Anniversary.php">More information</a></p>
                     				
                     <h3>Endowed Chairs Program </h3>
                     				
                     <p>Who is Eligible? Full-Time Tenured Faculty<br>
                        					Application Process Opens:Every year in January<br>
                        					Application Deadline: Every year in April <br>
                        					<a href="endowed_chairs.php">More information
                           					</a></p>
                     				
                     <h3>Faculty Association Awards for Excellence in Counseling, Teaching and Librarianship
                        
                     </h3>
                     				
                     <p>Who is Eligible? All Faculty (Full-Time and Part-Time)<br>
                        					Nomination Process Opens: Every year in January <br>
                        					Nomination Deadline: Every year in March <br>
                        					<a href="FAAECTL.php">More information</a></p>
                     				
                     <h3>Innovation of the Year Awards </h3>
                     				
                     <p>Who is Eligible? All Faculty &amp; Staff (Full-Time and Part-Time)<br>
                        					Nomination Process Opens: Every year in November <br>
                        					Nomination Deadline: Every year in January <br>
                        					<a href="innovation_of_the_year.php">More information</a></p>
                     				
                     <h3>Sabbatical Leave</h3>
                     				
                     <p>Who is Eligible? Full-Time Tenured Faculty, Professional and Administrative Staff<br>
                        					Application Process Opens: Every year in September <br>
                        					Application Deadline: Every year in October<br>
                        					<a href="sabbatical.php">More information</a></p>
                     				
                     <h3>The John &amp; Suanne Roueche Excellence Awards</h3>
                     				
                     <p>Who is Eligible? Full-Time Faculty and Staff<br>
                        					Nomination Process Opens: Every year in September <br>
                        					Nomination Deadline: Every year in October<br>
                        					<a href="roueche.php">More information </a>
                        
                        				
                     </p>
                     
                     				
                     <p>For informal recognition programs and tools, visit the Recognition channel in <br>
                        					Valencia EDGE 
                        				
                     </p>
                     					
                     <h3>The Grove - Awards &amp; Recognitions</h3>
                     					<?php /*
    (demo) Localist Event Feed Formatted Asset
*/
    if(isset($article_feed_url)){
        include_once($_SERVER['DOCUMENT_ROOT'] . '/_resources/php/feeds/localist_feed.php');
        echo build_news_feed_widgets($article_feed_url);
    } else {
        echo "Articles unavailable.";
    }

		?>					
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/recognition.pcf">©</a>
      </div>
   </body>
</html>