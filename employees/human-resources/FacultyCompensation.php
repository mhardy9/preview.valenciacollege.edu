<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Organizational Development &amp; Human Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/FacultyCompensation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Organizational Development &amp; Human Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Organizational Development &amp; Human Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        <h2>Faculty Compensation </h2>
                        
                        
                        
                        <p>Professional Development Implementation and Oversight Team (PDT)</p>
                        
                        <p><a href="http://www.valenciacc.edu/hr/documents/ApprovedProfessionalDevelopmentComponentPlan.pdf">Approved 
                              Professional Development Component Plan</a></p>
                        
                        <p><a href="http://www.valenciacc.edu/hr/documents/PDTCharge.pdf">PDT 
                              Charge</a></p>
                        
                        
                        
                        <h3>Compensation Implementation Team </h3>
                        
                        <p><a href="documents/CompensationImplementationTeamMembership9-13-07.pdf">Compensation 
                              Implementation Team </a></p>
                        
                        <p>Compensation Implementation Meeting Minutes</p>
                        
                        
                        <ul>
                           
                           <li><a href="documents/FacComp112105final.doc">November 21, 2005</a></li>
                           
                           <li><a href="documents/FacComp121605final.doc">December 16, 2005</a></li>
                           
                           <li><a href="documents/MinutesFeb2506final.doc">February 25, 2006</a></li>
                           
                           <li><a href="documents/Approved3-24-06.doc">March 24, 2006</a></li>
                           
                           <li><a href="documents/FacCompImplTm4-28-06.Min.doc">April 28, 2006</a></li>
                           
                           <li><a href="documents/FacCompImp5-25-06.doc">May, 25, 2006</a></li>
                           
                           <li><a href="documents/FacCompImp6-22-06.doc">June 22, 2006</a></li>
                           
                           <li><a href="documents/FacultyCompensationImpementationMtg7-20-07.pdf.html">July 
                                 17, 2007</a></li>
                           
                           <li><a href="documents/RevisedFacultyCompensationImpementationMtg9-19-07.11-6-07.doc">September 17, 2007</a></li>
                           
                           <li><a href="documents/COMPENSATIONIMPL3-27-08.pdf">February 
                                 26, 2008</a></li>
                           
                           <li><a href="documents/FacultyCompensationImpementationMtg3-26-08rev6-4-08.pdf">March 
                                 25, 2008</a></li>
                           
                           <li><a href="documents/FacultyCompensationImpementationMtg10-20-09.pdf">October 20, 2009</a></li>
                           
                        </ul>
                        
                        <p><a href="documents/FAQExpandedfinal.pdf">FAQs</a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/FacultyCompensation.pcf">©</a>
      </div>
   </body>
</html>