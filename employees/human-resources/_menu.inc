
<ul>
	<li><a href="index.php">Organizational Development &amp; Human Resources</a></li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
		<li><a href="aboutValencia.php">About Valencia</a></li>
		<li><a href="benefits.php">Valencia Benefits</a></li>
		<li><a href="jobs.php">Jobs</a></li>
		<li><a href="compensation.php">Compensation</a></li>
		<li><a href="EqualAccessEqualOpportunity.php">Equal Access Equal Opportunity</a></li>
		<li><a href="recognition.php">Awards &amp; Recognition</a></li>
		<li><a href="videos.php">Get to Know Us</a></li>
		<li><a href="http://net4.valenciacollege.edu/forms/HR/contact.cfm" target="_blank">Contact Us</a></li>
		</ul>
	</li>
</ul>