<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Organizational Development &amp; Human Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/wellness/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Organizational Development &amp; Human Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Wellness</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Wellness</h2>
                        
                        <p>The Valencia Wellness website can now be found at HRinTouch which is located in the
                           Employee Tab under Benefits/Wellness.
                        </p>
                        
                        <p>Any questions please email <a href="mailto:lyoungman@valenciacollege.edu">Laurie Youngman</a>, director, compensation and workforce planning. 
                        </p>
                        
                        <p>Valencia College is moving forward to a healthier work place. Valencia's Wellness
                           program is dedicated to creating communities of people with healthy bodies and minds.
                           We invite you to browse the web site, send us questions or feedback and check back
                           regularly for continued updates. Remember we are here for you......
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <p>HOURS &amp; LOCATION</p>
                           
                           
                           
                           <p>
                              <strong>Hours</strong><br>
                              <span>Monday - Friday: 8 AM to 5 PM</span>
                              
                           </p>
                           
                           <p>
                              <strong>Location</strong>
                              
                           </p>
                           
                           <p>District Office<br>
                              1768 Park Center Drive<br> Orlando, FL 32825
                           </p>
                           
                           <p><a href="http://maps.google.com/maps?q=1768+Park+Center+Drive,+Orlando,+FL+32825&amp;iwloc=A&amp;hl=en" target="_blank" title="Driving Directions">Driving Directions</a></p>
                           
                           
                           <p>
                              <strong>Phone</strong><br>
                              <span>407-582-8033</span>
                              
                           </p>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/wellness/index.pcf">©</a>
      </div>
   </body>
</html>