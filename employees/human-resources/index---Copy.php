<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/index---Copy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt=" " height="8" src="arrow.gif" width="8">
                                 
                                 <img alt=" " height="8" src="arrow.gif" width="8">
                                 
                                 
                                 
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a name="navigate" id="navigate"></a>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          
                                          
                                          <div><a href="../employees/employment-at-valencia/index.html">EMPLOYMENT OPPORTUNITIES</a></div>
                                          <br>
                                          
                                          <div><a href="aboutValencia.html" title="ABOUT VALENCIA">ABOUT VALENCIA</a></div>
                                          <br>
                                          
                                          <div>
                                             <a href="http://net4.valenciacollege.edu/forms/hr/contact.cfm" target="_blank" title="Contact HR">CONTACT US</a><br>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <p><a href="EmploymentVerifications.html">VERIFY EMPLOYMENT<br>
                                                   </a></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <a name="content" id="content"></a>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h2>Featured Faculty Job Listing</h2>
                                       
                                       
                                       <p>Valencia College is seeking an <strong>Adjunct Professor, Mathematics</strong> to join our dedicated, passionate  team as a part-time, adjunct faculty member. Through
                                          this role, you would have  a direct impact on our students, community and, ultimately,
                                          our world by inspiring  students to reach their fullest potential.            
                                       </p>
                                       
                                       <div>
                                          
                                          <div><a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=4619" target="_blank" title="VIEW LISTING">VIEW LISTING</a></div>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <br>
                                       
                                       
                                       <div>
                                          <iframe allowfullscreen="" frameborder="0" height="154" src="http://www.youtube.com/embed/IJpazqJ7uGU" width="245"></iframe>
                                          
                                          
                                          <div>
                                             
                                             <div><a href="videos.html" title="VIEW MORE VIDEOS">VIEW MORE VIDEOS</a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>          
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/index---Copy.pcf">©</a>
      </div>
   </body>
</html>