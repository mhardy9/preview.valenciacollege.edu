<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Contacts | Title IX | Valencia College</title>
      <meta name="Description" content="Valencia College strives to be a community in which all members can learn and work in an atmosphere free from all forms of harassment, including sexual harassment, discrimination, intimidation and/or retaliation. All forms of discrimination and harassment based on protected status are prohibited at Valencia College. "><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/title-ix/contacts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/title-ix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title IX</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li><a href="/employees/human-resources/title-ix/">Title IX</a></li>
               <li>Contacts</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  			
                  <h2>Contacts</h2>
                  			
                  <p><a href="accessible/contacts.html">Click this link for text only version</a></p>
                  
                  			
                  <p>For more information, please contact: </p>
                  			
                  <p><strong>Ryan D. Kane</strong><br>
                     				Assistant Vice President, Organizational  Development &amp; Inclusion<br>
                     				Title IX Coordinator<br>
                     				407-582-3421<br>
                     				<a href="mailto:rkane8@valenciacollege.edu">rkane8@valenciacollege.edu</a></p>
                  			
                  <p><strong>Lauren Kelly</strong><br>
                     				Director,  Equal Opportunity &amp; Employee Relations<br>
                     				4017-582-8125<br>
                     				<a href="mailto:lkelly22@valenciacollege.edu">lkelly22@valenciacollege.edu</a></p>
                  			
                  <p><strong>Chanda  Postell</strong><br>
                     				Assistant  Director, Equal Opportunity &amp; Employee Relations<br>
                     				Project  Director, DOJ OVW Grant<br>
                     				407-582-3422<br>
                     				<a href="mailto:cpostell2@valenciacollege.edu">cpostell2@valenciacollege.edu</a></p>
                  			
                  <p><strong>Ben Taylor</strong><br>
                     				Assistant  Director, Equal Opportunity &amp; Employee Relations<br>
                     				407-582-3454<br>
                     				<a href="mailto:wtaylor17@valenciacollege.edu">wtaylor17@valenciacollege.edu</a></p>
                  			
                  <p><strong>Jennifer  Bevan</strong><br>
                     				Coordinator,  Department of Justice, OVW Grant<br>
                     				407-582-3867<br>
                     				<a href="mailto:Jbevan2@valenciacollege.edu">Jbevan2@valenciacollege.edu</a></p>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/title-ix/contacts.pcf">©</a>
      </div>
   </body>
</html>