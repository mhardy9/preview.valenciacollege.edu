<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Reporting | Title IX | Valencia College</title>
      <meta name="Description" content="Valencia College strives to be a community in which all members can learn and work in an atmosphere free from all forms of harassment, including sexual harassment, discrimination, intimidation and/or retaliation. All forms of discrimination and harassment based on protected status are prohibited at Valencia College. "><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/title-ix/reporting.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/title-ix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title IX</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li><a href="/employees/human-resources/title-ix/">Title IX</a></li>
               <li>Reporting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  			
                  <h2>Reporting</h2>
                  			
                  <p><a href="accessible/reporting.html">Click this link for text only version</a></p>
                  
                  			
                  <p>You are not alone. You don't have to accept sexual harassment. Get help — for you
                     or someone else. Valencia College has dedicated resources to address abuse or harassment.
                     Be the one to report behavior that makes you feel uncomfortable, or feel unsafe.
                  </p>
                  
                  <p>Title IX Coordinator, Ryan D. Kane
                     <br> Office, Human Resources
                     <br> 407-582-3421
                     		<br> <a href="mailto:rkane8@valenciacollege.edu">rkane8@valenciacollege.edu</a>
                     
                  </p>
                  
                  
                  <p><a href="/students/disputes/" target="_blank">Student Dispute Resolution Form</a></p>
                  
                  <p>If you witness or are affected by abuse or harassment, Valencia College has dedicated
                     resources available to you.
                  </p>
                  
                  
                  <h3>District Office</h3>
                  
                  <ul>
                     
                     <li>Director, Equal Opportunity &amp; Employee Relations: 407-582-8125</li>
                     
                     <li>Assistant Director, Equal Opportunity &amp; Employee Relations: 407-582-3422 or 407-582-3454</li>
                     
                     <li>Director, HR Policy &amp; Compliance Program: 407-582-8256</li>
                     
                     <li>Managing Director, Safety &amp; Security: 407-582-1336</li>        
                     
                  </ul>
                  
                  
                  <h3>East Campus, Winter Park and School of Public Safety</h3>
                  
                  <ul>
                     
                     <li>Dean of Students: 407-582-2586</li>
                     
                     <li>Coordinator, Student Conduct &amp; Academic Success: 407-582-2346</li>
                     
                     <li>Regional Director, ODHR: 407-582-2760</li>
                     
                     <li>Regional Assistant Director, ODHR: 407-582-2816</li>
                     
                     <li>Assistant Director, Security: 407-582-2365</li>
                     
                     <li>Director, Student Services, Winter Park: 407-582-6868</li>
                     
                  </ul>
                  
                  
                  <h3>Osceola, Lake Nona and Poinciana Campuses</h3>
                  
                  <ul>
                     
                     <li>Dean of Students: 321-682-4142</li>
                     
                     <li>Regional Director, ODHR: 321-682-4710</li>
                     
                     <li>Regional Assistant Director, ODHR: 321-682-4710</li>
                     
                     <li>Assistant Director, Security: 407-582-1047</li>
                     
                     <li>Director, Student Services, Poinciana: 407-582-6069</li>
                     
                     <li>Director, Student Services, Lake Nona: 407-582-7780</li>
                     
                  </ul>
                  
                  
                  <h3>West Campus</h3>
                  
                  <ul>
                     
                     <li>Dean of Students: 407-582-1388</li>
                     
                     <li>Coordinator, Student Conduct &amp; Academic Success: 407-582-1557</li>
                     
                     <li>Regional Director, ODHR: 407-582-1756</li>
                     
                     <li>Assistant Director, Security: 407-582-1327</li>
                     
                  </ul>
                  
                  
                  <h3>Safety and Security</h3>
                  
                  <ul>
                     
                     <li>District Office: 407-582-3000</li>
                     
                     <li>East Campus: 407-582-2000</li>
                     
                     <li>Lake Nona Campus 407-582-7000</li>
                     
                     <li>Osceola Campus: 407-582-4000</li>
                     
                     <li>Poinciana Campus: 407-582-6500</li>
                     
                     <li>West Campus: 407-582-1000</li>
                     
                     <li>Winter Park Campus: 407-582-6000</li>
                     
                  </ul>
                  
                  
                  <p>* All reports made to on-campus resources <strong>will be shared</strong> with the college's Title IX Coordinator for further review and appropriate response.
                  </p>
                  			
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/title-ix/reporting.pcf">©</a>
      </div>
   </body>
</html>