<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Title IX | Valencia College</title>
      <meta name="Description" content="Valencia College strives to be a community in which all members can learn and work in an atmosphere free from all forms of harassment, including sexual harassment, discrimination, intimidation and/or retaliation. All forms of discrimination and harassment based on protected status are prohibited at Valencia College. "><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/title-ix/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/title-ix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title IX</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Title IX</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  			
                  <h2>Title IX</h2>
                  
                  			
                  <p>Wave them over. Smile. Listen. Make eye contact. Inviting someone to participate brings
                     perspective, and makes them feel engaged and supported. It enriches and strengthens
                     a diverse, inclusive community. Be the one to show them they matter.
                  </p>
                  			
                  <p>Valencia College strives to be a community in which all members can learn and work
                     in an atmosphere free from all forms of harassment, including sexual harassment, discrimination,
                     intimidation and/or retaliation. All forms of discrimination and harassment based
                     on protected status are prohibited at Valencia College. As such, students, faculty,
                     and staff are expected to adhere to the College's Policy: 6Hx28:2-01 which prohibits
                     the above actions as well as sexual assault, sexual exploitation, stalking and interpersonal
                     violence.
                  </p>
                  			
                  <p>All members of the College community are responsible for conducting themselves in
                     accordance with these expectations and other College policies and procedures. Allegations
                     of sexual misconduct should be reported to a College official. The College takes these
                     matters seriously and will respond with appropriate action.
                  </p>
                  			
                  <p>Valencia College is an equal opportunity institution. We provide equal opportunity
                     for employment and educational services to all individuals as it relates to admission
                     to the College or to programs, any aid, benefit, or service to students or wages and
                     other terms, conditions or privileges of employment, without regard to race, ethnicity,
                     color, national origin, age, religion, disability, marital status, sex/gender, sexual
                     orientation, genetic information, gender identity, pregnancy, and any other factor
                     prohibited under applicable federal, state, and local civil rights laws, rules and
                     regulations. 
                  </p>
                  			
                  <p>Dr. Amy Bosley is the Vice President for Organizational Development and Human Resources
                     and Ryan Kane is the College's Title IX Coordinator and Equal Opportunity Officer,
                     and both ensure compliance with federal, state and local laws prohibiting discrimination
                     and sexual harassment at Valencia.
                  </p>
                  			
                  <p>Employees and students who believe they have been a victim of discrimination or sexual
                     harassment should contact: 
                  </p>
                  
                  			
                  <p>
                     				<strong>Ms. Lauren Kelly</strong>
                     				<br> Director, Equal Opportunity &amp; Employee Relations
                     				<br>
                     				
                     <address>
                        					1768 Park Center Drive
                        					<br> Orlando, Florida 32835
                        				
                     </address>
                     				407-582-8125
                     				<br>
                     				<a href="mailto:lkelly22@valenciacollege.edu">E-mail Lauren Kelly</a>
                     			
                  </p>
                  			
                  <p>
                     				<strong>Mr. Ryan Kane</strong>
                     				<br> Assistant Vice President, Org. Development &amp; Inclusion
                     				<br> Title IX Coordinator, Section 504 Coordinator
                     				<br>
                     				
                     <address>
                        					1768 Park Center Drive
                        					<br> Orlando, Florida 32835
                        				
                     </address>
                     				407-582-3421
                     				<br>
                     				<a href="mailto:rkane8@valenciacollege.edu">E-mail Ryan Kane</a>
                     			
                  </p>
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/title-ix/index.pcf">©</a>
      </div>
   </body>
</html>