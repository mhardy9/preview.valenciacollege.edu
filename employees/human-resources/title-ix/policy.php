<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Policy | Title IX | Valencia College</title>
      <meta name="Description" content="Valencia College strives to be a community in which all members can learn and work in an atmosphere free from all forms of harassment, including sexual harassment, discrimination, intimidation and/or retaliation. All forms of discrimination and harassment based on protected status are prohibited at Valencia College. "><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/title-ix/policy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/title-ix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title IX</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li><a href="/employees/human-resources/title-ix/">Title IX</a></li>
               <li>Policy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  			
                  <h2>Policy</h2>
                  			
                  <p>A. It is the policy of the  District Board of Trustees to provide equal opportunity
                     for employment and  educational opportunities to all applicants for employment, employees,
                     applicants for admission, students, and others affiliated with the College, without
                     regard to race, ethnicity, color, national origin, age, religion, disability, marital
                     status, sex/gender, genetic information, sexual orientation, gender identity, pregnancy,
                     and any other factor protected under applicable federal, state, and local civil rights
                     laws, rules and regulations  (collectively referred to as “Protected Status”). 
                  </p>
                  			
                  <p>B. In  addition, Valencia College (“Valencia” or “College”) strives to be a community
                     in which all members can learn and work in an atmosphere free from all forms of  Harassment,
                     including sexual Harassment, discrimination, intimidation and/or retaliation. This
                     Policy  prohibits all forms of Discrimination  and Harassment based on Protected Status.
                     It expressly, therefore, also prohibits Sexual Assault and Sexual  Exploitation, which
                     by definition involve  conduct of a sexual nature and are prohibited forms of Sexual
                     or Gender-Based Harassment. This Policy further prohibits Stalking and Interpersonal
                     Violence, which need not be based on an individual’s Protected Status. Finally, this
                     Policy prohibits Complicity for knowingly assisting in an act that violates this 
                     Policy and Retaliation against an individual because of his or her good faith  participation
                     in the reporting, investigation, and/or adjudication of  violations of this Policy.
                     These behaviors are collectively referred to in this  Policy as Prohibited Conduct.
                  </p>
                  			<a href="/about/general-counsel/policy/documents/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">2.01 Discrimination Harassement and Related Conduct (PDF)</a>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/title-ix/policy.pcf">©</a>
      </div>
   </body>
</html>