<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Resources | Title IX | Valencia College</title>
      <meta name="Description" content="Valencia College strives to be a community in which all members can learn and work in an atmosphere free from all forms of harassment, including sexual harassment, discrimination, intimidation and/or retaliation. All forms of discrimination and harassment based on protected status are prohibited at Valencia College. "><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/title-ix/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/title-ix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title IX</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li><a href="/employees/human-resources/title-ix/">Title IX</a></li>
               <li>Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  			
                  <h2>College Resources</h2>
                  			
                  			
                  <p><a href="accessible/resources.html">Click this link for plain text view</a></p>
                  
                  			
                  <p>The College identifies key individuals that can serve as a resource in various ways.
                     This includes the Title IX Coordinator and Deputy Title IX Coordinators. The full
                     list of Deputy Title IX Coordinators can be found under the ‘Reporting” tab. These
                     individuals are tasked with coordinating the college’s response and education on the
                     topics of sexual assault, sexual harassment, interpersonal violence and stalking.
                     Deputy Coordinators are located on all of our campuses and are available to assist
                     students and employees at any time. The resources that Deputy Title IX Coordinators
                     may provide includes, but is not limited to the following:
                  </p>
                  			
                  <ul>
                     				
                     <li>interim measures</li>
                     				
                     <li>academic support</li>
                     				
                     <li>assistance in contacting Law Enforcement</li>
                     				
                     <li>assistance in contacting community based confidential support resources</li>
                     			
                  </ul>
                  
                  
                  			
                  <h3>OFF CAMPUS RESOURCES</h3>
                  			
                  <p>
                     				Victim Services Center of Central Florida (Confidential 24 hour Sexual Assault
                     Hotline) If you need medical attention, the Victim Services Center offers 24/7 on-call
                     forensic examinations and medical services.<br>
                     				407-497-6701 or www.victimservicecenter.org
                     			
                  </p>
                  
                  			
                  <p>
                     				<a href="http://www.harborhousefl.com">Harbor House of Central Florida (24 hour Domestic Violence Hotline)</a><br>
                     				407-886-2856
                     			
                  </p>
                  
                  			
                  <p>
                     				<a href="http://www.helpnowshelter.org">Help Now of Osceola Inc. (24 hour crisis hotline for domestic abuse)</a><br>
                     				407-847-8562
                     			
                  </p>
                  
                  			
                  <p>
                     				<a href="http://www.thehotline.org">National Domestic Violence Hotline</a><br>
                     				800-799-7233
                     			
                  </p>
                  
                  			
                  <p>
                     				<a href="http://www.baycare.org/sap">BayCare Student Assistance Program (available to students who are currently enrolled
                        in Valencia College credit classes)</a><br>
                     				800-878-5470
                     			
                  </p>
                  
                  			
                  <p>
                     				<a href="http://www.rainn.org">RAINN (Rape, Abuse &amp; Incest National Network)</a><br>
                     				800-656-4673
                     			
                  </p>
                  
                  			
                  <h3>Local Police</h3>
                  			
                  			
                  <p>In an emergency, dial 911.</p>
                  			
                  			
                  <p>City of Orlando Police Department<br>
                     				Main Number: 321-235-5300
                  </p>
                  
                  			
                  <p>Orange County Sheriff's Office<br>
                     				Main Number: 407-254-7000
                  </p>
                  
                  			
                  <p>City of Kissimmee Police Department<br>
                     				Main Number: 407-846-3333
                  </p>
                  
                  			
                  <p>Osceola County Sheriff's Office<br>
                     				Main Number: 407-348-2222
                  </p>
                  
                  			
                  <p>City of Winter Park Police Department<br>
                     				Main Number: 407-599-3444
                  </p>
                  
                  			
                  <h3>Local Hospitals</h3>
                  			
                  <p>Florida Hospital Orlando: 407-303-5600<br>
                     				Florida Hospital East: 407-303-8110<br>
                     				Florida Hospital Kissimmee: 407-846-4343<br>
                     				Orlando Regional Medical Center: 407-841-5111<br>
                     				Dr. P. Phillips Hospital: 407-351-8500<br>
                     				Health Central Hospital: 407-296-1000<br>
                     				Osceola Regional Medical Center: 407-846-2266
                  </p>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/title-ix/resources.pcf">©</a>
      </div>
   </body>
</html>