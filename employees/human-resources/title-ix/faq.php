<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Frequently Asked Questions | Title IX | Valencia College</title>
      <meta name="Description" content="Valencia College strives to be a community in which all members can learn and work in an atmosphere free from all forms of harassment, including sexual harassment, discrimination, intimidation and/or retaliation. All forms of discrimination and harassment based on protected status are prohibited at Valencia College. "><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/title-ix/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/title-ix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Title IX</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li><a href="/employees/human-resources/title-ix/">Title IX</a></li>
               <li>Frequently Asked Questions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  			
                  <h2>Frequently Asked Questions</h2>
                  			
                  			
                  <p><a href="accessible/faq.html">Click this link for plain text view</a></p>
                  			
                  			
                  <h3>General Questions</h3>
                  			
                  <h4>Do I have to report this to the police?</h4>
                  			
                  <p>
                     				No. Reporting sexual misconduct to the police is an option available to you, but
                     you are not required to notify the police. Students are, however, encouraged to reach
                     out to the appropriate police department through Safety &amp; Security or on their own
                     to report sexual violence.
                     			
                  </p>
                  			
                  <h4>How many people am I going to have to talk to?</h4>
                  			
                  <p>
                     				We are aware that reporting parties often do not want to have to tell their story
                     to multiple sources. The Title IX coordinator/deputy coordinator may ask a reporting
                     party to share what took place, and may ask him/her to provide a written account of
                     what happened. If a college investigation is conducted, the complainant may be asked
                     to provide further details than what may have been originally shared at the original
                     time of reporting.
                     			
                  </p>
                  			
                  <h4>What if I don’t know who did this?</h4>
                  			
                  <p>
                     				You should still report the misconduct and share any relevant information with
                     the college and/or police. The college will follow up with you, make sure that you
                     are aware of appropriate on and off-campus resources, and may be able to investigate
                     based on the information you have provided.
                     			
                  </p>
                  			
                  <h4>What if I don’t want the person to get in trouble?</h4>
                  			
                  <p>
                     				The college’s goal is not to get someone in trouble, but to respond to reports
                     of sexual misconduct, eliminate the behavior, prevent its recurrence and address its
                     effects. Part of this may include disciplinary action taken against another individual,
                     which will involve holding the individual accountable for his/her behavior and taking
                     steps to prevent this from happening again.
                     			
                  </p>
                  			
                  			
                  <h3>Confidentiality</h3>
                  			
                  <h4>Who is going to find out?</h4>
                  			
                  <p>
                     				The staff at the college who will be involved in the response to the report, such
                     as the Title IX coordinator/deputy Title IX coordinator will be notified. Should a
                     request be made for classroom, employment and/or transportation accommodations, the
                     appropriate offices will be notified of the accommodation request, but not informed
                     of the details of the report. Other students who are not involved, media/press and
                     other faculty/staff without a “need to know” role are not informed of the complaint.
                     The Title IX coordinator/deputy Title IX coordinator can discuss any request for confidentiality
                     with the reporting party at any time.
                     			
                  </p>
                  			
                  <h4>Is my family going to be called?</h4>
                  			
                  <p>
                     				No. However, the College can assist you with notifying your parents or other supportive
                     individuals, should you choose to do so. Reporting parties under the age of 18, however,
                     may be subject to additional disclosures, based on the situation. If you have questions
                     regarding this disclosure, please contact the Title IX Coordinator.
                     			
                  </p>
                  			
                  <h4>Why can’t you guarantee confidentiality?</h4>
                  			
                  <p>
                     				A reporting party may request that his/her name not be disclosed to alleged perpetrators
                     or that no investigation or disciplinary action be pursued to address the sexual misconduct,
                     particularly in cases of sexual violence. Valencia College supports a reporting party’s
                     interest in confidentiality in cases involving sexual violence. However, there are
                     situations in which the college must override a request for confidentiality in order
                     to meet its obligations. These instances will be limited and the information will
                     be maintained in a secure manner and will only be shared with individuals who are
                     responsible for handling the college’s response to the incident of sexual misconduct.
                     			
                  </p>
                  			
                  <h4>What happens if the reporting party requests that his/her name be confidential or
                     ask that the college not take any action?
                  </h4>
                  			
                  <p>
                     				If a reporting party requests that his/her name not be revealed to the responding
                     party or asks that the college not investigate or seek action against the responding
                     party, the college will inform the reporting party that honoring the request may limit
                     its liability to respond fully to the incident, including pursuing disciplinary action
                     against the responding party. The college will also explain that Title IX includes
                     protections against retaliation, and that college officials will not only take steps
                     to prevent retaliation but will also take responsive action if it occurs.
                  </p>
                  			
                  <p>
                     				If the reporting party still requests that his/her name not be disclosed to the
                     responding party or that the college not investigate or seek action against the responding
                     party, the college will need to determine whether or not it can honor such a request
                     while still providing a safe and nondiscriminatory environment for all students, and
                     staff, including the reporting party.
                  </p>
                  			
                  <p>If the college decides that it must take action and the reporting party asks that
                     the college inform the responding party that the reporting party asked the college
                     not to investigate or seek discipline, the college should honor this request and inform
                     the alleged perpetrator that the college made the decision to go forward. 
                  </p>
                  			
                  			
                  <h3>Valencia College Processes and Procedures</h3>
                  			
                  <h4>What are some examples of sexual misconduct?</h4>
                  			
                  <p>
                     				Sexual misconduct includes, but is not limited to, interpersonal (dating/domestic)
                     violence, sexual assault, non-consensual sexual contact, stalking, sexual harassment,
                     unwelcome sexual advances, requests for sexual favors, or verbal and physical conduct
                     of a sexual nature, (when behavior is severe or pervasive enough to create an intimidating
                     or hostile environment) , threatening to sexually assault someone, cyber-stalking,
                     indecent exposure, sexual exploitation (which includes taking non-consensual or abusive
                     advantage of others, i.e. taking sexual photos).
                     			
                  </p>
                  			
                  <h4>Can I have a friend come with me?</h4>
                  			
                  <p>
                     				Yes, both reporting parties and accused responding parties can have a person of
                     their choosing with them throughout all steps in the college’s process, including
                     any meeting with the Title IX coordinator/deputy coordinator or designee. The supportive
                     person cannot represent the reporting or responding party during a disciplinary hearing
                     (see “advisor” definition in the&nbsp;Discrimination, Harassment &amp; Related Misconduct Policy
                     <a href="/generalcounsel/policy/documents/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">6Hx28:2-01</a>) but can serve as a supportive person for the reporting or responding party.
                     			
                  </p>
                  			
                  <h4>Who will explain the college’s response?</h4>
                  			
                  <p>
                     				The Title IX coordinator/deputy Title IX coordinator (or designee) will be able
                     to sit down with a reporting party to talk about options, requests for accommodations,
                     available resources on and off campus, the college’s response and any other questions
                     that may be asked. Reporting parties are strongly encouraged to meet with the above
                     listed individuals to get their questions answered.
                     			
                  </p>
                  			
                  <h4>What does the process look like?</h4>
                  			
                  <p>
                     				The process is outlined in detail in the Discrimination, Harassment &amp; Related
                     Misconduct Policy <a href="/generalcounsel/policy/documents/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">6Hx28:2-01</a>. It is a non-adversarial process in which the college attempts to determine the facts
                     of the report and take the necessary follow up action. It is not a criminal process
                     and does not include the participation of lawyers, judges, or juries.
                     			
                  </p>
                  			
                  <h4>What are some interim actions that may be taken?</h4>
                  			
                  <p>
                     				When students are accused of sexual misconduct, they may be subject to interim
                     action pending the outcome of the college’s disciplinary process. These include, but
                     are not limited to, emergency suspension from the college, removal from an academic
                     class, restriction from being on campus, restriction from entering a specific building,
                     or a specific area of campus, and no-contact with the complainant or others involved
                     in the complaint.
                  </p>
                  			
                  <p>
                     				When faculty or staff are accused of sexual misconduct, they may be subject to
                     interim action pending the outcome of the college’s process. These include, but are
                     not limited to, leave of absence (voluntary or involuntary), or college imposed leave
                     or separation.
                     			
                  </p>
                  			
                  <h4>Why am I being punished before my case is heard?</h4>
                  			
                  <p>
                     				At times, due to the nature of the allegation, it is essential for the college
                     to take immediate action to protect the safety of the campus community. This could
                     include involuntary leave or separation from the college, and/or other interim action.
                     This action is interim and pending the outcome of the college’s process.
                     			
                  </p>
                  			
                  <h4>What prevents retaliation against complainants?</h4>
                  			
                  <p>
                     				The college will take action against any individual or group of individuals who
                     retaliate against a reporting party or any other party involved in a report of sexual
                     misconduct. Often times, no-contact orders are issued to responding parties that inform
                     them that they are to have no contact with the reporting party. This includes, but
                     is not limited to, in-person, via telephone, email, social media, or having others
                     contact the reporting party or other parties on his/her behalf.
                     			
                  </p>
                  			
                  <h4>What if I don’t want to see the person during a disciplinary  process?</h4>
                  			
                  <p>
                     				Accommodations can be made during a disciplinary process to allow for participants
                     to participate through remote access locations or by providing partitions separating
                     parties should both parties be required to be in the same location at the same time.
                     			
                  </p>
                  			
                  <h4>Will I find out what happens?</h4>
                  			
                  <p>
                     				Both the reporting party and responding party will be notified concurrently of
                     the decision of the disciplinary process, any interim action that is being taken,
                     and the final outcome of the process, including the status of any appeal.
                     			
                  </p>
                  			
                  <h4>What if I am not happy with the outcome?</h4>
                  			
                  <p>
                     				Both the reporting party and responding party may appeal the outcome of a Title
                     IX process as per the appeal process listed in the&nbsp;policy <a href="/generalcounsel/policy/documents/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">6Hx28:2-01</a>.
                     			
                  </p>
                  			
                  <h4>What if I’ve been accused?</h4>
                  			
                  <p>
                     				Responding parties should review the information included in policy <a href="/generalcounsel/policy/documents/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">6Hx28:2-01</a> which describes the procedures for sexual misconduct cases. You will be offered an
                     opportunity to meet with the Title IX coordinator/deputy coordinator to discuss the
                     process, have your questions answered, and be able to provide information regarding
                     the allegations.
                     			
                  </p>
                  			
                  <h4>Why can’t my lawyer represent me in the process?</h4>
                  			
                  <p>
                     				Ultimately, the college’s disciplinary process is an educational process and responding
                     parties are not entitled to representation by an attorney or another party. Attorneys
                     can serve as “advisors” (see definition in policy <a href="/generalcounsel/policy/documents/Volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">6Hx28:2-01</a>) who can advise their client but do not play active roles in the college’s process.
                     If attorneys have questions regarding this expectation, they should contact the College’s
                     General Counsel Office at 407-582-3450.
                     			
                  </p>
                  			
                  			
                  <h3>Available Resources</h3>
                  			
                  <h4>Does Valencia College have a victim/survivor advocate?</h4>
                  			
                  <p>
                     				The Victim Service Center of Central Florida 407-497-6701&nbsp;www.victimservicecenter.org
                     is a 24 hour helpline and victim/survivor advocate at 211 East Michigan Ave. Suite
                     210, Orlando, FL 32806. They provide advocates who can be with reporting parties through
                     all steps of the process. Also, Harbor House of Central Florida 407-886-2856 <a href="http://www.harborhousefl.com">www.harborhousefl.com</a> provides a 24 hour hotline for victims/survivors of domestic/dating violence. They
                     also provide safe shelter, legal advocacy, injunctions and relocation assistance.
                     			
                  </p>
                  			
                  <h4>What interim measures are available?</h4>
                  			
                  <p>
                     				The college can consider interim measures that involve academic classes, no-contact
                     orders, and transportation accommodation requests from reporting parties. These requests
                     should be made through the Title IX coordinator/deputy coordinator. Efforts will be
                     made to honor these requests, whenever possible.
                     			
                  </p>
                  			
                  <h4>Where can I obtain a forensic medical exam?</h4>
                  			
                  <p>
                     				The Victim Service Center of Central Florida 407-497-6701&nbsp;www.victimservicecenter.org
                     is a 24 hour helpline and victim/survivor advocate at 211 East Michigan Ave. Suite
                     210, Orlando, FL 32806. They provide forensic evidence collection by a Sexual Assault
                     Nurse Examiner in a private facility offered in a home-like environment. While no
                     one is required to get this exam, we encourage reporting parties to be seen by a health
                     professional if they have been the victim of sexual assault or other forms of violence.
                     			
                  </p>
                  			
                  			
                  <h3>Concurrent or Possible Criminal/Civil Action</h3>
                  			
                  <h4>What if the court or police dismiss my case?</h4>
                  			
                  <p>
                     				The college’s disciplinary process is separate from any concurrent or pending
                     charges through the court system. The final results of a police investigation and/or
                     a criminal proceeding do not impact the college’s process.
                     			
                  </p>
                  			
                  			
                  <h4>What if I have a pending court case?</h4>
                  			
                  <p>
                     				Responding parties with pending court cases should contact their attorney to discuss
                     their individual situation. The college’s process will proceed independently of a
                     criminal court case and is not dependent on the decision from the court with regards
                     to cases of sexual misconduct.
                     			
                  </p>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/title-ix/faq.pcf">©</a>
      </div>
   </body>
</html>