<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Organizational Development &amp; Human Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/equalaccessequalopportunity.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Organizational Development &amp; Human Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Organizational Development &amp; Human Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <p><strong>VALENCIA COLLEGE</strong></p>
                        
                        <p><strong>COMMITMENT TO EQUAL ACCESS AND EQUAL OPPORTUNITY</strong></p>
                        
                        
                        <p>Valencia College is an equal opportunity institution. We provide equal opportunity
                           for employment and educational services to all individuals as it relates to admission
                           to the College or to programs, any aid, benefit, or service to students or wages and
                           other terms, conditions or privileges of employment, without regard to race, ethnicity,
                           color, national origin, age, religion, disability, marital status, sex/gender, sexual
                           orientation, genetic information, gender identity, pregnancy, and any other factor
                           prohibited under applicable federal, state, and local civil rights laws, rules and
                           regulations.
                        </p>
                        
                        <p>In accordance with <a href="../generalcounsel/policy/documents/volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">Title IX of the Education Amendments of 1972</a>, Section 504 of the Rehabilitation Act of 1973, Title II of the Americans with Disabilities
                           Act of 1990, and other applicable laws, the College is committed to ensuring that
                           current and future students are not discriminated against and ensuring a bias-free
                           learning environment. College Policy 6Hx28:2-01 Discrimination, Harassment and Related
                           Misconduct (Including Sexual and Gender-Based Harassment, Sexual Assault, Sexual Exploitation,
                           Interpersonal Violence, Stalking, Complicity, and Retaliation) outlines the expectation
                           of members of the College community as well as the procedures for addressing allegations
                           of this nature.            
                        </p>
                        
                        <p>Additionally, specific procedures for filing a complaint and resolving academic disputes
                           are provided under <a href="../students/disputes/index.html" target="_blank">valenciacollege.edu/students/disputes/</a> as well as the prevailing policy -<a href="../generalcounsel/policy/documents/volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank"> 6Hx28:8-10</a> (Student Academic Dispute and Administrative Complaint Resolution). Also, the College
                           provides an ombudsman on each campus with whom students are able to conduct off-the-record
                           discussions about student concerns. This information is provided in the Student LifeMap
                           Handbook and College Catalog.
                        </p>
                        
                        
                        <p>Lauren Kelly is the Director of Equal Opportunity &amp; Employee Relations. Ryan Kane
                           is the College’s Title IX Coordinator and Section 504 Coordinator, and is responsible
                           for coordinating the College’s efforts with respect to these laws. Both Ms. Kelly
                           and Mr. Kane ensure compliance with federal, state and local laws prohibiting discrimination
                           and sexual harassment at Valencia.
                        </p>
                        
                        
                        <p dir="auto">Employees and students who believe they have been a victim of discrimination or sexual
                           harassment should contact:
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Ms. Lauren Kelly&nbsp;</strong><br>
                                    Director, Equal Opportunity &amp; Employee Relations<br>                  1768 Park Center Drive <br>                  Orlando, Florida&nbsp; 32835<br>                  407-582-8125<br>                  <a href="mailto:lkelly22@valenciacollege.edu" title="Email Dr. Bosley at abosley@valenciacollege.edu">Email Lauren Kelly</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Mr. Ryan Kane </strong><br>
                                    Assistant Vice President, Org. Development &amp; Inclusion<br>
                                    Title IX Coordinator, Section 504 Coordinator <br>
                                    1768 Park Center Drive <br>
                                    Orlando, Florida 32835 <br>
                                    407-582-3421<br>                  <a href="mailto:rkane8@valenciacollege.edu">Email Ryan Kane </a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div><a href="title-ix/index.html">Title IX Website</a></div>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <p>HOURS &amp; LOCATION</p>
                           
                           
                           
                           <p>
                              <strong>Hours</strong><br>
                              <span>Monday - Friday: 8 AM to 5 PM</span>
                              
                           </p>
                           
                           <p>
                              <strong>Location</strong>
                              
                           </p>
                           
                           <p>District Office<br>
                              1768 Park Center Drive<br> Orlando, FL 32825
                           </p>
                           
                           <p><a href="http://maps.google.com/maps?q=1768+Park+Center+Drive,+Orlando,+FL+32825&amp;iwloc=A&amp;hl=en" target="_blank" title="Driving Directions">Driving Directions</a></p>
                           
                           
                           <p>
                              <strong>Phone</strong><br>
                              <span>407-582-8033</span>
                              
                           </p>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/equalaccessequalopportunity.pcf">©</a>
      </div>
   </body>
</html>