<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Organizational Development &amp; Human Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/human-resources/employmentverifications-verifiers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/human-resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Organizational Development &amp; Human Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/human-resources/">Human Resources</a></li>
               <li>Organizational Development &amp; Human Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
                        <p>Please note that our organization uses The Work Number® to provide automated employment
                           and income verifications. The Work Number reduces our risk of liability from providing
                           erroneous or unauthorized information, and our employees receive the benefit of rapid
                           verification completion, 24 hours a day, 7 days a week. You the verifier benefit by
                           receiving immediate access to information that is convenient, accurate and secure.
                           Please follow the instructions below to attain the information you need. 
                        </p>
                        
                        
                        <p><em>How to Use The Work Number</em></p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Verification Type</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Access Options</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Information Required</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Commercial <br>
                                          </strong><em>mortgage loan, auto finance, credit card, job offer, apartment lease, etc.</em><strong> </strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.theworknumber.com/verifiers" target="_blank"><strong>www.theworknumber.com</strong></a><strong> </strong></p>
                                    
                                    <p>1-800-367-5690</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Employer Name or Code<br>
                                          </strong><em>Valencia College’s Employer Code is 17357</em> <br>
                                       <br>
                                       
                                    </p>
                                    
                                    <p><strong>Employee’s Social Security Number</strong> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Social Services<br>
                                          </strong><em>Medicaid, SNAP, TANF, subsidized housing, etc. <br>
                                          </em><em>(only available to qualifying assistance agencies)</em> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.theworknumber.com/verifiers" target="_blank"><strong>www.theworknumber.com</strong></a><strong> </strong></p>
                                    
                                    <p>1-800-660-3399</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong><br>
                              </strong><em>Frequently Asked Questions</em></p>
                        
                        <strong>            </strong>
                        
                        <p><strong>What is The Work Number? </strong></p>
                        
                        <p>The Work Number, a service of Equifax Workforce Solutions, is an automated service
                           for employment and income verifications that allows employees to provide proof of
                           employment or income instantly. More than 200,000 credentialed verifiers (leading
                           mortgage companies, pre-employment screeners, banks, social service agencies and others)
                           access The Work Number to retrieve this critical decisioning information—direct from
                           employer payroll feeds—<br>
                           rapidly and securely. 
                        </p>
                        
                        <p><strong>Is the information secure?</strong></p>
                        
                        <p>The commitment to information security at Equifax is unparalleled. We are SSAE16,
                           FISMA NIST 800-53 and ISO 27001 certified. We employ risk based authentication and
                           data encryption technologies and house The Work Number data in an isolated network.
                        </p>
                        
                        <p><strong>Who is considered a verifier?</strong></p>
                        
                        <p>A verifier can be any lending institution, property manager or <br>
                           other business with a Fair Credit Reporting Act (FCRA)-compliant permissible purpose
                           for requesting employment or income information. All verifiers are screened and credentialed
                           before receiving system access, and authenticated at each login. 
                        </p>
                        
                        <p><strong>Does a verification from The Work Number take longer?</strong></p>
                        
                        <p>No. If the information is on the database, it is delivered instantly and does not
                           require a callback or other response from the employer.<strong> </strong></p>
                        
                        <p><strong>What is a Commercial verification?</strong> 
                        </p>
                        
                        <p>Standard employment and income verifications (as part of the approval process for
                           mortgage loans, auto financing, credit cards, <br>
                           job offers, apartment rentals, etc.) are commonly referred to as commercial verifications.
                        </p>
                        
                        <p><strong>What is a Social Services verification?</strong></p>
                        
                        <p>Typical social services verifications include TANF, SNAP, Public Housing, Medicaid,
                           Child Support, WIC, Welfare-to-Work Programs, Social Security, Supplemental Security
                           Income (SSI), Social Security Disability Insurance (SSDI) and others.
                        </p>
                        
                        <p><strong>Do I need anything special to obtain an income verification?</strong> <br>
                           The Work Number requires that verifiers have the documented consent of the employee-consumer
                           to access income information. Consent is generally established via a signed acknowledgement
                           at the point of application for a loan or service. And, as with all verifications
                           via The Work Number, you will need to state an FCRA-compliant permissible purpose
                           prior to accessing the data. 
                        </p>
                        
                        <p><strong>What Employer Code do I Use?</strong></p>
                        
                        <p><strong>Valencia College’s </strong>employer code is: 17357.<strong> </strong>This five-digit code is used to identify each organization in The Work Number database.
                           Credentialed verifiers can also search for employer codes by name.
                        </p>
                        <br>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <p>HOURS &amp; LOCATION</p>
                           
                           
                           
                           <p>
                              <strong>Hours</strong><br>
                              <span>Monday - Friday: 8 AM to 5 PM</span>
                              
                           </p>
                           
                           <p>
                              <strong>Location</strong>
                              
                           </p>
                           
                           <p>District Office<br>
                              1768 Park Center Drive<br> Orlando, FL 32825
                           </p>
                           
                           <p><a href="http://maps.google.com/maps?q=1768+Park+Center+Drive,+Orlando,+FL+32825&amp;iwloc=A&amp;hl=en" target="_blank" title="Driving Directions">Driving Directions</a></p>
                           
                           
                           <p>
                              <strong>Phone</strong><br>
                              <span>407-582-8033</span>
                              
                           </p>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/human-resources/employmentverifications-verifiers.pcf">©</a>
      </div>
   </body>
</html>