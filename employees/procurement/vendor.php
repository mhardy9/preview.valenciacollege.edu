<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/vendor.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a> <a href="index.html"></a>
                        
                        <h2>Vendors</h2>
                        
                        <p>Thank you for your interest in doing business with Valencia College!</p>
                        
                        <h3>Registration</h3>
                        
                        <p>Suppliers  interested in providing goods and/or services to the College in the future
                           are encouraged  to register with Valencia's vendor registration system, Vendorlink
                           to receive  free notifications of bid opportunities.&nbsp;  In order to become a registered
                           supplier, please click on the Vendor  Registration tab. 
                        </p>
                        
                        <p> Suppliers  that have been awarded a contract, <strong><u>or  coordinating with a Valencia department to begin providing goods and/or  services
                                 should complete the Supplier Business Profile Form and W-9, which will</u></strong> enable the College to issue purchase orders and checks in the Banner ERP  System.&nbsp;
                           <strong>The supplier must include the department name and contact person who  the supplier
                              is working with.</strong> 
                        </p>
                        
                        <p> <em>&nbsp;Note: Completing these forms do not guarantee  future business with the college.&nbsp;&nbsp;
                              </em> 
                        </p>
                        
                        <p> <strong><a href="documents/Supplier-Business-Profile-R4-21-2017.pdf">Supplier Business Profile</a></strong> and <strong><a href="documents/BlankSubstituteW9form.pdf">Substitute  W-9 Form</a></strong> 
                        </p>
                        
                        <p> Remember to keep your supplier data up-to-date.&nbsp; To  avoid the emails being discarded
                           by junk mail filters, we encourage suppliers  to make the necessary adjustments to
                           their e-mail inbox settings to allow  emails of purchase orders in the future. 
                        </p>
                        
                        <p> Suppliers and Valencia departments with questions regarding the registration process,
                           requiring assistance completing the Vendor Profile Form, inquiring as to the  status
                           of the Profile Form or with any other supplier related question or  concern, may reach
                           out to Valencia Procurement at <a href="mailto:ValenciaProcurement@valenciacollege.edu">ValenciaProcurement@valenciacollege.edu</a>. 
                        </p>
                        
                        <p> <strong><em>Please download the "How to Do Business Guide" to learn more about the Valencia College
                                 vendor process - </em></strong><a href="documents/HowtodoBuisnessGuideR09-21-15.pdf"><strong><em>How  to do Buisness Guide</em></strong></a><strong> </strong> 
                        </p>
                        
                        <h3>Sustainability</h3>
                        
                        <p>Valencia College has adopted Environmental Procurement Procedures to improve resource
                           efficiency and support recyling. The College encourages all vendors to help support
                           our sustainability goals: &nbsp;to obtain the high quality products and services at a cost
                           that represents the best possible value, while maintaining high ethical standards,
                           and taking our social and environmental responsibilities seriously.&nbsp; 
                        </p>
                        
                        <p><strong>Tips:</strong> Use   materials (i.e. paper, dividers, brochures) that contain post-consumer recycled
                           content and that are readily recyclable. The College discourages the use of materials
                           that cannot be readily recycled such as PVC (vinyl) binders, spiral bindings, and
                           plastic or glossy covers or dividers. Printed and photocopied documents relating to
                           the fulfillment of  bids should  use recycled paper, be double-sided, and   comply
                           with the provisions of the College's Environmental Procurement Procedures. Potential
                           bidders are encouraged to print on both sides of a single sheet of paper wherever
                           applicable. 
                        </p>
                        
                        <p><a href="documents/ValenciaCollegeEnvironmentalProcurementProcedure.doc">Valencia College Environmental Procurement Procedure</a></p>
                        
                        <h3><strong>Supplier Diversity</strong></h3>
                        
                        <p>Valencia College is committed to the maximum utilization 
                           of our diverse supplier communities. We encourage our 
                           departments to utilize local, minority, disabled veteran, and women owned businesses.
                        </p>
                        
                        <h3>Payments</h3>
                        
                        <p>Payment is made after acceptance, inspection and receipt of goods and services. A
                           Purchase Order is issued to the supplier and reference to the Purchase Order number
                           must be included on all invoices. The College may also use the Procurement Card for
                           payment of low dollar items. 
                        </p>
                        
                        <p>The Accounts Payable Department is solely responsible for the payment 
                           of invoices associated with the purchase order. Any questions concerning an invoice
                           should be directed 
                           to 407-582-3379. All invoices are to be mailed to: 
                        </p>
                        
                        <blockquote>
                           
                           <p>Valencia College<br>
                              Accounts Payable Department<br>
                              P.O. Box 3028<br>
                              Orlando, Florida<br>
                              32802-3028
                           </p>
                           
                        </blockquote>
                        
                        <ul>
                           
                           <li><a href="documents/2017ResaleCertificate.pdf">2017 Florida Annual Resale Certificate for Sales Tax</a></li>
                           
                           <li> <a href="documents/ValenciaCollegePurchaseOrderTermsandConditions.pdf">Valencia College Purchase Order Terms and Conditions</a>
                              
                           </li>
                           
                           <li> <a href="documents/TEMPORARYEVENTLOGO2013.doc">Orange County, Florida Department of Health Temporary Event Permit</a> 
                           </li>
                           
                           <li>
                              <a href="documents/W-9ValenciaCollege070116.pdf">Valencia College 2016 W-9</a> 
                           </li>
                           
                           <li><a href="documents/CreditApplicationLetterR11-16-16.pdf">Valencia College Credit Application Letter</a></li>
                           
                        </ul>
                        
                        <h3>State Statutes and Related Links:</h3>
                        
                        <ul>
                           
                           <li>2 CFR Part 200 - <a href="https://federalregister.gov/a/2013-30465">Grant Requirements</a>
                              
                           </li>
                           
                           <li>ESREF - <a href="http://www.fldow.org/edfacil/sref.asp">State Requirements for Educational Facilities</a>
                              
                           </li>
                           
                           <li>119 - <a href="http://www.leg.state.fl.us/Statutes">Florida Public Records Law </a>
                              
                           </li>
                           
                           <li>120.57(3) - <a href="http://www.leg.state.fl.us/Statutes">Single Source </a>
                              
                           </li>
                           
                           <li>287.055 - <a href="http://www.leg.state.fl.us/Statutes">Consultant's Competitive Negotiation Act </a>
                              
                           </li>
                           
                           <li>287.087 - <a href="http://www.leg.state.fl.us/Statutes">Drug Free Workplace </a>
                              
                           </li>
                           
                           <li>287.133 - <a href="http://www.leg.state.fl.us/Statutes">Public Entity Crimes </a>
                              
                           </li>
                           
                           <li>287.135 - <a href="http://www.leg.state.fl.us/Statutes">Prohibition Against Scrutinized Companies </a>
                              
                           </li>
                           
                           <li>215.473 - <a href="http://www.leg.state.fl.us/Statutes">Sudan List, Scrutinized Companies List </a>
                              
                           </li>
                           
                           <li>607.1501 - <a href="http://www.leg.state.fl.us/Statutes">Foreign Corporations</a>
                              
                           </li>
                           
                           <li>112.313 - <a href="http://www.leg.state.fl.us/Statutes">Standards of Conduct for Public Officers, Employees</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/vendor.pcf">©</a>
      </div>
   </body>
</html>