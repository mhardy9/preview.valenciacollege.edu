<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/office-depot.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a> <a href="index.html"></a>
                        
                        <h3>Office Depot</h3>
                        
                        <p><strong>New Office Supply Contract </strong></p>
                        
                        <p>The State of Florida recently rebid the contract for office supplies.&nbsp; The good news,
                           the College will not need to change suppliers.&nbsp; Office Depot was awarded a contract
                           which becomes effective April 18, 2017 that includes over 20,500 items.&nbsp; The pricing
                           discounts were based on product categories with discounts ranging from 39% to 85%
                           off of the original equipment manufacturer’s price list. 
                        </p>
                        
                        <p>The product categories and comparisons of current and new discounts are posted below.&nbsp;
                           An analysis of the new discounts based on current spending trends, results in a total
                           savings to the College <em>of $33,738.</em></p>
                        
                        <p>The State made one significant change to the new contract requirements.&nbsp; The new contract
                           price list will be all inclusive and non-contract items will not be made available
                           for  purchase.
                        </p>
                        
                        <p>When searching for item, keep your search terms generic and broad, such as pens, pencils,
                           etc.&nbsp; For example, when you search for an item, such as a note pad, there will be
                           multiple notepads from which to choose in the resulting search.&nbsp; If you search by
                           a specific manufacturer, you may not find the exact match.&nbsp; 
                        </p>
                        
                        <p>The State is allowing Office Depot to add items to the office supply contract on a
                           periodic basis. The State’s purpose is to standardize and take advantage of true contract
                           discount savings, thereby helping you stretch your budget.&nbsp; Office Depot has negotiated
                           deep discounts with multiple manufacturers.&nbsp; Purchasing a non-contracted product will
                           cost much more, therefore, the State removed the option to buy non-contracted products.
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Product Category</th>
                                 
                                 <th>Current OEM List Less %</th>
                                 
                                 <th>New SOF OEM List Less %</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Breakroom &amp; Cleaning Supplies</td>
                                 
                                 <td>40%</td>
                                 
                                 <td>40%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Folders, Binders &amp; Accessories</td>
                                 
                                 <td>70%</td>
                                 
                                 <td>71%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Office Consumables (Art, Educational &amp; Envelopes)</td>
                                 
                                 <td>66%</td>
                                 
                                 <td>66%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Office Equipment</td>
                                 
                                 <td>30%</td>
                                 
                                 <td>39%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IT Peripherals</td>
                                 
                                 <td>34%</td>
                                 
                                 <td>39%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Paper – Other</td>
                                 
                                 <td>60%</td>
                                 
                                 <td>69%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Paper – White, Recycled and Virgin</td>
                                 
                                 <td>NA</td>
                                 
                                 <td>85%</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        You can access the Office Depot ordering site at the following link: <u><a href="https://bsd.officedepot.com">ttps://bsd.officedepot.com</a></u> Contact information for OFFICE DEPOT:
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Representative</th>
                                 
                                 <th>Name</th>
                                 
                                 <th>Email</th>
                                 
                                 <th>Phone # </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Field/Local</td>
                                 
                                 <td>Lissette Lantieri </td>
                                 
                                 <td><a href="mailto:lissette.lantieri@officedepot.com">lissette.lantieri@officedepot.com</a></td>
                                 
                                 <td>407-701-5745</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Field/Local</td>
                                 
                                 <td>Tammy Miller </td>
                                 
                                 <td><a href="mailto:tammy.miller@officedepot.com">tammy.miller@officedepot.com</a></td>
                                 
                                 <td>407-432-3773</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>National Customer Service </td>
                                 
                                 <td colspan="2">
                                    <ul>
                                       
                                       <li>Ordering Questions</li>
                                       
                                       <li>Delivery Issues</li>
                                       
                                       <li>Returns</li>
                                       
                                       <li>Damaged Items</li>
                                       
                                    </ul>
                                 </td>
                                 
                                 <td>888-777-4044</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        Contact Procurement for the following:
                        
                        <ul>
                           
                           <li>Creation of an Office Depot account </li>
                           
                           <li>Update your account</li>
                           
                           <li>Password Reset </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/office-depot.pcf">©</a>
      </div>
   </body>
</html>