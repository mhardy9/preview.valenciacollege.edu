<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/toner-cartridges.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        <p><strong>Toner and Ink Cartridges</strong></p>
                        
                        <p>Valencia College has awarded a contract to <strong><u>The Office Pal</u></strong> for ink &amp; toner print cartridges. The Office Pal has a vast inventory and can provide
                           all types of ink and toner to fulfill our needs.This Awarded Contract will provide
                           the College with over $30,000 in savings.
                        </p>
                        
                        <p>This contract will replace the current contracts with Ace Office Supplies, Apex Office
                           Supplies, Central Florida Data Supply, and S&amp;S Graphics. <em>Please check with The Office Pal for your needs before choosing alternate vendors</em>. 
                        </p>
                        
                        <p><strong>How to Order</strong></p>
                        
                        <p><u>Online Ordering</u>:
                        </p>
                        
                        <p>Contact the Procurement Department at <a href="mailto:valenciaprocurement@valenciacollege.edu">ValenciaProcurement@ValenciaCollege.edu</a> <u>to receive the link to create your sub-account under our main Procurement Account.
                              This will ensure you are getting the contracted pricing. </u>All pricing includes shipping.&nbsp; Should your order show shipping charges, please contact
                           customer service to assist with removing these charges. A P-Card or Purchase Order
                           can be used for purchases.
                        </p>
                        
                        <p><u>By Phone/Email</u>:
                        </p>
                        
                        <p>Contact the Customer Service department at the information below.</p>
                        
                        
                        <p><em><a href="http://valenciacollege.edu/procurement/documents/PricingSheet.pdf">**Click here for Contract Price List</a></em></p>
                        
                        
                        <p><strong>Questions?</strong></p>
                        
                        <p><strong><u>Customer service:</u></strong></p>
                        
                        <p>(P) 877-486-0590</p>
                        
                        <p>(e) <a href="mailto:Sales@theofficepal.com">Sales@theofficepal.com</a> 
                        </p>
                        
                        
                        <p><strong><u>Escalated issues</u></strong><u>:</u></p>
                        
                        <p>Latzie Tober</p>
                        
                        <p>(P) 877-486-0590 ext. 108</p>
                        
                        <p>(e) <a href="mailto:Latzie@theofficepal.com">Latzie@theofficepal.com</a> 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/toner-cartridges.pcf">©</a>
      </div>
   </body>
</html>