<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/procurementcard.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        
                        
                        
                        <h2>Procurement Card</h2>
                        
                        <h3>Purpose</h3>
                        
                        <p>The Procurement Card Program is designed to provide departments 
                           with a streamlined method of purchasing small dollar items, thereby 
                           reducing the volume of requisitions, purchase orders, invoices and 
                           checks processed by Valencia Staff.
                        </p>
                        
                        <p>All  employees, excluding temporary,  and 
                           volunteer employees of the College designated by their Vice President/Dean/Departmental
                           
                           Chair may be permitted to obtain a Procurement Card.
                        </p>
                        
                        
                        <h3>How it Works</h3>
                        
                        <p>A "Valencia College Procurement Card Application"
                           must be completed by the requesting cardholder and the agreement form for the appropriate
                           approver(s).
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Procurement Card Manual &amp; Forms</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/Purchasing-Card-Application-Form.pdf" target="_blank">Application Form</a></div>
                                 
                                 <div data-old-tag="td"><a href="documents/Procurement-Card-Cardholder-and-Approver-Agreement-Form.pdf" target="_blank">Agreement Form</a></div>
                                 
                                 <div data-old-tag="td"><a href="documents/Procurement-Card-Maintenance-Form.pdf" target="_blank">Maintenance Form </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/Purchasing-Card-Update-Form.pdf" target="_blank">Update Form</a></div>
                                 
                                 <div data-old-tag="td"><a href="documents/SageAgreementFormVer1.1Nov2014.pdf" target="_blank">SAGE - Agreement Form</a></div>
                                 
                                 <div data-old-tag="td"><a href="documents/Procurement-Card-Receipt-Exception-Form.pdf" target="_blank">Receipt Exception Form </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li><a href="documents/4.0Version.pdf" target="_blank">PCard Procedures &amp; Guidelines, October 2016 Version 4.0</a></li>
                                       
                                       <li><a href="documents/Procurement-Card-Training.pdf" target="_blank">P-Card Training</a></li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <p>Download Calendar: <a href="documents/Procurement-Card-Download-Schedule-FY-17-18.pdf" target="_blank">Download Schedule</a> for FY 17/18 
                        </p>
                        
                        <p>PaymentNet Specificatons: <a href="documents/20150929150903873.pdf" target="_blank">PaymentNet Specifications</a></p>
                        
                        <p>First Time Login Setup: <a href="documents/20150929150836240.pdf" target="_blank">First Time Login Setup</a></p>
                        
                        
                        
                        <h3>Current Card Holders</h3>
                        
                        <p>JPMorgan Chase's 24-hour Customer Service Team is available for 
                           assistance at: <strong>1-800-270-7760</strong></p>
                        
                        <p>PaymentNet questions should be directed towards Kellie Robertson at x1974 or Haresh
                           Singh
                           at x3887<br>
                           
                        </p>
                        
                        <p><a href="http://www.paymentnet.jpmorgan.com">JP Morgan/Chase PaymentNet 4</a> 
                        </p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/procurementcard.pcf">©</a>
      </div>
   </body>
</html>