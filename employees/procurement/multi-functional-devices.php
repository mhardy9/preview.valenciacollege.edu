<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/multi-functional-devices.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a> <a href="index.html"></a>
                        
                        <h2><strong>Multi-Functional Devices - MFDs</strong></h2>
                        
                        <h3>MFD Toner &amp; Staple Orders:</h3>
                        
                        <p>To request a new toner or staples for your MFD, please send an <strong>Email</strong> to Courier Services based on the chart below. 
                        </p>
                        
                        <p>In your email be sure to include the following:</p>
                        
                        <ul>
                           
                           <li>The 5-digit System I.D. number located on the MFD</li>
                           
                           <li>Name of person requesting</li>
                           
                           <li>Mail Code for delivery purposes.</li>
                           
                        </ul>
                        
                        <h3>West Campus Courier Services</h3>
                        
                        <p><i class="far fa-envelope fa-spacer"></i><a href="mailto:west_courierservices@valenciacollege.edu">west_courierservices@valenciacollege.edu</a> 
                        </p>
                        
                        <p>Servicing Campuses:</p>
                        
                        <ul>
                           
                           <li>West Campus</li>
                           
                           <li>District Office</li>
                           
                           <li>Fire Training Facility</li>
                           
                           <li>Osceola Campus</li>
                           
                           <li>Advance Manufacturing Training Center</li>
                           
                           <li>Poinciana Campus (Opening Fall 2017)</li>
                           
                        </ul>
                        
                        
                        <h3>East Campus Courier Services</h3>
                        
                        <p><i class="far fa-envelope fa-spacer"></i><a href="mailto:east_courierservices@valenciacollege.edu">east_courierservices@valenciacollege.edu</a> 
                        </p>
                        
                        <p>Servicing Campuses:</p>
                        
                        <ul>
                           
                           <li>East Campus</li>
                           
                           <li>School of Public Safety</li>
                           
                           <li>Winter Park Campus</li>
                           
                           <li>Lake Nona Campus</li>
                           
                        </ul>
                        
                        
                        <h3>MFD Service Calls:</h3>
                        
                        <p>If your MFD needs serviced or has an error message on the display, please call: 407-830-7950.
                           You may also send a request through Seminole Office Solutions Customer Center at <a href="https://www.sosfla.com/customer-center">https://www.sosfla.com/customer-center</a>. Be sure to have your System I.D. number, location, and error message (if possible)
                           before you call.
                        </p>
                        
                        <h3>MFD Relocation Requests:</h3>
                        
                        <p>OIT manages all requests for relocation of MFD'’'s. A Service Request must be completed
                           at least two weeks in advance of the scheduled office move. OIT will coordinate the
                           move of all MFD'’'s with Seminole Office.
                        </p>
                        
                        <h3>MFD Copy Codes</h3>
                        
                        <p>Departments are provided a 5-digit code specific to your Index. If you do not remember
                           your Department code or need a new code for a new department, please contact OIT or
                           complete a service request. OIT will provide the appropriate Code.
                        </p>
                        
                        <h3>Copy Paper Orders</h3>
                        
                        <p>Please email the Courier Services office based on the chart above.</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/multi-functional-devices.pcf">©</a>
      </div>
   </body>
</html>