<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/code.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        
                        
                        <h2>NAEP Code of Ethics</h2>
                        
                        <p>Purchasing Services believes that all persons in the procurement 
                           process at the college should have a highly developed sense of professional 
                           ethics to protect their own and Valencia's reputation for fair dealing. 
                           To strengthen ethical awareness, and to provide high standards of 
                           ethical practices, Valencia College subscribes to the 
                           National Association of Educational Procurement (NAEP) Code of Ethics.&nbsp; 
                           
                        </p>
                        
                        <ol>
                           
                           <li>Give first consideration to the objectives and policies of my institution.</li>
                           
                           <li>Strive to obtain the maximum value for each dollar of expenditure.</li>
                           
                           <li>Decline personal gifts or gratuities.</li>
                           
                           <li>Grant all competitive suppliers equal consideration insofar as state or federal statute
                              and institutional policy permit.
                           </li>
                           
                           <li>Conduct business with potential and current suppliers in an atmosphere of good faith,
                              devoid of intentional misrepresentation.
                           </li>
                           
                           <li>Demand honesty in sales representation whether offered through the medium of a verbal
                              or written statement, an advertisement, or a sample of the product.
                           </li>
                           
                           <li>Receive consent of originator of proprietary ideas and designs before using them for
                              competitive purchasing purposes.
                           </li>
                           
                           <li>Make every reasonable effort to negotiate an equitable and mutually agreeable settlement
                              of any controversy with a supplier; and/or be willing to submit any major controversies
                              to arbitration or other third party review, insofar as the established policies of
                              my institution permit.
                           </li>
                           
                           <li>Accord a prompt and courteous reception insofar as conditions permit to all that call
                              on legitimate business.
                           </li>
                           
                           <li>Cooperate with trade, industrial and professional associations, and with government
                              and private agencies for the purpose of promoting and developing sound business practices.
                           </li>
                           
                           <li>Foster fair, ethical and legal trade practices.</li>
                           
                           <li>Counsel and cooperate with NAEP members and promote a spirit 
                              of unity and a keen interest in professional growth among them.&nbsp;
                           </li>
                           
                        </ol>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/code.pcf">©</a>
      </div>
   </body>
</html>