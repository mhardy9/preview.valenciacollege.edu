<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/contracts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        <p>Procurement constantly seeks opportunities to enhance services and processes so that
                           we can exceed your expections. 
                        </p>
                        
                        <p>Note: For additional Procurement User Resources relative to Valencia College faculty/staff,
                           please refer to the Budget Tab in <a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=https%3A//ptl5pd-prod.valenciacollege.edu/c/portal/login">Atlas</a>.
                        </p>
                        
                        <hr>    
                        
                        <p>All contracts are regularly updated and maintained in the Procurement Department.
                           
                        </p>
                        
                        <p>Contractors doing business with the College, should become familiar with the College's
                           contractual terms and conditions.
                        </p>
                        
                        <p><a href="documents/ConsultantAgreementR02-18-2016.docx">Consultant Agreement </a></p>
                        
                        <p><a href="documents/ContinuingServicesTemplateR12-15-15.pdf">Continuing Services Contract </a></p>
                        
                        <p><a href="documents/IndependentContractorAgreementR12-15-15.pdf">Independent Contractor Agreement</a> (Used for student related activities)
                        </p>
                        
                        <p><a href="documents/SpeakerTemplate2015-Paid-fillable.pdf">Speaker Agreement </a></p>
                        
                        <p><a href="documents/AttachmentC-SpecialContractProvisionsGrants.pdf">Special Provisions for Grant related Contracts </a></p>
                        
                        
                        <p><strong>Insurance Requirements</strong> - Valencia has established Minimum Insurance Requirements based on the type of services
                           provided. Click here to view: <a href="documents/MinimunInsuranceRequirementsMatrixNov.2015.pdf">Insurance Matrix</a></p>
                        
                        
                        <p><strong>Awarded Contract List</strong> - <a href="documents/Updated11-15-16AwardedContractList.pdf">Updated Awarded Contract List</a></p>
                        
                        <p><strong>Rental Car Information</strong>: 
                        </p>
                        
                        <ol>
                           
                           <li>Valencia has utilized the Cooperative Agreement between the State of Florida and Enterprise
                              Rent-A-Car/National Rent-A-Car.
                              
                              <ul>
                                 
                                 <li><a href="documents/PriceSheet_EnterpriseNational_Eff.08-27-2015.pdf">Pricing Page</a></li>
                                 
                                 <li>The Contract Number and steps necessary for making a rental reservation with either
                                    Enterprise Rent-A-Car or National Rent-A-Car can be referenced through the "Rental
                                    Car Information" PDF under the Budget Tab in <a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=https%3A//ptl5pd-prod.valenciacollege.edu/c/portal/login">Atlas</a>. <br>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ol>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/contracts.pcf">©</a>
      </div>
   </body>
</html>