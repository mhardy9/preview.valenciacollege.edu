<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/printingservices.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Printing Services</h2>
                        
                        
                        <p>Valencia College has awarded a contract to  Xperient (Central Florida's premier print
                           provider) for college-wide print services.
                        </p>
                        
                        <p> Passionate about quality printing and service, Xperient is looking forward to serving
                           you. 
                        </p>
                        
                        
                        <p>Xperient offers a variety of printing services to include, but are not limited to:</p>
                        
                        <ul>
                           
                           <li>BUSINESS CARDS</li>
                           
                           <li> LETTERHEAD STATIONERY with Valencia logo </li>
                           
                           <li>ENVELOPES with Valencia logo</li>
                           
                           <li> CERTIFICATES with Valencia logo</li>
                           
                           <li>POSTERS (a variety of sizes available)</li>
                           
                           <li>BANNERS (a variety of sizes available)</li>
                           
                           <li>BROCHURES</li>
                           
                           <li>BINDING</li>
                           
                        </ul>            
                        
                        
                        <p><strong>How to Order</strong> 
                        </p>
                        
                        <p>Online Stationery orders: <a href="http://www.xperient.com/#order">http://www.xperient.com/#order</a> 
                        </p>
                        
                        <p>Request a Quote for custom printing projects: <a href="http://www.xperient.com/#order">http://www.xperient.com/#order</a> 
                        </p>
                        
                        <p>Upload Files for on-demand printing projects: <a href="http://www.xperient.com/#order">http://www.xperient.com/#order</a><u> </u></p>
                        
                        <p><strong><em><a href="http://valenciacollege.edu/procurement/documents/HowtoOrderBusinessStationeryOnline.pdf">Click here for Ordering Instructions</a></em></strong></p>
                        
                        <p><strong><em><a href="http://valenciacollege.edu/procurement/documents/XperientPriceSheetWebsite7-15-16.xlsx">**Click here for Contract Price List </a></em></strong></p>
                        
                        
                        <p><strong>Questions? or Need to create an ACCOUNT? </strong></p>
                        
                        <p>Contact  Xperient directly with your print related questions and to request an account.&nbsp;
                           
                        </p>
                        
                        <p>Contact: Corinna (Cory) McCormick </p>
                        
                        <p>Email: <a href="mailto:print@xperient.com">print@xperient.com</a> 
                        </p>
                        
                        <p>Phone: 407-265-8000</p>
                        
                        <p>Website: www.xperient.com</p>
                        
                        
                        <p><em>Your dedicated Account Representative is looking forward to hearing from you.</em></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/printingservices.pcf">©</a>
      </div>
   </body>
</html>