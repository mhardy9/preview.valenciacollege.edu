<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/contact.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a> 
                        
                        <h2>Procurement Staff </h2>
                        
                        <div class="row">
                           
                           <div class="col-md-4">
                              
                              <ul class="list_staff">
                                 
                                 <li> 
                                    
                                    <figure>
                                       <img src="images/lautesha-strange.jpg" alt="Lautesha Strange Photo" class="img-circle"> 
                                       
                                    </figure>
                                    
                                    <h4>
                                       <a href="mailto:lstrange1@valenciacollege.edu">Lautesha Strange</a>
                                       
                                    </h4>
                                    
                                    <p>
                                       <span title="Degrees"><i class="far fa-certificate fa-spacer"></i>MPA, CPPB, FCPA</span><br>
                                       <span title="Title"><i class="far fa-globe fa-spacer"></i>Assistant Director</span><br>
                                       <span title="Phone"><i class="far fa-phone fa-spacer"></i>407-582-5527</span>
                                       
                                    </p>
                                    
                                 </li>
                                 
                                 <li> 
                                    
                                    <figure>
                                       <img src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available" class="img-circle"> 
                                       
                                    </figure>
                                    
                                    <h4>
                                       <a href="mailto:ccolon70@valenciacollege.edu">César Colón</a>
                                       
                                    </h4>
                                    
                                    <p>
                                       <span title="Degrees"><i class="far fa-certificate fa-spacer"></i>MS, CPSM</span><br>
                                       <span title="Title"><i class="far fa-globe fa-spacer"></i>Assistant Director</span><br>
                                       <span title="Phone"><i class="far fa-phone fa-spacer"></i>407-582-3469 </span>
                                       
                                    </p>
                                    
                                 </li>
                                 
                                 <li> 
                                    
                                    <figure>
                                       <img src="images/christy-colgan.jpg" alt="Christy Colgan Photo" class="img-circle"> 
                                       
                                    </figure>
                                    
                                    <h4>
                                       <a href="maito:ccolgan@valenciacollege.edu">Christy Colgan</a>
                                       
                                    </h4>
                                    
                                    <p>
                                       <span title="Title"><i class="far fa-globe fa-spacer"></i>Procurement Specialist </span><br>
                                       <span title="Phone"><i class="far fa-phone fa-spacer"></i>407-582-5542 </span><br><br>
                                       
                                    </p>
                                    
                                 </li>
                                 
                                 <li> 
                                    
                                    <figure>
                                       <img src="images/yolanda-custodio-guzman.jpg" alt="Yolanda Custodio-Guzman Photo" class="img-circle"> 
                                       
                                    </figure>
                                    
                                    <h4>
                                       <a href="maito:ycustodioguzman@valenciacollege.edu">Yolanda Custodio-Guzman</a>
                                       
                                    </h4>
                                    
                                    <p>
                                       <span title="Degrees"><i class="far fa-certificate fa-spacer"></i>C.P.M., CPSD, CPSM </span><br>
                                       <span title="Title"><i class="far fa-globe fa-spacer"></i>Purchasing Agent </span><br>
                                       <span title="Phone"><i class="far fa-phone fa-spacer"></i>407-582-3004 </span>
                                       
                                    </p>
                                    
                                 </li>
                                 
                                 <li> 
                                    
                                    <figure>
                                       <img src="images/kellie-robertson.jpg" alt="Valencia image description" class="img-circle"> 
                                       
                                    </figure>
                                    
                                    <h4>
                                       <a href="maito:krobertson9@valenciacollege.edu">Kellie Robertson</a>
                                       
                                    </h4>
                                    
                                    <p>
                                       <span title="Title"><i class="far fa-globe fa-spacer"></i>Procurement Coordinator/<br>P-Card Administrator </span><br>
                                       <span title="Phone"><i class="far fa-phone fa-spacer"></i>407-582-1974 </span><br>
                                       
                                    </p>
                                    
                                 </li>
                                 
                                 <li> 
                                    
                                    <figure>
                                       <img src="images/haresh-singh.jpg" alt="Valencia image description" class="img-circle"> 
                                       
                                    </figure>
                                    
                                    <h4>
                                       <a href="maito:hsingh5@valenciacollege.edu">Haresh Singh</a>
                                       
                                    </h4>
                                    
                                    <p>
                                       <span title="Title"><i class="far fa-globe fa-spacer"></i>Purchasing Card Assistant </span><br>
                                       <span title="Phone"><i class="far fa-phone fa-spacer"></i>407-582-3887 </span>
                                       
                                    </p>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-8">
                              
                              <h3>General Contact Information</h3>
                              
                              <span title="Fax Number"><i class="far fa-fax fa-2x fa-spacer"></i>Fax: 407-582-3007</span><br>
                              <span title="Email"><i class="far fa-envelope fa-2x fa-spacer"></i><a href="mailto:valenciaprocurement@valenciacollege.edu">Email Valencia Procurement</a></span><br>
                              <span title="Physical Address"><i class="far fa-building fa-2x fa-spacer"></i>Physical Address</span>
                              
                              <div class="box_style_2">District Office<br>
                                 1768 Park Center Drive <br>
                                 Mail Code: DO-38 <br>
                                 Orlando, FL&nbsp; 32835<br>
                                 
                              </div>
                              <span title="Mailing Address"><i class="far fa-2x fa-envelope-o fa-spacer"></i>Mailing Address</span>
                              
                              <div class="box_style_2">PO Box 3028<br>
                                 Orlando, FL 32802
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/contact.pcf">©</a>
      </div>
   </body>
</html>