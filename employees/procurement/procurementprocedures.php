<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/procurementprocedures.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        
                        
                        
                        <h2>Procurement Procedures</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Number</div>
                                 
                                 <div data-old-tag="th">Procedure Title</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">1.0</div>
                                 
                                 <div data-old-tag="td">Procurement Program </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">1.1</div>
                                 
                                 <div data-old-tag="td"><a href="documents/ValuesamdGuidingPrinciples1.2.pdf">Values and Guiding Principles of Public Procurement </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">2.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/ProcurementThreshold-2016.pdf">Procurement Thresholds</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">2.1</div>
                                 
                                 <div data-old-tag="td"><a href="documents/LeadTimeQuickReferenceGuide2.1.docx">Lead Time Reference Guide </a></div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">3.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/RequisitionProcedures3.0.pdf">Requisitions</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">3.1</div>
                                 
                                 <div data-old-tag="td"><a href="documents/StandingPurchaseOrderProc3.2.docx">Standing Purchase Orders </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">3.2</div>
                                 
                                 <div data-old-tag="td">Receiving and Inspections </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">3.3</div>
                                 
                                 <div data-old-tag="td">Exemptions and Cooperative Purchasing</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">4.0</div>
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/SoleSourceProcedures4.0R121316.pdf">Sole Source</a> 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">5.0</div>
                                 
                                 <div data-old-tag="td">Specifications </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">4.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/RequestforQuotationProc4.0.docx">Request for Quotations</a></div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">5.0</div>
                                 
                                 <div data-old-tag="td">Invitation for Bids</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">6.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/RequestforProposalProcess6.0.docx">Request for Proposals</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/CommFormationNonConflict70R61216.docx">Evaluation Committee Formation, Conflict of Interest</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7.1</div>
                                 
                                 <div data-old-tag="td">Conducting Evaluation Committee Meetings </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7.2</div>
                                 
                                 <div data-old-tag="td"><a href="documents/Part1EvaluationCommitteeInstructions7-12-16.docx">Instruction for Participation on Evaluation Committee </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7.3</div>
                                 
                                 <div data-old-tag="td"><a href="documents/Part2RFP_Evaluation_Committee_Confidentiality-No_Conflict_Certification2-13-15.docx">No Conflict of Interest Form </a></div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">8.0</div>
                                 
                                 <div data-old-tag="td">Request for Qualifications (RFQu)</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">9.0</div>
                                 
                                 <div data-old-tag="td">Invitation to Negotiate</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">10.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/4.0Version_000.pdf">Procurement Card Program</a></div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">11.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/Procurement-Procedures-Grant-Funding.pdf">Procedures for Grant Funding, 2 CFR Part 200 </a></div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">12.0</div>
                                 
                                 <div data-old-tag="td">Owner Direct Purchase Program</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">13.0</div>
                                 
                                 <div data-old-tag="td"><a href="documents/FLStatutesPertainingtoLocalProcurementServices.doc">Florida Statutes Applicable to Procurement </a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/procurementprocedures.pcf">©</a>
      </div>
   </body>
</html>