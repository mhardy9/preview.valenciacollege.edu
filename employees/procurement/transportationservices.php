<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Procurement | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/procurement/transportationservices.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/procurement/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Procurement</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/procurement/">Procurement</a></li>
               <li>Procurement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Transportation Services</h2>
                        
                        
                        <p><strong>Transportation Services</strong></p>
                        
                        <p><strong>Bus Transportation Services </strong></p>
                        
                        <p>Valencia College has awarded a contract with two vendors to provide bus transportation
                           services.
                        </p>
                        
                        <p>With great customer service, <em>Space Tours and Dynamic Tours</em>, are here to provide you with your transportation needs.
                        </p>
                        
                        <p><strong><u>To obtain a price quote please contact:</u></strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Space Tours – Effective&nbsp; January 08, 2015 </strong></p>
                                    
                                    <p><strong>Contact Information:</strong></p>
                                    
                                    <p>Maurice Vargas</p>
                                    
                                    <p>Phone: 407-903-9996</p>
                                    
                                    <p>Fax: 407-363-1440</p>
                                    
                                    <p><a href="mailto:spacetoursbus@gmail.com">spacetoursbus@gmail.com</a></p>
                                    
                                    <p>5381 Water Vista Drive</p>
                                    
                                    <p>Orlando, FL 32821</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Dynamic Tours – Effective January 08, 2015</strong></p>
                                    
                                    <p><strong>Contact Information:</strong></p>
                                    
                                    <p>Edna Dakkak</p>
                                    
                                    <p>Phone: 407-888-3500</p>
                                    
                                    <p>Fax: 407-363-3939</p>
                                    
                                    <p><a href="mailto:edakkak@dynamicbuslines.com">edakkak@dynamicbuslines.com</a></p>
                                    
                                    <p><a href="mailto:dynamictrans@att.net">dynamictrans@att.net</a></p>
                                    
                                    <p>175 Thorpe Road</p>
                                    
                                    <p>Orlando, FL 32824</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><strong>Rental Car Services – State of Florida Contract Number: 788111808-15-1</strong></p>
                        
                        <p><em>Contract Term: September 30, 2015 to September 29, 2020</em></p>
                        
                        <p>Valencia College entered into a Cooperative Agreement with Enterprise and National
                           Car Rental for in-state and out-of-state car rental services.
                        </p>
                        
                        <p>All rates include unlimited mileage and roadside assistance; liability coverage and
                           loss damage waiver. Please note that any utilization of any college contract is subject
                           to all applicable policies and procedures and may require prior written approval.
                        </p>
                        
                        <p>Enterprise offers Valencia employees a discount for personal use. &nbsp;Personal rentals
                           do not include insurance coverage (damage waiver and liability protection).
                        </p>
                        
                        <p><strong>Questions?</strong></p>
                        
                        <p><strong>Contact: </strong></p>
                        
                        <p>Customer Service number at 877-690-0064 or Denise Berthiaume at 407-770-2971</p>
                        
                        <p><strong>To view the contract numbers, please visit the Rental Car Link under the Employees
                              Tab in Atlas.</strong></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/procurement/transportationservices.pcf">©</a>
      </div>
   </body>
</html>