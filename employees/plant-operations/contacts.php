<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Plant Operations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/plant-operations/contacts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/plant-operations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Plant Operations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/plant-operations/">Plant Operations</a></li>
               <li>Plant Operations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        <h2>Contact Us</h2>
                        
                        <p>The Plant Operations Department is charged with providing and 
                           maintaining a high quality, clean and safe environment that supports 
                           and enhances the learning centered atmosphere at Valencia. 
                        </p>
                        
                        <p>Office hours of operation: East 8:00 - 5:00, West 8:00 - 5:00 Monday - Friday </p>
                        
                        <h3>Work Requests</h3>
                        
                        <p>Work requests for all campuses may be submitted by going to our 
                           web page at <a href="http://iwmvcc.valenciacollege.edu/ExternalWorkRequest.do">
                              http://iwmvcc.valenciacollege.edu/ExternalWorkRequest.do</a>. 
                        </p>
                        
                        <p>&nbsp;Please do not make requests for service by telephone 
                           (except for emergencies such as fire, smoke, water, etc.
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="th">Phone</div>
                                 
                                 <div data-old-tag="th">Fax</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">Services at East Campus, Winter Park Campus and 
                                    CJI 
                                 </div>
                                 
                                 <div data-old-tag="td">407-582-2242 <br>
                                    &nbsp;
                                 </div>
                                 
                                 <div data-old-tag="td">407-582-8924</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Services at the District Office and West Campus</div>
                                 
                                 <div data-old-tag="td">407-582-1446 </div>
                                 
                                 <div data-old-tag="td">407-582-1209</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Services at Osceola Campus and Lake Nona Campus </div>
                                 
                                 <div data-old-tag="td">407-682 - 4311 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">For after hours emergencies call <a href="../../security/emergency.html">Campus Security</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3><strong>East Campus</strong></h3>
                        
                        <p>Superintendent:  Boris Feijoo <a href="mailto:bfeijoo@valenciacollege.edu">bfeijoo@valenciacollege.edu</a> 
                        </p>
                        
                        <p>                Staff Assistant: Lynn Marlar <a href="mailto:lmarlar@valenciacollege.edu">lmarlar@valenciacollege.edu </a></p>
                        
                        <p>                Staff Assistant: Yvette Willis<a href="mailto:ywillis@valenciacollege.edu"> ywillis@valenciacollege.edu </a></p>
                        
                        <p>Supervisor, Trades &amp; Maintenance: Carlos Cuevas <a href="mailto:ccuevas@valenciacollege.edu">ccuevas@valenciacollege.edu</a></p>
                        
                        <p>      Supervisor, Custodial Services: Raul Martinez Adrover <a href="mailto:rmartinezadrover@valenciacollege.edu">rmartinezadrover@valenciacollege.edu</a></p>
                        
                        <p>                    Supervisor, Grounds Maintenance: Clarence Canada <a href="mailto:ccanada@valenciacollege.edu">ccanada@valenciacollege.edu</a></p>
                        
                        <h3><strong>West Campus</strong></h3>
                        
                        <p>Superintendent: Adam Talbot <a href="mailto:atalbot4@valenciacollege.edu">atalbot4@valenciacollege.edu</a> 
                        </p>
                        
                        <p>                Administrative Assistant: Stephanie Vazquez <a href="mailto:svazquez17@valenciacollege.edu">svazquez17@valenciacollege.edu</a> 
                        </p>
                        
                        <p>Staff Assistant: Lillian Lozada <a href="mailto:llozada5@valenciacollege.edu">llozada5@valenciacollege.edu</a> 
                        </p>
                        
                        <p>Staff Assistant: Debbie Conti <a href="mailto:dconti@valenciacollege.edu">dconti@valenciacollege.edu </a></p>
                        
                        <p>Supervisor, Trades &amp; Maintenance: Jim Nelson <a href="mailto:jnelson@valenciacollege.edu">jnelson@valenciacollege.edu</a></p>
                        
                        <p>Supervisor, Custodial Services: Melvin Scott <u><a href="mailto:mscott44@valenciacollege.edu">mscott44@valenciacollege.edu</a></u></p>
                        
                        <p>Supervisor, Grounds Maintenance: Earl Spatcher <a href="mailto:espatcher@valenciacollege.edu">espatcher@valenciacollege.edu</a>            
                        </p>
                        
                        <h3>
                           <strong>Osceola  Campus</strong> 
                        </h3>
                        
                        <p>Superintendent: Marty Campbell <a href="mailto:jcampbell79@valenciacollege.edu">jcampbell79@valenciacollege.edu</a></p>
                        
                        <p>  Staff Assistant: Marie Cala <a href="mailto:mcala@valenciacollege.edu">mcala@valenciacollege.edu</a></p>
                        
                        <p>Supervisor, Trades &amp; Maintenance : Arcadio Rivera <a href="mailto:arivera236@valenciacollege.edu">arivera236@valenciacollege.edu</a></p>
                        
                        <p>Supervisor, Custodial Services: Mary Hardaway
                           <a href="mailto:mhardaway@valenciacollege.edu">mhardaway@valenciacollege.edu</a></p>
                        
                        <p>Working Supervisor, Grounds Maintenance: Frank D. Amendolara <a href="mailto:fdamendolara@valenciacollege.edu">fdamendolara@valenciacollege.edu</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/plant-operations/contacts.pcf">©</a>
      </div>
   </body>
</html>