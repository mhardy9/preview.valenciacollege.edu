<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Since its inception, Learning Day, an annual event, has demonstrated Valencia&amp;rsquo;s commitment to learning and building our college community. By closing the campuses so that all faculty and staff can participate in learning and community service, all employees can be inspired and celebrate each other.">
      <meta name="Keywords" content="frequently, asked, questionslearning, day, employee, development college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/development/learning-day/FAQ.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/development/learning-day/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Day</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/development/">Development</a></li>
               <li><a href="/employees/development/learning-day/">Learning Day</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               
               <div class="container margin-60">
                  		
                  <div class="row">
                     
                     <h2>Frequently Asked Questions</h2>
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <h4>When is Learning Day 2017?</h4>
                              
                              <p>Learning Day is Friday, February 10, 2017. </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>Where is Learning Day 2017?</h4>
                                 
                              </div>
                              
                              <p>Learning Day 2017 will be held at the Renaissance Orlando at SeaWorld (6677 Sea Harbor
                                 Dr, Orlando,
                                 32821) in the Ocean’s Ballroom.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>Will the College be closed on Learning Day 2017?</h4>
                                 
                              </div>
                              
                              <p>All campus locations will be closed for Learning Day 2017, and no classes will be
                                 held. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>What are the directions to Learning Day 2017 From I-4 East? </h4>
                                 
                              </div>
                              
                              <ul>
                                 
                                 <li>Take I-4 East to Exit 71 Sea World - Central Florida Parkway</li>
                                 
                                 <li>Merge right onto Central Florida Pkwy</li>
                                 
                                 <li>Turn left on Westwood Blvd</li>
                                 
                                 <li>Right onto Florida Festival Drive (You will see Valencia Flags)</li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>What are the directions to Learning Day 2017 From I-4 West? </h4>
                                 
                              </div>
                              
                              <ul>
                                 
                                 <li>Take I-4 West to Exit 72 – State Rd 528</li>
                                 
                                 <li>Proceed to Exit 1 - International DriveProceed to Exit 1 - International Drive</li>
                                 
                                 <li> Go straight at the light and you will be on Westwood Blvd</li>
                                 
                                 <li>Continue on Westwood Blvd for about 1 mile</li>
                                 
                                 <li>Florida Festival Drive will be on the left hand side (You will see Valencia Flags)</li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>What are the directions to Learning Day 2017 From 528 West? </h4>
                                 
                              </div>
                              
                              <ul>
                                 
                                 <li>Take 528 West to Exit 1 - International Drive</li>
                                 
                                 <li>Make a left on International Drive</li>
                                 
                                 <li>Proceed to the next light and make a right on Westwood Blvd</li>
                                 
                                 <li>Continue on Westwood Blvd for about 1 mile</li>
                                 
                                 <li>Florida Festival Drive will be on the left hand side (You will see Valencia Flags)</li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <h4> Where can I park for Learning Day 2017?</h4>
                              
                              <p>Complementary parking will be provided on the Westwood Blvd. side of the hotel nearest
                                 to the Ocean’s
                                 Ballroom convention space. The fastest and most direct way to park is by using address
                                 6796 Florida
                                 Festival Drive, Orlando, 32887, to reach our dedicated parking. Please avoid using
                                 the hotel’s main
                                 entrance as this will result in a delayed arrival to the event. Please note that you
                                 may choose to
                                 walk from the parking lot to the hotel, but shuttles will be available. Please allow
                                 time for transit.
                                 Disabled parking will be available to those with a hang tag near the hotel. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>Am I required to attend Learning Day 2017?</h4>
                                 
                              </div>
                              
                              <p>All full-time employees are required to attend. It is considered a work day, and if
                                 an employee does
                                 not attend, a certificate of absence (COA) is required using either vacation or personal
                                 time (Staff –
                                 8 hours of leave; Faculty – 7 hours of leave). 
                              </p>
                              
                              <p>Part-time employees are invited and encouraged to attend, and all supervisors should
                                 work with their
                                 team to adjust schedules as needed for part-time employees who are planning to attend.
                                 If part-time
                                 employees do not plan to attend, a COA is not require
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>If I am a part-time employee, will I be compensated for attending Learning Day 2017?</h4>
                                 
                              </div>
                              
                              <p>Part-time non-insturctional staff members will be compensated for attendance at their
                                 hourly rate of
                                 pay and should work with their supervisor to have their normal schedule adjusted if
                                 they plan to
                                 attend. As part-time faculty members are compensated based on class contact hours,
                                 they are invited
                                 and welcome to attend. Attendance is voluntary and will be considered on their own
                                 time. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <h4>If afternoon events end early, will non-exempt employees be penalized? </h4>
                              
                              <p>All full-time employees will be paid for eight hours of work, even if volunteer or
                                 town hall events
                                 end early. Non-exempt employees should adjust their work schedules, so they can attend
                                 the full day.
                                 Any hours over 40 must be paid as overtime, even on Learning Day 2017. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>How do I register to attend Learning Day 2017 and choose a community service project?</h4>
                                 
                              </div>
                              
                              <p>Before registering for Learning Day 2017, employees should explore the service project
                                 options found
                                 <a href="service-projects.html">here</a>.
                              </p>
                              
                              
                              <p>Employees should <a href="https://learningday2017.eventbrite.com/" target="_blank">click here</a> to
                                 register and select their afternoon activity. Please note that Valencia’s coordinated
                                 service projects
                                 are available on a first-come, first-served basis.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>May I volunteer on my own or must I select one of Valencia’s service projects?</h4>
                                 
                              </div>
                              
                              <p>All employees may volunteer at the organization of their choice.</p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <h4>Am I required to participate in an afternoon activity?</h4>
                              
                              <p>Yes, all full-time employees must participate in an afternoon activity. Employees
                                 may either attend
                                 the town hall on inclusion and access, volunteer on their own or participate in one
                                 of Valencia’s
                                 service projects. 
                              </p>
                              
                              <p>If employees decide not to attend an afternoon activity, they must enter a COA for
                                 the time not
                                 worked.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4> Will I be reimbursed for in-district travel to Learning Day 2017 and/or to my individual
                                    community service project?
                                 </h4>
                                 
                              </div>
                              
                              <p>Yes. Since Learning Day 2017 is not held on our campuses, some employees may incur
                                 miles in excess of
                                 their normal commute. In these instances, employees should complete the In-district
                                 Travel Form, as is
                                 standard, with the appropriate documentation. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <div>
                                 
                                 <h4>How should I dress for Learning Day 2017?</h4>
                                 
                              </div>
                              
                              <p>To accommodate those who wish to participate in community service projects, casual
                                 attire is
                                 encouraged.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_2">
                              
                              <h4>Can I share my Learning Day 2017 experience on social media?</h4>
                              
                              <p>Yes. All employees are encouraged to share their experience on social media. If you
                                 do, please use
                                 the hashtag, #ValenciaLearningDay, in your post so your colleagues can share in your
                                 experience. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/development/learning-day/FAQ.pcf">©</a>
      </div>
   </body>
</html>