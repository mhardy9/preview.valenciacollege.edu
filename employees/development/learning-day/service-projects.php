<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Day  | Valencia College</title>
      <meta name="Description" content="Since its inception, Learning Day, an annual event, has demonstrated Valencia&amp;rsquo;s commitment to learning and building our college community. By closing the campuses so that all faculty and staff can participate in learning and community service, all employees can be inspired and celebrate each other.">
      <meta name="Keywords" content="learning, day, employee, development college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/development/learning-day/service-projects.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/development/learning-day/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Day</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/development/">Development</a></li>
               <li><a href="/employees/development/learning-day/">Learning Day</a></li>
               <li>Learning Day </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-60">		
                  		
                  <div class="row">
                     
                     <h2> Service Projects</h2>
                     
                     <p>During the afternoon of Learning Day, you may select one of three options: attend
                        a town hall on inclusion
                        and access at the hotel, volunteer on your own in the community or participate in
                        one of Valencia’s selected
                        service projects. Please note that Valencia’s coordinated service projects will be
                        offered on a first-come,
                        first-served basis. Please note your preference when you register.
                     </p>
                     
                     <div>
                        
                        <h3>A Gift for Teaching</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>A Gift for Teaching is Central Florida’s primary provider of free school supplies.</p>
                        
                        <p>We will be assisting in the main facility, which includes the store and the warehouse.
                           Volunteers will
                           work in the warehouse; re-stock shelves in the store; or put together boxes of items
                           for schools. Please
                           download the volunteer packet, complete and sign the release form (included in the
                           packet) and bring it
                           with you to the volunteer site.
                        </p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 6501 Magic Way, Orlando, 32809
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>Clean the World</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>By recycling soap and other discarded hygiene projects, Clean the World saves lives
                           with items that
                           traditionally end in landfills.
                        </p>
                        
                        <p>Valencia volunteers will clean the gently used soap from hotels with table knives
                           for families living in
                           poverty around the world. Please note that this work will be done while standing.
                        </p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 400A Pittman Street, Orlando, 32801
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>New Hope for Kids</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p> New Hope for Kids brings hope, healing and happiness to children and families suffering
                           from grief, loss
                           or life-threatening illnesses through two programs:
                        </p>
                        
                        <ul>
                           
                           <li>The Center for Grieving Children, which helps children and families cope with the
                              feelings of grief
                              and loss after the death of a loved one, and
                              
                           </li>
                           
                           <li>Wishes for Kids, which grants wishes to children up to age 19, diagnosed with a life-threatening
                              illness.
                              
                           </li>
                           
                        </ul>
                        
                        <p>Volunteers will help with outside activities including raking, cutting grass, pulling
                           weeds, planting,
                           bagging leaves, picking up tree limbs and trash, trimming shrubs and blowing the parking
                           lot. Some
                           volunteers will work inside disinfecting toys, wiping down the hurricane room and
                           prepping crafts for
                           upcoming grief activities.
                        </p>
                        
                        <p>Please bring gloves for outside work.</p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 544 Mayo Ave, Maitland, 32751
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>The Mustard Seed of Central Florida</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>The Mustard Seed collects donations of furniture, appliances, household items, toys
                           or anything and
                           everything that is needed to create a home.
                        </p>
                        
                        <p>The organization receives large quantities of mattresses from hotels, homes and manufacturers.
                           Mattresses
                           that are in good shape are given to families in need. The rest are deconstructed,
                           and the materials are
                           bundled and sold for recycling, helping to generate income for their client programs.
                        </p>
                        
                        <p>Valencia’s volunteers will assist in the recycling facility, although if anyone would
                           like a
                           less-physical task, the organization will have several opportunities to help in the
                           office with data entry
                           and/or in the thrift store to sort, tag and hang donated clothing.
                        </p>
                        
                        <p>Please bring a water bottle and wear comfortable clothing and closed toed shoes.</p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 12 Mustard Seed Lane, Orlando, 32810
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>United Against Poverty</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>United Against Poverty provides crisis care, case management, transformative education,
                           food and
                           household subsidy, employment training and placement and personal empowerment training
                           to inspire and
                           empower people living in poverty to lift themselves and their families to economic
                           sufficiency. 
                        </p>
                        
                        <p>Valencia’s volunteers will help sort food products and stock shelves.</p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 150 W. Michigan Street, Orlando, 32806
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>Second Harvest Food Bank of Central Florida</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p> Second Harvest Food Bank collects and distributes donated food to more than 550 nonprofit
                           partner
                           agencies in six Central Florida counties.
                        </p>
                        
                        <p>Volunteers will help sort donations.</p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 411 Mercy Dr, Orlando, 32805
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>Boys and Girls Club of America Poinciana Teen Center</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>Boys &amp; Girls Clubs of Central Florida has helped inspire hope and opportunity in those
                           who need it
                           most – at-risk children in our local community.
                        </p>
                        
                        <p>Valencia volunteers will help with cleaning the center’s floors by sweeping, mopping,
                           scrubbing or
                           vacuuming; gathering and emptying trash; and service, clean and supply restrooms,
                           as needed. This
                           volunteer opportunity will take place from 3 - 5 p.m.
                        </p>
                        
                        <p>Please note that volunteers may be in contact with chemical cleaners and general cleaning
                           equipment, so
                           dress to clean. Also, moving furniture, equipment, and supplies, either manually or
                           by using equipment, is
                           possible. There is also a possibility of doing work outside — mowing, trimming lawns
                           and shrubbery, and
                           clearing debris from grounds.
                        </p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 2190 S. Marigold Avenue, Kissimmee, 34759
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>Ronald McDonald House at Nemours Children's Hospital</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>The Ronald McDonald House Charities of Central Florida has come to mean hope, healing,
                           and strength for
                           over 23,000 families.
                        </p>
                        
                        <p>Volunteers are needed to prepare the House for Valentine's Day (donations are needed
                           as well). Activities
                           for the day included decorating the house for Valentine’s Day, creating casseroles
                           with the ingredients
                           that we bring and general house cleaning.
                        </p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 13551 Nemours Parkway, Orlando, 32827 
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>Central Avenue Elementary</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>As an extension of Literacy Week, Central Avenue Elementary is asking for volunteers
                           to be guest readers
                           in the classroom and conduct "read-alouds" with students from Pre-K, including English
                           as a second
                           language students, to fifth grade. Volunteers can share their favorite, grade appropriate
                           book, or the
                           school can provide one.
                        </p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 500 West Columbia Ave, Kissimmee, 34741
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <div>
                        
                        <h3>Clancy &amp; Theys</h3>
                        
                     </div>
                     
                     <div>
                        
                        <p>Clancy &amp; Theys is a construction company who is looking for volunteers to help update
                           their
                           construction plans. Please note that this volunteer opportunity will happen at their
                           construction trailer,
                           located at the Poinciana Campus.
                        </p>
                        
                        <p><i class="far fa-map-marker"></i>&nbsp;Address: 3255 Pleasant Hill Rd, Kissimmee, 34746
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/development/learning-day/service-projects.pcf">©</a>
      </div>
   </body>
</html>