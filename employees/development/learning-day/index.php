<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Day  | Valencia College</title>
      <meta name="Description" content="Since its inception, Learning Day, an annual event, has demonstrated Valencia&amp;rsquo;s commitment to learning and building our college community. By closing the campuses so that all faculty and staff can participate in learning and community service, all employees can be inspired and celebrate each other.">
      <meta name="Keywords" content="learning, day, employee, development college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/development/learning-day/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/development/learning-day/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Day</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/development/">Development</a></li>
               <li>Learning Day</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60">
                  
                  <div class="row">
                     
                     <div class="col-md-8">
                        
                        <p>Save the date for Learning Day 2018, coming to a campus near you on Friday, February
                           9, 2018.
                        </p>
                        
                        <p>This year, Learning Day&nbsp;will be returning&nbsp;to the campus model. Information about campus-specific
                           times and programming will be posted as it becomes available.
                        </p>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <h3>Join Us</h3>
                        
                        <p>Date: Friday, February 9, 2018<br> Time: 8 a.m. – 5 p.m.<br> Location: Your Home Campus
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div>
                     	
                     <h3>West Campus Planning for Learning Day 2018</h3>
                     	
                     <p>A Message from Falecia Williams, President, West and Downtown Campuses</p>
                     
                     <p>Learning Day will be on Friday, February 9, 2018. As in 2016, each region will host
                        its own event. The West/Downtown event will tentatively feature:
                     </p>
                     
                     <table class="table table">
                        	
                        <tr>
                           <th scope="col">Time</th>
                           <th scope="col">Event</th>
                        </tr>
                        	
                        <tr>
                           <td>8 - 9 a.m.</td>
                           <td>Breakfast and networking</td>
                        </tr>
                        	
                        <tr>
                           <td>9 - 10:15 a.m.</td>
                           <td>Welcome and opening session</td>
                        </tr>
                        	
                        <tr>
                           <td>10:30 - 11:15 a.m.</td>
                           <td>Learning session I</td>
                        </tr>
                        	
                        <tr>
                           <td>11:30 - 12:15 a.m.</td>
                           <td>Learning session II</td>
                        </tr>
                        	
                        <tr>
                           <td>12:15 - 1:45 p.m.</td>
                           <td>Lunch and networking</td>
                        </tr>
                        	
                        <tr>
                           <td>2 - 2:45 p.m.</td>
                           <td>Learning session III or community service</td>
                        </tr>
                        	
                        <tr>
                           <td>3 - 3:45 p.m.</td>
                           <td>Learning session IV or community service</td>
                        </tr>
                        	
                        <tr>
                           <td>4 - 5 p.m.</td>
                           <td>Personal reflection</td>
                        </tr>
                        
                     </table>
                     
                     <p>The West Campus planning team is soliciting presentation proposals that fall under
                        one of the approved learning session themes of health and wellness; service in the
                        community; personal interest; technology, creativity and innovation; campus plan updates
                        and strategic initiatives; scholarship of teaching and learning; and training and
                        development. Click the button below to submit a presentation proposal by Friday, January
                        5, 2018.<a href="https://valenciacc.ut1.qualtrics.com/jfe/form/SV_6mViJ16IN5KbNI1" target="_blank" rel="noopener"><img class="responsive" src="http://thegrove.valenciacollege.edu/files/2015/12/ld16-wc-proposal-button-grove.png" alt="" width="570" height="172"></a></p>
                     	
                     <p>If you have questions, please contact Carla McKnight, campus director, organizational
                        development and human resources, at <a href="mailto:cmcknight5@valenciacollege.edu" target="_blank" rel="noopener">cmcknight5@valenciacollege.edu</a> or 407-299-5000, extension 1756, or Rose Quiles, assistant campus director, organizational
                        development and human resources, at <a href="mailto:rquiles9@valenciacollege.edu" target="_blank" rel="noopener">rquiles9@valenciacollege.edu</a> or 407-299-5000, extension 5001.
                     </p>
                     
                     <hr class="styled_2">
                     
                     <h3>East and Winter Park Campuses</h3>
                     
                     <p>A Message from Stacey Johnson, President, East and Winter Park Campuses</p>
                     
                     <p>Next month, we come together to be inspired, to learn and celebrate together at Learning
                        Day. This year’s campus-based event will provide East, Winter Park and District Office
                        employees the opportunity to attend learning sessions in the morning and community
                        service in the afternoon. All Valencia campus locations will be closed on Friday,
                        February 9, 2018, for Learning Day, and all full-time employees are required to attend.
                        Full-time employees who are not attending should complete a certificate of absence
                        for the day.
                     </p>
                     
                     <p><a href="https://drive.google.com/open?id=105Lzm_5bq-tM5DweZo-2ThOMzulz0nqO" target="_blank" rel="noopener noreferrer">Click here</a> to view an advance copy of the program brochure, which includes the agenda and descriptions
                        for the learning sessions. Please note that sessions are subject to change.
                     </p>
                     
                     <p>Please register for the event and volunteer opportunity (select one provided or list
                        your own) by Tuesday, February 6, 2018.<a href="https://www.eventbrite.com/e/eastwinter-park-campus-learning-day-2018-tickets-36955951245" target="_blank" rel="noopener"><img class="aligncenter size-full wp-image-53177" src="http://thegrove.valenciacollege.edu/files/2017/11/LD18-RSVP-button-ewp-grove.png" alt="" width="570" height="158"></a></p>
                     
                     <hr class="styled_2">
                     
                     <h3>Osceola, Lake Nona and Poinciana Campuses</h3>
                     
                     <p>More information coming soon!</p>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/development/learning-day/index.pcf">©</a>
      </div>
   </body>
</html>