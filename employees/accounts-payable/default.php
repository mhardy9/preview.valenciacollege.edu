<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia College Blank Template | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/accounts-payable/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/accounts-payable/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/accounts-payable/">Accounts Payable</a></li>
               <li>Valencia College Blank Template</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Announcements</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>December 21, 2015</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Valencia College is contracted with Enterprise Rent-A-Car and National Car Rental
                              to provide discounted
                              car rental rates which include insurance. Both vendors provide competitive nationwide
                              rates as well as
                              traveler's insurance for all Valencia employees. Employees are required to use either
                              Enterprise or
                              National when renting a car for college business. If an employee uses a car rental
                              agency other than
                              Enterprise or National,, the traveler will only be reimbursed the lesser of the Enterprise
                              contract rate
                              or actual amount paid. Mileage is not reimbursed when using rental cars, however travel
                              reimbursements are
                              considered for gasoline expenses with the rental car and receipts are required for
                              purchases over $5.00.
                              The traveler should be diligent in acquiring the most economical gas rates. When completing
                              the
                              Authorization for Travel and Per Diem forms, please include all support documentation
                              and receipts for
                              auditing purposes. The<a href="http://valenciacollege.edu/accounts-payable/documents/AuthorizationforTravelFinal.pdf">&nbsp;Authorization
                                 for Travel Form</a>&nbsp;has been updated to reflect this policy.
                           </p>
                           
                           <p>Please Note: Utilization of any College Contract is subject to all applicable policies
                              and procedures and
                              may require prior written approval. As with any college transaction, please be certain
                              that you are duly
                              authorized and have obtained the required approvals before initiating any transaction.
                           </p>
                           
                           <h5><u><strong>ENTERPRISE RENT-A-CAR CONTRACT #XD24488</strong></u></h5>
                           
                           <h5><strong><u>NATIONAL CAR RENTAL CONTRACT #XD24488</u></strong></h5>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>April 29, 2015</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Changes were made to some tolls on the&nbsp;<a href="http://valenciacollege.edu/accounts-payable/documents/UpdatedIn-DistrictMileageChart_EffectiveMay2015.pdf">In-District
                                 Mileage Chart</a>. Please use the Updated In-District Mileage Chart for all In-District Travel. Accounts
                              Payable has revised the Mileage Chart to reflect all toll increases. Please contact
                              Michelle Rodriguez at
                              extension 3379 with any questions.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>May 2, 2014</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Accounts Payable has moved to the District Office. Our new Mail Code is DO-330. Please
                              use this Mail Code
                              when sending internal mail to Accounts Payable.
                           </p>
                           
                           <p>The&nbsp;<a href="http://valenciacollege.edu/accounts-payable/documents/UpdatedIn-DistrictMileageChart_EffectiveApril2014.pdf">In-District
                                 Mileage Chart</a>&nbsp;has been updated. It is now available under forms on the Accounts Payable Website.
                           </p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>June 6, 2013</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">The&nbsp;<a href="http://valenciacollege.edu/accounts-payable/documents/MileageReimbursementFormUpdated.pdf">Mileage
                                 Reimbursement Form</a>,&nbsp;<a href="http://valenciacollege.edu/accounts-payable/documents/AuthorizationforTravelUpdated.pdf">Authorization
                                 for Travel Form</a>&nbsp;and&nbsp;<a href="http://valenciacollege.edu/accounts-payable/documents/PerDiemandLocalMileageVoucherFINAL.pdf">Per
                                 Diem and Local Mileage Voucher Form</a>&nbsp;have been updated. These forms can be found on the Accounts
                              Payable website and in Atlas under Valencia Forms. For any questions about these new
                              forms, please contact
                              Michelle Rodriguez at extension 3379.
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>February 19, 2013</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">The&nbsp;<u>Certification of Services</u>&nbsp;form has been updated. It is now available on the
                              Accounts Payable website and in Atlas under Valencia Forms. Please use this updated
                              form for your
                              processing needs.
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        
                        
                        <h3>Contact Us!</h3>
                        
                        <p><strong>Debbie Gill</strong><br> Suppliers A-C<br> Ext. 3331
                           
                        </p>
                        
                        <p></p>
                        <strong>Marcey Camacho</strong><br> Suppliers D-I<br> Ext. 3310
                        <p></p>
                        <strong>Jo Ratliff</strong><br> Suppliers J-O, W-Z<br> Ext. 3322
                        <p></p>
                        <strong>Dana Scott</strong><br>
                        Suppliers P-V<br> Ext. 3312
                        <p></p>
                        <strong>Michelle Rodriguez</strong><br> Accounts Payable Coordinator<br> Ext. 3379
                        <p></p>
                        <strong>Katrina Manley</strong><br> Director, Accounting<br> Ext. 1052
                        <p></p>
                        <strong>Mailing Address</strong><br> Valencia College<br> ATTN: Accounts Payable DO-330<br> PO Box
                        3028<br> Orlando, FL 32802-3028
                        <p></p>
                        <strong>Email Contact</strong><br> <a href="mailto:AccountsPayable@valenciacollege.edu">AccountsPayable@valenciacollege.edu</a>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/accounts-payable/default.pcf">©</a>
      </div>
   </body>
</html>