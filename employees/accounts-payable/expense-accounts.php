<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Expense Accounts  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/accounts-payable/expense-accounts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/accounts-payable/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/accounts-payable/">Accounts Payable</a></li>
               <li>Expense Accounts </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Travel Expense Accounts</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top">
                                    
                                    <th width="129" valign="top" bgcolor="#CCCCCC" scope="col">Types of Travel</th>
                                    
                                    <th width="83" valign="top" bgcolor="#CCCCCC" scope="col">Acct/GL Code</th>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>In-District</td>
                                    
                                    <td>605010</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>Out-of-District</td>
                                    
                                    <td>605020</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>Out-of-State</td>
                                    
                                    <td>605030</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>Other Travel (Webinar)</td>
                                    
                                    <td>605100</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>International</td>
                                    
                                    <td>605040</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>Student Travel</td>
                                    
                                    <td>605060</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        <strong>NOTE:</strong> These Account Codes should be used to properly record your travel expenses. They
                        should
                        be included along with the Index Number for your specified budget. The In-District
                        Travel GL Code should be
                        used when traveling in<em>&nbsp;Orange &amp; Osceola Counties only</em>. All other travel should use the
                        appropriate GL Code specifying the type of travel.
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        
                        
                        <h3>Useful LInks!</h3>
                        
                        <p>
                           <a href="http://www.leg.state.fl.us/Statutes/index.cfm?StatuteYear=2010&amp;Tab=statutes&amp;Submenu=1&amp;NewSelection=Go">2010
                              Florida Statutes</a></p>
                        
                        <p><a href="http://travel.state.gov/travel/cis_pa_tw/tw/tw_1764.html">Current Travel Warnings</a></p>
                        
                        <p><a href="https://www.oocea.com/TravelersExpressways/TravelResources/TollCalculator.aspx">Expressway
                              Authority: Toll Calculator</a></p>
                        
                        <p><a href="http://www2.dot.state.fl.us/CityToCityMileage/viewer.html">FDOT Official Highway Mileage</a></p>
                        
                        <p><a href="http://www.floridasturnpike.com/tools_tollrates.cfm">Florida's Turnpike</a></p>
                        
                        <p><a href="http://turnpikeinfo.com/florida/tolls/tollData.php">Florida's Turnpike: Toll costs &amp; rate
                              calculator</a></p>
                        
                        <p><a href="http://www.oanda.com/">Foreign Currency Web Site</a></p>
                        
                        <p><a href="http://aoprals.state.gov/content.asp?content_id=184&amp;menu_id=78">Foreign Per Diem Web Site</a></p>
                        
                        <p><a href="http://www.mapquest.com/">Map Mileage Web Site</a></p>
                        
                        <p><a href="http://aoprals.state.gov/content.asp?content_id=114&amp;menu_id=75">Meal Allowances for International
                              Travel</a></p>
                        
                        <p><a href="https://travelregistration.state.gov/ibrs/ui/">U.S. Department of State</a></p>
                        
                     </div>
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/accounts-payable/expense-accounts.pcf">©</a>
      </div>
   </body>
</html>