<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/accounts-payable/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/accounts-payable/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/accounts-payable/">Accounts Payable</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Travel</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>When traveling directly from home address to an approved location, then to your home
                              campus, then home
                              address at the end of the business day, how you would calculate your mileage?
                           </h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Calculate the whole leg of the trip: home address-to the location-to home campus-to
                              home address minus
                              your normal commute to your home campus.
                           </p>
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>When attending a meeting/conference in Orange or Osceola County and assuming an Authorization
                              for Travel
                              Form was previously submitted for the trip, do I claim my excess miles on the Per
                              Diem Voucher or on the
                              In-District Travel Report?
                           </h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If a registration fee is paid, a&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/PerDiemandLocalMileageVoucherFINAL.pdf">Per
                                 Diem Form</a>&nbsp;should be submitted to Accounts Payable. The in-district miles can be claimed on
                              the Per
                              Diem Form.&nbsp;<strong><u>If NO registration fee is paid</u></strong>&nbsp;and a $0 Authorization for Travel is
                              used, then claim the excess miles on the&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/MileageReimbursementFormUpdated.pdf">Mileage
                                 Reimbursement Form&nbsp;</a>.
                           </p>
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>Do I need to submit a Authorization for Travel to Accounts Payable if I am not requesting
                              any
                              reimbursements or Per Diem?
                           </h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>No. A travel Authorization is needed if an employee is performing job-related duties
                              out of the office.
                              If the employee is requesting any type of Per Diem or reimbursement, then the employee
                              must submit an
                              Authorization for Travel form to Accounts Payable. If the employee is NOT requesting
                              any reimbursements
                              and there is no registration fee being paid, then the Authorization for Travel is
                              only required on a
                              departmental level and Accounts Payable DOES NOT require a copy.
                           </p>
                           
                           <p>EXCEPTION: An Authorization for Travel and Per Diem must&nbsp;<strong>ALWAYS</strong>&nbsp;be submitted to the
                              Grants Department for Grant Funded Programs and Endowed Chair Restricted Funds per
                              the Grant Guidelines,
                              regardless of the amount.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>Your "home campus" is located on West Campus and you must drive to East Campus for
                              a meeting. After work
                              you go home. How many excess miles can you claim?
                           </h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Add mileage from West campus to East Campus, then to your home and subtract your normal
                              commute (example:
                              West to East + East to Home - West to Home = Excess Miles)
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>How do I pay for registration fees?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>There are few ways to pay (A) Use your P-card; (B) If it is being paid by a grant
                              do a Check Request and
                              have a check cut for that vendor; or (C) Pay it yourself and get reimbursed on your
                              <a href="https://valenciacollege.edu/accounts-payable/documents/PerDiemandLocalMileageVoucherFINAL.pdf">Per
                                 Diem Form</a>.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>How do I pay for lodging?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>There are a few ways to pay (A) Do a<a href="https://valenciacollege.edu/accounts-payable/documents/CheckRequestUpdated.pdf">&nbsp;Check Request</a>&nbsp;to
                              have a check mailed to the hotel prior to traveling; (B) Pay with a personal credit
                              card and get
                              reimbursed on the&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/PerDiemandLocalMileageVoucherFINAL.pdf">Per
                                 Diem Form</a>; or (C) Pay using a P-Card (if the holder of the P-Card is not present, they will
                              have to
                              fill out an authorization from the hotel prior to the date of the travel for the charge).
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>How do I pay for airline tickets?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>There are few ways to pay (A) Use your P-Card; (B) If it is a Grant and there is no
                              P-Card available, let
                              Travel Max know and they will charge it to the college American Express; or (C) Pay
                              with your credit card
                              and get reimbursed on your&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/PerDiemandLocalMileageVoucherFINAL.pdf">Per
                                 Diem Form</a>.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>How do I pay for a rental car?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Pay with your personal credit card and get reimbursed on the Per Diem Form, however
                              for rental car use
                              advance approval from the appropriate vice-president, provost or designee is required.
                              Valencia College
                              has an agreement with Enterprise, so we STRONGLY recommend you use Enterprise for
                              all Car Rentals to save
                              you the most on your travel. If you choose to use an outside vendor you will be responsible
                              for all
                              insurance, service charges, etc.
                              
                              NOTE: Valencia College does NOT reimburse for any upgrades. All upgrades are considered
                              a personal expense
                              and you are responsible for all upgrade specific charges.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>Why do I have to provide repeated information if it is already included in travel
                              paperwork?
                           </h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>This information must be provided because not all reviewers, approvers, or auditors
                              have access to all
                              systems. The information must be provided in detail so there will be no questions
                              regarding whether or not
                              the item or service is an allowable expense.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>How do I determine my departure and arrival times for the Authorization For Travel
                              and Per Diem
                              Forms?
                           </h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The departure time on the Authorization for Travel form should be when you plan to
                              leave your home. If
                              flying, you are allowed to report two hours before your departure flight time and
                              one hour after your
                              returning flight time on your Authorization For Travel Form. On the Per Diem Form,
                              you should report the
                              ACTUAL time you left your home and returned home from your travel. Again, you are
                              allowed to report two
                              hours before your departure flight time and one hour after your returning flight time
                              on you Per Diem
                              Form. If any flight or travel changes occur that affect your departure and arrival
                              time, please attach a
                              memo to your Per Diem Form notifying Accounts Payable of the reason for the change.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h2>P-Card</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>Can I use my P-card for gas?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Only if the vehicle is a rented vehicle for College use. (If you are using a personal
                              vehicle, you are
                              reimbursed through the per diem mileage rate) As with any P-card purchase, the gas
                              receipt must be
                              retained for supporting the purchase.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h2>Contracts</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>Can you review the contracts process?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The instructions for completing a one-time contract can be found in Atlas under Valencia
                              Forms under&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/ContractorsServicesAgreementInstructions.pdf">Contractors
                                 Services Agreement Instructions</a>. The actual form is in a separate document called&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/ContractorsAgreementUpdated7_1_14.pdf">Contract
                                 Services Agreement Form</a>. If you are using the Contractor Services Agreement Form from Atlas
                              with&nbsp;<u><strong>NO CHANGES</strong></u>, have the vendor sign the contract and forward the contract to
                              Keith Houck for the Valencia Signature. Once Keith has signed the contract, Patty
                              Nicholas will send a
                              copy to the necessary department and the original contract to procurement.&nbsp;<u><strong>If any changes are
                                    made to the document or a vendor sends their own contract</strong></u>&nbsp;for Valencia to sign, please send
                              the contract to the<a href="https://valenciacollege.edu/generalcounsel/default.cfm">&nbsp;Legal Department</a>&nbsp;(DO-40).
                              The contract must be approved first through the Legal Department before any other
                              actions can be taken.
                              
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>Can you review the Procurement and Accounts Payable Workflow?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Due to process changes effective September 1, 2012, Accounts Payable would like to
                              clarify the Purchase
                              Order and Invoice creation, approval, and payment workflow. College Policy 6Hx28:6-03.1
                              states that all
                              purchases shall be made by requisition purchase order, purchasing card, or petty cash
                              procedures.&nbsp; A
                              purchase order (PO) is a legally binding document that authorizes the supplier to
                              ship and invoice for the
                              materials and/or services specified. The president or a designee (Keith Houck for
                              contracts and Ed Ames
                              for purchase orders) are the only authorized employees who can commit the college
                              to such expenditures.
                              For these reasons, it is important to follow proper procedures when making purchases
                              to protect you and
                              the College from liability.
                           </p>
                           
                           <p>When making purchases, you MUST complete a requisition to have Procurement create
                              a purchase order BEFORE
                              receiving materials and an invoice from the vendor.&nbsp;<strong>A purchase order will not be created after
                                 goods, services, or an invoice has been received.</strong>&nbsp;Also note that Accounts Payable will not
                              straight pay any invoices. Usually, a PO is created with a set quantity and price.
                              However, if you are not
                              sure how much the purchase will cost or how often you will receive invoices from the
                              vendor, you can
                              request an open purchase order by specifying a PO be open for the entire fiscal year,
                              July 1 to June 30.
                              An open purchase order is just a regular PO issued to a specific supplier for a specified
                              time, up to 12
                              months, for recurring purchases/payments with an unknown dollar amount. You can estimate
                              costs for the
                              year by term, project, or previous fiscal year expenditures. Open PO's can be increased
                              at any time
                              throughout the fiscal year. This can also save your department time as only one PO
                              needs to be requested
                              per vendor per year. At the beginning of each new fiscal year, new open purchase orders
                              should be
                              requested.
                           </p>
                           
                           <p>There are two workflows used commonly throughout the college for requisition purchase
                              orders. The first
                              workflow is for general goods and services purchased through vendors, for example
                              copier paper, which has
                              a fixed quantity and price. The second workflow is used for Valencia Contractor's
                              Agreements or recurring
                              monthly expenses, for example art models. Below is a visual of both workflows.&nbsp; Please
                              note that an open
                              PO should be requested at the time a Valencia Contractor's Agreement is signed. Also,
                              if Fund 1 funds are
                              being used, the Valencia Contractor's Agreement must be renewed each fiscal year.
                              The Contractor's
                              Agreements should be completed in detail with specific information on payment details,
                              for example $15 per
                              hour. It is strongly recommended that quotes be obtained for all purchases. This serves
                              as written
                              documentation from the vendor for your future purchases. If a purchase is above $5,000,
                              a least three
                              quotes are required. As you can see from the workflows below, both processes are very
                              similar. Our goal is
                              to help clarify these processes and assist all end-users in navigating these processes
                              successfully. If
                              you have any questions please contact Seher Awan, at extension 3379.
                           </p>
                           
                           <p><strong><u>For General Purchases the Workflow is:</u></strong></p>
                           
                           <ol>
                              
                              <li>Department requests good or service &amp; enters a requisition into Banner</li>
                              
                              <li>Requisition sent to approval queue</li>
                              
                              <li>Once approved, requisition is sent to Procurement</li>
                              
                              <li>Procurement creates Purchase Order (PO)</li>
                              
                              <li>PO is sent to vendor</li>
                              
                              <li>Vendor delivers goods and services to Department</li>
                              
                              <li>Vendor mails invoice to College</li>
                              
                              <li>Accounts Payable (AP) receives invoice &amp; sends to requestor for approval</li>
                              
                              <li>Invoice is confirmed and approved by requestor</li>
                              
                              <li>Invoice sent back to AP</li>
                              
                              <li>AP confirms invoice and PO#</li>
                              
                              <li>AP processes invoice and enters for payment</li>
                              
                              <li>Check is cut and mailed to vendor on Tuesday, Thursday, or Friday</li>
                              
                           </ol>
                           
                           <p><strong><u>For Contractor Agreements &amp; Monthly Recurring Expenses the Workflow is:</u></strong></p>
                           
                           <ol>
                              
                              <li>Department completes a Contractor's Agreement for a vendor for the fiscal year</li>
                              
                              <li>Department creates requisition in Banner for the entire fiscal year (If recurring
                                 bill, create
                                 requisition on July 1)
                                 
                              </li>
                              
                              <li>Requisition sent to approval queue</li>
                              
                              <li>Once approved, requisition is sent to Procurement</li>
                              
                              <li>Procurement creates Purchase Order (PO) that is open for the entire fiscal year</li>
                              
                              <li>Contractors/vendors bill the department hourly, monthly, or job (invoice or timesheet)</li>
                              
                              <li>Department writes PO# on invoice/timesheet and sends to Accounts Payable for processing</li>
                              
                              <li>Accounts Payable (AP) receives invoice and confirms invoice/timesheet and PO#</li>
                              
                              <li>AP processes invoice and enters for payment</li>
                              
                           </ol>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h2>Accounting</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>How will I know the proper account code to use when purchasing from the budget?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>After receiving training, you can access this information in the account code reference
                              guide called&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/WhatAccountShouldIUse01-2017.pdf">What GL
                                 (Account) Code Do I Use.</a>. This reference guide was developed in 2005 with descriptive explanations on
                              the different account codes. This guide was recently updated in April 2010. The document
                              can be found on
                              the&nbsp;<a href="https://valenciacollege.edu/procurement/">Procurement Web Site</a>, under Faculty &amp;
                              Staff FAQ's or on the Atlas page under the Finance Tab within the Procurement Channel.
                              Please note that
                              this is a general use of account codes and sometimes there are special accounts that
                              only apply to Grants.
                              If you are unsure of the account code to use with regards to a grant, please contact
                              the Grants Accounting
                              Office at extensions 3305 or 3335. For general questions on which account code to
                              use, please contact the
                              Budget Office at 3306 or 3309.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>How can I acquire petty cash for my department?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>With petty cash, this is requested within each department if necessary. You must complete
                              the Receipt of
                              Petty Cash Form. Submit the white copy to the Accounting/Finance Department at DTC.
                              Return the yellow copy
                              to the Business Office and keep the pink copy. Fund one funds CANNOT be used for food
                              purchases. Also,
                              petty cash CANNOT be used for any purchases with sales tax. The form must be signed
                              by a Budget Manager.
                              You MUST provide an original receipt with the form. If you do not have a receipt,
                              you must submit a
                              detailed memo about why there is no receipt and provide a detailed explanation of
                              the purchase.
                           </p>
                           
                           <p><strong>NOTE: You can only use petty cash for up to a $75 purchase. If the purchase is for
                                 more than $75,
                                 a Check Request must be submitted.</strong></p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>When should a Certification of Services be used?</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><u>A Certification of Services</u>&nbsp;is used to pay for services ONLY when the contractors/vendors do not
                              provide invoices OR if the check is needed before the event or service. All other
                              payments should be
                              processed with the vendor invoice or as an advance.
                           </p>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        
                        
                        <h3>Contact Us!</h3>
                        
                        <p><strong>Debbie Gill</strong><br> Suppliers A-C<br> Ext. 3331
                           
                        </p>
                        
                        <p></p>
                        <strong>Marcey Camacho</strong><br> Suppliers D-I<br> Ext. 3310
                        <p></p>
                        <strong>Jo Ratliff</strong><br> Suppliers J-O, W-Z<br> Ext. 3322
                        <p></p>
                        <strong>Dana Scott</strong><br>
                        Suppliers P-V<br> Ext. 3312
                        <p></p>
                        <strong>Michelle Rodriguez</strong><br> Accounts Payable Coordinator<br> Ext. 3379
                        <p></p>
                        <strong>Katrina Manley</strong><br> Director, Accounting<br> Ext. 1052
                        <p></p>
                        <strong>Mailing Address</strong><br> Valencia College<br> ATTN: Accounts Payable DO-330<br> PO Box
                        3028<br> Orlando, FL 32802-3028
                        <p></p>
                        <strong>Email Contact</strong><br> <a href="mailto:AccountsPayable@valenciacollege.edu">AccountsPayable@valenciacollege.edu</a>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/accounts-payable/faq.pcf">©</a>
      </div>
   </body>
</html>