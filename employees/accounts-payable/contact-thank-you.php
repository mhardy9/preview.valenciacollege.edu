<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Contact Us  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/accounts-payable/contact-thank-you.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/accounts-payable/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/accounts-payable/">Accounts Payable</a></li>
               <li>Contact Us </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Contact Us</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Thank you for contacting us. We will get back with you as soon as we can.</p>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        
                        
                        <h3>Contact Us!</h3>
                        
                        <p><strong>Debbie Gill</strong><br> Suppliers A-C<br> Ext. 3331
                           
                        </p>
                        
                        <p></p>
                        <strong>Marcey Camacho</strong><br> Suppliers D-I<br> Ext. 3310
                        <p></p>
                        <strong>Jo Ratliff</strong><br> Suppliers J-O, W-Z<br> Ext. 3322
                        <p></p>
                        <strong>Dana Scott</strong><br>
                        Suppliers P-V<br> Ext. 3312
                        <p></p>
                        <strong>Michelle Rodriguez</strong><br> Accounts Payable Coordinator<br> Ext. 3379
                        <p></p>
                        <strong>Katrina Manley</strong><br> Director, Accounting<br> Ext. 1052
                        <p></p>
                        <strong>Mailing Address</strong><br> Valencia College<br> ATTN: Accounts Payable DO-330<br> PO Box
                        3028<br> Orlando, FL 32802-3028
                        <p></p>
                        <strong>Email Contact</strong><br> <a href="mailto:AccountsPayable@valenciacollege.edu">AccountsPayable@valenciacollege.edu</a>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/accounts-payable/contact-thank-you.pcf">©</a>
      </div>
   </body>
</html>