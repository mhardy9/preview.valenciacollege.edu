<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>International Travel  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/employees/accounts-payable/international-travel.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/employees/accounts-payable/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/employees/">Employees</a></li>
               <li><a href="/employees/accounts-payable/">Accounts Payable</a></li>
               <li>International Travel </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>International Travel</h2>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>A detailed checklist for international travel is located on the&nbsp;<a href="https://valenciacollege.edu/international/studyabroad/staff/internationaltravel.cfm">SAGE
                                 Website</a>. It is recommended that Valencia staff and faculty traveling overseas register with
                              the SAGE
                              office in order to get support in the event of an emergency. As part of that support,
                              the SAGE Department
                              will provide you with an emergency binder, international cell phone (if needed), and
                              registers your trip
                              with the&nbsp;<a href="https://travelregistration.state.gov/ibrs/ui/">Department of State</a>.
                           </p>
                           
                           <p>In regards to&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/IntlTravel_PerDiemForm.pdf">International Per
                                 Diem</a>, if an employee is requesting international per diem, each situation can be different.
                              However,
                              there are some similarities which can be highlighted:
                           </p>
                           
                           <ul>
                              
                              <li>If a staff member is paying for their international hotel using a P-Card they must
                                 contact Janet
                                 Vivian or Rhonda Ulmer in Procurement Services and notify them that they are traveling
                                 out of the
                                 country and will be using their P-Card. Accounts Payable will reimburse the maximum
                                 per diem rate on the
                                 per diem (and deduct the hotel costs if using a P-Card in the advance section of the
                                 per diem) to
                                 determine their reimbursement. This is if the employee is traveling and is incurring
                                 actual hotel
                                 expenses as an advance.&nbsp;<strong>NOTE</strong>: Most international travel is processed and paid for as a
                                 group, not as an individual hotel expense.
                                 
                              </li>
                              
                              <li>If a staff member is claiming the&nbsp;<a href="http://aoprals.state.gov/web920/per_diem.asp">Maximum Per
                                    Diem Rates</a>&nbsp;for the Country and City they will be traveling to, they must not include incidentals
                                 in
                                 their request for reimbursement (this includes laundry, dry cleaning, etc.). The incidentals
                                 and Foreign
                                 Per Diem Rates can be found on the FTR Appendix B (Breakdown of meals/incidentals).
                                 
                              </li>
                              
                              <li>If a staff member is traveling to more than 1 city, they must add up the total for
                                 the days and they
                                 must attach a copy of the Foreign Per Diem Rates in U.S. dollars to show the Country
                                 and effective date
                                 of the rate of travel.
                                 
                              </li>
                              
                              <li>Staff members should use the International Per Diem form located on the&nbsp;<a href="https://valenciacollege.edu/international/studyabroad/staff/internationaltravel.cfm">SAGE
                                    website</a>.
                                 
                              </li>
                              
                              <li>The College will NOT reimburse passport renewals. However, if an employee does not
                                 have a passport and
                                 needs one to travel out of the country, the College will pay the expense IF they employee
                                 does not have
                                 a passport using Fund 6. See&nbsp;<a href="http://travel.state.gov/travel/">travel.state.gov/travel</a>&nbsp;for
                                 the requirements to enter the country.
                                 
                              </li>
                              
                              <li>The College will reimburse the CMI Insurance of $24.00 mentioned in Step 10 of the
                                 SAGE instructions.
                                 
                              </li>
                              
                              <li>The College will reimburse the costs of an international phone card when using an
                                 International Phone
                                 that SAGE has provided from their office.
                                 
                              </li>
                              
                              <li>If an expense is recommended, it is up to the traveler to choose to pay the expense.</li>
                              
                              <li>The College will reimburse the cost of visas if the visa is placed on the Authorization
                                 for Travel and
                                 is signed by the employee's supervisor and the budget manger (which is the normal
                                 process). Accounts
                                 Payable will pay for the expense out of the designated college budget. The supervisor's
                                 signatures allow
                                 the trip to be considered a&nbsp;<strong>required</strong>&nbsp;part of the employee's job duties. Our guideline
                                 for the required expenditures will be the entry/exit requirements for US Citizens
                                 as posted on the&nbsp;<a href="http://travel.state.gov/travel/travel_1744.html">Department of State Website</a>&nbsp;at the time
                                 the&nbsp;<a href="https://valenciacollege.edu/accounts-payable/documents/IntlTravel_AuthorizationforInternationalTravelForm.pdf">Authorization
                                    for Travel</a>&nbsp;is submitted.
                                 
                              </li>
                              
                           </ul>
                           
                           <p>For questions or concerns specific to International Travel, contact Jennifer Robertson
                              the Director of
                              Study Abroad and Global Experiences at (407) 582-3404 or Kara Parsons at (407) 582-3452.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h4>College Preferred Travel Agent</h4>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>Travel Max</strong><br> 4807 Big Horn St.&nbsp;<br> Orlando, Florida 32819&nbsp;<br> <br> Contact Person:
                              Robert Reed&nbsp;<br> Telephone: 407-704-7850&nbsp;<br> Fax: 407-704-7856
                           </p>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        
                        
                        <h3>Useful LInks!</h3>
                        
                        <p>
                           <a href="http://www.leg.state.fl.us/Statutes/index.cfm?StatuteYear=2010&amp;Tab=statutes&amp;Submenu=1&amp;NewSelection=Go">2010
                              Florida Statutes</a></p>
                        
                        <p><a href="http://travel.state.gov/travel/cis_pa_tw/tw/tw_1764.html">Current Travel Warnings</a></p>
                        
                        <p><a href="https://www.oocea.com/TravelersExpressways/TravelResources/TollCalculator.aspx">Expressway
                              Authority: Toll Calculator</a></p>
                        
                        <p><a href="http://www2.dot.state.fl.us/CityToCityMileage/viewer.html">FDOT Official Highway Mileage</a></p>
                        
                        <p><a href="http://www.floridasturnpike.com/tools_tollrates.cfm">Florida's Turnpike</a></p>
                        
                        <p><a href="http://turnpikeinfo.com/florida/tolls/tollData.php">Florida's Turnpike: Toll costs &amp; rate
                              calculator</a></p>
                        
                        <p><a href="http://www.oanda.com/">Foreign Currency Web Site</a></p>
                        
                        <p><a href="http://aoprals.state.gov/content.asp?content_id=184&amp;menu_id=78">Foreign Per Diem Web Site</a></p>
                        
                        <p><a href="http://www.mapquest.com/">Map Mileage Web Site</a></p>
                        
                        <p><a href="http://aoprals.state.gov/content.asp?content_id=114&amp;menu_id=75">Meal Allowances for International
                              Travel</a></p>
                        
                        <p><a href="https://travelregistration.state.gov/ibrs/ui/">U.S. Department of State</a></p>
                        
                     </div>
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/employees/accounts-payable/international-travel.pcf">©</a>
      </div>
   </body>
</html>