<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical Facility - Orlando Health System  | Valencia College</title>
      <meta name="Description" content="Clinical Facility - Orlando Health System Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, facilities, ohs, orlando, health, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical-facility-ohs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical Facility - Orlando Health System </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Orlando Health System (OHS)</h2>
                        
                        <hr class="styled_2">
                        
                        <p>Website: <a href="http://www.orlandohealth.com/" target="_blank">http://www.orlandohealth.com/</a><br>
                           Locations: <a href="http://www.orlandohealth.com/facilities" target="_blank">http://www.orlandohealth.com/facilities</a><br>
                           Map to the Computer Classrooms: <a href="/documents/secure/nursing/ohs-computer-class-map.pdf">Download</a>
                           
                        </p>
                        
                        <div>
                           
                           <h3>How to obtain an Orlando Health Student Badge</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>If you attend a student orientation given by Orlando Health, your picture will be
                                 taken in order to
                                 make your student badge. One faculty member from your school will pick-up the badges
                                 and distributes
                                 them at your school when they are ready.
                                 
                              </li>
                              
                              <li>If you did not attend a student orientation given by Orlando Health, you will need
                                 to go to the
                                 Downtown Orlando Campus Badge Room to obtain your badge, which is located at 100 W.
                                 Copeland Street,
                                 Orlando, FL 32806 on the ground floor of the Lucerne Terrace Parking Garage.
                                 
                              </li>
                              
                              <li>The Badge Room at Dr. P. Phillips Hospital (DPH) is located on the first floor of
                                 the parking garage
                                 behind DPH at 9450 Turkey Lake Road.
                                 
                              </li>
                              
                              <li>You will be notified by your instructor when you are eligible to obtain your badge.
                                 Do not go to the
                                 Badge Room before checking with your faculty AND/OR your school's student coordinator.
                                 
                              </li>
                              
                              <li>If you are an employee, you must still only go to the Badge Room for a student badge
                                 during student
                                 Badge Room hours. You must obtain your badge PRIOR to getting your parking decal.
                                 If you are at the
                                 Badge Room and they tell you that you aren't in the Workforce Training Database, call
                                 the Nursing Office
                                 at 407.582.1566 OR 407.582.1118 OR Ms. Kovalsky at 407.582.1023 OR Stacey Brown, Student
                                 Educational
                                 Coordinator at Orlando Health at 321.841.1566
                                 
                              </li>
                              
                              <li>If you lose or damage your Orlando Health Student ID Badge, the Badge Room charges
                                 a $10.00 (cash
                                 only) replacement fee.
                                 
                              </li>
                              
                              <li>Orlando Health team members or volunteers that are also students must have an Orlando
                                 Health Student
                                 ID Badge for rotations, in addition to their team member or volunteer ID badge. You
                                 may not use your
                                 team member or volunteer ID badge in place of your Student ID Badge.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Student/Faculty Badge Locations &amp; Hours</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Downtown Orlando Campus
                                 
                                 <ul>
                                    
                                    <li>100 West Copeland Street, Orlando, 32806</li>
                                    
                                    <li>See room location information in <a href="/documents/secure/nursing/ohs-clinical-orientation-packet.pdf" target="_blank">Nursing Student
                                          Orientation Packet</a>.
                                       
                                    </li>
                                    
                                    <li>Open Wednesday, Thursday and Friday. Closed to students all other days</li>
                                    
                                    <li>Hours: 7:00 AM - 4:00 PM</li>
                                    
                                    <li>Badge Room Phone: 321-841-6362</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Dr. P. Phillips Hospital (DPH)
                                 
                                 <ul>
                                    
                                    <li>9400 Turkey Lake Rd, Orlando, FL 32819</li>
                                    
                                    <li>See room location information in <a href="/documents/secure/nursing/ohs-clinical-orientation-packet.pdf" target="_blank">Nursing Student
                                          Orientation Packet</a>.
                                       
                                    </li>
                                    
                                    <li>Open Monday - Friday.</li>
                                    
                                    <li>Hours: 7:00 AM - 3:00 PM</li>
                                    
                                    <li>Calling prior to arriving is recommended</li>
                                    
                                    <li>Badge Room Phone: 321-842-7301</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Orientation</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Thank you for choosing an Orlando Regional Healthcare facility as your clinical site.
                              Our highly trained
                              staff is dedicated to providing quality learning opportunities for students of all
                              disciplines. Our
                              multiple sites offer various and vast experiences for all of your clinical goals and
                              objectives. It is our
                              sincerest desire to make your experience at ORH as enriching as possible. We wish
                              you luck in the nursing
                              program and welcome you into our healthcare family. 
                           </p>
                           
                           <p>All forms to be turned in to your nursing course faculty as directed. </p>
                           
                           <p>PLEASE NOTE: As a participant in student programs, each student, regardless of current
                              employment with
                              Orlando Health, is required to complete this checklist in full, including the Regulatory
                              Test. 
                           </p>
                           
                           <p><i class="far fa-book fa-spacer"></i> Read the entire Regulatory Packet below. This packet details the mandatory
                              requirements that must be completed before you start your rotation and get an Orlando
                              Health Student ID
                              Badge and parking decal. Turn in ONLY the graded Regulatory Test Answer Sheet to your
                              nursing course
                              faculty. This is an open book test. Please refer to the Regulatory Test Answer Key
                              AFTER you take your
                              test and grade your test. 
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <a href="/documents/secure/nursing/ohs-regulatory-packet.pdf" target="_blank">Regulatory Packet</a>
                                 
                              </li>
                              
                              <li><a href="/documents/secure/nursing/ohs-regulatory-packet-answer-key.pdf" target="_blank">Regulatory
                                    Test Answer Key</a></li>
                              
                           </ul>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> <a href="/documents/secure/nursing/TURN-IN-ohs-regulatory-packet-answer-sheet.pdf" target="_blank">Regulatory
                                    Test Answer Sheet</a></p>
                              
                           </div>
                           
                           <p><i class="far fa-book fa-spacer"></i> Read the entire <a href="/documents/secure/nursing/ohs-clinical-orientation-packet.pdf">Clinical Groups Orientation
                                 Packet</a>. This packet details the mandatory requirements that must be completed before you
                              start your
                              rotation. 
                           </p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Turn in only the signed Student Acknowledgement Form on page 8 (of the
                                 above linked document) to your nursing course faculty.
                              </p>
                              
                           </div>
                           
                           <p><i class="far fa-book fa-spacer"></i> Obtain your Orlando Health Student ID Badge from your school faculty person
                              or coordinator (Please see Page 16 of the <a href="/documents/secure/nursing/ohs-clinical-orientation-packet.pdf">Nursing Student Orientation
                                 Packet</a> for instructions). 
                           </p>
                           
                           <p><i class="icon-book-alt"></i> Obtain your Orlando Health Student parking decal (Please see Page 17 of the
                              <a href="/documents/secure/nursing/ohs-clinical-orientation-packet.pdf">Nursing Student Orientation
                                 Packet</a> for instructions).
                           </p>
                           
                           <p><i class="far fa-book fa-spacer"></i> For Students involved in direct patient care, please see the <a href="computer-training.html#sunrise">Sunrise Computer Class information</a>/p&amp;gt;
                              
                           </p>
                           
                           <p><i class="far fa-book fa-spacer"></i> ADDITIONAL Items to Review for Orlando Health Orientation
                           </p>
                           
                           <ul>
                              
                              <li><a href="/documents/secure/nursing/ohs-approved-skills-procedures.pdf" target="_blank">Approved Skills
                                    &amp; procedures for Nursing Students</a></li>
                              
                              <li><a href="/documents/secure/nursing/ohs-hazardous-meds-update.pdf" target="_blank">Hazardous
                                    Medications Policy</a></li>
                              
                              <li><a href="/documents/secure/nursing/ohs-hazardous-meds-list.pdf" target="_blank">List of Hazardous
                                    Medications</a></li>
                              
                              <li><a href="/documents/secure/nursing/ohs-safety-prep.pdf" target="_blank">Safety Prep</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Computer Training Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Policy &amp; Procedures</h4>
                           
                           <ul>
                              
                              <li>New Sunrise 5.5 Logon Procedures - <a href="/documents/secure/nursing/ohs-sunrise-logon.pdf" target="_blank">Logon Manager</a> &amp; <a href="/documents/secure/nursing/ohs-sunrise-FAQ.pdf" target="_blank">Logon Manager FAQ</a>
                                 
                              </li>
                              
                              <li>Nursing students who needs an Orlando Health Sunrise computer class (i.e. if you have
                                 not taken the
                                 class before), please sign up for a class via the SUNRISE SUPERCENTER (Sign Up for
                                 Patient Electronic
                                 Record), which is located in Blackboard. Directions to sign into the Sunrise Supercenter
                                 are under the
                                 "Resources" tab on the right side tabl on the HOME PAGE here in the Secure Nursing
                                 Website.
                                 
                              </li>
                              
                              <li>You can take the classes which are scheduled after the Summer 2016 Term begins, however,
                                 you will not
                                 be able to document in clinical until you take the class. Faculty will only let you
                                 go ONE WEEK without
                                 documenting.
                                 
                              </li>
                              
                              <li>There will be no Sunrise Computer Classes scheduled over the Holiday Break.</li>
                              
                              <li>ONLY students going to an Orlando Health clinical site, including Dr. Philips, ORMC,
                                 MD Anderson,
                                 Lucerne, and Arnold Palmer and Winnie Palmer should take these classes. If you are
                                 at another facility
                                 and not in an Orlando Health site for Summer 2016 you DO NOT need to take the class.
                                 
                              </li>
                              
                              <li>If you have already taken the class, you do not have to re-take it.</li>
                              
                              <li>Orlando Health Team Members should contact IS directly about whether they are to attend
                                 the Sunrise
                                 Class.
                                 
                              </li>
                              
                              <li>You may not attend a class unless you have previously signed up for it. If you do
                                 show up, the IT
                                 Department at Orlando Health will most likely ask you to leave.
                                 
                              </li>
                              
                              <li>Orlando Health Hospital Computer Training for clinical documentation is a requirement
                                 for all courses
                                 in the nursing program. Documentation is a critical skill and the student MUST complete
                                 the computer
                                 training to successfully complete each nursing course.
                                 
                              </li>
                              
                              <li>Computer classes will be scheduled by the Valencia College Clinical Coordinator and
                                 posted in the
                                 SUNRISE SUPERCENTER (Sign Up for Patient Electronic Record)
                                 
                              </li>
                              
                              <li>Students must sign up forOrlando Health computer training via the SUNRISE SUPERCENTER
                                 in Blackboard
                                 and attend computer training as scheduled.
                                 
                              </li>
                              
                              <li>Students are expected to attend the assigned computer training. DO NOT ATTEND IF YOUR
                                 NAME IS NOT ON
                                 THE LIST, OR THEY WILL SEND YOU HOME!
                                 
                              </li>
                              
                              <li>Time: You must be on time or will not be allowed to attend the training</li>
                              
                              <li>For illness or unforeseen evenst which cause the student to miss the assigned computer
                                 training, the
                                 student must notify Ms. Kovalsky by e-mail (no phone messages please) at akovalsky@valenciacollege.edu
                                 24 hours prior to the scheduled class. Absence from computer training will be reviewed
                                 on an individual
                                 basis. For valid excused absences, the training will be rescheduled by the Valencia
                                 computer training
                                 coordinator, Dr. Hudson.
                                 
                              </li>
                              
                              <li>For an unexcused absence from computer training, the student can be withdrawn from
                                 the nursing course.
                                 This course withdrawal is based on the fact that documentation is essential for safe
                                 clinical practice.
                                 A student that delivers nursing care without the ability to document is demonstrating
                                 unsafe care and
                                 this unsafe care puts the client at risk for injury. Unsafe clinical practice is grounds
                                 for immediate
                                 dismissal from the nursing program.
                                 
                              </li>
                              
                              <li>Orlando Health Computer Training Dress Code: You may wear either your community uniform
                                 or white
                                 clinical uniform with your Orlando Health badge. JEANS OF ANY TYPE ARE FORBIDDEN TO
                                 BE WORN ON THE
                                 HOSPITAL PREMISES BY STUDENTS. ID BADGE: If you do not yet have your Orlando Health
                                 Badge (they will be
                                 distributed on the first day of class next term), you MUST wear your Valencia ID.Students
                                 will be
                                 refused entrance to the training if not in accepted dress code.
                                 
                              </li>
                              
                              <li>NOTE: You cannot get a parking sticker without having your Orlando Health ID.</li>
                              
                              <li>NOTE: Failure to attend class because of dress code violations or prompt attendance
                                 is an unexcused
                                 absence. Thanks, Dr. Hudson (e-mail me only through ATLAS, not Blackboard and include
                                 your current
                                 Nursing course and a cell phone number) lhudson3@valenciacollege.edu
                                 
                              </li>
                              
                           </ul>
                           
                           <h4>Directions</h4>
                           
                           <p>Training will be conducted at the <a href="/documents/secure/nursing/ohs-computer-class-map.pdf" target="_blank">IS Education Training Center</a>, located at 1517
                              Sligh Blvd. (across Miller Street from the D-Deck parking garage). The D-Deck parking
                              garage is next to
                              the APH parking garage. We share the long building with Corporate Engineering â€“
                              we are right next door
                              to them. Please enter on the back side of the building, not the Sligh side. Our entrance
                              has red awnings.
                              
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Attention</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Subject: Orlando Health Important Ebola Message<br> From: Antonio Crespo MD<br> Ebola Management Plan
                              2014
                           </p>
                           
                           <p>Dear Colleagues,<br> For your safety, all physicians, clinicians and non-clinical team members should
                              follow the Orlando Health Ebola Management Plan to ensure you are adhering to the
                              approved protocols for
                              suspected and/or confirmed Ebola cases. The Orlando Health Ebola Management Plan is
                              attached to this
                              email, and is posted on the Physician Portal, the Nurse Portal and SWIFT under the
                              Ebola Information
                              banner.<br> All questions regarding the protocols detailed in the plan can be directed to me,
                              Antonio.CrespoMD@orlandohealth.com, or Eric Alberts, corporate manager, Emergency
                              Preparedness at
                              Eric.Alberts@orlandohealth.com.<br> It is very important that all physicians and team members familiarize
                              themselves with the plan at their earliest possible convenience. Thank you for your
                              cooperation and
                              adherence to the protocols that have been designed to protect you, our patients and
                              Orlando Health team
                              members.<br> Antonio Crespo, MD<br> Chief Quality Officer 
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>New Orlando Health Policy regarding Medication Administration</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>From: Brown, Stacey [mailto:Stacey.Brown@orlandohealth.com]<br> Sent: Monday, March 10, 2014 10:18 AM
                           </p>
                           
                           <p>Please share with faculty related changes in policy for students and faculty doing
                              Independent Double
                              Verification of medications and breast milk, etc.
                           </p>
                           
                           <ul>
                              
                              <li>Each professional must check: the actual prescriber’s order, the drug, the calculation,
                                 the
                                 concentration, last dose administered, and pertinent laboratory results specific to
                                 the dosing or
                                 administration of the medication
                                 
                              </li>
                              
                              <li>IDV must be performed before administering gastrostomy-jejunostomy (G/J) enteral feeds
                                 in pediatrics
                                 
                              </li>
                              
                              <li>When the chemotherapy dosage is based on the patient age, verification will include
                                 calculation of the
                                 patient age in months and years based on the patient’s date of birth
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Parking</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>It was recently brought to our attention that many Valencia students are parking in
                              the B deck garage,
                              next to APH, while taking computer classes. The parking arrangement for students is
                              the D deck garage.
                              Please inform the students they are to park in the D deck garage. The garage next
                              APH, B deck garage, is
                              for guests, visitors and a selected group of team members.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical-facility-ohs.pcf">©</a>
      </div>
   </body>
</html>