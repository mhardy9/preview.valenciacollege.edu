<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Nursing Students  | Valencia College</title>
      <meta name="Description" content="Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li>Nursing</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div>
                           
                           <h3>Welcome</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Welcome to the Valencia Nursing Program Secure Nursing Website which is exclusively
                              for enrolled Valencia Nursing Students!
                           </p>
                           
                           <p>Congratulations on your successful entrance into the Valencia Nursing Program. The
                              nursing faculty hope that this web site is easy for you to navigate. If you experience
                              any difficulty with the website please contact&nbsp;Dr.&nbsp;Leann Hudson, Clinical Nursing
                              Program Director. lhudson3@valenciacollege.edu.
                           </p>
                           
                           <p>Good luck in your studies!</p>
                           
                        </div>
                        
                        <hr class="styled_2"><br><hr class="styled_2"><br><hr class="styled_2">
                        
                        <div>
                           
                           <h3>Important Announcements</h3>
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                        <h3>Nursing Arts Lab Information</h3>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Osceola Nursing Lab will be closed from 4/10/2017 until the start of summer</li>
                              
                              <li>Spring 2017 - West &amp; Osceola Campus Nursing Lab Hours - <a href="/documents/secure/nursing/spring-2017-nursing-lab-hours.pdf">Click here</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <div>
                           
                           <h3>Helpful Documents</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li><a href="/documents/secure/nursing/spring-2017-nursing-handbook.pdf">Nursing Program Student Handbook</a></li>
                              
                              <li><a href="/documents/secure/nursing/health-physical-form.pdf" target="_blank">Health and Physical Form</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="strip_all_courses_list wow fadeIn animated" style="visibility: hidden; animation-delay: 0.1s; animation-name: none;" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list"><img src="/_resources/img/SECURE/nursing/RN-program-recognition.jpg" alt="Valencia's Registered Nursing (RN) Program ranked as the ninth best RN program in the state">
                                    
                                    <div class="short_info">
                                       
                                       <h3>Valencia's Registered Nurse Program Ranked Among Top Programs in Florida</h3>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">&nbsp;</div>
                              
                              <div class="col-lg-8 col-md-8 col-sm-8">
                                 
                                 <div class="course_list_desc">
                                    
                                    <p>In light of the nursing shortage facing Florida, Valencia has risen to meet the challenges
                                       of educating more nurses and, in effect, meeting our community's need. The College's
                                       work has recently been recognized by RegisteredNursing.com, which ranked Valencia's
                                       Registered Nursing (RN) Program as the ninth best RN program in the state. <a href="http://valenciacollege.us2.list-manage.com/track/click?u=f9512075b988841ea877a0102&amp;id=7c850ea593&amp;e=b88cb57120">Read what they said on The Grove.</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/index.pcf">©</a>
      </div>
   </body>
</html>