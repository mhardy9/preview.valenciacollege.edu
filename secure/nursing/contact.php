<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Contact  | Valencia College</title>
      <meta name="Description" content="Contact Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, contact, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/contact.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Contact </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Contact Us</h2>
                        
                        <hr class="styled_2">
                        
                        <p><strong>Nursing Office</strong><br> West Campus HSB 200
                        </p>
                        
                        <p><strong>Dean</strong><br> <a href="mailto:rsandrowitz@valenciacollege.edu">Ms. Rise Sandrowitz, RN, MSN</a><br>
                           407-582-1412
                        </p>
                        
                        <p><strong>Clinical Nursing Program Director</strong><br> <a href="mailto:akovalsky@valenciacollege.edu">Anita
                              Kovalsky, RN, MNEd, CNE</a> <br> 407-582-1023 
                        </p>
                        
                        <p><strong>Senior Administrative Assistant</strong><br> <a href="mailto:mdevonish@valenciacollege.edu">Marva
                              Devonish</a> <br> 407-582-1566 
                        </p>
                        
                        <p><a href="mailto:dbonillatorres@valenciacollege.edu">Daira Bonilla-Torres</a><br> 407-582-1118
                        </p>
                        
                        <p><strong>Manager, Student Services and Outreach</strong><br> <a href="mailto:smatadin@valenciacollege.edu">Samanta
                              Matadin</a></p>
                        
                        <p><strong>Career Program Advisor</strong><br> <a href="mailto:kjoseph21@valenciacollege.edu">Kenyatta
                              Joseph</a></p>
                        
                        <p><strong>Nursing Department Fax</strong><br> 407-582-1278
                        </p>
                        
                        <p><strong>Compliance</strong><br> Have questions about compliance? Send an email to <a href="mailto:HScompliance@valenciacollege.edu">HScompliance@valenciacollege.edu</a>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/contact.pcf">©</a>
      </div>
   </body>
</html>