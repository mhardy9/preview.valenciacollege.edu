<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>FIT Testing  | Valencia College</title>
      <meta name="Description" content="FIT Testing Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, compliance, FIT, testing, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/fit-testing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>FIT Testing </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>FIT Testing</h2>
                        
                        <hr class="styled_2">
                        
                        <p>For assistance with Compliance Issues, contact <a href="mailto:HSCompliance@valenciacollege.edu">HSCompliance@valenciacollege.edu</a>
                           
                        </p>
                        
                        
                        <p>ADD INFORMATION HERE ABOUT FIT TESTING</p>
                        
                        <p>Please note: you should have nothing to eat or dring (except water) 30 minutes to
                           the test appointment
                           time. Also refrain from smoking, mints or gum for 30 minutes prior to the test appointment
                           time.
                        </p>
                        
                        <div>
                           
                           <h3>FIT Test Renewal Schedule</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Location: Valencia West Campus, HSB, Room 120</li>
                              
                              <li>Time: 2:30 PM - 3:30 PM</li>
                              
                              <li>Dates:
                                 
                                 <ul>
                                    
                                    <li>January 19, 2017</li>
                                    
                                    <li>February 16, 2017</li>
                                    
                                    <li>March 23, 2017</li>
                                    
                                    <li>April 13, 2017</li>
                                    
                                    <li>May 18, 2017</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>To request an appointment, email <a href="mailto:hscompliance@valeniacollege.edu">HSCompliance@valenciacollege.edu</a>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/fit-testing.pcf">©</a>
      </div>
   </body>
</html>