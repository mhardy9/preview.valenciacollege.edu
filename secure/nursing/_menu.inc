 <ul>
            <li class="submenu">
<a href="index.php" class="show-submenu">Nursing Students <i class="far fa-angle-down" aria-hidden="true"></i></a>
              <ul>
                <li><a href="career-options.php">Career Options</a></li>
                <li><a href="medical-exception-withdrawal.php">Medical Exception Withdrawal</a></li>
                <li><a href="nursing-arts-lab.php">Nursing Arts Lab</a></li>
                <li><a href="program-progression.php">Program Progression</a></li>
                <li><a href="registration.php">Registration Information</a></li>
                <li><a href="scholarships.php">Scholarships</a></li>
              </ul>
            </li>
            <li class="submenu">
<a href="compliance.php" class="show-submenu">Compliance<i class="far fa-angle-down" aria-hidden="true"></i></a>
              <ul>
                <li><a href="cpr-certification.php">CPR Certification</a></li>
                <li><a href="fit-testing.php">FIT Testing</a></li>
                <li><a href="immunizations.php">Immunizations</a></li>
                <li><a href="resources.php">Resources</a></li>
              </ul>
            </li>
            <li class="submenu">
<a href="clinical.php" class="show-submenu">Clinical<i class="far fa-angle-down" aria-hidden="true"></i></a>
              <ul>
                <li><a href="clinical-facility-FL-hospital.php"><i class="icon-map-1"></i> Florida Hospital</a></li>
                <li><a href="clinical-facility-health-central.php"><i class="icon-map-1"></i> Health Central</a></li>
                <li><a href="clinical-facility-lakeside.php"><i class="icon-map-1"></i> Lakeside Alternative</a></li>
                <li>
<a href="clinical-facility-nemours.php"><i class="icon-map-1"></i> Nemours Children's Hospital</a>
                </li>
                <li><a href="clinical-facility-ohs.php"><i class="icon-map-1"></i> Orlando Health System</a></li>
                <li><a href="clinical-facility-osceola.php"><i class="icon-map-1"></i> Osceola Regional Health Center
                </a></li>
                <li><a href="clinical-facility-st-cloud.php"><i class="icon-map-1"></i> St. Cloud Regional Medical
                  Center</a></li>
                <li><a href="clinical-uniforms.php">Uniform Information</a></li>
              </ul>
            </li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
   
