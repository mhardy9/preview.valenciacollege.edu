<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources  | Valencia College</title>
      <meta name="Description" content="Resources for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, resources, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Clinical</h2>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Helpful Links</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>
                                 <a href="http://hospitalenglish.com/" target="_blank">Hospital English</a> This site contains free
                                 medical English teaching materials and hospital English worksheets, for studying English,
                                 as well as
                                 medical English lesson plans for teachers. I host medical vocabulary builders, patient
                                 counseling
                                 activities, disease state directors, healthcare professional articles, medical flashcards,
                                 hospital
                                 lesson plans, ESP worksheets, and more to come.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.micromedexsolutions.com/home/dispatch" target="_blank">Micromedex® DRUG GUIDE</a>
                                 This index is the one that most hospitals use. It is a great resource for drug information
                                 and client
                                 teaching!<br> <strong>Username:</strong> Valencia<br><strong>Password:</strong> ccla29
                                 
                              </li>
                              
                              <li>
                                 <a href="/documents/secure/nursing/ohs-sunrise-blackboard-how-to-enroll.pdf" target="_blank">Sunrise
                                    Super How-To</a> How to enroll in Blackboard Sunrise Super (Sign-Up for Patient Electronic Record)
                                 Center Computer Class.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.mometrix.com/academy/nclex-exam/" target="_blank">NCLEX-RN Content Review and
                                    Practice Questions</a>- Please note that the practice questions are at the Direct Knowledge (factual)
                                 level rather than at the Analysis and Application levels (Critical Thinking), the
                                 latter of which are
                                 the majority of questions on the NCLEX-RN Exam.
                                 
                              </li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        
                        
                        <div>
                           
                           <h3>Dosage Practice</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <h4>Steps of Dimensional Analysis </h4>
                           
                           <p>
                              <iframe src="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/33226061/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1461961423&amp;entry_id=1_bquuqiy7&amp;flashvars%5BstreamerType%5D=auto" width="400" height="300" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                              
                           </p>
                           
                           <h4>Liquid Medication &amp; Fluid Input Measurement &amp; Calculations </h4>
                           
                           <p>
                              <iframe src="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/33226061/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1461961668&amp;entry_id=1_ja74kh66&amp;flashvars%5BstreamerType%5D=auto" width="400" height="300" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                              
                           </p>
                           
                           <h4>Temperature Conversion Calculations</h4>
                           
                           <p>
                              <iframe src="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/33226061/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1461961703&amp;entry_id=1_6nu467vj&amp;flashvars%5BstreamerType%5D=auto" width="400" height="300" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                              
                           </p>
                           
                           <h4>IV Infusion Calculations &amp; Reading Medication Labels</h4>
                           
                           <p>
                              <iframe src="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/33226061/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1461961765&amp;entry_id=1_87tc248h&amp;flashvars%5BstreamerType%5D=auto" width="400" height="300" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                              
                           </p>
                           
                           <h4>Syringes &amp; Dosage Calculation</h4>
                           
                           <p>
                              <iframe src="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/33226061/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1461962200&amp;entry_id=1_jk0vbino&amp;flashvars%5BstreamerType%5D=auto" width="400" height="300" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                              
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div>
                           
                           <h3>Dosage Practice</h3>
                           
                           <p></p>
                           
                           <ul>
                              
                              <li><a href="/documents/secure/nursing/dosage-practice-rounding-practice.pdf" target="_blank">Rounding
                                    Practice Test</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-practice-hypodermic-insulin-syringe.pdf" target="_blank">Hypodermic
                                    and Insulin Syringe Practice Test</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-ranges.pdf" target="_blank">Dosage Range
                                    Information</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-test.pdf" target="_blank">Dosage Range
                                    Practice Test</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-test-answer-key.pdf" target="_blank">Dosage
                                    Range Practice Answer Key</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-questions-1.pdf" target="_blank">Practice
                                    Test #1</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-questions-2.pdf" target="_blank">Practice
                                    Test #2</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-questions-3.pdf" target="_blank">Practice
                                    Test #3</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-questions-4.pdf" target="_blank">Practice
                                    Test #4</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-questions-5.pdf" target="_blank">Practice
                                    Test #5</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-questions-6.pdf" target="_blank">Practice
                                    Test #6</a></li>
                              
                              <li><a href="/documents/secure/nursing/dosage-calculation-practice-questions-7.pdf" target="_blank">Practice
                                    Test #7</a></li>
                              
                           </ul>
                           
                           <p></p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div>
                           
                           <h3>
                              Learning Modules for All Nursing Courses
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>These self-directed mini-courses are free, and designed to assist the nursing student
                              to achieve greater
                              success.
                           </p>
                           
                           <li>
                              <a href="http://faculty.valenciacollege.edu/avs/lm/index.html" target="_blank">Nursing I/AVS
                                 Transitions</a> - Strategies for Test Success
                              
                           </li>
                           
                           <li>
                              <a href="http://faculty.valenciacollege.edu/avs/lm2/index.html" target="_blank">Nursing II/AVS Adult
                                 Health </a>- Critical Thinking/Med-Surg
                              
                           </li>
                           
                           <li>
                              <a href="http://faculty.valenciacollege.edu/avs/lm3/index.html" target="_blank">Nursing III/AVS Maternal
                                 Child </a>- Critical Thinking/Pregnant Patient
                              
                           </li>
                           
                           <li>
                              <a href="http://faculty.valenciacollege.edu/avs/lm4/index.html" target="_blank">Nursing IV/AVS Advanced
                                 Adult Health </a>- Critical Thinking and the Patient with Diabetes Mellitus
                              
                           </li>
                           
                           <li>
                              <a href="http://faculty.valenciacollege.edu/avs/lm5/index.html" target="_blank">Nursing V/AVS PCMP </a>-
                              Nursing and the Art of Delegation
                              
                           </li>
                           
                           
                           
                           <hr>
                           
                           <p><a href="http://www.mometrix.com/academy/nclex-exam/" target="_blank" class="bgColor2ContentBox">NCLEX-RN
                                 Content Review and Practice Questions</a> - Please note that the practice questions are at the Direct
                              Knowledge (factual) level rather than at the 
                           </p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/resources.pcf">©</a>
      </div>
   </body>
</html>