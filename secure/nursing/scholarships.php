<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Nursing Students  | Valencia College</title>
      <meta name="Description" content="Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Nursing Students </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Scholarships</h2>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Health Central Nursing Scholarship</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><a href="/documents/secure/nursing/health-central-scholarship.pdf" class="button-outline" target="_blank">Download Application</a></p>
                           
                           <p>The Health Central Nursing Scholarship Program is a win, win program</p>
                           
                           <ul>
                              
                              <li>Provides a two (2) year RN Scholarship.</li>
                              
                              <li>Provides for LPN or Paramedic to RN transition program.</li>
                              
                              <li>Guarantees a Nursing position upon graduation.</li>
                              
                              <li>Provides Nurses that supply high quality care to our patients.</li>
                              
                           </ul>
                           
                           <p>Scholarships are currently offered for Valencia, South Seminole, Lake Sumter &amp; UCF.</p>
                           
                           <p>The student agrees to work at Health Central as a Registered Nurse, for one (1) year
                              or a fraction
                              thereof for each scholl year or fraction thereof for which he/she received scholarship
                              money.
                           </p>
                           
                           <p>The program covers such items as:</p>
                           
                           <ul>
                              
                              <li>Tuition - generally not to exceed 2 years</li>
                              
                              <li>Supply fees</li>
                              
                              <li>FDLE check</li>
                              
                              <li>Liability Insurance</li>
                              
                              <li>Textbooks</li>
                              
                              <li>Uniforms</li>
                              
                              <li>Watch (3 hands), stethoscope, hemostat, scissors</li>
                              
                              <li>ERI testing package</li>
                              
                              <li>Student nurses association dues</li>
                              
                              <li>State Board exam fees and licensure</li>
                              
                              <li>Graduation application &amp; pin</li>
                              
                           </ul>
                           
                           <hr class="styled_2">
                           
                           <div>
                              
                              <h3>How do I apply?</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <ul>
                                 
                                 <li>Complete the application.</li>
                                 
                                 <li>Supply official transcript of grades from current learning institution.</li>
                                 
                                 <li>Include a copy of the acceptance letter from the school you plan to attend.</li>
                                 
                                 <li>include a current resume and at least 2 letters of recommendation</li>
                                 
                                 <li>Submit to:
                                    
                                    <blockquote>Judy Szuder, HR Coordinator/Benefits<br> Health Central<br> 10000 West Colonial Drive<br>
                                       Ocoee, FL 34761
                                       
                                    </blockquote>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Orange County Medical Society Alliance Scholarship</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><a href="/documents/secure/nursing/scholarship-orange-county-medical-society.pdf" class="button-outline" target="_blank">Download Application</a></p>
                           
                           <p>The Orange County Medical SOciety Alliance is pleased to announce that we are now
                              accepting applications
                              for the OCMSA Scholarship Fund ("The Fund"). Last year we granted several scholarships
                              ranging from $200
                              to $800, totaling $2,500. We encourage you to identify yourself as a student with
                              need and apply to The
                              Fund using the application form linked below.
                           </p>
                           
                           <p>The Fund provides EMERGENCY grant assistance to nursing students enrolled in an accredited
                              healthcare
                              studies program leading to an RN or BSN. The application deadline is March 10, 2017.
                              The grants will be
                              distributed in April 2017. The appication must be completed and forwarded with a an
                              attached cover letter
                              from your school confirming that you are currently enrolled and in good academic standing.
                           </p>
                           
                           <hr class="styled_2">
                           
                           <div>
                              
                              <h3>How do I apply?</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>The completed application should be sent to:
                                 
                              </p>
                              
                              <blockquote>OCMSAF<br> c/o Peña<br> 7597 St. Stephens Ct<br> Orlando, FL 32835
                                 
                              </blockquote>
                              
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div>
                           
                           <h3>HRSA NURSE Corps Scholarship Program</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><a href="https://bhw.hrsa.gov/loansscholarships/nursecorps/scholarship" class="button-outline" target="_blank"> Scholarship Info</a></p>
                           
                           <p>Too many Americans—particularly in underserved areas—go without checkups, preventive
                              screenings,
                              vaccines, and other care, simply because there are not enough health care professionals
                              to provide care
                              and treatment in their communities. The NURSE Corps Scholarship Program enables you
                              to fulfill your
                              passion to care for underserved people in some of the neediest communities across
                              the country.
                           </p>
                           
                           
                           <p>If awarded, you will receive financial support, including:
                              
                           </p>
                           
                           <ul>
                              
                              <li>tuition;</li>
                              
                              <li> eligible fees;</li>
                              
                              <li>annual payment for other reasonable costs, such as books, clinical supplies/instruments
                                 and uniforms;
                                 and
                                 
                              </li>
                              
                              <li> a monthly stipend ($1,334 for the 2017-2018 academic year).</li>
                              
                           </ul>
                           
                           
                           <hr class="styled_2">
                           
                           <div>
                              
                              <h3>How do I apply?</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p>Before you apply, read the annually updated <a href="https://bhw.hrsa.gov/sites/default/files/bhw/nursecorpsguidance.pdf" target="_blank">Application
                                    and Program Guidance</a>. Make sure you understand the terms and conditions of the NURSE Corps contract,
                                 which outlines the requirement for fulfilling your minimum two years of service at
                                 an eligible Critical
                                 Shortage Facility.
                                 
                              </p>
                              
                              <p><a href="https://programportal.hrsa.gov/extranet/application/ncsp/login.seam" target="_blank">Apply for
                                    the 2017 NURSE Corps Scholarship Program</a></p>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Valencia Nursing &amp; Health Related Scholarships</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><a href="/documents/secure/nursing/scholarship-valencia-foundation.pdf" class="button-outline" target="_blank"> Scholarship Info</a></p>
                           
                           <p>Valencia Foundation offers multiple scholarships</p>
                           
                           <p>Information about Valencia Scholarships can be found here</p>
                           
                           
                           
                           <hr class="styled_2">
                           
                           <div>
                              
                              <h3>How do I apply?</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <ol>
                                 
                                 <li>Please visit <a href="https://fafsa.ed.gov/" target="_blank">fafsa.ed.gov</a> to complete the Free
                                    Application for Federal Financial Aid (FAFSA)
                                    
                                 </li>
                                 
                                 <li>Set up a new account, or log into an existing account on <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia
                                       College Foundation's scholarship application portal</a>
                                    
                                 </li>
                                 
                              </ol>
                              
                              <p>If you need assistance during the scholarship applicaton process, please contact the
                                 Foundation
                                 Scholarships Team at <a href="mailto:foundation_finaid@valenciacollege.edu">foundation_finaid@valenciacollege.edu</a>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/scholarships.pcf">©</a>
      </div>
   </body>
</html>