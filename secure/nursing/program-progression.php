<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Program Progression  | Valencia College</title>
      <meta name="Description" content="Program Progression Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/program-progression.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Program Progression </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        
                        <h2>Program Progression</h2>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Resequencing and Reinstatement Policy </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>A student who is unsuccessful or stopping-out of the Nursing Program who wants to
                              be considered for
                              resequencing or reinstatement may be reinstated based upon on space availability and
                              eligibility. Due to
                              the limited capacity of the program, regardless of circumstances, students are not
                              guaranteed
                              reinstatement in the program at any time. It may take multiple terms in order to be
                              reinstated. The
                              "Limitations of Re-Entry into Nursing Program Policy" will apply. Reinstatement and
                              Resequencing is on a
                              space-available basis and this may take several terms.
                           </p>
                           
                           
                           <p>Many factors are used to determine eligibility for Resequencing and Reinstatement
                              within the Nursing
                              Program. These may include: grade point average, theory grades, clinical performance
                              and evaluation,
                              participation in remediation with course faculty members and/or the Educational Specialist,
                              as well as
                              Nursing Program admission entrance test scores. Questions regarding this policy may
                              be directed to the
                              Course Leader(s) .
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Resequencing and Reinstatement Requirements and Procedure</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Students who wish to be considered for Resequencing and Reinstatement after receiving
                              their first D, F,
                              or W, or students who opt to “Stop Out” of the Nursing Program for personal reasons
                              must:
                           </p>
                           
                           <ul>
                              
                              <li> Submit the <a href="/documents/secure/nursing/program-progression-resequencing-reinstatement-form.pdf" target="_blank">Request for Resequencing and Reinstatement Request Form</a> (directions for submission
                                 are on the form)
                                 
                              </li>
                              
                              <li>Submit a detailed Plan for Improvement (along with the form)</li>
                              
                           </ul>
                           
                           
                           <div class="wrapper_indent">
                              
                              
                              <h4>Reinstatement and Resequencing documents must include the following</h4>
                              
                              <ul>
                                 
                                 <li>Student Name</li>
                                 
                                 <li> Student VID #</li>
                                 
                                 <li>The name of the course and course number for which the student received a D, F, or
                                    W
                                 </li>
                                 
                                 <li>The date and term that the course was taken for which the student received a D, F,
                                    or W
                                 </li>
                                 
                                 <li>An explanation for the reason the D, F, or W was received in the course</li>
                                 
                                 <li> A detailed plan for success in the subsequent terms. This plan should include the
                                    following:
                                    
                                    <ul>
                                       
                                       <li>improvements for studying</li>
                                       
                                       <li>time management</li>
                                       
                                       <li>early intervention (seeking assistance early in the term)</li>
                                       
                                       <li>test review (attend scheduled test reviews)</li>
                                       
                                       <li>outside resources (tutoring)</li>
                                       
                                       <li>increased support (from family &amp; friends)</li>
                                       
                                       <li>identify the factors for which led to the student being unsuccessful in the course
                                          and how you
                                          will modify them to insure success
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <p>Note: Students who “Stop Out” but have not failed a course do not need to submit a
                                 Plan for
                                 Improvement. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>"Stop Out” of the Nursing Program Policy/Procedure</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Students who pass the course in which they are currently enrolled and desire to "Stop
                              Out" of the Nursing
                              Program due to serious illness (physical or psychological) or injury, personal, or
                              financial reasons must
                              do the following:
                           </p>
                           
                           <ul>
                              
                              <li>Meet with the Clinical Nursing Program Director. All information provided by the student
                                 will remain
                                 private and will be shared only with school officials for legitimate educational reasons.
                                 
                              </li>
                              
                              
                              <li>Submit a <a href="/documents/secure/nursing/program-progression-resequencing-reinstatement-form.pdf" target="_blank">Request for Resequencing and Reinstatement Request Form</a>
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <p>The "Limitations of Re-Entry into Nursing Program Policy" will apply. Reinstatement
                              and Resequencing is
                              on a space available basis and this may take several terms.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Limitations of Re-Entry into Nursing Program Policy</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>A student who is out of progression in the Nursing Program may request readmission
                              under the following
                              conditions:
                           </p>
                           
                           <ul>
                              
                              <li> No more than two (2) consecutive academic terms have elapsed since exit from the
                                 last Nursing course
                                 
                              </li>
                              
                              <li>Resequencing must occur at the beginning of a course.</li>
                              
                              <li>Students may only request Reinstatement and Resequencing one time.</li>
                              
                           </ul>
                           
                           
                           <p>Example: A student who is enrolled in the Fall term must return by the following Fall
                              term
                           </p>
                           
                           
                           <p>NOTE: Students who fail an NUR class are readmitted to the Nursing Program on a space
                              available basis.
                              There is no guarantee of Reinstatement and Resequencing, and this may take several
                              terms, however, the
                              Limitations of Re-Entry into the Program Policy will apply
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/program-progression.pcf">©</a>
      </div>
   </body>
</html>