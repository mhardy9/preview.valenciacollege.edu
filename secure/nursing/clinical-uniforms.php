<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical Uniform Information  | Valencia College</title>
      <meta name="Description" content="Uniform Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical-uniforms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical Uniform Information </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Uniform Information</h2>
                        
                        <hr class="styled_2">
                        
                        
                        
                        <table class="table ">
                           
                           <tr>
                              
                              <th width="50%" class="headSub_1">Do's</th>
                              
                              <th width="50%" class="headSub_1">Don'ts</th>
                              
                           </tr>
                           
                           <tr bgcolor="#CCCCCC">
                              
                              <td class="headSub_1">&nbsp;</td>
                              
                              <td>
                                 
                                 <ul>
                                    
                                    <li>Inappropriate</li>
                                    
                                    <li>Shirt out</li>
                                    
                                    <li>Necklace</li>
                                    
                                    <li>Gray T</li>
                                    
                                    <li>Brown shoes</li>
                                    
                                 </ul>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr align="center" bgcolor="#CCCCCC">
                              
                              <td>
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-tucked.jpg" width="180" height="300" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                              <td>
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-not-untucked.jpg" width="175" height="300" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>&nbsp;</td>
                              
                              <td>
                                 
                                 <ul>
                                    
                                    <li>Inappropriate</li>
                                    
                                    <li>Gray T</li>
                                    
                                    <li>Brown shoes</li>
                                    
                                 </ul>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr align="center">
                              
                              <td>
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-white-t.jpg" width="178" height="300" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                              <td>
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-not-gray-t.jpg" width="179" height="300" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr bgcolor="#CCCCCC">
                              
                              <td>&nbsp;</td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr align="center" valign="top" bgcolor="#CCCCCC">
                              
                              <td>
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-labcoat.jpg" width="180" height="435" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>&nbsp;</td>
                              
                              <td>
                                 
                                 <ul>
                                    
                                    <li>Inappropriate</li>
                                    
                                    <li>Hair down</li>
                                    
                                    <li>Jewelry</li>
                                    
                                    <li>Capri pants</li>
                                    
                                    <li>Flip flops</li>
                                    
                                 </ul>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr align="center" valign="top">
                              
                              <td>
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-pants.jpg" width="180" height="385" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                              <td>
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-not-capri.jpg" width="180" height="404" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr valign="top">
                              
                              <td bgcolor="#CCCCCC">
                                 
                                 <ul>
                                    
                                    <li>Appropriate for getting patient assignment</li>
                                    
                                    <li>Appropriate for visit to clinical facility</li>
                                    
                                 </ul>
                                 
                              </td>
                              
                              <td bgcolor="#CCCCCC">&nbsp;</td>
                              
                           </tr>
                           
                           <tr align="center" valign="top">
                              
                              <td bgcolor="#CCCCCC">
                                 
                                 <p><img src="/_resources/img/SECURE/nursing/uniform-do-clinical-visit.jpg" width="180" height="382" border="1"></p>
                                 
                                 <p>&nbsp;</p>
                                 
                              </td>
                              
                              <td bgcolor="#CCCCCC">&nbsp;</td>
                              
                           </tr>
                           
                        </table>
                        
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical-uniforms.pcf">©</a>
      </div>
   </body>
</html>