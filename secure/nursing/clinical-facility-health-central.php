<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical Facility - Health Central  | Valencia College</title>
      <meta name="Description" content="Clinical Information for Health Central for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, facilities, health, central, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical-facility-health-central.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical Facility - Health Central </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Clinical Facility - Health Central</h2>
                        
                        <hr class="styled_2">
                        
                        <p>Website: <a href="http://www.orlandohealth.com/facilities/health-central-hospital" target="_blank">http://www.orlandohealth.com/facilities/health-central-hospital</a><br>
                           Location: <a href="http://www.orlandohealth.com/facilities/health-central-hospital/map-and-directions" target="_blank">10000 W Colonial Drive, Ocoee, FL 34761</a></p>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>General Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Discounts - You will get a 30% discount in the cafeteria (the same as for hospital
                                 employees).
                              </li>
                              
                              <li>Parking - <a href="/documents/secure/nursing/health-central-parking.pdf" target="_blank">Download
                                    Parking Map</a>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Health Central Orientation</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>These are the mandatory items to complete for Health Central Orientation.</p>
                           
                           <p><i class="far fa-book fa-spacer"></i> Annual Corporate Education (ACE) - Self-Learning Packet 2017 is to be
                              completed at least 3 weeks prior to the first clinical day. When you are ready to
                              take the post test print
                              out the exam and give the GRADED ACE Exam to the clinical coordinator (located in
                              HSB 200 at West Campus).
                              This is an open book test. Please refer to the Annual Corporate Education (ACE) 2017
                              Exam Answer Key AFTER
                              you take the quiz and grade your test. 
                           </p>
                           
                           <ul>
                              
                              <li>Read this packet: <a href="/documents/secure/nursing/health-central-ACE-packet.pdf" target="_blank">Annual
                                    Corporate Education (ACE) Self-Learning Packet 2017 </a>
                                 
                              </li>
                              
                              <li>Use this key to self grade your test: <a href="/documents/secure/nursing/health-central-ACE-exam-key.pdf" target="_blank">Annual Corportate
                                    Education (ACE) Exam Key 2017</a>
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Complete this form: <a href="/documents/secure/nursing/TURN-IN-health-central-ACE-exam.pdf" target="_blank"> Annual Corporate
                                    Education (ACE) Exam 2017</a> &amp; turn in to the clinical coordinator (West Campus: HSB-200)
                                 
                              </p>
                              
                           </div>
                           
                           
                           <p><i class="far fa-book fa-spacer"></i> The Confidentiality Form is to be completed at least 3 weeks prior to the
                              first clinical day. Click on the link below and print the form.
                           </p>
                           
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Print this form: <a href="/documents/secure/nursing/TURN-IN-health-central-confidentiality-statement.pdf" target="_blank"> Student Confidentiality Form</a> &amp; turn in to the clinical coordinator (West
                                 Campus: HSB-200)
                              </p>
                              
                           </div>
                           
                           
                           <p><i class="far fa-book fa-spacer"></i> A Health Central Badge is required. Photographs will be taken by your
                              clinical coordinator (located in HSB 200 at West Campus) at least 3 weeks prior to
                              the first clinical day.
                              Badges will distributed by your clinical instructor on the first day of clinical and
                              must be returned at
                              the end of the semester. Give the fully completed Badge Form below to the clinical
                              coordinator (located in
                              HSB 200 at West Campus) when you come to have your photograph taken. Be sure to include
                              your Emergency
                              Contact person on the form. 
                           </p>
                           
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Print this form: <a href="/documents/secure/nursing/TURN-IN-health-central-id-badge-form.pdf" target="_blank"> I.D. Badge
                                    Data Form </a>, fill in the checked areas &amp; turn in to the clinical coordinator (West Campus:
                                 HSB-200)
                              </p>
                              
                           </div>
                           
                           
                           <p><i class="far fa-book fa-spacer"></i> Computer Access - To receive a sign-on code, see the clinical coordinator
                              (located in HSB 200 at West Campus) when you come to be photograghed, and you will
                              fill out the
                              approproiate paperwork at that time. This must be at least 3 weeks prior to the first
                              clinical day.
                              Faculty will orient you to the computer system (on the job training). You wll be given
                              your sign-on code
                              by your clinical instructor on the first day of clinical. 
                           </p>
                           
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Nothing to print. You will fill out paperwork in person when you see the
                                 clinical coordinator.
                              </p>
                              
                           </div>
                           
                           
                           <p><i class="far fa-book fa-spacer"></i> Immunizations - All students must show documentation of their negative PPD
                              status (printer from your tracker) to the clinical coordinator (located in HSB 200
                              at West Campus) when
                              you come to be photograghed. Health Central is requiring that all team members, volunteers,
                              students,
                              vendors and physicians either be vaccinated against influenza or wear a surgical mask
                              while within six
                              feet of patients during peak flu months
                           </p>
                           
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Fill out: <a href="/documents/secure/nursing/TURN-IN-health-central-flu%20declination.pdf">Flu Declination Form</a>
                                 only if you decline the vaccination.
                              </p>
                              
                           </div>
                           
                           <hr class="styled_2">
                           
                           <div>
                              
                              <h3>Computer Training Information</h3>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              <p>To receive a sign-on code, see the clinical coordinator (located in HSB 200
                                 at West Campus) when you come to be photographed, and you will fill out the appropriate
                                 paperwork at that
                                 time. This must be at least 3 weeks prior to the first clinical day. Faculty will
                                 orient you to the
                                 computer system (on the job training). You will be given your sign-on code by your
                                 clinical instructor on
                                 the first day of clinical.
                              </p>
                           </div>
                           
                           <hr class="styled_2">
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical-facility-health-central.pcf">©</a>
      </div>
   </body>
</html>