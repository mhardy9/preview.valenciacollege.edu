<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical  | Valencia College</title>
      <meta name="Description" content="Clinical Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Clinical</h2>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>How do I Contact my Clinical Instructor?</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p> Most clinical instructors will give you a cell phone number by which to contact them,
                              however, some do
                              not. In your course syllabus, you should find Full-Time (FT) cell and office phone
                              numbers and email
                              addresses, as well as contact information for adjunct faculty as indicated.
                           </p>
                           
                           <p>If you do not have a clinical instructors' cell phone number, please call your Course
                              Leader to relay the
                              message to your clinical instructor.
                           </p>
                           
                           <p>Please do not leave messages on faculty office phones, as they will not get them in
                              a timely manner. If
                              your call is related to clinical, (absences, tardiness, etc.) it is IMPERATIVE that
                              you talk to faculty
                              members personally.
                           </p>
                           
                           <p>Only use your nursing course in Blackboard to send messages to your instructors for
                              non-clinical/non-emergency issues.
                           </p>
                           
                           <p>NOTE: If your faculty give you information which is different than what is noted here,
                              please follow
                              their directions. 
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Announcements</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>There are no Clinical Announcements at this time</p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Nursing II Forms</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li><a href="/documents/secure/nursing/clinical-assessment-checklist.pdf" target="_blank">Assessment
                                    Checklist</a></li>
                              
                              <li><a href="/documents/secure/nursing/clinical-concept-map-blank.pdf" target="_blank">Concept Map Nursing
                                    Arts Lab</a></li>
                              
                              <li><a href="/documents/secure/nursing/clinical-concept-map-diagnosis.pdf" target="_blank">Concept Map for
                                    Diagnosis</a></li>
                              
                              <li><a href="/documents/secure/nursing/clinical-concept-map-medication.pdf" target="_blank">Concept Map
                                    for Medications</a></li>
                              
                              <li><a href="/documents/secure/nursing/clinical-narrative-documentation.pdf" target="_blank">Narrative
                                    Documentation</a></li>
                              
                              <li>
                                 <a href="/documents/secure/nursing/clinical-nursing-process.pdf" target="_blank">Nursing Process</a>
                                 
                              </li>
                              
                              <li><a href="/documents/secure/nursing/clinical-preconference-report.pdf" target="_blank">Pre-Conference
                                    Report </a></li>
                              
                              <li><a href="/documents/secure/nursing/clinical-prohibited-procedures.pdf" target="_blank">Procedures
                                    Students are Prohibited from Performing</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical.pcf">©</a>
      </div>
   </body>
</html>