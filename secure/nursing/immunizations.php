<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Immunizations  | Valencia College</title>
      <meta name="Description" content="Immunizations Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, compliance, immunizations, flu, shots, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/immunizations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Immunizations </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Immunizations</h2>
                        
                        <hr class="styled_2">
                        
                        <p>For assistance with Compliance Issues, contact <a href="mailto:HSCompliance@valenciacollege.edu">HSCompliance@valenciacollege.edu</a>
                           
                        </p>
                        
                        <div>
                           
                           <h3>Flu Vaccine Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Regarding the FLU Vaccine, you do not need a special form for each hospital. When
                              you get your FLU SHOT,
                              you upload into Castle Branch the document give to you by whoever administers your
                              FLU SHOT. Then you
                              present a hard copy of that document to whomever is giving you a sticker at the hospital.
                           </p>
                           
                           <p>Also attached is the FLU DECLINATION which is on the first page of the <a href="/documents/secure/nursing/health-physical-form.pdf" target="_blank">Health and Physical Form</a>.
                              This is what you use for FLU DECLINICATION, as there is no longer a separate Flu Declination
                              Form. Again,
                              you will upload this form into Castle Branch if you are declining the flu shot.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Florida Hospital Flu Vaccine Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>As a reminder, Flu Shot Season begins December 1, 2016 and runs through March 31,
                              2017.
                           </p>
                           
                           <p>Students &amp; Faculty of affiliated nursing programs may receive a seasonal influenza
                              vaccine at no cost
                              at any Florida Hospital facility.
                           </p>
                           
                           <p>Due to National Healthcare Safety Network (NHSN) reporting requirements for healthcare
                              facilities,
                              schools must submit student/faculty verification of Flu Shot using <a href="/documents/secure/nursing/FL-hospital-flu-vaccination-form.pdf">FH Flu Vaccination Program
                                 Student/Observer Form</a> for all students &amp; faculty.
                           </p>
                           
                           <p>Students declining the vaccine will be required to complete a declanation form and
                              will be required to
                              wear a surgical mask while in patient care areas during the designated time period.
                           </p>
                           
                           <p>Flu shot forms will be due to Florida Hospital by December 1, 2016. All forms are
                              to be sent to <a href="mailto:FH.Academic.Programs@flhosp.org">FH.Academic.Programs@flhosp.org</a></p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Orlando Health Flu Vaccine Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>Stacey Brown, RN-BC, BSN, MSN/Ed</strong><br> Student Coordinator<br> Education Programs, Orlando
                              Health Institute for Learning<br> <a href="mailto:student.coordinator@orlandohealth.com">student.coordinator@orlandohealth.com</a>
                              
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Osceola Regional Hospital Flu Vaccine Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Students must be vaccinated by November 11, 2016. Masks must be worn in the hospital
                              between 11/1/2016
                              through 3/31/2017 if not vaccinated.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/immunizations.pcf">©</a>
      </div>
   </body>
</html>