<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical Facility - Nemours Children's Hospital  | Valencia College</title>
      <meta name="Description" content="Clinical Facility - Nemours Children's Hospital Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, facilities, nemours, children, hospital, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical-facility-nemours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical Facility - Nemours Children's Hospital </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Nemours Children's Hospital</h2>
                        
                        <hr class="styled_2">
                        
                        <p>Website: <a href="https://www.nemours.org/locations/nch.html" target="_blank">https://www.nemours.org/locations/nch.html</a><br>
                           Location: <a href="https://maps.google.com/maps?q=Nemours+Children%27s+Hospital,+Nemours+Parkway,+Orlando,+FL&amp;hl=en&amp;sll=28.375006,-81.273925&amp;sspn=0.022844,0.028903&amp;oq=Nemours+Chi&amp;hq=Nemours+Children%27s+Hospital,+Nemours+Parkway,+Orlando,+FL&amp;t=m&amp;z=17" target="_blank">13535 Nemours Parkway, Orlando, FL 32827</a></p>
                        
                        <div>
                           
                           <h3>Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You will be badged on the first day of clinical.</p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Nemours Orientation</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>There are 3 items to turn into your course faculty 3 weeks before your rotation begins.
                              Please read the
                              following carefully.
                           </p>
                           
                           <p><i class="far fa-book fa-spacer"></i> Student Orientation Packet. Read the following packet and sign the packet
                              acknowledgement form (on the last page) and turn it in to your faculty instructor
                              3 weeks prior to your
                              first clinical day. 
                           </p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Print the last page of <a href="/documents/secure/nursing/TURN-IN-nemours-orientation-packet.pdf">Student Orientation Packet</a>
                                 &amp; turn into faculty instructor.
                              </p>
                              
                           </div>
                           
                           <p><i class="far fa-book fa-spacer"></i> Complete the Student Web Based Education Requirements. This web-based
                              module is below (Internal NU Access Instructions). A completed transcript should be
                              printed and turned
                              into your faculty instructor 3 weeks prior to your clinical rotation. 
                           </p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Follow the instructions for this <a href="/documents/secure/nursing/nemours-training-how-to.pdf">Web Based Training</a> &amp; turn a
                                 completed transcript into faculty instructor when finished.
                              </p>
                              
                           </div>
                           
                           <p><i class="far fa-book fa-spacer"></i> Complete the Information Confidentiality &amp; User Agreement. Turn into
                              your faculty instructor 3 weeks prior to your clinical rotation.
                           </p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> <a href="/documents/secure/nursing/TURN-IN-nemours-confidentiality.pdf">Information
                                    Confidentiality &amp; User Agreement</a></p>
                              
                           </div>
                           
                           <p><i class="far fa-book fa-spacer"></i> Read the <a href="/documents/secure/nursing/nemours-id-requirements.pdf">Identification
                                 Requirements</a></p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="far fa-upload fa-spacer"></i> Nothing to turn in. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical-facility-nemours.pcf">©</a>
      </div>
   </body>
</html>