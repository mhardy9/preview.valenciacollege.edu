<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical Facility - Osceola Regional Health Center  | Valencia College</title>
      <meta name="Description" content="Clinical Facility -Osceola Regional Health Center  Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, facilities, osceola, regional, health, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical-facility-osceola.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical Facility - Osceola Regional Health Center </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Osceola Regional Health Center </h2>
                        
                        <hr class="styled_2">
                        
                        <p>Website: <a href="http://www.osceolaregional.com/" target="_blank">http://www.osceolaregional.com/</a><br>
                           Location: <a href="http://osceolaregional.com/location/osceola-regional-medical-center" target="_blank">700
                              West Oak Street, Kissimmee, FL 34741 </a><br> Phone: 407-846-2266
                        </p>
                        
                        <div>
                           
                           <h3>Welcome</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>We would like to extend a welcome to you and thank you for choosing Osceola Regional
                              Medical Center as
                              your site for clinical applications. The nursing department is committed to making
                              the experience for the
                              nursing students a valuable learning experience. The nurses at Osceola Regional Medical
                              Center take great
                              pride in mentoring nursing students.
                           </p>
                           
                           <p>Some of our most recent program developments are; a six week new graduate program,
                              preceptor program, and
                              internship program, Should you have interest in any of these programs please stop
                              by nursing
                              administration.
                           </p>
                           
                           <p>Congratulations to for choosing the most rewarding careers, and we look forward to
                              working with you
                              during you clinical rotation at Osceola Regional Medical Center.
                           </p>
                           
                           <p>Chief Nursing Officer Osceola Regional Medical Center</p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Forms to be submitted to Osceola Regional</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>All Nursing I students will fill out the paperwork below for Osceola Regional. You
                              must fill out the
                              forms below no later than 30 days before the week of final exams and submit them to
                              youre Nursing I course
                              faculty. 
                           </p>
                           
                           <p> Please note the following regarding these forms:
                              
                           </p>
                           
                           <ul>
                              
                              <li>All students in Nursing I must complete these forms - no exceptions</li>
                              
                              <li>Please review the <a href="/documents/secure/nursing/osceola-form-instructions.pdf" target="_blank">Osceola
                                    Forms Instructions</a> for assistance in filling out the forms listed below.
                                 
                              </li>
                              
                              <li>Complete as the <a href="/documents/secure/nursing/osceola-form-examples.pdf" target="_blank">example
                                    shown</a> - do not add any other signatures
                                 
                              </li>
                              
                              <li>Must include social security number and date of birth â€“ VID#'s are not acceptable</li>
                              
                              <li>Must include a copy of a current drivers license and or state approved ID</li>
                              
                              <li>Please <a href="/documents/secure/nursing/osceola-form-memo.pdf" target="_blank">read this memo</a>
                                 from the Director of Education regarding Osceola Regional forms.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload" aria-label="Turn In"></i> <a href="/documents/secure/nursing/TURN-IN-osceola-form-attestation.pdf" target="_blank">Background
                                    Attestation Form</a></p>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload" aria-label="Turn In"></i> <a href="/documents/secure/nursing/TURN-IN-osceola-form-HIPAA.pdf" target="_blank"> HIPAA Agreement
                                    Form</a></p>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload" aria-label="Turn In"></i> <a href="/documents/secure/nursing/TURN-IN-osceola-form-badge-agreement.pdf" target="_blank">Student/Mentor
                                    Badge Agreement Form</a></p>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Parking</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><a href="/documents/secure/nursing/osceola-parking.pdf" target="_blank">Osceola Regional Student Parking
                                 Map</a></p>
                           
                           <p>Please note that Osceola Regional Medical Center has opened the temporary employee/student
                              parking lot
                              across from the hospital on Rose Avenue. In doing so, we have cleared the path for
                              the construction of our
                              new parking deck which will be completed in early December of this year.
                           </p>
                           
                           <p>We apologize for the inconvenience and ask for your patience during this time. The
                              temporary parking lot
                              has over 200 well marked spaces, is fully fenced and lighted, and contains security
                              camera monitoring
                              24/7.
                           </p>
                           
                           <p>In addition, our security folks will be available to provide escort to your vehicle
                              if needed. Please
                              communicate this to students currently participating at Osceola Regional Medical Center
                              and any future
                              students as well. If you experience any issues during this temporary parking arrangement,
                              please do not
                              hesitate to contact myself or Human Resources directly.
                           </p>
                           
                           <p>Park in staff parking areas that are designated with yellow marked signs. Staff parking
                              is located in the
                              back of the hospital near the Emergency Department. Do not park in any area marked
                              with white signs as
                              this is for visitor parking only. 
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Computer Training Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Osceola Regional Medical Center does require computer training to access client records
                              and for
                              medication administration. Your faculty member will schedule this training when you
                              begin clinical there
                              if you are registered for an Osceola Regional clinical site. ALL Nursing I students
                              will fill out the
                              paperwork below for Osceola Regional. You must fill out the forms below NO LATER THAN
                              30 DAYS THE WEEK OF
                              FINAL EXAMS and submit them to your Nursing I course faculty. 
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical-facility-osceola.pcf">©</a>
      </div>
   </body>
</html>