<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical Facility - Lakeside Alternatives  | Valencia College</title>
      <meta name="Description" content="Clinical Facility - Lakeside Alternatives Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, facilities, lakeside, alternatives, mental, health, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical-facility-lakeside.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical Facility - Lakeside Alternatives </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Lakeside Alternatives</h2>
                        
                        <hr class="styled_2">
                        
                        <p> Phone: 407-875-3700Locations: Central Plaza, <a href="http://maps.google.com/maps?q=434+W.+Kennedy+Blvd.+Orlando,+FL+32810&amp;hl=en" target="_blank">434 W.
                              Kennedy Blvd., Orlando, Fl. 32810</a> <br> Princeton Plaza, <a href="http://maps.google.com/maps?q=1800+Mercy+Dr.+Orlando,+FL+32808&amp;hl=en" target="_blank">1800 Mercy Dr.,
                              Orlando, Fl. 32808</a></p>
                        
                        
                        <hr class="styled_2">
                        
                        <p>Lakeside Alternatives began as Mental Health Services of Orange County, a community-based
                           agency formed
                           when four local providers joined together in 1983. During this period of time, government
                           funding for mental
                           health services had been severely reduced. The goal of the merger was to make services
                           as cost-effective as
                           possible through more efficient use of program funding.
                        </p>
                        
                        <p>Lakeside Alternatives is a private not for profit 501(c) 3 charitable organization
                           that provides
                           comprehensive community behavioral healthcare services to the Orlando community. Lakeside
                           Alternatives has
                           been serving the community for 20 years. In 1991, Mental Health Services of Orange
                           County adopted the name
                           Lakeside Alternatives to reflect a major reorganization that included a new building,
                           new leadership and new
                           strategies to provide the best care at affordable costs. The organization's goal was
                           - and continues to be -
                           to provide the most effective, least restrictive level of care, and to create the
                           most appropriate
                           environment for independence and wellness.
                        </p>
                        
                        
                        <div class="wrapper_indent">
                           
                           <p><i class="far fa-upload fa-spacer"></i> Fill out this form: <a href="/documents/secure/nursing/TURN-IN-lakeside-confidentiality.pdf" target="_blank"> Lakeside
                                 Confidentiality Agreement </a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical-facility-lakeside.pcf">©</a>
      </div>
   </body>
</html>