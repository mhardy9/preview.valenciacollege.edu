<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clinical Facility - Florida Hospital  | Valencia College</title>
      <meta name="Description" content="Clinical Facility - Florida Hospital information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, clinical, facilities, florida, hospital, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/clinical-facility-FL-hospital.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Clinical Facility - Florida Hospital </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Clinical Facility - Florida Hospital</h2>
                        
                        <hr class="styled_2">
                        
                        <p>Website: <a href="https://www.floridahospital.com/" target="_blank">https://www.floridahospital.com/</a><br> Locations: <a href="https://www.floridahospital.com/locations/hospitals" target="_blank">https://www.floridahospital.com/locations/hospitals</a>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Clinical Site Access</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>ID Badge Replacement &amp; Returns</h4>
                           
                           <ul>
                              
                              <li>When a badge is lost, please notify Florida Hospital Academic Liason for approval
                                 of replacement. The
                                 student will be charged a $5 replacement fee.
                                 
                              </li>
                              
                              <li>All student ID badges are to be returned to the Acacdemic Team upon student graduation</li>
                              
                           </ul>
                           
                           <h4>Electronic Healthcare Record Access</h4>
                           
                           <ul>
                              
                              <li>Students/Faculty should be instructed to call FH MIS Help Desk for login issues -
                                 407-303-8000
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Parking</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>
                                 <strong>Altamonte:</strong> Parking Garage and non-reserved areas. Do not park in visitors on South of
                                 building.
                                 
                              </li>
                              
                              <li>
                                 <strong>Apopka:</strong> At the rear of building.
                              </li>
                              
                              <li>
                                 <strong>Celebration:</strong> Any row after the first four rows in front parking lot.
                              </li>
                              
                              <li>
                                 <strong>East:</strong> In the back of the building.
                              </li>
                              
                              <li>
                                 <strong>Kissimmee:</strong> Any non-reserved parking spot
                              </li>
                              
                              <li>
                                 <strong>Orlando:</strong> Alden garage 5th floor &amp; up, McRae, King Streed garages 4th floor &amp;
                                 up. New North King Street garage reserved for employees only.
                                 
                              </li>
                              
                              <li>
                                 <strong>Winter Park:</strong> Park in red log (across the street) &amp; parking garage.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Frequently Asked Questions</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <tr>
                                 
                                 <td>What is the Dress Policy?</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <div class="wrapper_indent">Please review the <a href="/documents/secure/nursing/FL-hospital-image-standards.pdf">Florida Hospital Dress Standards
                                          for details.</a>
                                       
                                    </div>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Do I have to take the Florida Hospital Computer Class?</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <div class="wrapper_indent">You will learn the Florida Hospital computer system on the unit with your
                                       clinical instructor. No class is needed. You should also do the OPTIONAL I-Extend
                                       tutorial and the
                                       online "CERNERWORKS Link to Practice Patient Charting" which are both listed on <a href="computer-training.html">Computer Training for Hospitals</a>.
                                       
                                    </div>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Where do I park?</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <div class="wrapper_indent">Students are to park in designated Student Parking Areas. No student is to
                                       park in an area designated for employees, even if you are a Florida Hospital employee.
                                       See detailed
                                       Parking Information in newsletter above.
                                       
                                    </div>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Do I need an ID badge?</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <div class="wrapper_indent">Yes, Florida hospital does require you to wear Florida Hospital badge at
                                       all times while in the facility as a student nurse. The badging process occurs in
                                       the Nursing I
                                       course for Traditional/Generic students and in Maternal-Child Health for Accelerated
                                       Track in
                                       Nursing (ATN) students. Your Course Leaders will let you know the date of the MANDATORY
                                       Florida
                                       Hospital Orientation. The MANDATORY documents (listed below) are to be completed and
                                       turned into
                                       your course faculty. Photos for the ID Badges will be taken at the Florida Hospital
                                       Orientation and
                                       distributed on the first day of the semester ONLY if you are going to a Florida Hospital
                                       clinical
                                       site.
                                       
                                    </div>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>What if I miss the badging day or have not taken Nursing I or Maternal-Child Health
                                    for some reason?
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <div class="wrapper_indent">Students who do not attend the Florida Hospital Orientation and who need
                                       assistance in obtaining a Florida Hospital ID Badge should follow the steps below
                                       at least ONE MONTH
                                       prior to the first clinical day. Turn-around on making you a badge and obtaining a
                                       computer OPID
                                       number may take up to 3 weeks. Students who do not follow these steps ONE MONTH prior
                                       to clinical
                                       will be denied access to clinical until they complete the items. All missed clinicals
                                       will be made
                                       up on the date and time specified by your Course Leader.
                                       
                                    </div>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </table>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Florida Hospital Orientation</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>These are the mandatory items to complete for Florida Hospital Orientation. All forms
                              are to be turned
                              into ONLY your nursing course faculty as directed.
                           </p>
                           
                           <p><i class="icon_book_alt"></i> Read <a href="http://content.floridahospital.org/cne/Flipbooks/BARE_Facts_2013/FLASH/" target="_blank">BARE
                                 facts</a>.
                           </p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload"></i> Nothing to turn in.
                              </p>
                              
                           </div>
                           
                           <p><i class="icon_book_alt"></i> Read the Clinical Patient Safety Handbook. 
                           </p>
                           
                           <p>This packet details the mandatory requirements that must be completed before you start
                              your rotation and
                              get a Florida Hospital Student ID Badge. Take the Clinical Patient Safety Handbook
                              Post Test. This is an
                              open book test. Please refer to the Quiz Answer Key AFTER you take the quiz and grade
                              your test.
                           </p>
                           
                           <ul>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-clinical-patient-safety-handbook.pdf">Clinical Patient
                                    Safety Handbook</a></li>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-clinical-patient-safety-handbook-post-test.pdf">Clinical
                                    Patient Safety Handbook Post Test</a></li>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-clinical-patient-safety-handbook-answer-key.pdf">Clinical
                                    Patient Safety Handbook Post Quiz Answer Key</a></li>
                              
                           </ul>
                           
                           <p>Turn in ONLY the graded Clinical Patient Safety Handbook Post Quiz Answer Sheet to
                              your nursing course
                              faculty. This is an open book test. Please refer to the Clinical Patient Safety Handbook
                              Post Quiz Answer
                              Key after you take the quiz and grade your quiz. Give only the graded Clinical Patient
                              Safety Handbook
                              Post Quiz Answer Key to your course faculty. 
                           </p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload"></i> Turn in the <a href="/documents/secure/nursing/TURN-IN-FL-hospital-clinical-patient-safety-handbook-post-quiz-answer-sheet.doc">Clinical
                                    Patient Safety Handbook Post Quiz Answer Sheet</a>.
                              </p>
                              
                           </div>
                           
                           <p><i class="icon_book_alt"></i> Read the <a href="/documents/secure/nursing/FL-hospital-staff-orientation.pdf">Non-Florida Hospital Staff Orientation
                                 Packet</a></p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload"></i> Turn in <a href="/documents/secure/nursing/TURN-IN-FL-hospital-orientation-forms.pdf">Florida Hospital Orientation
                                    forms</a>. Turn in the <a href="/documents/secure/nursing/TURN-IN-FL-hospital-physical-mental-health-form.pdf">Student/Faculty
                                    Mental &amp; Physical Requirements form</a>.
                              </p>
                              
                           </div>
                           
                           <p><i class="icon_book_alt"></i> Read <a href="/documents/secure/nursing/FL-hospital-CPOE.pdf" target="_blank">Read the Clinician Electronic Order Entry
                                 Information</a>.
                           </p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload"></i> Nothing to turn in.
                              </p>
                              
                           </div>
                           
                           <p><i class="icon_book_alt"></i> Florida Hospital Barcoding for Medication Administration
                           </p>
                           
                           <p>Below you will find the CBL for the new “FLORIDA HOSPITAL BARCODING FOR MEDICATION
                              ADMINISTRATION”
                              tutorial. The video is about 25 minutes. After you are finished watching the video,
                              please take the IM
                              Safe Bar Code Medication Administration Post-Test and check your answers via the attached
                              answer key.
                           </p>
                           
                           <ul>
                              
                              <li>Florida Hospital Barcoding for Medication Administration - Video  
                                 
                              </li>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-orlando_imsafe-FAQ.pdf">Orlando IM Safe FAQ</a></li>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-imsafe-icons-auto-programing.pdf">IMSafe Icons and Auto
                                    Programing</a></li>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-barcodes-safe-post-test.pdf">BARCODE IM Safe
                                    Post-Test</a></li>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-barcodes-safe-post-test-answers.pdf">BARCODE IM Safe
                                    Post-Test Answer Key</a></li>
                              
                           </ul>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload"></i> Nothing to turn in.
                              </p>
                              
                           </div>
                           
                           <p><i class="icon_book_alt"></i> Falls Prevention Education 
                           </p>
                           
                           <p>Read the <a href="/documents/secure/nursing/FL-hospital-fall-prevention-interventions.pdf">Falls
                                 Prevention Education Information</a> and take the Post-Test.  
                           </p>
                           
                           <p>It was brought to our attention a couple of instances of near events related to falls
                              which involved
                              nursing students. We are not aware of the particular school in which these students
                              attended, however
                              thought this was a good opportunity to communicate some new fall prevention strategies.
                           </p>
                           
                           <p>Some of our campuses have instituted a new fall prevention strategy of the concept
                              of staying “within
                              arm’s reach.” This means when a student (or tech or nurse) takes a patient to the
                              bathroom/commode â€“
                              they must stay “within arm’s reach.” This applies to all patients. Attached is an
                              informational flyer that
                              reviews this requirement as well as other fall prevention strategies.
                           </p>
                           
                           <p>As always, thank you for your partnership to keep our patients safe!</p>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload"></i> Nothing to turn in.
                              </p>
                              
                           </div>
                           
                           <p><i class="icon_book_alt"></i> Read about Bathing Inpatients
                           </p>
                           
                           <ul>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-bathing-chg-foam.pdf">Bathing with CHG Foam</a></li>
                              
                              <li><a href="/documents/secure/nursing/FL-hospital-inpatient-bathing-pericare.pdf">Inpatient Bathing and
                                    Pericare</a></li>
                              
                           </ul>
                           
                           <div class="wrapper_indent">
                              
                              <p><i class="icon_upload"></i> Nothing to turn in.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Computer Training Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You will learn the Florida Hospital computer system on the unit with your clinical
                              instructor. No class
                              is needed. You should also do the OPTIONAL I-Extend tutorial and the online "CERNERWORKS
                              Link to Practice
                              Patient Charting" which are both listed below. 
                           </p>
                           
                           <h4>Overview of iExtend Computer Modules</h4>
                           
                           <p>I-EXTEND MODULES - READ AND REVIEW THE FOLLOWING 3 ITEMS.</p>
                           
                           <ul>
                              
                              <li>
                                 <a href="http://faculty.valenciacollege.edu/avs/iExtend/EnhancementView/iExtend%20Enhancement%20View.htm" target="_blank">iExtend Enhanced View </a>
                                 
                              </li>
                              
                              <li>
                                 <a href="http://faculty.valenciacollege.edu/avs/iExtend/iNet/iExtend%20Enhancement%20INet_IO%20for%20CD.htm" target="_blank">iExtend Enhancement iNet and I&amp;O </a>
                                 
                              </li>
                              
                              <li>
                                 <a href="http://faculty.valenciacollege.edu/avs/iExtend/MedicationReconciliation/iExtend%20Enhancement%20Medication%20Reconciliation%20for%20CD.htm" target="_blank">iExtend Medication Reconciliation </a>
                                 
                              </li>
                              
                              
                           </ul>
                           
                           <h4>Florida Hospital Computer Practice Training Site</h4>
                           
                           <p>There have been a few changes to the Florida Hospital site. You will need a couple
                              of things installed on
                              your PC in order for it to work.
                           </p>
                           
                           <ul>
                              
                              <li>Install Citric Receiver. <a href="https://www.citrix.com/go/receiver.html" target="_blank">Click here
                                    to download.</a>
                                 
                              </li>
                              
                              <li>Install the Entrust 2048 Certificiate. <a href="https://www.entrust.com/root-certificates/entrust_2048_ca.cer" target="_blank">Click here to
                                    download.</a> (this is a direct download and will begin to download the item immediately. Just
                                 open it
                                 after it downloads and follow the prompts). Watch this short video where it is modeled.<br>
                                 
                                 <p>
                                    
                                    
                                 </p>
                                 
                              </li>
                              
                           </ul>
                           
                           <p>After you have installed the two files above, read this PDF file before you begin:
                              Florida Hospital
                              Electronic Health Record Training Update 2016
                           </p>
                           
                           
                           <p><a href="https://home.floridahospitalmd.org/trn2" target="_blank">Log on to iExtend</a></p>
                           
                           <ul>
                              
                              <li>
                                 <strong>Username: </strong>TRD090 this is like an OPID (Username is case sensitive)
                              </li>
                              
                              <li>
                                 <strong>Password:</strong> iExtend1
                              </li>
                              
                              <li>Click on the icon, labeled "<strong>Powerchart TRN2</strong>." Click on "permit use" if it asks. This
                                 part may take 2 or 3 minutes to load. A list of pateints will come up on the screen.
                                 Double click on any
                                 name to open the chart and now you can practice!!!!!!!! The patients are all the same
                                 unless you change
                                 the Username and sign in again.
                                 
                              </li>
                              
                              <li>
                                 <a href="/documents/secure/nursing/fl-hospital-username-OPID.pdf" target="_blank">Click here</a> to
                                 get additional Username OPIDs which will give you different patients to view.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/clinical-facility-FL-hospital.pcf">©</a>
      </div>
   </body>
</html>