<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Options  | Valencia College</title>
      <meta name="Description" content="Career Option Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, career, options, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/career-options.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Career Options </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Career Options</h2>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Helpful Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <li><a href="/ABOUT/life-map/" target="_blank">Valencia's LifeMap</a></li>
                           
                           <li><a href="http://jobs.aol.com/articles/2010/04/30/nursing-salaries/?ncid=webmaildl4" target="_blank">10
                                 Great-Paying Nursing Jobs</a></li>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div>
                           
                           <h3>Undergraduate Degrees in Nursing</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <li><a href="http://www.barry.edu/nursing/bsn/rn/RNtoBSN.htm" target="_blank">Barry University, Miami
                                 Florida - RN to BSN Program</a></li>
                           
                           <li>
                              <a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/post-licensure-rn-bsn-completion/rn-to-bsn/index" target="_blank">University of Central Florida (UCF) - ASN to BSN Program/UCF Valencia Concurrent
                                 Program</a>
                              
                           </li>
                           
                           <li><a href="http://health.usf.edu/nocms/nursing/AdmissionsPrograms/registered_nurses.html" target="_blank">University
                                 of South Florida, Tampa, Florida (USF) - RN to BSN/RN to MSN Programs</a></li>
                           
                           <li><a href="http://www.spcollege.edu/bachelors/nursing.php?program=nursing" target="_blank">St. Petersburg
                                 College, St. Petersburg, Florida - RN to BSN Program</a></li>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Certified Registered Nurse Anesthesist</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <li><a href="http://fana.org/" target="_blank">Florida Assocation of Nurse Anesthesists</a></li>
                           
                           <li><a href="http://www.barry.edu/anesthesiology/nurseAnesthetist.htm" target="_blank">Barry University,
                                 Miami Florida - Certifed Registered Nurse Anesthesist</a></li>
                           
                           <li><a href="http://health.usf.edu/nocms/nursing/AdmissionsPrograms/crna.html" target="_blank">University of
                                 South Florida, Tampa, Florida (USF) - Certifed Registered Nurse Anesthesist</a></li>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Graduate Degrees in Nursing</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <li>Advanced Registered Nursing Practitioner (ARNP)</li>
                           
                           <li>
                              <a href="http://www.graduatecatalog.ucf.edu/programs/Program.aspx?ID=1362&amp;TID=696&amp;track=Adult%20Nurse%20Practitioner" target="_blank">University of Central Florida (UCF) - ARNP</a>
                              
                           </li>
                           
                           <li>Doctorate of Nursing Practice (DNP)</li>
                           
                           <li>
                              <a href="http://www.graduatecatalog.ucf.edu/programs/Program.aspx?ID=1348&amp;program=Nursing%20Practice%20DNP" target="_blank">University of Central Florida (UCF) - DNP</a>
                              
                           </li>
                           
                           <li>Doctor of Philosophy in Nursing (PhD)</li>
                           
                           <li><a href="http://www.graduatecatalog.ucf.edu/programs/Program.aspx?ID=1358&amp;program=Nursing%20PhD" target="_blank">University of Central Florida (UCF) - PhD</a></li>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Certifications in Nursing</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <li><a href="/documents/secure/nursing/certified-nurse-midwife.pdf" target="_blank">Certified Nurse Midwife
                                 (CNM)</a></li>
                           
                           <li><a href="http://www.nln.org/professional-development-programs/Certification-for-Nurse-Educators" target="_blank">Certified Nursing Educator (CNE)</a></li>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/career-options.pcf">©</a>
      </div>
   </body>
</html>