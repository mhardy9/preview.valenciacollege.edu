<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Nursing Registration  | Valencia College</title>
      <meta name="Description" content="Nursing Registration Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, contact, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/medical-exception-withdrawal.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Nursing Registration </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Spring 2017 Registration Information</h2>
                        
                        <hr class="styled_2">
                        
                        <p>NOTE TO STUDENTS: The request regarding the serious illness is made at any time throughout
                           the term prior
                           to taking the Final Examination for the course that the student currently is enrolled.
                        </p>
                        
                        <p>Policy from Student Handbook:</p>
                        
                        <p>Medical Exception Withdrawal Policy</p>
                        
                        <p>Nursing students may repeat a required course for the Nursing Program only one time,
                           whether the grade is
                           D, F or W. A second D, F, W, or combination of any two results in permanent dismissal
                           from the Nursing
                           Program. If the grade is a W, and the reason for the withdrawal is a student's serious
                           illness (physical or
                           psychological) or injury, the student may apply for an exception. The policy may not
                           be applied to friends
                           or family who have a serious illness (physical or psychological) or injury. 
                        </p>
                        
                        <p>The request regarding the serious illness is made at any time throughout the term
                           prior to taking the Final
                           Examination for the course that the student currently is enrolled. If approved, the
                           entire term of the
                           nursing course from which the student is seeking a medical exception would have to
                           be repeated, and no
                           grades from that term would be recorded. The serious illness cannot be applied to
                           the student’s standing in
                           any (future) term.
                        </p>
                        
                        <p>A Withdrawal Medical Exception Request form must be completed and submitted to the
                           Dean of Nursing for
                           consideration by a review panel composed of the Dean of Nursing, Clinical Nursing
                           Program Director and
                           Career Program Advisors (no teaching faulty will be on the panel). Documentation from
                           appropriate
                           physicians, therapists, hospitals, or healthcare professionals must include a statement
                           that the student is
                           medically unable to function as a Nursing student at this time. All information provided
                           by the student will
                           remain private and will be shared only with school officials for legitimate educational
                           reasons, including
                           facilitating the review process. The panel may request further documentation from
                           the student’s healthcare
                           provider(s).
                        </p>
                        
                        <p>Note: If the illness/injury occurs during the final weeks of a term, a grade of Incomplete
                           may still be an
                           option depending on the specific circumstance(s). If the Incomplete is utilized, it
                           cannot become a
                           Withdrawal Medical Exception (W) at any period of time thereafter. 
                        </p>
                        
                        <p>The review panel will meet and make a decision within 10 business days of receipt
                           of the written request
                           and documentation. All decisions will be made using pre-determined criteria and applied
                           equitably to each
                           request. This Medical Exception Withdrawal Policy will only be granted one time per
                           student and is for
                           medical reasons only. The decision of the panel is final. Return to the Nursing Program
                           must be within one
                           year of the withdrawal per the "Limitations of Re-Entry into Nursing Program Policy"
                           found within this
                           Student Handbook.
                        </p>
                        
                        <p>For those students whose request is granted, they must petition to return to the Nursing
                           Program by
                           submitting a "Request for Resequencing and Reinstatement Form." Additionally, you
                           must submit documentation
                           from your treating healthcare provider that you can now fully function as a Nursing
                           student with no
                           limitations. If any accommodations are needed, you will be referred the Office of
                           Students with
                           Disabilities. Resequencing is on a space available basis.
                        </p>
                        
                        <p>It is the student’s responsibility to address any financial issues related to the
                           Withdrawal Medical
                           Exception. The review panel will not be involved with any financial concerns. However,
                           if contacted by
                           financial aid to verify your circumstances, the Nursing Program Dean will cooperate
                           to the extent that
                           confidentiality of the student's records is maintained.
                        </p>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/medical-exception-withdrawal.pcf">©</a>
      </div>
   </body>
</html>