<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Nursing Arts Lab  | Valencia College</title>
      <meta name="Description" content="Nursing Arts Lab Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, lab, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/nursing-arts-lab.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Nursing Arts Lab </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Nursing Arts Lab</h2>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Accutrack Appointments</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>All ACCUTRACK appointments should be cancelled 24 hours prior to the the scheduled
                                 appointment or a
                                 "No Show Referral" will be issued.
                                 
                              </li>
                              
                              <li>Emergency Cancellations: The appropriate phone number to use for cancelling (or notification
                                 of
                                 tardiness) a tutor/learning assistant scheduled appointment is: 407-582-5479. This
                                 number is checked at
                                 the beginning of shift by all tutors.
                                 
                              </li>
                              
                              <li>NOTE: Please do not leave messages on NAL Office phone/email as these messages are
                                 not always relayed
                                 to tutors in a timely manner.
                                 
                              </li>
                              
                              <li><a href="http://www.youtube.com/watch?v=UQr5JrpIrsU" target="_blank">Accutrack Tutorial</a></li>
                              
                              <li><a href="/documents/secure/nursing/accutrack-instructions.pdf" target="_blank">Accutrack
                                    Instructions</a></li>
                              
                              <li><a href="http://nursing.valenciacollege.edu/NAL" target="_blank">Accutrack Login</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Helpful Documents &amp; Videos</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Spring 2017 - West &amp; Osceola Campus Nursing Lab Hours - <a href="/documents/secure/nursing/spring-2017-nursing-lab-hours.pdf">Click here</a>
                                 
                              </li>
                              
                              <li><a href="/documents/secure/nursing/nal-policy-procedure-manual.pdf" target="_blank">Nursing Arts
                                    Policy and Procedure Manual</a></li>
                              
                              <li>Introduction to the NAL - Osceola Campus
                                 
                                 <p>
                                    
                                    
                                 </p>
                                 
                              </li>
                              
                              <li><a href="http://www2c.cdc.gov/podcasts/videowindow.asp?f=9467&amp;af=v" target="_blank">CDC Video on
                                    Handwashing</a></li>
                              
                              
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div>
                           
                           <h3>Medication Resources</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li><a href="http://dailymed.nlm.nih.gov/dailymed/about.cfm" target="_blank">Daily Med - Current
                                    Medication Information</a></li>
                              
                              <li><a href="http://www.ismp.org/" target="_blank">Institute for Safe Medication Practices</a></li>
                              
                              <li><a href="http://medlineplus.gov/" target="_blank">Medline Plus</a></li>
                              
                              <li><a href="http://www.fda.gov/Drugs/DrugSafety/MedicationErrors/ucm164587.htm" target="_blank">Medication
                                    Name Differentiation</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Sample Patient Charts</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              
                              <li><a href="documents/Chart_Bed1_EricThompson.xlsx" target="_blank">Eric Thompson - Bed 1 (Updated
                                    5/115/2014) (Excel)</a></li>
                              
                              <li><a href="documents/Chart_Bed2_IvanaDGree.xlsx" target="_blank">Ivana D'gree - Bed 2 (Updated
                                    5/15/2014) (Excel)</a></li>
                              
                              <li><a href="documents/Chart_Bed3_VictorJenkins.xlsx" target="_blank">Victor Jenkins - Bed 3 (Updated
                                    5/15/2014) (Excel)</a></li>
                              
                              <li><a href="documents/Chart_Bed4_AngelaFarley.xlsx" target="_blank">Angela Farley - Bed 4 (Updated
                                    5/15/2014) (Excel)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/nursing-arts-lab.pcf">©</a>
      </div>
   </body>
</html>