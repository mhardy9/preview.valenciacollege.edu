<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>CPR Certification  | Valencia College</title>
      <meta name="Description" content="CPR Certification Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, compliance, cpr, certification, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/cpr-certification.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>CPR Certification </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>CPR Certification</h2>
                        
                        <hr class="styled_2">
                        
                        <p>For assistance with Compliance Issues, contact <a href="mailto:HSCompliance@valenciacollege.edu">HSCompliance@valenciacollege.edu</a>
                           
                        </p>
                        
                        <p>All students enrolled in programs in Valencia’s Divisions of Nursing or Allied Health
                           shall be required to
                           obtain and maintain the American Heart Association Basic Life Support (BLS) for Health
                           Care Providers
                           certification. For the purpose of Valencia student clinical rotations, ONLY American
                           Heart Association
                           certification is acceptable per our hospital affiliates. For students required by
                           their respective program
                           to have advanced certification (ACLS and/or PALS), only American Heart Association
                           certification is
                           acceptable.
                        </p>
                        
                        <p>Students will upload a copy of their current American Heart Association card to their
                           Certified Background
                           tracker. It is the student’s responsibility to maintain all certifications required
                           by the program in which
                           he/she is enrolled and to present documentation of updated certifications during the
                           duration of their
                           enrollment.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Valencia College CPR Classes</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>American Heart Association Training Site</li>
                              
                              <li>Classes offered in certification and re-certification at least once a month at Valencia's
                                 West Campus.
                                 
                              </li>
                              
                              <li>Fee Includes the student manual</li>
                              
                              <li>You may register online with a credit card. See<a href="http://valenciacollege.edu/west/health/cehealth/BLSforHealthcareProviders.cfm">Continuing Ed for
                                    Healthcare Professions</a> for details &amp; to sign up for classes. Sections for Valencia Degree
                                 Students are identified as such.
                                 
                              </li>
                              
                              <li>You must have your Valencia VID to register in these special sections to obtain student
                                 pricing.
                                 
                                 <ul>
                                    
                                    <li>Certification: $40.00</li>
                                    
                                    <li>Recertification of an unexpired AHA BLS card: $20.00</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>for more informatoin or to pay by check or cash, contact the Office of Continuing
                                 Education &amp;
                                 Clinical Compliance
                                 
                                 <ul>
                                    
                                    <li>Location: West Campus, HSB 200</li>
                                    
                                    <li>Phone: 407-582-1793 or 407-582-1870</li>
                                    
                                    <li>Email: <a href="mailto:cehealthinfo@valenciacollege.edu">cehealthinfo@valenciacollege.edu</a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Other Student-Friendly American Heart Association Providers</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>CPR for Citizens
                                 
                                 <ul>
                                    
                                    <li>Contact: Rick McGarrity</li>
                                    
                                    <li>For schedule, pricing and/or to register for classes, call 407-629-5183</li>
                                    
                                    <li>Provider accepts only cash or check. No Credit Cards accepted.</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>All Care Health Services
                                 
                                 <ul>
                                    
                                    <li>Website: <a href="http://www.allcarecpr.com/student">www.allcarepr.com/student</a>
                                       
                                    </li>
                                    
                                    <li>Phone: 407-432-4756</li>
                                    
                                    <li>Please present current Valencia ID</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <p>To locate other approved American Heart Association providers, go to <a href="http://www.heart.org/HEARTORG/CPRAndECC/CPR_UCM_001118_SubHomePage.jsp">American Heath Assocation CPR
                              Training</a>.
                        </p>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/cpr-certification.pcf">©</a>
      </div>
   </body>
</html>