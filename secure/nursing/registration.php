<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Nursing Registration  | Valencia College</title>
      <meta name="Description" content="Nursing Registration Information for Valencia Nursing Students">
      <meta name="Keywords" content="nursing, secure, contact, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/secure/nursing/registration.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/secure/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Nursing Students</h1>
            <p>
               		Welcome to the Valencia Nursing Program Secure Nursing Website
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/secure/">Secure</a></li>
               <li><a href="/secure/nursing/">Nursing</a></li>
               <li>Nursing Registration </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Summer 2017 Nursing Registration</h2>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>For All Nursing Students</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>To:</strong> All Valencia College Nursing Students<br> <strong>From:</strong> Professor Anita
                              Kovalsky, RN, MNEd, CNE; Nursing Program Clinical Coordinator<br> <strong>Subject: </strong>Valencia
                              College — Summer 2017 Nursing Registration<br> <strong>Date:</strong> Wednesday, February 15, 2017
                           </p>
                           
                           <p>Hello Nursing Students!</p>
                           
                           <p>For Summer 2017 Nursing Registration, students will register based on the dates and
                              times noted below.
                              You will need to know what date and time that students in your course will register
                              for the next course,
                              because courses will be frozen until a particular group registers. This means that
                              even though your ticket
                              says that you can register, you won't be able to do it until the prescribed date and
                              time. Therefore, you
                              must pay close attention to the dates below.
                           </p>
                           
                           <p>Professor Kovalsky will be coming to all Traditional Nursing classes to speak prior
                              to registration.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <p><a href="/documents/secure/nursing/summer-2017-schedule.pdf" class="button-outline" target="_blank">Download
                              Summer 2017 Schedule</a></p>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Important Dates</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>
                                 <strong>Monday, April 3, 2017 at 11:00am</strong><br> Clinical/Class Summer 2017 Nursing Schedule will
                                 be posted to the Secure Nursing Website<br> 4/11/2017: Please note changes for Nursing IV CRN # 30112
                                 and Nursing V CRN#30118
                                 
                              </li>
                              
                              <li>
                                 <strong>Monday, April 3, 2017 at 3:00pm</strong><br> Early Registration Students (qualifying OSD, LAs
                                 and Veterans) will register
                                 
                              </li>
                              
                              <li>
                                 <strong>Tuesday, April 4, 2017 at 3:00pm</strong><br> Early Registration ENDS Nursing Tickets Open
                                 
                              </li>
                              
                              <li>
                                 <strong>Wednesday, April 5, 2017 at 9:00am</strong><br> All nursing student registration tickets are
                                 opened. You should see this date and time via ATLAS under Courses Registration Status.
                                 Please note that
                                 this is NOT your registration date and time. See below for your course registration
                                 date.
                                 
                              </li>
                              
                              <li>
                                 <strong>Wednesday, April 5, 2017 at 3:00pm</strong><br> Students going from Nursing I to Nursing II
                                 will register
                                 
                              </li>
                              
                              <li>
                                 <strong>Thursday, April 6, 2017 at 3:00pm:</strong><br> Students going from Nursing II to Nursing III
                                 will register<br> Currently enrolled AVS students will register
                                 
                              </li>
                              
                              <li>
                                 <strong>Friday, April 7, 2017 at 3:00pm:</strong><br> Students going from Nursing III to Nursing IV
                                 will register<br> Students going from Nursing IV to Nursing V, Nursing IV and HSC 2151 will register
                                 
                              </li>
                              
                              <li>
                                 <strong>Monday, April 10, 2017 at 3:00pm</strong><br> INCOMING Traditional Nursing I will register<br>
                                 AVS Transition Nursing and AVS Maternal-Child Health students will register
                                 
                              </li>
                              
                              <li>
                                 <strong>Friday, April 14, 2017 at 3:00pm:</strong><br> All Nursing Course CRNs Frozen
                                 
                              </li>
                              
                              <li>
                                 <strong>Friday, April 21, 2017 by 5:00pm:</strong><br> Payment Deadline
                                 
                              </li>
                              
                              <li>
                                 <strong>Monday, May 15, 2017:</strong><br> Drop/Refund Deadline (Drop classes with 100 % Refund)
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Special Note</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you do not pay by the payment deadline for Summer 2017 you will be charged a late
                              registration fee.
                              Once you are re-registered the payment will be due that same day. Students who are
                              dropped for non-payment
                              or who deregister themselves will be placed back into the same CRN in which they were
                              previously
                              enrolled.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Resequencers</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>For students who are repeating the course in which they are CURRENTLY enrolled:</strong></p>
                           
                           <p>If you are repeating the course that you are CURRENTLY enrolled in, please send an
                              email from your ATLAS
                              account to Ms. Kate Martin, Coordinator, Career Program Advisor (kmartin33@valenciacollege.edu
                              and also CC
                              Ms. Kovalsky akovalsky@valenciacollege.edu) by Friday, March 24, 2017. Include your
                              name, VID, and the
                              course that you are currently repeating.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Students Out of Progression</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Registration for students (Students Out of Progression) or those who think they may
                              not pass the course
                              they are currently enrolled in: Please do not register yourself, or the course leader
                              will remove you from
                              the course. Once we have received and reviewed your Re-Sequencing paperwork you will
                              be registered by your
                              instructor.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Switching CRNS</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>PLEASE NOTE THAT THERE ARE NO "WAITING LISTS" OR OPPORTUNITIES TO SWITCH CRNs ONCE
                                 THE COURSES
                                 ARE FROZEN (Departmental Holds placed).</strong></p>
                           
                           <p>Remember that the only switching that we allow is prior to finals. Students may switch
                              with each other up
                              until Friday, April 14, at 3:00pm. You can't take the place of a student who is unsuccessful,
                              as we need
                              to have those slots available for students repeating that course. Any future changes
                              will be addressed by
                              course faculty on the first day of class in May, however, there is no guarantee that
                              you will get the
                              clinical group that you desire.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Combining or Collapsing CRN Clinical Groups</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Due to hospital restrictions, clinical groups may only have 8 students. Therefore,
                              we sometimes have to
                              remove the last student(s) registered for some groups so that each clinical group
                              has only 8 students. We
                              will make every effort to not remove Early Registration Students from clinical groups.
                              Also, due to
                              changes in the numbers of students moving forward, some groups may have to be collapsed
                              (deleted).
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>If you are unsuccessful in your current Nursing course and are requesting to resequence:</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you are requesting to resequence, go to the Secure Nursing Website and click on
                              the "Program
                              Progressionx tab on the left-side of the Home Page. This contains the Resequencing
                              Requirements and
                              Procedure, as well as Resequencing and Reinstatement Form. Students requesting to
                              resequence should NOT
                              email Ms. Martin.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Questions About Registration</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you are having registration issues, please e-mail Ms. Kate Martin FROM YOUR ATLAS
                              ACCOUNT. Include
                              your VID# and the CRN for which you are trying to register, as well as the exact nature
                              of your issue.
                              Please do not call Ms. Martin on the phone. She will answer e-mails in the order in
                              which they were
                              received. <a href="mailto:kmartin33@valenciacollege.edu">kmartin33@valenciacollege.edu</a> (also CC Ms.
                              Kovalsky: <a href="mailto:akovalsky@valenciacollege.edu">akovalsky@valenciacollege.edu</a>).
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/secure/nursing/registration.pcf">©</a>
      </div>
   </body>
</html>