<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Florida Respiratory Care Practitioners Team Places 1st  | Valencia College</title>
      <meta name="Description" content="The Florida Respiratory Care Practitioner team placed 1st at the AARC Congress in San Antonio, Texas this past October. "><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.min.css">
      <link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.validation.min.css">
      <meta property="og:title" content="Florida Respiratory Care Practitioners Team Places 1st ">
      <meta property="og:description" content="The Florida Respiratory Care Practitioner team placed 1st at the AARC Congress in San Antonio, Texas this past October. ">
      <meta property="og:image" content="https://preview.valenciacollege.edu/blog/allied-health/posts/img/Respriatory-Care-Team-1.jpg">
      <meta property="og:url" content="https://preview.valenciacollege.edu/blog/allied-health/posts/respiratory-care-practicioners-place-1st.php">
      <meta name="twitter:title" content="Florida Respiratory Care Practitioners Team Places 1st ">
      <meta name="twitter:description" content="The Florida Respiratory Care Practitioner team placed 1st at the AARC Congress in San Antonio, Texas this past October. ">
      <meta name="twitter:image" content="https://preview.valenciacollege.edu/blog/allied-health/posts/img/Respriatory-Care-Team-1.jpg">
      <meta name="twitter:card" content="summary">
      <meta name="author" content="Allied Health">
      <link rel="stylesheet" type="text/css" href="/_resources/css/blog/blog.css"><?php require_once($_SERVER["DOCUMENT_ROOT"] . "/_resources/php/blog/listing.php");
			?>
      <link href="/_resources/css/blog.css" rel="stylesheet"><script>
					var page_url="https://preview.valenciacollege.edu/blog/allied-health/posts/respiratory-care-practicioners-place-1st.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/blog/allied-health/site-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_1">
         <div id="intro_txt">
            <h1>Florida Respiratory Care Practitioners Team Places 1st </h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/blog/">Blog</a></li>
               <li><a href="/blog/allied-health/">Allied Health</a></li>
               <li>Florida Respiratory Care Practitioners Team Places 1st </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-9">
                  <div class="post"><img src="/blog/allied-health/posts/img/Respriatory-Care-Team-1.jpg" alt="Respiratory Care Team poses before the competition" class="img-responsive"><div class="post_info clearfix">
                        <div class="post-left">
                           <ul>
                              <li><i class="icon-calendar-empty"></i>11/1/2017 <em>by Allied Health</em></li>
                              <li><i class="icon-inbox-alt"></i><a href="../?category=Events">Events</a></li>
                              <li><i class="icon-tags"></i></li>
                           </ul>
                        </div>
                     </div>
                     <h2>Florida Respiratory Care Practitioners Team Places 1st </h2>
                     <p>The Florida Respiratory Care Practitioner team placed 1st at the AARC Congress in
                        San Antonio, Texas this past October. This is the first time that Florida has ever
                        won this nationally recognized competition. Special congratulations to James Deckman
                        (Respiratory Care Supervisor at Morton Plant Hospital), Gina Ricard (Program Director
                        for Respiratory Care at Hillsborough Community College), Sharon Shenton (Program Chair
                        for the BS Program in Cardiopulmonary Sciences at Valencia College) and Bob Lamme
                        (Seminar Faculty for Kettering National Seminars).
                     </p>
                     
                     <p><img class="" src="/blog/allied-health/posts/img/respiratory-care-team-2.jpg" alt="Respiratory Care Team" width="314" height="164"></p>
                     
                     <p>(L-R) James Deckman, Gina Ricard, Sharon Shenton, Bob Lamme</p>
                  </div>
                  <div id="disqus_thread"></div><script type="text/javascript">
	/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
	var disqus_shortname = 'valenciacollege-edu'; // required: replace example with your forum shortname

	/* * * DON'T EDIT BELOW THIS LINE * * */
	(function() {
		var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
	})();
</script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
               </div>
               <aside class="col-md-3" id="sidebar"><?php $conditions = array();
					$conditions["title"] = 'Categories';
					if("available-categories" == "popular-tags") {display_popular_tags("/blog/", 10, $conditions);}
					else if("available-categories" == "available-tags") {display_available_tags("/blog/", 10, $conditions);}
					else if("available-categories" == "available-categories") {display_available_categories("/blog/", 10, $conditions);}
					else if("available-categories" == "recent-posts") {display_recent_posts("/blog/", 10, $conditions);}
					else {
						$conditions["filtering"] = "strict";
						if("" != '') {$conditions["year"] = "";}
						if("available-categories" == "page-related") {$conditions["tags"] = "";}
						elseif("available-categories" == "tag-related") {$conditions["tags"] = "";}
						elseif("available-categories" == "featured") {$conditions["featured"] = "true";}
						display_asset_listing(get_all_post_files("/blog/", $conditions), 10, $conditions);
					}
					?>
                  <hr>
                  	
                  		<?php $conditions = array();
					$conditions["title"] = 'Recent Post';
					if("recent-posts" == "popular-tags") {display_popular_tags("/blog/", 3, $conditions);}
					else if("recent-posts" == "available-tags") {display_available_tags("/blog/", 3, $conditions);}
					else if("recent-posts" == "available-categories") {display_available_categories("/blog/", 3, $conditions);}
					else if("recent-posts" == "recent-posts") {display_recent_posts("/blog/", 3, $conditions);}
					else {
						$conditions["filtering"] = "strict";
						if("" != '') {$conditions["year"] = "";}
						if("recent-posts" == "page-related") {$conditions["tags"] = "";}
						elseif("recent-posts" == "tag-related") {$conditions["tags"] = "";}
						elseif("recent-posts" == "featured") {$conditions["featured"] = "true";}
						display_asset_listing(get_all_post_files("/blog/", $conditions), 3, $conditions);
					}
					?>
                  	
                  <hr><?php $conditions = array();
					$conditions["title"] = 'Tags';
					if("popular-tags" == "popular-tags") {display_popular_tags("/blog/", 10, $conditions);}
					else if("popular-tags" == "available-tags") {display_available_tags("/blog/", 10, $conditions);}
					else if("popular-tags" == "available-categories") {display_available_categories("/blog/", 10, $conditions);}
					else if("popular-tags" == "recent-posts") {display_recent_posts("/blog/", 10, $conditions);}
					else {
						$conditions["filtering"] = "strict";
						if("" != '') {$conditions["year"] = "";}
						if("popular-tags" == "page-related") {$conditions["tags"] = "";}
						elseif("popular-tags" == "tag-related") {$conditions["tags"] = "";}
						elseif("popular-tags" == "featured") {$conditions["featured"] = "true";}
						display_asset_listing(get_all_post_files("/blog/", $conditions), 10, $conditions);
					}
					?>
               </aside>
            </div>
         </div>
      </div>
      <div class="ou-form container_gray_bg" id="newsletter_container">
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-8 col-md-offset-2 text-center">
                  <h3>Subscribe to our Newsletter for latest news.</h3>
                  <div id="message-newsletter">
                     <div id="status_74643119-339c-4781-be67-88d76c494736"></div>
                  </div>
                  <form id="form_74643119-339c-4781-be67-88d76c494736" name="newsletter" method="post" class="form-inline" autocomplete="off"><span class="hp74643119-339c-4781-be67-88d76c494736" style="display:none; margin-left:-1000px;"><label for="hp74643119-339c-4781-be67-88d76c494736" class="hp74643119-339c-4781-be67-88d76c494736">If you see this don't fill out this input box.</label><input type="text" id="hp74643119-339c-4781-be67-88d76c494736"></span><input type="hidden" name="form_uuid" value="74643119-339c-4781-be67-88d76c494736"><input type="hidden" name="site_name" value="www"><input type="hidden" name="pageurl" value="https://preview.valenciacollege.edu/blog/allied-health/posts/respiratory-care-practicioners-place-1st.php"><input name="email" id="email" type="email" value="" placeholder="Your Email" class="form-control"><button type="submit" id="btn_74643119-339c-4781-be67-88d76c494736" class="button"> Subscribe</button></form>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><script type="text/javascript" src="/_resources/ldp/forms/js/ou-forms.js"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/blog/allied-health/posts/respiratory-care-practicioners-place-1st.pcf">©</a>
      </div>
   </body>
</html>