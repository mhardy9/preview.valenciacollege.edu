<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blog | Valencia College</title>
      <meta name="Description" content="OU Blogs"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.min.css">
      <link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.validation.min.css"><?php require_once($_SERVER["DOCUMENT_ROOT"] . "/_resources/php/blog/listing.php");
 				$tmp = array();
				$tmp["tags"] = (isset($_GET["tags"])) ? htmlspecialchars($_GET["tags"]) : "";
				$tmp["author"] = (isset($_GET["author"])) ? htmlspecialchars($_GET["author"]) : "";
				$tmp["year"] = (isset($_GET["year"])) ? htmlspecialchars($_GET["year"]) : "";
				$tmp["month"] = (isset($_GET["month"])) ? htmlspecialchars($_GET["month"]) : "";
				$tmp["category"] = (isset($_GET["category"])) ? htmlspecialchars($_GET["category"]) : "";
				$tmp["filtering"] = "loose";
				$posts = get_all_post_files("/blog", $tmp);
			?>
      <link href="/_resources/css/blog.css" rel="stylesheet"><script>
					var page_url="https://preview.valenciacollege.edu/blog/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_1">
         <div id="intro_txt">
            <h1>Valencia Blog page</h1>
            <p>Ex saepe accusata duo, vel ne summo option delenit.</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li>Blog</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-9">
                  <div id="blog-posts"><?php display_listing($posts,1,3);
						?></div>
               </div>
               <aside class="col-md-3" id="sidebar">
                  <div class="widget">
                     	
                     		<?php $conditions = array();
					$conditions["title"] = 'Categories';
					if("available-categories" == "popular-tags") {display_popular_tags("/blog/", 10, $conditions);}
					else if("available-categories" == "available-tags") {display_available_tags("/blog/", 10, $conditions);}
					else if("available-categories" == "available-categories") {display_available_categories("/blog/", 10, $conditions);}
					else if("available-categories" == "recent-posts") {display_recent_posts("/blog/", 10, $conditions);}
					else {
						$conditions["filtering"] = "strict";
						if("" != '') {$conditions["year"] = "";}
						if("available-categories" == "page-related") {$conditions["tags"] = "";}
						elseif("available-categories" == "tag-related") {$conditions["tags"] = "";}
						elseif("available-categories" == "featured") {$conditions["featured"] = "true";}
						display_asset_listing(get_all_post_files("/blog/", $conditions), 10, $conditions);
					}
					?>
                     	
                  </div>
                  <hr>
                  <div class="widget">
                     	
                     		<?php $conditions = array();
					$conditions["title"] = 'Recent Post';
					if("recent-posts" == "popular-tags") {display_popular_tags("/blog/", 3, $conditions);}
					else if("recent-posts" == "available-tags") {display_available_tags("/blog/", 3, $conditions);}
					else if("recent-posts" == "available-categories") {display_available_categories("/blog/", 3, $conditions);}
					else if("recent-posts" == "recent-posts") {display_recent_posts("/blog/", 3, $conditions);}
					else {
						$conditions["filtering"] = "strict";
						if("" != '') {$conditions["year"] = "";}
						if("recent-posts" == "page-related") {$conditions["tags"] = "";}
						elseif("recent-posts" == "tag-related") {$conditions["tags"] = "";}
						elseif("recent-posts" == "featured") {$conditions["featured"] = "true";}
						display_asset_listing(get_all_post_files("/blog/", $conditions), 3, $conditions);
					}
					?>
                     	
                  </div>
                  <hr>
                  <div class="widget tags"><?php $conditions = array();
					$conditions["title"] = 'Tags';
					if("popular-tags" == "popular-tags") {display_popular_tags("/blog/", 10, $conditions);}
					else if("popular-tags" == "available-tags") {display_available_tags("/blog/", 10, $conditions);}
					else if("popular-tags" == "available-categories") {display_available_categories("/blog/", 10, $conditions);}
					else if("popular-tags" == "recent-posts") {display_recent_posts("/blog/", 10, $conditions);}
					else {
						$conditions["filtering"] = "strict";
						if("" != '') {$conditions["year"] = "";}
						if("popular-tags" == "page-related") {$conditions["tags"] = "";}
						elseif("popular-tags" == "tag-related") {$conditions["tags"] = "";}
						elseif("popular-tags" == "featured") {$conditions["featured"] = "true";}
						display_asset_listing(get_all_post_files("/blog/", $conditions), 10, $conditions);
					}
					?></div>
               </aside>
            </div>
         </div>
      </div>
      <div class="ou-form container_gray_bg" id="newsletter_container">
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-8 col-md-offset-2 text-center">
                  <h3>Subscribe to our Newsletter for latest news.</h3>
                  <div id="message-newsletter">
                     <div id="status_74643119-339c-4781-be67-88d76c494736"></div>
                  </div>
                  <form id="form_74643119-339c-4781-be67-88d76c494736" name="newsletter" method="post" class="form-inline" autocomplete="off"><span class="hp74643119-339c-4781-be67-88d76c494736" style="display:none; margin-left:-1000px;"><label for="hp74643119-339c-4781-be67-88d76c494736" class="hp74643119-339c-4781-be67-88d76c494736">If you see this don't fill out this input box.</label><input type="text" id="hp74643119-339c-4781-be67-88d76c494736"></span><input type="hidden" name="form_uuid" value="74643119-339c-4781-be67-88d76c494736"><input type="hidden" name="site_name" value="www"><input type="hidden" name="pageurl" value="https://preview.valenciacollege.edu/blog/index.php"><input name="email" id="email" type="email" value="" placeholder="Your Email" class="form-control"><button type="submit" id="btn_74643119-339c-4781-be67-88d76c494736" class="button"> Subscribe</button></form>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><script type="text/javascript" src="/_resources/ldp/forms/js/ou-forms.js"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/blog/index.pcf">©</a>
      </div><script type="text/javascript">
	/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
	var disqus_shortname = 'valenciacollege-edu'; // required: replace example with your forum shortname

	/* * * DON'T EDIT BELOW THIS LINE * * */
	(function () {
		var s = document.createElement('script'); s.async = true;
		s.type = 'text/javascript';
		s.src = '//' + disqus_shortname + '.disqus.com/count.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
	}());
</script></body>
</html>