<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Sitemap | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/sitemap.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sitemap</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li>Sitemap</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <table class="table table">
                     			
                     <thead>
                        				
                        <tr>
                           					
                           <th scope="col">Current Site</th>
                           					
                           <th scope="col">Preview Site</th>
                           				
                        </tr>
                        			
                     </thead>
                     			
                     <tbody>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/banner">https://valenciacollege.edu/banner</a></td>
                           					
                           <td><a href="https://mailvalenciacc.sharepoint.com/banner/pages/oprod.aspx">https://mailvalenciacc.sharepoint.com/banner/pages/oprod.aspx</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a>https://preview.valenciacollege.edu</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/about/">https://preview.valenciacollege.edu/about/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/AboutUs/">https://valenciacollege.edu/AboutUs/</a></td>
                           					
                           <td><a href="/about/about-valencia/">https://preview.valenciacollege.edu/about/about-valencia/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/audit/">https://valenciacollege.edu/audit/</a></td>
                           					
                           <td><a href="/about/audit/">https://preview.valenciacollege.edu/about/audit/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/budget/">https://valenciacollege.edu/budget/</a></td>
                           					
                           <td><a href="/about/budget/">https://preview.valenciacollege.edu/about/budget/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/divisionplans/">https://valenciacollege.edu/divisionplans/</a></td>
                           					
                           <td><a href="/about/division-plans">https://preview.valenciacollege.edu/about/division-plans</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/facilities/">https://valenciacollege.edu/facilities/</a></td>
                           					
                           <td><a href="/about/facilities/">https://preview.valenciacollege.edu/about/facilities/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/ferpa/">https://valenciacollege.edu/ferpa/</a></td>
                           					
                           <td><a href="/about/ferpa/">https://preview.valenciacollege.edu/about/ferpa/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/generalcounsel/">https://valenciacollege.edu/generalcounsel/</a></td>
                           					
                           <td><a href="/about/general-counsel/">https://preview.valenciacollege.edu/about/general-counsel/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/governance/">https://valenciacollege.edu/governance/</a></td>
                           					
                           <td><a href="/about/governance/">https://preview.valenciacollege.edu/about/governance/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/olt">https://valenciacollege.edu/olt</a></td>
                           					
                           <td><a href="/about/governance/operations-leadership-team/">https://preview.valenciacollege.edu/about/governance/operations-leadership-team/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/lci/">https://valenciacollege.edu/lci/</a></td>
                           					
                           <td><a href="/about/learning-centered-initiative/">https://preview.valenciacollege.edu/about/learning-centered-initiative/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/lifemap/">https://valenciacollege.edu/lifemap/</a></td>
                           					
                           <td><a href="/about/lifemap/">https://preview.valenciacollege.edu/about/lifemap/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/live">https://valenciacollege.edu/live</a></td>
                           					
                           <td><a href="/about/live/">https://preview.valenciacollege.edu/about/live/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/ournextbigidea/">https://valenciacollege.edu/ournextbigidea/</a></td>
                           					
                           <td><a href="/about/our-next-big-idea/">https://preview.valenciacollege.edu/about/our-next-big-idea/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/privacy">https://valenciacollege.edu/privacy</a></td>
                           					
                           <td><a href="/about/privacy/">https://preview.valenciacollege.edu/about/privacy/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/public-records">https://valenciacollege.edu/public-records</a></td>
                           					
                           <td><a href="/about/public-records.php">https://preview.valenciacollege.edu/about/public-records.php</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/contact">https://valenciacollege.edu/contact</a></td>
                           					
                           <td><a href="/about/support/">https://preview.valenciacollege.edu/about/support/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/support">https://valenciacollege.edu/support</a></td>
                           					
                           <td><a href="/about/support/">https://preview.valenciacollege.edu/about/support/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/sustainability">https://valenciacollege.edu/sustainability</a></td>
                           					
                           <td><a href="/about/sustainability/">https://preview.valenciacollege.edu/about/sustainability/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/trustee-education">https://valenciacollege.edu/trustee-education</a></td>
                           					
                           <td><a href="/about/trustee-education/">https://preview.valenciacollege.edu/about/trustee-education/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/academics/">https://preview.valenciacollege.edu/academics/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/academic-affairs">https://valenciacollege.edu/academic-affairs</a></td>
                           					
                           <td><a href="/academics/academic-affairs/">https://preview.valenciacollege.edu/academics/academic-affairs/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/academic-affairs/career-workforce-education/">https://valenciacollege.edu/academic-affairs/career-workforce-education/</a></td>
                           					
                           <td><a href="/academics/academic-affairs/career-workforce-education/">https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/">https://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/</a></td>
                           					
                           <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/">https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment">https://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment</a></td>
                           					
                           <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan">https://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan</a></td>
                           					
                           <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/academic-affairs/new-student-experience/">https://valenciacollege.edu/academic-affairs/new-student-experience/</a></td>
                           					
                           <td><a href="/academics/academic-affairs/new-student-experience/">https://preview.valenciacollege.edu/academics/academic-affairs/new-student-experience/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/academic-affairs/resource-development">https://valenciacollege.edu/academic-affairs/resource-development</a></td>
                           					
                           <td><a href="/academics/academic-affairs/resource-development/">https://preview.valenciacollege.edu/academics/academic-affairs/resource-development/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/calendar">https://valenciacollege.edu/calendar</a></td>
                           					
                           <td><a href="/academics/calendar">https://preview.valenciacollege.edu/academics/calendar</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/camino-a-ucf">https://valenciacollege.edu/camino-a-ucf</a></td>
                           					
                           <td><a href="/academics/camino-a-ucf/">https://preview.valenciacollege.edu/academics/camino-a-ucf/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/competencies">https://valenciacollege.edu/competencies</a></td>
                           					
                           <td><a href="/academics/competencies/">https://preview.valenciacollege.edu/academics/competencies/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/courses">https://valenciacollege.edu/courses</a></td>
                           					
                           <td><a href="/academics/courses/">https://preview.valenciacollege.edu/academics/courses/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/deanslist">https://valenciacollege.edu/students/deanslist</a></td>
                           					
                           <td><a href="/academics/deans-list/">https://preview.valenciacollege.edu/academics/deans-list/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/departments">https://valenciacollege.edu/departments</a></td>
                           					
                           <td><a href="/academics/departments/">https://preview.valenciacollege.edu/academics/departments/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment">https://valenciacollege.edu/artsandentertainment</a></td>
                           					
                           <td><a href="/academics/departments/arts-entertainment/">https://preview.valenciacollege.edu/academics/departments/arts-entertainment/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/arts-and-humanities">https://valenciacollege.edu/west/arts-and-humanities</a></td>
                           					
                           <td><a href="/academics/departments/arts-humanities-west/">https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/behavioral-and-social-sciences">https://valenciacollege.edu/west/behavioral-and-social-sciences</a></td>
                           					
                           <td><a href="/academics/departments/behavioral-social-sciences/">https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/departments/east/business">https://valenciacollege.edu/departments/east/business</a></td>
                           					
                           <td><a href="/academics/departments/business-east/">https://preview.valenciacollege.edu/academics/departments/business-east/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/business-and-hospitality">https://valenciacollege.edu/west/business-and-hospitality</a></td>
                           					
                           <td><a href="/academics/departments/business-hospitality/">https://preview.valenciacollege.edu/academics/departments/business-hospitality/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/communications">https://valenciacollege.edu/east/communications</a></td>
                           					
                           <td><a href="/academics/departments/communications-east/">https://preview.valenciacollege.edu/academics/departments/communications-east/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/departments/west/communications">https://valenciacollege.edu/departments/west/communications</a></td>
                           					
                           <td><a href="/academics/departments/communications-west/">https://preview.valenciacollege.edu/academics/departments/communications-west/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/communications">https://valenciacollege.edu/west/communications</a></td>
                           					
                           <td><a href="/academics/departments/communications-west/">https://preview.valenciacollege.edu/academics/departments/communications-west/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/engineering">https://valenciacollege.edu/west/engineering</a></td>
                           					
                           <td><a href="/academics/departments/engineering/">https://preview.valenciacollege.edu/academics/departments/engineering/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/departments/west/health">https://valenciacollege.edu/departments/west/health</a></td>
                           					
                           <td><a href="/academics/departments/health-sciences">https://preview.valenciacollege.edu/academics/departments/health-sciences</a><a href="/academics/programs/health-sciences">https://preview.valenciacollege.edu/academics/programs/health-sciences/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/nursing">https://valenciacollege.edu/west/nursing</a></td>
                           					
                           <td><a href="/academics/departments/health-sciences/nursing/">https://preview.valenciacollege.edu/academics/departments/health-sciences/nursing/<br></a><a href="/academics/programs/health-sciences/nursing">https://preview.valenciacollege.edu/academics/programs/health-sciences/nursing</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/humanities">https://valenciacollege.edu/east/humanities</a></td>
                           					
                           <td><a href="/academics/departments/humanities/">https://preview.valenciacollege.edu/academics/departments/humanities/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/math/">https://valenciacollege.edu/osceola/math/</a></td>
                           					
                           <td><a href="/academics/departments/math-osceola/">https://preview.valenciacollege.edu/academics/departments/math-osceola/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/science">https://valenciacollege.edu/east/science</a></td>
                           					
                           <td><a href="/academics/departments/science/">https://preview.valenciacollege.edu/academics/departments/science/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/departments/west/science">https://valenciacollege.edu/departments/west/science</a></td>
                           					
                           <td><a href="/academics/departments/science-west/">https://preview.valenciacollege.edu/academics/departments/science-west/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/socialsciences">https://valenciacollege.edu/east/socialsciences</a></td>
                           					
                           <td><a href="/academics/departments/social-sciences/">https://preview.valenciacollege.edu/academics/departments/social-sciences/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/fitness">https://valenciacollege.edu/west/fitness</a></td>
                           					
                           <td><a href="/academics/departments/social-sciences/fitness/">https://preview.valenciacollege.edu/academics/departments/social-sciences/fitness/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/eap">https://valenciacollege.edu/eap</a></td>
                           					
                           <td><a href="/academics/english-for-academic-purposes/">https://preview.valenciacollege.edu/academics/english-for-academic-purposes/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/hotclasses">https://valenciacollege.edu/hotclasses</a></td>
                           					
                           <td><a href="/academics/hot-classes/">https://preview.valenciacollege.edu/academics/hot-classes/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/linc">https://valenciacollege.edu/linc</a></td>
                           					
                           <td><a href="/academics/learning-in-community/">https://preview.valenciacollege.edu/academics/learning-in-community/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/academics/programs/">https://preview.valenciacollege.edu/academics/programs/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/aadegrees">https://valenciacollege.edu/aadegrees</a></td>
                           					
                           <td><a href="/academics/programs/aa-degree/">https://preview.valenciacollege.edu/academics/programs/aa-degree/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/">https://valenciacollege.edu/artsandentertainment/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/artstudio/">https://valenciacollege.edu/artsandentertainment/artstudio/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/art-studio-fine-art/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/art-studio-fine-art/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/camps/">https://valenciacollege.edu/artsandentertainment/camps/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/camps.php">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/camps.php</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/dance/">https://valenciacollege.edu/artsandentertainment/dance/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/dance-performance/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/digitalmedia/">https://valenciacollege.edu/artsandentertainment/digitalmedia/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/digital-media-technology/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/digital-media-technology/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/entertainmentdesignandtech/">https://valenciacollege.edu/artsandentertainment/entertainmentdesignandtech/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/entertainment-design-technology/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/entertainment-design-technology/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/filmtech/">https://valenciacollege.edu/artsandentertainment/filmtech/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/film-production-technology/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/film-production-technology/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/graphicstech/">https://valenciacollege.edu/artsandentertainment/graphicstech/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/graphic-interactive-design/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/graphic-interactive-design/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/music/">https://valenciacollege.edu/artsandentertainment/music/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/music-performance/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/music-performance/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/musical-theatre/">https://valenciacollege.edu/artsandentertainment/musical-theatre/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/musical-theatre/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/musical-theatre/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/musicandsoundtech/">https://valenciacollege.edu/artsandentertainment/musicandsoundtech/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/sound-music-technology/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/sound-music-technology/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/performingarts/">https://valenciacollege.edu/artsandentertainment/performingarts/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/performing-arts/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/performing-arts/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/artsandentertainment/theater/">https://valenciacollege.edu/artsandentertainment/theater/</a></td>
                           					
                           <td><a href="/academics/programs/arts-entertainment/theater/">https://preview.valenciacollege.edu/academics/programs/arts-entertainment/theater/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/asdegrees">https://valenciacollege.edu/asdegrees</a></td>
                           					
                           <td><a href="/academics/programs/as-degree/">https://preview.valenciacollege.edu/academics/programs/as-degree/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/health">https://valenciacollege.edu/west/health</a></td>
                           					
                           <td>
                              						
                              <p><a href="/academics/programs/health-sciences/">https://preview.valenciacollege.edu/academics/programs/health-sciences/</a><br><a href="/academics/departments/health-sciences/">https://preview.valenciacollege.edu/academics/departments/health-sciences/</a></p>
                              					
                           </td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/health/pre-professional-health">https://valenciacollege.edu/health/pre-professional-health</a></td>
                           					
                           <td><a href="/academics/programs/health-sciences/pre-professional-help">https://preview.valenciacollege.edu/academics/programs/health-sciences/pre-professional-help</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/honors">https://valenciacollege.edu/honors</a></td>
                           					
                           <td><a href="/academics/programs/honors/">https://preview.valenciacollege.edu/academics/programs/honors/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/education">https://valenciacollege.edu/education</a></td>
                           					
                           <td><a href="/academics/programs/teacher-education-program/">https://preview.valenciacollege.edu/academics/programs/teacher-education-program/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/lsamp">https://valenciacollege.edu/lsamp</a></td>
                           					
                           <td><a href="/academics/programs-cf-stem-alliance">https://preview.valenciacollege.edu/academics/programs-cf-stem-alliance</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/supplemental-learning">https://valenciacollege.edu/supplemental-learning</a></td>
                           					
                           <td><a href="/academics/supplemental-learning/">https://preview.valenciacollege.edu/academics/supplemental-learning/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/admissions/">https://preview.valenciacollege.edu/admissions/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/admissions-records">https://valenciacollege.edu/admissions-records</a></td>
                           					
                           <td><a href="/admissions/admissions-records/">https://preview.valenciacollege.edu/admissions/admissions-records/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/dual">https://valenciacollege.edu/dual</a></td>
                           					
                           <td><a href="/admissions/dual-enrollment/">https://preview.valenciacollege.edu/admissions/dual-enrollment/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/finaid">https://valenciacollege.edu/finaid</a></td>
                           					
                           <td><a href="/admissions/finaid/">https://preview.valenciacollege.edu/admissions/finaid/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/orientation">https://valenciacollege.edu/orientation</a></td>
                           					
                           <td><a href="/admissions/orientation/">https://preview.valenciacollege.edu/admissions/orientation/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/refund">https://valenciacollege.edu/refund</a></td>
                           					
                           <td><a href="/admissions/refund/">https://preview.valenciacollege.edu/admissions/refund/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/locations/">https://preview.valenciacollege.edu/locations/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/">https://valenciacollege.edu/east/</a></td>
                           					
                           <td><a href="/locations/east/">https://preview.valenciacollege.edu/locations/east/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/locations-president">https://valenciacollege.edu/east/locations-president</a></td>
                           					
                           <td><a href="/locations/east/locations-president/">https://preview.valenciacollege.edu/locations/east/locations-president/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/lakenona">https://valenciacollege.edu/lakenona</a></td>
                           					
                           <td><a href="/locations/lake-nona/">https://preview.valenciacollege.edu/locations/lake-nona/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/math">https://valenciacollege.edu/east/math</a></td>
                           					
                           <td><a href="/academics/departments/math-east/">https://preview.valenciacollege.edu/academics/departments/math-east/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola">https://valenciacollege.edu/osceola</a></td>
                           					
                           <td><a href="/locations/osceola/">https://preview.valenciacollege.edu/locations/osceola/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/locations-president/">https://valenciacollege.edu/osceola/locations-president/</a></td>
                           					
                           <td><a href="/locations/osceola/locations-president">https://preview.valenciacollege.edu/locations/osceola/locations-president</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/college-to-careers/">https://valenciacollege.edu/osceola/college-to-careers/</a></td>
                           					
                           <td><a href="/locations/osceola/college-to-careers/">https://preview.valenciacollege.edu/locations/osceola/college-to-careers/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/learningcenter/">https://valenciacollege.edu/osceola/learningcenter/</a></td>
                           					
                           <td><a href="/locations/osceola/learning-center">https://preview.valenciacollege.edu/locations/osceola/learning-center</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/mainlab/">https://valenciacollege.edu/osceola/mainlab/</a></td>
                           					
                           <td><a href="/locations/osceola/main-lab">https://preview.valenciacollege.edu/locations/osceola/main-lab</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/poinciana">https://valenciacollege.edu/poinciana</a></td>
                           					
                           <td><a href="/locations/poinciana/">https://preview.valenciacollege.edu/locations/poinciana/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/public-safety">https://valenciacollege.edu/public-safety</a></td>
                           					
                           <td><a href="/locations/school-of-public-safety/">https://preview.valenciacollege.edu/locations/school-of-public-safety/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/public-safety/fire-rescue-institute/">https://valenciacollege.edu/public-safety/fire-rescue-institute/</a></td>
                           					
                           <td><a href="/locations/school-of-public-safety/fire-rescue-institute/">https://preview.valenciacollege.edu/locations/school-of-public-safety/fire-rescue-institute/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west">https://valenciacollege.edu/west</a></td>
                           					
                           <td><a href="/locations/west/">https://preview.valenciacollege.edu/locations/west/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/locations-president">https://valenciacollege.edu/west/locations-president</a></td>
                           					
                           <td><a href="/locations/west/locations-president/">https://preview.valenciacollege.edu/locations/west/locations-president/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/wp">https://valenciacollege.edu/wp</a></td>
                           					
                           <td><a href="/locations/winter-park/">https://preview.valenciacollege.edu/locations/winter-park/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/wp/credit/">https://valenciacollege.edu/wp/credit/</a></td>
                           					
                           <td><a href="/locations/winter-park/credit/">https://preview.valenciacollege.edu/locations/winter-park/credit/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/employees/">https://preview.valenciacollege.edu/employees/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/accounts-payable/">https://valenciacollege.edu/accounts-payable/</a></td>
                           					
                           <td><a href="/employees/accounts-payable/">https://preview.valenciacollege.edu/employees/accounts-payable/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/admin">https://valenciacollege.edu/admin</a></td>
                           					
                           <td><a href="/employees/admin-assistants/">https://preview.valenciacollege.edu/employees/admin-assistants/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/courier-services">https://valenciacollege.edu/courier-services</a></td>
                           					
                           <td><a href="/employees/courier-services/">https://preview.valenciacollege.edu/employees/courier-services/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/learningday">https://valenciacollege.edu/learningday</a></td>
                           					
                           <td><a href="/employees/development/learning-day/">https://preview.valenciacollege.edu/employees/development/learning-day/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/employment">https://valenciacollege.edu/employment</a></td>
                           					
                           <td><a href="/employees/employment-at-valencia/">https://preview.valenciacollege.edu/employees/employment-at-valencia/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/events">https://valenciacollege.edu/events</a></td>
                           					
                           <td><a href="/employees/events/">https://preview.valenciacollege.edu/employees/events/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/HR">https://valenciacollege.edu/HR</a></td>
                           					
                           <td><a href="/employees/human-resources/">https://preview.valenciacollege.edu/employees/human-resources/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/AFC">https://valenciacollege.edu/AFC</a></td>
                           					
                           <td><a href="/employees/organizations-committees/association-of-florida-colleges/">https://preview.valenciacollege.edu/employees/organizations-committees/association-of-florida-colleges/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/avw">https://valenciacollege.edu/avw</a></td>
                           					
                           <td><a href="/employees/organizations-committees/association-of-valencia-women/">https://preview.valenciacollege.edu/employees/organizations-committees/association-of-valencia-women/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/bac">https://valenciacollege.edu/bac</a></td>
                           					
                           <td><a href="/employees/organizations-committees/black-advisory-committee/">https://preview.valenciacollege.edu/employees/organizations-committees/black-advisory-committee/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/curriculumcommittee">https://valenciacollege.edu/curriculumcommittee</a></td>
                           					
                           <td><a href="/employees/organizations-committees/college-curriculum-committee/">https://preview.valenciacollege.edu/employees/organizations-committees/college-curriculum-committee/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/iec">https://valenciacollege.edu/iec</a></td>
                           					
                           <td><a href="/employees/organizations-committees/institutional-effectiveness-committee/">https://preview.valenciacollege.edu/employees/organizations-committees/institutional-effectiveness-committee/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/iac">https://valenciacollege.edu/iac</a></td>
                           					
                           <td><a href="/employees/organizations-committees/instructional-affairs-committee/">https://preview.valenciacollege.edu/employees/organizations-committees/instructional-affairs-committee/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/irb">https://valenciacollege.edu/irb</a></td>
                           					
                           <td><a href="/employees/organizations-committees/international-review-board/">https://preview.valenciacollege.edu/employees/organizations-committees/international-review-board/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/plantops">https://valenciacollege.edu/plantops</a></td>
                           					
                           <td><a href="/employees/plant-operations/">https://preview.valenciacollege.edu/employees/plant-operations/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/procurement">https://valenciacollege.edu/procurement</a></td>
                           					
                           <td><a href="/employees/procurement/">https://preview.valenciacollege.edu/employees/procurement/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/propertymanagement">https://valenciacollege.edu/propertymanagement</a></td>
                           					
                           <td><a href="/employees/property-management/">https://preview.valenciacollege.edu/employees/property-management/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/retireeconnection">https://valenciacollege.edu/retireeconnection</a></td>
                           					
                           <td><a href="/employees/retiree-connection/">https://preview.valenciacollege.edu/employees/retiree-connection/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/valencia-promise">https://valenciacollege.edu/valencia-promise</a></td>
                           					
                           <td><a href="/employees/valencia-promise/">https://preview.valenciacollege.edu/employees/valencia-promise/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/wordprocessing">https://valenciacollege.edu/wordprocessing</a></td>
                           					
                           <td><a href="/employees/word-processing/">https://preview.valenciacollege.edu/employees/word-processing/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/faculty.cfm">https://valenciacollege.edu/faculty.cfm</a></td>
                           					
                           <td><a href="/faculty/">https://preview.valenciacollege.edu/faculty/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/faculty/association">https://valenciacollege.edu/faculty/association</a></td>
                           					
                           <td><a href="/faculty/association/">https://preview.valenciacollege.edu/faculty/association/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/care">https://valenciacollege.edu/care</a></td>
                           					
                           <td><a href="/faculty/care/">https://preview.valenciacollege.edu/faculty/care/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/faculty/development">https://valenciacollege.edu/faculty/development</a></td>
                           					
                           <td><a href="/faculty/development/">https://preview.valenciacollege.edu/faculty/development/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/faculty/development/tla/">https://valenciacollege.edu/faculty/development/tla/</a></td>
                           					
                           <td><a href="/faculty/development/tla/">https://preview.valenciacollege.edu/faculty/development/tla/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/epi">https://valenciacollege.edu/epi</a></td>
                           					
                           <td><a href="/faculty/educator-preparation-institute/">https://preview.valenciacollege.edu/faculty/educator-preparation-institute/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/focus">https://valenciacollege.edu/focus</a></td>
                           					
                           <td><a href="/faculty/focus-on-the-workplace/">https://preview.valenciacollege.edu/faculty/focus-on-the-workplace/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/faculty/forms">https://valenciacollege.edu/faculty/forms</a></td>
                           					
                           <td><a href="/faculty/forms">https://preview.valenciacollege.edu/faculty/forms</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/faculty/resources">https://valenciacollege.edu/faculty/resources</a></td>
                           					
                           <td><a href="/faculty/resources/">https://preview.valenciacollege.edu/faculty/resources/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/faculty/sabbatical">https://valenciacollege.edu/faculty/sabbatical</a></td>
                           					
                           <td><a href="/faculty/sabbatical/">https://preview.valenciacollege.edu/faculty/sabbatical/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/foundation">https://preview.valenciacollege.edu/foundation</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/alumni">https://valenciacollege.edu/alumni</a></td>
                           					
                           <td><a href="/foundation/alumni/">https://preview.valenciacollege.edu/foundation/alumni/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>&nbsp;</td>
                           					
                           <td><a href="/secure/">https://preview.valenciacollege.edu/secure/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/atlas/nursing">https://valenciacollege.edu/atlas/nursing</a></td>
                           					
                           <td><a href="/secure/nursing/">https://preview.valenciacollege.edu/secure/nursing/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students.cfm">https://valenciacollege.edu/students.cfm</a></td>
                           					
                           <td><a href="/students/">https://preview.valenciacollege.edu/students/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/catalog">https://valenciacollege.edu/catalog</a></td>
                           					
                           <td><a href="/students/catalog/">https://preview.valenciacollege.edu/students/catalog/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/disputes">https://valenciacollege.edu/students/disputes</a></td>
                           					
                           <td><a href="/students/disputes/">https://preview.valenciacollege.edu/students/disputes/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/disputes/academic">https://valenciacollege.edu/students/disputes/academic</a></td>
                           					
                           <td><a href="/students/disputes/academic/">https://preview.valenciacollege.edu/students/disputes/academic/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/disputes/administrative">https://valenciacollege.edu/students/disputes/administrative</a></td>
                           					
                           <td><a href="/students/disputes/administrative/">https://preview.valenciacollege.edu/students/disputes/administrative/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/disputes/civil-rights-discrimination">https://valenciacollege.edu/students/disputes/civil-rights-discrimination</a></td>
                           					
                           <td><a href="/students/disputes/civil-rights-discrimination/">https://preview.valenciacollege.edu/students/disputes/civil-rights-discrimination/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/disputes/conduct">https://valenciacollege.edu/students/disputes/conduct</a></td>
                           					
                           <td><a href="/students/disputes/conduct/">https://preview.valenciacollege.edu/students/disputes/conduct/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/disputes/grade">https://valenciacollege.edu/students/disputes/grade</a></td>
                           					
                           <td><a href="/students/disputes/grade/">https://preview.valenciacollege.edu/students/disputes/grade/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/students/disputes/sexual-harassment-assault">https://valenciacollege.edu/students/disputes/sexual-harassment-assault</a></td>
                           					
                           <td><a href="/students/disputes/sexual-harassment-assault/">https://preview.valenciacollege.edu/students/disputes/sexual-harassment-assault/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/international/exchange/">https://valenciacollege.edu/international/exchange/</a></td>
                           					
                           <td><a href="/students/exchange/">https://preview.valenciacollege.edu/students/exchange/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/graduation">https://valenciacollege.edu/graduation</a></td>
                           					
                           <td><a href="/students/graduation/">https://preview.valenciacollege.edu/students/graduation/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/international/intensive-english-program/">https://valenciacollege.edu/international/intensive-english-program/</a></td>
                           					
                           <td><a href="/students/intensive-english-program/">https://preview.valenciacollege.edu/students/intensive-english-program/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/international/studyabroad/">https://valenciacollege.edu/international/studyabroad/</a></td>
                           					
                           <td><a href="/students/study-abroad/">https://preview.valenciacollege.edu/students/study-abroad/</a></td>
                           				
                        </tr>
                        				
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/advising-counseling">https://valenciacollege.edu/advising-counseling</a></td>
                           					
                           <td><a href="/students/advising-counseling/">https://preview.valenciacollege.edu/students/advising-counseling/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/answer-center">https://valenciacollege.edu/answer-center</a></td>
                           					
                           <td><a href="/students/answer-center/">https://preview.valenciacollege.edu/students/answer-center/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/assessments">https://valenciacollege.edu/assessments</a></td>
                           					
                           <td><a href="/students/assessments/">https://preview.valenciacollege.edu/students/assessments/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/bridges">https://valenciacollege.edu/bridges</a></td>
                           					
                           <td><a href="/students/bridges-to-success/">https://preview.valenciacollege.edu/students/bridges-to-success/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/b2b">https://valenciacollege.edu/b2b</a></td>
                           					
                           <td><a href="/students/brother-to-brother/">https://preview.valenciacollege.edu/students/brother-to-brother/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/businessoffice">https://valenciacollege.edu/businessoffice</a></td>
                           					
                           <td><a href="/students/business-office/">https://preview.valenciacollege.edu/students/business-office/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/locations-store">https://valenciacollege.edu/locations-store</a></td>
                           					
                           <td><a href="/students/locations-store/">https://preview.valenciacollege.edu/students/locations-store/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/careercenter">https://valenciacollege.edu/careercenter</a></td>
                           					
                           <td><a href="/students/career-center/">https://preview.valenciacollege.edu/students/career-center/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/careerpathways">https://valenciacollege.edu/careerpathways</a></td>
                           					
                           <td><a href="/students/career-pathways/">https://preview.valenciacollege.edu/students/career-pathways/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/wp/cssc/">https://valenciacollege.edu/wp/cssc/</a></td>
                           					
                           <td><a href="/students/communications-winter-park/">https://preview.valenciacollege.edu/students/communications-winter-park/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/internship">https://valenciacollege.edu/internship</a></td>
                           					
                           <td><a href="/students/internship/">https://preview.valenciacollege.edu/students/internship/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/Labs">https://valenciacollege.edu/Labs</a></td>
                           					
                           <td><a href="/students/labs/">https://preview.valenciacollege.edu/students/labs/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/learning-support">https://valenciacollege.edu/learning-support</a></td>
                           					
                           <td><a href="/students/learning-support/west/">https://preview.valenciacollege.edu/students/learning-support/west</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/academic-success/">https://valenciacollege.edu/east/academic-success/</a></td>
                           					
                           <td><a href="/students/learning-support/east/">https://preview.valenciacollege.edu/students/learning-support/east</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/tutoring.php">https://valenciacollege.edu/lake-nona/tutoring.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring.php'</a></td>
                        </tr>
                        
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/tutoring-business.php">https://valenciacollege.edu/lake-nona/tutoring-business.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-business.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-business.php'</a></td>
                        </tr>
                        
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/tutoring-english.php">https://valenciacollege.edu/lake-nona/tutoring-english.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-english.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-english.php'</a></td>
                        </tr>
                        
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/tutoring-math.php">https://valenciacollege.edu/lake-nona/tutoring-math.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-math.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-math.php'</a></td>
                        </tr>
                        
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/tutoring-resources.php">https://valenciacollege.edu/lake-nona/tutoring-resources.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-resources.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-resources.php'</a></td>
                        </tr>
                        
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/tutoring-science.php">https://valenciacollege.edu/lake-nona/tutoring-science.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-science.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-science.php'</a></td>
                        </tr>
                        
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/tutoring-spanish.php">https://valenciacollege.edu/lake-nona/tutoring-spanish.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-spanish.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-spanish.php'</a></td>
                        </tr>
                        
                        <tr>
                           <td><a href="https://valenciacollege.edu/lake-nona/become-a-tutor.php">https://valenciacollege.edu/lake-nona/become-a-tutor.php</a></td>
                           <td><a href="https://preview.valenciacollege.edu/students/learning-support/lake-nona/become-a-tutor.php">https://preview.valenciacollege.edu/students/learning-support/lake-nona/become-a-tutor.php'</a></td>
                        </tr>
                        
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/learning-support/communications/">https://valenciacollege.edu/learning-support/communications/</a></td>
                           					
                           <td><a href="/students/learning-support/west/communications/">https://preview.valenciacollege.edu/students/learning-support/west/communications/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/learning-support/math">https://valenciacollege.edu/learning-support/math</a></td>
                           					
                           <td><a href="/students/learning-support/math/">https://preview.valenciacollege.edu/students/learning-support/west/math/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/learning-support/pert/">https://valenciacollege.edu/learning-support/pert/</a></td>
                           					
                           <td><a href="/students/learning-support/pert/">https://preview.valenciacollege.edu/students/learning-support/west/pert/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/learning-support/tutoring">https://valenciacollege.edu/learning-support/tutoring</a></td>
                           					
                           <td><a href="/students/learning-support/tutoring/">https://preview.valenciacollege.edu/students/learning-support/west/tutoring/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/students/library/">https://preview.valenciacollege.edu/students/library/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/library/west/cal">https://valenciacollege.edu/library/west/cal</a></td>
                           					
                           <td><a href="/students/library/west/cal/">https://preview.valenciacollege.edu/students/library/west/cal/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/west/math">https://valenciacollege.edu/west/math</a></td>
                           					
                           <td><a href="/students/math/west/">https://preview.valenciacollege.edu/students/math/west/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/east/mathematics-lab">https://valenciacollege.edu/east/mathematics-lab</a></td>
                           					
                           <td><a href="/students/mathematics-lab-east/">https://preview.valenciacollege.edu/students/mathematics-lab-east/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="http://valenciacollege.edu/math/livescribe.cfm">http://valenciacollege.edu/math/livescribe.cfm</a></td>
                           					
                           <td><a href="/students/math-help/">https://preview.valenciacollege.edu/students/math-help/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/wp/mathcenter/">https://valenciacollege.edu/wp/mathcenter/</a></td>
                           					
                           <td><a href="/students/math-support-center-winter-park/">https://preview.valenciacollege.edu/students/math-support-center-winter-park/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/office-for-students-with-disabilities">https://valenciacollege.edu/office-for-students-with-disabilities</a></td>
                           					
                           <td><a href="/students/office-for-students-with-disabilities/">https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/pji">https://valenciacollege.edu/pji</a></td>
                           					
                           <td><a href="/students/peace-justice-institute/">https://preview.valenciacollege.edu/students/peace-justice-institute/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/students/security/">https://preview.valenciacollege.edu/students/security/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/studentaffairs/">https://valenciacollege.edu/studentaffairs/</a></td>
                           					
                           <td><a href="/students/student-affairs/">https://preview.valenciacollege.edu/students/student-affairs/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/student-services">https://valenciacollege.edu/student-services</a></td>
                           					
                           <td><a href="/students/student-services/">https://preview.valenciacollege.edu/students/student-services/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/unitplans">https://valenciacollege.edu/unitplans</a></td>
                           					
                           <td><a href="/students/student-services/unit-plans/">https://preview.valenciacollege.edu/students/student-services/unit-plans/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/studentservices/">https://valenciacollege.edu/osceola/studentservices/</a></td>
                           					
                           <td><a href="/students/student-services-osceola/">https://preview.valenciacollege.edu/students/student-services-osceola/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/testing-center">https://valenciacollege.edu/testing-center</a></td>
                           					
                           <td><a href="/students/testing/">https://preview.valenciacollege.edu/students/testing/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/testing/">https://valenciacollege.edu/osceola/testing/</a></td>
                           					
                           <td><a href="/students/testing-osceola">https://preview.valenciacollege.edu/students/testing-osceola</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/transitions">https://valenciacollege.edu/transitions</a></td>
                           					
                           <td><a href="/students/transitions-planning/">https://preview.valenciacollege.edu/students/transitions-planning/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/veterans-affairs">https://valenciacollege.edu/veterans-affairs</a></td>
                           					
                           <td><a href="/students/veterans-affairs/">https://preview.valenciacollege.edu/students/veterans-affairs/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/writing-center/">https://valenciacollege.edu/osceola/writing-center/</a></td>
                           					
                           <td><a href="/students/writing-osceola">https://preview.valenciacollege.edu/students/writing-osceola</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/fallero">https://valenciacollege.edu/fallero</a></td>
                           					
                           <td><a href="/students/publications/fallero/">https://preview.valenciacollege.edu/students/publications/fallero/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/phoenix">https://valenciacollege.edu/phoenix</a></td>
                           					
                           <td><a href="/students/publications/phoenix/">https://preview.valenciacollege.edu/students/publications/phoenix/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/REACH">https://valenciacollege.edu/REACH</a></td>
                           					
                           <td><a href="/students/reach/">https://preview.valenciacollege.edu/students/reach/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/servicelearning">https://valenciacollege.edu/servicelearning</a></td>
                           					
                           <td><a href="/students/service-learning/">https://preview.valenciacollege.edu/students/service-learning/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/student-development">https://valenciacollege.edu/student-development</a></td>
                           					
                           <td><a href="/students/student-development/">https://preview.valenciacollege.edu/students/student-development/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/osceola/clubs/">https://valenciacollege.edu/osceola/clubs/</a></td>
                           					
                           <td><a href="/students/student-life/clubs/osceola/">https://preview.valenciacollege.edu/students/student-life/clubs/osceola/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/mobile">https://valenciacollege.edu/mobile</a></td>
                           					
                           <td><a href="/technology/mobile/">https://preview.valenciacollege.edu/technology/mobile/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/oit/">https://valenciacollege.edu/oit/</a></td>
                           					
                           <td><a href="/technology/office-of-information-technology/">https://preview.valenciacollege.edu/technology/office-of-information-technology/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td>New Prestige Page</td>
                           					
                           <td><a href="/technology/office-of-information-technology/">https://preview.valenciacollege.edu/technology/office-of-information-technology/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/oit/locations-technology-services">https://valenciacollege.edu/oit/locations-technology-services</a></td>
                           					
                           <td><a href="/technology/office-of-information-technology/locations-technology-services/">https://preview.valenciacollege.edu/technology/office-of-information-technology/locations-technology-services/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/oit/learning-technology-services">https://valenciacollege.edu/oit/learning-technology-services</a></td>
                           					
                           <td><a href="/technology/office-of-information-technology/learning-technology-services/">https://preview.valenciacollege.edu/technology/office-of-information-technology/learning-technology-services/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/oit/networking">https://valenciacollege.edu/oit/networking</a></td>
                           					
                           <td><a href="/technology/office-of-information-technology/networking/">https://preview.valenciacollege.edu/technology/office-of-information-technology/networking/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/oit/techgov">https://valenciacollege.edu/oit/techgov</a></td>
                           					
                           <td><a href="/technology/office-of-information-technology/techgov/">https://preview.valenciacollege.edu/technology/office-of-information-technology/techgov/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/oit/tss">https://valenciacollege.edu/oit/tss</a></td>
                           					
                           <td><a href="/technology/office-of-information-technology/tss">https://preview.valenciacollege.edu/technology/office-of-information-technology/tss</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/oit/web">https://valenciacollege.edu/oit/web</a></td>
                           					
                           <td><a href="/technology/office-of-information-technology/web">https://preview.valenciacollege.edu/technology/office-of-information-technology/web</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="https://valenciacollege.edu/wifi">https://valenciacollege.edu/wifi</a></td>
                           					
                           <td><a href="/technology/wifi/">https://preview.valenciacollege.edu/technology/wifi/</a></td>
                           				
                        </tr>
                        				
                        <tr>
                           					
                           <td><a href="http://blogs.valenciacollege.edu/canvas/">http://blogs.valenciacollege.edu/canvas/</a></td>
                           					
                           <td><a href="/academics/canvas/">https://preview.valenciacollege.edu/academics/canvas/</a></td>
                           				
                        </tr>
                        			
                     </tbody>
                     		
                  </table>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/sitemap.pcf">©</a>
      </div>
      	
   </body>
</html>