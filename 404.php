<?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/php/url-redirect.php"); ?>
<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="page not found, college, school, educational">
      <meta name="description" content=" Page Not Found | Valencia College">
      <title>Page Not Found | Valencia College</title>
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="container-fluid header-college">
         <nav class="container navbar navbar-expand-lg">
            <div class="navbar-toggler-right"><button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-val" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div>
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/_menu-collegewide.inc"); ?></div>
         </nav>
         <nav class="container navbar navbar-expand-lg">
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
         </nav>
      </div>

   
      <div class="page-header bg-interior">
         <div id="intro-txt">
            <h1>Page Not Found</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            
         </div>
      </div>
      <div>
         <div class="container margin-60">
            <div class="row">
               <div class="col-md-12">
				    <div class="main-title">
						
						
                     
                     <h2>404 File Not Found</h2>
                     
<hr class="styled_2">
                     <form label="search" class="relative" action="https://search.valenciacollege.edu/search" name="seek" id="seek" method="get">
                     <label for="q"><span style="display:none;">Search</span></label>
                      <input type="text" name="q" id="q" value="" aria-label="Search" />
					   
                      <button type="submit" name="submitMe" value="seek" >Search</button>
                     <input type="hidden" name="site" value="preview" />
                      <input type="hidden" name="client" value="preview"/>
                      <input type="hidden" name="proxystylesheet" value="preview"/>
                      <input type="hidden" name="output" value="xml_no_dtd"/>
                </form>
                  </div>	
				   <p>Refer to the <a href="/sitemap.php">Site Map</a> for more information.</p>
                  </div>
               </div>
            </div>
         
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         © 2018 - All rights reserved.
      </div>
   </body>
</html>