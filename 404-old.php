<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page Not Found | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script>
					var page_url="http://preview.valenciacollege.edu/academics/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
     <!-- ChromeNav Header================================================== --><div class="container-fluid header-college">
         <nav class="container navbar navbar-expand-lg">
            <div class="navbar-toggler-right"><button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-val" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div>
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/_menu-collegewide.inc"); ?></div>
         </nav>
         <nav class="container navbar navbar-expand-lg">
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/_menu.inc"); ?></div>
         </nav>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            
         </div>
      </div>
      <div>
         <div class="container margin-60">
            <div class="row">
               <div class="col-md-12">
                  <div class="box_style_1">
                     	
                     		<p>
								Content Still in Development!
					  </p>
					  <p>
						  Refer to the <a href="/sitemap.php">Site Map</a> for more information.
					  </p>
					 
                     	
                     <hr class="styled_2">
                  </div>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/index.pcf">©</a>
      </div>
   <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/404.php">©</a></body>
</html>
