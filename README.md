# README #

How to use GIT: http://rogerdudler.github.io/git-guide/ or https://git-scm.com/book/en/v2 for all the details

## CMS CLEAN-UP MIGRATION FOR VALENCIA .EDU ##

* Use your IDE's GIT commands, SourceTree or GITKraken for connecting to the GIT (*CLI for the savvy*)
* Clone https://Valencia-Repo@bitbucket.org/wps-valencia/cms-clean-up-migration.git

### Guidelines ###

* Create a branch whose name starts with your own; ie, ***sfbard-clean-up***
* Commit to your own branch frequently while working during the week
* Push your branch up to the GIT repo ***only*** when you are finished with all changes
* Don't commit any master css or js files or any files to the ***css*** and ***js*** folders. Leave those as app.js and Jim will merge them as needed.
* Don't make changes to any master img files without permission
* Always download or pull the latest version before starting