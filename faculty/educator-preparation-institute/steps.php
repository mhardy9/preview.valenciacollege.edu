<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>How To Enroll | Educator Preparation Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/steps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educator Preparation Institute</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>How To Enroll</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        					
                        <h2>How to Enroll in EPI</h2>
                        					
                        <p> In order to be considered for enrollment in the 
                           						Educator Preparation Institute (EPI) program, you must possess a <strong><em>bachelor's degree with a minimum overall GPA of 2.5 </em></strong> from a regionally accredited institution, and have a passing score on the Basic Skills
                           General Knowledge Exam. You must also possess a <strong><em>Statement of Status of Eligibility </em></strong> from the Florida Department of Education (FLDOE) stating "<strong>You are Eligible for a Florida Educator's Certificate.</strong>" The "Eligibility" statement must be presented at the EPI Enrollment Session. This
                           process takes approximately 4-6 weeks, please plan accordingly. You cannot begin EPI
                           coursework until a valid Statement of Eligibility has been submitted to our office.
                           If your Statement of Eligibility expires while you are enrolled in EPI, you will not
                           be permitted to continue taking courses until you renew your eligibility statement
                           and provide the EPI office with a copy. 
                        </p>
                        					
                        <p>To obtain this statement, you must apply for Certification at <a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">http://www.fldoe.org/edcert/steps.asp</a></p>
                        					
                        <p> This process includes a $75.00 processing fee for each subject area in which you
                           intend to be certified. 
                        </p>
                        					
                        <p><strong><em>If you are not eligible, the statement will list any necessary requirements that you
                                 must fulfill in order to be found eligible. You cannot be admitted to the program
                                 unless you are "Eligible for a Florida Educator's Certificate" </em></strong></p>
                        
                        					
                        <h3>Steps to Enrollment</h3>
                        					
                        <h4>1. Complete all of the following required documents to submit at an EPI Enrollment
                           Session:
                        </h4>
                        					
                        <ul class="list_style_1">
                           						
                           <li>Complete an <a href="/admissions/admissions-records/forms.php">Valencia Admission Application</a> from  this Valencia website.  You can do this online. You will need to supply proof
                              of all residency documents and a check for the $35.00 processing fee in order to be
                              enrolled. You do not need the application, residency information, and application
                              fee when attending the Enrollment/Information session, but you will need them in order
                              to register for classes. 
                           </li>
                           						
                           <li>Review the <a href="documents/ADMISSION-CRITERIA-AND-CHECKLIST-2017-2018.pdf" target="_blank">EPI Enrollment Checklist</a>
                              						
                           </li>
                           						
                           <li>Download, print, and complete the <a href="documents/EPI-Application.pdf" target="_blank">EPI Application</a> 
                           </li>
                           						
                           <li>Download, complete, and print the <a href="documents/BRIEF-WRITING-STATEMENT-INSTRUCTIONS.pdf" target="_blank">Brief Writing Statement</a>
                              						
                           </li>
                           						
                           <li>Download, print, and complete the <a href="documents/FEAPsPre-Assessment.pdf" target="_blank">FEAPS Pre-Assessment</a> 
                           </li>
                           						
                           <li>Download, print, and complete the <a href="documents/EPIEducationPlan2016.pdf" target="_blank">EPI Education Plan</a>
                              						
                           </li>
                           					
                        </ul>
                        					
                        <p>All required paperwork must be submitted to the EPI office no later than the deadlines
                           below:
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th scope="col">Enrollment Term</th>
                                 								
                                 <th scope="col">Application Deadline</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Fall Semester </td>
                                 								
                                 <td>August 1 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Spring Semester </td>
                                 								
                                 <td>December 1</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Summer Semester</td>
                                 								
                                 <td>May 1 *</td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p>* Note: Summer ONLINE courses begin in early May and all required EPI paperwork must
                           be submitted before online courses begin in the summer term. Once a course begins,
                           you cannot register for the course. Summer INTENSIVE courses begin in mid June. All
                           required EPI paperwork must be submitted by June 1st. All EPI courses are limited
                           to a space available basis. 
                        </p>
                        
                        
                        					
                        <h4>2. Please bring your official sealed transcripts to the Enrollment session, or have
                           them sent to:
                        </h4>
                        					
                        <address>
                           						The Educator Preparation Institute<br>
                           						Valencia College 4-1<br>
                           						1800 S. Kirkman Rd.<br>
                           						Orlando, FL 32811<br>
                           					
                        </address>
                        					
                        <p>Note:  You may present your unopened, sealed official transcripts at the Enrollment
                           Session if they cannot be sent.  You must present all of the above mentioned documents
                           at the mandatory Enrollment Session to be approved for the program. 
                        </p>
                        					
                        <h4>3. Attend a Mandatory Enrollment Session:</h4>
                        					
                        <p>To reserve a seat or to request an individual enrollment session,  contact Donna Deitrick
                           at 407-582-5473 or email <a href="mailto:ddeitrick@valenciacollege.edu" rel="noreferrer">ddeitrick@valenciacollege.edu</a>. 
                        </p>
                        
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th scope="col">Campus</th>
                                 								
                                 <th scope="col">Date</th>
                                 								
                                 <th scope="col">Room Number</th>
                                 								
                                 <th scope="col">Start Time</th>
                                 								
                                 <th scope="col">End Time</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>West</td>
                                 								
                                 <td>10/25/2017</td>
                                 								
                                 <td>1-254</td>
                                 								
                                 <td>5:30 pm </td>
                                 								
                                 <td>7:00 pm </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>West</td>
                                 								
                                 <td>11/29/2017</td>
                                 								
                                 <td>1-254</td>
                                 								
                                 <td>5:30 pm </td>
                                 								
                                 <td>7:00 pm </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <p>Note: Dates and times are subject to change, please check schedule before attending
                           your selected enrollment sessions
                        </p>
                        
                        					
                        <h4>4.  After all required paperwork has been submitted, you will receive a letter from
                           the EPI program regarding your admission status by mail. 
                        </h4>
                        					
                        <p>For addtional information you may contact the Educator Preparation Institute contacts:
                           
                        </p>
                        					
                        <ul class="list_style_1">
                           						
                           <li>Otoniel Rodriguez, Records Specialist, 407.582.5581 or email <a href="mailto:orodriguez40@valenciacollege.edu">orodriguez40@valenciacollege.edu</a>
                              						
                           </li>
                           						
                           <li>Donna Deitrick, Coordinator, 407.582.5473 or email <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a>
                              						
                           </li>
                           					
                        </ul>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/steps.pcf">©</a>
      </div>
   </body>
</html>