<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Educator Preparation Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/about.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educator Preparation Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>Program Information</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        
                        					
                        <h2>The Educator Preparation Institute (EPI) Alternative Certification 
                           						Program
                        </h2>
                        					
                        <p>The Educator Preparation Institute (EPI) program at Valencia College is designed for
                           individuals who want to teach, but hold a bachelor's degree in an area other than
                           education.&nbsp; This alternative certification program provides the knowledge and tools
                           necessary for earning a Florida Professional Teaching Certificate. 
                        </p>
                        					
                        <p>The program consists of 7 courses and a <a href="field-experience.php">field experience course </a>.&nbsp; The field experience course requires a total of 30 hours of observation in an approved
                           school classroom. Each course is 8 weeks in length and is presented in either an online
                           or hybrid format. Hybrid courses meet twice per 8 week term for 3 hours on campus
                           with all other coursework being online. 
                        </p>
                        					
                        <p><strong> Students who are not in a full-time teaching position or working as a full-time teacher's
                              aide or paraprofessional will have to complete 5 hours of volunteer hours in K-12
                              setting per 8 week term, not per course for hybrid courses. </strong>The exception will be when students are registered for their field experience courses.
                           Information about how to complete these volunteer hours will be communicated to students
                           by their instructors at the beginning of the term. Each course can require between
                           5-10 hours of work outside of your class. For this reason, it is important that you
                           consider commitments and register for the right amount of courses. <em>You are only allowed to take 2 courses per 8 week term</em>, but this requires a heavy time commitment.
                        </p>
                        					
                        <p>We also offer fully-online courses, but if you are not a full-time teacher or a para-professional,
                           then you must complete <strong>10 hours of observation in a public, private, or charter school for each 8 week term
                              that you are registered for an online course. </strong>Information about the observation will be provided by your instructor about the observation
                           requirement. These courses are intensive and require the completion of several projects
                           and research assignments along with readings, group work, and quizzes. You can estimate
                           about 10 hours worth of coursework per week, per course. Many students take 2 courses
                           at a time, but you are not required to take more than one. By taking 2 courses at
                           a time, it is possible to complete the program in two semesters, but it may take three
                           to four semesters. You must also complete all of the FTCE Exams and an EPI portfolio
                           in order to complete the program. 
                        </p>
                        					
                        <p>These courses will prepare you not only for the Florida Teacher Certification Exam
                           (Profession), but for success as a teacher in a classroom. Please refer to <a href="courses.php">Courses </a>link for more information regarding courses. 
                        </p>
                        					<br>
                        					
                        					
                        <h3>Requirements for participation include:</h3>
                        					
                        <ul class="list_style_1">
                           						
                           <li> Hold a bachelor's degree with a minimum overall GPA of 2.5 from a regionally accredited
                              college 
                              							or university
                           </li>
                           						
                           <li>Obtain a statement of Status of Eligibility from the <a href="http://www.fldoe.org/edcert/steps.asp">Florida 
                                 							Department of Education (FLDOE) </a> stating "You are Eligible" 
                           </li>
                           						
                           <li>Have a passing score on the Basic Skills General Knowledge Exam</li>
                           						
                           <li>Attend a <em><strong>MANDATORY</strong></em> EPI Enrollment Session (email Donna Deitrick regarding enrollment session at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a>.
                           </li>
                           						
                           <li> Submit a College Application on-line and pay admission 
                              							fee
                           </li>
                           						
                           <li>Submit an EPI Application to Building 1 room 255 (West Campus), fax 407-582-5582 or
                              scan and email to Donna Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu. </a>
                              						
                           </li>
                           						
                           <li>Complete all required paperwork</li>
                           					
                        </ul>
                        					
                        <p>Click here to view a sample <a href="documents/HowdoIknowifIneedEPI.ppt">valid statement of eligibility</a></p>
                        					
                        <p>If you have further questions, please refer to our <a href="faqs.php">Frequently Asked Questions page </a>or use the <a href="http://net4.valenciacollege.edu/forms/epi/contact.cfm" target="_blank">Contact Us </a> link to email us your specific questions. We will respond in a timely manner.
                        </p>
                        
                        					
                        <p><strong><a href="enroll/index.php">Sign up for an Enrollment/Information Session here</a> </strong></p>
                        
                        					
                        <p><em><strong>NOTE: Dates and times are subject to change, please check schedule before attending
                                 your selected enrollment session.</strong></em></p>
                        					
                        					
                        <p>For information on Gainful Employment, <a href="/academics/programs/asdegrees/gainful-employment/">click here</a>. 
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/about.pcf">©</a>
      </div>
   </body>
</html>