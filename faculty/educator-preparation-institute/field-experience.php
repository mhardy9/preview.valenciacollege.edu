<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Field Experience Requirements | Educator Preparation Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/field-experience.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educator Preparation Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>Field Experience Requirements</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        
                        					
                        <h2>Field Experience Requirements:</h2>
                        					
                        <p>The purpose of field observations in EPI 950  is to note and interpret the various
                           interpersonal and instructional interactions between the teacher and student(s) as
                           well as interactions occurring among students in the classroom in order to identify
                           patterns of behavior.  You will complete a series of experiences designed to give
                           prospective teachers an insight on the varied backgrounds and cultures of students
                           in public schools. You will complete three 20-40 minute teaching demonstrations and
                           one mini lesson in the EPI classroom on West campus one Saturday during a 16 week
                           term.
                        </p>
                        					
                        <p>All Field Experience paperwork and <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> must be completed and submitted by the due dates. 
                        </p>
                        					
                        <ul>
                           						
                           <li>If you plan to enroll in field experience courses for the Fall semester, paperwork
                              is due by <em><strong>March 1st. </strong></em></li>
                           						
                           <li>If you plan to enroll in field experience courses in the Spring semester, paperwork
                              is due by <em><strong>October 1st. </strong></em></li>
                           					
                        </ul>
                        					
                        <p>If you have questions regarding field experience internships, or you need to submit
                           your <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> , please email or contact:
                        </p>
                        					
                        <p>Donna Deitrick: (407) 582-5473 or email <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582
                        </p>
                        					
                        <p><strong>NOTE: All EPI students MUST complete a Field Experience Intent Form, including current
                              teachers before the deadline the semester BEFORE you plan to enroll in field experience
                              course. (EPI0950). </strong></p>
                        					
                        					
                        <h3>Orange County Students: </h3>
                        					
                        <p><strong>Orange County Teachers who are currently employed</strong>:
                        </p>
                        					
                        <p>1. Complete and submit the <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> to Donna&nbsp; Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582 by the deadline. 
                        </p>
                        
                        					
                        <p><strong>If you are NOT currently employed by the Orange County School Board:</strong> 
                        </p>
                        					
                        <p>1. Complete and submit the <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> to Donna&nbsp; Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582 by the deadline. 
                        </p>
                        					
                        <p>2. You will receive an internship packet after you have submitted your Field Experience
                           Intent to Enroll form.
                        </p>
                        					
                        <p>3. You must complete the internship packet by the deadline provided when you received
                           the packet. These are <strong>non-negotiable deadlines</strong> set by the Orange County School Board.
                        </p>
                        					
                        <p>4. Complete Fingerprinting with OCPS, directions will be provided in the internship
                           packet.
                        </p>
                        					
                        <p>5. Submit the completed intership packet to Donna Deitrick by the deadline provided.
                           
                        </p>
                        					
                        <p><strong>***DO NOT COMPLETE FINGERPRINTING UNTIL INSTRUCTED BY EPI*** </strong></p>
                        					
                        <blockquote>
                           						
                           <p><strong><em>PLEASE NOTE:&nbsp; If you are CURRENTLY employed with Orange County School Board as a teacher,
                                    substitute, or paraprofessional, you will not need to be re-fingerprinted.&nbsp; If you
                                    were employed in the past and have had a break in employment, you WILL need to get
                                    fingerprinted again. </em></strong></p>
                           					
                        </blockquote>
                        					
                        					
                        <h3>Osceola County Students: </h3>
                        					
                        <p><strong>Osceola County Teachers who are currently employed</strong>:
                        </p>
                        					
                        <p>1. Complete and submit the <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> to Donna&nbsp; Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582 by the deadline. 
                        </p>
                        
                        					
                        <p><strong>If you are NOT currently employed by the Osceola County School Board</strong>:
                        </p>
                        					
                        <p>1. Complete and submit the <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> to Donna&nbsp; Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582 by the deadline. 
                        </p>
                        					
                        <p>2. You will receive an internship packet <strong>after</strong> you have submitted your Field Experience Intent to Enroll form with further directions.
                        </p>
                        					
                        <p><strong>***DO NOT COMPLETE FINGERPRINTING UNTIL INSTRUCTED BY EPI*** </strong></p>
                        					
                        					
                        <h3>Lake County Students: </h3>
                        					
                        <p><strong>Lake  County Teachers who are currently employed</strong>:
                        </p>
                        					
                        <p>1. Complete and submit the <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> to Donna&nbsp; Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582 by the deadline. 
                        </p>
                        
                        					
                        <p><strong>If you are NOT currently employed by the Lake County School Board</strong>:
                        </p>
                        					
                        <p>1. Complete and submit the <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> to Donna&nbsp; Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582 by the deadline. 
                        </p>
                        					
                        <p>2. You will receive an internship packet <strong>after</strong> you have submitted your Field Experience Intent to Enroll form with further directions.
                        </p>
                        					
                        <p><strong>***DO NOT COMPLETE FINGERPRINTING UNTIL INSTRUCTED BY EPI*** </strong></p>
                        					
                        					
                        <h3>All  Other Counties/Private or Charter School requests:</h3>
                        					
                        <p>1. Complete and submit the <a href="documents/FieldExperienceIntentForm_000.pdf">Field Experience Intent Form</a> to Donna&nbsp; Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> or fax to (407) 582-5582 by the deadline.
                        </p>
                        					
                        <p>2. You must work with your county to obtain placement in a K-12 school for field experience
                           internship. 
                        </p>
                        					
                        <p>3. Once you have obtained placement from your county or private school, you must submit
                           a <a href="documents/FieldExperienceProposalForm.pdf">Field Experience Proposal Form</a>to Donna Deitrick at <a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a> by the deadline provided.
                        </p>
                        					
                        <p>4. You will receive an internship packet <strong>after</strong> you have submitted your Field Experience Intent to Enroll form with further directions.
                        </p>
                        					
                        <p>5. The EPI Coordinator will review your placement and notify you of placement approval.</p>
                        					
                        <p><strong><em>Please Note: If you plan to complete a field experience in another county or private
                                 school, you will be responsible for meeting county requirements for internship placement.
                                 (fingerprinting, background check etc) Internship placement must reflect the grade
                                 level and subject area of your status of eligibility from the Florida Department of
                                 Education. Supervising teachers should be fully certified in their teaching area.&nbsp;
                                 They must be high performing educators with 3 years of teaching experience, and have
                                 completed the Clinical Supervision course.&nbsp; In addition, for interns in Early Childhood
                                 Education, Elementary Education, English Education and Exceptional Education, supervising
                                 teachers must be ESOL endorsed.&nbsp; They must express willingness to participate in Clinical
                                 Supervision and demonstrate a high level of mentoring and supervisory skills.&nbsp; Supervising
                                 teachers must be recommended by school administrators. </em></strong></p>            
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/field-experience.pcf">©</a>
      </div>
   </body>
</html>