<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Educator Preparation Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educator Preparation Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li>Educator Preparation Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Teacher Preparation and Re-Certification</h2>
                        
                        
                        <h3>Educator Preparation Institute (EPI)</h3>
                        
                        <p>The Educator Preparation Institute (EPI) program at Valencia College is designed for
                           individuals who want to teach, but hold a bachelor's degree in an area other than
                           education.&nbsp; This alternative certification program provides the knowledge and tools
                           necessary for earning a Florida Professional Educator's Certificate. To qualify for
                           the Educator Preparation Institute, you must have the following: 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Hold a bachelor's degree with a minimum overall GPA of 2.5 from a regionally accredited
                              college or university 
                           </li>
                           
                           <li>Obtain a statement of Status of Eligibility from the <a href="http://www.fldoe.org/edcert/steps.asp">Florida Department of Education (FLDOE) </a>stating "You are Eligible" 
                           </li>
                           
                           <li>Have a passing score on the Basic Skills General Knowledge Test </li>
                           
                           <li>Attend a MANDATORY EPI Enrollment Session </li>
                           
                           <li>Submit a College Application on line at <a href="/admissions/admissions-records/">Admissions &amp; Records - Valencia College</a>
                              
                           </li>
                           
                           <li>Submit a Proof of Residency Form on line at <a href="/admissions/admissions-records/florida-residency/">Florida Residency - Admissions &amp; Records - Valencia College</a>
                              
                           </li>
                           
                           <li>Contact Donna Deitrick, Coordinator 407.582.5473 or Jennifer Walsh, Technical Documentation
                              Specialist, 407.582.5581 with questions 
                           </li>
                           
                           <li>Complete all required paperwork</li>
                           
                           <li>Submit required paperwork to Building 1 Room 255 (West Campus) or bring to the MANDATORY
                              EPI enrollment session. 
                           </li>
                           
                        </ul>
                        
                        <p> Please click the <a href="steps.php">"How to Enroll in EPI"</a> link for more details regarding the EPI program 
                        </p>
                        
                        <h3> Teacher Re-Certification </h3>
                        
                        <p> Teachers who already hold a Professional Educator's Certificate and in need of certificate
                           renewal, may complete six (6) semester hours of college credit or equivalent earned
                           during each renewal period.&nbsp;Most of Valencia's courses meet the requirements of teachers
                           seeking re-certification. Please click the<a href="teacher-recertification.php"> "Teacher Re-Certification" </a>link for more details.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/index.pcf">©</a>
      </div>
   </body>
</html>