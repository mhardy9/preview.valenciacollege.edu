<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/teacher-recertification.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div> 
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div> 
                                          
                                          
                                          
                                          <div>
                                             <span>Related Links</span>
                                             
                                             <ul>
                                                
                                                <li><a href="http://www.fldoe.org/edcert/step4.asp" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                
                                                <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                
                                                <li><a href="http://www.education.ucf.edu/ftce/schedule.cfm" target="_blank">Florida Teacher Certification Examination Workshops</a></li>        
                                                
                                                <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                
                                                <li><a href="http://www.ocps.k12.fl.us/" target="_blank">Orange County  Public Schools</a></li>
                                                
                                                <li><a href="../valencia-promise/index.html" target="_blank">Valencia Promise</a></li>
                                                
                                             </ul>
                                             
                                          </div>
                                          
                                          
                                          
                                          
                                          
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">          
                                                      
                                                      
                                                      <h2>Teacher Re-Certification</h2>
                                                      
                                                      <p>Teachers who already hold a Professional Certificate and in need of certificate renewal
                                                         may complete six (6) semester hours of college credit or equivalent earned during
                                                         each renewal period.&nbsp;Most of Valencia's courses meet the requirements of teachers
                                                         seeking recertification. These courses are offered in a variety of delivery modes
                                                         (face-to-face, online, hybrid, etc.) on each Valencia campus. A grade of at least
                                                         “C” must be earned in each college course used for renewal. Schools/Districts may
                                                         have specific expectations regarding which courses a teacher should complete, teachers
                                                         should seek the recommendations and clarification through the school or district of
                                                         employment.&nbsp; ALL other questions regarding fulfilling the re-certification requirements
                                                         <strong>must be directed to the Florida Department of Education</strong>.
                                                      </p>
                                                      
                                                      <p>Please access pertinent information through the links to the left and complete the
                                                         <a href="documents/RE-CERTIFICATIONCHECKLIST.pdf">Teacher Re-Certification checklist. </a></p>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <h4>_____</h4>
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p><strong>1.</strong> Submit a completed college application for admission by the College Application Deadline.
                                                                     Visit Valencia's Web site at <a href="http://www.valenciacollege.edu/admissions%20">www.valenciacollege.edu/admissions </a>for specific information. Select non-degree seeking and teacher re-certification for
                                                                     “Planned Course of Study” (do NOT&nbsp; select EPI). A $35 non-refundable application fee
                                                                     is required.<strong></strong></p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <h4>_____ </h4>
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p><strong>2. </strong>Complete a "Statement of Florida Residency for Tuition Purposes" at <a href="http://www.valenciacollege.edu/admissions-records">valenciacollege.edu/admissions-records</a> It must be completed and submitted by U.S. Mail, time permitting to allow for registration,
                                                                     or brought to one of the Answer Centers:&nbsp;&nbsp; <a href="../../academics/answer-center.html">http://valenciacollege.edu/answer-center</a> *<strong>Note: until the residency document is processed (which also takes 5- 7 days business
                                                                        days to process), residency status is entered as a non- resident and tuition is calculated
                                                                        at the non-resident rate. There are deadlines identified to submit the appropriate
                                                                        paperwork/documentation to secure Florida residency.&nbsp;&nbsp;&nbsp; PLEASE be aware of that information
                                                                        prior to submitting your material.</strong> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>_____</p>
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p><strong>3.</strong> Once admitted to the college, an Atlas account needs to be created. Please visit
                                                                     the website <a href="http://valenciacollege.edu/atlas/documents/AtlasQuickRef.pdf">http://valenciacollege.edu/atlas/documents/AtlasQuickRef.pdf</a> for directions 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>_____ </p>
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p><strong>4. </strong>Register for class(es) (if the class is not full) through ATLAS: <a href="https://atlas.valenciacollege.edu/">https://atlas.valenciacollege.edu/</a> *<strong>Note: EPI (Educator Preparation Institute) courses, Real Estate, Public Administration
                                                                        or Developmental/Prep courses CANNOT be used for recertification</strong> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>_____ </p>
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p><strong>5. </strong>Payment of Courses: Once registered, payment for classes can be made through your
                                                                     Atlas account or in person at the business office on any campus.&nbsp; For more information,
                                                                     please visit <a href="../business-office/index.html">http://valenciacollege.edu/businessoffice/</a> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>_____</p>
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p><strong>6.</strong> Purchase Textbooks: It is best to purchase your book through the campus where the
                                                                     class is being offered.&nbsp; Please visit the Bookstore website at <a href="../../students/locations-store/index.html">http://valenciacollege.edu/locations-store/</a> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>            
                                                      
                                                      <h2> Tuition &amp; Cost </h2>
                                                      
                                                      <ul>
                                                         
                                                         <li> 
                                                            <h3>Instate Tuition</h3> 
                                                            <strong>= $103.06 per credit hour </strong>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <h3>Out-of-State Tuition</h3> 
                                                            <strong>= $390.96 per credit hour</strong>
                                                            
                                                         </li>
                                                         
                                                      </ul>            
                                                      
                                                      <p>*Please be aware for the admissions process, if you   applied as "Teacher Recertification"
                                                         or "Personal Interest" status, you   are not required to attend a college orientation
                                                         session.<strong>.</strong></p>
                                                      
                                                      <h2> Course Topics</h2>
                                                      
                                                      <p>The following topics are appropriate for renewing your Professional Certificate. If
                                                         you have any questions regarding your re-certification, please contact the Florida
                                                         Department of Education and visit the DOE website at <a href="http://www.fldoe.org/teaching/certification/fl-educator-certification-renewal-requ.stml">http://www.fldoe.org/edcert/renew.asp</a></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Content specific to the subject area(s</p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Methods or education strategies specific to the subject area(s) </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Methods of teaching reading and literacy skills acquisition </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Computer literacy, computer applications, and computer education </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Exceptional student education </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>ESOL (English for Speakers of Other Languages) </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Drug abuse, child abuse and neglect, or student dropout prevention </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Training related to the goals of the Florida K-20 System, such as:
                                                               
                                                            </p>
                                                            
                                                            <ul>
                                                               
                                                               <li>
                                                                  
                                                                  <p>Content - English, economics, mathematics, science, social sciences, foreign languages,
                                                                     humanities, global economy, technology, ecology, first aid, health, or safety 
                                                                  </p>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <p>Classroom Strategies - Cooperative learning, problem-solving skills, critical-thinking
                                                                     skills, classroom management, child development, collaboration techniques for working
                                                                     with families, social services, child guidance and counseling, teaching reading, or
                                                                     educational assessments, etc. 
                                                                  </p>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <p>School Administration Accountability - Instructional design, leadership skills, school
                                                                     and community relations, school finance, school facilities, school law, or school
                                                                     organization 
                                                                  </p>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <p>Vocational and Adult Education Accountability - Adult learning, principles of adult
                                                                     or vocational education, vocational education for students with special needs, or
                                                                     vocational guidance
                                                                  </p>
                                                                  
                                                               </li>
                                                               
                                                            </ul>
                                                            
                                                         </li>
                                                         
                                                         <blockquote>
                                                            
                                                         </blockquote>
                                                         
                                                      </ul>
                                                      
                                                      <h2>Transcript Requests</h2>
                                                      
                                                      <p>Valencia College charges $3.00 per copy for official transcripts. Valencia College
                                                         cannot process your requests via e-mail, fax or over the telephone.&nbsp; Both Federal
                                                         and State laws on student privacy require that we have your signature on the request.
                                                         We cannot under any circumstances, fax or e-mail official transcripts.&nbsp; Additionally,
                                                         we cannot provide an official transcript if there is a hold on your student account.
                                                         Please be sure to include the exact mailing address of where the official transcript
                                                         is to be mailed with your signature. Official transcripts are only mailed. We do not
                                                         provide the option for in-person pick-up. Please plan accordingly. For instructions
                                                         regarding transcript requests, please visit the Records website at <a href="../admissions-records/officialtranscripts.html">http://valenciacollege.edu/admissions-records/officialtranscripts.cfm</a></p>
                                                      
                                                      <p>*Teachers completing summer class(es) in Term A and requiring documentation (Valencia
                                                         memo only) for certification/recertification prior to the posting of official summer
                                                         transcripts in early August, please complete the following: Contact: Reda Buchanan
                                                         at (407) 582-1506 or by email at: <a href="mailto:rjackson68@valenciacollege.edu">ubuchanan@valenciacollege.edu</a>&nbsp; and provide: 
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Name</li>
                                                         
                                                         <li>VID/Social Security number (if appropriate)</li>
                                                         
                                                         <li>Course(s)</li>
                                                         
                                                         <li>CRN(s)</li>
                                                         
                                                         <li>Course Dates</li>
                                                         
                                                         <li># Of Credits</li>
                                                         
                                                         <li>Instructor Name(s)</li>
                                                         
                                                         <li>How memo should be addressed: To DOE (Department of Education) or the County School
                                                            Office or specific school etc.
                                                         </li>
                                                         
                                                         <li>How memo should be distributed (i.e. U.S. mail and to what address or picked up at
                                                            West Campus, SSB, Room 106
                                                         </li>
                                                         
                                                      </ul>            
                                                      
                                                      <h3>&nbsp;</h3>
                                                      
                                                      <h2>Contact Us</h2>
                                                      
                                                      <p>For questions related to Teacher Recertification, please send a request (for an automated
                                                         reply with general information) or visit <a href="http://www.valenciacollege.edu/epi">www.valenciacollege.edu/epi</a></p>
                                                      
                                                      
                                                      <p><strong>Teacher Preparation and Re-certification - Building 1 room 255, West Campus </strong></p>
                                                      
                                                      <p><strong>1800 S. Kirkman Rd. Orlando, FL 32811</strong></p>
                                                      
                                                      <p><strong>(407) 582-5473</strong></p>
                                                      
                                                      <p><span>**If your Professional Educator's Certificate has expired, visit the <a href="http://www.fldoe.org/teaching/certification/renewal-requirements/reinstatement.stml">Florida Department of Education website</a> to view the steps to renew your certificate.**</span></p>
                                                      
                                                      <p><a href="#top">TOP</a></p>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/teacher-recertification.pcf">©</a>
      </div>
   </body>
</html>