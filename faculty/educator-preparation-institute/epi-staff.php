<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/epi-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div> 
                                          
                                          
                                          
                                          <div>
                                             <span>Related Links</span>
                                             
                                             <ul>
                                                
                                                <li><a href="http://www.fldoe.org/edcert/step4.asp" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                
                                                <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                
                                                <li><a href="http://www.education.ucf.edu/ftce/schedule.cfm" target="_blank">Florida Teacher Certification Examination Workshops</a></li>        
                                                
                                                <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                
                                                <li><a href="http://www.ocps.k12.fl.us/" target="_blank">Orange County  Public Schools</a></li>
                                                
                                                <li><a href="../valencia-promise/index.html" target="_blank">Valencia Promise</a></li>
                                                
                                             </ul>
                                             
                                          </div>
                                          
                                          
                                          
                                          
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      <div>Teacher Preparation and Re-Certification 
                                                         
                                                      </div>
                                                      
                                                      <h2>About Us</h2>
                                                      
                                                      <h3>Dr. Rhonda Atkinson, Program Chair </h3>
                                                      
                                                      <p>Rhonda is proud to say that she is from the great state of Arkansas and was on the
                                                         faculty at Louisiana State University (LSU) in Baton Rouge for about 20 years and
                                                         at Central Missouri State University (CMSU) for about 7 years.&nbsp; Although she has taught
                                                         undergraduate and graduate education students, most of her higher education experience
                                                         concerns the development of reading and learning strategies in first-year students.&nbsp;
                                                         This provides her with an excellent background for teaching education courses because
                                                         she has had broad experiences in a number of content areas. Rhonda enjoys curriculum
                                                         development and instructional design.&nbsp; She started teaching at Valencia in Fall of
                                                         2006 and she teaches undergraduate education courses as well as courses for the Educator
                                                         Preparation Institute.&nbsp;Rhonda has an undergraduate degree in Elementary, Special,
                                                         and Early Childhood Education from the University of Central Arkansas and a Master’s
                                                         degree in Learning Disabilities.&nbsp; Her Ph.D. is in Educational Curriculum and Instruction
                                                         (reading emphasis) from LSU.&nbsp; Rhonda’s husband teaches graduate courses in educational
                                                         technology and is also a jazz musician.&nbsp; They have been married for 35 years. In her
                                                         spare moments in the past, she has written several college textbooks in reading and
                                                         study skills and worked on a variety of other writing projects (e.g. workplace learning
                                                         in industrial construction trades, ESL health curriculum for low-literate adults,
                                                         afterschool program on museums in Louisiana for middle school students.) She is also
                                                         currently working on a textbook on college success for students at community colleges
                                                         and she most recently worked on curriculum projects for the U.S. Army and for Northrop
                                                         Grumman.&nbsp;&nbsp;Rhonda’s daughter graduated from UCF with a degree in hospitality management
                                                         and works for the corporate offices of the Kessler Collection here in Orlando. Rhonda
                                                         has 2 cats and enjoys going to the theme parks! She has two major goals for for EPI
                                                         students, she says, "First, I want to provide you with the <strong><em>knowledge you need to become a certified teacher</em> </strong>in Florida. Second, I want to help you develop the<strong> <em>knowledge and expertise you need to face your students with complete confidence."</em></strong> 
                                                      </p>
                                                      
                                                      <h3>Donna Deitrick, Coordinator</h3>
                                                      
                                                      <p>Donna has connections with Valencia College that are very strong.&nbsp; In 1986 she began
                                                         her career with Valencia as an Administrative Assistant for the Center of High-Tech
                                                         Training for Individuals with Disabilities where she eventually became th<img alt="Donna in a red dress and blonde hair" height="150" hspace="0" src="Valenciapic.jpg" vspace="0" width="104">e Recruitment/Placement Specialist for 11 years.&nbsp; While working at Valencia she attained
                                                         an AS Degree in Real Estate Management.&nbsp; In 1998, Donna retired to become an advocate
                                                         for her son’s educational pursuits. &nbsp;Over the next nine years Donna worked in and
                                                         around the public school system as a Substitute Teacher, Administrative Clerk for
                                                         ESE, PTA and SAC President, and Additions Volunteer.&nbsp; In 2007,  her son prepared to
                                                         start his college education with Valencia College. Donna decided to return to the
                                                         workforce, and as luck would have it, to her prior family at Valencia College.&nbsp; After
                                                         working with teachers and the school system closely, the Educator Preparation Institute
                                                         has been a good transition and use of her skills. She enjoys her role as  Coordinator
                                                         for EPI and   loves helping students navigate the process of becoming certified teachers!&nbsp;
                                                         Donna has been married 30 years. Her son is a graduate of Florida State University
                                                         and she looks forward to watching him become the man she has raised him to become….self
                                                         sufficient!
                                                      </p>
                                                      
                                                      <h3>Otoniel "Otto" Rodriguez-Perez, Record Specialist</h3>
                                                      
                                                      <p>Otoniel Rodriguez-Perez's journey at Valencia College began in August 2012 as a freshman.
                                                         Otoniel wasn't satisfied with just taking classes; he wanted to be able to help others.
                                                         He decided to work as an Atlas Lab Assistant at the Osceola campus <img alt="Young man" height="136" hspace="0" src="Otto4_000.jpg.html" vspace="0" width="103">and the West campus. While working as a student leader, he was able to achieve his
                                                         Associates in Arts in December 2015. He states that "it is an amazing experience being
                                                         able to grow as a person and as a professional while helping others. I love giving
                                                         students the help they need in order for them to be successful in their academic and
                                                         career life." His work and perseverance paid off earning him a full time temporary
                                                         position at the Valencia College's HR department. This position led him to obtaining
                                                         the Records Specialist position here with the Teacher Preparation and Re-certification
                                                         program. At the same time, Otoniel is studying to earn his Bachelors in Science in
                                                         Information Technology from the University of Central Florida and is  very excited
                                                         to continue to help students realize their dreams of becoming educators. Otoniel Rodriguez-Perez
                                                         always strives to become a better professional each day while helping others achieve
                                                         their academic and professional goals.
                                                      </p>
                                                      
                                                      
                                                      <p><a href="#top">TOP</a>            
                                                      </p>
                                                      
                                                      <h3>Cassandra Barnhill, EPI Instructor</h3>
                                                      
                                                      <p> Cassandra began working at Valencia College in 2007 and loves teaching for the Educator
                                                         Preparation Institute program.&nbsp; She stated, “It is such a rewarding experience to
                                                         share my love and knowledge of teaching with future educators.”&nbsp; &nbsp;After attending
                                                         Valencia for an Associate in Arts degree, she attended the University of Central Florida
                                                         and graduated in 2000 with a Bachelors of Art degree in Psychology. Cassandra began
                                                         teaching first grade in 2002 as she continued her education and received a Master
                                                         of Science degree in Reading from Nova Southeastern University in 2006.&nbsp; She is happily
                                                         married and has a peaceful dog named Peanut.&nbsp; Cassandra loves to read, garden and
                                                         travel.&nbsp; 
                                                      </p>
                                                      
                                                      <h3>&nbsp;</h3>
                                                      
                                                      <h3>Dr. Lisa Bugden, EPI Instructor </h3>
                                                      
                                                      
                                                      <p>Lisa F. Bugden is an Associate Faculty Member for Valencia College (VCC). &nbsp;She also
                                                         holds a Digital Professor Certification from VCC which aids her in teaching fully
                                                         online, hybrid and web-enhanced courses. Lisa teaches several courses for the EPI
                                                         Program, three education courses and the Student Success course for VCC.&nbsp; Previously,
                                                         she was an experienced special education teacher who taught for eight years at the
                                                         middle and high school levels.&nbsp;Lisa completed a Bachelors of Science degree in Exceptional
                                                         Student Education, Specialization in Emotionally Handicap, in 1997 and a Masters of
                                                         Education in Varying Exceptionalities in 1998 at the University of Central Florida
                                                         (UCF).&nbsp; In December, 2007, Lisa received her Doctor of Education degree in Educational
                                                         Leadership also from UCF.&nbsp; Since 2000, Lisa has also taught teacher certification
                                                         and recertification courses as an Adjunct Instructor for Florida Southern College
                                                         at the Orlando, Florida campus and most recently she has taught EDU 325 Classroom
                                                         Organization and Management and EDU 209 Human Development and Learning in a workshop
                                                         format.&nbsp; Additionally, Lisa taught courses in special education for UCF and will be
                                                         teaching EDG 4410 Instructional Strategies and Classroom Management for the fall term.&nbsp;
                                                         Lisa joined the Educator Preparation Institute at Valencia College as an Instructor
                                                         shortly after its inception in the fall of 2006.&nbsp; In 2007, she was a member of the
                                                         EPI Course Redesign Team where the seven hybrid course and two online field experiences
                                                         were standardized for the program. In 2008, Lisa worked to convert six of the seven
                                                         hybrid courses to fully online for EPI Online Course Development Project.&nbsp; Lisa also
                                                         facilitates a professional development course for VCC called Facilitating Online Learning.&nbsp;
                                                         She has served on two Quality Matters reviews at VCC and is currently serving on a
                                                         dissertation committee for a student at Capella University.<a href="#top">TOP</a></p>
                                                      
                                                      <h3>&nbsp;</h3>
                                                      
                                                      
                                                      <h3>&nbsp;</h3>
                                                      
                                                      <h3>Deborah Mullins, EPI Instructor            </h3>
                                                      
                                                      <p>Deborah started with the EPI program in August of 2007 and really enjoys the different
                                                         groups of people she teaches within each course. She stated, "I really like hearing
                                                         about the experiences of the new teachers starting in the classroom. They truly want
                                                         to get started on the right foot and be the best for the sake of their students. It
                                                         is a great feeling to share information and provide the instruction that is needed
                                                         to help them be successful in their new career. Plus, I get to hear new ideas that
                                                         I can try in my own classroom!" Deborah was in the Navy for over 14 years and always
                                                         knew she would teach when she left the military because she said she felt like she
                                                         was just born to help others. Deborah graduated from the University of Central Florida
                                                         in December 2000 with a Master's in Art degree in Varying Exceptionalities and has
                                                         been teaching for Orange County Public Schools for the last 14 years at the high school
                                                         level. In 2003 she earned her Certificate inEducational Leadership from the University
                                                         of Central Florida and hopes to someday open her own school. Deborah has one daughter
                                                         that graduated from Valencia in 2009, 2 adorable mini-dachshunds and the daughter's
                                                         cat. Deborah enjoys traveling, reading, and listening to music.
                                                      </p>
                                                      
                                                      <h3>&nbsp;</h3>
                                                      
                                                      <h3>Joshua Murdock, EPI Instructor            </h3>
                                                      
                                                      <p> Josh Murdock grew up in Central Florida attending Seminole Community College after
                                                         high school.  With over 12 years of higher education experience, he strives to provide
                                                         excellent educational services to the students at Valencia College. He is the Manager
                                                         of Learning Support Services  supervising the Communications, Math, Testing, and Tutoring
                                                         Centers on West Campus. In addition, he is an adjunct professor for the Educator Preparation
                                                         Institute (EPI) Technology course and Student Success professor. He holds a Master
                                                         of Arts in both Educational Leadership for Higher Education and Instructional Technology
                                                         from the University of Central Florida, with a graduate certificate in E-learning.
                                                         He is a millennial who not only loves surfing the internet, but surfing the waves
                                                         every chance he gets. 
                                                      </p>
                                                      
                                                      <p><a href="#top">TO</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/epi-staff.pcf">©</a>
      </div>
   </body>
</html>