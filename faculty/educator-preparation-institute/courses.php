<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>EPI Courses | Educator Preparation Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educator Preparation Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>Courses</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        					
                        <h2>Courses</h2>
                        					
                        <h3>Certificate of Completion</h3>
                        					
                        <p><strong>Description:</strong><br>
                           						The Educator  Preparation Institute (EPI) provides a competency-based program
                           that offers the  individual with a bachelor's degree in a discipline other than education
                           the  preparation to become a classroom teacher.&nbsp; Participants who demonstrate  mastery
                           of the twelve Florida Educator Accomplished Practices and present  passing scores
                           on all required sections of the Florida Teacher Certification  Exams will be awarded
                           a Certificate of Completion.&nbsp; The program components  designated by an EPI prefix
                           provide institutional credit, are not  transferable&nbsp;to a state university, and do
                           not count toward any  degree.&nbsp; Program acceptance is required for participation in
                           the  EPI.&nbsp;
                        </p>
                        					
                        <p>Please contact Otoniel Rodriguez at (407) 582-5581 or email <span>orodriguez40@valenciacollege.edu</span> for questions regarding EPI course registration or Donna Deitrick, Coordinator (407)
                           582-5473 or email ddeitrick@valenciacollege.edu. 
                        </p>
                        
                        					
                        <h3>The Instructional  Process (Module I – 160 Hours):</h3>
                        					
                        <p><strong>These four courses must be completed first.</strong></p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Course Number</th>
                                 								
                                 <th>Course Title</th>
                                 								
                                 <th>Credit Hours</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI 0001</td>
                                 								
                                 <td>Classroom Management</td>
                                 								
                                 <td>3</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI 0002</td>
                                 								
                                 <td>Instructional Strategies</td>
                                 								
                                 <td>3</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI 0003</td>
                                 								
                                 <td>Technology</td>
                                 								
                                 <td>3</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI 0004</td>
                                 								
                                 <td>The Teaching and Learning Process</td>
                                 								
                                 <td>3</td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <h3>Reading  Fundamental (Module II - 45 hours):</h3>
                        					
                        <p><strong>This course can be taken anytime after the Instructional Process Module.</strong><br>
                           					
                        </p>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Course Number </th>
                                 								
                                 <th>Course Title </th>
                                 								
                                 <th>Credit Hours</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI 0010</td>
                                 								
                                 <td>Foundations of Research-Based Practices in Reading</td>
                                 								
                                 <td>3</td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <p><a href="http://www.readinghorizons.com/">READING HORIZONS</a> 
                        </p>
                        
                        					
                        <h3>Profession Foundations(Module III – 45 hours):</h3>
                        
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Course Number </th>
                                 								
                                 <th>Course Title </th>
                                 								
                                 <th>Credit Hours</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI 0020</td>
                                 								
                                 <td>Professional Foundations</td>
                                 								
                                 <td>2</td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <h3>Diversity in the Classroom (Module IV – 45 hours):</h3>
                        
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Course Number </th>
                                 								
                                 <th>Course Title </th>
                                 								
                                 <th>Credit Hours</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI 0030</td>
                                 								
                                 <td>Diversity</td>
                                 								
                                 <td>2</td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <h3>Field Experience Course (Module V):</h3>
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>Course Number</th>
                                 								
                                 <th>Course Title</th>
                                 								
                                 <th>Credit Hours</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>EPI0950</td>
                                 								
                                 <td>Field Experience </td>
                                 								
                                 <td>4</td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <p><strong>23 total credit hours</strong></p>
                        
                        					
                        <p><a href="http://net5.valenciacollege.edu/schedule/">Search Schedule of EPI Courses</a>*<br>
                           						* search under "education" 
                        </p>
                        
                        
                        
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/courses.pcf">©</a>
      </div>
   </body>
</html>