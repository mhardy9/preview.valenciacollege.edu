<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Educator Preparation Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educator Preparation Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>Educator Preparation Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <h3>Q. How do I know if I qualify for the EPI program?</h3>
                        
                        <blockquote>
                           
                           <p>A. If you hold a bachelor's degree from a regionally accredited college or university
                              and have a  statement of Status of Eligibility from the <a href="http://www.fldoe.org/edcert/steps.asp">Florida Department of Education (FLDOE) </a>stating "You are Eligible," you qualify for our program. Click here to view  a <a href="documents/HowdoIknowifIneedEPI.ppt">valid Statement of Eligibility </a></p>
                           
                        </blockquote>
                        
                        <h3>Q. How do I get my Status of Eligibility?</h3>
                        
                        <blockquote>
                           
                           <p>              A. Visit the <a href="http://www.fldoe.org/edcert/steps.asp">Florida Department of Education</a> and apply for your Status of Eligibility.
                              
                           </p>
                           
                        </blockquote>
                        
                        <h3>Q. What do I need to submit prior to the mandatory enrollment session?</h3>
                        
                        <blockquote>
                           
                           <p>A. The following paperwork must be completed and presented at the EPI Enrollment Session:
                              
                           </p>
                           
                           <ul>
                              
                              <li>Statement of Status of Eligibility from FLDOE </li>
                              
                              <li>College Application needs to be completed before attending an enrollment session </li>
                              
                              <li>EPI Application</li>
                              
                              <li>FEAPs Pre-Assessment</li>
                              
                              <li>Short Biography Statement</li>
                              
                              <li>EPI Education Plan</li>
                              
                              <li>Official Transcripts reflecting a minimum overall GPA of 2.5 must be submitted before
                                 beginning the EPI program
                              </li>
                              
                              <li>Passing FTCE score report for the Basic Skills General Knowledge Exam</li>
                              
                              <li><a href="steps.html">Admission Checklist</a></li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        <h3>Q. How do I have international transcripts evaluated?</h3>
                        
                        <blockquote>
                           
                           <p>A. You will need to have international transcripts evaluated by a<a name="Foreign_Credential_Evaluation_Agencies" id="Foreign_Credential_Evaluation_Agencies"><strong> Foreign Credential Evaluation Agency</strong></a><strong> </strong></p>
                           
                           <p>Students who have pursued secondary school studies outside of the U.S. must submit
                              official records such as transcripts, diplomas and/or standardized exam results that
                              are equivalent to a U.S. high school diploma. Secondary school records that are not
                              issued in English must be accompanied by an official English translation.
                           </p>
                           
                           <p>Students who have completed college/university coursework at an institution outside
                              of the U.S. must have their transcripts/diplomas evaluated by <a href="http://ies.aacrao.org/">AACRAO</a>, members of<a href="http://naces.org/"> NACES</a>, or other professional credential evaluation companies. Students should obtain “course
                              by course” evaluations. The evaluations may be used for transfer credit consideration,
                              fulfilment of Valencia College pre-requisites, or waiving of the foreign language
                              requirement. Students are responsible for paying any fees associated with the credential
                              evaluation process. Students coming from foreign institutions recognized by U.S. regional
                              accreditation bodies do not need to have their credentials evaluated.
                           </p>
                           
                        </blockquote>            
                        
                        <h3>Q. Do you offer online courses?</h3>
                        
                        <blockquote>
                           
                           <p>A. Yes, we offer online and hybrid classes in EPI.</p>
                           
                        </blockquote>
                        
                        <h3>Q. Should I fill out the online admissions application before attending an enrollment
                           session?
                        </h3>            
                        
                        <blockquote>
                           
                           <p>A. Yes, we recommend that you submit your <a href="../admissions-records/forms.html">college application</a> before you attend the enrollment session. It takes approximately five business days
                              to process your application, so if you wish to be able to register as soon as possible,
                              you should fill out the admissions application first. Classes will fill up quickly.
                           </p>
                           
                        </blockquote>
                        
                        <h3>Q. Do I have to submit proof of Florida Residency?</h3>
                        
                        <blockquote>
                           
                           <p>A. Yes. After you have completed the online admissions application, you must upload
                              <a href="../admissions-records/index.html">proof of your Florida Residency</a> on line. It takes approximately five business days to process and must be completed
                              by the deadline or you will be charged out of state tuition. 
                           </p>
                           
                        </blockquote>
                        
                        <h3>Q. How do I view the course listings for each semester?</h3>
                        
                        <blockquote>
                           
                           <p>A. To view all upcoming and available EPI courses follow these instructions: </p>
                           
                           <ol>
                              
                              <li>Click on "Class Schedule" under "Quicklinks" at the top of the web page </li>
                              
                              <li>Select Current or Future Term, leave Discipline blank, and select "EPI" for Subject
                                 Prefix 
                              </li>
                              
                              <li>You may then specify which campus and/or which days you would like to attend classes
                                 
                              </li>
                              
                              <li>Click "Search" </li>
                              
                           </ol>
                           
                           <p>The resulting search shows each course  with all available date ranges. It will also
                              indicate if the class is full.
                           </p>
                           
                        </blockquote>
                        
                        <h3>Q. Do EPI courses transfer to another college or university?</h3>
                        
                        <blockquote>
                           
                           <p>A. No. 
                              
                              
                              The program components designated by an EPI prefix provide institutional credit, are
                              not transferable&nbsp;to a state university, and do not count toward any degree.&nbsp; 
                           </p>
                           
                        </blockquote>
                        
                        <h3> Q. Does the EPI courses meet the requirement for Guidance and Counseling Certification?</h3>
                        
                        <blockquote>
                           
                           <p>A. No. EPI does NOT meet the requirement by the state of Florida for the following
                              certifications:
                           </p>
                           
                           <ul>
                              
                              <li>Reading</li>
                              
                              <li>Speech-Language Impaired</li>
                              
                              <li>Administration/Principal</li>
                              
                              <li>Guidance and Counseling</li>
                              
                              <li>School Psychologist</li>
                              
                              <li>Director of Vocational Education</li>
                              
                           </ul>
                           
                           <p>In order to meet the Florida Department of Education guidelines, you must be attaining
                              a certification in a teaching K-12 field. 
                           </p>
                           
                        </blockquote>
                        
                        <h3><strong>Q. I only  need a couple of courses, can I take only the courses I need?</strong></h3>
                        
                        <blockquote>
                           
                           <p>A. No. You must take all EPI courses when you enroll in the program.</p>
                           
                        </blockquote>
                        
                        <h3>&nbsp;</h3>
                        
                        <blockquote>&nbsp;</blockquote>
                        
                        <h3>Q. If I am currently teaching in the school system, do I still have to take the field
                           experience courses?
                        </h3>
                        
                        <blockquote>
                           
                           <p>A. Yes, current teachers must register for the field experience courses and complete
                              all the assignments for the courses. However, your 30 field experience hours can be
                              completed through your current teaching assignment.
                           </p>
                           
                        </blockquote>
                        
                        <h3>
                           <strong>Q. If I have completed the </strong>Florida Online Reading Professional Development, do I still have to take EPI 0010,
                           
                           
                           
                           Foundations of Research-Based Practices in Reading? 
                        </h3>
                        
                        <blockquote>
                           
                           <p>A. No. FORPD will substitute and meet the requirements for EPI 0010. </p>
                           
                        </blockquote>
                        
                        <h3>
                           <strong>Q. How can I Volunteer in Orange County</strong>? 
                        </h3>
                        
                        <blockquote>
                           
                           <p>A. Take Stock in Children is a great opportunity to mentor a child in middle or high
                              school. Your time involved in the program will attribute to the volunteer hours required
                              for EPI courses. More information about this program can be found at <a href="../valencia-promise/index.html">valenciacollege.edu/valencia-promise</a> 
                           </p>
                           
                        </blockquote>            
                        
                        <h3>Q. Once I have completed the EPI program, what steps do I need to complete to finish
                           the program and get my Professional Educator Certificate?
                        </h3>
                        
                        <blockquote>
                           
                           <p> A. Complete the <a href="documents/COMPLETIONCHECKLIST.pdf">EPI Completion Checklist</a> to make sure all required documentation has been submitted. 
                           </p>
                           
                        </blockquote>
                        
                        <h3>For information on Gainful Employment programs, <a href="../../academics/programs/asdegrees/gainful-employment/index.html">click here</a>. 
                        </h3>
                        
                        <h3>&nbsp;</h3>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/faqs.pcf">©</a>
      </div>
   </body>
</html>