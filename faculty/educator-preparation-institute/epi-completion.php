<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Completion Checklist | Educator Preparation Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/educator-preparation-institute/epi-completion.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educator Preparation Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/educator-preparation-institute/">Educator Preparation Institute</a></li>
               <li>Completion Checklist</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        
                        					
                        <h2>Completion Checklist</h2>
                        					
                        <p>The following <a href="documents/COMPLETIONCHECKLIST.pdf">completion checklis</a><a href="documents/COMPLETIONCHECKLIST.pdf">t</a> and criteria will be used to determine successful completion of the Educator Preparation
                           Institute. 
                        </p>
                        					
                        <p>Admission to the Educator Preparation Institute is limited to eligible applicants
                           only.&nbsp; All students must have a Bachelor’s degree from a regionally accredited institution
                           and a valid statement of eligibility and/or a valid Temporary Certificate from the
                           Florida Department of Education.&nbsp; To complete the EPI program, students must complete
                           the following:
                        </p>
                        
                        					
                        <ol>		
                           						
                           <li>A valid Statement of Eligibility and/or valid Temporary Certificate from the Florida
                              Department of Education must be on file with the EPI office.&nbsp; If your statement of
                              eligibility has expired, you will need to request an updated eligibility statement
                              at <a href="http://www.fldoe.org/edcert/steps.asp">http://www.fldoe.org/edcert/steps.asp</a>.
                           </li>
                           
                           						
                           <li>Successful completion of all EPI coursework.&nbsp; Passing EPI grades must be posted on
                              college transcripts.&nbsp; Grades are posted at the end of each term for Fall, Spring,
                              and Summer.
                           </li>
                           
                           						
                           <li>FTCE Exams must be taken and passed.&nbsp; A copy of all passing scores must be on file
                              with the EPI office.&nbsp; To register for FTCE exams, please visit <a href="http://www.fl.nesinc.com/">http://www.fl.nesinc.com/</a></li>
                           
                           						
                           <li>Complete the <a href="documents/EmploymentVerificationofEPICompleters.pdf">Employment Verification of EPI Completers</a>  and sign the <a href="documents/EPICompleterWaiver.pdf">EPI Completer Waiver. </a> Please submit a copy of both with completion documents.
                           </li>
                           
                           						
                           <li>EPI Portfolio must be submitted to the EPI office and on file for successful completion.
                              EPI Portfolio must be completed and approved. See <a href="documents/PortfolioHandbook2016-2017.pdf"> Portfolio handbook </a> 
                           </li>
                           
                           						
                           <li>Complete the <a href="http://valenciacollege.qualtrics.com/SE?SID=SV_aaAEeO6D0aUr4QA">EPI Post Evaluation Survey</a>.&nbsp; Please complete the online survey and print the last confirmation page of the survey,
                              “your response has been recorded” and submit to the EPI office with your documentation.&nbsp;
                              
                           </li>
                           
                           						
                           <li>Complete the <a href="documents/FEAPsPost-Assessment.pdf">EPI FEAPs Post Assessment.&nbsp;</a> Please submit a copy of your post-assessment to the EPI office with your documentation.&nbsp;
                              
                           </li>
                           
                           						
                           <li>If you have completed the Florida Online Reading Professional Development (FOR-PD)
                              in lieu of EPI 010, a copy of your FOR-PD certificate must be on file with the EPI
                              office. 
                           </li>
                           
                           						
                           <li>After the completion checklist is complete, the EPI director will review your portfolio,
                              sign the CT 133, and send the paperwork to the state. The EPI program will mail a
                              copy of the CT 133, an EPI completion letter, and EPI certificate to you for your
                              records.
                           </li>
                           					
                        </ol>
                        
                        					
                        <p><strong>* After this process has been completed, </strong>and you have already obtained a Temporary Certificate, you may apply for your Professional
                           Certificate and enjoy being an educator of our youth. If you have not been issued
                           a Professional Certificate, you will receive a letter stating you are eligible for
                           a Professional Certificate. When you have been hired and passed the fingerprinting
                           and background check, you will be issued your Professional Certificate. If you wish
                           to request your Professional Certificate before being hired, you may do so, but you
                           will have to be fingerprinted and have a background check as part of the process.
                        </p>
                        
                        					
                        <p>Click here to take the <a href="http://valenciacollege.qualtrics.com/SE?SID=SV_aaAEeO6D0aUr4QA"> EPI Post Evaluation Survey</a></p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/educator-preparation-institute/epi-completion.pcf">©</a>
      </div>
   </body>
</html>