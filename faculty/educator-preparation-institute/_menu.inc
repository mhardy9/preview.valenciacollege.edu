<ul>
	<li><a href="index.php">Educator Preparation Institute</a></li>
	<li class="submenu megamenu">
		<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<div class="menu-wrapper c3">
			<div class="col-md-6">
				<ul>
					<li><a href="about.php">Program Info</a></li>
					<li><a href="steps.php">How to Enroll in EPI</a></li>
					<li><a href="EPI-application.php">EPI Application</a></li>
					<li><a href="handbooks.php">Handbooks</a></li>
					<li><a href="courses.php">Courses</a></li>
					<li><a href="field-experience.php">Field Experience Requirements</a></li>
					<li><a href="tuition.php">Tuition &amp; Cost</a></li>
					<li><a href="EPI-Completion.php">Completion Checklist</a></li>
					<li><a href="course-Schedule.php">Course Schedule</a></li>
					<li><a href="FTCE-Test-Prep-Info.php">FTCE Test Prep Information</a></li>
					<li><a href="faqs.php">Frequently Asked Questions</a></li>
					<li><a href="teacher-recertification.php">Teacher Recertification</a></li>
					<li><a href="epi-staff.php">Program Staff</a></li>
					<li><a href="http://net4.valenciacollege.edu/forms/epi/contact.cfm" target="_blank">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</li>
</ul>