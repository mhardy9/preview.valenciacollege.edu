<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Roster of Instructional Staff | Valencia College</title>
      <!-- Favicons-->
      <link rel="shortcut icon" href="/_resources/img/favicon.ico" type="image/x-icon">
      <link rel="apple-touch-icon" type="image/x-icon" href="/_resources/img/apple-touch-icon-57x57-precomposed.png">
      <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/_resources/img/apple-touch-icon-72x72-precomposed.png">
      <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/_resources/img/apple-touch-icon-114x114-precomposed.png">
      <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/_resources/img/apple-touch-icon-144x144-precomposed.png">
      <!-- BASE CSS -->
      <link href="/_resources/css/base.css" rel="stylesheet">
      <!-- SPECIFIC CSS -->
      <link href="/_resources/css/tabs.css" rel="stylesheet">
      <link rel="stylesheet" href="/_resources/css/oustyles.css" />

	<script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script>

	</head>
				
<body>
<div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Roster of Instructional Staff </h1>
            <p></p>
         </div>
      </div>
<div class="container-fluid margin_60">
            <div class="row">
               <div class="container margin_60" role="main">




<?php include ("d:/dsn/banner_oprod.config"); 
				   
$get_dept 		= 'Accounting';
//$get_dept_esc	=$_POST[$get_dept] ?? '';

$conn = oci_connect($user, $pass, $connection);
if (!$conn) {
   $m = oci_error();
   echo $m['message'], "\n";
   exit;
}




$sql = "SELECT NAME_1, CRDE_LIST, DEGLIST_DEPTLIST, DEGREE_MAJOR_COLL FROM z_faculty_web_report_data WHERE DEPT_DESC = '$get_dept'";
$get_faculty = oci_parse($conn, $sql);
oci_execute($get_faculty);

/*$creds=$CRDE_LIST;
$credsClean=str_replace("\r", "\n", $creds);


echo $credsClean;
exit;*/
echo "<table class='table'><tbody>
    <tr>
      <th scope='col' width='250'>Name</th>
      <th scope='col'>Courses Taught this Term</th>
      <th scope='col'>Relevant Academic Degrees and Course Credits Earned</th>
      <th scope='col'>Other Qualifications</th>
    </tr>";
while ($queryRow = oci_fetch_array($get_faculty, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>";
    foreach ($queryRow as $item){
        echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>";
    }
    echo "</tr>";
}
echo "</table>";
// Close the Oracle connection
oci_close($conn);



?>
		</div>
	</div>
</div>
</body>
</html>