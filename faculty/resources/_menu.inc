<ul>
	<li><a href="index.php">Faculty Resources</a></li>
	<li><a href="checklist.php">Adjunct Checklist</a></li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">By Campus <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="east.php">East</a></li>
			<li><a href="lake-nona.php">Lake Nona</a></li>
			<li><a href="osceola.php">Osceola</a></li>
			<li><a href="west.php">West</a></li>
			<li><a href="winter-park.php">Winter Park</a></li>
		</ul>
	</li>
</ul>