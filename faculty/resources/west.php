<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/resources/west.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/resources/">Resources</a></li>
               <li>Faculty Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>West Campus Faculty Resources</h2>
                        
                        <h3>West Campus Departments</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="../../../west/health/alliedhealth/index.html">Allied Health</a> 
                           </li>
                           
                           <li>
                              <a href="../../../west/arts-and-humanities/index.html">Arts and Humanities</a> 
                           </li>
                           
                           <li>
                              <a href="../../../west/engineering/index.html">Engineering, Computer Programming, &amp; Technology </a> 
                           </li>
                           
                           <li>
                              <a href="../../../west/behavioral-and-social-sciences/index.html">Behavioral and Social Sciences</a> 
                           </li>
                           
                           <li>
                              <a href="../../../west/business-and-hospitality/index.html">Business and Hospitality</a> 
                           </li>
                           
                           <li>
                              <a href="../../../departments/west/communications/index.html">Communications</a> 
                           </li>
                           
                           <li>
                              <a href="../../../west/math/index.html">Mathematics</a> 
                           </li>
                           
                           <li>
                              <a href="../../../west/health/nursingdivision/index.html">Nursing</a> 
                           </li>
                           
                           <li><a href="../../../departments/west/Science/index.html">Science</a></li>
                           
                        </ul>
                        
                        <h3>West Classroom Information
                           
                        </h3>
                        
                        <ul>
                           
                           <li><a href="documents/ClassroomEmergenciesWest.pdf">Classroom Emergencies</a></li>
                           
                           <li>
                              <a href="documents/FacultyResourcesWest.pdf"></a><a href="documents/QuickAnswersWest.pdf">What To Do If...Quick Answers</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h3>West Campus  Resources</h3>
                        
                        <ul>
                           
                           <li><a href="documents/FacultyResourcesWest.pdf">Faculty Resources</a></li>
                           
                           <li><a href="../../../learning-support/testing/index.html">Testing Center</a></li>
                           
                           <li><a href="../../../employees/wordprocessing/orderform_west1.html">Word Processing Order Form</a></li>
                           
                        </ul>
                        
                        
                        <p><a href="index.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>FACULTY EVENTS</h3>
                        
                        
                        
                        <div data-id="whats_your_spiritual_rock">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/whats_your_spiritual_rock" target="_blank">What's Your Spiritual Rock?  at Winter Park Campus</a><br>
                              
                              <span>10:00 AM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="hispanic_heritage_month">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/hispanic_heritage_month" target="_blank">Hispanic Heritage Month at West Campus Student Services Building (SSB)</a><br>
                              
                              <span>12:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="visiting_author_ira_sukrungruang">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/visiting_author_ira_sukrungruang" target="_blank">Visiting Author Ira Sukrungruang at East Campus Building 3</a><br>
                              
                              <span>1:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="ufit_indoor_soccer_league">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/ufit_indoor_soccer_league" target="_blank">UFit Indoor Soccer League at East Campus Building 6</a><br>
                              
                              <span>6:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="valencia_college_reading_and_writing_conference_2017">
                           
                           
                           <div>
                              Sep<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/valencia_college_reading_and_writing_conference_2017" target="_blank">Valencia College Reading and Writing Conference 2017 at West Campus Building 8 – Special
                                 Events Center</a><br>
                              
                              <span>8:00 AM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/resources/west.pcf">©</a>
      </div>
   </body>
</html>