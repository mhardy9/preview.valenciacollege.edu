<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Faculty Resources | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/resources/osceola.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Resources</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/resources/">Resources</a></li>
               <li>Osceola Campus Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="margin-30 container">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Osceola Campus Faculty Resources</h2>
                        
                        <h3>Osceola Campus Departments</h3>
                        
                        <ul>
                           
                           <li><a href="/academics/departments/math-osceola/">Math Department</a></li>
                           
                           <li><a href="/locations/osceola/">Osceola Campus Home</a></li>
                           
                           <li><a href="/locations/osceola/ucf-at-osc.php">UCF at Osceola</a></li>
                           
                        </ul>
                        
                        <h3>Osceola Classroom Information</h3>
                        
                        <ul>
                           
                           <!-- <li><a href="/osceola/faculty-staff.php">Faculty &amp; Staff Contacts</a></li> -->
                           
                           <li><a href="/locations/osceola/learning-center/">Learning Center</a></li>
                           
                        </ul>
                        
                        <h3>Osceola Resources</h3>
                        
                        <ul>
                           
                           <li><a href="documents/ClassroomOsceolaEmergencies.pdf">Classroom Emergencies</a></li>
                           
                           <li><a href="documents/QuickAnswers_001_Osceola.pdf">"What To Do If..." Quick Answers</a></li>
                           
                           <li><a href="documents/OsceolaPrinting.pdf">Word Processing/Copying Services </a></li>
                           
                        </ul>
                        
                     </div>
                     		
                     
                     <aside class="col-md-3">
                        
                        <h3>FACULTY EVENTS</h3>
                        
                        <div data-id="rob_reedy">
                           
                           <div>
                              Jan
                              <br>
                              <span>26</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/rob_reedy" target="_blank">Rob Reedy  at East Campus Anita S. Wooten Gallery</a>
                              <br>
                              <span>8:30 AM</span>
                              
                           </div>
                           
                        </div>
                        
                        <div data-id="3_in_motion_3920">
                           
                           <div>
                              Jan
                              <br>
                              <span>26</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/3_in_motion_3920" target="_blank">3 in Motion at East Campus Performing Arts Center (PAC)</a>
                              <br>
                              <span>8:00 PM</span>
                              
                           </div>
                           
                        </div>
                        
                        <div data-id="hunger_banquey_hosted_by_valencia_volunteers">
                           
                           <div>
                              Jan
                              <br>
                              <span>29</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/hunger_banquey_hosted_by_valencia_volunteers" target="_blank">Hunger Banquey hosted by Valencia Volunteers at Bldg 5</a>
                              <br>
                              <span>1:00 PM</span>
                              
                           </div>
                           
                        </div>
                        
                        <div data-id="ufit_jiu-jitsu">
                           
                           <div>
                              Jan
                              <br>
                              <span>29</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/ufit_jiu-jitsu" target="_blank">UFIT Jiu-Jitsu at Osceola Campus</a>
                              <br>
                              <span>3:00 PM</span>
                              
                           </div>
                           
                        </div>
                        
                        <div data-id="ufit_basketball_league_4419">
                           
                           <div>
                              Jan
                              <br>
                              <span>29</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/ufit_basketball_league_4419" target="_blank">UFIT Basketball League at East Campus Building 6</a>
                              <br>
                              <span>6:00 PM</span>
                              
                           </div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/resources/osceola.pcf">©</a>
      </div>
   </body>
</html>