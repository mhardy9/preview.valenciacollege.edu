<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/resources/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li>Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <ul>
                           
                           <li>
                              
                              <p><a href="checklist.html">Adjunct Checklist</a></p>
                              
                           </li>
                           
                           
                           <li>
                              
                              <p><a href="index.html#toolbox">College-wide Resources</a> (below)
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="east/index.html">East Campus Resources</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="lake-nona/index.html">Lake Nona Campus Resources</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="osceola/index.html">Osceola Campus Resources</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="west/index.html">West Campus Resources</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="winter-park/index.html">Winter Park Resources</a></p>
                              
                           </li>
                           
                        </ul>
                        
                        <p>&nbsp;<a name="toobox" id="toobox"></a></p>
                        
                        <h2>College-wide Resources</h2>
                        
                        
                        <h3>College Information</h3>
                        
                        <ul>
                           
                           <li><a href="../../calendar/index.html">Important Dates/College Calendar</a></li>
                           
                           <li><a href="../../calendar/FinalExam.html">Final Exam Schedule</a></li>
                           
                           <li><a href="../../general-counsel/policy/index.html">College Policies</a></li>
                           
                           <li><a href="../../office-for-students-with-disabilities/default.html">Office for Students with Disabilities</a></li>
                           
                           <li><a href="../../security/parking.html">Parking Decals and Regulations</a></li>
                           
                           <li><a href="../../refund/index.html">Student Refund Information</a></li>
                           
                           <li><a href="../../competencies/index.html">Valencia Core Competencies</a></li>
                           
                        </ul>
                        
                        
                        <h3>Adjunct Faculty Development</h3>
                        
                        <ul>
                           
                           <li><a href="../development/programs/resources/index.html">Adjunct Faculty Development</a></li>
                           
                           <li><a href="../development/index.html">Faculty Development</a></li>
                           
                        </ul>
                        
                        
                        <h3>Contracts and Payroll</h3>
                        
                        <ul>
                           
                           <li><a href="../../HR/compensation.html">Payroll Information</a></li>
                           
                           <li><a href="../../human-resources/index.html">Human Resources</a></li>
                           
                        </ul>
                        
                        
                        <h3>Classroom Information</h3>
                        
                        <ul>
                           
                           <li><a href="documents/FacultyConduct.pdf" target="_blank">Faculty Conduct</a></li>
                           
                           <li><a href="documents/FirstDayChecklist.pdf" target="_blank">First Day Checklist</a></li>
                           
                           <li><a href="documents/ReqClassMeetOffCampus.pdf" target="_blank">Request for Class to Meet Off Campus</a></li>
                           
                           <li><a href="documents/ReqForGuestSpeaker.pdf" target="_blank">Request for Guest Speaker</a></li>
                           
                           <li><a href="documents/StarboardSoftwareandPen.pdf" target="_blank">Starboard Software and Pen</a></li>
                           
                           <li><a href="documents/SyllabusInfo.pdf" target="_blank">Syllabus Information</a></li>
                           
                           <li>
                              <a href="documents/SampleSyllabus.pdf" target="_blank">Sample Syllabus</a> 
                           </li>
                           
                           <li><a href="../../office-of-information-technology/learning-technology-services/services/classroom-technology/index.html">Smart and Bright Classrooms</a></li>
                           
                           <li><a href="../../office-of-information-technology/tss/cts/tierclass/index.html">Tier Classrooms</a></li>
                           
                        </ul>
                        
                        
                        <h3>Resources</h3>
                        
                        <ul>
                           
                           <li><a href="../../students/locations-store/index.html">Bookstore</a></li>
                           
                           <li><a href="../development/centers/index.html">Centers for Teaching / Learning Innovation</a></li>
                           
                           <li><a href="documents/AcademicStudentConflictResolutionProcedure.pdf" target="_blank">Conflict Resolution Procedure</a></li>
                           
                           <li><a href="../../library/index.html">Library</a></li>
                           
                           <li>Faculty Support
                              
                              <ul>
                                 
                                 <li><a href="https://valenciacollege.zendesk.com/forums/20987400-Faculty-Information" target="_blank">Faculty FAQ</a></li>
                                 
                                 <li><a href="../development/documents/Faculty-Lifemap-Guide.pdf" target="_blank">LifeMap Planner (PDF)</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><a href="../../about/ferpa/index.html">FERPA</a></li>
                           
                           <li><a href="../../office-for-students-with-disabilities/default.html">Office for Students With Disabilities</a></li>
                           
                           <li>Proctored Testing--Please visit the following websites
                              
                              <ul>
                                 
                                 <li><a href="http://www.distancelearn.org/" target="_blank">Florida Distance Learning Association </a></li>
                                 
                                 <li><a href="http://www.ncta-testing.org/cctc/find.php" target="_blank">National College Testing Association</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><a href="http://valenciacollege.edu/facultystaff/room-request.cfm">Room Request Form </a></li>
                           
                           <li><a href="../../students/testing/index.html">Testing Center</a></li>
                           
                           <li><a href="../../learning-support/tutoring/index.html">Tutoring Services</a></li>
                           
                           <li>
                              <a href="../../employees/wordprocessing/index.html">Word Processing Order Form </a>            
                           </li>
                           
                        </ul>
                        
                        <p><a href="index.html#top">TOP</a></p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>FACULTY EVENTS</h3>
                        
                        
                        
                        <div data-id="whats_your_spiritual_rock">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/whats_your_spiritual_rock" target="_blank">What's Your Spiritual Rock?  at Winter Park Campus</a><br>
                              
                              <span>10:00 AM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="hispanic_heritage_month">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/hispanic_heritage_month" target="_blank">Hispanic Heritage Month at West Campus Student Services Building (SSB)</a><br>
                              
                              <span>12:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="visiting_author_ira_sukrungruang">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/visiting_author_ira_sukrungruang" target="_blank">Visiting Author Ira Sukrungruang at East Campus Building 3</a><br>
                              
                              <span>1:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="ufit_indoor_soccer_league">
                           
                           
                           <div>
                              Sep<br>
                              <span>21</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/ufit_indoor_soccer_league" target="_blank">UFit Indoor Soccer League at East Campus Building 6</a><br>
                              
                              <span>6:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="valencia_college_reading_and_writing_conference_2017">
                           
                           
                           <div>
                              Sep<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/valencia_college_reading_and_writing_conference_2017" target="_blank">Valencia College Reading and Writing Conference 2017 at West Campus Building 8 – Special
                                 Events Center</a><br>
                              
                              <span>8:00 AM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/resources/index.pcf">©</a>
      </div>
   </body>
</html>