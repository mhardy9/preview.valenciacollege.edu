<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Adjunct Checklist | Faculty Resources | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/resources/checklist.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Resources</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/resources/">Resources</a></li>
               <li>Adjunct Checklist</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        
                        					
                        <h2>Adjunct Faculty College-wide Checklist</h2>
                        					
                        <p>Welcome to Valencia College! We are so glad you have joined us as an adjunct this
                           term. Each adjunct faculty member brings with them a wealth of real world knowledge,
                           this knowledge is so important to our students’ learning. As we begin this term we
                           have created this checklist to help you become more familiar with Valencia.
                        </p>
                        
                        					
                        <h3>Administrative Issues</h3>
                        					
                        <ul>
                           						
                           <li><a href="http://valenciacollege.edu/faculty/forms/credentials/">Credential verification completed</a></li>
                           						
                           <li>Become familiar with <a href="/about/ferpa/">FERPA</a>, HIPPA and <a href="/students/office-for-students-with-disabilities/faculty-toolbox.php">ADA</a> protocols
                              							
                              <ul>
                                 								
                                 <!-- <li><a href="/about/ferpa/index.php#faqs">FERPA faqs and quiz</a></li> -->
                                 								
                                 <li><a href="http://www.ed.gov/policy/gen/guid/fpco/ferpa/safeschools/index.html">FERPA Safe Schools </a></li>
                                 								
                                 <li><a href="/students/office-for-students-with-disabilities/">Office for Students with Disabilities</a></li>
                                 							
                              </ul>
                              						
                           </li>
                           						
                           <li><a href="documents/ClassroomBehavioralInterventionandSupportFINAL.pdf">Classroom Behavioral Intervention and Support </a></li>
                           						
                           <li>Know about <a href="/students/security/parking.php">parking</a>, <a href="/students/security/">security</a>, and access issues.
                           </li>
                           						
                           <li><a href="/students/security/reporting.php">Be familiar with procedures for reporting crime and emergencies</a></li>
                           						
                           <li>Proctored Testing--Please visit the following websites
                              							
                              <ul>
                                 								
                                 <li><a href="http://www.distancelearn.org/">Florida Distance Learning Association</a></li>
                                 								
                                 <li><a href="http://www.ncta-testing.org/cctc/find.php">National College Testing Association</a></li>
                                 							
                              </ul>
                              						
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>Before You Start: 2 Weeks Out</h3>
                        					
                        <ul>
                           						
                           <li><a href="/employees/human-resources/about-valencia.php">Review the Organizational Chart</a></li>
                           						
                           <li><a href="/students/security/parking.php">Get Your Parking Sticker</a></li>
                           						
                           <li><a href="/academics/calendar/">Be Familiar with College and Campus Calendar</a></li>
                           						
                           <li>
                              							<a href="http://catalog.valenciacollege.edu/academicpoliciesprocedures/">Understand the college attendance, grading, final exam, and graduation policies</a> (located in the Catalog under Academic Policies and Procedures) 
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>Before You Start: 1 Week Out</h3>
                        					
                        <ul>
                           						
                           <li><a href="/about/support/howto/">Create Your Atlas Account </a></li>
                           						
                           <li>
                              							<a href="https://atlas.valenciacollege.edu/">Log in to Atlas </a>(Please email your Dean once you are logged in!) 
                           </li>
                           						
                           <li><a href="documents/ObtainingYourSchedule.pdf">Check Your Class Schedule</a></li>
                           						
                           <li><a href="documents/PrintingYourClassRolls.pdf">Print Your Class Rolls</a></li>
                           					
                        </ul>
                        
                        					
                        <h3>Teaching and Learning: Just the Facts</h3>
                        					
                        <ul>
                           						
                           <li>Course Enrollment and Drop/Add Procedures how to: </li>
                           						
                           <ul>
                              							
                              <li><a href="https://valenciacollege.zendesk.com/hc/en-us/articles/202579114-Processing-No-Show-Students">Process No Show Students </a></li>
                              							
                              <li><a href="https://valenciacollege.zendesk.com/hc/en-us/articles/202579054-Withdrawing-a-Student">Withdraw Students</a></li>
                              							
                              <li><a href="documents/EmailingEntireClass.pdf">Email your entire class </a></li>
                              							
                              <li><a href="https://valenciacollege.zendesk.com/hc/en-us/articles/202579124-Sending-an-Interim-Progress-Notification">Send an "interim progress" notification</a></li>
                              							
                              <li><a href="https://valenciacollege.zendesk.com/hc/en-us/articles/202579094-Processing-Below-C-Notifications">Process a below "C" notification</a></li>
                              							
                              <li><a href="https://valenciacollege.zendesk.com/hc/en-us/articles/202579104-Inputting-Final-Grades">Input your final grades</a></li>
                              
                              							
                              <li>Meet your class off campus--Now located in Atlas under Valencia Forms</li>
                              						
                           </ul>
                           						
                           <li>Classroom Issues
                              							
                              <ul>
                                 								
                                 <li>Student recruitment and retention guidelines</li>
                                 								
                                 <li>Grading protocols and final grade procedures </li>
                                 							
                              </ul>
                              						
                           </li>
                           						
                           <li>Auxiliary Resources
                              							
                              <ul>
                                 								
                                 <li><a href="/employees/word-processing/">Copies (Printing and Typing) for your class </a></li>
                                 								
                                 <!-- <li><a href="http://valenciacollege.edu/facultystaff/audio-visual/">Audio-Visual Procedures</a></li> -->
                                 							
                              </ul>
                              						
                           </li>
                           						
                           <li>Review Valencia's Learning College Mission.
                              							
                              <ul>
                                 								
                                 <li><a href="/faculty/development/coursesResources/">Learning Centered Reference Guide</a></li>
                                 								
                                 <li><a href="/about/learning-centered-initiative/">Implementing the Learning College Concept: Challenges and Opportunities</a></li>
                                 							
                              </ul>
                              						
                           </li>
                           						
                           <li>Get acquainted with <a href="/faculty/development/">Faculty Development</a> Resources.
                           </li>
                           						
                           <li>Discover what Valencia's <a href="/faculty/development/programs/resources/certificateProgram.php">Associate Faculty Program</a> can do for you!
                           </li>
                           						
                           <li>Know procedures for <a href="documents/AcademicStudentConflictResolutionProcedure.pdf">Conflict Resolution</a>. 
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>General Instructional Issues</h3>
                        					
                        <ul>
                           						
                           <li><a href="/about/about-valencia/vision.php">Valencia Mission: A Learning-Centered College</a></li>
                           						
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/Reporting/internal/facts.php">Demographics, Lifestyles, Needs of Students Probable Class Size</a></li>
                           						
                           <li><a href="/admissions/admissions-records/">Course enrollment procedures</a></li>
                           						
                           <li><a href="/students/business-office/policies.php">Drop/add procedures</a></li>
                           						
                           <li><a href="/students/advising-counseling/">Student Counseling protocols</a></li>
                           					
                        </ul>
                        
                        					
                        <h3>Student Support Resources</h3>
                        					
                        <ul>
                           						
                           <li><a href="/students/library/">Library</a></li>
                           						
                           <li><a href="/students/labs/">Labs</a></li>
                           						
                           <li><a href="/students/learning-support/">Tutoring services</a></li>
                           						
                           <li><a href="/students/office-for-students-with-disabilities/">Office for Students with Disabilities<br>
                                 							</a></li>
                           					
                        </ul>
                        
                        					
                        <h3>Information Resources</h3>
                        					
                        <ul>
                           						
                           <li><a href="/students/catalog/">College Catalog</a></li>
                           						
                           <li><a href="/academics/calendar/">Meetings Calendar</a></li>
                           						
                           <li>Newsletters: <a href="http://thegrove.valenciacollege.edu" target="_blank" title="The Grove">The Grove</a><br>
                              						
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>Department/Division/Program Specific Issues</h3>
                        					
                        <p>These issues will be covered in your orientation as well as on your department's website.
                           Please visit your department website <a href="west.php">West Campus</a>, <a href="east.php">East Campus</a>, <a href="lake-nona.php">Lake Nona Campus</a>, <a href="winter-park.php">Winter Park Campus</a>, or <a href="osceola.php">Osceola Campus</a> or contact your dean for the following information:
                        </p>
                        					
                        <ul>
                           						
                           <li>Role of assigned course in overall curriculum</li>
                           						
                           <li>Acutal course syllabus, outline, standard learning objectives</li>
                           						
                           <li>Office, storage space, work areas, meeting space with students</li>
                           						
                           <li> Mailbox, distribution system</li>
                           						
                           <li>Specialized facilities, equipment, resources</li>
                           						
                           <li> Information resources: meetings and workshops</li>
                           						
                           <li>Critical issues facing academic </li>
                           					
                        </ul>
                        
                        				
                     </div>
                     				
                     <aside class="col-md-3">
                        
                        					
                        <h3>FACULTY EVENTS</h3>
                        
                        
                        					
                        <div data-id="whats_your_spiritual_rock">
                           
                           						
                           <div>
                              							Sep<br>
                              							<span>21</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/whats_your_spiritual_rock" target="_blank">What's Your Spiritual Rock?  at Winter Park Campus</a><br>
                              
                              							<span>10:00 AM</span>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <div data-id="hispanic_heritage_month">
                           
                           						
                           <div>
                              							Sep<br>
                              							<span>21</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/hispanic_heritage_month" target="_blank">Hispanic Heritage Month at West Campus Student Services Building (SSB)</a><br>
                              
                              							<span>12:00 PM</span>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <div data-id="visiting_author_ira_sukrungruang">
                           
                           						
                           <div>
                              							Sep<br>
                              							<span>21</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/visiting_author_ira_sukrungruang" target="_blank">Visiting Author Ira Sukrungruang at East Campus Building 3</a><br>
                              
                              							<span>1:00 PM</span>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <div data-id="ufit_indoor_soccer_league">
                           
                           						
                           <div>
                              							Sep<br>
                              							<span>21</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/ufit_indoor_soccer_league" target="_blank">UFit Indoor Soccer League at East Campus Building 6</a><br>
                              
                              							<span>6:00 PM</span>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <div data-id="valencia_college_reading_and_writing_conference_2017">
                           
                           						
                           <div>
                              							Sep<br>
                              							<span>23</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/valencia_college_reading_and_writing_conference_2017" target="_blank">Valencia College Reading and Writing Conference 2017 at West Campus Building 8 – Special
                                 Events Center</a><br>
                              
                              							<span>8:00 AM</span>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        				
                     </aside>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/resources/checklist.pcf">©</a>
      </div>
   </body>
</html>