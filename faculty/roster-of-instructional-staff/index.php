<!DOCTYPE HTML>
<html lang="en-US">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content=", college, school, educational">
	<meta name="description" content=" | Valencia College">
	<meta name="author" content="Valencia College">
	<title>Roster of Instructional Staff | Valencia College</title>
	<!-- Favicons-->
	<link rel="shortcut icon" href="/_resources/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="/_resources/img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/_resources/img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/_resources/img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/_resources/img/apple-touch-icon-144x144-precomposed.png">
	<!-- BASE CSS -->
	<link href="/_resources/css/base.css" rel="stylesheet">
	<!-- SPECIFIC CSS -->
	<link href="/_resources/css/tabs.css" rel="stylesheet">
	<link rel="stylesheet" href="/_resources/css/oustyles.css"/>
	<script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script>

	<style type="text/css">
		.wrapcolumns {
			-webkit-columns: 4 150px;
			-moz-columns: 4 150px;
			columns: 4 150px;
			-webkit-column-gap: 2em;
			-moz-column-gap: 2em;
			column-gap: 2em;
		}
		
		li {
			border-right: 1px solid #dee2e6;
			margin-left: -25px;
		}
	</style>

</head>

<body>
	<div class="sub-header bg-interior">
		<div id="intro-txt">
			<h1>Roster of Instructional Staff </h1>
			<p></p>
		</div>
	</div>
	<div class="container-fluid margin-60">
		<div class="row">
			<div class="container margin-60" role="main">

				<?php 
// Open the Oracle connection
include ("d:/dsn/banner_oprod.config"); 
$conn = oci_connect($user, $pass, $connection);
if (!$conn) {
   $m = oci_error();
   echo $m['message'], "\n";
   exit;
}
				   
// Query Banner Database for department list
$sql_depts = "SELECT DISTINCT DEPT_DESC FROM z_faculty_web_report_data ORDER BY DEPT_DESC";
$get_depts = oci_parse($conn, $sql_depts);
oci_execute($get_depts);

// Replace ampersand symbols with html encoded symbols so they will work in URL
function fnCleanDept($inText) {
	$badText = array("&");
	$goodText = array("%26");
	$outText = str_replace($badText, $goodText, $inText);
	return($outText);
} 

//print department list
echo '<div class="wrapcolumns">';
echo "<ul>";		   
while ($queryRow = oci_fetch_array($get_depts, OCI_ASSOC+OCI_RETURN_NULLS)) {
    foreach ($queryRow as $item){
        echo "<li><a href='index.php?dept=" . fnCleanDept($item) . "#start'>" . $item . "</a></li>";
    }
}
echo "</ul>";
echo '</div>';

// Get the department name from the URL and default to accounting if it's missing
if (isset($_GET['dept'])) {
	$get_dept = $_GET['dept'];
} else {
	$get_dept = 'Accounting';
}

// Query Banner Database for specific department
$sql = "SELECT NAME_1, CRDE_LIST, DEGLIST_DEPTLIST, COMMENTS FROM z_faculty_web_report_data WHERE DEPT_DESC = '$get_dept'";
$get_faculty = oci_parse($conn, $sql);
oci_execute($get_faculty);

// replace carriage returns and employee status description with <br>
function fnClean($inText) {
	$badText = array("\r\n", "(full", "(part");
	$goodText = array("<br>", "<br>(full", "<br>(part");
	$outText = str_replace($badText, $goodText, $inText);
	return($outText);
} 	

//print roster for selected department
echo "<a name='start'></a><h2 style='margin-top:60px'>" . $get_dept . "</h2>";
				   
echo "<table class='table'><thead>
    <tr>
      <th scope='col' width='250'>Name</th>
      <th scope='col'>Courses Taught this Term</th>
      <th scope='col'>Relevant Academic Degrees and Course Credits Earned</th>
      <th scope='col'>Other Qualifications</th>
    </tr></thead><tbody>";
while ($queryRow = oci_fetch_array($get_faculty, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo '<tr>
		<td nowrap>'.fnClean($queryRow["NAME_1"]).'</td>
		<td nowrap>'.fnClean($queryRow["CRDE_LIST"]).'</td>
		<td nowrap>'.fnClean($queryRow["DEGLIST_DEPTLIST"]).'</td>
		<td>'.$queryRow["COMMENTS"].'</td>
		</tr>';
    }
echo "</tbody></table>";
// Close the Oracle connection
oci_close($conn);
?>
			</div>
		</div>
	</div>
</body>

</html>