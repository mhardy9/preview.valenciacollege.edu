<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/representatives/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li>Representatives</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Faculty Council Representatives</h2>
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>East Campus</h3>
                              
                              <p><strong>President of the East Campus Assembly: </strong>Anna Saintil
                              </p>
                              
                              <p> <strong>Vice President of the  East Campus Assembly: </strong>Diane Dalrymple
                              </p>
                              
                              <p> <strong>Representative At-Large  of the East Campus Assembly: </strong>Clay Holliday
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>West Campus</h3>
                              
                              <p><strong>President of the West  Campus Assembly:</strong> Dave Curtis
                              </p>
                              
                              <p> <strong>Vice President of the  West Campus Assembly:</strong> Patrick Bartee
                              </p>
                              
                              <p> <strong>Representative At-Large of  the West Campus Assembly:</strong> Diane Gomez
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Osceola Campus</h3>
                              
                              <p><strong>President of the Osceola  Campus Assembly:</strong> Jenna Settles
                              </p>
                              
                              <p> <strong>Vice President of the  Osceola Campus Assembly: </strong>Rick  Dexter
                              </p>
                              
                              <p> <strong>Representative At-Large  of the Osceola Campus Assembly: </strong>Andrea Foley
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Winter Park Campus</h3>
                              
                              <p><strong>President of the Winter  Park Campus Assembly: </strong>Upasana Santra
                              </p>
                              
                              <p> <strong>Vice President of the  Winter Park Campus Assembly: </strong>Jason Balserait
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Lake Nona Campus</h3>
                              
                              <p><strong>President of the Lake  Nona Campus Assembly:</strong> April Raneri
                              </p>
                              
                              <p> <strong>Vice President of the Lake Nona Campus  Assembly:</strong> Jackie Starren
                              </p>
                              
                              <p><strong>Representative At-Large of  the Lake Nona Campus Assembly: </strong>LaVonda Walker
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Poinciana Campus</h3>
                              
                              <p><strong>President of the  Poinciana Campus Assembly:</strong> Michael Robbins
                              </p>
                              
                              <p> <strong>Vice President of the Lake Nona Campus  Assembly:</strong> Eunice Laurent
                              </p>
                              
                              <p><strong>Representative At-Large of  the Lake Nona Campus Assembly: </strong>Sam Murugan
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="http://site.valenciacollege.edu/faculty-association/" target="_blank">Meeting Summaries &amp; Discussions</a>
                        
                        <a href="http://net4.valenciacollege.edu/forms/faculty/association/contact.cfm" target="_blank">Contact</a>
                        
                        
                        
                        
                        <h3>Association News</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/upcoming-meetings-2/" target="_blank">Upcoming Meetings</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/a-faculty-heads-up-october-2017/" target="_blank">A Faculty Heads Up — October 2017</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/students-may-now-apply-for-the-jack-kent-cooke-foundation-scholarship/" target="_blank">Students May Now Apply for the Jack Kent Cooke Foundation Scholarship</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/faculty-workshops/" target="_blank">Faculty Development Opportunities</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/getting-to-know-learning-support/" target="_blank">Getting to Know Learning Support</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>Association Events</h3>
                        
                        There are no events at this time.
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/representatives/index.pcf">©</a>
      </div>
   </body>
</html>