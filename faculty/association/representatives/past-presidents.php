<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/representatives/past-presidents.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li><a href="/faculty/association/representatives/">Representatives</a></li>
               <li>Faculty Association</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Past Presidents</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Name</div>
                                 
                                 <div data-old-tag="td">Term  of Service</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Neal Phillips</p>
                                    
                                    <p>Suzette Dohany</p>
                                    
                                    <p>Carl Creasman </p>
                                    
                                    <p>Deidre Holmes DuBois  </p>
                                    
                                    <p>Robert McCaffrey </p>
                                    
                                    <p>Robert Gessner</p>
                                    
                                    <p>Lisa Macon</p>
                                    
                                    <p>Jean Marie Fuhrman</p>
                                    
                                    <p>Michael Shugg</p>
                                    
                                    <p>Rose Watson</p>
                                    
                                    <p>Tom Byrnes</p>
                                    
                                    <p>Amy Bosley</p>
                                    
                                    <p>Chris Borglum</p>
                                    
                                    <p>Michael Shugg</p>
                                    
                                    <p>Lisa Armour</p>
                                    
                                    <p>Ron Colburn</p>
                                    
                                    <p>Linda Anthon</p>
                                    
                                    <p>Gustavo Morales </p>
                                    
                                    <p>David Rogers </p>
                                    
                                    <p>Bill Castelanno </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>2016 - 2017</p>
                                    
                                    <p>2015 - 2016</p>
                                    
                                    <p>2014 - 2015</p>
                                    
                                    <p>2013 - 2014</p>
                                    
                                    <p>2012 - 2013</p>
                                    
                                    <p>2011 - 2012</p>
                                    
                                    <p>2010 - 2011</p>
                                    
                                    <p>2009 - 2010</p>
                                    
                                    <p>2008 - 2009</p>
                                    
                                    <p>2007 - 2008</p>
                                    
                                    <p>2006 - 2007</p>
                                    
                                    <p>2005 - 2006</p>
                                    
                                    <p>2004 - 2005</p>
                                    
                                    <p>2003 - 2004</p>
                                    
                                    <p>2002 - 2003</p>
                                    
                                    <p>2001 - 2002</p>
                                    
                                    <p>2000 - 2001</p>
                                    
                                    <p>1999 - 2000 </p>
                                    
                                    <p>1998 - 1999 </p>
                                    
                                    <p>1997 - 1998 </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Presidents Prior to 1997 </strong></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Ivan Applebaum </p>
                                    
                                    <p>Roberta Vandermast </p>
                                    
                                    <p>Darryl Thorne</p>
                                    
                                    <p>Dennis Jenkins </p>
                                    
                                    <p>Mert Hollister </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Philip Bishop</p>
                                    
                                    <p>Kevin Mulholland</p>
                                    
                                    <p>Stan Melnick</p>
                                    
                                    <p>Ruth Prather</p>
                                    
                                    <p>Carol Ancona   </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="http://site.valenciacollege.edu/faculty-association/" target="_blank">Meeting Summaries &amp; Discussions</a>
                        
                        <a href="http://net4.valenciacollege.edu/forms/faculty/association/contact.cfm" target="_blank">Contact</a>
                        
                        
                        
                        
                        <h3>Association News</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/upcoming-meetings-2/" target="_blank">Upcoming Meetings</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/a-faculty-heads-up-october-2017/" target="_blank">A Faculty Heads Up — October 2017</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/students-may-now-apply-for-the-jack-kent-cooke-foundation-scholarship/" target="_blank">Students May Now Apply for the Jack Kent Cooke Foundation Scholarship</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/faculty-workshops/" target="_blank">Faculty Development Opportunities</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/getting-to-know-learning-support/" target="_blank">Getting to Know Learning Support</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>Association Events</h3>
                        
                        There are no events at this time.
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/representatives/past-presidents.pcf">©</a>
      </div>
   </body>
</html>