<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/committees/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li>Committees</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Work Teams</h2>
                        
                        <p>Valencia's vision for collaboration in the governance of the College is the collaborative
                           process of the college making courageous, wise choices that moves our work forward.
                           Using the strengths of diverse individuals and giving a voice to various constituencies,
                           everyone contributes with good faith and effort, transparency and integrity. 
                        </p>
                        
                        <p>The Faculty Association plays a key role by placing representatives of its members
                           on the many work teams of the College. Some are created by the Faculty Council while
                           others emerge from others areas such as Student Services or HR. Some work teams have
                           an ongoing process while others will be short-term.
                        </p>
                        
                        <p>If you have interest, questions or concerns about the work of these teams, please
                           contact any of the members. If you wish to serve, keep watching your email for opportunities
                           to volunteer for service. 
                        </p>
                        
                        <h3>Call for Faculty</h3>
                        
                        <p>To request faculty to serve on your work team, please email us at <a href="mailto:DL-FacultyCouncil@valenciacollege.edu">DL-FacultyCouncil@valenciacollege.edu</a></p>
                        
                        <br>
                        
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <h3>Short Term Work Teams </h3> 
                              
                              
                              <div> 
                                 
                                 
                                 <h3>Online Redesign Work Teams</h3> 
                                 
                                 
                                 <div> 
                                    
                                    
                                    <div> 
                                       
                                       
                                       <p>Faculty Preparedness for Online and Hybrid Learning Team</p>
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Liza Schellpfeffer (Co-chair)</li>
                                          
                                          <li>Nicole Spottke</li>
                                          
                                          <li>Susan Gosnell</li>
                                          
                                          <li>Anna Santil</li>
                                          
                                          <li>Wendi Bush</li>
                                          
                                          <li>Carl Creasman</li>
                                          
                                          <li>Erin O'Brien</li>
                                          
                                          <li>Celine Kavalec</li>
                                          
                                       </ul>
                                       
                                       <p>Online Student Preparedness Team</p>
                                       
                                       <p>Faculty Representatives: </p>
                                       
                                       <ul>
                                          
                                          <li>Karen Cowden</li>
                                          
                                          <li>Renee Becker</li>
                                          
                                          <li>Peggy Bivins</li>
                                          
                                          <li>Wendi Bush</li>
                                          
                                          <li>Stephanie Freuler</li>
                                          
                                          <li>Damion Hammock</li>
                                          
                                          <li>Jerry Hensel</li>
                                          
                                          <li>Debra Hollister</li>
                                          
                                          <li>Liza Schellpfeffer</li>
                                          
                                          <li>Katie Shephard</li>
                                          
                                       </ul>
                                       
                                       <p>Online Student Services &amp; Support Team</p>
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Emily Elrod</li>
                                          
                                          <li>Reneva Walker</li>
                                          
                                          <li>Beth King</li>
                                          
                                       </ul>
                                       
                                       <p>Course and Curriculum Design Team</p>
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Erin O'Brien (Co-chair)</li>
                                          
                                          <li>Brian Macon</li>
                                          
                                          <li>Stacey DiLiberto</li>
                                          
                                          <li>Katie Shephard</li>
                                          
                                          <li>Michael Savage</li>
                                          
                                          <li>Shari Koopmann</li>
                                          
                                       </ul>
                                       
                                       <p>Online Data and Evaluation Team</p>
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Jerry Reed</li>
                                          
                                          <li>Neal Phillips</li>
                                          
                                          <li>Jennifer Lawhon</li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Review of Incentive Compontents of Faculty Compensation</h3> 
                                 
                                 
                                 <div> 
                                    
                                    
                                    <div> 
                                       
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Damion Hammock</li>
                                          
                                          <li>Robin Poole</li>
                                          
                                          <li>David Freeman</li>
                                          
                                       </ul>
                                       
                                       
                                    </div> 
                                    
                                 </div> 
                                 
                                 
                                 
                                 
                                 <h3>College ID Task Force</h3> 
                                 
                                 
                                 <div> 
                                    
                                    
                                    <div> 
                                       
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Jerry Reed</li>
                                          
                                          <li>Sylvana Vester</li>
                                          
                                          <li>Mary Margaret Thompson</li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <h3>Internship &amp; Workforce Services Redesign Team</h3> 
                                 
                                 
                                 <div> 
                                    
                                    
                                    <div> 
                                       
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Kristy Pennino</li>
                                          
                                          <li>Beverly Bond</li>
                                          
                                          <li>Christie Miller </li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <h3>Review of Faculty Essential Compentencies</h3> 
                                 
                                 
                                 <div> 
                                    
                                    
                                    <div> 
                                       
                                       
                                       <p>Steering Committee:</p>
                                       
                                       <ul>
                                          
                                          <li>Carl Creasman</li>
                                          
                                          <li>Celine Kavalec</li>
                                          
                                          <li>Adriene Tribble</li>
                                          
                                          <li>Celeste Henry</li>
                                          
                                          <li>Kourtney Baldwin</li>
                                          
                                          <li>Regina Seguin</li>
                                          
                                          <li>Tim Grogan</li>
                                          
                                          <li>Bob Gessner (Dean of Science, West)</li>
                                          
                                          <li>Wendi Dew (AVP, Teaching and Learning)</li>
                                          
                                       </ul>
                                       
                                       <p>Team members:</p>
                                       
                                       <ul>
                                          
                                          <li>Donna Colwell</li>
                                          
                                          <li>Julie Kloft</li>
                                          
                                          <li>Kim Long</li>
                                          
                                          <li>Michele Lima</li>
                                          
                                          <li>Shari Koopmann</li>
                                          
                                          <li>Shawn Pollgreen</li>
                                          
                                          <li>Stanton Reed</li>
                                          
                                          <li>Cecil Battiste</li>
                                          
                                          <li>Deborah Simko</li>
                                          
                                          <li>Jamy Chulak</li>
                                          
                                          <li>Kevin Colwell</li>
                                          
                                          <li>Kristin Abel</li>
                                          
                                          <li>Laura Sessions</li>
                                          
                                          <li>Kristin Bartholomew</li>
                                          
                                          <li>Liza Schellpfeffer</li>
                                          
                                          <li>Manny Ramos</li>
                                          
                                          <li>Rick Dexter</li>
                                          
                                          <li>Ron VonBehren</li>
                                          
                                          <li>Steve Cunningham</li>
                                          
                                          <li>Summer Trazzera</li>
                                          
                                          <li>Tina Tan</li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <h3>Space Mountain Academic Integrity Work Team (under IAC)</h3> 
                                 
                                 
                                 <div> 
                                    
                                    
                                    <div> 
                                       
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Debra Jacobs</li>
                                          
                                          <li>Debra Hollister</li>
                                          
                                          <li>Deidre Holmes DuBois </li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           
                           <div>
                              
                              <h3>Work Directed by Faculty Council</h3>
                              
                              <div>
                                 
                                 
                                 <h3>Student Feedback on Instruction</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Faculty Representatives (two-year appointment):</p>
                                       
                                       <ul>
                                          
                                          <li>Jolene Rhodes (Chair)</li>
                                          
                                          <li>Kris Dougherty</li>
                                          
                                          <li>Dave Curtis</li>
                                          
                                          <li>Raul Valery</li>
                                          
                                          <li>Kathy Pierre</li>
                                          
                                       </ul>
                                       
                                       
                                       <p><a href="SAI-members.html">Membership </a>: <a href="http://site.valenciacollege.edu/faculty-association/Minutes/Forms/AllItems.aspx?RootFolder=/faculty-association/Minutes/Student%20Assessment%20of%20Instruction%20Committee&amp;FolderCTID=0x01200069319A16E10E8649BF433011AD32DAB3&amp;View=%7B987CE2A7-2286-4BB7-A0D8-FA0871C83824%7D">Minutes</a></p>
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <h3>Faculty Association Award for Excellence in Teaching, Counseling &amp; Libraianship</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <p>Faculty Representatives (two-year appointment, rotating in groups of three and then
                                          four):
                                       </p>
                                       
                                       <p><u>2014 – 2015</u></p>
                                       
                                       <ul>
                                          
                                          <li>Shari Koopmann (Chair)</li>
                                          
                                          <li>Diane Gomez</li>
                                          
                                          <li>Betsy Brantley</li>
                                          
                                       </ul>
                                       
                                       <p><u>2015 – 2016</u></p>
                                       
                                       <ul>
                                          
                                          <li>Diane Dalrymple (Chair) </li>
                                          
                                          <li>Stacey DiLiberto</li>
                                          
                                          <li>Allen Waters </li>
                                          
                                       </ul>
                                       
                                       <p><u>2016 – 2017</u></p>
                                       
                                       <ul>
                                          
                                          <li>Jamie Prusak</li>
                                          
                                          <li>Jenna Settles</li>
                                          
                                          <li>Cate McGowan</li>
                                          
                                          <li>Ryan Kasha</li>
                                          
                                          <li>Magdala Emmanuel</li>
                                          
                                          <li>Dennis Hunchuck</li>
                                          
                                       </ul>
                                       
                                       <p>To nominate a faculty member for this award, please click on the following link: 
                                          
                                          
                                          <a href="../../../hr/FAAECTL.html">http://valenciacollege.edu/hr/FAAECTL.cfm</a>. 
                                       </p>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <h3>Instructional Materials Committee</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Faculty Representatives (two-year appointment):
                                          
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Aaron Powell (Co-Chair)</li>
                                          
                                          <li>Nick Bekas (Co-Chair)</li>
                                          
                                          <li>Lynta Thomas </li>
                                          
                                          <li>Karene Best</li>
                                          
                                          <li>John Edwards</li>
                                          
                                          <li>George Brooks</li>
                                          
                                          <li>Patrick Murphy</li>
                                          
                                          <li>Cathy Ferrer</li>
                                          
                                          <li>Jackie Lindbeck</li>
                                          
                                          <li>Andy Ray</li>
                                          
                                          <li>Robin Simmons</li>
                                          
                                       </ul>
                                       
                                       <p><a href="documents/ValenciaCollegePolicy.pdf">Instructional Materials Policy</a>; <a href="documents/InstructionalMaterialsChoiceflow.docx">Instructional Materials Choice Flow</a> 
                                       </p>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <h3>Ongoing Work Teams</h3>
                              
                              <div>
                                 
                                 
                                 <h3>Curriculum &amp; Assessment (Karen Borglum, AVP)</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong><a href="../../../college-curriculum-committee/CommitteeMembership.html">Curriculum Committee</a></strong></p>
                                       
                                       <p>Faculty Representatives (two-year appointment):</p>
                                       
                                       <p><u>2015-2017</u></p>
                                       
                                       <ul>
                                          
                                          <li>Lee Thomas</li>
                                          
                                          <li>Melissa Pedone</li>
                                          
                                          <li>Lisa Macon</li>
                                          
                                          <li>Carin Gordon</li>
                                          
                                          <li>Bonnie Oliver</li>
                                          
                                          <li>Rebecca Edwards Newman</li>
                                          
                                          <li>Storm Russo</li>
                                          
                                          <li>Natali Shulterbrondt</li>
                                          
                                          <li>Veeramutha Rajaravivarma</li>
                                          
                                          <li>Mohua Kar</li>
                                          
                                          <li>Neal Phillips</li>
                                          
                                          <li>Suzette Dohany</li>
                                          
                                          <li>Yolanda Gonzalez</li>
                                          
                                          <li>Aida Diaz</li>
                                          
                                          <li>Joan Alexander</li>
                                          
                                          <li>John Niss</li>
                                          
                                          <li>Armira Shkembi</li>
                                          
                                          <li>Melody Boeringer</li>
                                          
                                          <li>Flora Chisholm</li>
                                          
                                          <li>Leann Hudson</li>
                                          
                                          <li>Anita Kovalsky</li>
                                          
                                          <li>Terry Miller</li>
                                          
                                          <li>Debra Jacobs</li>
                                          
                                          <li>Yasmeen Quadri</li>
                                          
                                          <li>Melissa Sierra</li>
                                          
                                          <li>Betty Wanielista</li>
                                          
                                          <li>Marie Howard</li>
                                          
                                       </ul>
                                       
                                       <p><u>2016-2018</u></p>
                                       
                                       <ul>
                                          
                                          <li>Beverly Bond</li>
                                          
                                          <li>Pam Sandy</li>
                                          
                                          <li>Esther Coombs</li>
                                          
                                          <li>Dawn Sedik</li>
                                          
                                          <li>Chris Klinger</li>
                                          
                                          <li>Raul Valery</li>
                                          
                                          <li>Kristy Pennino</li>
                                          
                                          <li>Lisa Lippitt</li>
                                          
                                          <li>Ralph Jenne</li>
                                          
                                          <li>Regina Seguin</li>
                                          
                                          <li>Nardia Cumberbatch</li>
                                          
                                       </ul>
                                       
                                       <p>Alternative Credentialing Committee</p>
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Tim Grogan</li>
                                          
                                          <li>Dan Dutkofski</li>
                                          
                                          <li>Kevin Mulholland </li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <h3>Student Affairs (Joyce Romano, VP)</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Student Activities Budget Committee</p>
                                       
                                       <p>Faculty Representatives (annual appointment):</p>
                                       
                                       <ul>
                                          
                                          <li>Jocelyn Morales</li>
                                          
                                          <li>Deborah Simko</li>
                                          
                                          <li>John McFarland</li>
                                          
                                          <li>Chris Borglum</li>
                                          
                                       </ul>
                                       
                                       <p>Testing Center Advisory Group </p>
                                       
                                       <p>Valencia’s Testing Center Advisory Group (T.A.G.) meets occasionally in order to collaborate,
                                          create and implement fair and equitable procedures for academic testing centers college-wide.
                                          Currently, T.A.G. is made up of managers, supervisors and testing staff from East,
                                          West, Winter Park, Osceola, and Lake Nona campuses. 
                                       </p>
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Linda Wilson (West)</li>
                                          
                                          <li>Diane Ciesko (East)</li>
                                          
                                          <li>Trymain Rivero (Osceola)</li>
                                          
                                          <li>Linda Firmani (Winter Park)</li>
                                          
                                          <li>Brian Macon (Lake Nona)</li>
                                          
                                       </ul>
                                       
                                       <p>Commencement Committee</p>
                                       
                                       <p> Faculty Representatives (two-year appointment):</p>
                                       
                                       <p><u>2014-2016</u></p>
                                       
                                       <ul>
                                          
                                          <li>Kim Long</li>
                                          
                                       </ul>
                                       
                                       <p><u>2015-2017</u></p>
                                       
                                       <ul>
                                          
                                          <li>Richard Sansone</li>
                                          
                                       </ul>
                                       
                                       <p><em>Note: Ushers &amp; Marshals for Commencement: appointment made each spring for current
                                             year's ceremony.</em></p>
                                       
                                       <p>Calendar and Scheduling Committee</p>
                                       
                                       <p> Faculty Representatives (three-year appointment, rotating in groups of three):</p>
                                       
                                       <p><u>October 2012 – September 2015</u></p>
                                       
                                       <ul>
                                          
                                          <li>Anita Kovalsky, Prof Nursing, West</li>
                                          
                                          <li>Steve Tullo, Counselor, East</li>
                                          
                                          <li>Jenny Hu, Prof Computer Programming &amp; Analysis, Osceola</li>
                                          
                                       </ul>
                                       
                                       <p><u>October 2013- September 2016</u></p>
                                       
                                       <ul>
                                          
                                          <li>Pat Boyle, Prof Humanities, West</li>
                                          
                                          <li>Patrick Murphy, Prof Math, East</li>
                                          
                                          <li>Tina Tan, Prof Speech, West</li>
                                          
                                       </ul>
                                       
                                       <p><u>October 2014- September 2017</u></p>
                                       
                                       <ul>
                                          
                                          <li>Karene Best, Librarian, Osceola</li>
                                          
                                          <li>Dan Dutkofski, Humanities, East</li>
                                          
                                          <li>James Leonard, English, East </li>
                                          
                                       </ul>
                                       
                                       <p>Behavioral Advisory Team</p>
                                       
                                       <p>Faculty Representatives:</p>
                                       
                                       <ul>
                                          
                                          <li>Diane Ashe </li>
                                          
                                          <li>Nancy Rizzo</li>
                                          
                                          <li>Judi Addelston (Alternate) </li>
                                          
                                       </ul>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Business Operations and Finances (Loren Bender, VP)</h3>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <p>Budget and Financial Advisory Group</p>
                                          
                                          <p>Faculty Representatives:</p>
                                          
                                          <ul>
                                             
                                             <li>Rob McCaffrey</li>
                                             
                                             <li>Kristin Abel</li>
                                             
                                             <li>Bonnie Oliver</li>
                                             
                                          </ul>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div>
                                       
                                       
                                       <h3>Institutional Assessment (Laura Blasi, Director)</h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>Learning Outcomes Leaders Team</p>
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <div>
                                          
                                          
                                          <h3>Office of Information Technology (John Slot, Chief Information Officer)</h3>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p>Enterprise Systems Advisory Group (co-led by Patti Smith)</p>
                                                
                                                <p>Faculty Representatives:</p>
                                                
                                                <ul>
                                                   
                                                   <li>Faculty Association President</li>
                                                   
                                                   <li>Faculty Association Vice President</li>
                                                   
                                                </ul>
                                                
                                                <p>Faculty Workload Advisory Group</p>
                                                
                                                <p>Faculty Representative:</p>
                                                
                                                <ul>
                                                   
                                                   <li>Deidre Holmes DuBois</li>
                                                   
                                                </ul>
                                                
                                                <p>Web, Portal, and Mobile Advisory Group</p>
                                                
                                                <p>Faculty Representatives (two-year appointment):</p>
                                                
                                                <ul>
                                                   
                                                   <li>Brian Macon</li>
                                                   
                                                   <li>Erich Heintzelman</li>
                                                   
                                                   <li>James May</li>
                                                   
                                                </ul>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             
                                             
                                             <h3>Employee Engagement (Michelle Sever, Director)</h3>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>Grievance Committee</p>
                                                   
                                                   <p>Faculty Representatives (two-year appointment):</p>
                                                   
                                                   <p><u>2014-2015</u></p>
                                                   
                                                   <ul>
                                                      
                                                      <li>Diane Ashe</li>
                                                      
                                                      <li>Julie Kloft</li>
                                                      
                                                      <li>Paul Flores</li>
                                                      
                                                      <li>Sarah Melanson</li>
                                                      
                                                      <li>Raul Valery</li>
                                                      
                                                      <li>Cate McGowan</li>
                                                      
                                                      <li>Colin Archibald</li>
                                                      
                                                      <li>Michael Robbins </li>
                                                      
                                                   </ul>
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             <div>
                                                
                                                
                                                <h3>Employee Development (Leda Pacheco, Coordinator)</h3>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <p>Endowed Chair Committee</p>
                                                      
                                                      <p>Faculty Representatives (two-year appointment, rotating in groups of four):</p>
                                                      
                                                      <p><u>2014-2015</u></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Kim Long</li>
                                                         
                                                         <li>Susan Dauer</li>
                                                         
                                                         <li>Deborah Simko</li>
                                                         
                                                         <li>John Niss</li>
                                                         
                                                      </ul>
                                                      
                                                      <p><u>2015-2016</u></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Diane Brown</li>
                                                         
                                                         <li>Edie Gaythwaite</li>
                                                         
                                                         <li>Mailin Barlow</li>
                                                         
                                                         <li>Noy Sparks </li>
                                                         
                                                      </ul>
                                                      
                                                      <p><u>2016-2017</u></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Karen Cowden (Chair)</li>
                                                         
                                                         <li>Diane Ashe</li>
                                                         
                                                         <li>Susan Dauer</li>
                                                         
                                                         <li>Mailin Barlow</li>
                                                         
                                                         <li>Angela Blewitt</li>
                                                         
                                                         <li>Noy Sparks</li>
                                                         
                                                         <li>Diane Brown</li>
                                                         
                                                         <li>Upasana Santra</li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                      <p>NISOD Selection Committee</p>
                                                      
                                                      <p>Faculty Representatives (annual appointment):</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Celeste Henry</li>
                                                         
                                                         <li>Heath Hennel</li>
                                                         
                                                      </ul>
                                                      
                                                      <p>Pivot 180 Selection Committee</p>
                                                      
                                                      <p>Faculty Representatives (two-year appointment): </p>
                                                      
                                                      <p><u>2013-2015</u></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Karen Murray</li>
                                                         
                                                      </ul>
                                                      
                                                      <p><u>2014-2016</u></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>John Niss </li>
                                                         
                                                      </ul>
                                                      
                                                      <p><u>2015-2017</u></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Adriene Tribble</li>
                                                         
                                                      </ul>
                                                      
                                                      <p>Innovation of the Year Award Committee</p>
                                                      
                                                      <p>Faculty Representatives:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Suzanne Johnson</li>
                                                         
                                                         <li>Sarah Dockray</li>
                                                         
                                                         <li>Susan Dauer</li>
                                                         
                                                         <li>Rhonda Atkinson</li>
                                                         
                                                         <li>Jennifer Papoula </li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                
                                                <h3>Organizational Development &amp; Human Resources (Amy Bosley, VP)</h3>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <p>Faculty Compensation Committee</p>
                                                      
                                                      <p>Faculty Representatives:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Faculty Association President (Co-chair)</li>
                                                         
                                                         <li>Courtney Moore</li>
                                                         
                                                         <li>Andrea Bealler</li>
                                                         
                                                         <li>Anita Kovalsky</li>
                                                         
                                                         <li>David Freeman </li>
                                                         
                                                      </ul>
                                                      
                                                      <p>Organizational Development and Human Resources Advisory Team</p>
                                                      
                                                      <p>Faculty Representatives:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Chris Wettstein</li>
                                                         
                                                         <li>Tina Tan </li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                <div>
                                                   
                                                   
                                                   <h3>Teaching and Learning (Wendi Dew, AVP)</h3>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <p>Sabbatical Leave Committee</p>
                                                         
                                                         <p>Faculty Representatives:</p>
                                                         
                                                         <p><u>2014-2015</u></p>
                                                         
                                                         <ul>
                                                            
                                                            <li>Brian Macon</li>
                                                            
                                                            <li>John McFarland</li>
                                                            
                                                            <li>Jeffrey Donley</li>
                                                            
                                                            <li>Susan Dauer</li>
                                                            
                                                            <li>Debra Jacobs</li>
                                                            
                                                            <li>Kourtney Baldwin</li>
                                                            
                                                            <li>Wendy Wish-Bogue</li>
                                                            
                                                         </ul>
                                                         
                                                         <p><u>2015-2016</u></p>
                                                         
                                                         <ul>
                                                            
                                                            <li>Angelica Vagle</li>
                                                            
                                                            <li>Kathleen O’Neal</li>
                                                            
                                                            <li>Susan Dauer </li>
                                                            
                                                            <li>Kurt Overhiser </li>
                                                            
                                                            <li>Betsy Brantley </li>
                                                            
                                                            <li>Jeffrey Donley </li>
                                                            
                                                            <li>Nichole Spottke </li>
                                                            
                                                            <li>Tina Tan </li>
                                                            
                                                            <li>Karen Murray </li>
                                                            
                                                            <li>Diane Brown </li>
                                                            
                                                         </ul>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      <h3>Study Abroad and Global Experience</h3>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <p><strong><a href="../../../international/studyabroad/faculty-staff/committees.html">Study Abroad Committee</a></strong></p>
                                                            
                                                            <p>Faculty Representatives (two-year appointment):</p>
                                                            
                                                            <p><u>2016-2017</u></p>
                                                            
                                                            <ul>
                                                               
                                                               <li>Eric Wallman</li>
                                                               
                                                               <li>Jeffrey Donley</li>
                                                               
                                                               <li>Jennifer Tomlinson</li>
                                                               
                                                               <li> Jerry Hensel</li>
                                                               
                                                               <li>Lisa Cole</li>
                                                               
                                                               <li>Michael Robbins</li>
                                                               
                                                            </ul>
                                                            
                                                            <p><u>2015-2016</u></p>
                                                            
                                                            <ul>
                                                               
                                                               <li>Jerry Hensel</li>
                                                               
                                                               <li>Lisa Cole</li>
                                                               
                                                            </ul>
                                                            
                                                            <p><u>2014-2015</u></p>
                                                            
                                                            <ul>
                                                               
                                                               <li>Betsy Brantley</li>
                                                               
                                                               <li>Eric Wallman</li>
                                                               
                                                               <li>Jocelyn Morales</li>
                                                               
                                                               <li>Lauren Grant</li>
                                                               
                                                               <li>Melissa Schreiber</li>
                                                               
                                                               <li>Stacey DiLiberto</li>
                                                               
                                                            </ul>
                                                            
                                                            <p><a href="../../../international/studyabroad/faculty-staff/committees.html"><strong>Internationalizing the Curriculum Committee</strong></a></p>
                                                            
                                                            <p>Faculty Representatives (two-year appointment): </p>
                                                            
                                                            <p><u>2016-2017</u></p>
                                                            
                                                            <ul>
                                                               
                                                               <li>Aby Boumarate</li>
                                                               
                                                               <li>Adrienne Trier-Bieniek</li>
                                                               
                                                               <li>Aryan Ashkani</li>
                                                               
                                                               <li>Cheryl Bollinger</li>
                                                               
                                                               <li>Heather Bryson</li>
                                                               
                                                               <li>Marsha Butler</li>
                                                               
                                                               <li>Nely Hristova</li>
                                                               
                                                               <li>Tammy Gitto</li>
                                                               
                                                               <li>Yasmeen Quadri</li>
                                                               
                                                            </ul>
                                                            
                                                            <p><u>2015-2016</u></p>
                                                            
                                                            <ul>
                                                               
                                                               <li>Aby Boumarate</li>
                                                               
                                                               <li>Tammy Gitto</li>
                                                               
                                                            </ul>
                                                            
                                                            <p><u>2014-2015</u></p>
                                                            
                                                            <ul>
                                                               
                                                               <li>Jenny Hu</li>
                                                               
                                                               <li>Michael Robbins</li>
                                                               
                                                               <li>Rich Gair</li>
                                                               
                                                            </ul>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         <h3>Governance Councils</h3>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p>Six Faculty Representatives; one AA and one AS from from each campus cluster (two-year
                                                                  appointment):
                                                               </p>
                                                               
                                                               <p><u>2014-2015</u></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Karen Cowden</li>
                                                                  
                                                                  <li>Suzette Dohany</li>
                                                                  
                                                                  <li>Stanton Reed</li>
                                                                  
                                                               </ul>
                                                               
                                                               <p><u>2014-2016</u></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Jolene Rhodes (East/Winter Park Campus Cluster)</li>
                                                                  
                                                                  <li>Debra Hollister (Lake Nona/Osceola Campus Cluster)<u></u>
                                                                     
                                                                  </li>
                                                                  
                                                               </ul>
                                                               
                                                               <p><u>2015-2017</u></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Diane Dalrymple (East/Winter Park Campus Cluster)</li>
                                                                  
                                                                  <li>Nichole Shorter (Lake Nona/Osceola Campus Cluster)</li>
                                                                  
                                                                  <li>Rudy Darden (West Campus)</li>
                                                                  
                                                               </ul>
                                                               
                                                               <p>Executive Council</p>
                                                               
                                                               <p>Faculty Representatives:</p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Faculty Council Chair</li>
                                                                  
                                                                  <li>Faculty Council Co-chair</li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="http://site.valenciacollege.edu/faculty-association/" target="_blank">Meeting Summaries &amp; Discussions</a>
                        
                        <a href="http://net4.valenciacollege.edu/forms/faculty/association/contact.cfm" target="_blank">Contact</a>
                        
                        
                        
                        
                        <h3>Association News</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/upcoming-meetings-2/" target="_blank">Upcoming Meetings</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/a-faculty-heads-up-october-2017/" target="_blank">A Faculty Heads Up — October 2017</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/students-may-now-apply-for-the-jack-kent-cooke-foundation-scholarship/" target="_blank">Students May Now Apply for the Jack Kent Cooke Foundation Scholarship</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/faculty-workshops/" target="_blank">Faculty Development Opportunities</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/getting-to-know-learning-support/" target="_blank">Getting to Know Learning Support</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>Association Events</h3>
                        
                        There are no events at this time.
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/committees/index.pcf">©</a>
      </div>
   </body>
</html>