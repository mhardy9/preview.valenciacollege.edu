<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Year in Review | Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/yearinreview.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li>Year in Review</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Year in Review: 2016-17</h2>
                        
                        <h3>Policies</h3>
                        
                        <ul>
                           
                           <li>FLSA guidelines and/or about the 26 college policies that have  been reviewed and
                              will go to the DBOT in October for approval. For more  information, please <a href="http://thegrove.valenciacollege.edu/valencia-invites-feedback-on-flsa-driven-policy-changes/">refer to this article in  The Grove</a>.
                           </li>
                           
                           <li>Paid leave for PT faculty : POLICY: 6Hx28:3D-14</li>
                           
                        </ul>
                        
                        <h3>Decisions</h3>
                        
                        <ul>
                           
                           <li>Gen Ed and Humanities</li>
                           
                           <li>Migrate to Canvas</li>
                           
                           <li>Tenure Appeal Process</li>
                           
                        </ul>
                        
                        <p>The Faculty Council voted and endorsed the appeal panel guidelines. In  regard to
                           the time to review the portfolio, Celine will look at the calendar  for next year
                           to see if changing the time to 2 weeks will be possible.
                        </p>
                        
                        <h3>Endowed  Chair</h3>
                        
                        <p>The Council discussed the benefits of identifying a minimum score and voted  unanimously
                           to recommend that applicants receive at least a 1 or higher in each  category in order
                           to be eligible for the Endowed Chair the applicant is  applying for. Another area
                           that the Council will ask the Endowed Chair Review  Committee to consider is a structured
                           feedback loop for applicants that are not  awarded.
                        </p>
                        
                        <h3>Ombudsmen</h3>
                        
                        <p>The Faculty Council voted in favor of a college-wide Ombuds  program (10:5). Amy Bosley
                           has been notified and will begin exploring this in  2017. 
                        </p>
                        
                        <hr>
                        
                        <h2>Year  in Review: 2015-16</h2>
                        
                        <h3>Engagement </h3>
                        
                        <p>The Faulty Council has had several conversations with Human  Resources and Campus
                           Presidents about Engagement Hours. The following  documents: 
                        </p>
                        
                        <ol>
                           
                           <li> <a href="documents/Memo-Student-Engagement-Hours-2016.pdf" target="_blank">Valencia  Memo Engagement Hours</a>
                              
                           </li>
                           
                           <li>
                              <a href="documents/Email-from-Falecia-Williams-on-Faculty-Workload-and-Engagement-Hours.pdf" target="_blank">Email  from Falecia Williams</a>, summarize the practice as follows: engagement hours  are determined by the faculty
                              and their Dean or Supervisor. There are no  parameters on the length of meeting, and
                              start and end times.
                           </li>
                           
                        </ol>
                        
                        <h3>Certificate  of Absence </h3>
                        
                        <p>Deans should work with faculty on an individual basis  regarding time off, however
                           set times have been established for Learning Day --  7.0 hours and Commencement --
                           3.5 hours. 
                        </p>
                        
                        <h3>6-6-0-0  (1 attachment) </h3>
                        
                        <p>A concern about low summer enrolment from several faculty  members on West campus
                           was brought to the attention of the Faculty Council.  They suggested relaxing the
                           3 year rule concerning 6-6-0-0 contracts as a  viable solution; however, after discussing
                           this with Amy Bosley <a href="documents/Email-from-Amy-Bosley-on-6-6-0-0.pdf">(email  attached)</a>, it was determined that a continuous 6-6-0-0 contract is  not a viable solution.
                        </p>
                        
                        <h3>Compensation</h3>
                        
                        <p>2016-2017 <a href="http://thegrove.valenciacollege.edu/faculty-governance-update-june-2016/?utm_source=MASTER+LIST&amp;utm_campaign=40eb45e765-Faculty+Insight+%E2%80%93+Volume+5%2C+Issue+6&amp;utm_medium=email&amp;utm_term=0_8f87859af4-40eb45e765-277011821">Raise  Explanation</a></p>
                        
                        <h3>Work  Plans </h3>
                        
                        <ul>
                           
                           <li><a href="documents/SFI-Work-Proposal-April-5-2016.pdf" target="_blank">Student  Feedback of Instruction (SFI)</a></li>
                           
                           <li><a href="../../governance/llc/llcworkplans.html">Online  Learning</a></li>
                           
                        </ul>
                        
                        <h3>New  Committee</h3>
                        
                        <p>Instructional Materials Committee:</p>
                        
                        <p> We have had two years to implement the Instructional  Materials policy, so now it
                           is time to reflect on the process. The Council  commissioned the Instructional Materials
                           Advisory Team in hopes that they would  be able help shepherd in the new policy along
                           with providing feedback on the  process. A big thank you to Chris Borglum and Arron
                           Powel for doing just that!  One recommendation from them was to turn this Advisory
                           group into a standing  committee with various stakeholders. The committee will start
                           meeting this  fall, stay tuned for details.
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="http://site.valenciacollege.edu/faculty-association/" target="_blank">Meeting Summaries &amp; Discussions</a>
                        
                        <a href="http://net4.valenciacollege.edu/forms/faculty/association/contact.cfm" target="_blank">Contact</a>
                        
                        
                        
                        
                        <h3>Association News</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/upcoming-meetings-2/" target="_blank">Upcoming Meetings</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/a-faculty-heads-up-october-2017/" target="_blank">A Faculty Heads Up — October 2017</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/students-may-now-apply-for-the-jack-kent-cooke-foundation-scholarship/" target="_blank">Students May Now Apply for the Jack Kent Cooke Foundation Scholarship</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/faculty-workshops/" target="_blank">Faculty Development Opportunities</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/getting-to-know-learning-support/" target="_blank">Getting to Know Learning Support</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>Association Events</h3>
                        
                        There are no events at this time.
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/yearinreview.pcf">©</a>
      </div>
   </body>
</html>