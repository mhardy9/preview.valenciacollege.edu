<ul>
    <li><a href="/faculty/association/">Faculty Association</a></li>
    <li class="submenu">
        <a class="show-submenu" href="/faculty/association/constitution/">Constitution <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
            <li><a href="/faculty/association/constitution/AssociationConstitution.php">Association Constitution</a></li>
            <li><a href="/faculty/association/constitution/FacultyCouncilBy-laws.php">Faculty Council By-laws</a></li>
            <li><a href="/faculty/association/constitution/Glossary.php">Glossary</a></li>
            <li><a href="/faculty/association/constitution/2015Transitionplan.php">2015 Transition plan</a></li>
        </ul>
    </li>
    <li class="submenu">
        <a class="show-submenu" href="/faculty/association/representatives/">Representatives <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
            <li><a href="/faculty/association/representatives/officers.php">Collegewide Officers</a></li>
            <li><a href="/faculty/association/representatives/past-presidents.php">Past Presidents</a></li>
        </ul>
    </li>
    <li><a href="/faculty/association/committees/">Work Teams</a></li>
    <li><a href="/faculty/association/yearinreview.php">Year in Review</a></li>
    <li><a href="http://net4.valenciacollege.edu/forms/faculty/association/contact.cfm" target="_blank">Contact</a></li>
</ul>