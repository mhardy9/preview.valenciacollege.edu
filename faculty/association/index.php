<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li>Association</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>
                           <a name="content" id="content"></a>
                           
                        </h2>
                        
                        
                        
                        
                        
                        
                        
                        <p>The Valencia College-wide Faculty Association represents Valencia faculty through
                           five campus assemblies: East,  Lake Nona, Osceola, West, and Winter Park. 
                        </p>
                        
                        <p>Representatives from the campuses meet as the Faculty Council and function as a governing
                           council of the College on issues concerning faculty.
                        </p>
                        
                        <h3>Faculty Council  </h3>
                        
                        <p>Faculty Council agenda items can be sent to the Faculty Council Chair, Faculty Council
                           Co-Chair, or any Campus Assembly President. The meeting schedule is provided below.
                           Please note that these meeting dates, times, and locations are subject to change.
                           
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>Meeting Dates</div>
                                 </div>
                                 
                                 <div data-old-tag="th">Meeting Times </div>
                                 
                                 <div data-old-tag="th">Location</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>September    14, 2017 </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 - 5:00    PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>School of    Public Safety <br>
                                       Room 224
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>October    12, 2017</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Lake Nona    Campus<br>
                                       Room 148
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>November    9, 2017 </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Winter    Park Campus<br>
                                       Room 112
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>December    14, 2017 </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>West    Campus<br>
                                       11-106 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>January    11, 2018</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Osceola    Campus<br>
                                       1-219B
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>February    8, 2018</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>East    Campus<br>
                                       5-112
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>March 8,    2018</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Lake Nona    Campus<br>
                                       Room 148 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>April 12,    2018</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Winter    Park Campus<br>
                                       Room 112
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>May 10,    2018</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>West    Campus<br>
                                       11-106
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>June 14,    2018</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Osceola    Campus<br>
                                       4-105 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>July 12,    2018</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2:00 -    5:00 PM</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>East    Campus<br>
                                       3-113
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><strong>Better decisions. Greater trust.</strong><strong></strong></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="http://site.valenciacollege.edu/faculty-association/" target="_blank">Meeting Summaries &amp; Discussions</a>
                        
                        <a href="http://net4.valenciacollege.edu/forms/faculty/association/contact.cfm" target="_blank">Contact</a>
                        
                        
                        
                        
                        <h3>Association News</h3>
                        
                        
                        
                        <ul>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/upcoming-meetings-2/" target="_blank">Upcoming Meetings</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/a-faculty-heads-up-october-2017/" target="_blank">A Faculty Heads Up — October 2017</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/students-may-now-apply-for-the-jack-kent-cooke-foundation-scholarship/" target="_blank">Students May Now Apply for the Jack Kent Cooke Foundation Scholarship</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/faculty-workshops/" target="_blank">Faculty Development Opportunities</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                           <li>
                              
                              
                              
                              
                              
                              
                              <a href="http://thegrove.valenciacollege.edu/getting-to-know-learning-support/" target="_blank">Getting to Know Learning Support</a> <br>
                              
                              
                              
                              
                              
                              
                           </li>
                           
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>Association Events</h3>
                        
                        There are no events at this time.
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/index.pcf">©</a>
      </div>
   </body>
</html>