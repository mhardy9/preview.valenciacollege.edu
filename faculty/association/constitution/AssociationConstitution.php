<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/constitution/AssociationConstitution.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li><a href="/faculty/association/constitution/">Constitution</a></li>
               <li>Faculty Association</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h1><strong>Faculty Association Constitution</strong></h1>
                        
                        <h3><strong>Preamble</strong></h3>
                        
                        <p>The faculty of Valencia College are committed to the philosophy that the faculty play
                           a vital role in the success of the College and should be provided with a formal voice
                           in the development of the College. Our mutual expectation of one another is that the
                           faculty will desire to contribute their skills, knowledge, and expertise to the overall
                           success of the Valencia College-wide Faculty Association and of the College.
                        </p>
                        
                        <h3><strong>Article I: Name</strong></h3>
                        
                        <p>This organization shall be known as the Valencia College-wide Faculty Association
                           (hereinafter VCFA).
                        </p>
                        
                        <h3><strong>Article II: VCFA Membership</strong></h3>
                        
                        <p>VCFA members are tenured, tenure-track, and annually appointed professors, counselors,
                           and librarians who are classified as faculty by the Office of Organizational Development
                           and Human Resources. If an employee’s status is unclear, his or her status will be
                           determined by a majority vote of the <a href="#FacultyCouncil">Faculty Council.</a></p>
                        
                        <p>Members of the VCFA maintain a deep level of <a href="#Accountability">accountability</a> toward one another. They will contribute their skills, knowledge, and expertise on
                           various committees, both college-wide and campus-based, within their divisions and
                           disciplines and outside of them. Their appointments and participation on these committees
                           provide the entire VCFA with a voice for the life and direction of the College; therefore,
                           the work of individual members should be undertaken with the utmost effort. If an
                           appointment to represent the VCFA proves too demanding, leading to missed meetings
                           or failure to provide necessary communication, members may be removed from a position
                           by a majority vote of the Faculty Council.
                        </p>
                        
                        <h3><strong><a name="ArticleIII" id="ArticleIII"></a>Article III: VCFA Purpose</strong></h3>
                        
                        <p>The VCFA exists to:</p>
                        
                        <ul>
                           
                           <li>advise the president of the College on matters of faculty-related interest and faculty
                              welfare, 
                           </li>
                           
                           <li>promote <a href="#Communication">communication</a> and mutual understanding within the faculty and between the faculty and other groups,
                              and 
                           </li>
                           
                           <li>provide a <a href="#Collaboration">collaborative</a> means of formulating questions, articulating problems, and contributing solutions.
                              
                           </li>
                           
                        </ul>
                        
                        <p>The VCFA as a whole attends to and speaks to policy-level decisions regarding compensation,
                           member benefits, and workload conditions. Members also vote on the selection of VCFA
                           leadership and revisions to this constitution.
                        </p>
                        
                        <h3><strong><a name="ArticleIV" id="ArticleIV"></a>Article IV: VCFA Decision Making</strong></h3>
                        
                        <p>In cases where the entire membership is called to make a decision or offer a recommendation,
                           the VCFA will usually hold an electronic vote or survey. 
                        </p>
                        
                        <p>All members shall be notified in writing of the purpose and any appropriate information
                           relevant to making a decision or recommendation. The VCFA leadership should allow
                           for a reasonable amount of time (generally at least two weeks) for reading, reflection,
                           and discussion before a vote is taken.
                        </p>
                        
                        <p>Any decision demanding a vote of the VCFA will require a quorum of thirty (30) percent
                           of the VCFA membership, except on any vote regarding changes to this constitution
                           (<a href="#ArticleVII">see Article VII</a>). 
                        </p>
                        
                        <p>Decisions of the VCFA are obtained by majority vote of members who vote, where quorum
                           is present, whether attending a special meeting or responding to an electronic ballot
                           or questionnaire distributed by the VCFA President. Proxy voting is prohibited.
                        </p>
                        
                        <p>On matters of recommendation, no quorum is required. The recommendation of the VCFA
                           is the option selected by the majority of responding members.
                        </p>
                        
                        <p>In the rare event that a face-to-face meeting is necessary, the VCFA may choose to
                           meet by the call of the VCFA Leadership Team, the Faculty Council, or the members
                           of the VCFA. Twenty-five (25) percent of the members of the VCFA is required, in writing
                           or through electronic ballot, to direct the VCFA Leadership Team to call a special
                           meeting.
                        </p>
                        
                        <h3><strong>Article V: VCFA Leadership</strong></h3>
                        
                        <p>A two-person Leadership Team comprised of a President and a Vice President leads the
                           VCFA. A <a href="#FacCouncil">Faculty Council,</a> along with the Past President, assists this Leadership Team in the College’s <a href="#VisionShdGovt">collaborative decision-making process</a>.
                        </p>
                        
                        <h3><strong>Section I: VCFA Leadership Team</strong></h3>
                        
                        <p>The two members of the VCFA Leadership Team will each serve for one year in the consecutive
                           roles of Vice-President and President. The Past President will also serve for one
                           year in the role of advisor. A time period of two years must elapse before a member
                           of the Leadership Team may be reelected to the Leadership Team or to the Faculty Council;
                           for a person who has served as a member of the Leadership Team, the two-year period
                           begins after the term of service as Past President. The “year” for serving the Association
                           runs from August through July.
                        </p>
                        
                        <p>The VFCA Leadership Team is designed to work collaboratively, with both leaders actively
                           involved in decision-making for the VCFA. Both members of the Leadership Team attend
                           major meetings, including but not limited to meetings with the College president and
                           any senior leadership teams led by the College president where he or she seeks faculty
                           representation.
                        </p>
                        
                        <p>The President will be fully reassigned for the one-year term (Fall, Spring, Summer).
                           The Vice President will receive reassigned time equivalent to two 3-hour courses for
                           all three semesters of the one-year term (Fall, Spring, Summer). The Past President
                           will receive reassigned time equivalent to one 3-hour course for all three semesters
                           of the one-year term (Fall, Spring, Summer).
                        </p>
                        
                        <p>The President is the chief executive officer and is first among equals on the VCFA
                           Leadership Team and Faculty Council. He or she will:
                        </p>
                        
                        <ul>
                           
                           <li>preside at all meetings of the VCFA;</li>
                           
                           <li>chair the Faculty Council;</li>
                           
                           <li>serve as spokesperson for the VCFA on all matters pertaining to the concerns of the
                              VCFA (in/after consultation with the Vice President);
                           </li>
                           
                           <li>communicate directly to the VCFA;</li>
                           
                           <li>serve on the College’s other governing councils, plus the Council Officers’ Coordination
                              Team; 
                           </li>
                           
                           <li>represent the Association with voice at meetings of the Valencia College District
                              Board of Trustees; and
                           </li>
                           
                           <li>perform such other duties as pertain to the office of President.</li>
                           
                        </ul>
                        
                        <p>The Vice President will serve as the principal assistant for the President. He or
                           she will actively participate at Faculty Council meetings as well as on the College
                           president’s Executive Council and other governing councils as assigned. The Vice President
                           will also participate on the College Senior Team, on the Council Officers’ Coordination
                           Team, and in the Campus President meetings; and serve as the Faculty Council representative
                           on the College Curriculum Committee. The Vice President will assume the role of President
                           at the conclusion of the one-year vice-presidential term. 
                        </p>
                        
                        <p>The Past President will provide aid and counsel to the VCFA Leadership Team as requested,
                           ensuring a smooth transition in leadership. The Past President will serve on Faculty
                           Council as a non-voting member, on the Council Officers’ Coordination Team, and as
                           co-chair of the Learning Leadership Council. 
                        </p>
                        
                        <p>Neither member of the VCFA Leadership Team nor the Past President may take a sabbatical,
                           nor may he or she hold a Campus Assembly leadership position, while maintaining the
                           role of service to the Association.
                        </p>
                        
                        <p>Each year, any eligible member of the VCFA can submit his or her name as a candidate
                           for the office of Vice President. The Vice President must have signed a continuing
                           contract before taking office. Procedures for this election are found in <a href="#V3A">Article V, Section 3.A</a>.
                        </p>
                        
                        <p>Either VCFA Leadership Team member may be removed from office by a vote of two-thirds
                           of the members at a special meeting (face-to-face or by electronic ballot; quorum
                           required). Removal may be with or without cause. The person under consideration for
                           removal shall be given an opportunity to be heard at the meeting; sufficient notice
                           of two weeks to the proposed removal is required. On a vote of removal for the President,
                           the Vice President shall preside at the meeting.
                        </p>
                        
                        <p>In the event the office of President of the VCFA becomes vacant or the President is
                           removed from office, the Vice President of the VCFA shall temporarily assume the office
                           of President. If the remainder of the term assumed by the Vice President is less than
                           six months, the Vice President shall assume the office of President and then continue
                           as President for the following year. If the remainder of the term is longer than 6
                           months, the presidency of the VCFA shall be filled at the earliest convenience of
                           the Faculty Council in a <a href="#SpecialElections">special election</a> with full vote of all VCFA members on all candidates nominated by VCFA members. The
                           current Vice President may choose to run in this special election, but if elected,
                           will serve as President for the remainder of the current term. In that event, a second
                           <a href="#SpecialElections">special election</a> for a new Vice President would be held as soon as possible.
                        </p>
                        
                        <p>If the office of Vice President becomes vacant or the Vice President is removed from
                           office, the President will call for a <a href="#SpecialElections">special election</a> to replace the Vice President.
                        </p>
                        
                        <h3><strong>Section II: Faculty Council</strong></h3>
                        
                        <p>The Faculty Council provides leadership for and to the VCFA by convening conversations
                           of over-riding interest to the faculty and their professional practice, <a href="#Representing">representing</a> faculty views on College leadership teams, speaking directly to concerns about the
                           welfare of the faculty, and actively working to engage the VCFA in the College’s strategies
                           for improvement. Because of its central role for faculty <a href="#Communication">communication</a> and <a href="#Collaboration">collaboration</a>, the Faculty Council shall serve as the official voice of the faculty as a governing
                           council of the College. 
                        </p>
                        
                        <p>The Faculty Council may <a href="#Representing">represent</a> faculty views directly, through their own actions as outlined below, while at other
                           times, faculty are <a href="#Representing">represented</a> by the members of the VCFA selected by the Faculty Council who sit on councils, committees,
                           and work teams.
                        </p>
                        
                        <h3><strong><a name="V2A" id="V2A"></a>A: Membership of Faculty Council</strong></h3>
                        
                        <p>Faculty Council will comprise the VCFA President, the VCFA Vice President, the VCFA
                           Past President, each campus Leadership Team (the Campus Assembly President and Vice-President)
                           and one at-large Representative from each campus. A campus may choose not to send
                           a third representative from the campus. All Faculty Council representatives will be
                           elected from all voting VCFA members on that campus.
                        </p>
                        
                        <p>A "campus" is defined as a place of instruction where members of the Association are
                           assigned. Any campus with less than five (5) Association members is not required to
                           organize with a Campus Assembly Leadership Team, and shall be represented on the Faculty
                           Council by the Association Vice President. 
                        </p>
                        
                        <p><a name="FCYear" id="FCYear">The year of service on the Faculty Council runs from August to July. </a>Elections for all campus representatives will occur in April, with first Council meeting
                           occurring the following September. All members will begin their service by attending
                           <a href="#GovernRetreat">the annual Governance Retreat.</a> 
                        </p>
                        
                        <p>The At-Large <a href="#Representing">Representatives</a> shall serve two-year terms, to a maximum of four (4) years consecutively. A time
                           period of two years must elapse before a member may be reelected to the Faculty Council.
                           A current Faculty Council member may choose to stand for election for Vice President
                           of VCFA, regardless of years served currently in the Faculty Council; if elected,
                           his or her position becomes vacant and the campus will hold a special election to
                           replace the member.
                        </p>
                        
                        <p>If a member of the Faculty Council should choose to step down, the vacancy shall be
                           filled immediately by election on the campus of the vacancy.
                        </p>
                        
                        <h3><strong>B: Mission of Faculty Council</strong></h3>
                        
                        <p>The Faculty Council will be the official voice for the VCFA in the College’s <a href="#VisionShdGovt">collaborative decision-making process</a>. The Council takes actions for the VCFA, with the exception of <a href="#VCFAvotes">decisions reserved for the VCFA as a whole.</a> The Faculty Council engages the collaborative governance model in several ways. Generally,
                           the President of the VCFA is given direct access to the Board of Trustees, the president
                           of the College, and the Executive Council to effectively represent and engage the
                           voice of the faculty in College governance. Other faculty leaders, such as the Vice
                           President and, at times, the Past President, are also engaged in the larger governance
                           process.
                        </p>
                        
                        <p>Generally, the Council is responsible for organizing strategic conversations about
                           long-term and/or important concerns of the faculty. 
                        </p>
                        
                        <p>Members of the Faculty Council subscribe to the following actions:</p>
                        
                        <ul>
                           
                           <li>value and attend all meetings of the Faculty Council and VCFA.</li>
                           
                           <li>present accurate and timely information to the VCFA members and bring their ideas,
                              questions, and concerns back to the Faculty Council.
                           </li>
                           
                           <li>be active and collegial in all dealings with the Leadership Team, other Representatives,
                              and VCFA members.
                           </li>
                           
                           <li>contribute time to the business of the VCFA, committees and events.</li>
                           
                           <li>organize, enlist aid, and direct work in the VCFA</li>
                           
                        </ul>
                        
                        <p>The Faculty Council will:</p>
                        
                        <ul>
                           
                           <li>represent the VCFA in all matters of faculty interest and faculty welfare;</li>
                           
                           <li>speak to the VCFA as the leaders of the VCFA on various matters;</li>
                           
                           <li>commission conversations among VCFA members on strategic issues related to the future
                              development of the College;
                           </li>
                           
                           <li>review and <a href="#FacOpinionPolicy">express its opinion on all College policy changes</a> before they are presented to the Board of Trustees through either a vote of the Council
                              or by seeking the opinion of the entire VCFA;
                           </li>
                           
                           <li>make appointments of members who have volunteered to councils, work teams, and committees;</li>
                           
                           <li>hear and disseminate reports from councils, work teams, and committees;</li>
                           
                           <li>discuss, coordinate, and recommend action on the various reports presented;</li>
                           
                           <li>disseminate to the members of the VCFA appropriate information regarding issues affecting
                              all members of the VCFA;
                           </li>
                           
                           <li>advise the president of the College on all matters of College improvement or other
                              matters of faculty interest that may affect the operation of the College;
                           </li>
                           
                           <li>determine via majority vote an VCFA member’s status as a member, if the status is
                              in question; and
                           </li>
                           
                           <li>act on all other matters of business which are not specifically reserved for the VCFA
                              members and which are necessary and proper in order to carry out the foregoing duties
                              and functions.
                           </li>
                           
                        </ul>
                        
                        <p>On occasion, a major initiative may be commissioned from the Faculty Council. When
                           commissioning a major initiative, as with the other councils, the work will be given
                           to a collaborative work team developed for each major project that represents both
                           the key stakeholders and the best expertise for the work of collaborative design.
                           
                        </p>
                        
                        <p>Every commissioned project will have an executive sponsor, a member of the College
                           president’s Senior Team or his or her designee, who partners with the work team to
                           facilitate the work and guide it through the governance process, and a Faculty Council
                           sponsor. In addition, every project will have a work plan approved by the Faculty
                           Council that will include the process, participants, design principles, and intended
                           outcomes, as well as a rough schedule of their work. Every project will be supported
                           by project management tools to track the work and keep the efforts as transparent
                           to the rest of the College as possible.
                        </p>
                        
                        <p>The Faculty Council has the right to defer decisions to the VCFA when it deems appropriate,
                           as explained in <a href="#ArticleIV">Article IV</a>.
                        </p>
                        
                        <p>The work of the Council shall be governed by a <a href="#ByLaws">set of by-laws</a>.
                        </p>
                        <span>
                           
                           <h3>
                              
                              
                           </h3>
                           
                           <h3>
                              <strong>Section III: Elections for Leadership</strong>
                              
                              
                           </h3>
                           
                           <h3> </h3>
                           
                           <h3>
                              <strong><a name="V3A" id="V3A"></a>A: Normal Election for Vice President</strong>
                              
                              
                           </h3>
                           
                           <p>By the date of the March meeting of the Faculty Council, members of the VCFA must
                              send names of any VCFA members they wish to nominate for Vice President to the VCFA
                              Leadership Team or to any member of the Faculty Council. Nominations may be sent electronically,
                              or members of the Faculty Council may announce nominees at the March meeting. Nominees
                              must have signed a continuing contract before taking office in August.
                           </p>
                           
                           <p>Within two weeks of being nominated at the March meeting, each nominee will prepare
                              a one-page statement explaining his or her experience at the College, experience in
                              governance, and other pertinent information, and send it electronically to the President,
                              who will share the statements with the members of the VCFA, either in advance of the
                              election or with the secure electronic ballot.
                           </p>
                           
                           <p>All VCFA members will be eligible to vote for one Vice President from the slate of
                              nominees. The election period shall be two weeks in length. The nominee receiving
                              the most votes will assume the position of Vice President of the VCFA in August; he
                              or she must be able to attend the July College-wide Governance retreat.
                           </p>
                           
                           <h3>
                              <strong><a name="SpecialElections" id="SpecialElections"></a>B: Special Elections</strong>
                              
                              
                           </h3>
                           
                           <p>Upon the need for a special election of the President or Vice President, the VCFA
                              shall direct the lead presiding officer to begin proceedings for a special election
                              within one week of needing a replacement. 
                           </p>
                           
                           <p>The members of the VCFA will be notified of the change and be requested to submit
                              nominations for the post. Each nominee will prepare a one-page statement explaining
                              his or her experience at the College, experience in governance, and other pertinent
                              information, and send it electronically to the President, who will share the statements
                              with the members of the VCFA, either in advance of the election or with the secure
                              electronic ballot.
                           </p>
                           
                           <p>All VCFA members will be eligible to vote in the special election for the vacant position
                              from the slate of nominees. The election period shall be two weeks in length. The
                              nominee receiving the most votes will assume the vacated position.
                           </p>
                           
                           <p>If a Campus Assembly President, Vice-President, or At-Large Representative resigns
                              or is removed from the Council, the appropriate Campus Assembly will be tasked with
                              replacing the member within one month of the vacancy.
                              
                           </p>
                           
                           <h3>
                              <strong>Article VI: Campus Assembly</strong>
                              
                              
                           </h3>
                           
                           <p>Members of the VCFA on each campus will organize in order to aid in fulfilling the
                              <a href="#ArticleIII">VCFA’s purpose</a> in Article III, as well as to advise the chief administrative officer of that campus.
                              The membership of the campus faculty organization may include representatives of faculty
                              who are not members of the VCFA as specified in the by-laws of the campus faculty
                              organization.
                           </p>
                           
                           <p>To fulfill the VCFA’s purpose on each campus, a Campus Assembly will be formed, led
                              by the campus Leadership Team of a President and Vice President. Thus, the leader
                              will be the “President of the [campus name] Assembly.”
                           </p>
                           
                           <p>A Campus Assembly President shall serve for one year in that role; this person will
                              receive reassigned time equivalent to one 3-hour course for all three semesters (Fall,
                              Spring, Summer) of his or her term.
                           </p>
                           
                           <p>Each respective campus shall form their Assembly’s structure; the VCFA members on
                              that campus shall determine the best structure for their campus. Options include gatherings
                              with representatives from all academic divisions, a structure with at-large representatives,
                              or broad-based gatherings that all VCFA members attend.
                           </p>
                           
                           <p>The Campus Assembly President presides over whatever type structure that campus has
                              chosen, as well as participates in the Campus Executive Council led by the campus’s
                              chief administrative officer. The Campus Assembly President will serve as primary
                              contact between the campuses and the VCFA Leadership Team, typically via electronic
                              communication.
                           </p>
                           
                           <p>Campus Assemblies shall meet on the third Thursday of the month to avoid conflicts
                              with Faculty Council meetings as described in the Faculty Council <a href="#BylawsSectionII">bylaws, Section II.</a></p>
                           
                           <p>Members of any Campus Assembly, whether on the Leadership Team or serving as representatives,
                              subscribe to the following actions:
                           </p>
                           
                           <ul>
                              
                              <li>value and attend all meetings of the Campus Assembly</li>
                              
                              <li>present accurate and timely information to the Campus Assembly, including any division
                                 or discipline one may <a href="#Representing">represent</a> and bring their ideas, questions and concerns back to the Campus Assembly.
                              </li>
                              
                              <li>be active and collegial in all dealings with the campus Leadership Team, other Representatives,
                                 and those one may represent.
                              </li>
                              
                              <li>contribute time to the business of the Assembly, committees, and events.</li>
                              
                              <li>organize, enlist aid, and direct work in the Assembly and within any division or discipline
                                 one may <a href="#Representation">represent.</a>
                                 
                              </li>
                              
                           </ul>
                           
                           <p>Campus elections for the Assembly leadership team as well as the At-Large Representative
                              from the campus to Faculty Council will occur in the same time period as the VCFA
                              elections for the Association’s Vice-President. Campus leaders will take office starting
                              in August.
                           </p>
                           
                           <p>The by-laws of a Campus Assembly may contain such additional or implementing matters
                              of internal organization as are deemed appropriate to that campus provided they are
                              not in conflict with this Constitution.
                           </p>
                           
                           <h3>
                              <strong><a name="ArticleVII" id="ArticleVII"></a>Article VII: Changes to Constitution</strong>
                              
                              
                           </h3>
                           
                           <p>This Constitution may be amended by majority vote of the members present and voting,
                              providing that any proposed amendment shall be presented, in writing, to the membership
                              at least two (2) weeks prior to the special meeting or responding to an electronic
                              ballot or questionnaire distributed by the VCFA President., where such amendments
                              shall be considered. Quorum for voting on changes to the Constitution is fifty (50)
                              percent of members of the VCFA. Proxy voting is prohibited.
                           </p>
                           
                           
                           <h1><strong><a name="ByLaws" id="ByLaws"></a>Faculty Council By-Laws</strong></h1>
                           
                           <h3>
                              <strong>Section I: Membership</strong>
                              
                              
                           </h3>
                           
                           <p>The Faculty Council will comprise the VCFA President, the VCFA Vice President, the
                              VCFA Past President, each campus Leadership Team (a campus Assembly President and
                              Vice-President) and one at-large Representative from each campus. A campus may choose
                              not to send a third representative from the campus.
                           </p>
                           
                           <p>Members of the Faculty Council subscribe to the following actions:</p>
                           
                           <ul>
                              
                              <li>value and attend all meetings of the Faculty Council and VCFA.</li>
                              
                              <li>present accurate and timely information to the VCFA members and bring their ideas,
                                 questions, and concerns back to the Faculty Council.
                              </li>
                              
                              <li>be active and collegial in all dealings with the Leadership Team, other Representatives,
                                 and VCFA members.
                              </li>
                              
                              <li>contribute time to the business of the VCFA, committees, and events.</li>
                              
                              <li>organize, enlist aid, and direct work in the VCFA.</li>
                              
                           </ul>
                           
                           <p>At the start of the Council year as described in <a href="#V2A">Article V, Section II.A</a>, each member of the Council will verbally state his or her acceptance of the five
                              statements above.
                           </p>
                           
                           <h3>
                              <strong><a name="BylawsSectionII" id="BylawsSectionII"></a>Section II: Meetings</strong> 
                           </h3>
                           
                           <p>A quorum for any meeting or electronic voting of the Council shall be a majority of
                              the members.
                           </p>
                           
                           <p>The Council shall meet once a month on the second Thursday of the month except for
                              the month of August. These meetings shall take place at a location chosen by the VCFA
                              President in consultation with the Council. Additional meetings may be scheduled according
                              to the availability of the Council. Some months the meeting may be moved to accommodate
                              scheduling issues, such as the start of term, end of term, or a holiday such as Spring
                              Break.
                           </p>
                           
                           <p>Meetings shall begin at 2:00 p.m. unless prior notice has been given by the President
                              prior to the scheduled meeting date. A time change may not be made in the 24 hours
                              preceding a meeting. Most meetings shall last till 5:00.
                           </p>
                           
                           <p>If a representative is unable to attend a meeting, the representative shall notify
                              the VCFA President or Vice President. There are no alternates for Faculty Council
                              members nor may any member, including the VCFA Leadership Team, vote by proxy.
                           </p>
                           
                           <p>All members are expected to attend the <a href="#GovernRetreat">annual Governance retreat</a>, to be held in July of each year.
                           </p>
                           
                           <h3>
                              <strong>Section III: Duties and Responsibilities</strong>
                              
                              
                           </h3>
                           
                           <p>Members represent and share the perspectives of the VCFA during Faculty Council meetings.
                              All Council Representatives are available to all members of the VCFA.
                           </p>
                           
                           <p>Members shall aid the President in communicating and gathering information from VCFA
                              members regarding issues affecting or of interest to the VCFA.
                           </p>
                           
                           <p>Because a core purpose of Faculty Council is convening conversations of over-riding
                              interest to the faculty and their professional practice, members should expect to
                              aid in the planning, promotion, and execution of these conversations, including but
                              not limited to hosting discussions on various campuses (including campuses beyond
                              any campus of prime responsibility).
                           </p>
                           
                           <p>The President or representative shall produce and distribute electronically the agenda
                              and any related documents for Council meetings at least 48 hours prior to the meeting.
                              The agenda shall be placed on the VCFA’s website by the Faculty Council’s administrative
                              assistant.
                           </p>
                           
                           <p>Many decisions will be made by <a href="#Consensus">consensus</a>. Other decisions will be made by a formal vote. The decisions on all votes will be
                              made by majority vote of members who are voting where quorum is present. The President
                              may vote on any matter where his or her vote can affect the outcome of the vote. Proxy
                              voting is prohibited.
                           </p>
                           
                           <p>Voting is a privilege and a responsibility. However, members cannot be compelled to
                              vote and should not vote on a question to which they have a direct personal or financial
                              interest. In that event, members may prefer to abstain from voting. Abstentions are
                              not recorded in the Meeting Summary.
                           </p>
                           
                           <p>The Faculty Council places members of the VCFA on councils, committees, and work teams.
                              The President may choose to communicate directly with the entire VCFA requesting volunteers,
                              or the Faculty Council may choose to nominate specific members based on knowledge
                              and expertise said members could provide to the assignment, or the President and Council
                              may choose to solicit campus-based representation, in which case the Assembly on each
                              campus will be tasked with providing a certain number of volunteers. In this latter
                              case, the decision of the Campus Assembly will be binding for the appointment. 
                           </p>
                           
                           <p>Meetings are open unless a majority of the Council vote to discuss a matter confidentially,
                              in which case they may reserve a part of their regular meeting for that business or
                              conduct a special meeting as a closed session. 
                           </p>
                           
                           <p>A representative from the Academic Affairs &amp; Planning Division will take meeting notes
                              at Faculty Council meetings. The President will review and edit where necessary the
                              notes; those meeting notes will subsequently be sent to the full Faculty Council as
                              a Meeting Summary for review and approval, whether electronically or during the next
                              meeting. Approval electronically is to be preferred in order to have the meeting notes
                              posted on the Association’s website. All members are expected to vote within one week
                              of being notified of the vote, if the meeting notes are sent electronically.
                           </p>
                           
                           <h3>
                              <strong>Section IV: Amendments</strong>
                              
                              
                           </h3>
                           
                           <p>These By-Laws may be amended by a majority vote of the members of the Faculty Council
                              present and voting, provided that written or printed notice of the proposed amendment(s)
                              is given to all members of the Faculty Council at least two weeks prior to the balloting.
                           </p>
                           
                           
                           <h1><strong>College-wide Faculty Association Definitions</strong></h1>
                           
                           <p>A mutual understanding of key words or concepts is critical for any organization or
                              civic entity. During the summer of 2013, representatives from each Valencia campus
                              and employee group met to develop design options for the College’s shared governance
                              system. As a part of that work, key terms were defined and are useful for our work
                              in the VCFA. 
                           </p>
                           
                           <p><a name="VisionShdGovt" id="VisionShdGovt"><strong>Vision for Shared Governance</strong></a><strong>:</strong> Shared governance at Valencia is the collaborative process of the college making
                              courageous, wise choices that moves our work forward. Using the strengths of diverse
                              individuals and giving a voice to various constituencies, everyone contributes with
                              good faith and effort, transparency and integrity. The process articulates clear expectations,
                              includes a timeframe, provides a communication plan, and allows for meaningful and
                              efficient participation. The conclusions are trusted, accepted, and respected by the
                              entire College as the work of good people making the best decision possible at the
                              time.
                           </p>
                           
                           <p><a name="Accountability" id="Accountability"><strong>Accountability</strong></a> is the establishment of individual and mutual ownership, responsibility, and obligation
                              including partners and constituents who are willing and able to make decisions, take
                              actions, and then convey and justify those decisions/actions.
                           </p>
                           
                           <p><a name="Collaboration" id="Collaboration"><strong>Collaboration</strong></a> is a process of working together and embracing different perspectives that includes
                              appropriate representatives or stakeholders to achieve a clear goal.
                           </p>
                           
                           <p>Principles of collaboration:</p>
                           
                           <ul>
                              
                              <li>The process by which the work proceeds should be clearly defined, including how participation
                                 will be encouraged and how communication will be accomplished.
                              </li>
                              
                              <li>Representatives or stakeholders should be a diverse group with a broad view of college
                                 functions, specialized expertise, or closeness to the work.
                              </li>
                              
                              <li>All participants should share their best ideas, but they must realize that the work
                                 is shared and all will advocate for the outcome that evolves from the work.
                              </li>
                              
                           </ul>
                           
                           <p>Effective <a name="Communication" id="Communication"><strong>communication</strong></a> is the exchange of ideas, messages, and information between individuals and/or a
                              group that considers not only the audience and purpose but also leads to understanding
                              although not necessarily agreement. The sender and the receiver are both responsible
                              for listening.
                           </p>
                           
                           <p><strong><a name="Representing" id="Representing"></a>Representation</strong> is the valued expression of the perspectives or interests of constituents through
                              a trustee model where the representative is charged to make the best decision given
                              the information he or she has.
                           </p>
                           
                           <p><a name="FacOpinionPolicy" id="FacOpinionPolicy"><strong>Faculty Opinion on College Policy </strong></a> is the way that the College president hears the VCFA’s thoughts on policy matters
                              or changes. The VCFA does not vote on policy, but uses voting as a means to express
                              its opinion to the College president and the College’s Board of Trustees. At times,
                              the Faculty Council will express the opinion of the VCFA without a direct voting mechanism
                              unless clearly directed by the Constitution.
                           </p>
                           
                           <p><a name="Consensus" id="Consensus"><strong>Consensus</strong></a> is a process of continued discussion until all members honestly agree to one decision.
                           </p>
                           
                           <p><strong>Integrity</strong> is the expectation of a consistent adherence to ethical principles, methods, and
                              actions, which lead all constituents to feel decision-making is honest, open and inclusive.
                              The Valencia College shared governance system should create a policy creation implementation
                              tool that creates real solutions to actual problems that face a forward-looking college.
                           </p>
                           
                           <p><strong>Courage</strong> is the strength and determination to venture in new directions to achieve the goals
                              of the college and to persevere in the face of discouragement. The Valencia College
                              shared governance system not only serves as an important institution for designing
                              and implementing policy but also serves as a source of innovation at the college.
                           </p>
                           
                           <p><strong>Wisdom</strong> is the accumulation of knowledge, insight, and judgment needed to determine a course
                              of action when making decisions and the realization that uncertainty plays a role
                              in making difficult decisions. The Valencia College shared governance system encourages
                              a wide range of talent, interests and ideas that draws from the real strength of the
                              college – the collective teaching and learning experiences of faculty, administration,
                              and staff.
                           </p>
                           
                           <p><strong>Trust</strong> is the assurance that all constituencies can rely on each other to demonstrate integrity
                              when decisions need to be made at the college. The Valencia College shared governance
                              model implies that decisions made are transparent and reflective of the input of college-wide
                              interests. The system is not a vehicle for implementing policies that are narrowly
                              supported by a small cadre of interests.
                           </p>
                           
                           <p><strong>Effectiveness </strong>is the ability to use our best wisdom, talent, and creativity to achieve the goals
                              of the college in an efficient and timely manner. The Valencia College shared governance
                              system is a reliable structure for tackling a wide variety of interests and policy
                              needs. The system should not be unable to manage strategic policy implementation.
                           </p>
                           
                           <p><strong>Creativity/Innovation </strong>is the ability of the College to join in bringing forth new solutions and ideas to
                              campus and college-wide concerns and needs. The Valencia College shared governance
                              system encourages creativity and innovation by allowing for a wide range of perspectives
                              to converge on policy creation.
                           </p>
                           
                           <p><a name="GovernRetreat" id="GovernRetreat"><strong>Annual Governance Retreat </strong></a> was first planned during the 2013-2014 redesign of Valencia College’s governance
                              model. This retreat shall occur in July of each year and is required for all members
                              of all governing councils. The purpose of the retreat is to allow for better coordination
                              between the three governing councils and to help both new and old members maintain
                              clarity as to roles and purpose of the councils and the governance system.
                           </p>
                           
                           
                           
                           
                           <p>Revised: December, 1994</p>
                           
                           <p>Revised: January, 1998</p>
                           
                           <p>Revised: October, 2004 </p>
                           
                           <p>Revised: May, 2011</p>
                           
                           <p>Final Revision Adopted: March, 2015</p>
                           
                           <h2>&nbsp;</h2>
                           </span>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/constitution/AssociationConstitution.pcf">©</a>
      </div>
   </body>
</html>