<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/constitution/FacultyCouncilBy-laws.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li><a href="/faculty/association/constitution/">Constitution</a></li>
               <li>Faculty Association</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Faculty Council By-laws</h2>
                        
                        <h3><strong>Section I: Membership</strong></h3>
                        
                        
                        <p>The Faculty Council will comprise the VCFA President, the VCFA Vice President, the
                           VCFA Past President, each campus Leadership Team (a campus Assembly President and
                           Vice-President) and one at-large Representative from each campus. A campus may choose
                           not to send a third representative from the campus.
                        </p>
                        
                        <p>Members of the Faculty Council subscribe to the following actions:</p>
                        
                        <ul>
                           
                           <li>value and attend all meetings of the Faculty Council and VCFA.</li>
                           
                           <li>present accurate and timely information to the VCFA members and bring their ideas,
                              questions, and concerns back to the Faculty Council.
                           </li>
                           
                           <li>be active and collegial in all dealings with the Leadership Team, other Representatives,
                              and VCFA members.
                           </li>
                           
                           <li>contribute time to the business of the VCFA, committees, and events.</li>
                           
                           <li>organize, enlist aid, and direct work in the VCFA.</li>
                           
                        </ul>
                        
                        <p>At the start of the Council year as described in <a href="#FCYear">Article V, Section 2.A,</a> each member of the Council will verbally state his or her acceptance of the five
                           statements above.
                        </p>
                        
                        <h3><strong>Section II: Meetings</strong></h3>
                        
                        <p>A quorum for any meeting or electronic voting of the Council shall be a majority of
                           the members.
                        </p>
                        
                        <p>The Council shall meet once a month on the second Thursday of the month except for
                           the month of August. These meetings shall take place at a location chosen by the VCFA
                           President in consultation with the Council. Additional meetings may be scheduled according
                           to the availability of the Council. Some months the meeting may be moved to accommodate
                           scheduling issues, such as the start of term, end of term, or a holiday such as Spring
                           Break.
                        </p>
                        
                        <p>Meetings shall begin at 2:00 p.m. unless prior notice has been given by the President
                           prior to the scheduled meeting date. A time change may not be made in the 24 hours
                           preceding a meeting. Most meetings shall last till 5:00.
                        </p>
                        
                        <p>If a representative is unable to attend a meeting, the representative shall notify
                           the VCFA President or Vice President. There are no alternates for Faculty Council
                           members nor may any member, including the VCFA Leadership Team, vote by proxy.
                        </p>
                        
                        <p>All members are expected to attend the <a href="#GovernRetreat">annual Governance retreat</a>, to be held in July of each year. 
                        </p>
                        
                        <h3><strong>Section III: Duties and Responsibilities</strong></h3>
                        
                        <p>Members represent and share the perspectives of the VCFA during Faculty Council meetings.
                           All Council Representatives are available to all members of the VCFA.
                        </p>
                        
                        <p>Members shall aid the President in communicating and gathering information from VCFA
                           members regarding issues affecting or of interest to the VCFA.
                        </p>
                        
                        <p>Because a core purpose of Faculty Council is convening conversations of over-riding
                           interest to the faculty and their professional practice, members should expect to
                           aid in the planning, promotion, and execution of these conversations, including but
                           not limited to hosting discussions on various campuses (including campuses beyond
                           any campus of prime responsibility).
                        </p>
                        
                        <p>The President or representative shall produce and distribute electronically the agenda
                           and any related documents for Council meetings at least 48 hours prior to the meeting.
                           The agenda shall be placed on the VCFA’s website by the Faculty Council’s administrative
                           assistant.
                        </p>
                        
                        <p>Many decisions will be made by <a href="#Consensus">consensus</a>. Other decisions will be made by a formal vote. The decisions on all votes will be
                           made by majority vote of members who are voting where quorum is present. The President
                           may vote on any matter where his or her vote can affect the outcome of the vote. Proxy
                           voting is prohibited.
                        </p>
                        
                        <p>Voting is a privilege and a responsibility. However, members cannot be compelled to
                           vote and should not vote on a question to which they have a direct personal or financial
                           interest. In that event, members may prefer to abstain from voting. Abstentions are
                           not recorded in the Meeting Summary.
                        </p>
                        
                        <p>The Faculty Council places members of the VCFA on councils, committees, and work teams.
                           The President may choose to communicate directly with the entire VCFA requesting volunteers,
                           or the Faculty Council may choose to nominate specific members based on knowledge
                           and expertise said members could provide to the assignment, or the President and Council
                           may choose to solicit campus-based representation, in which case the Assembly on each
                           campus will be tasked with providing a certain number of volunteers. In this latter
                           case, the decision of the Campus Assembly will be binding for the appointment. 
                        </p>
                        
                        <p>Meetings are open unless a majority of the Council vote to discuss a matter confidentially,
                           in which case they may reserve a part of their regular meeting for that business or
                           conduct a special meeting as a closed session. 
                        </p>
                        
                        <p>A representative from the Academic Affairs &amp; Planning Division will take meeting notes
                           at Faculty Council meetings. The President will review and edit where necessary the
                           notes; those meeting notes will subsequently be sent to the full Faculty Council as
                           a Meeting Summary for review and approval, whether electronically or during the next
                           meeting. Approval electronically is to be preferred in order to have the meeting notes
                           posted on the Association’s website. All members are expected to vote within one week
                           of being notified of the vote, if the meeting notes are sent electronically. 
                        </p>
                        
                        <h3><strong>Section IV: Amendments</strong></h3>
                        
                        <p>These By-Laws may be amended by a majority vote of the members of the Faculty Council
                           present and voting, provided that written or printed notice of the proposed amendment(s)
                           is given to all members of the Faculty Council at least two weeks prior to the balloting.
                        </p>
                        
                        
                        <p>Adopted: March, 2015 </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/constitution/FacultyCouncilBy-laws.pcf">©</a>
      </div>
   </body>
</html>