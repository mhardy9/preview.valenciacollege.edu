<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/constitution/2015Transitionplan.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li><a href="/faculty/association/constitution/">Constitution</a></li>
               <li>Faculty Association</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2><strong>Transition Plan for 2015-16 </strong></h2>
                        
                        <p>In order to guide a smooth transition from the old governance structure and Association
                           Constitution to the new structure and Constitution, the following procedures will
                           be in effect:
                        </p>
                        
                        <ol>
                           
                           <li>The current VCFA Vice President, Suzette Dohany will become Association President
                              May 2015, holding the Presidency through July 2016
                           </li>
                           
                           <li>The Association will elect a new Vice President in April 2015 with this person becoming
                              President in August 2016 as described in the proposed Constitution. 
                           </li>
                           
                           <li>Each Campus Assembly will make necessary changes to its respective By-Laws to ensure
                              elections for the Campus Assembly Leadership team are in compliance by no later than
                              spring 2016. This includes the following concepts: 
                           </li>
                           
                           <ol>
                              
                              <li>Election for new Vice President to happen in spring rather than late fall. </li>
                              
                              <li>The At-Large Representative be elected from the entire campus, and said election happen
                                 in spring. 
                              </li>
                              
                           </ol>
                           
                           <li>The members of the current Faculty Council will work with the Leadership Team to populate
                              the Council under the new Constitution. Each campus Assembly that currently has more
                              than 3 members (East, West) will determine which of the current members will remain
                              by holding a campus-wide election from among the current members. Osceola Campus will
                              keep its three current members until new elections needed in spring 2016. Winter Park
                              and Lake Nona Campuses will decide if it is feasible for them to send an At-Large
                              Representative to hold the position till new elections in spring 2016. These current
                              members (serving currently in January 2015) will serve through July 2016. 
                           </li>
                           
                        </ol>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/constitution/2015Transitionplan.pcf">©</a>
      </div>
   </body>
</html>