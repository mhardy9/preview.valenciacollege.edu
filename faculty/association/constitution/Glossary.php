<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Association | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/association/constitution/Glossary.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/association/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Association</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/association/">Association</a></li>
               <li><a href="/faculty/association/constitution/">Constitution</a></li>
               <li>Faculty Association</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2><strong>College-wide Faculty Association Definitions</strong></h2>
                        
                        <p>A mutual understanding of key words or concepts is critical for any organization or
                           civic entity. During the summer of 2013, representatives from each Valencia campus
                           and employee group met to develop design options for the College’s shared governance
                           system. As a part of that work, key terms were defined and are useful for our work
                           in the VCFA. 
                        </p>
                        
                        <p><a name="VisionShdGovt" id="VisionShdGovt"><strong>Vision for Shared Governance</strong></a><strong>:</strong> Shared governance at Valencia is the collaborative process of the college making
                           courageous, wise choices that moves our work forward. Using the strengths of diverse
                           individuals and giving a voice to various constituencies, everyone contributes with
                           good faith and effort, transparency and integrity. The process articulates clear expectations,
                           includes a timeframe, provides a communication plan, and allows for meaningful and
                           efficient participation. The conclusions are trusted, accepted, and respected by the
                           entire College as the work of good people making the best decision possible at the
                           time.
                        </p>
                        
                        <p><a name="Accountability" id="Accountability"><strong>Accountability</strong></a> is the establishment of individual and mutual ownership, responsibility, and obligation
                           including partners and constituents who are willing and able to make decisions, take
                           actions, and then convey and justify those decisions/actions.
                        </p>
                        
                        <p><a name="Collaboration" id="Collaboration"><strong>Collaboration</strong></a> is a process of working together and embracing different perspectives that includes
                           appropriate representatives or stakeholders to achieve a clear goal.
                        </p>
                        
                        <p>Principles of collaboration:</p>
                        
                        <ul>
                           
                           <li>The process by which the work proceeds should be clearly defined, including how participation
                              will be encouraged and how communication will be accomplished.
                           </li>
                           
                           <li>Representatives or stakeholders should be a diverse group with a broad view of college
                              functions, specialized expertise, or closeness to the work.
                           </li>
                           
                           <li>All participants should share their best ideas, but they must realize that the work
                              is shared and all will advocate for the outcome that evolves from the work.
                           </li>
                           
                        </ul>
                        
                        <p>Effective <a name="Communication" id="Communication"><strong>communication</strong></a> is the exchange of ideas, messages, and information between individuals and/or a
                           group that considers not only the audience and purpose but also leads to understanding
                           although not necessarily agreement. The sender and the receiver are both responsible
                           for listening.
                        </p>
                        
                        <p><a name="Representation" id="Representation"><strong>Representation</strong></a> is the valued expression of the perspectives or interests of constituents through
                           a trustee model where the representative is charged to make the best decision given
                           the information he or she has. 
                        </p>
                        
                        <p><a name="FacOpinionPolicy" id="FacOpinionPolicy"><strong>Faculty Opinion on College Policy </strong></a> is the way that the College president hears the VCFA’s thoughts on policy matters
                           or changes. The VCFA does not vote on policy, but uses voting as a means to express
                           its opinion to the College president and the College’s Board of Trustees. At times,
                           the Faculty Council will express the opinion of the VCFA without a direct voting mechanism
                           unless clearly directed by the Constitution.
                        </p>
                        
                        <p><a name="Consensus" id="Consensus"><strong>Consensus</strong></a> is a process of continued discussion until all members honestly agree to one decision.
                        </p>
                        
                        <p><strong>Integrity</strong> is the expectation of a consistent adherence to ethical principles, methods, and
                           actions, which lead all constituents to feel decision-making is honest, open and inclusive.
                           The Valencia College shared governance system should create a policy creation implementation
                           tool that creates real solutions to actual problems that face a forward-looking college.
                        </p>
                        
                        <p><strong>Courage</strong> is the strength and determination to venture in new directions to achieve the goals
                           of the college and to persevere in the face of discouragement. The Valencia College
                           shared governance system not only serves as an important institution for designing
                           and implementing policy but also serves as a source of innovation at the college.
                        </p>
                        
                        <p><strong>Wisdom</strong> is the accumulation of knowledge, insight, and judgment needed to determine a course
                           of action when making decisions and the realization that uncertainty plays a role
                           in making difficult decisions. The Valencia College shared governance system encourages
                           a wide range of talent, interests and ideas that draws from the real strength of the
                           college – the collective teaching and learning experiences of faculty, administration,
                           and staff.
                        </p>
                        
                        <p><strong>Trust</strong> is the assurance that all constituencies can rely on each other to demonstrate integrity
                           when decisions need to be made at the college. The Valencia College shared governance
                           model implies that decisions made are transparent and reflective of the input of college-wide
                           interests. The system is not a vehicle for implementing policies that are narrowly
                           supported by a small cadre of interests.
                        </p>
                        
                        <p><strong>Effectiveness </strong>is the ability to use our best wisdom, talent, and creativity to achieve the goals
                           of the college in an efficient and timely manner. The Valencia College shared governance
                           system is a reliable structure for tackling a wide variety of interests and policy
                           needs. The system should not be unable to manage strategic policy implementation.
                        </p>
                        
                        <p><strong>Creativity/Innovation </strong>is the ability of the College to join in bringing forth new solutions and ideas to
                           campus and college-wide concerns and needs. The Valencia College shared governance
                           system encourages creativity and innovation by allowing for a wide range of perspectives
                           to converge on policy creation.
                        </p>
                        
                        <p><a name="GovernRetreat" id="GovernRetreat"><strong>Annual Governance Retreat </strong></a> was first planned during the 2013-2014 redesign of Valencia College’s governance
                           model. This retreat shall occur in July of each year and is required for all members
                           of all governing councils. The purpose of the retreat is to allow for better coordination
                           between the three governing councils and to help both new and old members maintain
                           clarity as to roles and purpose of the councils and the governance system.
                        </p>
                        
                        
                        <p>Created: July, 2013</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/association/constitution/Glossary.pcf">©</a>
      </div>
   </body>
</html>