<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Focus On the Workplace | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/focus-on-the-workplace/test_employer.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/focus-on-the-workplace/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Focus On the Workplace</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/focus-on-the-workplace/">Focus On The Workplace</a></li>
               <li>Focus On the Workplace</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2 class="v-red">Employer Testimonials</h2>
                        
                        <h3><strong>Testimonials and Insights from Employers who have Participated:<br><br></strong></h3>
                        
                        <p>"<em>Focus on the Workplace is a great program. We are pleased that Valencia is interested
                              in bringing "real life" experience into the classroom."<strong><br> </strong></em><strong><span>Universal Studios <br> </span></strong></p>
                        
                        <p><em>"Partnerships are based on mutual understanding. The experience not only broadened
                              our understanding of each other's institution, it has opened the door for future collaborative
                              projects based on our mutual goals."</em><strong><br> <span>Orlando Science Center <br> </span></strong></p>
                        
                        <p><em>“Participating in Focus on the Workplace has given us unique insight from an academic
                              perspective, while giving Valencia a better understanding of the needs and dynamics
                              of today’s workplace.”</em><em><br> </em><span><strong>Progress Energy <br> </strong></span></p>
                        
                        <p><em>“The partnership is great! Focus on the Workplace is structured to meet the needs
                              of both the business and the educator with an understanding that both sides are committee
                              to each other. I know of no other where the participant and the host company receive
                              such mutual benefit.”<br></em><strong>WKMG-TV Channel 6&nbsp;</strong></p>
                        
                        <p><em>“This is a brilliant idea! Get the professor to submerge their self in current industry
                              issues and bring back to the college classroom a wealth of common, current examples.
                              The result is both education and industry wins. What a merger!”</em><br> <span><strong>Florida Hospital</strong> </span><strong><br> </strong></p>
                        
                        <p><em>“Focus on the Workplace is an excellent program! It gives new insight into the importance
                              of connecting classroom theory with achieving successful outcomes in the workplace.
                              ”</em><em><br> </em><span><strong>The Orlando Sentinel</strong> </span></p>
                        
                        <p><em>“Focus on the Workplace is a great way to allow the corporate world to build better
                              communication bridges with the academic world.”</em><br> <span><strong>Rosen Centre Hotel</strong> </span></p>
                        
                        <p><em>“Valencia is to be commended for its proactive efforts to learn more about the requirements
                              of today's workforce. Collaboration is essential in today's business environment,
                              and we are very appreciative of the opportunity we had to form a reciprocal relationship
                              with Valencia.”<br> </em><span><strong>Orange County Government</strong> </span></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/focus-on-the-workplace/test_employer.pcf">©</a>
      </div>
   </body>
</html>