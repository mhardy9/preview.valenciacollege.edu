<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Focus On the Workplace | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/focus-on-the-workplace/companies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/focus-on-the-workplace/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Focus On the Workplace</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/focus-on-the-workplace/">Focus On The Workplace</a></li>
               <li>Focus On the Workplace</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2 class="v-red">Worksites:</h2>
                        
                        <h3><strong>How do I find a Worksite?</strong></h3>
                        
                        <p>You are not responsible for locating your own worksite, however if you have any personal
                           contacts or specific preferences, please let the coordinator know. This will make
                           the process much easier. The coordinator will work with you to find a place for you
                           to work, and will contact the host business to see if they would like to participate.
                           You may choose your own business or you may want to work at one of the businesses
                           that have previously participated below.
                        </p>
                        
                        <p>Once a worksite has been located, then an orientation meeting will be set up with
                           you, the coordinator and the business to introduce yourself, share about your background
                           and areas of expertise, discuss your goals and objectives for participating, and talk
                           about some specific things or projects you might like to do. Note: This is not a job
                           shadowing experience. We want you to get "hands on" experience and feel like you are
                           contributing sometime valuable to the host business while you are there.
                        </p>
                        
                        <h3><strong>What Businesses have Previously Participated?</strong></h3>
                        
                        <p><em>The following is a list of businesses and organizations that have participated in
                              Focus on the Workplace:</em></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Akerman, Senterfitt &amp; Edison, P.A.</li>
                                       
                                       <li>Arnold Palmer Hospital</li>
                                       
                                       <li>Center for Family Practice &amp; Sports Medicine</li>
                                       
                                       <li>CIRENT Semiconductor</li>
                                       
                                       <li>City of Kissimmee</li>
                                       
                                       <li>City of Orlando</li>
                                       
                                       <li>Corporate Investment, Int'l</li>
                                       
                                       <li>Cox Radio</li>
                                       
                                       <li>Denault Construction Corporation</li>
                                       
                                       <li>Digitec Creative Media Solutions</li>
                                       
                                       <li>Economic Development Commission</li>
                                       
                                       <li>Florida Hospital</li>
                                       
                                       <li>Florida Power Corporation</li>
                                       
                                       <li>Greater Orlando Aviation Authority</li>
                                       
                                       <li>Habitat for Humanity</li>
                                       
                                       <li>Harcourt, Inc.</li>
                                       
                                       <li>Health Central Hospital</li>
                                       
                                       <li>Hope Community Center</li>
                                       
                                       <li>IATSE Local 631 Stagehands</li>
                                       
                                       <li>International House of Blues Foundation</li>
                                       
                                       <li>Interpretak</li>
                                       
                                       <li>Investment Trust Company of Fla.</li>
                                       
                                       <li>Kissimmee Utility Authority</li>
                                       
                                       <li>Lucent Technologies</li>
                                       
                                       <li>Mennello Museum of American Art</li>
                                       
                                       <li>National Training Center for Sports, Health &amp; Fitness</li>
                                       
                                       <li>Nemours Children’s Clinic</li>
                                       
                                       <li>New Hope for Kids</li>
                                       
                                       <li>Orange County Convention Center</li>
                                       
                                       <li>Orange County Government</li>
                                       
                                       <li>Orange County Health Department</li>
                                       
                                       <li>O.C. Supervisor of Elections Office</li>
                                       
                                       <li>Orlando Regional Healthcare System</li>
                                       
                                       <li>Orlando Opera</li>
                                       
                                       <li>Orlando Science Center</li>
                                       
                                       <li>Orlando Sentinel</li>
                                       
                                       <li>Osceola County Government</li>
                                       
                                       <li>Osceola County Historical Society</li>
                                       
                                       <li>Osceola Mental Health, Inc.</li>
                                       
                                       <li>PBS &amp; J</li>
                                       
                                       <li>Pediatric Services of America</li>
                                       
                                       <li>Ritz Carlton and JW Mariott</li>
                                       
                                       <li>Rosen Centre Hotel</li>
                                       
                                       <li>SeaWorld Adventure Park</li>
                                       
                                       <li>StarMedia, Inc.</li>
                                       
                                       <li>SunTrust, Inc.</li>
                                       
                                       <li>Swiger Appraisal Co.</li>
                                       
                                       <li>The Nature Conservancy</li>
                                       
                                       <li>The Tile Market</li>
                                       
                                       <li>Turning Point of Central Florida</li>
                                       
                                       <li>Universal Orlando</li>
                                       
                                       <li>USDA Forest Service</li>
                                       
                                       <li>U.S. Geological Survey</li>
                                       
                                       <li>Walt Disney World Co.</li>
                                       
                                       <li>Well's Built Museum of African American History</li>
                                       
                                       <li>Westgate Resorts</li>
                                       
                                       <li>WESH-TV Channel 2</li>
                                       
                                       <li>WFTV Channel 9</li>
                                       
                                       <li>WKMG-TV Channel 6</li>
                                       
                                       <li>YMCA</li>
                                       
                                       <li>York Financial Group</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>&nbsp;</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/focus-on-the-workplace/companies.pcf">©</a>
      </div>
   </body>
</html>