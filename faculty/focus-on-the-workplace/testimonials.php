<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Focus On the Workplace | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/focus-on-the-workplace/testimonials.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/focus-on-the-workplace/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Focus On the Workplace</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/focus-on-the-workplace/">Focus On The Workplace</a></li>
               <li>Focus On the Workplace</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2 class="v-red">Faculty Testimonials</h2>
                        
                        <h3><strong>Testimonials and Insights from Faculty who have Participated:<br><br></strong></h3>
                        
                        <p><em>"Focus on the Workplace is an excellent opportunity to see an academic discipline
                              in action. I have personally participated in Focus on the Workplace several times
                              over the last ten years and truly believe it is the best -- not to mention -- most
                              exciting professional development opportunity provided to Valencia faculty."</em><strong><em><strong><br> </strong></em><strong><span>Mark Guillette </span></strong><em><strong><br> </strong></em></strong>Professor of Sociology, Osceola Campus <strong><em><br> </em></strong></p>
                        
                        <p><em>"Focus on the Workplace has been one of the most educational and beneficial experiences
                              I have ever participated in! My experience at the Orlando Hyatt Grand Cypress has
                              given me unique insight into the industry that I could never have seen from the outside.
                              Everyone wants to know what is going on in today's industry. Now that I have had the
                              opportunity to witness it first-hand, I have current, relative information that I
                              can bring back to the classroom and share with my students. There is no question that
                              my experience has helped me to become a better educator."<strong><br> </strong></em><strong><span>Jim Inglis</span></strong><em><strong><br> </strong></em>Professor of Hospitality, West Campus<br> <br> <em>"Why should you participate in Focus on the Workplace? Good question! It is a fantastic
                              opportunity to leave the cocoon of your classroom and morph into an instructor with
                              a clearer view of today's changing world. Through my experiences, I have gained new
                              insight into the world of business that will greatly benefit and impact my students."</em> <br> <span><strong>Teresa Nater</strong></span><br> Professor of English
                        </p>
                        
                        <p><em>"This experience has allowed me to examine the business world from a different perspective
                              and has given me a better understanding of the needs and dynamics of today's workplace.
                              I would encourage all faculty to participate in this exciting and rewarding program."</em><strong><em><br> </em></strong><span><strong>Ron Colburn</strong></span><strong><br> </strong>Speech Professor, Osceola Campus<strong><br> </strong></p>
                        
                        <p><em>"Working with the Orlando Sentinel has been exciting and eye opening. I can't think
                              of a better way to see the connection between the classroom and the workplace than
                              through "Focus on the Workplace."</em><strong><em><br> </em></strong><span><strong>Helen Clarke </strong></span><strong><br> </strong>Professor of English/Communications, East Campus<strong><br> </strong></p>
                        
                        <p><em>"The knowledge and experiences that I have gained from my time at the county is invaluable
                              and could not have been achieved over a three year period sitting in a classroom and
                              reading textbooks."</em><strong><em><br> </em><span>Desmond Duncan, Jr. </span><br> </strong>Political Science Professor, West Campus
                        </p>
                        
                        <p><em>"Focus on the Workplace provided me with the opportunity to play a variety of active
                              roles in the dynamic and demanding environment of the WKMG-TV Channel 6 newsroom.
                              As a communications professor, this learning experience has given me great insight
                              regarding the many communication styles that thrive in the work environment. Learning
                              experiences in my classroom will now be enhanced by my integration of work skill applications
                              in academic instruction, resulting in students who are better prepared for the realities
                              of the workplace."<strong><br> </strong></em><strong><span>Bill Gombash </span><br> </strong>Speech Professor, East Campus
                        </p>
                        
                        <p><em>“My experience has been an invaluable learning experience that has given me a different
                              outlook on my role as an educator and what I must do to help my students become better
                              employees.”</em> <br> <span><strong>Irene Lindgren</strong> <br> </span>Foreign Language Professor, Osceola Campus
                        </p>
                        
                        <p><em>“The insights I gained will influence the material I teach, the methods of presenting
                              that material and the type of projects I assign. I can’t wait to share what I have
                              learned with my students.”</em><br> <span><strong>Mary Allen</strong> <br> </span>Professor of Speech , East Campus
                        </p>
                        
                        <p><strong><em>“</em></strong><em>In many ways, it validated the approach I have taken in my classes. It also provided
                              me with many ideas for ways to improve the teaching and learning that takes place
                              in my classes.”</em><br> <span><strong>Michael Shugg</strong><br> </span>Theater &amp; Entertainment Professor, East Campus
                        </p>
                        
                        <p><em>“Wow! Get your nose out of the books, role up your sleeves, and plunge in. What a
                              great way to sharpen your skills, improve your courses, and increase your credibility
                              at the same time!”</em><br> <span><strong>Jim Knapp</strong><br> </span>Biology Professor, East Campus
                        </p>
                        
                        <p><em>“The experience proved to me that the ability to continually improve and learn is
                              necessary for success in any workplace!”</em><br> <span><strong>David Rogers</strong><br> </span>English Professor, Osceola Campus
                        </p>
                        
                        <p>“It was exciting to be able to work towards a goal with tangible results while gaining
                           valuable insight into the types of skills workers of today must possess.”<br> <span><strong>Wendy Bush</strong> <br> </span>Mathematics Professor, Osceola Campus<br> <br> <br> <em><strong><br> </strong></em></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/focus-on-the-workplace/testimonials.pcf">©</a>
      </div>
   </body>
</html>