<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Focus On the Workplace | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/focus-on-the-workplace/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/focus-on-the-workplace/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Focus On the Workplace</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/focus-on-the-workplace/">Focus On The Workplace</a></li>
               <li>Focus On the Workplace</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2><em>Would you like to renew your enthusiasm and vigor and have a new, fresh perspective<br>
                              on what your teach? Then Focus on the Workplace is for you! </em></h2>
                        
                        <h2><font color="#000000"><strong>We are currently accepting applications for Faculty to participate <br>
                                 during the 2016-17 year... <br>
                                 Application deadline is October 28, 2016<br>
                                 <br>
                                 <span>Hurry and don't delay... Spaces are Limited <br>
                                    <br>
                                    </span></strong></font></h2>
                        
                        <p><strong><em>“This is a brilliant idea! Get the professors to submerge themselves in current industry
                                 issues and bring back to the college classroom <br>
                                 a wealth of common, current examples. The result is both education and industry wins.
                                 What a merger!”</em></strong><br>
                           <span>Florida Hospital </span><strong><br>
                              </strong></p>
                        
                        
                        
                        
                        <h3>Click here for a Video Testimonial:</h3>
                        
                        <p>
                           
                           
                           
                           <iframe allowfullscreen="" frameborder="0" height="265" src="http://www.youtube.com/embed/_ePnEfA18v8" width="510"></iframe>
                           
                           
                        </p>            
                        
                        
                        <h3>Purpose of Program:</h3>
                        
                        <p> “Focus on the Workplace” is a unique professional development program that is designed
                           to give full-time faculty the opportunity to step out of the realm of academia and
                           observe, work and learn in an exciting business environment related to their field
                           so they can learn more about workplace needs, trends and expectations. Faculty who
                           have participated return to their classroom with renewed enthusiasm and vigor, and
                           have a fresh new perspective on how they can use real world applications that better
                           connect classroom learning to the workplace. 
                        </p>
                        
                        <h3>Benefits of Participating: </h3>            
                        
                        <h3><em>Focus on the Workplace is an excellent opportunity to... </em></h3>
                        
                        <ul>
                           
                           <li>Gain first-hand knowledge about the changing job market</li>
                           
                           <li>Stay informed about current and expected workforce needs and trends</li>
                           
                           <li>Be exposed to the latest technological advances related to your field or discipline</li>
                           
                           <li>Learn first-hand, the skills students need to be successful and stay competitive in
                              today's workplace
                           </li>
                           
                           <li>Get a different outlook and a new perspective on your role as an educator</li>
                           
                           <li>Validate what you teach and do in the classroom</li>
                           
                           <li>Discover real world applications that will enhance the teaching and learning in your
                              classroom
                           </li>
                           
                           <li>Develop new, potential partnerships with the community </li>
                           
                        </ul>            
                        
                        <h3>Who is Eligible to Participate?</h3>
                        
                        <ul>
                           
                           <li>Full-Time tenured or tenure-track faculty</li>
                           
                           <li> Full-Time counselors</li>
                           
                           <li> Full-Time professional staff</li>
                           
                           <li> Full-Time educational and career advisors</li>
                           
                        </ul>
                        
                        <h3>When can I Participate?</h3>
                        
                        <h3>Faculty:</h3>
                        
                        <ul>
                           
                           <li>Eligible and approved faculty are released from two 3-credit hour classes 
                              (6 credit hours) to work with an organization or business during the 1st 6 weeks of
                              the summer (H1) at 
                              35 hours/week; or during the Spring Term -- for 16 weeks 
                              - 2 days/week. We will pay for an adjunct to teach your two classes 
                              while you are in the workplace and will assist you in finding a work site.<br>
                              <br>
                              Most have found that the Summer Term is usually easier and better 
                              since you have the continuity of being there for six straight 
                              weeks. However, others have found the Spring Term to be preferable. 
                              It's whatever works best for you.
                           </li>
                           
                        </ul>            
                        
                        <h3>Counselors, Advisors &amp; Professional Staff: </h3>
                        
                        <ul>
                           
                           <li>Counselors, Advisors &amp; Professional Staff are released for 1 - 2 weeks (40 to 80 hours).
                              You may participate 
                              anytime during the year at whatever time is most convenient for 
                              you -- just get your supervisor ’s/dean’s approval. 
                              You may also choose to split your time up and participate 2 days/week 
                              for 5 weeks if that works better for you.
                           </li>
                           
                        </ul>            
                        
                        <h3>How do I find a Work site?</h3>
                        
                        <p>The coordinator  will work with you to find a work site that meets your expectations.
                           However, it is helpful, if you already have some ideas of where you would like to
                           work or have some specific places in mind. If you have any contacts or connections,
                           please let us know. You may need to do some research or exploration on your own. You
                           are welcomed to review our list of previous employers who have participated, or to
                           suggest any new organizations or businesses that you are interested in. 
                        </p>            
                        
                        <h3>How will I be Paid?</h3>
                        
                        <p>You will continue to be paid through your regular college contract, 
                           but <strong>NOT</strong> as an overload or supplemental activity. Through a special Perkins grant, we will
                           pay for  an adjunct to replace you in the classroom while you  are in the workplace.
                           There are, however, a limited number of spaces available. So the earlier you get your
                           application in, the better chance you have of being selected to participate.
                        </p>
                        
                        <h3>How do I Apply?</h3>
                        
                        <p>Discuss your interest in participating with your Dean as soon as possible so they
                           can accommodate your release time  in the department schedule. Once you have your
                           Dean's approval, you will need to do the following: 
                        </p>
                        
                        <ul>
                           
                           <li>Come up with some ideas or suggestions of where you would like to work </li>
                           
                           <li>Complete a written narrative response defining the learning outcomes and benefits
                              you envision as a result of your participation
                           </li>
                           
                           <li>Complete the Focus on the Workplace Application with your Dean's signature and approval</li>
                           
                           <li>Update your  Resume</li>
                           
                           <li>Send to LeSena Jones  by the required deadline </li>
                           
                        </ul>            
                        
                        <p><strong>LeSena Jones</strong>, the Coordinator, will contact you regarding the next steps and assist you in locating
                           a host work site.
                        </p>
                        
                        <h3>Outcomes:</h3>
                        
                        <p>At the end of your Focus on the Workplace experience, you will 
                           be asked to complete a written report sharing a summary of your workplace activities,
                           what you learned,  how it will help you to be a better educator, and how it will benefit
                           your students.  You will also be asked to share about 
                           your experience with other educators in department meetings and 
                           other College forums to help recruit future participants. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/focus-on-the-workplace/default.pcf">©</a>
      </div>
   </body>
</html>