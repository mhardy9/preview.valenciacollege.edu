<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Focus On the Workplace | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/focus-on-the-workplace/benefits.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/focus-on-the-workplace/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Focus On the Workplace</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/focus-on-the-workplace/">Focus On The Workplace</a></li>
               <li>Focus On the Workplace</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2 class="v-red">Benefits:</h2>
                        
                        <h3><strong>Why Should I Participate?</strong></h3>
                        
                        <p><em> You will have the opportunity to learn more about: </em></p>
                        
                        <ul>
                           
                           <li>
                              
                              <p>the job market – what employers are looking for in today’s workers and the critical
                                 skills they will need to be successful in the future
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>“real world” applications that will improve the teaching and learning that takes<br> place in your classroom
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>future trends and technological advances that are taking place in the workplace</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>employment opportunities and career paths available for students (from entry- level
                                 to upper-level positions)
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>the “soft skills” that organizations value most in their employees (how employees
                                 are expected to communicate, perform, and work together as a team)
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <h3><strong>Other Benefits of the Program:</strong></h3>
                        
                        <ul>
                           
                           <li>
                              
                              <p>increases your effectiveness as an educator by giving you first-hand knowledge and
                                 experience
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>enables you to build new partnerships with business and industry</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>validates what you teach and do in the classroom</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>gives you a new, fresh perspective</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>enables you to see your academic discipline first-hand in action</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>increases your ability to communicate business &amp; industry needs and expectations to
                                 students and co-workers
                              </p>
                              
                           </li>
                           
                           <li>gives employers an increased understanding of the role Valencia plays in workforce
                              Development and measures taken by the College to prepare students for the workplace
                           </li>
                           
                        </ul>
                        
                        <h2>&nbsp;</h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/focus-on-the-workplace/benefits.pcf">©</a>
      </div>
   </body>
</html>