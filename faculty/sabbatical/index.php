<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sabbatical Leave | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/sabbatical/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/sabbatical/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sabbatical Leave</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li>Sabbatical Leave</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h2>Overview </h2>
                                    
                                    <p>The purpose of Sabbatical leave is to provide professional renewal for eligible faculty,
                                       administrators, and professional staff employees.A sabbatical should provide the individual
                                       a significant opportunity for new, or renewed, achievement and growth through activities
                                       such as study, research, writing, creative work and travel so that the individual’s
                                       teaching and/or professional effectiveness may be enhanced.Sabbatical leaves are not
                                       to be understood as deferred compensation or automatically approved.
                                    </p>
                                    
                                    <p>Sabbatical leave may be granted, upon application and approval, based upon established
                                       criteria for study, research, creative work, field observations or other pursuits
                                       or value to the individual and the College, so that they may return to their posts
                                       with renewed vigor, perspective, and insight.
                                    </p>
                                    
                                    <p>Opportunities for additional training, for improving skills and for maintaining currency
                                       in the field are understood to be included as a purpose of sabbatical leave.
                                    </p>
                                    
                                    <p><a href="documents/PresidentMemoforwebsite.pdf" target="_blank">Click here</a> to read a memo from Dr. Shugart about Sabbatical Leave at Valencia. 
                                    </p>
                                    
                                    <h2>Eligibility </h2>
                                    
                                    <p>Full-time tenured faculty, administrators, and professional staff with at least seven
                                       years of continuous qualifying Valencia service and not in the Deferred Retirement
                                       Option Program (DROP) are eligible to apply for a sabbatical leave. Eligible employees
                                       may apply for a sabbatical leave upon completion of six years of service but must
                                       complete seven years of continuous service before a sabbatical leave can commence.
                                       For more information, please refer to the <a href="../../general-counsel/policy/documents/Volume3D/3D-12-Sabbatical-Leave.pdf" target="_blank">Sabbatical Leave policy and procedure.</a> 
                                    </p>
                                    
                                    <p>General criteria for award will include exceptional service to the college, seniority,
                                       whether the sabbatical proposal provides for individual renewal or experience which
                                       will enhance the individual's ability to contribute to the College, and the ability
                                       of the affected college department or division to reallocate work during the proposed
                                       sabbatical.
                                    </p>
                                    
                                    <p>Please see the navigation bar to the left for the application and additional information
                                       related to Sabbatical Leave. 
                                    </p>                  
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/sabbatical/index.pcf">©</a>
      </div>
   </body>
</html>