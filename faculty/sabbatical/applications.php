<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sabbatical Leave | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/sabbatical/applications.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/sabbatical/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sabbatical Leave</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/sabbatical/">Sabbatical Leave</a></li>
               <li>Sabbatical Leave</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Application and Selection </h2>
                        
                        <p>Applicants  must submit the online application by<strong> Friday, October 13, 2017. </strong></p>
                        
                        <p><img alt="Timeline" height="352" src="sabbatical-timeline.jpg" width="770"></p>
                        
                        <ul>
                           
                           <li>Applicants must have a conversation with their supervisor and obtain his or her verbal
                              approval prior to submitting the application form. Download a pdf of the <a href="https://drive.google.com/file/d/0ByVmm2DQ37JFeGRQMjVjN3ZHOFU/view?usp=sharing" target="_blank">application form</a> to use as a guide for the conversation with your supervisor.
                           </li>
                           
                           <li> The supervisor is required to provide a plan for replacing the applicant's teaching/work
                              schedule. The document will be required on the application form.
                           </li>
                           
                           <li> The supervisor must discuss the plan with the campus president/vice president and
                              receive his or her verbal approval to continue with the application process.
                           </li>
                           
                           <li> The application form must be completed online by the applicant by the deadline: Friday,
                              October 13, 2017.
                           </li>
                           
                           <li> Once the application is submitted, the supervisor and campus president/vice president
                              will receive an electronic copy.
                           </li>
                           
                           <li> Organizational Development and Human Resources will check eligibility and will notify
                              applicants and supervisors if there are any issues with the applicant's eligibility.
                           </li>
                           
                           <li> All applications and plans are sent to the sabbatical selection committee chair on
                              or before Tuesday, October 31, 2017 and to all selection committee members on or before
                              Friday, November 3, 2017.
                           </li>
                           
                           <li> The committee will evaluate and make recommendations to the president by Tuesday,
                              January 2, 2018. All applicants granted their sabbatical leave will be notified by
                              the campus president or vice president no later than Monday, January 22, 2018.
                           </li>
                           
                        </ul>            
                        
                        <p><a href="https://form.jotformpro.com/72605196578972" target="_blank">APPLY NOW</a></p>
                        
                        
                        <h3><strong>Selection Committee</strong></h3>
                        
                        <p dir="auto">The Sabbatical Leave Committee is composed of 13 persons: two faculty members from
                           each of the campuses, two administrators, two professional staff employees, and the
                           assistant vice president of teaching &amp; learning, who will serve as chair. The Faculty
                           Association Board will be asked to nominate faculty representatives. The Professional
                           Staff leadership team will nominate professional staff representatives. The president
                           will appoint the administrators. Members shall serve one year terms that coincide
                           with the academic year.
                        </p>
                        
                        <p dir="auto">If you are interested in serving as a member of the annually appointed Sabbatical
                           Leave Committee, please look for communication from the Faculty Association and Professional
                           Staff Leadership Team.
                        </p>
                        
                        
                        <p><img alt="" height="52" src="share-the-love.png" width="157"></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/sabbatical/applications.pcf">©</a>
      </div>
   </body>
</html>