<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sabbatical Leave | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/sabbatical/recipients/2014-2015recipients.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/sabbatical/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sabbatical Leave</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/sabbatical/">Sabbatical Leave</a></li>
               <li><a href="/faculty/sabbatical/recipients/">Award Recipients</a></li>
               <li>Sabbatical Leave</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>2014-2015 Sabbatical Leave Recipients</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="Melody Boering" height="234" src="MelodyBoering.jpg" width="351"><strong>Melody Boeringer Hartnup</strong><br>
                                       Professor, Chemistry<br>
                                       Osceola Campus <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Karen Borglum" height="234" src="KarenBorglum.jpg" width="351"><br>
                                       <strong>Karen Marie Borglum</strong><br>
                                       Asst VP, Curriculum Development &amp; Articulation<br>
                                       Dowtown Center
                                       <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Marie Brady" height="234" src="MaryBrady_000.jpg" width="351"><br>
                                       <strong>Marie Brady</strong><br>
                                       Manager, Campus Operations <br>
                                       Osceola Campus
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Geraldine Gallagher" height="234" src="GeraldineGallagher_001.jpg" width="351"><br>
                                       <strong>Geraldine Gallagher</strong><br>
                                       Foundation President and CEO, Valencia Foundation <br>
                                       Downtown Center 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="Al Groccia" height="234" src="AlGroccia.jpg" width="351"><br>
                                       <strong>Al Groccia</strong><br>
                                       Professor, Mathematics<br>
                                       Osceola Campus
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>                    
                                    <p><img alt="Joe Menig" height="234" src="JoeMenig.jpg" width="351"><br>
                                       <strong>Joe Menig</strong><br>
                                       Professor, Spanish<br>
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Karen Murray" height="234" src="KarenMurray_000.jpg" width="351"><br>
                                       <strong>Karen Murray</strong><br>
                                       Professor, ESL<br>
                                       West Campus 
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Jennifer Taylor" height="234" src="JenniferTaylor_000.jpg" width="351"><br>
                                       <strong>Jennifer Taylor </strong><br>
                                       Professor, Humanities<br>
                                       East Campus 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/sabbatical/recipients/2014-2015recipients.pcf">©</a>
      </div>
   </body>
</html>