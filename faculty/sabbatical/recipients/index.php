<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sabbatical Leave | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/sabbatical/recipients/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/sabbatical/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sabbatical Leave</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/sabbatical/">Sabbatical Leave</a></li>
               <li>Award Recipients</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Congratulations to the 2018-2019 Sabbatical Leave Recipients  </h2>
                        
                        <h2>
                           <img alt="Sabbatical Recipients 18-19" height="372" src="Sabbatical.jpg" width="568"> 
                        </h2>
                        
                        <p>We are pleased to announce the 12 Valencia employees who have been selected by a group
                           of faculty, administrators and staff as the 2018-2019 sabbatical leave recipients:
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/featured-colleague-celeste-henry/">Celeste Henry</a>, counselor, West Campus
                           </li>
                           
                           <li>Chris Wettstein, librarian, East Campus</li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/featured-colleague-christina-hardin/">Christina Hardin</a>, professor, English, Osceola Campus
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/featured-colleague-diana-ciesko/">Diana Ciesko</a>, professor, psychology, East Campus
                           </li>
                           
                           <li>James Leonard,&nbsp;professor, English, East Campus</li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/featured-colleague-jamy-chulak/">Jamy Chulak</a>, program chair and professor, respiratory care, West Campus
                           </li>
                           
                           <li>Jenny Hu, professor, computer programming and analysis, Osceola Campus</li>
                           
                           <li>Kevin Matis, server administrator, senior, West Campus</li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/enhance-your-teaching-with-industry-work-participate-in-focus-on-the-workplace/">Mark Guillette</a>, professor, sociology, Osceola Campus
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/valencia-professors-discover-the-strength-and-hope-in-haiti-during-peace-education-mission/">Paul Chapman</a>, professor, humanities, Winter Park Campus
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/march-2015-kudos/#Innovation">Susan Dunn</a>, manager, credit programs, Winter Park Campus
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/featured-colleague-suzette-dohany/">Suzette Dohany</a>, professor, speech, Winter Park Campus
                           </li>
                           
                        </ul>            
                        
                        <h2>Past Recipients </h2>
                        
                        <p><a href="2017-2018Recipients.html">2017-2018 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2016-2017Recipients.html">2016-2017 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2015-2016_SabbaticalLeave_Recipients.html">2015-2016 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2014-2015Recipients.html">2014-2015 Sabbatical Leave Recipients</a> 
                        </p>
                        
                        <p><a href="2013-2014Recipients.html">2013-2014 Sabbatical Leave Recipients</a></p>
                        
                        <p><a href="2012-2013Recipients.html">2012-2013 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2011-2012Recipients.html">2011-2012 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2010-2011Recipients.html">2010-2011 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2009-2010Recipients.html">2009-2010 Sabbatical Leave Recipients</a> 
                        </p>
                        
                        <p><a href="2008-2009Recipients.html">2008-2009 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2007-2008Recipients.html">2007-2008 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2006-2007Recipients.html">2006-2007 Sabbatical Leave Recipients </a></p>
                        
                        <p><a href="2005-2006Recipients.html">2005-2006 Sabbatical Leave Recipients</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/sabbatical/recipients/index.pcf">©</a>
      </div>
   </body>
</html>