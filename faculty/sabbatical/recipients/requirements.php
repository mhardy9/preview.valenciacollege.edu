<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sabbatical Leave | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/sabbatical/recipients/requirements.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/sabbatical/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sabbatical Leave</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/sabbatical/">Sabbatical Leave</a></li>
               <li><a href="/faculty/sabbatical/recipients/">Award Recipients</a></li>
               <li>Sabbatical Leave</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Requirements</h2>
                        
                        <p><strong>Getting Ready for your Sabbatical Leave</strong><br>
                           <br>
                           Below are the steps that will take place before, during and after your Sabbatical
                           leave. 
                        </p>
                        
                        <p><strong>Before</strong></p>
                        
                        <ul>
                           
                           <li>Have a conversation with your supervisor so you are both in agreement about the dates
                              of your leave, and to ensure that your duties are covered during your absence.
                           </li>
                           
                           <li>Submit a Certificate of Absence through the “Request and Manage My Leave System” in
                              Atlas.&nbsp; From the drop-down menu for Type of Leave select “Sabbatical Leave”.&nbsp; <a href="http://www.evernote.com/shard/s283/sh/c2d319cf-9f4f-4e0b-bf7a-c25d53b6ff79/561d0323eb48c5ced8015b33b55a903a">Click here</a> for detailed instructions.
                           </li>
                           
                           <li>Your submission of this form will trigger internal processes in Human Resources to
                              ensure that your record shows that you are on sabbatical, and because your supervisor
                              will approve your leave, he/she will also have a copy of your leave.
                           </li>
                           
                        </ul>            
                        
                        <p><strong>During</strong></p>
                        
                        <ul>
                           
                           <li>Enjoy your time!</li>
                           
                           <li>You are relieved of all academic and administrative duties and responsibilities during
                              the period of your leave. You should not serve on college committees, faculty senates/associations,
                              or accept other similar assignments during the time of your sabbatical leave. Exceptions
                              may be made at the discretion of the College President.
                           </li>
                           
                        </ul>            
                        <p><strong>After</strong></p>
                        
                        <p>Submit your report</p>
                        
                        <ul>
                           
                           <li>Within four (4) months following completion of the Sabbatical leave, you will need
                              to submit a report summarizing the experiences, benefits and/or achievements resulting
                              from the Sabbatical leave.&nbsp; Your report will be forwarded to the Sabbatical leave
                              committee, the Human Resources office, and your dean.<br>
                              <br>
                              <a href="https://adobeformscentral.com/?f=se4qhqYNcni6AeOLs9wizg" target="_blank"><img alt="Submit Report" border="0" src="submit-report-button-300w.jpg"></a> <br>
                              
                           </li>
                           
                           <li>Expectation of future service<br>
                              Sabbatical leave is only awarded with the expressed provision that you will continue
                              in full-time service at the college for at least one (1) full year after completion
                              of the Sabbatical leave.
                           </li>
                           
                           <li>As you move forward with your sabbatical leave planning, please don’t hesitate to
                              contact your dean or me with any questions.&nbsp; 
                           </li>
                           
                           <li>Please take time to review the <a href="../../../generalcounsel/policy/default.cfm-policyID=213&amp;volumeID_1=15&amp;navst=0.html">sabbatical leave policy and procedure</a> (6Hx28: 3D-12) and make note of the requirements of the award.&nbsp; 
                           </li>
                           
                        </ul>            
                        
                        <p>Any substantive changes in your Sabbatical plans, including changes while the leave
                           is underway, must be submitted in writing and approved by your dean and campus president
                           for faculty, or supervisor and senior administrator for non-faculty and the chair
                           on behalf of the Sabbatical committee. The final decision on approval of modifications
                           of proposed plans rests with the President. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/sabbatical/recipients/requirements.pcf">©</a>
      </div>
   </body>
</html>