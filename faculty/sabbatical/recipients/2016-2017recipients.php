<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sabbatical Leave | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/sabbatical/recipients/2016-2017recipients.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/sabbatical/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sabbatical Leave</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/sabbatical/">Sabbatical Leave</a></li>
               <li><a href="/faculty/sabbatical/recipients/">Award Recipients</a></li>
               <li>Sabbatical Leave</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>2016-2017 Sabbatical Leave Recipients</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="Mailin Barlow" height="150" src="Barlow_Mailin.jpg" width="150"></p>
                                    
                                    <p><strong>Mailin Barlow </strong><br>
                                       Professor, English<br>
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Diane Brown" height="151" hspace="0" src="diane-brown-270w.jpg" vspace="0" width="150"></p>
                                    
                                    <p><strong>Diane Brown</strong><br>
                                       Professor, Humanities<br>
                                       Osceola
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Roberta Carew" height="149" hspace="0" src="CarewRobertaCC.jpg" vspace="0" width="150"></p>
                                    
                                    <p><strong>Roberta Carew </strong><br>
                                       Professor, Mathematics<br>
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Diane Dalrymple" height="150" src="Dalrymple_Diane.jpg" width="150"></p>
                                    
                                    <p><strong>Diane Dalrymple </strong><br>
                                       Librarian<br>
                                       East Campus 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="Ed Frame " height="150" src="Frame_Ed.jpg" width="150"></p>
                                    
                                    <p><strong>Edward Frame </strong><br>
                                       Professor, Humanities<br> 
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Jean Marie Fuhrman" height="150" src="Fuhrman_JeanMarie.jpg" width="150"></p>
                                    
                                    <p><strong>Jean Marie Fuhrman </strong><br>
                                       Professor, Reading<br>
                                       Winter Park Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Edie Gaythwaite" height="150" src="Gaythwaite_Edie.jpg" width="150"></p>
                                    
                                    <p><strong>Edie Gaythwaite </strong><br>
                                       Professor, Speech<br>
                                       East Campus
                                       <br>                  
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Wendy Givoglu" height="151" hspace="0" src="wendy-givoglu-270w.jpg" vspace="0" width="150"></p>
                                    
                                    <p><strong>Wendy Givoglu </strong><br>
                                       Dean, Arts/Entertainment<br>
                                       East Campus 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="Jeff Hogan" height="150" src="Hogan_Jeff.jpg" width="150"></p>
                                    
                                    <p><strong>Jeff Hogan </strong><br>
                                       Functional IS Support Specialist, Nursing Office <br>
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Sonya Joseph" height="150" src="Joseph_Sonya.jpg" width="150"></p>
                                    
                                    <p><strong>Sonya Joseph</strong> <br>
                                       Asst VP, Student Affairs<br>
                                       Lake Nona Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Ilyse Kusnetz" height="150" hspace="0" src="Kusnetz_Ilyse.jpg" vspace="0" width="150"></p>
                                    
                                    <p><strong>Ilyse Kusnetz </strong><br>
                                       Professor, English<br>
                                       Winter Park Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Michele Lima" height="150" hspace="0" src="michele-lima-270w.jpg" vspace="0" width="150"></p>
                                    
                                    <p><strong>Michele Lima </strong><br>
                                       Professor, Speech<br>
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="Jolene Rhodes" height="146" hspace="0" src="Rhodes_Jolene.jpg" vspace="0" width="150"></p>
                                    
                                    <p><strong>Jolene Rhodes </strong><br>
                                       Professor, Mathematics<br>
                                       East Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Cheryl Robinson" height="150" src="Robinson_Cheryl.jpg" width="150"></p>
                                    
                                    <p>C<strong>heryl Robinson </strong><br>
                                       Director, Honors Program<br>
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Nicole Spottke" height="150" src="Spotke_Nicole.jpg" width="150"></p>
                                    
                                    <p><strong>Nicole Spottke </strong><br>
                                       Professor, English<br>
                                       West Campus 
                                    </p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>  
                        
                        
                        <h2>&nbsp; </h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/sabbatical/recipients/2016-2017recipients.pcf">©</a>
      </div>
   </body>
</html>