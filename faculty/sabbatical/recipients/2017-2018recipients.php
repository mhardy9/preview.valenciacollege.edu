<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sabbatical Leave | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/sabbatical/recipients/2017-2018recipients.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/sabbatical/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Sabbatical Leave</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/sabbatical/">Sabbatical Leave</a></li>
               <li><a href="/faculty/sabbatical/recipients/">Award Recipients</a></li>
               <li>Sabbatical Leave</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>2017-2018 Sabbatical Leave Recipients</h2>
                        
                        <p> <img alt="2017-2018-sabbatical-recipients-grove" height="312" src="2017-2018-sabbatical-recipients-grove.jpg" width="570"> 
                        </p>
                        
                        <p>East Campus</p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/faculty-share-best-practices-at-second-valencia-math-conference/">Jody DeVoe</a>, professor, mathematics
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/east-unveils-new-bachelors-degree/">Debbie Drobney</a>, professor, sign language
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/troy-gifford-plays-truth-in-10-songs/">Troy Gifford</a>, professor, music
                           </li>
                           
                           <li>Celine Kavalec, faculty director, Teaching/Learning Academy</li>
                           
                           <li>Karen Styles, professor, humanities</li>
                           
                        </ul>
                        
                        <p>West Campus</p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/karen-cowden-reflects-on-role-as-faculty-fellow/">Karen Cowden</a>, professor, reading
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/tech-professor-josh-murdock-test-drives-google-glass/">Josh Murdock</a>, instructional designer/developer
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/in-the-kitchen-with-chef-pierre-pilloud-and-valencias-culinary-management-program/">Pierre Pilloud</a>, professor, culinary management
                           </li>
                           
                           <li>
                              <a href="http://thegrove.valenciacollege.edu/by-karen-reilly-you-may-have-heard-the-name-seen-the-redorange-logo-around-west-campus-or-perhaps-even-attended-a-training-session-on-one-of-our-latest-large-scale-pilots-smarthinking-the-divisio/">Irina Struganova</a>, professor, physics
                           </li>
                           
                        </ul>            
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/sabbatical/recipients/2017-2018recipients.pcf">©</a>
      </div>
   </body>
</html>