<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>CARE Program | Valencia College</title>
      <meta name="Description" content="A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society">
      <meta name="Keywords" content="college, education, certificate, degree, credit, undergraduate, continuing education"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/care/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/care/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li>Care</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="top" id="top"></a>
               
               <div id="accessibility">
                  <a href="index.html#navigate" class="trackWebHeader">Skip to Local Navigation</a> | <a href="index.html#content" class="trackWebHeader">Skip to Content</a>
                  
               </div>
               
               <div id="outContainerTopBar">
                  
                  <div id="topBar">
                     
                     <div id="logoandlogin">
                        
                        <div id="logo">    
                           
                           
                           <a href="../../index.html" class="trackWebHeader" aria-label="Valencia College"><img src="https://valenciacollege.edu/images/logos/valencia-college-logo.svg" alt="Valencia College" width="275" height="42" role="img"></a>
                           
                           
                        </div>
                        
                        <div class="AtlasLoginContainer">
                           <a href="https://atlas.valenciacollege.edu/" title="Atlas Login" style="text-decoration:none;" class="trackWebHeader"><span class="AtlasIcon"></span> <span class="AtlasText">ATLAS LOGIN</span></a>
                           
                        </div>
                        
                        <div style="clear:both;"></div>
                        
                     </div>
                     
                     
                     
                  </div>
                  
                  
                  <div id="navOuterContainer">
                     
                     <div id="nav" style="margin-left:auto; margin-right:auto; width:980px;">
                        
                        <ul class="navMenu">
                           
                           <li class="navLinks"><a href="http://preview.valenciacollege.edu/future-students/" id="nav-future" class="trackWebHeader">Future Students</a></li>
                           
                           <li class="spacer">
                              
                           </li>
                           <li class="navLinks"><a href="../../students/index.html" id="nav-current" class="trackWebHeader">Current Students</a></li>
                           
                           <li class="spacer">
                              
                           </li>
                           <li class="navLinks"><a href="../../faculty.html" id="nav-faculty" class="trackWebHeader">Faculty &amp; Staff</a></li>
                           
                           <li class="spacer">
                              
                           </li>
                           <li class="navLinks"><a href="../../visitors.html" id="nav-visitors" class="trackWebHeader">Visitors &amp; Friends</a></li>
                           
                           <li class="spacer">
                              
                           </li>
                           <li class="blank">
                              
                              <div class="util">
                                 
                                 <ul class="x_level1">
                                    
                                    <li class="x_quicklinks">
                                       <a href="../../includes/quicklinks.html" style="text-decoration:none;">Quick Links</a>
                                       
                                       <ul class="x_level2">
                                          
                                          <li class="x_quicklinks2"><a href="../../calendar/index.html" class="trackWebHeader">Academic Calendar</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../students/admissions-records/index.html" class="trackWebHeader">Admissions</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="https://learn.valenciacollege.edu/" class="trackWebHeader">Blackboard Learn</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../../students/locations-store/index.html" class="trackWebHeader">Bookstore</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../../business-office/index.html" class="trackWebHeader">Business Office</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../map/index.html" class="trackWebHeader">Campuses</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../../calendar/index.html" class="trackWebHeader">Calendars</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../../students/catalog/index.html" class="trackWebHeader">Catalog</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="http://valenciacollege.edu/schedule/" class="trackWebHeader">Class Schedule</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="http://valenciacollege.edu/continuing-education/" class="trackWebHeader">Continuing Education</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../../academics/programs/index.html" class="trackWebHeader">Degree &amp; Career Programs</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../../departments/index.html" class="trackWebHeader">Departments</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="http://valenciacollege.edu/economicdevelopment/" class="trackWebHeader">Economic Development</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../human-resources/index.html" class="trackWebHeader">Employment</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../finaid/index.html" class="trackWebHeader">Financial Aid Services</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../../international/index.html" class="trackWebHeader">International</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../library/index.html" class="trackWebHeader">Libraries</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../map/index.html" class="trackWebHeader">Locations</a></li>
                                          
                                          
                                          <li class="x_quicklinks2"><a href="../../contact/directory.html" class="trackWebHeader">Phone Directory</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../students/admissions-records/index.html" class="trackWebHeader">Records/Transcripts</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../security/index.html" class="trackWebHeader">Security/Parking</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="../support/index.html" class="trackWebHeader">Support</a></li>
                                          
                                          <li class="x_quicklinks2"><a href="http://valenciacollege.edu/events/" class="trackWebHeader">Valencia Events</a></li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </li>
                           
                           <li class="spacer">
                              
                           </li>
                           <li class="blank">
                              
                              <div class="util">
                                 
                                 <div class="x_search">
                                    
                                    <form label="search" class="relative" action="http://search.valenciacollege.edu/search" name="seek" method="get" id="seek">
                                       <label for="q"><span style="display:none;">Search</span></label>
                                       <input type="text" name="q" id="q" alt="Search" value="">
                                       
                                       <button style="position:absolute; top:0; right:12;" type="submit" name="submitMe" value="seek" onmouseover="clearTextSubmit(document.seek.q)" onmouseout="clearTextSubmitOut(document.seek.q)">go</button>
                                       <input type="hidden" name="site" value="default_collection">
                                       <input type="hidden" name="client" value="default_frontend">
                                       <input type="hidden" name="proxystylesheet" value="default_frontend">
                                       <input type="hidden" name="output" value="xml_no_dtd">
                                       
                                    </form>
                                    
                                 </div>
                                 
                              </div>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               <div id="containerMain">
                  
                  
                  <div id="containerContent" role="main">
                     
                     
                     <div class="container_12" style="padding-top:20px;">
                        
                        
                        <div class="grid_2">
                           
                           <div id="left-navigation" style="width:150px;height:262px">
                              
                              <div class="Navigation" style="width:150px">
                                 <a href="../east/academic-success/math/index.html" class="Group">MATH CENTER</a>
                                 <a href="../east/academic-success/writing/index.html" class="Group">COMMUNICATIONS</a>
                                 <a href="../east/academic-success/tutoring/index.html" class="Group">GENERAL TUTORING</a>
                                 <a href="../east/academic-success/languageLabs.html" class="Group">LANGUAGE LABS</a>
                                 <a href="../east/academic-success/eap/index.html" class="Group">EAP LAB</a>
                                 <a href="../east/academic-success/testing/index.html" class="Group">TESTING CENTER</a>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div id="featured" class="grid_10" style="width:770px">
                           
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/kFH1aP6x2Y4" frameborder="0" allowfullscreen="" align="middle"></iframe>
                           
                        </div>    
                        
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  <div id="containerContent" role="main">
                     
                     
                     <div class="container_12" style="padding-top:20px;">
                        
                        
                        <div class="grid_12">
                           
                           <h1 class="style5" style="text-align:justify; margin-bottom: 0;">CARE Strategies - (LFMP 3348) - Early Alert System</h1>
                           
                        </div>
                        
                        
                        <div class="grid_12" style="height:37px;padding-bottom:20px;">
                           
                           <table class="table ">
                              
                              <tr valign="top">
                                 
                                 <td width="306">
                                    <div align="center" class="style3"><a href="http://issuu.com/nrcpubs/docs/e-source_13.2/11?e=5049601/34019425" class="style14">CARE History </a></div>
                                 </td>
                                 
                                 <td width="263">
                                    <div align="center" class="style3"><a href="http://libguides.valenciacollege.edu/c.php?g=484852" class="style14">CARE Resources </a></div>
                                 </td>
                                 
                                 <td width="274">
                                    <div align="center" class="style3"><a href="../faculty/development/howToRegisterForCourses.html" class="style14">Register for CARE </a></div>
                                 </td>
                                 
                              </tr>
                              
                           </table>
                           
                        </div>
                        
                        
                        <div class="grid_12">
                           
                           <h1 class="style5" style="margin-top: 0; margin-bottom: 0;">What is CARE? </h1>
                           
                        </div>
                        
                        
                        <div class="grid_12">
                           
                           <div align="left" class="style3">
                              
                              <p class="style3">CARE,  which stands for <u><span class="style14">C</span></u>ontinuous <u><span class="style14">A</span></u>ssessment and <u><span class="style14">R</span></u>esponsive <u><span class="style14">E</span></u>ngagement, is a faculty-led initiative aimed at establishing a systematic process
                                 for identifying and supporting struggling students. In other words, CARE represents
                                 an Early Alert System at Valencia. Through the CARE course, faculty develop their
                                 own CARE strategies to teach students the skills to self-assess their study habits
                                 “early” in the semester. Students also learn what adjustments need to be made to improve
                                 their academic performance. In this class, faculty will research and develop intervention
                                 strategies for struggling students that are grounded in continuous assessment and
                                 responsive engagement for the purpose of ensuring student learning. Participants will
                                 employ best practices in the area of early alert to design and implement a CARE strategy
                                 in their course. 
                              </p>
                              
                              <p style="margin-bottom: 0;">&nbsp;</p>
                              
                              <p align="left" class="style3" style="margin-top: 0;">Click<span class="style10"> <a href="https://youtu.be/6nk9v4S1UuI" class="style14" style="color:#bf311a">HERE</a> </span>to view a CARE testimonial.
                              </p>
                              
                              <p align="left" class="style3" style="margin-bottom: 0;">&nbsp; </p>
                              
                              <p style="margin-top: 0;"><img src="http://valenciacollege.edu/care/images/care-design-principles-website_004.png" width="582" height="368" alt="dp"> <img src="http://valenciacollege.edu/care/why-care-website.jpeg" width="311" height="284" alt="why"></p>
                              
                              <p style="margin-bottom: 0;">&nbsp;</p>
                              
                              <div class="grid_12">
                                 
                                 <h1 align="center" class="tableHead_1">How CARE Works </h1>
                                 
                                 <p style="margin-bottom: 0;">&nbsp;</p>
                                 
                                 <p align="left" style="margin-top: 0;"><img src="http://valenciacollege.edu/care/care-learning-outcomes-website_005.jpeg" width="531" height="421" alt="lo"> <img src="http://valenciacollege.edu/care/images/care-logoblacknoline_002.png" width="381" height="405" alt="lo"></p>
                                 
                                 <p align="left" style="margin-bottom: 0;">&nbsp;</p>
                                 
                              </div>
                              
                              <div class="grid_12">
                                 
                                 <p align="left" class="style3" style="margin-bottom: 0;"><img src="http://valenciacollege.edu/care/care-overviewforwebsite_001.jpeg" width="728" height="518" alt="o"></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>    
                        
                        
                        <div class="grid_12">
                           
                           <p align="center" class="style12" style="margin-bottom: 0;">&nbsp;</p>
                           
                           <p align="center" class="tableHead_1" style="margin-top: 0;">Where to CARE </p>
                           
                           <p align="center" class="style12"><img src="http://valenciacollege.edu/care/care-schedule16-17_002.jpeg" width="686" height="432" alt="sc"></p>
                           
                           <p align="center" class="style12" style="margin-bottom: 0;">&nbsp;</p>
                           
                           <p align="center" class="style12" style="margin-top: 0; margin-bottom: 0;">Click <a href="../faculty/development/howToRegisterForCourses.html" class="style10" style="color:#bf311a">HERE</a> to register for this class. 
                           </p>
                           
                        </div>      
                        
                        
                        
                        <div id="containerMain">
                           
                           <div class="clear style6">
                              
                              <div align="center">
                                 <br>
                                 
                              </div>
                              
                           </div>           
                           
                        </div> 
                        
                        
                        
                        
                        <div id="containerMain">
                           
                           <div class="grid_6"></div>   
                           
                           
                           <div class="grid_6">
                              
                              <div class="grid_12"></div>
                              
                              <div class="grid_12"></div>
                              
                              <p style="margin-bottom: 0;"><br>
                                 
                              </p>
                              
                           </div> 
                           
                        </div> 
                        
                        
                        
                        
                        <div id="containerMain">
                           
                           <div class="grid_12"></div> 
                           
                           <div class="clear style6 style10 style12"><br></div>
                           
                        </div> 
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>  
                  
                  
               </div>
               
               <div id="containerBtm">
                  
                  
                  
                  
                  
               </div>
               
               
               
               
               
               
               
               <noscript>
                  
                  <div style="display:inline;">
                     <img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/967106136/?value=0&amp;guid=ON&amp;script=0">
                     
                  </div>
                  
               </noscript>
               
               
               
               
               
               <noscript>
                  <iframe src="http://20677575p.rfihub.com/ca.html?rb=20089&amp;ca=20677575&amp;ra=YOUR_CUSTOM_CACHE_BUSTER" style="display:none;padding:0;margin:0" width="0" height="0"></iframe>
                  
               </noscript>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/care/index.pcf">©</a>
      </div>
   </body>
</html>