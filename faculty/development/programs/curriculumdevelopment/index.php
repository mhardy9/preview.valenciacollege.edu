<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Instructional Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/curriculumdevelopment/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Instructional Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Curriculumdevelopment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <p>The Office of Faculty and Instructional Development supports all faculty members as
                           they develop course and curriculum experiences, in all modalities, to improve student
                           learning. A variety of courses, programs and one-to-one consultations are available
                           to support all faculty in a broad range of topics. 
                        </p>
                        
                        <h3>Student learning outcomes for programs and courses </h3>
                        
                        <blockquote>
                           
                           <ul>
                              
                              <li>Faculty Development Courses in  <a href="../../coursesResources/OutcomesBasedPractice.html" target="_blank">Outcomes-based Practice</a> and <a href="../../coursesResources/assessmentTools.html" target="_blank">Assessment </a>
                                 
                              </li>
                              
                              <li>Individual or group <a href="../../Consulation.html">consultations </a>
                                 
                              </li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        <h3>Course design and development in specialized curricular experiences            </h3>
                        
                        <blockquote>
                           
                           <ul>
                              
                              <li> Learning in Community </li>
                              
                              <li>Service Learning </li>
                              
                              <li>Study Abroad and Global Exchange</li>
                              
                              <li>Supplemental Learning </li>
                              
                              <li>Individual or group <a href="../../Consulation.html">consultations</a>
                                 
                              </li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        <h3>General course design in all instructional modalities (face-to-face, hybrid and online)
                           <br>
                           
                        </h3>
                        
                        <blockquote>
                           
                           <ul>
                              
                              <li>Face-to-Face Courses
                                 
                                 <ul>
                                    
                                    <li>Courses in <a href="../../coursesResources/OutcomesBasedPractice.html" target="_blank"> Outcomes-based Practice</a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Hybrid and Online
                                 Courses                          
                                 
                                 <ul>
                                    
                                    <li><a href="../online/BootCampOnlineInstruction.html" target="_blank">Boot Camp for Online Instruction</a></li>
                                    
                                    <li><a href="http://valenciacollege.edu/faculty/development/programs/exemplar/default.cfm" target="_blank">Quality Matters Program</a></li>
                                    
                                    <li><a href="../online/DigitalProfessorCertification.html" target="_blank">Digital Professor Certification Program</a></li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Individual or group <a href="../../Consulation.html">consultations</a>
                                 
                              </li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        <h3>Development of instructional materials, in all  modalities, including course syllabi
                           
                        </h3>
                        
                        <blockquote>
                           
                           <ul>
                              
                              <li>Faculty Development courses in <a href="../../coursesResources/assessmentTools.html" target="_blank">Assessment</a>, <a href="../../coursesResources/InclusionandDiversity.html" target="_blank">Inclusion &amp; Diversity</a>, <a href="../../coursesResources/LearningcenteredTeachingStrategies.html" target="_blank">Learning-centered Teaching Strategies</a>, and <a href="../../coursesResources/lifeMap.html" target="_blank">LifeMap</a>
                                 
                              </li>
                              
                              <li>Individual or group <a href="../../Consulation.html">consultations</a>
                                 
                              </li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        <h3><a href="../../centers/default.html"><img alt="Center for Teaching/Learning Innovation contact" border="0" height="205" src="ctli-locations-footer-r1.png" width="699"></a></h3>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/curriculumdevelopment/index.pcf">©</a>
      </div>
   </body>
</html>