<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Instructional Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/curriculumdevelopment/studyabroadplcert.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Instructional Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li><a href="/faculty/development/programs/curriculumdevelopment/">Curriculumdevelopment</a></li>
               <li>Instructional Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Study Abroad Program Leader Certification Program</h2>
                        
                        <p> This series is designed to culminate in the certification of faculty as Study Abroad
                           Program Leaders.&nbsp; The designation will indicate completion of 24 hours of professional
                           development in the area of study abroad and global experiences and will prepare faculty
                           members to develop and lead a meaningful short-term study abroad experience for Valencia’s
                           students.&nbsp; &nbsp; 
                        </p>
                        
                        <h3><strong>Congratulations to our newly certified </strong></h3>
                        
                        <h3>
                           <span><strong><a href="documents/StudyAbroadProgramLeadersWebsiteList.pdf">Study Abroad Program Leaders</a></strong></span><strong>!</strong>
                           
                        </h3>
                        
                        <blockquote>
                           
                           
                        </blockquote>
                        
                        <p>To successfully complete the Study Abroad Leader Certification and meet the <a href="../../../../international/studyabroad/staff/submitproposal.html" target="_blank">qualifications to lead a study abroad program</a>, the full-time faculty member must complete both courses in any order.&nbsp; As new courses
                           are developed, additional options will be added to the program. 
                        </p>
                        
                        <p>The Study Abroad Program Leader Certification Program was recognized by <a href="http://wp.valenciacollege.edu/thegrove/prestigious-andrew-heiskell-award-cites-valencias-innovation-in-international-education-2/">The Institute of International Education (IIE)</a> on January 2014.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Certification Planning Document</h3>
                        
                        <p><br>
                           Below you will find a planning document for the Study Abroad Program Leader Certification.
                           This planning document provides a comprehensive list of required and optional courses,
                           which can be used as a checklist as you progress through the certification program.
                           This document is a fillable PDF and is in a printer friendly format.
                        </p>
                        
                        <p><a href="../../CertificationPlanningDocuments.html">Study Abroad Program Leader Certification Planning Document</a></p>
                        
                        
                        
                        <h3>Required Courses (24 Hours) </h3>
                        
                        
                        
                        <div>
                           
                           <div><a href="studyabroadplcert.html#">PRFC3210 Roles and Responsibilities for the Study Abroad Program Ldr<span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>This session is designed for full-time faculty members who plan to lead a short-term
                                 study abroad program and their respective deans.&nbsp; Participants will examine processes
                                 and procedures for creating, implementing, and leading a meaningful study abroad program
                                 for&nbsp; Valencia students. <br>
                                 NOTE:  This course is required for Study Abroad Program Leader Certification; however,
                                 completion of the course does not guarantee the participant a study abroad trip.<br>
                                 <br>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="studyabroadplcert.html#">LCTS3214 Designing a Short-term Study Abroad Experience for Students<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Creating a study abroad program is one way that faculty can internationalize the curriculum
                                    for students to become more competitive in a global environment.&nbsp; In this course,
                                    participants will learn how to plan and prepare for a short-term study abroad program
                                    through the office of Study Abroad and Global Experiences (SAGE). Participants will
                                    submit a completed Short-term Study Abroad Proposal, course outline, and course syllabus
                                    in order to successfully complete the course. <br>
                                    NOTE: This course is required for Study Abroad Program Leader Certification; however,
                                    completion of the course does not guarantee the participant a study abroad trip. This
                                    course is an elective course in the Seneff Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>            
                        
                        
                        
                        
                        <h3>Infusing Global Competency into the Classroom</h3>
                        
                        <p>
                           In addition to preparing faculty to lead a short-term study abroad experience, SAGE
                           and Faculty and Instructional Development also offer courses to help faculty bring
                           global experiences to the classroom. These courses are ideal for faculty who would
                           like to infuse global competencies into courses.
                           
                        </p>
                        
                        <div>
                           
                           
                           <div><a href="studyabroadplcert.html#">INDV3351 Internationalizing the Curriculum at Home<span>20 PD Hours </span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course is designed to provide faculty members with the knowledge and resources
                                    to internationalize their courses through the development of a course internationalization
                                    toolkit, which will be shared with colleagues college-wide. Using an add-on, infusion,
                                    or transformational approach to internationalization, participants will work in small
                                    groups to develop the student learning outcomes, course content, materials, activities
                                    and assignments, and assessments for a specific course.  Note: This course is an elective
                                    course in the Seneff Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="studyabroadplcert.html#">INDV3353 Cross-cultural Awareness and Infusion in the College Classroom<span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> Since cultural variations can have a profound impact on teaching and learning, this
                                    course focuses on cross-cultural awareness and some of the challenges with engaging
                                    students from diverse backgrounds.&nbsp; Participants will engage in a cross-cultural simulation
                                    role play in order to explore differences in belief and value systems.&nbsp; Strategies
                                    for building a more inclusive learning community both inside and outside of the classroom
                                    will be provided. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>            
                        
                        
                        <p><span>For More Information on Study Abroad and Global Experiences</span></p>
                        
                        <p>Please contact <a href="mailto:rbrighton1@valenciacollege.edu">Robyn Brighton</a>, Director of Curriculum Initiatives, or visit the <a href="../../../../international/studyabroad/staff/default.html" target="_blank">SAGE webpage</a>. 
                        </p>            
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/curriculumdevelopment/studyabroadplcert.pcf">©</a>
      </div>
   </body>
</html>