<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certifications &amp; Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/destination/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Certifications &amp; Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Destination</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        
                        <h3>DESTINATION</h3>
                        
                        <p>This annual professional development program brings Valencia colleagues together to
                           explore teaching and learning innovations, challenges, and solutions. Each year's
                           program will feature multiple experiences that will be valuable for faculty who have
                           participated in Destination and those who are new to the experience. Join us this
                           summer to explore teaching and learning innovations, challenges, and solutions with
                           colleagues from diverse fields with varied experiences and interests. All Valencia
                           faculty members, part-time and full-time, are encouraged to apply. 
                        </p>
                        
                        <p>Participants will be awarded $500 for successful completion of Summer Destination
                           2017.  Please note, if you are looking to renew your Associate Faculty status, be
                           advised that PD credit will not be awarded for the summer portion of Destination.
                           
                        </p>
                        
                        <p>Destination 2017 will feature seven tracks (detailed below), which will empower participants
                           to reflect on their own teaching strategies, learn from faculty in other disciplines
                           and identify new strategies and tools. 
                        </p>
                        
                        
                        <div> 
                           
                           
                           <div><a href="index.html#"><strong>TEAL: Technology-enhanced Active Learning</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Description:</strong> Through an immersive technology-enhanced experience, faculty will explore strategies
                                    and tools for implementing active learning with technology. Looking at active learning
                                    through the lens of adult learning theory, social cognitive theory, and constructivism,
                                    faculty will explore and evaluate specific educational technologies that align to
                                    these learning theories to their practice. This exciting five-week hands-on experience
                                    will allow participants to reflect on their teaching philosophy, identify student
                                    struggles, and create an active learning lesson plan enhanced with technology tools
                                    to promote learning in the classroom. 
                                 </p>
                                 
                                 <p><strong>Participants will:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>define technology-enhanced active learning</li>
                                    
                                    <li>explore a variety of technology tools that supports/enhances active learning strategies</li>
                                    
                                    <li>choose a technology-enhanced active learning strategy that is aligned to a lesson</li>
                                    
                                    <li>create a lesson plan that incorporates technology-enhanced active learning</li>
                                    
                                    <li>apply universal design principles to their lesson</li>
                                    
                                    <li>demonstrate the technology-enhanced final product as a lesson</li>
                                    
                                 </ul>
                                 
                                 <p><strong>Track Leaders:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-aaron-bergeson">Aaron Bergeson,</a> Faculty Developer/Instructional Designer, Winter Park Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-amanda-kern/">Amanda Kern</a>, Graphic &amp; Interactive Design Professor, East Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/welcome-claudine-bentham-faculty-development-faculty-fellow/">Claudine Bentham</a>, Faculty Fellow, West Campus
                                    </li>
                                    
                                    <li>Dori Haggerty, Faculty and Instructional Development Director, West Campus</li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/elizabeth-faulcon-brings-virtual-reality-to-the-classroom-faculty-highlight/">Elizabeth Faulcon</a>, Humanities Professor, Winter Park
                                    </li>
                                    
                                    <li>Lauren Kelly, Faculty Developer/Instructional Designer, Lake Nona</li>
                                    
                                 </ul>
                                 
                                 
                                 <p><strong>Participant Commitment:</strong> 
                                 </p>
                                 
                                 <p>Participants must attend all scheduled meetings and complete all projects by the stated
                                    due dates in order to receive compensation.<em>Seats are limited to 60 faculty and may be subject to dean approval.</em></p>
                                 
                                 <p><strong>Compensation:</strong> 
                                 </p>
                                 
                                 <p>Upon completion of Destination 2017, participants will receive $500.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#"><strong>Writing to Learn/Learning to Write: What is WAC?</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Description:</strong> What course concept is most difficult for your students to learn? Interested in strategies
                                    for your discipline that will develop reading and critical thinking strategies, build
                                    confidence, offer just-in-time feedback? That's WAC: Using writing as a tool for learning
                                    and learning to use writing as a tool. This Destination track is recommended for full-time
                                    and part-time faculty interested in exploring these ideas through a problem-based
                                    approach to WAC topics, such as<br>
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>critical reading, thinking, and analysis</li>
                                    
                                    <li>writing to learn about challenging concepts</li>
                                    
                                    <li>writing for your discipline</li>
                                    
                                    <li>and assessing for deeper understanding</li>
                                    
                                 </ul>
                                 
                                 <p>Participating faculty will have the opportunity to create a lesson that they can implement
                                    in their practice and will have the opportunity to contribute to the college-wide
                                    initiative of Writing Across the Curriculum at Valencia. 
                                 </p>
                                 
                                 <p><strong>Participants will:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>examine the connection between writing and learning course content</li>
                                    
                                    <li>consider their definition of writing, inclusive of writing to learn and learning to
                                       write
                                    </li>
                                    
                                    <li>explore strategies to develop critical, reading, thinking and analysis within the
                                       discipline
                                    </li>
                                    
                                    <li>align formative and summative writing assessments to develop understanding of discipline-specific
                                       concepts
                                    </li>
                                    
                                    <li>create a plan to be implemented in their practice</li>
                                    
                                    <li>develop their reflective practice as a tool for learning</li>
                                    
                                 </ul>
                                 
                                 <p><strong>Track Leaders:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Donna Colwell, Professor, English, West Campus </li>
                                    
                                    <li>Vasudha Sharma, Professor, Chemistry, East Campus </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-a-natural-problem-solver-claire-yates-impacts-students-through-faculty-development/" target="_blank">Claire Yates</a>, Faculty Developer/Instructional Designer, West Campus 
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/stephanie-spong-named-new-east-and-winter-park-campus-director-faculty-and-instructional-development/" target="_blank">Stephanie Spong</a>, Faculty and Instructional Development Director, East and Winter Park Campus 
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Participant Commitment:</strong> 
                                 </p>
                                 
                                 <p>Participants must attend all scheduled meetings and complete all projects by the stated
                                    due dates in order to receive compensation. During the summer, participants will create
                                    and refine a WAC lesson plan. During fall 2017, participants will be invited to implement
                                    their plans with the guidance of a colleague mentor. Participation in Fall 2017 will
                                    be optional. <em>Seats are limited to 30 faculty and may be subject to dean approval</em>.
                                 </p>
                                 
                                 <p><strong>Compensation and Award of PD Credit:</strong> 
                                 </p>
                                 
                                 <p>Upon completion of Destination 2017, participants will receive the following compensation
                                    and PD credit.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Summer term: $500</li>
                                    
                                    <li>Fall term: 20 PD hours (PD hours will apply to the 2017-2018 term)</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#"><strong>Early alert, Valencia Style: Creating a Culture of Continuous Assessment and Responsive
                                    Engagement (CARE)</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Description:</strong> The CARE track will provide faculty with an opportunity to explore innovative resources
                                    and tools that will aid them in supporting struggling students.  Faculty will learn
                                    from past CARE course participants and research best-practices that will help to transform
                                    the learning experience for their students. <br>
                                    
                                 </p>
                                 
                                 <p><strong>Participants will:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>investigate best practices for student engagement</li>
                                    
                                    <li>demonstrate their awareness of how empathy can impact student success</li>
                                    
                                    <li>explore and select appropriate assessments (include summative and formative assessments)
                                       to identify academic and nonacademic behaviors
                                    </li>
                                    
                                    <li>examine available data on Valencia students and their learning experience</li>
                                    
                                    <li>investigate student support resources and identify the resources to support their
                                       intervention
                                    </li>
                                    
                                    <li>create a CARE plan that outlines a specific intervention.</li>
                                    
                                    <li>understand how their CARE plans contribute to creation of CARE culture at Valencia</li>
                                    
                                 </ul>
                                 
                                 <p><strong>Track Leaders:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Chris Teumer, Assistant Director, Learning Support &amp; Advising, Title V, East Campus</li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/welcome-2015-2016-faculty-fellows-brian-macon-and-daeri-tenery/">Daeri Tenery</a>. Chemistry Professor, East Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-leonard-bass-driven-to-help-students-succeed/">Leonard Bass</a>, Dean, Learning Support, East Campus
                                    </li>
                                    
                                    <li>Stephanie Spong, Faculty &amp; Instructional Development Director, East Campus and Winter
                                       Park
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Participant Commitment:</strong> 
                                 </p>
                                 
                                 <p>Participants must attend all scheduled meetings and complete all projects by the stated
                                    due dates in order to receive compensation. During the summer, participants will create
                                    and refine a CARE plan. During fall 2017, participants will implement their plans
                                    with the guidance of a colleague mentor. <em>Seats are limited to 30 faculty and may be subject to dean approval.</em></p>
                                 
                                 <p><strong>Compensation and Award of PD Credit:</strong> 
                                 </p>
                                 
                                 <ul>
                                    Upon completion of Destination 2017, participants will receive the following compensation
                                    and PD credit.
                                    
                                    
                                    <li>Summer term: $500</li>
                                    
                                    <li>Fall term: 25 PD hours (PD hours will apply to the 2017-2018 term)</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#"> <strong>SEED: Seeking Educational Equity and Diversity</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Description:</strong> SEED stands for Seeking Educational Equity and Diversity. It is an international
                                    project founded 30 years ago by Dr. Peggy McIntosh of Wellesley College. It utilizes
                                    a cohort-based model with the intention of creating gender fair, multiculturally equitable,
                                    and globally informed educational spaces and workplaces. SEED differs from other diversity
                                    programs in that SEED leaders do not lecture. Instead they lead their own colleagues
                                    in experiential, interactive exercises and conversation often stimulated by films
                                    and readings.  These sessions deepen participants' understanding of themselves, expand
                                    their knowledge of the world, and point the way to making Valencia more inclusive.
                                    
                                 </p>
                                 
                                 <p><strong>Participants will:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>understand how one's personal and community history affects one's self-esteem and
                                       sense of safety
                                    </li>
                                    
                                    <li>explore experience and learn how to converse from a diverse perspective</li>
                                    
                                    <li>listen and respond to personal experiences from a cultural perspective</li>
                                    
                                    <li>notice the impact and intent of our communication</li>
                                    
                                    <li>appreciate and respond compassionately and openly about diversity issues</li>
                                    
                                    <li>examine how personal stories can affect our perceptions and attitudes</li>
                                    
                                    <li>create a sense of community through dialogue and stories</li>
                                    
                                    <li>work with conflict and hurt when diversity issues are involved</li>
                                    
                                    <li>employ techniques on how to listen and respond to intercultural communications</li>
                                    
                                 </ul>
                                 
                                 
                                 <p><strong>Track Leaders:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Celine Kavalec-Miller, Faculty Director, TLA</li>
                                    
                                    <li>Shari Koopman, Professor, English, East Campus </li>
                                    
                                    <li>Stanton Reed, Professor, Business/Accounting, Osceola Campus </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-hank-van-putten/" target="_blank">Hank Van Putten</a>, Adjunct Professor, Student Life Skills, East Campus 
                                    </li>
                                    
                                    <li>Wendi Dew, Assistant VP, Teaching &amp; Learning </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Participant Commitment:</strong> 
                                 </p>
                                 
                                 <p>Participants must attend all scheduled meetings and complete all projects by the stated
                                    due dates in order to receive compensation. <em>Seats are limited to 30 faculty and may be subject to dean approval.</em></p>
                                 
                                 <p><strong>Compensation:</strong> 
                                 </p>
                                 
                                 <p>Upon completion of Destination 2017, participants will receive $500.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#"> <strong>Education for Sustainability: A Transformative Learning Experience</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Description:</strong> Sustainability, the challenge of meeting "the needs of the present without compromising
                                    the ability of future generations to meet their own needs" (Brundtland Report, 1983),
                                    is a multi-faceted concept that impacts our daily lives, engages our students, and
                                    can enrich any classroom curriculum. Through an understanding of (1) systems dynamics,
                                    (2) scale, (3) long-term development, (4) tradeoffs, and (5) interdisciplinary collaboration/participation,
                                    we can prepare students to meet the complex problem solving and critical thinking
                                    skills required to thrive in the 21st century (Future of Jobs Report, World Economic
                                    Forum, 2015). Join us for an exploration of sustainability concepts, challenges, and
                                    practices to enhance learning in your classroom. 
                                 </p>
                                 
                                 <p><strong>Participants will:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>define foundational concepts of sustainability including systems dynamics, scale,
                                       long-term development, tradeoffs, and interdisciplinary collaboration/participation
                                    </li>
                                    
                                    <li>connect the impact of individual behavior on broader social, environmental, and economic
                                       systems and ultimately on quality of life
                                    </li>
                                    
                                    <li>create instructional materials that apply foundational concepts of sustainability
                                       to your classroom
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Track Leaders:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-gary-kokaisel/">Gary Kokaisel</a>, Faculty Developer/Instructional Designer, Osceola Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/jerrid-kalakay-prepares-students-for-the-challenges-of-tomorrow-faculty-highlight/">Jerrid Kalakay</a>, Professor, Student Life Skills, East Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-resham-shirsat/">Resham Shirsat</a>, Director of Sustainability, West Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-shara-lee/">Shara Lee</a>, Faculty and Instructional Development, Director, Osceola/Lake Nona/Poinciana Campuses
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/susan-ledlow-our-new-vp-of-academic-affairs-and-planning/">Susan Ledlow</a>, Vice President of Academic Affairs and Planning, District Office
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Participant Commitment:</strong> 
                                 </p>
                                 
                                 <p>Participants must attend all scheduled meetings and complete all projects by the stated
                                    due dates in order to receive compensation. <em>Seats are limited to 30 faculty and may be subject to dean approval.</em></p>
                                 
                                 <p><strong>Compensation:</strong> 
                                 </p>
                                 
                                 <p>Upon completion of Destination 2017, participants will receive $500.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#"> <strong>First 30: Strengthening the Foundation for Meta-Major Pathways</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Description:</strong> First 30 is a clustered learning community that offers students a clear academic
                                    pathway during their first year in college. By pre-registering students in a block
                                    schedule of recommended front-door courses, they develop stronger bonds with their
                                    peers and stay on track to graduate sooner. The model was developed in 2016 and following
                                    the recommendations that emerged from the Academic Initiative Review (AIR) process
                                    on LinC. 
                                 </p>
                                 
                                 <p>In this Destination track, faculty and Career Program Advisors will build community
                                    with faculty mentors as they collaborate in the development of a Campus Action Plan
                                    for their First 30 cohort. Facilitators will guide participants in the creation of
                                    interdisciplinary assignments along with co-curricular and Service Learning activities
                                    intended to engage students in real-world application of content while strengthening
                                    their academic, pre-professional and personal learning goals. 
                                 </p>
                                 
                                 <p><strong>Participants will:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>explain the theoretical basis for offering learning communities for students that
                                       have been identified as being at-risk at the college
                                    </li>
                                    
                                    <li>build community with a LinC partner and/or campus team</li>
                                    
                                    <li>develop a comprehensive Campus Action Plan that includes an overarching theme, communication
                                       plan, co-curricular events, and Service Learning activities 
                                    </li>
                                    
                                    <li>create integrated lessons to improve student learning of course outcomes</li>
                                    
                                 </ul>
                                 
                                 <p><strong>Track Leaders:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/featured-colleague-christy-cheney/">Christy Cheney</a>, Professor, Student Life Skills, Osceola Campus
                                    </li>
                                    
                                    <li>Joshua Guillemette, Professor, Mathematics, East Campus</li>
                                    
                                    <li>Robyn Brighton, Director of Curriculum Initiatives </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Participant Commitment:</strong> 
                                 </p>
                                 
                                 <p>Participants must attend all scheduled meetings and complete all projects by the stated
                                    due dates in order to receive compensation. <em>Seats are limited to 30 faculty selected by their deans to teach in the First 30 Cohort
                                       on East and West Campuses.</em></p>
                                 
                                 <p><strong>Compensation and Award of PD Credit:</strong> 
                                 </p>
                                 
                                 <p>Upon completion of Destination 2017, participants will receive the following compensation
                                    and PD credit. 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Summer term: $500</li>
                                    
                                    <li>2017-2018 Development Opportunities: PD hours will be award and applied to the 2017-2018
                                       term
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#"><strong>ROC 101: The Art of Migration &amp; Painting Your Own Canvas * by invitation only</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Description:</strong> Say hello to the ROC, the Rubric for Online Competencies. You have been invited to
                                    become a master of your craft and to establish a baseline of quality for online design
                                    and instruction. Participants in this track will be introduced to navigation and functionality
                                    skills in CANVAS, our future learning management system. It takes more than skill
                                    to master CANVAS, so participants will also master the art of migration by preparing
                                    course content and developing quality course construction. This collaborative experience
                                    will utilize the ROC, a draft tool to review quality course design and online instruction
                                    criteria, while recognizing innovations in online teaching.
                                 </p>
                                 
                                 <p><strong>Participants will:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>gain knowledge in Canvas navigation and functionality</li>
                                    
                                    <li>apply best practices for migrating from Blackboard to Canvas</li>
                                    
                                    <li>prepare course content for Canvas migration</li>
                                    
                                    <li>apply the Rubric for Online Competencies (ROC) as an assessment tool for establishing
                                       quality course design, through a peer review process
                                    </li>
                                    
                                    <li>use the Rubric for Online Competencies (ROC) as an assessment tool for examining the
                                       essential skills of the online instructor role, through a peer review process 
                                    </li>
                                    
                                    <li>provide thoughtful feedback to support course migration and refinement of the ROC
                                       and peer review process
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <p><strong>Track Leaders:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Geni Wright, Online Teaching &amp; Learning, Assistant Director, District Office</li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/james-may-wins-professor-of-the-year-award/">James May</a>, EAP Professor and Faculty Fellow for Innovation and Technology, East Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/professor-kristin-bartholomew-energizes-nutrition-education-in-central-florida/">Kristin Bartholomew</a>, Nutrition Discipline Chair and Nutrition Professor, West Campus
                                    </li>
                                    
                                    <li>
                                       <a href="http://thegrove.valenciacollege.edu/valencia-professors-share-keynote-presentation-on-action-research-at-third-annual-conference-on-teaching-and-learning/">Liza Schellpfeffer</a>, Speech Communication Professor and Faculty Fellow for Excellence in Online Teaching
                                       &amp; Learning, East Campus
                                    </li>
                                    
                                    <li>Page Jerzak, Online Teaching &amp; Learning, Director, District Office</li>
                                    
                                 </ul>
                                 
                                 <p><strong>Participant Commitment:</strong> 
                                 </p>
                                 
                                 <p>Participants must attend all scheduled meetings and complete all projects by the stated
                                    due dates in order to receive compensation. Participation in this track is by Dean
                                    nomination only. <em>Seats are limited to 60 faculty and are by invitation only with dean approval.</em></p>
                                 
                                 <p><strong>Compensation:</strong> 
                                 </p>
                                 
                                 <p>Upon completion of Destination 2017, participants will receive $500.</p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>If Destination 2017 sounds like an opportunity right for you, the Faculty and Instructional
                           Development team invites you to join us this summer! Follow the link below to submit
                           your application.
                        </p>
                        
                        
                        
                        
                        <p>Destination 2017 Application period closed. <br>
                           Please check back in early 2018 for upcoming program details.
                        </p>
                        
                        <p><strong>Questions or For More Information</strong></p>
                        
                        <p> If you have any questions, please contact, Trish Chiao, Faculty &amp; Instructional Development
                           Coordinator at <a href="mailto:tchiao@valenciacollege.edu">tchiao@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Highlights from Past Destination Sessions</h3>
                        
                        <p>
                           <a href="http://thegrove.valenciacollege.edu/destination-2016-receives-rave-reviews/">Destination 2016 Receives Rave Reviews</a><br>
                           <a href="http://thegrove.valenciacollege.edu/destination2016-social-media-challenge-winners-announced/">#Destination16 Social Media Challenge</a><br>
                           <a href="http://thegrove.valenciacollege.edu/a-look-back-at-circles-of-innovation-during-destination-2015/">Circles of Innovation During Destination 2015</a><br>
                           <a href="http://thegrove.valenciacollege.edu/front-door-alignment-work-continues-in-destination-2015/">Front Door Alignment in Destination 2015</a><br>
                           <a href="http://thegrove.valenciacollege.edu/care-emerges-as-a-key-destination-2015-initiative/">"CARE" as a Key Destination 2015 Initiative</a></p>
                        
                        <h3>Summer Dates and Times: </h3>
                        
                        <p><strong>Kick-off Session</strong>
                           <br>
                           Friday, May 12, 2017
                           <br>
                           12 - 1 p.m. (Lunch) 
                           <br>
                           1 - 5 p.m.
                           <br>
                           West Campus
                        </p>
                        
                        <p><strong>Session 2 </strong>
                           <br>
                           Friday, May 19, 2017
                           <br>
                           1 - 4 p.m.
                           <br>
                           West Campus
                        </p>
                        
                        <p><strong>Session 3 </strong>
                           <br>
                           Friday, May 26, 2017
                           <br>
                           1 - 4 p.m.
                           <br>
                           West Campus
                        </p>
                        
                        <p><strong>Session 4 </strong>
                           <br>
                           Friday, June 2, 2017
                           <br>
                           1 - 4 p.m.
                           <br>
                           West Campus
                        </p>
                        
                        <p><strong>Wrap-up Session</strong>
                           <br>
                           Friday, June 9, 2017
                           <br>
                           1 - 4 p.m.
                           <br>
                           West Campus
                        </p>
                        
                        <h3>Fall Dates and Times: </h3>
                        
                        <p> <strong> (for specific program tracks)</strong>
                           
                        </p>
                        
                        <p> <strong>Kick-off Session<br>
                              
                              </strong>Friday, September 8, 2017 <br>
                           2 - 4 p.m. <br>
                           West Campus 
                        </p>
                        
                        <p><strong>Individual Meetings with <br>
                              Colleague Mentors throughout<br>
                              the Fall</strong></p>
                        
                        <p><strong>Wrap-Up Session<br>
                              </strong>Friday, December 8, 2017<br>
                           2 - 4 p.m.<br>
                           West Campus
                           
                        </p>
                        
                        
                        <h5><strong>Locations</strong></h5>
                        
                        <p><em>Due to accomodation needs and expansion of the Destination program this year, all
                              sessions will be held at <strong>West Campus</strong>. Specific buildings and rooms to be announced as we near the event.</em></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/destination/index.pcf">©</a>
      </div>
   </body>
</html>