<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certifications &amp; Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/seneffcert.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Certifications &amp; Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Certifications &amp; Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Seneff Faculty Development Program </h2>
                        
                        <p> This series is designed to culminate in the designation of the faculty as a member
                           of the Seneff Faculty Community.&nbsp; The program is open to all Valencia faculty members
                           and indicates an individual commitment to the development of the honors community
                           at the college.
                        </p>
                        
                        <p>To successfully complete the program, the faculty member must complete a minimum of
                           8 hours in two foundational courses, 6 hours in honors pedagogy courses, and 6 hours
                           of electives that can include additional courses or, with dean approval, other professional
                           activities.
                        </p>
                        
                        
                        <h3>Congratulations to our Valencia faculty who have achieved the Seneff Faculty Development
                           Program! <a href="documents/Faculty-Seneff-Honors-Certification-List.pdf">View list here</a>
                           
                        </h3>
                        
                        
                        <h3>Certification Planning Document</h3>
                        
                        <p><br>
                           The planning document provides a list of required and optional courses which can be
                           used to plan your certification or program. It is available as a printer-friendly
                           fillable PDF. <a href="../CertificationPlanningDocuments.html">Seneff Honors Faculty Development Program Planning Document</a> 
                        </p>
                        
                        <h3>Foundational Courses (8 Hours) </h3>
                        
                        
                        
                        <div>
                           
                           <div><a href="SeneffCert.html#">PFRC6360 What is Honors?<span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course will explore the history of the honors movement and attempt to answer
                                    the ever-elusive question, "What is honors?"&nbsp; In addition, expectations of a Valencia
                                    honors student and a Valencia honors faculty will be discussed.&nbsp; NOTE:&nbsp; This is required
                                    course for the Seneff Faculty Development program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS6310 Best Practices in Honors Education<span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course will review some of the established best practices and cutting-edge ideas
                                    in honors education, as well as encourage instructors to develop techniques and assignments
                                    that challenge students to engage course materials in new and inventive ways.&nbsp; NOTE:&nbsp;
                                    This is a required course for the Seneff Faculty Development program. This course
                                    requires completion of a pre-reading assignment in addition to a 3-hour face-to-face
                                    meeting. Course facilitator will e-mail participants one week prior to the start of
                                    the course. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>Honors Pedagogy Courses (minimum of 6 Hours) </h3>
                        
                        <div>
                           
                           <div><a href="SeneffCert.html#">LCTS6311  Interdisciplinary Teaching: Pedagogical Practices that Encourage Critical
                                 Thinking and Action<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course will explore the benefits and challenges of designing and implementing
                                    interdisciplinary teaching techniques in the classroom.&nbsp; Emphasis will be placed on
                                    developing instructional activities that nurture students' critical thinking skills
                                    by constructing alternative perspectives to course questions.&nbsp; In addition, participants
                                    will discuss strategies to meet challenges of interdisciplinary teaching and to maximize
                                    benefits to students.&nbsp; NOTE:&nbsp; This is an optional course for the Seneff Faculty Development
                                    program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS6312  Great Books (And Other Masterpieces in Human Thought)<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course will review ways to include primary source materials into lower division,
                                    general education courses.&nbsp; While emphasis will be placed on canonical 'great books,'
                                    methods for incorporating other primary source materials such as historical documents
                                    and artifacts will also be discussed. NOTE:&nbsp; This is an optional course for the Seneff
                                    Faculty Development program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS6313  City as Text™: A Model for Active Learning<span>5 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>City as Text™ is the National Collegiate Honors Council signature program that provides
                                    the opportunity for participants to "read" an urban landscape and find meaning through
                                    walkabouts and active exploration of the environment.&nbsp; Basic tenets of active learning
                                    will also be covered. NOTE:&nbsp; This is an optional course for the Seneff Faculty Development
                                    program. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS6314  What is Undergraduate Research?*<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> This course will define and outline expectations of undergraduate research at Valencia.
                                    In addition, the course will address the increasing need for two-year students to
                                    be research-ready upon transfer to four-year schools.<br>
                                    
                                 </p>
                                 
                                 <p>                        NOTE: This is an optional course for the Seneff Faculty Development
                                    program. <br>
                                    *This course is required for faculty mentoring students in the Honors Undergraduate
                                    Research Track who do not meet alternative credentialing. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LFMP6340  Mentoring Students in Undergraduate Research *<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> This course will provide the essential skills required to mentor honors students
                                    enrolled in the Undergraduate Research Track (IDH2912) course. Mentoring students
                                    in this track requires that the faculty member facilitate students in determining
                                    their research, establishing a research question, and conducting research specific
                                    to the mentor's own discipline. In addition, mentors will be responsible for guiding
                                    students in the presentation of their research in a formal setting in accordance with
                                    the guidelines set by that forum. NOTE: This is an optional course for the Seneff
                                    Faculty Development program. This course is required for faculty mentoring students
                                    in the Honors Undergraduate Research Track who do not meet alternative credentialing.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <h3>Electives (minimum of 6 Hours)</h3>
                        
                        <p>The faculty member must complete at least 6 additional hours in the below courses
                           or, with dean approval, participate in professional activities designed to enhance
                           involvement in the honors community such as completion of the Phi Theta Kappa Leadership
                           Development Certification Seminar, attendance at national, regional, and state honors
                           conferences, attendances at discipline conferences, etc.&nbsp; 
                        </p>
                        
                        <p><em>Please note that participation in other professional activities outside the Valencia
                              Faculty Development Catalog cannot be applied to the hours required for faculty seeking
                              or renewing the Associate Faculty Certification.</em></p>
                        
                        <div>
                           
                           <div><a href="SeneffCert.html#">INDV2254 The Art and Science of Learning and the Brain <span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In this course, participants will examine the relationship between the ways people
                                    learn and the biology of the brain.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">INDV3248 Building Online Communities <span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course will engage participants in the concepts, effective strategies and best
                                    practices in creating and maintaining an online learning community.<br>
                                    NOTE: This course is required for the Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">INDV3358 The Art of Facilitating Online Discussions <span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course is designed to enhance participants' ability to effectively facilitate
                                    discussions in the online environment. Participants will examine the facilitator's
                                    role in online discussions that build community and enhance student learning. Participants
                                    will enhance their facilitation skills through activities with their peers that simulate
                                    facilitator-student interaction.<br>
                                    NOTE: This course is an elective for the Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">INDV2255 Multiple Perspectives <span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In this workshop, participants will investigate teaching strategies to improve students'
                                    abilities to engage in conversation with alternative viewpoints.&nbsp; Participants will
                                    reflect on ways to reveal the importance of recognizing and engaging multiple perspectives
                                    as well as ways to motivate students to learn from reputable sources.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">INDV3259 LinC: Integrating a High Impact Practice (prev. INDV3257/3258) <span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Learning communities are regarded as a high impact educational practice that has shown
                                    to be beneficial to college students from many backgrounds. This course will review
                                    the foundation of Valencia's LinC initiative, along with providing the necessary guidelines
                                    and strategies to build community in the classroom and implement integrated learning
                                    with two or more joined courses. The course will help you design a joint syllabus
                                    with a faculty partner from another discipline and partner with a Success Coach to
                                    support student learning.&nbsp; NOTE: This course is an elective course in the LifeMap
                                    Certification program.
                                 </p>
                                 
                                 <p>Please contact <a href="mailto:rbrighton1@valenciacollege.edu">Robyn Brighton</a> for more information or to register. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">INDV3351 Internationalizing the Curriculum at Home<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> This course is designed to provide faculty members with the knowledge and resources
                                    to internationalize their courses through the development of a course internationalization
                                    toolkit, which will be shared with colleagues college-wide. Using an add-on, infusion,
                                    or transformational approach to internationalization, participants will work in small
                                    groups to develop the student learning outcomes, course content, materials, activities
                                    and assignments, and assessments for a specific course. Faculty are strongly encouraged
                                    to attend the course with at least one other person from their discipline in order
                                    to develop the toolkit together.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">INDV3356 Stewardship as a Study Abroad Leader  <span>4 Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Participants will develop global leadership skills to lead study abroad experiences.
                                    Participants will learn how to create an atmosphere of inclusion and mitigate issues
                                    when studying abroad through exploring a variety of scenarios and role-playing. Additionally,
                                    faculty will develop a risk management plan. This course is designed for full-time
                                    faculty interested in leading a study abroad program.<br>
                                    Note: This is a required course in the Study Abroad Program Leader Certification.
                                    Participants must successfully complete the following prerequisite prior to enrolling
                                    in this course: Introduction to SAGE (LCTS 3316).
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS2213 Active Learning Techniques<span>2 PD Hours</span> </a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In this hands-on session, participants learn a variety of strategies that guide students
                                    to become more active learners Faculty members from all disciplines will find this
                                    session useful.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS2214  Problem-based Learning<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>This course explores the learning of subject matter and skill acquisition through
                                 collaborative problem-solving.&nbsp; Emphasis is placed on using this method in community
                                 college courses. 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS2217 Project-based Learning<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> In project-based learning, students answer a complex, open-ended question or solve
                                    an authentic problem through a collaborative process of investigation. Unlike traditional
                                    instruction where students are presented with knowledge and given an opportunity to
                                    apply that knowledge, project-based learning starts with the end in mind – a product
                                    that requires the learning of essential content and skills. In this workshop, faculty
                                    will be introduced to the seven characteristics of project-based learning, as well
                                    as techniques for managing a project-based learning experience in one's classroom.
                                    Workshop participants will draft a driving-question and will begin developing ideas
                                    for project-based learning experiences in their disciplines.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS2220 Using Collaborative Writing Strategies Across the Disciplines<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In this course, participants in all disciplines examine strategies to teach students
                                    how to write more effectively for their specific course using collaborative writing
                                    as a teaching method.&nbsp; Participants will be given resources and the opportunity to
                                    create a lesson or activity incorporating collaborative writing into their discipline.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS2221  Impacting Student Motivation<span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>IIn this course, participants will discuss and apply strategies for helping students
                                    to understand motivation and how it influences their thoughts and behaviors.&nbsp; Participants
                                    will read Daniel Pink's <em>Drive</em> as background for the discussions.&nbsp; In registering for this course, you are committing
                                    to attend two meetings, read the book, and develop one activity/lesson on motivation.
                                    NOTE:&nbsp; This course is an elective  for the<a href="LifeMapCertificatePage.html"> LifeMap Certification and</a> Seneff Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS3213  Service Learning Across the Curriculum<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Participants will create a plan to infuse service learning into a current course by
                                    linking course outcomes to meaningful community service.&nbsp; Participants will explore
                                    service learning opportunities and create assessment plans to measure student learning.
                                    Successful completion of this course will enable participants to add a service learning
                                    designation to their courses.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS3214 Designing a Short-Term Study Abroad Experience for Students<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Creating a study abroad program is one way that full-time faculty can internationalize
                                    the curriculum for students to better compete in a global environment.&nbsp; In this course,
                                    participants will learn how to plan and prepare for a short-term study abroad program
                                    through the office of Study Abroad and Global Experiences (SAGE). Faculty will design
                                    a study-abroad course with learning outcomes, activities, and assessment methods and
                                    complete an initial draft of the Short-Term Study Abroad Proposal.&nbsp; Participants will
                                    submit a completed proposal, course outline, and course syllabus in order to successfully
                                    complete the course. NOTE:&nbsp; This course is limited to full-time faculty members. This
                                    course is required for Study Abroad Program Leader Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS3315 Introduction to SAGE <span>6 Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Participants will engage in an overview of the Study Abroad program, the student global
                                    distinction pathway, and global competency skills. Additionally, participants will
                                    develop a global perspective as well as explore different models for internationalizing
                                    curriculum and high impact educational practices.<br>
                                    
                                    Note: This is a required course in the Study Abroad Program Leader Certification and
                                    must be taken first in the series of required courses Stewardship as a Study Abroad
                                    Program Leader (INDV 3356) and Designing a Study Abroad Proposal (LCTS 3316). Although
                                    leading a study abroad program is limited to full-time faculty, this course is appropriate
                                    for all faculty interested in learning more about internationalizing their curriculum.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LCTS3316 Designing a Study Abroad Proposal <span>10 Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Participants will create authentic learning experiences for studying abroad. Additionally,
                                    participants will develop a curriculum plan, destination logistics, budget, and recruitment
                                    strategies.<br>
                                    Note: This is a required course in the Study Abroad Program Leader Certification.
                                    Participants must successfully complete the following prerequisites prior to enrolling
                                    in this course: Introduction to SAGE (LCTS 3315) and Stewardship as a Study Abroad
                                    Leader (INDV 3356). This course is designed for full-time faculty interested in leading
                                    a study abroad program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">LOBP3230  Thinking Things Through: Critical Thinking Theory and Practice<span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Over 2 two-hour meetings, participants will discuss and apply the concepts of Paul
                                    and Elder's critical thinking model outlined in Gerald Nosich's book <em>Learning to Think Things Through</em>:&nbsp; <em>A Guide to Critical Thinking Across the Disciplines.&nbsp; </em>The text offers both a practical model that can be applied to any discipline as well
                                    as exercises and activities for teaching students how to think more critically. Each
                                    participant will receive a copy of the book after registering. In registering for
                                    this course, you are committing to attend 2 meetings, read the book, and integrate
                                    the model into one activity/lesson in your class. NOTE:&nbsp; This course is an elective
                                    the <a href="LifeMapCertificatePage.html">LifeMap Certification</a> and the Seneff Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">PRFC3210 Roles and Responsibility for the Study Abroad Program Leader<span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This session is designed for full-time faculty members who plan to lead a short-term
                                    study abroad program and their respective deans.&nbsp; Participants will examine processes
                                    and procedures for creating, implementing, and leading a meaningful study abroad program
                                    for Valencia students.&nbsp; NOTE: This course is required for Study Abroad Program Leader
                                    Certification; however, completion of the course does not guarantee the participant
                                    a study abroad trip.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">SOTL3272/3273  IRB Requirements &amp; Your Research (not currently offered) <span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>Meeting one-hour face-to-face and three hours online, this course will provide an
                                 overview of the Institutional Review Board (IRB) for all faculty members who are planning
                                 to conduct research studies at Valencia College. The online portion of the course
                                 will result in nationally-recognized certification from the National Institutes of
                                 Health (NIH) on protecting human subjects. The online training is self-paced and required
                                 for faculty members conducting research at Valencia College.&nbsp; It is recognized by
                                 most institutions of higher education for three years after the date of completion.
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="SeneffCert.html#">SOTL4270  Community of Scholars (not currently offered) <span>Variable PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>A selected group of faculty will collaboratively engage in scholarly inquiry.&nbsp; Each
                                 Community of Scholars will be a specialized "community of practice" that will explore
                                 a shared topic related to the innovation of teaching and learning at Valencia.&nbsp; Led
                                 by a faculty facilitator, each community will engage in collaborative activities and
                                 disseminate their work to campus and college colleagues.<em>&nbsp; NOTE: Participants may be selected through an application process.&nbsp; For more information
                                    please contact the Office of Faculty Development</em>.
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/seneffcert.pcf">©</a>
      </div>
   </body>
</html>