<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certifications &amp; Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/lifemapcertificatepage.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Certifications &amp; Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Certifications &amp; Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <h2>LifeMap Certification</h2>
                        
                        <p>LifeMap Certification is designed to support all faculty members, full-time and part-time,
                           as they expand their knowledge and integration of LifeMap and College Success Skills
                           with the goal of enhanced student learning. To attain this certification, faculty
                           will complete 32 hours of foundational courses and 4 hours of electives. 
                        </p>
                        
                        <p><strong>LifeMap:&nbsp; Essential Competency of a Valencia Educator</strong></p>
                        
                        <p>Valencia educators will design learning opportunities that promote student life skills
                           development while enhancing discipline learning. Through intentional inclusion of
                           growth-promoting strategies, faculty will facilitate the students' gradual assumption
                           of responsibility for making informed decisions and formulating and executing their
                           educational, career, and life plans.
                        </p>
                        
                        <p>In demonstration of this Essential Competency, the faculty member will: </p>
                        
                        <ul>
                           
                           <li>establish student &amp; faculty contact that contributes to students' academic, personal,
                              and professional growth
                           </li>
                           
                           <li>employ digital tools to aid student contact (e.g., Atlas, MyPortfolio, Blackboard,
                              Ask-A-Librarian, email, etc.)
                           </li>
                           
                           <li>seek out struggling students and identify options through dialog and appropriate referrals</li>
                           
                           <li>help students assume responsibility for making informed academic decisions (e.g.,
                              degree requirements, transfer options, financial aid, etc.)
                           </li>
                           
                           <li>guide students in developing academic behaviors for college success (e.g., time management,
                              study, test and note taking strategies, etc.)
                           </li>
                           
                           <li>help students identify academic behaviors that can be adapted as life skills (e.g.,
                              library search skills, decision-making, communication skills, scientific understanding,
                              etc.)
                           </li>
                           
                           <li>assist students in clarifying and developing purpose (attention to life, career, education
                              goals)
                           </li>
                           
                        </ul>
                        
                        
                        <div>
                           
                           <div><a href="LifeMapCertificatePage.html#">Certification Outcomes</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>Faculty members will apply the principles of LifeMap, one of the Essential Competencies
                                       of a Valencia Educator, to improve their practice. 
                                    </li>
                                    
                                    <li>Faculty members will integrate LifeMap and College Success Skills into their current
                                       practice. 
                                    </li>
                                    
                                    <li>Faculty members will engage in continuous improvement processes, through the completion
                                       of an infusion project, with the goal of enhanced student learning. 
                                    </li>
                                    
                                    <li>Faculty members will develop professional relationships throughout the division, campus,
                                       and college. 
                                    </li>
                                    
                                    <li>Faculty members will contribute to an evolving community of peers focused on reflection,
                                       innovation, and enhanced student learning. 
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3><strong> Congratulations to our Valencia faculty and staff who have achieved the <a href="documents/LifeMap-Certification-Staff.pdf" target="_blank">LifeMap Certification</a>! </strong></h3>
                        
                        
                        
                        <h3>Certification Planning Document</h3>
                        
                        <p><span>The planning document provides a list of required and optional courses which can be
                              used to plan your certification or program. It is available as a printer-friendly
                              fillable PDF. </span></p>
                        
                        <ul>
                           
                           <li><a href="../CertificationPlanningDocuments.html">LifeMap Certification Planning Document</a></li>
                           
                           <li><a href="documents/2017-18-LifeMap-Cohort-Schedule.pdf" target="_blank">2017-2018 Cohort Schedule</a></li>
                           
                           <li><a href="documents/2016-17-LifeMap-Cohort-Schedule.pdf" target="_blank">2016-2017 Cohort Schedule</a></li>
                           
                           <li><a href="documents/2015-16-Lifemap-Cohort-Schedule.pdf" target="_blank">2015-2016 Cohort Schedule</a></li>
                           
                        </ul>
                        
                        
                        <h3>Foundational Courses (32 Hours) </h3>
                        
                        <p> *The Foundational Courses must be taken in a specific sequence and have required
                           prerequisites. <strong>Participants will take the required foundational courses as a cohort. Please refer
                              to the cohort schedule above. </strong></p>
                        
                        <p>To begin the certification you must register for the Fundamentals of LifeMap course
                           first. By registering for that course you are confirming your intent to participate
                           in the selected cohort. You will be manually registered for the remaining courses
                           by the faculty development staff. 
                        </p>
                        
                        <div>
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">1. LFMP3339  Fundamentals of LifeMap (Previously LFMP3340 and LFMP3341)<br>&nbsp;<span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>LifeMap is Valencia's name for a developmental advising system designed to increase
                                 students' social and academic integration, development of education and career plans,
                                 and the acquisition of study and life skills.&nbsp; LifeMap describes the ideal progression
                                 of a student in a five-stage model.&nbsp; This course provides the LifeMap conceptual framework
                                 including an in-depth review of the developmental theories on which LifeMap was created.&nbsp;
                                 In addition, the course will focus on the learner-centered advising strategies utilizing
                                 LifeMap concepts and tools. NOTE:&nbsp; This course is required for the LifeMap Certification
                                 program and must be taken first in the series of required courses. <strong>Participants will take the required courses as a cohort. </strong>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">2. LFMP3344  Infusing College Success Skills<span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>Faculty from all disciplines who are committed to enhancing student learning will
                                 benefit from this interactive, project-based course.&nbsp; Participants will develop the
                                 knowledge and strategies to effectively infuse college success skills into student-
                                 learning experiences and will view examples from Valencia experts who successfully
                                 infuse these strategies. Note:&nbsp; This course is required for the LifeMap Certification
                                 program. 
                                 <strong>Participants will take the required courses as a cohort. Please refer to the cohort
                                    schedule below.</strong>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">3. LFMP3346  LifeMap Certificate Capstone<span>20  PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>This LifeMap Certificate Capstone course is designed as the last course within the
                                 LifeMap Certification Program and involves the completion of a LifeMap Infusion Project.&nbsp;
                                 Participants will be asked to integrate what they learned about the LifeMap model,
                                 developmental advising, LifeMap Tools, and LifeMap College Success Skills into a learning
                                 opportunity for one of their courses.&nbsp; Participants will study LifeMap infusion examples
                                 and develop learning outcomes and assessments for their own discipline. The faculty
                                 will implement the project and assess results during the current term (participants
                                 must be teaching in the term registered for this course).&nbsp; Each faculty member will
                                 share the project and the results in implementation and student learning. 
                                 <strong>Participants will take the required courses as a cohort. Please refer to the cohort
                                    schedule below.</strong><br>
                                 <em>Note:&nbsp; This course is required for the LifeMap Certification Program .&nbsp; Participants
                                    must successfully complete the following required prerequisites prior to enrolling
                                    in this course: Fundamentals of LifeMap (LFMP3339, previously LFMP3340 and LFMP3341),
                                    Infusing College Success Skills (LFMP3344) and a minimum of 4 hours of LifeMap electives
                                    coursework.&nbsp;</em>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <h3>Electives (a minimum of 4 Hours) </h3>
                        
                        <div>
                           
                           <div><a href="LifeMapCertificatePage.html#">INDV2252 Introduction to StrengthsQuest<span>(Variable PD Hours)</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>StrenghtsQuest in this introductory course, participants will develop an understanding
                                    of the principles of strenghts-based education. Through self-discovery, participants
                                    will explore their talents, and as a result, learn how to assist facilitate students'
                                    discovery of talents. Participants will receive an overview of Gallup's research,
                                    explore strategies that can be implemented to maximize students' talents, and discuss
                                    ways to infuse strengths-based development into an environment that supports wellbeing
                                    among students, staff, and faculty. NOTE: This  is an elective course for the LifeMap
                                    program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">INDV3259 LinC: Integrating a High Impact Practice (prev. INDV3257/3258)<span>20 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>Learning communities are regarded as a high impact educational practice that has shown
                                    to be beneficial to college students from many backgrounds. This course will review
                                    the foundation of Valencia's <a href="http://www.valenciacollege.edu/linc/">LinC</a> initiative, along with providing the necessary guidelines and strategies to build
                                    community in the classroom and implement integrated learning with two or more joined
                                    courses. The course will help you design a joint syllabus with a faculty partner from
                                    another discipline and partner with a Success Coach to support student learning. Successful
                                    completion of this course is required to teach a LinC course. NOTE: This is an elective
                                    course in the Seneff and LifeMap Certification programs. 
                                 </p>
                                 
                                 <p>Please contact&nbsp;<a href="mailto:rbrighton1@valenciacollege.edu">Robyn Brighton</a>&nbsp;for more information.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LCTS1117  Reading and Writing:&nbsp; Strategies for YOUR Course<span>20 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>This online course focuses on practical strategies and techniques to enhance students'
                                    reading and writing skills in any course.&nbsp; Participants will develop and revise a
                                    plan for implementation of these strategies.&nbsp; NOTE:&nbsp; This is an elective course in
                                    the LifeMap Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LCTS2221 Impacting Student Motivation<span>10 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>In this course, participants will discuss and apply strategies for helping students
                                    to understand motivation and how it influences their thoughts and behaviors.&nbsp; Participants
                                    will read Daniel Pink's <em>Drive</em> as background for the discussions.&nbsp; In registering for this course, you are committing
                                    to attend two (2) meetings, read the book, and develop one activity/lesson on motivation.
                                    NOTE:&nbsp; This  is an elective course for the LifeMap Certification and Seneff Certification
                                    programs.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LCTS3215 Step by Step: Faculty Development for SLS 1122 (no longer offered)<br>&nbsp;<span>Variable PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>Step by Step is an interactive, faculty-led session required for faculty who will
                                    be teaching SLS 1122 Student Success. Step by Step focuses on the four key elements
                                    of the SLS 1122 course: Self Discovery, Study Skills, Academic Planning, and Career
                                    Exploration. You will learn how to design a classroom learning community that focuses
                                    on integrated, active learning, and learn how to assess how students integrate their
                                    learning into their other courses, through the completion of the SLS 1122 Learning
                                    Portfolio.  NOTE: This is an elective course in the LifeMap Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LCTS7111 Introduction to the New Student Experience<span>Variable PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>This course will help prepare faculty to teach the New Student Experience Course (SLS1122).
                                    Participants will learn about course curriculum, effective teaching strategies, and
                                    resources related to the course.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LCTS3216  Supplemental Learning to Enhance Student Learning<span>10 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>Faculty will learn how Supplemental Learning (SL) is used at Valencia to enhance student
                                    learning through SL strategies such as facilitated learning, the integration of college
                                    success skills with course material, and group activities. This interactive course
                                    focuses on the essentials of SL, as well as strategies for integrating SL into your
                                    course and creating a dynamic partnership with the SL Leader to facilitate learning.<br>
                                    Participation in this course does not guarantee a faculty member will be able to teach
                                    a course enhanced by SL. SL is currently focused on Introduction to Statistics, Intermediate
                                    Algebra, College Mathematics, Freshman Composition I and II, General Chemistry I and
                                    General Biology I.  NOTE:&nbsp; This is an elective course in the LifeMap Certification
                                    program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LCTS3217 College Survival Skills: Faculty Development for SLS 1101<span>2 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>This is an interactive session for faculty who will be teaching SLS 1101 College Survival
                                    Skills within the Bridges to Success program. Faculty will learn about the Bridges
                                    to Success program, active learning, and the four key elements of the SLS 1101 curriculum:
                                    Financial Empowerment, Health, Community Service, and Etiquette. NOTE: This course
                                    is required for all faculty who teach SLS 1101 College Survival Skills to Bridges
                                    students. Attendance at this session does not guarantee a teaching opportunity, as
                                    enrollment is dependent on the Bridges department. This is an elective course in the
                                    LifeMap Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LCTS3218 Personal Development: Faculty Development for SLS 1201<span>2 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>This session is designed for faculty who will be teaching SLS 1201 Personal Development,
                                    a course that focuses on the process of understanding personal responsibility and
                                    getting on course to success. Faculty will explore this engaging  curriculum, learn
                                    about the Bridges to Success program, and engage in active learning activities they
                                    can use in the classroom. NOTE: This is an elective course in the LifeMap Certification
                                    program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LFMP1151   Reading Circle (topics vary from year to year)<span>Variable PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>A reading circle discussion of a selected text on LifeMap. Participants will be expected
                                    to engage with the text and with each other, as well as being willing to try new techniques
                                    with their students and report back to the discussion about their results. (Variable
                                    credits; course may be repeated for credit.) NOTE:&nbsp; This is an elective course in
                                    the LifeMap Certification program and the Seneff Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LFMP3345   Learning Support Services on Your Campus<span>4 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>This course is designed to help participants learn more about campus Learning Support
                                    Services available to students.&nbsp; This course will benefit faculty members from all
                                    disciplines. NOTE:&nbsp; This is an elective course in  the LifeMap Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LFMP3348 Continuous Assessment and Responsive Engagement (CARE) Strategies<br>&nbsp;<span>20 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p> Faculty will research and develop intervention strategies for struggling students
                                    that are grounded in continuous assessment and responsive engagement for the purpose
                                    of ensuring student learning. Participants in this course will employ best practices
                                    in the area of early alert to design and implement a CARE strategy in their course.
                                    NOTE: This  is an elective course in  the LifeMap Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>              
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LFMP3349 Supporting Students with Behavioral Concerns<span> 4 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>Students are faced with many challenges such as balancing academics, work, and personal
                                    life. This course focuses on identifying behaviors of students in distress and understanding
                                    recommended response guidelines. In addition, faculty will learn about the related
                                    resources/services helpful to distressed students such as BayCare and Valencia's counselors.
                                    Participants will engage in discussions in order to better understand how to help
                                    students in distress and when to make an appropriate referral. NOTE: This is an elective
                                    course in  the LifeMap Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">LFMP3350 LifeMap Toolbox to Enhance Student Learning<span> 4 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div>
                                 
                                 <p>This course will review the LifeMap tools and resources&nbsp;available to support students
                                    in completing their career and&nbsp;educational goals. Participants will determine and
                                    explore&nbsp;potential uses and design an activity which integrates one&nbsp;of the LifeMap
                                    tools.NOTE: This is an elective course in  the LifeMap&nbsp;Certification program.&nbsp;
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LifeMapCertificatePage.html#">  LOBP3230 Thinking Things Through: Critical Thinking Theory and Practice<span>10 PD Hours</span></a></div>
                           
                           <div> 
                              
                              <div> 
                                 
                                 <p>Over 2 two-hour meetings, participants will discuss and apply the concepts of Paul
                                    and Elder's critical thinking model outlined in Gerald Nosich's book Learning to Think
                                    Things Through:&nbsp; A Guide to Critical Thinking Across the Disciplines.&nbsp; The text offers
                                    both a practical model that can be applied to any discipline as well as exercises
                                    and activities for teaching students how to think more critically. Each participant
                                    will receive a copy of the book after registering. In registering for this course,
                                    you are committing to attend two meetings, read the book, and integrate the model
                                    into one activity/lesson in your class. NOTE: This is an elective course in the LifeMap
                                    Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3"> 
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/lifemapcertificatepage.pcf">©</a>
      </div>
   </body>
</html>