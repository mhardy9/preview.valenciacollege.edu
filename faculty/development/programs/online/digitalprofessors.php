<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Online Teaching &amp; Learning | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/online/digitalprofessors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Online Teaching &amp; Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li><a href="/faculty/development/programs/online/">Online</a></li>
               <li>Online Teaching &amp; Learning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Congratulations Digital Professors! </h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>James Adamski (3/2014)</p>
                                    
                                    <p>Judith Addelston (10/2014)</p>
                                    
                                    <p>Concepcion Alcaide Cranston (2/2015) </p>
                                    
                                    <p>Joan Alexander (11/2014) </p>
                                    
                                    <p>John Allender <span>(11/2012)</span></p>
                                    
                                    <p>Malcolm Alexis <span>(3/2011)</span></p>
                                    
                                    <p>Hector Alfaro (11/2010)</p>
                                    
                                    <p>Benjamin Alonzo (3/2016) </p>
                                    
                                    <p>Ruby Alvarez (6/2013) </p>
                                    
                                    <p>Billie Amstadt (6/2014)</p>
                                    
                                    <p>Martin Denizard Anglero (10/2014) </p>
                                    
                                    <p>Remi Ansiello (6/2013)</p>
                                    
                                    <p>Aron Apanah (6/2013)</p>
                                    
                                    <p>Mea Arline (6/2016)</p>
                                    
                                    <p>Alia Asi (2/2012) </p>
                                    
                                    <p>Rhonda Atkinson (4/2010)</p>
                                    
                                    <p>Seher Awan (6/2012) </p>
                                    
                                    <p>Sharon Bailey (6/2015) </p>
                                    
                                    <p>Kate Baldridge-Hale (9/2015) </p>
                                    
                                    <p>Jason Balserait (6/2014)</p>
                                    
                                    <p>Victoria Barnett <span>(11/2010)</span></p>
                                    
                                    <p>Deborah Barr (2/2014)</p>
                                    
                                    <p>Kristin Ann Bartholomew (5/2014) </p>
                                    
                                    <p>Jennifer Baselice (6/2015)</p>
                                    
                                    <p>Thomsas Baselice (10/2013) </p>
                                    
                                    <p>Jeremy Bassetti (3/2016) </p>
                                    
                                    <p>Donna Beck (12/2013)</p>
                                    
                                    <p>Renee Becker (4/2015)</p>
                                    
                                    <p>Sameena Beg (5/2016)  </p>
                                    
                                    <p>Megan Benson (12/2013)</p>
                                    
                                    <p>Aaron Bergeson (10/2015)</p>
                                    
                                    <p>Joel Berman (4/2010) </p>
                                    
                                    <p>George Bernard (11/2011) </p>
                                    
                                    <p>Karene Best (3/2014) </p>
                                    
                                    <p>Peggy Bivins (4/2011) </p>
                                    
                                    <p>Tatiana Bizon (2/2013) </p>
                                    
                                    <p>Michael Blackburn (6/2015) </p>
                                    
                                    <p>Rebecca Blackburn (3/2014)</p>
                                    
                                    <p>Angela Blewitt (6/2015)  </p>
                                    
                                    <p>Karen Blondeau (5/2012) </p>
                                    
                                    <p>Victor Bondzie (5/2013) </p>
                                    
                                    <p>Eve Bouchard (5/2012)</p>
                                    
                                    <p>Nicolle Boujaber-Diederichs (6/2012) </p>
                                    
                                    <p>Abdallah Boumarate (2/2014)</p>
                                    
                                    <p>Kenneth Bourgoin (5/2016) </p>
                                    
                                    <p>Maureen Branham (3/2016)</p>
                                    
                                    <p>Houston Briggs (3/2011) </p>
                                    
                                    <p>Betty Bright (11/2011) </p>
                                    
                                    <p>Jed Broitman (1/2016) </p>
                                    
                                    <p>Kaitlyn Brooker (3/2015) </p>
                                    
                                    <p>Timothy Brookshire (6/2016)</p>
                                    
                                    <p>Veronica Brown (2/2012) </p>
                                    
                                    <p>Barbara Browning (4/2014) </p>
                                    
                                    <p>David Brunick (11/2009) </p>
                                    
                                    <p>Tatyana Brusentsova (3/2013)</p>
                                    
                                    <p>Elizabeth Bryant (6/2015) </p>
                                    
                                    <p> Lisa Bugden (6/2009) </p>
                                    
                                    <p>Radu Bunea (6/2013) </p>
                                    
                                    <p>Barry Bunn (11/2011) </p>
                                    
                                    <p>Wendi Bush (11/2009) </p>
                                    
                                    <p>Marsha Butler (9/2015) </p>
                                    
                                    <p>Mia Cancarevic (2/2015) </p>
                                    
                                    <p>Roberta Carew (5/2012)</p>
                                    
                                    <p>Bonnie Carmack (6/2016) </p>
                                    
                                    <p>Julia Carpenter (3/2016) </p>
                                    
                                    <p>Tracy Carpenter (2/2010) </p>
                                    
                                    <p>Jennifer Carter (12/2013)</p>
                                    
                                    <p>Debra Carruth (5/2015)</p>
                                    
                                    <p>Sonia Casablanca (6/2015) </p>
                                    
                                    <p>Samira Chater (2/2015) </p>
                                    
                                    <p>Flora Chisholm (6/2016) </p>
                                    
                                    <p>Jessica Chisholm (12/2013)</p>
                                    
                                    <p>Francie Chu (5/2014) </p>
                                    
                                    <p>Nelly Cintron (3/2012) </p>
                                    
                                    <p>Lisa Cohen <span>(11/2012)</span></p>
                                    
                                    <p><span>Lisa Cole (4/2016) </span></p>
                                    
                                    <p>Donna Colwell (3/2016) </p>
                                    
                                    <p>Kevin Colwell (6/2012) </p>
                                    
                                    <p>Amy Comerford (2/2010) </p>
                                    
                                    <p>Angela Cortes (4/2013) </p>
                                    
                                    <p>Candace Cravaritis (5/2014) </p>
                                    
                                    <p>Scott Creamer (4/2012) </p>
                                    
                                    <p>Joel Crichlow (6/2016) </p>
                                    
                                    <p>Lois Crichlow (2/2016) </p>
                                    
                                    <p>Derek Croad (4/2015)</p>
                                    
                                    <p>Elvin Cruz-Vargas (6/2013)</p>
                                    
                                    <p>Caroline Cully (5/2013) </p>
                                    
                                    <p>Dawna Culpeppe (6/2009)</p>
                                    
                                    <p>Cheri Cutter (4/2016) </p>
                                    
                                    <p>Laura D'Alessio (4/2016)</p>
                                    
                                    <p>Tiffany Daniels (6/2015)</p>
                                    
                                    <p>Charles Davis (6/2012)</p>
                                    
                                    <p>Angela Dean (2/2014)</p>
                                    
                                    <p>John DeGraca (6/2013)</p>
                                    
                                    <p>Lynn Desjarlais (3/2015) </p>
                                    
                                    <p>Richard Dexter (4/2013) </p>
                                    
                                    <p>Aida Diaz (10/2009) </p>
                                    
                                    <p>Diego Diaz Lopez (6/2014)</p>
                                    
                                    <p>Hillary Dickens (6/2015) </p>
                                    
                                    <p>John DiDonna (5/2013) </p>
                                    
                                    <p>Colleen Dieckmann (2/2012)</p>
                                    
                                    <p><span>Stacey Lynn DiLiberto<span>(10/2012)</span></span> 
                                    </p>
                                    
                                    <p>Sarah Dockray (5/2013)</p>
                                    
                                    <p>Jeffrey Donley (6/2014) </p>
                                    
                                    <p>Amy Downs (12/2013)</p>
                                    
                                    <p>Debbie Drobney (10/2013)</p>
                                    
                                    <p>Desmond Duncan (11/2011) </p>
                                    
                                    <p>Susan Dunn (3/2012) </p>
                                    
                                    <p>Daniel Dutkofski (10/2015) </p>
                                    
                                    <p>LaShonda Eaddy (6/2013) </p>
                                    
                                    <p>Elizabeth Earle (7/2010)</p>
                                    
                                    <p>Erin Ebanks (6/2015) </p>
                                    
                                    <p>Masood Ejaz (5/2014) </p>
                                    
                                    <p>Melissa Ekberg (4/2010)</p>
                                    
                                    <p>Dina El Musa (6/2015)</p>
                                    
                                    <p>Holly Elliot (4/2015) </p>
                                    
                                    <p>Magdala Emmanuel (6/2014) </p>
                                    
                                    <p>Amita Engineer (5/2014) </p>
                                    
                                    <p>Amber Epperson (4/2015) </p>                  
                                    <p>Dina Fabery (4/2010) </p>
                                    
                                    <p>Elizabeth Faulcon (2/2014)</p>
                                    
                                    <p>Amy Jo Felshaw (6/2016) </p>
                                    
                                    <p>Corinne Fennessy (5/2014) </p>
                                    
                                    <p>Catherine Ferrer (3/2013) </p>
                                    
                                    <p>Terri Fine (5/2013)</p>
                                    
                                    <p>George Flores (6/2016) </p>
                                    
                                    <p>Amanda Foelich (9/2015) </p>
                                    
                                    <p>Karen Fowler (4/2015) </p>
                                    
                                    <p>Patrick Fowler (3/2012) </p>
                                    
                                    <p>Steven Francis (4/2015) </p>
                                    
                                    <p>David Freeman (2/2016)</p>
                                    
                                    <p>Stephanie Freuler (4/2012)</p>
                                    
                                    <p>William Freyer (2/2014) </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Jean Marie Fuhrman (2/2010) </p>
                                    
                                    <p>Richard Gair (4/2011) </p>
                                    
                                    <p>Javier Garces (4/2011) </p>
                                    
                                    <p>Carmen Garcia (4/2016) </p>
                                    
                                    <p>Kay Garner (2/2010)</p>
                                    
                                    <p><span>Mary Sue Gausz (4/2013)</span> 
                                    </p>
                                    
                                    <p>Edie Gaythwaite (2/2013) </p>
                                    
                                    <p>Courtney Gegenheimer (6/2016) </p>
                                    
                                    <p>Claudia Genovese Martinez (9/2015) </p>
                                    
                                    <p>Jill Geraghty <span>(4/2011)</span></p>
                                    
                                    <p>Melissa Giblin (6/2012) </p>
                                    
                                    <p>Kevin Giordano(9/2015)</p>
                                    
                                    <p> Dalia Gil <span>(4/2011)</span></p>
                                    
                                    <p>Jose Gonzalez (4/2011) </p>
                                    
                                    <p>Margaret Gonzalez (2/2012) </p>
                                    
                                    <p>Yolanda Gonzalez (4/2010) </p>
                                    
                                    <p>Susan Gosnell (5/2014) </p>
                                    
                                    <p>Mahendra Gossai (11/2014) </p>
                                    
                                    <p>Victoria Grajeda (2/2012) </p>
                                    
                                    <p>Lauren Grant (11/2011) </p>
                                    
                                    <p>Lisa Gray (6/2008) </p>
                                    
                                    <p>Marilyn Greaves-Rodolfo (3/2013) </p>
                                    
                                    <p>Suzzanne Green (5/2014) </p>
                                    
                                    <p>Annamarie Greller (4/2011) </p>
                                    
                                    <p>Jeannette Griffith (6/2016) </p>
                                    
                                    <p>Stephanie Grimes (4/2010) </p>
                                    
                                    <p>Barbara Gutierrez (12/2013)</p>
                                    
                                    <p>Al Groccia (2/2011) </p>
                                    
                                    <p>Debbie Hall (6/2012) </p>
                                    
                                    <p>Damion Hammock <span>(11/2012)</span></p>
                                    
                                    <p>Cathleen Hansen (3/2013) </p>
                                    
                                    <p>Glenna Heath (2/2016) </p>
                                    
                                    <p>Larry Herndon (6/2013) </p>
                                    
                                    <p>John Hilston (3/2012)</p>
                                    
                                    <p>Jeffrey Hogan (6/2014) </p>
                                    
                                    <p>Heather Holden (5/2015) </p>
                                    
                                    <p>Deidre Holmes DuBois (4/2015) </p>
                                    
                                    <p>Natalie Holter (6/2013) </p>
                                    
                                    <p>Mayra Holzer (3/2012) </p>
                                    
                                    <p>Lynn Howard (4/2011) </p>
                                    
                                    <p>Marie Howard (3/2012)</p>
                                    
                                    <p>Nely Hristova <span>(11/2012)</span></p>
                                    
                                    <p>Ying "Jenny" Hu (4/2014)</p>
                                    
                                    <p>Zachary Hyde (2/2014)</p>
                                    
                                    <p>Elizabeth Ingram (12/2013)</p>
                                    
                                    <p>Jesusa Jackson (6/2012) </p>
                                    
                                    <p>Judy Jackson (9/2011) </p>
                                    
                                    <p>Ralf Jenne (11/2011) </p>
                                    
                                    <p>Margaret Jennings (10/2014) </p>
                                    
                                    <p>Valentine Johns (2/2012) </p>
                                    
                                    <p>Daniela Johnson (12/2015) </p>
                                    
                                    <p>Coleen Jones (6/2013) </p>
                                    
                                    <p>Karen Jorgensen (6/2015) </p>
                                    
                                    <p>Jerrid Kalakay (6/2016) </p>
                                    
                                    <p>Rajeshwart Kalicharan (6/2014) </p>
                                    
                                    <p>Mohua Kar (11/2010)</p>
                                    
                                    <p>Jennifer Keefe (10/2012)</p>
                                    
                                    <p>Lisa Keeton (6/2015) </p>
                                    
                                    <p>Michael Kiely (3/2013) </p>
                                    
                                    <p>Lisabeth King (6/2016) </p>
                                    
                                    <p>Amanda Kirchner (6/2015) </p>
                                    
                                    <p>Gary Kokaisel (2/2016)</p>
                                    
                                    <p>Shari Koopmann (2/2012) </p>
                                    
                                    <p>Leonard Kornblau (3/2016) </p>
                                    
                                    <p>Lindi Kourtellis (3/2012)</p>
                                    
                                    <p>Stephen Kuiper (6/2013) </p>
                                    
                                    <p>Vishma Kunu (4/2016)</p>
                                    
                                    <p>Alan LaCerra (5/2012)</p>
                                    
                                    <p>Carmen Laguer Diaz (6/2015)</p>
                                    
                                    <p>Eric Lane <span>(10/2012)</span></p>
                                    
                                    <p>Laurie Larson (3/2011) </p>
                                    
                                    <p>Andrew Lash (4/2016) </p>
                                    
                                    <p>Jennifer Lawhon <span>(3/2011)</span></p>
                                    
                                    <p><span>James Lee (6/2016) </span></p>
                                    
                                    <p>Shara Lee (11/2014) </p>
                                    
                                    <p>Sonya Lenhof (5/2014) </p>
                                    
                                    <p>Kelli Lewis (4/2016) </p>
                                    
                                    <p>Melissa Lombardi (4/2010) </p>
                                    
                                    <p>Particia Lopez (4/2011) </p>
                                    
                                    <p>Laurinda Lott (5/2014) </p>
                                    
                                    <p>Joe Lynn Look (6/2008) </p>
                                    
                                    <p>Karen Lougheed (10/2013) </p>
                                    
                                    <p>Dennis Lucius (6/2015) </p>
                                    
                                    <p>Julie Lux (4/2010) </p>
                                    
                                    <p>Mabel Machin (4/2011) </p>
                                    
                                    <p>Brian Macon (6/2008) </p>
                                    
                                    <p>Lisa Macon (6/2008) </p>
                                    
                                    <p>Laura Magness (5/2014) </p>
                                    
                                    <p>Jane Maguire (3/2015) </p>
                                    
                                    <p>Kim Mancas (11/2011) </p>
                                    
                                    <p>Jan Mangos <span>(10/2012)</span></p>
                                    
                                    <p>Kathleen Marquis (11/2014) </p>
                                    
                                    <p>Amy Martello (4/2011) </p>
                                    
                                    <p>Dewaun Martin (3/2015) </p>
                                    
                                    <p>Lateshia Martin (11/2015) </p>
                                    
                                    <p>Cynthia Massaad (6/2016) </p>
                                    
                                    <p>Richard Matias <span>(4/2011)</span></p>
                                    
                                    <p>Adrienne Mathews (3/2013) </p>
                                    
                                    <p>James May <span>(2/2011)</span></p>
                                    
                                    <p>Sharon May (2/2015)</p>
                                    
                                    <p>Dianna McBride (1/2016) </p>
                                    
                                    <p>Lee McCain (11/2013)</p>
                                    
                                    <p>Kelli McCarthy (10/2015)</p>
                                    
                                    <p>Jennifer McCormick (5/2013)</p>
                                    
                                    <p>Viktoryia McGrath (2/2015) </p>
                                    
                                    <p>Elizabeth McKenna (4/2013)</p>
                                    
                                    <p>Scott McKenzie (12/2013)</p>
                                    
                                    <p>Carla McKnight (5/2016) </p>
                                    
                                    <p>Melba Medina (12/2013)</p>
                                    
                                    <p>Michael Mendoza (6/2014) </p>
                                    
                                    <p>Joseph Menig (6/2015) </p>
                                    
                                    <p>Ashley Miller (2/2014)</p>
                                    
                                    <p>Carol Miller <span>(10/2012)</span></p>
                                    
                                    <p>Katherine Miller (5/2015) </p>
                                    
                                    <p>Tasha Mills-Foster (6/2016) </p>
                                    
                                    <p>Mary Minion (6/2012)</p>
                                    
                                    <p>Eric Model (5/2012) </p>
                                    
                                    <p>Jenny Mohess (5/2013) </p>
                                    
                                    <p>Julie Montione (2/2016) </p>
                                    
                                    <p>Charlotte Mortimer (5/2013) </p>
                                    
                                    <p>Maria Montalvo (4/2010) </p>
                                    
                                    <p>Brad Mundt (6/2016) </p>
                                    
                                    <p>Josh Murdock <span>(4/2011)</span></p>
                                    
                                    <p>Scha-Jani Murrell (6/2013)</p>
                                    
                                    <p>Soheyla Nakhai (3/2013) </p>
                                    
                                    <p>Elizabeth Nappo (2/2015) </p>
                                    
                                    <p>Buddy Nash (6/2010) </p>
                                    
                                    <p>Nicolas Navarro Castellanos (6/2016) </p>
                                    
                                    <p>Linda Neal (6/2009) </p>
                                    
                                    <p>Carole Nevels (6/2008) </p>
                                    
                                    <p>Rebecca Newman (6/2013) </p>
                                    
                                    <p>Joanna Nazario Grzechowiak (9/2015) </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Julia Nudel (3/2009)</p>
                                    
                                    <p>Erin O'Brien (10/2009) </p>
                                    
                                    <p>Katherine Oses (2/2014)</p>
                                    
                                    <p>Cassandra O'Little (3/2013) </p>
                                    
                                    <p>Kathleen O'Neal (3/2015) </p>
                                    
                                    <p>Ian O'Toole (5/2014)</p>
                                    
                                    <p>Ivan Padron <span>(11/2012)</span></p>
                                    
                                    <p>Gabriela Palou De Jesus (3/2012)</p>
                                    
                                    <p>Sanika Paranjape (6/2015) </p>
                                    
                                    <p>Juan Parra (4/2014) </p>
                                    
                                    <p>Terry Pasfield (2/2010) </p>
                                    
                                    <p>Ellen Pastorino (6/2014) </p>
                                    
                                    <p>Hemangi Patil (12/2015) </p>
                                    
                                    <p>Scott Paxton (10/2015)</p>
                                    
                                    <p>Kathleen Perri (3/2013)</p>
                                    
                                    <p>Patra Petroongrad <span>(11/2012)</span></p>
                                    
                                    <p>Neal Phillips <span>(10/2012)</span></p>
                                    
                                    <p>Florence Pierre-Louis (4/2015)</p>
                                    
                                    <p>Brian Polk <span>(3/2011)</span></p>
                                    
                                    <p><span>Fritzlaine Powell (6/2016)</span></p>
                                    
                                    <p>Jamie Prusak (2/2013) </p>
                                    
                                    <p>Marva Pryor (4/2010) </p>
                                    
                                    <p>Terry Rafter-Carles (6/2012) </p>
                                    
                                    <p>Kinyel Ragland (4/2014) </p>
                                    
                                    <p>Veeramuthu Rajaravivarma (5/2014) </p>
                                    
                                    <p>Cristian Ramirez (4/2012) </p>
                                    
                                    <p>Heidi Ramirez (3/2013) </p>
                                    
                                    <p>April Raneri (6/2016) </p>
                                    
                                    <p>Blanca Rangel-Godinez <span>(10/2012)</span></p>
                                    
                                    <p>Mamta Rao (3/2015) </p>
                                    
                                    <p>Craig Rapp (3/2014) </p>
                                    
                                    <p>Lester Sandres Rapalo (10/2009) </p>
                                    
                                    <p>George Rausch (12/2013)</p>
                                    
                                    <p>Andy Ray (6/2010) </p>
                                    
                                    <p>Gerald Reed (5/2012)</p>
                                    
                                    <p>Nancy Reed (3/2014) </p>
                                    
                                    <p>Stanton Reed (3/2015)</p>
                                    
                                    <p>Denise Richardson (6/2015) </p>
                                    
                                    <p>Nancy Rizzo (5/2012) </p>
                                    
                                    <p>Michael Robbins (4/2013)</p>
                                    
                                    <p>Cheryl Robinson (6/2008) </p>
                                    
                                    <p>Kris Rodgers (3/2014) </p>
                                    
                                    <p>Daniel Rubin (6/2008) </p>
                                    
                                    <p>Kevin Rushing (6/2013) </p>
                                    
                                    <p>Jeremy "Storm" Russo (6/2008) </p>
                                    
                                    <p>Kleber Saavedra <span>(11/2012)</span></p>
                                    
                                    <p>Lorie Anne Sablad (11/2013)</p>
                                    
                                    <p>Brian Sage (2/2014)</p>
                                    
                                    <p>Anna Saintill(3/2009)</p>
                                    
                                    <p>Caroline Salvador (3/2016) </p>
                                    
                                    <p>Edwin Sanchez Velez (3/2016)</p>
                                    
                                    <p>Liza Schellpfeffer (6/2009)</p>
                                    
                                    <p>Derek Schorsch (6/2012) </p>
                                    
                                    <p>Melissa Schreiber (4/2010)</p>
                                    
                                    <p>Edward Schultz (6/2014) </p>
                                    
                                    <p>Katrina Schultz (2/2010) </p>
                                    
                                    <p>Laura Sessions (11/2015) </p>
                                    
                                    <p>Anna Sezonenko (6/2016) </p>
                                    
                                    <p>Vasudha Sharma (2/2014)</p>
                                    
                                    <p>Mariza Shelbrook (1/2014)</p>
                                    
                                    <p>Sharon Shenton (3/2016)</p>
                                    
                                    <p>Kathryn Shepard (6/2010)</p>
                                    
                                    <p>Rebecca Shevlin (2/2013) </p>
                                    
                                    <p>Jamie Shipley (5/2013) </p>
                                    
                                    <p>Nichole Shorter (4/2012) </p>
                                    
                                    <p>Keri Siler (9/2014) </p>
                                    
                                    <p>Les Simmonds (2/2010) </p>
                                    
                                    <p>Lynn Sims (6/2015) </p>
                                    
                                    <p>Allison Sloan (6/2009) </p>
                                    
                                    <p>Crystal Smith (11/2015)</p>
                                    
                                    <p>Keyma Sobratti (5/2015) </p>
                                    
                                    <p>Calvin Snyder (6/2012) </p>
                                    
                                    <p>Michael Snyder (6/2012)</p>
                                    
                                    <p>Nicole Spottke (9/2015)  </p>
                                    
                                    <p>Wanda Stanek (3/2015) </p>
                                    
                                    <p>Margaret Staton (2/2010) </p>
                                    
                                    <p>Laura Stevens (4/2016) </p>
                                    
                                    <p>Peter Stoepker (4/2015) </p>
                                    
                                    <p>Irina Struganova (3/2012)</p>
                                    
                                    <p>Isabella Stryker (5/2015) </p>
                                    
                                    <p>Ronald Stumpf (5/2012)</p>
                                    
                                    <p>Michael Szalma (5/2015) </p>
                                    
                                    <p>Patrick Szymanski (3/2014) </p>
                                    
                                    <p>Christina Tan (6/2015) </p>
                                    
                                    <p>Benjamin Taylor (5/2016) </p>
                                    
                                    <p>Daeri Tenery (4/2012)</p>
                                    
                                    <p>Lynta Thomas (3/2014) </p>
                                    
                                    <p>Stephen Thomas (10/2014) </p>
                                    
                                    <p>Mary Thompson (10/2014) </p>
                                    
                                    <p>Nelson Torres Arroyo (12/2013)</p>
                                    
                                    <p>Ileana Trautwein (10/2014)</p>
                                    
                                    <p>Summer Trazzera (3/2015)</p>
                                    
                                    <p>Heather Trees (4/2015 </p>
                                    
                                    <p>Adriene Tribble (3/2013)</p>
                                    
                                    <p>Marie Trone (4/2016) </p>
                                    
                                    <p>Samuel Tsegaye (6/2013) </p>
                                    
                                    <p>Daniel Turner (3/2012)</p>
                                    
                                    <p>Meenawattie Udho (4/2015)</p>
                                    
                                    <p>Angelica Marie Vagle (12/2015) </p>
                                    
                                    <p>Vanessa Vale Feliciano (3/2015) </p>
                                    
                                    <p>Nicole Valentino (4/2011) </p>
                                    
                                    <p>Carol Van Horn (10/2014) </p>
                                    
                                    <p>Henry Van Putten, Jr. (5/2015)</p>
                                    
                                    <p>Sarah Verron-Bassetti (3/2016) </p>
                                    
                                    <p>Sylvana Vester (4/2016)</p>
                                    
                                    <p>Nancy Vinces (3/2014) </p>
                                    
                                    <p>James Vrhovac (1/2016) </p>
                                    
                                    <p>LaVonda Walker (4/2016) </p>
                                    
                                    <p>Reneva Walker (3/2013) </p>
                                    
                                    <p>Selwyn Walters <span>(2/2013)</span></p>
                                    
                                    <p>Betty Wanielista (6/2008) </p>
                                    
                                    <p>Russell Ward <span>(11/2010)</span></p>
                                    
                                    <p>Chris Weidman <span>(4/2011)</span></p>
                                    
                                    <p>Michael Wheaton (6/2016)</p>
                                    
                                    <p>Lisa Whitmore (6/2016) </p>
                                    
                                    <p>Jane Wiese (6/2010)</p>
                                    
                                    <p>Sharalyn Wight (6/2014) </p>
                                    
                                    <p>Dave Williams (7/2008) </p>
                                    
                                    <p>Tarteashia Williams (6/2013) </p>
                                    
                                    <p>Brian Williamson (2/2015)</p>
                                    
                                    <p>Lauren Wilson (1/2016)</p>
                                    
                                    <p>Valerie Woldman (4/2011) </p>
                                    
                                    <p>Shannon Word (6/2013) </p>
                                    
                                    <p>Chrysalis Wright (5/2014) </p>
                                    
                                    <p>Geni Wright (6/2015) </p>
                                    
                                    <p>Marcelle Wycha (6/2016) </p>
                                    
                                    <p>Abdelbassit Yacoub (9/2015) </p>
                                    
                                    <p>Claire Yates (3/2016) </p>
                                    
                                    <p>Shawn Yawn (1/2016) </p>
                                    
                                    <p>Laurie Youngman (2/2014)</p>
                                    
                                    <p>Wael Yousif (9/2015)</p>
                                    
                                    <p>Areeje Zufari (2/2012) </p>
                                    
                                    <p>Jacquelyn Zuromski (3/2016) </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/online/digitalprofessors.pcf">©</a>
      </div>
   </body>
</html>