<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Quality Matters | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/exemplar/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Quality Matters</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Exemplar</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Quality Matters  Program</h2>
                        
                        
                        <h3><em>Would you like to receive collegial feedback on your online or hybrid course? Consider
                              participating in this  opportunity to have your online or hybrid course reviewed by
                              colleagues using a best-practice framework, Quality Matters. </em></h3>
                        
                        <h3>Quality Matters Peer Review Process at Valencia</h3>
                        
                        <p>Valencia has adopted <a href="http://www.qmprogram.org/" target="_blank" title="http://www.qmprogram.org/">Quality Matters</a> for its peer-based approach to continuous improvement for online and hybrid courses.
                           Quality Matters is:
                        </p>
                        
                        <ul>
                           
                           <li> a set of standards based upon current literature, best practices, and national standards
                              for course design. These standards can be used as a framework to design, revise and
                              improve online and hybrid courses. 
                           </li>
                           
                           <li> a peer review process where Valencia faculty members who have completed Quality Matters
                              training provide feedback on hybrid and online courses according to a set of standards
                              (<a href="http://www.qmprogram.org/rubric" target="_blank" title="http://www.qmprogram.org/rubric">QM Rubric</a>) for course design. 
                           </li>
                           
                        </ul>
                        
                        <p>Quality Matters was designed with the following principles in mind. It is:</p>
                        
                        <ul>
                           
                           <li>a faculty-driven, peer review process</li>
                           
                           <li>collegial, not evaluative or judgemental</li>
                           
                           <li>based upon  a model of continuous improvement </li>
                           
                           <li>centered on the student experience </li>
                           
                           <li>a team-based, collaborative review</li>
                           
                           <li>designed to provide specific, detailed, and constructive feedback and suggestions</li>
                           
                           <li>made to provide feebdack on course design, not course delivery </li>
                           
                        </ul>            
                        
                        <h3>How Does it Work? </h3>
                        
                        <ul>
                           
                           <li><a href="peerCourseProcess.html" target="_blank">Peer Course Review Process</a></li>
                           
                        </ul>            
                        
                        <h3>How Do I Apply to Participate?</h3>
                        
                        
                        <ul>
                           
                           <li>Review the <a href="../online/documents/PreparingforaQualityMattersCourseReviewChecklist.pdf" target="_blank">Preparing for a QM Peer Review Checklist</a> to ensure your course is ready to be reviewed
                           </li>
                           
                           
                        </ul>            
                        <h3>What are the Application Requirements? </h3>
                        
                        <ul>
                           
                           <li>Complete the ASMT 3232 - Quality Matters: Peer Reviewer  Training course and any recent
                              QM refresher training courses. <em>Valencia  faculty members are designated trained Quality Matters Peer Reviewers by
                                 completing all of these courses.</em>
                              
                           </li>
                           
                           <li>Have taught the course  in an online or hybrid format for at least two terms (at the
                              time the course is  reviewed).
                           </li>
                           
                           <li>The course must be completely online or a hybrid course.</li>
                           
                           <li>The course must be designed according to current and approved College Course Outcomes.
                              
                           </li>
                           
                           <li>Please note that priority will be given to faculty members who have achieved the designation
                              of <a href="../online/DigitalProfessorCertification.html" target="_blank">Digital Professor</a>.
                           </li>
                           
                        </ul>            
                        
                        <h3>Who Do I Contact for Questions?</h3>
                        
                        <p><strong>Page Jerzak</strong><br>
                           Associate Director for Online Teaching and Learning <br>
                           <a href="mailto:pjerzak@valenciacollege.edu">pjerzak@valenciacollege.edu</a><br>
                           (407) 582-3865
                        </p>            
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/exemplar/index.pcf">©</a>
      </div>
   </body>
</html>