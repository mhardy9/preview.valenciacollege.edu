<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Quality Matters | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/exemplar/peercourseprocess.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Quality Matters</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li><a href="/faculty/development/programs/exemplar/">Exemplar</a></li>
               <li>Quality Matters</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        <h2>Peer Course Review Process</h2>
                        
                        <h3>QUALITY MATTERS PEER COURSE REVIEW PROCESS</h3>
                        
                        <p>Once an application is submitted, the following steps will be taken:</p>
                        
                        <ol>
                           
                           <li>It will be confirmed that the course, common course outline and the faculty member
                              meet the requirements (see below) for a Valencia Quality Matters Peer Course Review.
                           </li>
                           
                           <li>Courses/ faculty members meeting the requirements are placed on a schedule according
                              to  the Associate Director or QM Coordinator with consent from the faculty member.
                           </li>
                           
                           <li>The QM Coordinator will assign and confirm a review team of 3 Valencia faculty members
                              who have completed QM training.
                           </li>
                           
                           <li>The assigned Review Team Chair will lead the review process, which typically takes
                              about 2-3 weeks to complete, and will submit a Final Review Report, when completed,
                              to the faculty member and the QM Coordinator. 
                           </li>
                           
                           <li>If the course does not  meet QM expectations upon the initial review and the faculty
                              member chooses to  have the course re-reviewed, support will be provided to the faculty
                              member to  implement revisions. The support will be determined upon faculty requests,
                              revisions suggested by the review team, and in consultation with the QM  Coordinator
                              or Associate Director. All course revisions should be completed  within 3 months of
                              the review end date.
                           </li>
                           
                           <li>After course revisions are completed by the faculty member, a re-review by only the
                              Team Chair will take place. The Team Chair will review the course according to those
                              standards not originally met upon the initial review and confirm if course meets QM
                              review standards. 
                           </li>
                           
                           <li>Upon successful completion of the QM review, the faculty member will be notified.</li>
                           
                        </ol>
                        
                        <ul>
                           
                           <li>Note: The results of QM Peer Course Reviews are only available to members of the Review
                              Team, the faculty member, and necessary members of the Faculty and Instructional Development
                              team. 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>APPLICATION REQUIREMENTS</h3>
                        
                        <ul>
                           
                           
                           <li>Complete the ASMT 3232 - Quality Matters: Peer Reviewer  Training course and any recent
                              QM refresher training courses (click <a href="courses.html">here</a> to see upcoming QM  courses). <em>Valencia faculty members are designated trained Quality Matters  Peer Reviewers by
                                 completing all of these courses.</em> 
                           </li>
                           
                           <li>Have taught the course in an online or hybrid format for at least two terms (at the
                              time the course is reviewed).
                           </li>
                           
                           <li>The course must be completely online or a hybrid course.</li>
                           
                           <li>The course must be designed according to current and approved College Course Outcomes.
                              The Common Course Outline should be completed and approved per college policy prior
                              to application.
                           </li>
                           
                           <li>Please note that priority will be given to faculty members who have achieved the designation
                              of <a href="../online/DigitalProfessorCertification.html" target="_blank">Digital Professor</a>.
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>FREQUENTLY ASKED QUESTIONS</h3>
                        
                        <p><strong>1. Is My Course Ready For Review?</strong></p>
                        
                        <p> Use the <a href="../online/documents/PreparingforaQualityMattersCourseReviewChecklist.pdf" target="_blank">Preparing for a QM Peer Review Checklist</a> to ensure the course is ready to be reviewed
                        </p>
                        
                        <p><strong>2. What kind of course access does the review team need?</strong></p>
                        
                        <p>The review team will be reviewing the course from the student  perspective so they
                           will be provided student-level access. The review team will  be given access to the
                           course in a "development space" (this contains  all course content but has no enrolled
                           students or student work in it).
                        </p>
                        
                        <p><strong>3. What is my role in the review as the faculty member/instructor?</strong></p>
                        
                        <p>As the faculty member/instructor I agree to do the following:</p>
                        
                        <ul>
                           
                           <li>Provide access to the  course and its associated materials to selected peer reviewers
                              (this includes  access to any publisher materials or outside websites).
                           </li>
                           
                           <li>Complete the Instructor  Worksheet located on the QM website to provide information
                              to the review team.
                           </li>
                           
                           <li>Participate in the  pre-review discussion to discuss the instructor worksheet, ensure
                              all members  have access to the course, and answer any questions from team members
                              before  the review begins.
                           </li>
                           
                           <li>Communicate with the  review team as needed regarding the course in review.</li>
                           
                           <li>At the time of the final  report, discuss/communicate with the QM Coordinator regarding
                              whether or not  you choose to proceed with course revisions, re-review of the course,
                              and to  discuss any desired optional support resources.
                           </li>
                           
                           <li>For re-review- Complete  the Course Amendment Form on the QM website to provide information
                              to the team  chair about the revisions made, and communicate with the team chair as
                              needed  regarding the course in review. All re-reviews must be completed within three
                              months of the original review date.
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>FOR QUESTIONS</h3>
                        
                        <p><strong>Page Jerzak</strong><br>
                           Associate Director for Online Teaching and Learning <br>
                           <a href="mailto:pjerzak@valenciacollege.edu">pjerzak@valenciacollege.edu</a><br>
                           (407) 582-3865
                        </p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/exemplar/peercourseprocess.pcf">©</a>
      </div>
   </body>
</html>