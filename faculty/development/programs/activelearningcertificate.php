<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certifications &amp; Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/activelearningcertificate.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Certifications &amp; Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Certifications &amp; Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Active Learning Certification</h2>
                        
                        <p>The Active Learning Certification is designed to support  part-time and full-time
                           faculty in transforming students from passive listeners  to active learners while
                           improving learning and deepening understanding. As a  result of the coursework, faculty
                           will learn how to engage students more deeply  in the learning process through meaningful
                           lessons while promoting student  reflection of understanding. At least 36 PD hours
                           of both required and    are needed to earn the certification.
                        </p>
                        
                        <p> To successfully achieve the certification, the faculty  member must complete (3)
                           required courses: Foundations of Active Learning (10  PD hrs.), Impacting Active Learning
                           through Metacognition (10 PD hrs.), and  Active Learning Capstone (10 PD hrs.). At
                           least 6 additional PD hours of    must be taken.
                        </p>
                        
                        <h3> Required Courses</h3>
                        
                        <p><strong> LCTS3290 Foundations of Active Learning - 10  PD Hours</strong><br>
                           This face to face course, is the first course in the Active  Learning Certificate
                           sequence. Throughout the course, participants will be  introduced to the theories
                           and principles of Active Learning, as well as gain  an introduction to the Capstone
                           portfolio assessment at the end of the  certificate process. Specific emphasis will
                           be placed on integrating and  sharing active learning strategies into the classroom.
                           Note: This course is  required for Active Learning Certification.
                        </p>
                        
                        <p> <strong> LCTS3291 Impacting  Active Learning through Metacognition - 10 PD Hours</strong><br>
                           Metacognition, the awareness of one’s own thought processes,  can play a powerful
                           role in what and how we learn. This course will discuss  metacognitive awareness and
                           how it can impact learning. Participants will  examine how metacognitive strategies
                           can be integrated into active learning  activities to support student learning. Participants
                           will also apply  metacognitive principles to their own teaching practices. Note: This
                           course is  required for Active Learning Certification.
                        </p>
                        
                        <p> <strong>LCTS3292  Active Learning Capstone - 10  PD Hours</strong><br>
                           This capstone course includes showcasing a portfolio of work  demonstrating the active
                           learning program outcomes have been met. The  portfolio's artifacts will include lesson
                           plans incorporating active learning  and metacognitive strategies, a variety of student
                           feedback examples, and  self-reflections. Note: This course is required for Active
                           Learning  Certification.<br>
                           
                        </p>
                        <br clear="all">
                        
                        <h3> Electives</h3>
                        
                        <p><strong>LCTS3293  Technology Enhanced Active Learning (TEAL) I - 2 PD Hours</strong><br>
                           Through an immersive technology-enhanced experience, faculty  will explore strategies
                           and tools for implementing active learning with  technology. Looking at active learning
                           through the lens of Adult Learning  Theory, Social Cognitive Theory, and Constructivism,
                           faculty will explore and  evaluate specific educational technologies that align to
                           these learning  theories to their practice.<strong></strong></p>
                        
                        <p><strong> LCTS3294 Technology  Enhanced Active Learning (TEAL) II - 8 PD Hours</strong><br>
                           Continuing the Technology-Enhanced Active Learning Experience  from TEAL 1, participants
                           will build on the concepts that they have begun  mapping, constructing a lesson plan
                           "recipe" with a technology that will  correlate to an active learning activity for
                           their course. In this hands-on  mixed mode course, participants will have the opportunity
                           to test their tech  ideas in the "Test Kitchen" format before posting their lesson
                           plan and active  learning technology tools.
                        </p>
                        
                        <p> <strong> LCTS2212  Engaging Lectures - 2  PD Hours</strong><br>
                           This workshop will explore effective lecture techniques  including the use of media
                           and interactive learning strategies in planning,  organizing, and assessing engaging
                           lectures. Participants will discuss and  reflect on communication delivery style and
                           will leave with practical  application for engaging student learning through the lecture
                           format.
                        </p>
                        
                        <p> <strong> LCTS2223  Asking the Right Questions - 2  PD Hours</strong><br>
                           Well-designed questions function as critical thinking  prompts for students. This
                           workshop will explore how to construct questions for  class discussion and written
                           work that encourages critical thinking.  Participants will be introduced to a theoretical
                           framework for "preparing,  posing and pondering" questions and then use that framework
                           to examine  sample questions and create questions for their classes.<strong></strong></p>
                        
                        <p dir="auto"><strong>LCTS2214: Problem-Based Learning - 2 PD Hours</strong><br>
                           This course explores the learning of subject matter and skill acquisition through
                           collaborative problem-solving. Emphasis is placed on using this method in community
                           college courses.
                        </p>
                        
                        <p dir="auto"><strong>LCTS2217: Project-based Learning - 2 PD Hours</strong><br>
                           In project-based learning, students answer a complex, open-ended question or solve
                           an authentic problem through a collaborative process of investigation. Unlike traditional
                           instruction where students are presented with knowledge and given an opportunity to
                           apply that knowledge, project-based learning starts with the end in mind--a product
                           that requires the learning of essential content and skills. In this workshop, faculty
                           will be introduced to the seven charateristics of project-based learning, as well
                           as techniques for managing a project-based learning experience in one's classroom.
                           Workshop participants will draft a driving-question and will begin developing ideas
                           for project-based learning experiences in their disciplines.
                        </p>
                        
                        <p dir="auto"><strong>LCTS2222: Case-based Teaching - 2 PD Hours</strong><br>
                           Case studies can provide a rich base for developing problem-solving and decision making
                           skills. These critical thinking skills are necessary to meet higher level learning
                           outcomes. This class will explore the most appropriate methods of integrating case-based
                           teaching strategies into your course.
                        </p>
                        
                        <p dir="auto"><strong>LCTS2226: Write to Learn - 2PD Hours</strong><br>
                           Write to learn activities allow students to think through a course concept to demonstrate
                           understanding. As a learning-centered strategy, it assesses whether learning has occurred
                           and promotes confidence. In this workshop, participants will examine several write-to-learn
                           activities designed to deepen discipline learning and create an activity for their
                           course.
                        </p>
                        
                        <p><a href="activeLearningCertificate.html#top">TOP</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/activelearningcertificate.pcf">©</a>
      </div>
   </body>
</html>