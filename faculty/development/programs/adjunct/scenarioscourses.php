<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Associate Faculty Certification | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/adjunct/scenarioscourses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/programs/adjunct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Associate Faculty Certification</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li><a href="/faculty/development/programs/adjunct/">Associate Faculty</a></li>
               <li>Associate Faculty Certification</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <h2>Recommended Courses for Faculty New to Valencia or New to Teaching </h2>
                        
                        
                        
                        
                        
                        <p>With input from part-time and Associate Faculty members, Program Chairs, Deans and
                           other campus leaders, we composed a list of recommended courses for part-time and
                           full-time annually appointed faculty members new to Valencia or new to teaching. These
                           are only recommendations, any courses within the   <a href="../../documents/faculty-development-catalog-2015-16-NF-120815.pdf" target="_blank">Valencia's Faculty Development Catalog of Courses</a>can satisfy the 30 optional course requirement for the <a href="certificateProgram.html" target="_blank">Associate Faculty Certification</a>. 
                        </p>
                        
                        <p>Although there is no required sequence of courses, it is recommended to first take
                           the only required course for the Associate Faculty Certification, Teaching in our
                           Learning College. In this course, you will  develop expertise in each of the Essential
                           Competencies, participate in meaningful discussion with colleagues,  and learn more
                           about our learning college.  In addition, you will develop a My Development Plan that
                           will guide you in the selection of the optional courses according to your individualized
                           needs and interests. 
                        </p>
                        
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/faculty/development/programs/adjunct/documents/MyDevelopmentPlanMaster.docx" target="_blank">My Development Plan</a></li>
                           
                        </ul>
                        
                        
                        <p>When developing your plan, it is recommended that you consult with your dean, department
                           or program chair, or discipline coordinator. Support in developing your plan and selecting
                           which faculty development opportunities may be best suited for you, is available by
                           contacting a member of our <a href="../../centers/locations.html">Faculty Development Team</a>.
                        </p>
                        
                        
                        
                        
                        <h3>Recommended Courses</h3>
                        
                        <div>
                           
                           <div><a href="ScenariosCourses.html#">LCTS1110 Teaching in our Learning College  (previously LCTS1111)<span>30 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This course introduces the Essential Competencies of a Valencia Educator and provides
                                    faculty with tools and resources to become more effective, learning-centered instructors.&nbsp;
                                    NOTE: This course is required for the <a href="certificateProgram.html">Associate Faculty Certification</a>.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">PRFC3371  Learning Partners: Developing Reflective Practitioners <span>20 PD Hours</span> </a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In this course, participants will have an opportunity to examine their current teaching
                                    strategies with a learning partner through mutual classroom observations, inform their
                                    ongoing learning-centered practice through a literature search, showcase their learner-centered
                                    approach through a classroom demonstration, and reflect upon their practice by considering
                                    multiple perspectives: self, student, and colleague.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">ASMT2122 Classroom Assessment Techniques<span>10 PD Hours</span></a></div>
                           
                           
                           <div>
                              
                              <p>This introductory course examines classroom assessment techniques that promote student
                                 learning through consistent, timely, formative measures. Also examined will be assessment
                                 practices that invite student feedback on the teaching and learning process, as well
                                 as on student achievement.&nbsp; In order to complete the weekly assignments, participants
                                 should be teaching a course while enrolled.
                              </p>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">ASMT2227 Understanding and Designing Rubrics<span>5 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In this online, interactive course, participants will learn the elements of rubric
                                    construction and examine a variety of models to use in creating their own discipline-specific
                                    rubrics.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LCTS2224 Interactive Lecture<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Research shows that students lose focus after fifteen to twenty minutes of listening
                                    to a lecture. This workshop will provide participants with strategies to punctate
                                    lectures with active learning techniques to better engage students and improve learning.
                                    NOTE: This workshop is for incoming faculty only.
                                 </p>
                                 
                              </div>
                              
                           </div> 
                           
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">INDV1153 Making Learning Accessible<span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This online course is designed for faculty and staff who wish to develop an enhanced
                                    awareness of learner diversity including those with disabilities.&nbsp; This course will
                                    assist faculty in identifying and utilizing strategies in order to create an optimal
                                    learning environment to teach all learners.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">INDV3352 Classroom Management<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Through a case-based approach, participants will examine effective community college
                                    classroom management processes and strategies that establish positive student achievement
                                    and behavior.&nbsp; Participants will discuss best practices and apply what they have learned
                                    to their classroom settings.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LCTS2225 Flipped Classroom<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The flipped classroom is a pedagogical model in which the typical lecture and homework
                                    elements of a course are reversed - flipped! Students view short video lectures or
                                    narrated presentations outside of class so that class time is freed up for exercises,
                                    projects, or discussions. This allows students to apply the concepts and engage collaboratively
                                    in class as the professor guides their learning. In this course, participants will
                                    review the research, examine models from across the disciplines, and create a flipped
                                    lesson for a specific concept or skill in their course.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LCTS3211 Strategies for Academic Integrity<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course will explore plagiarism and academic integrity. Participants will develop
                                    a comprehensive plan to avoid and detect plagiarism and encourage students to engage
                                    in behaviors supportive of academic integrity. NOTE: This course is optional for Digital
                                    Professor Certification and Seneff Certification program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LCTS3212 Boot Camp for Online Instruction<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This intensive course addresses the pedagogy and technology essentials for teaching
                                    online using Valencia’s learning management system (LMS). Participants will develop
                                    a framework of online best practice strategies for engaging, instructing, communicating
                                    and assessing students in the online environment.<em></em></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LFMP3345 Learning Support Services on Your Campus  <span>4  PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course is designed to help participants learn more about campus Learning Support
                                    Services available to students. This course will benefit faculty members from all
                                    disciplines. NOTE: This is an elective course for the LifeMap and Seneff Certification
                                    programs.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LTAD3286 Tools for Plagiarism Prevention<span>2  PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Explore technology based tools that aide educators in preventing and detecting unoriginal
                                    content in assignment submissions.&nbsp; This course will also explore writing and researching
                                    resources that can be easily integrated into any web-enhanced course. NOTE: This course
                                    is optional for the Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LOBP3230 Thinking Things Through: Critical Thinking<span>10 PD Hours</span>    Theory and Practice</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Over 2 two-hour meetings, participants will discuss and apply the concepts of Paul
                                    and Elder's critical thinking model outlined in Gerald Nosich's book Learning to Think
                                    Things Through: A Guide to Critical Thinking Across the Disciplines. The text offers
                                    both a practical model that can be applied to any discipline, as well as exercises
                                    and activities for teaching students how to think more critically. Each participant
                                    will receive a copy of the book after registering. In registering for this course,
                                    you are committing to attending two meetings, read the book and integrate the model
                                    into one activity/lesson in your class. NOTE: This course is an elective course in
                                    the LifeMap Certification program and the Seneff Certification program. .
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="ScenariosCourses.html#">LTAD3240 Multimedia Tools for All Courses<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> This course will assist participants in identifying and incorporating multimedia
                                    and instructional tools to accommodate students' diverse learning styles. NOTE: This
                                    course is optional for the Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/adjunct/scenarioscourses.pcf">©</a>
      </div>
   </body>
</html>