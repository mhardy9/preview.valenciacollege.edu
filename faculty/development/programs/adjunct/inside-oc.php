<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Associate Faculty Certification | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/adjunct/inside-oc.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/programs/adjunct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Associate Faculty Certification</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li><a href="/faculty/development/programs/adjunct/">Associate Faculty</a></li>
               <li>Associate Faculty Certification</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Inside the OC - 
                           A Path to Associate Faculty Certification for Osceola and Lake Nona Faculty 
                        </h2>
                        
                        
                        <h3>Earn your Associate Faculty Certification</h3>
                        
                        <p>Inside the OC is a new program designed specifically for adjunct and full-time, annually
                           appointed faculty on Osceola and Lake Nona campuses.  It provides and opportunity
                           to earn your <a href="certificateProgram.html" target="_blank">Associate Faculty Certification</a> while also becoming part of a collaborative, campus-based teaching community. 
                        </p>
                        
                        <h3>Benefits</h3>
                        
                        <ul>
                           
                           <li>Earn the designation of Associate Faculty member.</li>
                           
                           <li>Become part of a collaborative community made up of faculty members from your campus.</li>
                           
                           <li>Get tools and training to help you meet the expectations for being a great teacher
                              at Valencia.
                           </li>
                           
                           <li>Take advantage of support and resources specific to the Osceola and Lake Nona campuses.</li>
                           
                           <li>Receive a pay raise (please visit the <a href="certificateProgram.html" target="_blank">Associate Faculty Certification webpage</a> for more details).
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>Get Started Now</h3>  
                        
                        <p>To earn the Associate Faculty Certificate, you will need to complete 60 hours of Faculty
                           Development courses.  Although the only required course is LCTS1110: Teaching in Our
                           Learning College, we recommend the following courses to faculty members seeking initial
                           Associate Faculty Certification through the Inside the OC Program.
                        </p>
                        
                        <div>
                           
                           <div><a href="inside-OC.html#">PRFC1141 OC 101: A Great Place to Teach<span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p> This course is designed to be an introduction to successful teaching at Osceola and
                                    Lake Nona campuses. In this cornerstone course of the Inside the OC program, faculty
                                    members will learn about essential tools and resources as well as have an opportunity
                                    to meet the campus president. 
                                 </p>
                                 
                                 <p>This course is part of the Inside the OC program and is limited to adjunct, full-time,
                                    annually appointed faculty at the Osceola and Lake Nona Campuses.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="inside-OC.html#">* LCTS1110  Teaching in Our Learning College<span>30 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This  hybrid course introduces the Essential Competencies of a Valencia Educator and
                                    provides faculty with tools and resources to become more effective, learning-centered
                                    instructors. Participants will develop a "My Development Plan" and learning-centered
                                    syllabus. *NOTE: This course is required  for the Associate Faculty Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="inside-OC.html#">PRFC1142 OC 102: Get Connected<span>1.5 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    In this individualized session, participants will discuss and refine their “My Development
                                    Plan” with their dean. 
                                 </p>
                                 
                                 <p>NOTE: This course is part of the Inside OC program and is limited to adjunct and annually
                                    appointed faculty at Osceola and Lake Nona campuses. 
                                 </p>
                                 
                                 <p>To participate, faculty members should have taken or be currently enrolled in LCTS
                                    1110: Teaching in Our Learning College. 
                                 </p>
                                 
                                 <p>** After registration, your dean will be in touch to schedule an appointment.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="inside-OC.html#">PRFC3371 Learning Partners: Developing Reflective Practitioners<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> In this course, participants will have an opportunity to examine their current teaching
                                    strategies with a learning partner through mutual classroom observations, inform their
                                    ongoing learning-centered practice through a literature search, showcase their learner-centered
                                    approach through a classroom demonstration, and reflect upon their practice by considering
                                    multiple perspectives: self, student, and colleague. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="inside-OC.html#">PRFC1143 OC 103: Faculty to Faculty<span>2.5 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In this capstone experience within the OC program, faculty will share best practices
                                    and successful coaching strategies with colleagues.
                                 </p>
                                 
                                 <p>NOTE: This course is part of the Inside OC program and is limited to adjunct and annually
                                    appointed faculty at Osceola and Lake Nona campuses. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="inside-OC.html#"> Electives</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Participants will select 12 PD Hours of electives  from the Faculty Development Catalog
                                    according to their "My Development Plan".  These courses can be online, hybrid or
                                    face-to-face.
                                 </p>
                                 
                                 <blockquote>
                                    
                                    <p><strong>Suggested Courses:</strong></p>
                                    
                                    <ul>
                                       
                                       <li>ASMT2122:  Classroom Assessment Techniques (10 PD Hours)</li>
                                       
                                       <li>INDV3352: Classroom Management (20 PD Hours)</li>
                                       
                                       <li>LCTS2213: Active Learning Techniques (2 PD Hours)</li>
                                       
                                       <li>LFMP3345: Learning Support Services on Your Campus (4 PD Hours)</li>
                                       
                                       <li>LOBP3230: Thinking Things Through: Critical Thinking Theory and Practice (10 PD Hours)</li>
                                       
                                       <li>LTAD3240: Multimedia Tools for All Courses (2 PD Hours)</li>
                                       
                                    </ul>
                                    
                                 </blockquote>                                              
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/adjunct/inside-oc.pcf">©</a>
      </div>
   </body>
</html>