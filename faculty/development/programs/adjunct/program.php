<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Program Information | Associate Faculty Certification | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/adjunct/program.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/programs/adjunct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Associate Faculty Certification</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li><a href="/faculty/development/programs/adjunct/">Associate Faculty</a></li>
               <li>Program Information</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Program Information</h2>
                        
                        
                        <p>This certification is Valencia's way of annually recognizing and rewarding part-time
                           faculty members and full-time annually appointed faculty members who take advantage
                           of Valencia's professional development activities. The certification supports faculty
                           members in their ongoing commitment to the enhancement of their knowledge, skills
                           and abilities that lead to student learning or academic success.
                        </p>
                        
                        <p>Be a part of a professional development program designed specifically for part-time
                           and full-time, annually appointed faculty members. It provides an opportunity to enhance
                           your teaching practice and student learning, while also becoming a part of a collaborative,
                           innovative teaching community. 
                        </p>
                        
                        <p>Benefits</p>
                        
                        <ul>
                           
                           <li>Earn the designation of Associate Faculty member.</li>
                           
                           <li>Become part of a collaborative community made up of faculty members from your campus
                              and college-wide.
                           </li>
                           
                           <li>Get tools and training to help you meet the expectations for being a great teacher
                              at Valencia.
                           </li>
                           
                           <li>Take advantage of support and resources.</li>
                           
                           <li>Receive a pay raise (see Compensation below).</li>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h3>Program Details</h3>
                                 
                                 <h4>Eligibility</h4>
                                 
                                 <ul>
                                    
                                    <li>Part-time faculty </li>
                                    
                                    <li>Full-time, annually appointed faculty<strong> </strong></li>
                                    
                                    <li>Non-faculty, full-time staff </li>
                                    
                                 </ul>
                                 
                                 <h4>Compensation</h4>
                                 
                                 <ul>
                                    
                                    <li>Part-time faculty members receive an increase in pay by approximately $40.00 per contact
                                       hour taught (a 3 contact hour course would provide approximately a $120.00 increase).
                                       *For exact amounts, please review pages 5-6 of the <a href="documents/Valencia-College-Salary-Schedule-for-Fiscal-Year-2017-18.pdf" target="_blank"> Salary Schedule</a>.
                                       
                                    </li>
                                    
                                    <li>Full-time, annually appointed faculty members and non-faculty, full-time staff receive
                                       an increase in pay by approximately $40.00 per contact hour taught for overload courses
                                       only (a 3 contact hour course would provide approximately a $120.00 increase). *For
                                       exact amounts, please review pages 5-6 of the <a href="/faculty/development/programs/adjunct/documents/Valencia-College-Salary-Schedule-for-Fiscal-Year-2017-18.pdf" target="_blank"> Salary Schedule</a>.
                                       
                                    </li>
                                    
                                    <li>
                                       Continuing Education instructors and part-time faculty compensated hourly or by clock-hour
                                       are not eligible for additional compensation associated with the Associate Faculty
                                       program.
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <h4>Annual Renewal</h4>
                                 
                                 <p>After the initial Associate Faculty Certificate is awarded, Associate Faculty are
                                    required to renew the certification annually. To continue receiving the increased
                                    compensation, faculty must complete 20 hours of qualifying faculty development activities
                                    each year (from July 1 - June 15) and apply for the Associate Faculty renewal. The
                                    renewal process is not automatic. For more information, click on the <em>Renewal Process</em> tab above. 
                                 </p>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h3>Course Requirements</h3>
                                 
                                 <p>To earn the Associate Faculty Certificate, you will need to complete 60 hours of Faculty
                                    Development courses (see below). Earned hours are verified through <a href="/faculty/development/howToRegisterForCourses.php"> Professional Development transcripts</a>. After attaining the required 60 hours, complete the online Associate Faculty Certification
                                    Application by June 15th. Qualification in this program is not automatic. 
                                 </p>
                                 
                                 <h4>1. Required Course (30 Hours)</h4>
                                 
                                 <blockquote>
                                    
                                    <p><strong>LCTS1110: Teaching in Our Learning College (Previously LCTS1111) - 30 PD Hours </strong></p>
                                    
                                    <p>This course introduces the Essential Competencies of a Valencia Educator and provides
                                       faculty with tools and resources to become more effective, learning-centered instructors.
                                       Participants will develop a "My Development Plan" and learning-centered syllabus.
                                       <em>*It is recommended to successfully complete this  course before completing optional
                                          courses.</em></p>
                                    
                                 </blockquote>
                                 
                                 <h4>2. Optional Courses (30 Hours)</h4>
                                 
                                 <blockquote>
                                    
                                    <p><strong>Successfully complete 30 professional development (PD) hours by taking any combination
                                          of courses within the Faculty and Instructional Development Catalog of Courses. </strong></p>
                                    
                                 </blockquote>
                                 
                                 <h4>*Recommended Optional Courses and Course Schedule </h4>
                                 
                                 <blockquote>
                                    
                                    <p>For a list of recommended optional courses for faculty new to Valencia or new to teaching,
                                       click <a href="ScenariosCourses.php" target="_blank">here</a> or under the <em>Recommended Courses</em> link on the navigation bar to the left. 
                                    </p>
                                    
                                 </blockquote>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h3>Renewal Process</h3>
                                 
                                 <p>After the initial Associate Faculty Certificate is awarded, Associate Faculty are
                                    required to renew the certification annually.&nbsp; To continue receiving the increased
                                    compensation, faculty must complete 20 hours of qualifying faculty development activities
                                    each year (from July 1 – June 15) and apply for the Associate Faculty renewal.&nbsp; The
                                    renewal process is not automatic.
                                 </p>
                                 
                                 <p><strong>NOTICE:</strong> For faculty members renewing in the Associate Faculty Program who were granted initial
                                    Associate Faculty status prior to Spring 2009, contact Dori Haggerty at 407-582-1051
                                    or<a href="mailto:dhaggerty2@valenciacollege.edu"> dhaggerty2@valenciacollege.edu</a> for specific details regarding your renewal. 
                                 </p>
                                 
                                 <p><strong>How do I renew my Associate Faculty Certificate?</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Complete 20 hours of qualifying faculty development activities from July 1st – June
                                       15th
                                    </li>
                                    
                                    <li>Complete the Associate Faculty Renewal Application by June 15th (see <em>Application</em> tab above) 
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>What happens if I do not meet the criteria to renew my Associate Faculty Annual Certificate?</strong></p>
                                 
                                 <p>If a faculty member does not meet the criteria for the Associate Faculty Annual Certification
                                    Renewal by the June 15th deadline, the Associate Faculty Status will become inactive
                                    and the increase in compensation will cease beginning the next Fall term.&nbsp; The faculty
                                    member can regain the Associate Faculty Annual Certificate in any subsequent year
                                    by following the Renewal Process.
                                 </p>
                                 
                                 <h4>TIMELINE FOR RENEWAL </h4>
                                 
                                 <table class="table table">
                                    
                                    <tbody>
                                       
                                       <tr>
                                          
                                          <th>
                                             
                                             <p>Document</p>
                                             
                                          </th>
                                          
                                          <th>
                                             
                                             <p>Party Responsible</p>
                                             
                                          </th>
                                          
                                          <th>
                                             
                                             <p>Deadline</p>
                                             
                                          </th>
                                          
                                          <th>
                                             
                                             <p>Action</p>
                                             
                                          </th>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p>Associate Faculty Certification Renewal Application</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>Eligible Faculty Members </p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>June 15</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>Application Completion</p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p>Transcript Verification Process</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>Office of Faculty Development</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>July 1</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>Associate Faculty list compiled </p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>Notification email and letter sent</td>
                                          
                                          <td>Office of Faculty Development</td>
                                          
                                          <td>July 15</td>
                                          
                                          <td>Notify applicants of the status of their application </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p>Annual&nbsp; Report of Associate Faculty </p>
                                             
                                             <p>(Effective for Fall, Spring, Summer A, B, C)</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>Office of Faculty Development</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>July 15</p>
                                             
                                          </td>
                                          
                                          <td>
                                             
                                             <p>Annual Report of Associate Faculty sent to Human Resources and Divisions</p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                    </tbody>
                                    
                                 </table>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h3>Frequently Asked Questions</h3>
                                 
                                 <ol>
                                    
                                    <li> 
                                       When do I receive the compensation?
                                       
                                       <ul>
                                          
                                          <li>Annual Associate Faculty Certification compensation is effective for the Fall, Spring,
                                             and Summer terms in the academic year after which certification is awarded.&nbsp; 
                                          </li>
                                          
                                          <li>To continue earning the Associate Faculty Certification compensation, annual renewal
                                             is required.
                                          </li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    <li>
                                       What is a “qualifying” faculty development activity?
                                       
                                       <ul>
                                          
                                          <li>Any seminar, workshop or course listed in <a href="/faculty/development/documents/faculty-development-catalog.pdf" target="_blank">Valencia's Faculty and Instructional Development Catalog of Courses</a>.
                                          </li>
                                          
                                          <li>Please note that Valencia Employee Development workshops do NOT count.</li>
                                          
                                          <li><a href="/faculty/development/howToRegisterForCourses.php" target="_blank">How to select and register for faculty development activities</a>.
                                          </li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    <li>
                                       How do I apply for my initial Associate Faculty Certificate?
                                       
                                       <ul>
                                          
                                          <li>Complete the required 60 hours of qualifying faculty development activities by June
                                             15.&nbsp; 
                                          </li>
                                          
                                          <li>Complete the online Associate Faculty Certification Application by June 15.</li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    <li>
                                       When should I complete the online application for the initial Associate Faculty Certificate?
                                       
                                       <ul>
                                          
                                          <li>Faculty members should complete the online application AFTER the required hours have
                                             been completed. 
                                          </li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    <li>
                                       How do I keep track of my professional development hours?
                                       
                                       <ul>
                                          
                                          <li>As of Fall 2005, a transcript of professional development activities has been available
                                             to faculty members through the Atlas system.
                                          </li>
                                          
                                          <li><a href="/faculty/development/howToRegisterForCourses.php" target="_blank">How to print a professional development transcript</a></li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                 </ol>
                                 
                                 
                              </div>
                              
                              <!--*************************TAB 3***********************-->
                              
                              <div>
                                 
                                 <h3>Application</h3>
                                 
                                 <p>Now accepting applications for 2018. Deadline: June 15, 2018
                                    
                                 </p>
                                 
                                 <p><a href="http://net4.valenciacollege.edu/forms/faculty/development/programs/adjunct/apply.cfm" target="_blank">APPLY</a>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <p><strong>Osceola/Lake Nona Campus Faculty</strong> - visit the <a href="inside-OC.php" target="_blank" title="Inside the OC">Inside the OC</a> webpage for the latest information about this program on your campus.
                           </p>
                           
                           <p><strong>Winter Park Campus Faculty</strong> - visit the <a href="inside-WP.php" target="_blank" title="Inside Winter Park">Inside Winter Park</a> webpage for the latest information about this program on your campus.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/adjunct/program.pcf">©</a>
      </div>
   </body>
</html>