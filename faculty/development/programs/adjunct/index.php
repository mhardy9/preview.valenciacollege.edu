<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certifications &amp; Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/adjunct/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/programs/adjunct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Associate Faculty Certification</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Associate Faculty</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Associate Faculty Certification</h2>
                        
                        <p>Valencia's Office of Faculty and Instructional Development  welcomes you to be a part
                           of a professional development program designed specifically for part-time 
                           and full-time, annually appointed faculty members. The Office of Faculty and Instructional
                           Development, a division of Academic Affairs, supports all faculty members as they
                           expand their 
                           professional practices and examine their ongoing development of the Essential Competencies
                           of a Valencia Educator. 
                           The intention of Valencia’s Faculty Development program is to engage teachers, scholars
                           and practitioners in 
                           continuous improvement processes that result in student learning.
                        </p>
                        
                        <p>We invite you to participate in all of our courses and programs, each  designed to
                           enhance your teaching practice and student learning, while also providing an opportunity
                           to become a part of a collaborative, innovative teaching community. We offer many
                           opportunities, including face-to-face seminars, individual and group consultations,
                           peer observation of teaching sessions, hybrid and online courses. Courses are designed
                           and facilitated by Valencia experts...your colleagues. 
                        </p>
                        
                        <p>Faculty Development courses, resources and programs give faculty members the opportunity
                           to network with campus and college-wide colleagues, engage in meaningful discussions
                           about learning-centered topics, develop strategies to create an effective learning
                           environment for all instructional modalities, and develop a growing expertise in the
                           seven <a href="../../tla/Candidate/tla_competencies_LCF.php" target="_blank">Essential Competencies of a Valencia Educator</a>. These competencies are:<br>
                           
                        </p>
                        
                        
                        <ul>
                           
                           <li>Assessment</li>
                           
                           <li>Inclusion and Diversity</li>
                           
                           <li>Learning-centered Teaching Practices</li>
                           
                           <li>LifeMap</li>
                           
                           <li>Professional Commitment</li>
                           
                           <li>Outcomes-based Practice</li>
                           
                           <li>Scholarship of Teaching and Learning</li>
                           
                        </ul>
                        
                        <h3>HOW TO BEGIN<br> 
                           
                        </h3>
                        Create your plan for development today! Whether you are looking for a single course
                        or interested in working towards a certificate program, you can begin by taking one
                        or more of the following steps:
                        
                        <ul>
                           
                           <li> Review our <a href="certificateProgram.php" target="_blank">Associate Faculty Certification</a> and <a href="ScenariosCourses.php" target="_blank">recommended courses</a>
                              
                           </li>
                           
                           <li>Review the <a href="../../documents/faculty-development-catalog-2017-18.pdf">Catalog of Courses</a> 
                           </li>
                           
                           <li>Review all of our <a href="../index.php">Faculty Development Programs</a> 
                           </li>
                           
                           <li>Search for courses by <a href="../../concentrations/index.php">area of interest</a>
                              
                           </li>
                           
                           <li><a href="../../howToRegisterForCourses.php">Register Now!</a></li>
                           
                        </ul>            
                        
                        <h3>Certification Planning Document<br>
                           
                        </h3>
                        
                        <p>The planning document provides a list of required and optional courses which can be
                           used to plan your certification or program. It is available as a printer-friendly
                           fillable PDF. <a href="../../CertificationPlanningDocuments.php"><u>Associate Faculty Certification Planning Document</u></a> 
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../../howToRegisterForCourses.php" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../../CertificationPlanningDocuments.php" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../../centers/locations.php" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/adjunct/index.pcf">©</a>
      </div>
   </body>
</html>