<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certifications &amp; Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/digitalprofessorcertification.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Certifications &amp; Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Certifications &amp; Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Digital Professor Certification</h2>
                        
                        <p>This series is designed to culminate in the certification of faculty as "Digital Professors."&nbsp;This
                           designation will indicate completion of 20 hours of professional development in the
                           areas of pedagogy and technology in online/hybrid teaching and learning.&nbsp;<em> Note:&nbsp; Participants should take <a href="../concentrations/online/BootCampOnlineInstruction.html">Boot Camp for Online Instruction</a> (LCTS3212) or <a href="../concentrations/Blackboard.html">Blackboard Fundamentals</a> (LTAD3282) prior to enrolling in Digital Professor courses.</em>
                           <br>
                           To successfully complete the Digital Professor Certification, the faculty member must
                           complete the following certification requirements. 
                        </p>
                        
                        <ul>
                           
                           <li> 
                              <strong>16 hours</strong> of <strong>required pedagogy</strong> courses
                           </li>
                           
                           <li> at least <strong>2 hours</strong> of <strong>optional pedagogy</strong> courses 
                           </li>
                           
                           <li>and at least <strong>2 hours</strong> of <strong>optional technology</strong> courses
                           </li>
                           
                        </ul>            
                        
                        
                        <h3>We would like to congratulate our</h3>
                        
                        <h3> <a href="../concentrations/online/DigitalProfessors.html" target="_blank">Distinguished Digital Professors!</a>
                           
                        </h3>
                        
                        
                        <h3>Certification Planning Document</h3>
                        
                        <p><br>
                           Monthly reports are pulled and Digital Professor Certifications will be automatically
                           awarded. <span>The planning document provides a list of required and optional courses which can be
                              used to plan your certification or program. It is available as a printer-friendly
                              fillable PDF. </span><u><a href="../CertificationPlanningDocuments.html">Digital Professor Certfication Planning Document</a></u> 
                        </p>
                        
                        <h3>REQUIRED PEDAGOGY COURSES  (16 hours)</h3>
                        
                        
                        <div>
                           
                           <div><a href="DigitalProfessorCertification.html#">LCTS3242 Developing Interactive Web-based Courses<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <blockquote>
                                    
                                    <blockquote>
                                       
                                       <blockquote>
                                          
                                          <blockquote>
                                             
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                    </blockquote>
                                    
                                 </blockquote>
                                 
                                 <p>This orientation to online pedagogy includes an analysis of the best practices of
                                    online education from the perspectives of students, faculty and institutions as it
                                    relates to adult learning theory, (continued) development of online learning communities,
                                    technology usage, expectations, assessment, evaluation, communication and diversity.
                                    The participant will establish an understanding of best practices and establish practical
                                    applications for use in their online classrooms. Note: This course is required for
                                    Digital Professor Certification.                      
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">ASMT3232 Quality Matters: Peer Reviewer Training<span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This course will explore the Quality Matters project and processes and will prepare
                                    the participant to be part of an initiative that impacts the design of online courses
                                    and ultimately, student success.&nbsp; Participants should be experienced online instructors.&nbsp;
                                    After successfully completing this course, participants will be eligible to serve
                                    on a Valencia Quality Matters peer course review.&nbsp; Note: This course is required for
                                    Digital Professor Certification                      
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">PRFC3244 Legal Issues and the Virtual Student<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Identify the implications of TEACH, Digital Millennium Act, and copyright on digital
                                    rights management for virtual faculty and students. Understand the implications of
                                    Federal Educational Rights to Privacy Act (FERPA) when communicating in the virtual
                                    environment. This course will also explore plagiarism and academic integrity in the
                                    online classroom.&nbsp; Note: This course is required for faculty seeking Digital Professor
                                    Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">ASMT3353 Authentic Learning and Online Assessment<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Concepts and best practice strategies from current literature about learning-centered
                                    online testing and grading are presented.&nbsp; Participants will share strategies and
                                    suggestions to design and improve online authentic assessments.&nbsp; Note:&nbsp; This online
                                    course is required for Digital Professor Certification.                      
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">INDV3246 Universal Design for ONLINE Learning<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Universal design for learning is “the design of products and environments  useable
                                    by all people, to the greatest extent possible, without the need for adaptation or
                                    specialized design.” Universal design benefits all users including students with unique
                                    learning needs. This course will explore the creation of digital course content that
                                    can be used by the broadest audience possible. Note: This course is required for Digital
                                    Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">INDV3248 Building Online Learning Communities<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This course will engage participants in the concepts, effective strategies and best
                                    practices in creating and maintaining an online learning community.&nbsp; Note: This course
                                    is required for Digital Professor Certification.<em><br>
                                       </em></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        <h3>OPTIONAL PEDAGOGY COURSES (a minimum of 2 hours)</h3>
                        
                        
                        <div>
                           
                           <div><a href="DigitalProfessorCertification.html#">INDV3358 Facilitating Online Discussions<span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This course is designed to enhance participants’ ability to effectively facilitate
                                    discussions in the online environment.&nbsp; Participants will examine the facilitator’s
                                    role in online discussions that build community and enhance student learning.&nbsp; Participants
                                    will enhance their facilitation skills through activities with their peers that simulate
                                    facilitator-student interaction.&nbsp; NOTE: This course is optional for Digital Professor
                                    Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LCTS1112 Succeeding with Online Group Work<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This scenario-based online course focuses on designing and facilitating group projects
                                    in the online environment.&nbsp; The focus is using student groups in online classes, but
                                    the insights will translate to the face-to-face classroom as well.&nbsp; NOTE: This course
                                    is optional for Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LCTS3125 Engaging the Online Learner<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Engagement is the critical element for student success and retention in online classes.
                                    In this course, participants will explore virtual student engagement and develop a
                                    variety of resources and activities to connect with the online learner. NOTE: This
                                    course is optional for Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LCTS3137 Facilitating Online Learning<span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This scenario-based online course is designed for instructors who are currently teaching
                                    online or are planning to in the near future. This course will also be of great value
                                    to those involved in designing and developing online courses. It focuses on understanding
                                    the characteristics and needs of the online student and developing strategies to facilitate
                                    effective online discussions, assessment and ultimately the development of an online
                                    learning community.&nbsp; NOTE: This course is optional for Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LCTS3211 Strategies for Academic Integrit<span>y</span><span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This course will explore plagiarism and academic integrity.&nbsp; Participants will develop
                                    a comprehensive plan to avoid and detect plagiarism and encourage students to engage
                                    in behaviors supportive of academic integrity.&nbsp; NOTE: This course is optional for
                                    Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LCTS3247 Maximizing Hybrid Learning <span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Participants will examine best practices and models for developing a hybrid course.
                                    Particular attention will be paid to appropriately leveraging the face-to-face and
                                    online components of hybrid course design.&nbsp; Participants will develop a plan for the
                                    delivery of their course.&nbsp; NOTE: This course is optional for the Digital Professor
                                    Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LCTS3280 Teaching and Learning with Social Media Part I <span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Participants will explore teaching and learning strategies utilizing social media
                                    to enhance student learning.&nbsp; This course will showcase various applications and how
                                    they can be used to enhance communication, collaboration, and student engagement.&nbsp;
                                    Social and legal issues relating to the use of social media in higher education will
                                    also be explored.&nbsp; Participants will develop a plan to appropriately integrate social
                                    media into a specific course.&nbsp; NOTE: This course is optional for the Digital Professor
                                    Certification and is part one of a two-part series on social media. Part II, LTAD
                                    3383, provides an in-depth overview of various social media tools.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LFMP3115 Designing an Effective Online Student Orientation<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Getting students off on the right foot in an online class can mean the difference
                                    between success and failure. This workshop will engage participants in addressing
                                    the orientation needs of the virtual student. Participants will explore the essential
                                    elements and develop a plan for an effective online orientation. In addition, participants
                                    will review the resources in place at Valencia to help faculty provide a comprehensive
                                    student orientation. NOTE: This course is optional for Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        <h3>OPTIONAL TECHNOLOGY COURSES (a minimum of 2 hours)</h3>
                        
                        
                        <div>
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3240 Multimedia Tools for All Courses<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This course will assist participants in identifying and incorporating multimedia and
                                    instructional tools to accommodate students' diverse learning styles. NOTE: This course
                                    is optional for the Digital Professor Certification.                      
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3241 Podcasting 101(not currently offered) <span>4 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3270 Build Your Own Online Lessons (prev. SoftChalk)<span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This hands-on course will support faculty in identifying, creating, and integrating
                                    discipline-specific reusable learning objects. Faculty will learn how to use SoftChalk
                                    Lesson Builder to create original interactive course content.&nbsp; Faculty will also learn
                                    how to access and integrate RLOs from various digital repositories. NOTE:&nbsp; This course
                                    is optional for the Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3284 Mobile Learning (not currently offered) <span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>This introductory course on mobile learning will provide participants with an overview
                                    of mobile learning activities, including formative assessments, single topic lessons,
                                    and collaborative projects. Participants will also review mobile applications compatible
                                    with Blackboard learning management systems, with an emphasis on maximizing integration
                                    of iPads, iPhones and other mobile devices for teaching and learning. NOTE: This course
                                    is optional for Digital Professor Certification.
                                 </p>
                                 
                                 <p><em>Not Currently Being Offered</em><br>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3285 Test and Activity Creators (prev. Respondus &amp; Studymate)<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Bring your quizzes and test banks. Get ready to learn how to quickly and easily develop,
                                    edit, and publish quizzes, self-tests, flashcards, crossword puzzles, and many other
                                    interactive assessment tools to your course.&nbsp; NOTE: This course is optional for the
                                    Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3286 Tools for Plagiarism Prevention<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Explore technology based tools that aide educators in preventing and detecting unoriginal
                                    content in assignment submissions.&nbsp; This course will also explore writing and researching
                                    resources that can be easily integrated into a blackboard-enabled course. NOTE: This
                                    course is optional for the Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3287 Online Video Orientations<span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This two session hybrid course will explore best practices, planning, development,
                                    and delivery of a high-quality reusable video orientation for your course.&nbsp; This course
                                    will culminate in the taping of your online video orientation.&nbsp; NOTE: This course
                                    is optional for the Digital Professor Certification.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div><a href="DigitalProfessorCertification.html#">LTAD3383 Teaching and Learning with Social Media  Part II <span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>In this course, faculty will focus on using various social media tools that can be
                                    utilized to enhance the communication and engagement of students. This course will
                                    include an in-depth overview of Twitter, Facebook, Goggle+ and other emerging social
                                    media platforms.  NOTE: This course is optional for the Digital Professor Certification
                                    and is part two of a two-part series on social media. Participants should successfully
                                    complete part one, LCTS3280, before enrolling in this course.  
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>FOR MORE INFORMATION</h3>
                        
                        <p><strong>Page Jerzak </strong></p>
                        
                        <p> Director for Online Teaching and Learning <br>
                           <a href="mailto:pjerzak@valenciacollege.edu">pjerzak@valenciacollege.edu</a><br>
                           (407) 582-3865
                        </p>
                        <br>          
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/digitalprofessorcertification.pcf">©</a>
      </div>
   </body>
</html>