<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certifications &amp; Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/programs/studyabroadplcert.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Certifications &amp; Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/programs/">Programs</a></li>
               <li>Certifications &amp; Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Study Abroad Program Leader Certification </h2>
                        
                        <p>This series is designed to culminate in the certification of faculty as  Study Abroad
                           Program Leaders.&nbsp; The designation will indicate completion of  20 hours of professional
                           development in the area of study abroad and global  experiences and will prepare faculty
                           members to develop and lead a meaningful  short-term study abroad experience for Valencia’s
                           students.
                        </p>
                        
                        <p> To  successfully complete the Study Abroad Program Leader Certification and meet
                           the <a href="https://valenciacollege.edu/international/studyabroad/faculty-staff/leadstudyabroad/" target="_blank">qualifications  to lead a study abroad program</a>,  the full-time faculty member must complete three courses in the prescribed  order
                           below.
                        </p>
                        
                        
                        
                        <a class="box_feat" href="http://wp.valenciacollege.edu/thegrove/prestigious-andrew-heiskell-award-cites-valencias-innovation-in-international-education-2/"> <i aria-hidden="true" class="far fa-info-circle"></i> 
                           
                           
                           <p>The Study Abroad Program Leader Certification Program was recognized by The Institute
                              of International Education (IIE) on January 2014.
                           </p>
                           </a>
                        
                        
                        <h3><strong>Congratulations to our newly certified </strong></h3>
                        
                        <h3>
                           <span><strong><a href="../concentrations/documents/StudyAbroadProgramLeadersWebsiteList.pdf">Study Abroad Program Leaders</a></strong></span><strong>!</strong>
                           
                        </h3>
                        
                        
                        <h3>Certification Planning Document</h3>
                        
                        <p><span>The planning document provides a list of required and optional courses which can be
                              used to plan your certification or program. It is available as a printer-friendly
                              fillable PDF. </span><a href="../CertificationPlanningDocuments.html">Study Abroad Program Leader Certification Planning Document</a></p>
                        
                        <h3>Required Courses (24 Hours) </h3>
                        
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <div>                  </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="studyabroadplcert.html#">LCTS3315 Introduction to SAGE <span>6 Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Participants will engage in an overview of the Study Abroad program, the student global
                                    distinction pathway, and global competency skills. Additionally, participants will
                                    develop a global perspective as well as explore different models for internationalizing
                                    curriculum and high impact educational practices.<br>
                                    
                                    Note: This is a required course in the Study Abroad Program Leader Certification and
                                    must be taken first in the series of required courses Stewardship as a Study Abroad
                                    Program Leader (INDV 3356) and Designing a Study Abroad Proposal (LCTS 3316). Although
                                    leading a study abroad program is limited to full-time faculty, this course is appropriate
                                    for all faculty interested in learning more about internationalizing their curriculum.
                                 </p>
                                 
                                 <div><a href="studyabroadplcert.html#">INDV3356 Stewardship as a Study Abroad Leader <span>4 Hours</span></a></div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Participants will develop global leadership skills to lead study abroad experiences.
                                          Participants will learn how to create an atmosphere of inclusion and mitigate issues
                                          when studying abroad through exploring a variety of scenarios and role-playing. Additionally,
                                          faculty will develop a risk management plan. This course is designed for full-time
                                          faculty interested in leading a study abroad program.<br>
                                          Note: This is a required course in the Study Abroad Program Leader Certification.
                                          Participants must successfully complete the following prerequisite prior to enrolling
                                          in this course: Introduction to SAGE (LCTS 3316). 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="studyabroadplcert.html#">LCTS3316 Designing a Study Abroad Proposal <span>10 Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Participants will create authentic learning experiences for studying abroad. Additionally,
                                    participants will develop a curriculum plan, destination logistics, budget, and recruitment
                                    strategies.<br>
                                    Note: This is a required course in the Study Abroad Program Leader Certification.
                                    Participants must successfully complete the following prerequisites prior to enrolling
                                    in this course: Introduction to SAGE (LCTS 3315) and Stewardship as a Study Abroad
                                    Leader (INDV 3356). This course is designed for full-time faculty interested in leading
                                    a study abroad program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>              
                        
                        
                        
                        
                        
                        
                        <h3>For More Information on Study Abroad and Global Experiences</h3>
                        
                        <p>Please contact Robyn Brighton, Director of Study Abroad and Global Experiences, or
                           visit the <a href="../../../international/studyabroad/index.html" target="_blank">SAGE webpage</a>. 
                        </p>            
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/programs/studyabroadplcert.pcf">©</a>
      </div>
   </body>
</html>