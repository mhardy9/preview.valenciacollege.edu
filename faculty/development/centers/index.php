<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/centers/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li>Centers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Centers for Teaching/Learning Innovation</h2>
                        
                        <h3><strong><em>A place where faculty learning, collaboration, innovation and scholarship contribute
                                 to the greater success of our students, faculty and our community.</em></strong></h3>
                        
                        <p>Campus Centers for Teaching/Learning Innovation enhance our commitment to supporting
                           all faculty members in improving teaching excellence and student learning. The intention
                           of each Campus Center for Teaching/Learning Innovation is to create a hub for collaboration
                           and intellectual inquiry that serves as a catalyst for the expansion of campus-based
                           communities of practice and the advancement of teaching, learning, scholarship and
                           technology.
                        </p>
                        
                        <p><u>A variety of services are offered in support of teaching and learning on each campus</u>:
                        </p>
                        
                        <ul>
                           
                           <li><strong>Consultation Services: </strong>Partner with a faculty developer/instructional designer to create and practice delivery
                              of course content and materials for all instructional modalities
                           </li>
                           
                           <li><strong>Programs and Events: </strong>Plan your professional development path through courses, certifications, programs
                              and topic concentrations
                           </li>
                           
                           <li><strong>Teaching Tools:</strong> Discover tools to expand your professional practice and development in the Essential
                              Competencies of a Valencia Educator such as formative and summative assessment strategies
                           </li>
                           
                           <li><strong>Digital Engagement: </strong>Integrate educational technology in your classroom to enhance student engagement and
                              learning
                           </li>
                           
                           <li><strong>Essential Competency Library: </strong>Check out books and materials from the Professional Collection of educator resources
                           </li>
                           
                           <li><strong>Connect:</strong> Exchange ideas with fellow educators to advance teaching, learning, scholarship and
                              technology
                           </li>
                           
                           <li><strong>Resources: </strong>Utilize resources including computers, printers, ipads, and video equipment
                           </li>
                           
                        </ul>
                        
                        <p><strong>Active Learning Resources</strong></p>
                        
                        <ul>
                           
                           <li>A collection of<a href="/faculty/development/centers/activelearning.html"> Active Learning activities and strategies</a> you can use for your classes
                           </li>
                           
                        </ul>
                        
                        <h3><a href="/faculty/development/centers/locations.php">List of Center Locations</a></h3>
                        
                        <p><a href="/faculty/development/centers/index.html#top">TOP</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div><a title="Faculty Development Catalog" href="/faculty/development/documents/faculty-development-catalog-2017-18.pdf" target="_blank"><img src="/faculty/development/centers/faculty-development-catalog.jpg" alt="Faculty Development Catalog" width="245" height="210" border="0"></a></div>
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img src="/faculty/development/centers/icon_facebook_24.png" alt="TeachValencia Facebook"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://twitter.com/teachvalencia" target="_blank"><img src="/faculty/development/centers/icon_twitter_24.png" alt="TeachValencia Twitter"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img src="/faculty/development/centers/icon_youtube_24.png" alt="TeachValencia YouTube"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img src="/faculty/development/centers/icon_circles_24.png" alt="Circles Of Innovation"></a></div>
                        
                        <p><a title="How to Register" href="/faculty/development/howToRegisterForCourses.html"><img src="/faculty/development/centers/button-how-to-register.png" alt="How to Registers" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Certification Planning Documents" href="/faculty/development/CertificationPlanningDocuments.html"><img src="/faculty/development/centers/button-certification-docs.png" alt="Certification Planning Documents" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Have Questions? Contact a CTLI Member" href="/faculty/development/centers/locations.html"><img src="/faculty/development/centers/button-contact-ctli.png" alt="Have Questions? Contact a CTLI Member" width="245" height="60" border="0"></a></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/centers/index.pcf">©</a>
      </div>
   </body>
</html>