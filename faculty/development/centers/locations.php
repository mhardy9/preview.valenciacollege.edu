<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/centers/locations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/centers/">Centers</a></li>
               <li>Faculty Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h3>Centers for Teaching / Learning Innovation Locations</h3>
                        
                        <ul>
                           
                           <li><a title="East Campus" href="/faculty/development/centers/locations.html">East</a></li>
                           
                           <li><a title="Lake Nona CampusS" href="/faculty/development/centers/locations.html">Lake Nona </a></li>
                           
                           <li><a title="Osceola Campus" href="/faculty/development/centers/locations.html">Osceola<br> </a></li>
                           
                           <li><a title="West Campus" href="/faculty/development/centers/locations.html">West </a></li>
                           
                           <li><a title="Winter Park Campus" href="/faculty/development/centers/locations.html">Winter Park </a></li>
                           
                           <li><a title="Poinciana Campus" href="/faculty/development/centers/locations.html">Poinciana<br> </a></li>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <h2>East Campus</h2>
                                          
                                          <h3>Bldg 4, Room 133<br> Phone: (407) 582-2425
                                          </h3>
                                          
                                          <p><strong>Hours:</strong><br> Monday- Thursday 8am-6:00pm<br> Friday -8am-5:00pm
                                          </p>
                                          
                                          <p><strong>Summer Hours:</strong><br> Monday- Thursday 8am-5:00pm<br> Friday -8am-12:00pm
                                          </p>
                                          
                                          <p>&nbsp;</p>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <p><strong>Stephanie Spong</strong>, Campus Director, Faculty and Instructional Development
                                          </p>
                                          
                                          <p><a href="mailto:spong@valenciacollege.edu">spong@valenciacollege.edu</a></p>
                                          
                                          <p>(407) 582-2297</p>
                                          
                                          <p><strong>Ashley Cabrera</strong>, Faculty and Instructional Support Specialist<br> <a href="mailto:acabrera43@valenciacollege.edu">acabrera43@valenciacollege.edu</a><br> (407) 582-2519
                                          </p>
                                          
                                          <p><strong>Migdalia Otero-Olan</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:moteroolan@valenciacollege.edu">moteroolan@valenciacollege.edu</a> <br> (407) 582-2472
                                          </p>
                                          
                                          <p><strong>Celicia M. Wallace</strong>, Faculty Developer / Instructional Designer <br> <a href="mailto:cwallace28@valenciacollege.edu">cwallace28@valenciacollege.edu</a><br> (407) 582-2185
                                          </p>
                                          
                                          <p><strong>Amanda Molina</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:amolina12@valenciacollege.edu">amolina12@valenciacollege.edu</a><br> (407) 582-2400
                                          </p>
                                          
                                          <p><strong>Jennifer Calderon</strong>, Staff Assistant II<br> <a href="mailto:jcalderon38@valenciacollege.edu">jcalderon38@valenciacollege.edu</a><br> 407-582-2912
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <h2>Lake Nona Campus</h2>
                                          
                                          <h3>Bldg 1, Room 344 <br> Phone: (407) 582-7128
                                          </h3>
                                          
                                          <p><strong>Hours:<br> </strong>Monday - Thursday: 9:00 am - 6:00pm
                                          </p>
                                          
                                          <p>Virtual Support Available at (407) 582-7130 and by Appointment </p>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <p><strong>Shara Lee</strong>, Campus Director, Faculty and Instructional Development<br> <a href="mailto:moteroolan@valenciacollege.edu"> slee84@valenciacollege.edu</a> <br> (407) 582-4723
                                          </p>
                                          
                                          <p><strong>Lauren Kelley</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:lkelley8@valenciacollege.edu">lkelley8@valenciacollege.edu</a><br> (407) 582-7130
                                          </p>
                                          
                                          <p><strong>Dominique Shimizu</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:dshimizu@valenciacollege.edu">dshimizu@valenciacollege.edu</a><br> (407) 582-6131
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <h2>Osceola Campus</h2>
                                          
                                          <h3>Bldg 4, Room 124 <br> Phone: (407) 582-4918
                                          </h3>
                                          
                                          <p><strong>Hours:</strong><br> Monday - Thursday: 8:00am - 6:00pm<br> Friday: 8:00am - 5:00pm
                                          </p>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <p><strong>Shara Lee</strong>, Campus Director, Faculty and Instructional Development<br> <a href="mailto:moteroolan@valenciacollege.edu"> slee84@valenciacollege.edu</a> <br> (407) 582-4723
                                          </p>
                                          
                                          <p><strong>Gary Kokaisel</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:gkokaisel@valenciacollege.edu"> gkokaisel@valenciacollege.edu</a> <br> (407) 582-4812
                                          </p>
                                          
                                          <p><strong>Kevin Colwell</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:gkokaisel@valenciacollege.edu"> kcolwell1@valenciacollege.edu</a> <br> (407) 582-4214
                                          </p>
                                          
                                          <p><strong>Lauren Kelley</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:lkelley8@valenciacollege.edu">lkelley8@valenciacollege.edu</a><br> (407) 582-7130
                                          </p>
                                          
                                          <p><strong>Erica Diaz-Ramos</strong>, Faculty &amp; Instructional Support Specialist<br> <a href="mailto:ediaz88@valenciacollege.edu">ediaz88@valenciacollege.edu</a><br> (407) 582-4383
                                          </p>
                                          
                                          <p><strong>Dominique Shimizu, Faculty Developer / Instructional Designer </strong><br> <a href="mailto:dshimizu@valenciacollege.edu">dshimizu@valenciacollege.edu</a><br> (407) 582-6131
                                          </p>
                                          
                                          <p><strong>Denisha Rodriguez-Pflucker</strong>, Administative Assistant<br> <a href="mailto:drodriguez170@valenciacollege.edu">drodriguez170@valenciacollege.edu</a><br> 321-682-4724
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <h2>West Campus</h2>
                                          
                                          <h3><strong>Bldg 6 Room 327<br> Phone: (407) 582-5826</strong></h3>
                                          
                                          <p><strong>Hours:</strong> <br> Monday - Thursday: 8:00am - 6:00pm<br> Friday- 8:00am-5:00pm
                                          </p>
                                          
                                          <p><strong>Commons and Private Work Space Areas</strong><br> Monday-Thursday 8:00am-10:00pm <br> (access after 5pm is by access card only) <br> Friday- 8:00am-5:00pm
                                          </p>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <p><strong>Dori Haggerty</strong>, Campus Director, Faculty and Instructional Development<br> <a href="mailto:dhaggerty2@valenciacollege.edu">dhaggerty2@valenciacollege.edu</a><br> (407) 582-1051
                                          </p>
                                          
                                          <p><strong>Hunter Looney</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:glooney1@valenciacollege.edu">glooney1@valenciacollege.edu</a><br> (407) 582-5826
                                          </p>
                                          
                                          <p><strong>Claire Yates</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:llucia1@valenciacollege.edu"> cyates6@valenciacollege.edu</a> <br> (407) 582-1613
                                          </p>
                                          
                                          <p><strong>Greg Eugene</strong>, Faculty &amp; Instructional Support Specialist<br> <a href="mailto:geugene4@valenciacollege.edu">geugene4@valenciacollege.edu</a> <br> (407) 582-5826
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <h2><strong>Winter Park Campus </strong></h2>
                                          
                                          <h3>Room 126<br> Phone: (407) 582-6919<br>Administative Assistant
                                          </h3>
                                          
                                          <p><strong>Hours:</strong><br> Monday - Thursday: 9:00am - 6:00pm
                                          </p>
                                          
                                          <p>Virtual Support Available at (407) 582-6919 and by Appointment</p>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <p><strong>Stephanie Spong</strong>, Campus Director, Faculty and Instructional Development <a href="mailto:sspong@valenciacollege.edu"><br> sspong@valenciacollege.edu</a><br> (407) 582-2297
                                          </p>
                                          
                                          <p><strong>Aaron Bergeson</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:abergeson1@valenciacollege.edu">abergeson1@valenciacollege.edu</a><br> (407) 582-6919
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <h2>Poinciana Campus</h2>
                                          
                                          <h3>Room 305 <br> Phone: (407) 582-6131
                                          </h3>
                                          
                                          <p><strong>Hours:</strong><br> Monday - Thursday: 9:00 am - 6:00pm
                                          </p>
                                          
                                          <p>Virtual Support Available at (407) 582-7132 and by Appointment </p>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <p><strong>Shara Lee</strong>, Campus Director, Faculty and Instructional Development<br> <a href="mailto:moteroolan@valenciacollege.edu"> slee84@valenciacollege.edu</a> <br> (407) 582-4723
                                          </p>
                                          
                                          <p><strong>Dominique Shimizu</strong>, Faculty Developer / Instructional Designer<br> <a href="mailto:dshimizu@valenciacollege.edu">dshimizu@valenciacollege.edu</a><br> (407) 582-6131
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><a href="/faculty/development/centers/locations.html#top">TOP</a></p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/centers/locations.pcf">©</a>
      </div>
   </body>
</html>