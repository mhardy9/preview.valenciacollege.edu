<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/centers/activelearning.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/centers/">Centers</a></li>
               <li>Faculty Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Centers for Teaching/Learning Innovation</h2>
                        
                        <h3><strong><em>A place where faculty learning, collaboration, innovation and scholarship contribute
                                 to the greater success of our students, faculty and our community.</em></strong></h3>
                        
                        <p>Many of our instructors come to our centers and ask "how can I add some excitement
                           to my class"? Our response...<strong>Active Learning</strong>!  The concept behind active learning is that it involves direct engagement by students
                           in activities designed to make them think, develop meaningful skills and enhance their
                           enjoyment of the classes they attend.
                        </p>
                        
                        <p>This web page is dedicated to providing you with ideas you can use in your classes
                           to make them more interactive both face-to-face and online!
                        </p>
                        
                        <h2><img alt="Header" height="53" src="Active-Learning-Cork-Board-Header_000.jpg" width="500"></h2>
                        
                        <h2>
                           <br>
                           The JigSaw Strategy 
                        </h2>
                        
                        <p>This activity is ideal for developing team work and helping students understand how
                           each individual plays an important part in the accomplishment of a common goal. The
                           Jigsaw technique is one type of cooperative learning structure. 
                        </p>
                        
                        <p>A Jigsaw activity involves breaking a class up into groups and giving each group a
                           unique section of an assignment or project. Each group member has the responsibility
                           of becoming a subject matter expert on the subject  the group was assigned. These
                           members are then assigned to other groups to act as experts and share the information
                           they learned. At the end of the activity, all of the groups present their conclusions
                           to the class as a whole.
                        </p>
                        
                        <h2><em><strong>Learning more about it...</strong></em></h2>
                        
                        <p><a href="http://www.schreyerinstitute.psu.edu/pdf/alex/jigsaw.pdf">http://www.schreyerinstitute.psu.edu/pdf/alex/jigsaw.pdf</a></p>
                        
                        <p><a href="http://serc.carleton.edu/introgeo/jigsaws/steps.html">http://serc.carleton.edu/introgeo/jigsaws/steps.html</a></p>
                        
                        <p><a href="https://utah.instructure.com/courses/148446/pages/jigsaw">https://utah.instructure.com/courses/148446/pages/jigsaw</a></p>
                        
                        
                        <hr>
                        
                        <h2><strong>Think-Pair-Share</strong></h2>
                        
                        <p>A Think-Pair-Share activity consists of an activity that requires students to think
                           individually, work together in pairs and then present their findings to the class.
                           It usually only takes a short time to prepare and takes a small amount of class time.
                           It's ideal for motivating students about your class and also encourages students who
                           might not participate often.
                        </p>
                        
                        <h2><em><strong>Learning more about it...</strong></em></h2>
                        
                        <p><a href="http://serc.carleton.edu/introgeo/interactive/tpshare.html">http://serc.carleton.edu/introgeo/interactive/tpshare.html</a></p>
                        
                        <p><a href="http://www.wired.com/2014/05/empzeal-active-learning/">http://www.wired.com/2014/05/empzeal-active-learning/</a></p>
                        
                        <p><a href="https://utah.instructure.com/courses/148446/pages/active-learning">https://utah.instructure.com/courses/148446/pages/active-learning</a></p>
                        
                        
                        <hr>                  
                        <h2>Fish Bowl</h2>
                        
                        <p>The Fish bowl activity provides a way for students to practice under peer review and
                           also as part of an audience. A group of students are given a topic and the rest of
                           the class watches, listens,or reads a transcript of the discussion. Another discussion
                           occurs  concerning the outcomes and process of the first.
                        </p>
                        
                        <h2><em>Learning more about it...</em></h2>
                        
                        <p><a href="http://www.scienceteacherprogram.org/genscience/Chien04.html">http://www.scienceteacherprogram.org/genscience/Chien04.html</a></p>
                        
                        <p><a href="http://www.edchange.org/multicultural/activities/fishbowl.html">http://www.edchange.org/multicultural/activities/fishbowl.html</a></p>
                        
                        
                        <hr>                  
                        <h2>Muddiest Point-using Muddy Cards</h2>
                        
                        <p>This is a CAT (Classroom Assessment Technique) activity  that helps students reflect
                           on information that's been presented to them. It allows them to ask questions about
                           a specific point or area of a lesson or presentation that  they may not have understood.
                           It gives the instructor valuable feedback on what the class comprehended or if there
                           were areas of their presentation that could be improved upon. It's simple to do! It
                           basically asks the students to identify what was the most confusing or unclear point
                           about the lesson simply by jotting them down on an index card. The cards are then
                           turned in, and the instructor clarifies the information.
                        </p>
                        
                        <h2><em>Learning more about it...</em></h2>
                        
                        <p><a href="http://www.cdio.org/files/mudcards.pdf">www.cdio.org/files/mudcards.pdf</a></p>
                        
                        
                        <hr>                  
                        <h2>CATS (Classroom Assessment Techniques)</h2>
                        
                        <p>These are a series of techniques used to help determine what students are learning
                           and how well they are learning it. Some interesting techniques include... Memory Matrix,
                           Word Journal, Concept Maps. Check out some others by clicking on the resources below.
                        </p>
                        
                        <h2><em>Learning more about it...</em></h2>
                        
                        <p><a href="documents/ClassroomAssessmentTechniquesPrimerandWebsite.pdf">Classroom Assessment Techniques Primer and Website</a></p>
                        
                        <p><a href="documents/ClassroomAssessmentTechniques-Listof50.pdf">Classroom Assessment Techniques- A list of 50</a></p>
                        
                        
                        <hr>                  
                        
                        <h2><strong>Community Building Activities </strong></h2>
                        
                        <p>Building a strong classroom community starts on the first day of class and continues
                           throughout the whole semester as you build relationships with your students. Below
                           are links to a community building infographic and the learning acitivites Find Someone
                           Who, Can You Describe This?, and Visual Telephone. 
                        </p>
                        
                        <h2><em>Learning more about it...</em></h2>
                        
                        <p><a href="http://valenciacollege.edu/faculty/development/centers/CommunityBuilding.jpeg">Community Building Infographic</a></p>
                        
                        <p><a href="http://valenciacollege.edu/faculty/development/centers/documents/FindSomeoneWho.docx">Find Someone Who...</a></p>
                        
                        <p><a href="http://valenciacollege.edu/faculty/development/centers/documents/CanYouDescribeThis.docx">Can You Describe This?</a></p>
                        
                        <p><a href="http://valenciacollege.edu/faculty/development/centers/documents/VisualTelephone.docx">Visual Telephone</a></p>
                        
                        
                        
                        <p><a href="activelearning.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/centers/activelearning.pcf">©</a>
      </div>
   </body>
</html>