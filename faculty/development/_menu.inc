<ul>
    <li><a href="/faculty/development/">Faculty Development</a></li>
    <li class="submenu"><a class="show-submenu" href="/faculty/development/programs/"> Certifications <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
            <li><a href="/faculty/development/programs/activeLearningCertificate.php">Active Learning Certification</a></li>
            <li><a href="/faculty/development/programs/adjunct/">Associate Faculty Certification</a></li>
            <li><a href="/faculty/development/programs/destination/">Destination Program</a></li>
            <li><a href="/faculty/development/programs/DigitalProfessorCertification.php">Digital Professor Certification</a></li>
            <li><a href="/faculty/development/programs/LifeMapCertificatepage.php">LifeMap Certification</a></li>
            <li><a href="/faculty/development/programs/seedProgram.php">SEED Program</a></li>
            <li><a href="/faculty/development/programs/SeneffCert.php">Seneff Honors Certification</a></li>
            <li><a href="/faculty/development/programs/studyabroadplcert.php">Study Abroad Leader Certification</a></li>
        </ul>
    </li>
    <li class="submenu"><a class="show-submenu" href="/faculty/development/concentrations/"> Concentrations <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
            <li><a href="/faculty/development/concentrations/actionResearch.php">Action Research</a></li>
            <li><a href="/faculty/development/concentrations/Blackboard.php">Blackboard Fundamentals</a></li>
            <li><a href="/faculty/development/concentrations/care.php">CARE</a></li>
            <li><a href="http://circlesofinnovation.valenciacollege.edu/">Circles of Innovation</a></li>
            <li><a href="/faculty/development/concentrations/criticalThinking.php">Critical Thinking</a></li>
            <li><a href="/faculty/development/concentrations/experiential/">Experiential Learning</a></li>
            <li><a href="/faculty/development/concentrations/exemplar/">Quality Matters</a></li>
            <li><a href="/faculty/development/concentrations/PeaceandJustice.php">Peace and Justice Practitioner</a></li>
            <li><a href="/faculty/development/concentrations/ProgLearnOutcomes.php">Program Learning Outcomes</a></li>
        </ul>
    </li>
    <li class="submenu"><a class="show-submenu" href="/faculty/development/tla/"> TLA <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
<!--             <li><h3>Teaching / Learning Academy</h3></li> -->
            <li><a href="/faculty/development/tla/tla_team.php">TLA Team</a></li>
            <li><a href="/faculty/development/tla/alumni.php">TLA Alumni</a></li>
            <li><a href="/faculty/development/tla/actionResearch/">Action Research</a></li>
            <li><a href="/faculty/development/tla/Candidate/">Candidate Resources</a></li>
            <li><a href="/faculty/development/tla/Candidate/dean_resources.php">Dean / Director Resources</a></li>
            <li><a href="/faculty/development/tla/Candidate/tla_competencies_LCF.php">Essential Competencies</a></li>
            <li><a href="/faculty/development/tla/ILP/">Panel Reviews</a></li>
            <li><a href="/faculty/development/tla/Seminar/">Seminar Series</a></li>
            <li><a href="/faculty/development/tla/Tenure/">Tenure Reviews</a></li>
            <li><a href="/faculty/development/coursesResources/">Resources for Essential Competencies</a></li>
        </ul>
    </li>
    <li><a class="show-submenu" href="/faculty/development/centers/"> Centers </a></li>
    <li class="submenu"><a class="show-submenu" href="/faculty/development/about/"> About <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
            <li><a href="/faculty/development/about/Awards.php">Awards &amp; Publications</a></li>
            <li><a href="/faculty/development/about/facultyfellows.php">Faculty Fellows</a></li>
            <li><a href="/faculty/development/about/team.php">Our Team</a></li>
            <li><a href="http://net4.valenciacollege.edu/forms/faculty/development/contact.cfm" target="_blank">Contact Us</a></li>
        </ul>
    </li>
</ul>