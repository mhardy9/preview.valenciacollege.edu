<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/surveys.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li>Faculty Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Qualtrics Survey Tool</h2>
                        
                        <p> After  an extensive search and review process in Fall 2009 by a committee of Flashlight
                           users,&nbsp;Valencia  decided  to not renew the contract with 
                           
                           
                           the survey tool Flashlight as they  migrate to their new product Flashlight 2.0 (Flashlight
                           1.0 will no longer be support by its host after March 31, 2010).&nbsp; 
                        </p>
                        
                        <p>We are happy to announce<strong> Qualtrics </strong>(<a href="http://www.qualtrics.com/" target="_blank">http://www.qualtrics.com/</a>) for your use. To register and create your Qualtrics account please follow the steps
                           below:
                        </p>
                        
                        <ol>
                           
                           <li>Log in to Atlas</li>
                           
                           <li>Click on the Faculty Tab </li>
                           
                           <li>Click on the <strong>Qualtrics Survey Tool</strong> link located in the Employee Support channel
                           </li>
                           
                        </ol>
                        
                        <h3>For Questions and Support </h3>
                        
                        <p><a href="http://www.qualtrics.com/university/" target="_blank">Qualtrics University</a>: A collection of webinar training sessions presented by Qualtrics staff. Includes
                           from getting started with Qualtrics to customize your surveys. In addition, a Training
                           Calendar is available with free weekly webinar offers that you can sign up to attend.
                           These resources are also found at the top of the screen in the "Help and Tutorials"
                           tab within the survey tool. 
                        </p>
                        
                        <p>You can also contact Kurt Ewen at <a href="mailto:kewen@valenciacollege.edu">kewen@valenciacollege.edu</a><br>
                           
                        </p>
                        
                        <p><a href="surveys.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/surveys.pcf">©</a>
      </div>
   </body>
</html>