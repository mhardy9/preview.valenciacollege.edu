<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tla_team.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>TLA Support Team</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><img alt="Celine" height="179" src="images/celine.png" width="128"> 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="mailto:ckavalec@valenciacollege.edu">Celine Kavalec<br>
                                             </a>Director<br>
                                          407-582-2345<br>
                                          ckavalec@valenciacollege.edu
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><img alt="Shari Koopmann" height="179" src="images/clip_image003.jpg" width="128"></p>
                                    </div>
                                    
                                    <div>
                                       <p><a href="mailto:skoopman@valenciacollege.edu">Shari Koopmann</a><br>
                                          Activity Coordinator; East Campus Coordinator<br>
                                          407-582-2945<br>
                                          skoopman@valenciacollege.edu 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p><img alt="Stanton Reed" height="171" src="images/stantonweb.jpg" width="128"></p>
                                    </div>
                                    
                                    <div>
                                       <p><a href="mailto:sreed21@valenciacollege.edu">Stanton Reed </a><br>
                                          Osceola &amp; West <br>
                                          Campus Facilitator<br>
                                          407-582-4224<br>
                                          sreed21@valenciacollege.edu 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><img alt="Marcelle Cohen" height="179" src="images/Marcellesmall.jpg" width="128"></p>
                                    </div>
                                    
                                    <div>
                                       <p><a href="mailto:mwycha@valenciacollege.edu">Marcelle Wycha<br>
                                             </a>East &amp; Lake Nona<br>
                                          Campus Facilitator<br>
                                          407-582-2362<br>
                                          mwycha@valenciacollege.edu
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p><img alt="Kourtney Baldwin" height="179" src="images/clip_image001.jpg" width="128"></p>
                                    </div>
                                    
                                    <div>
                                       <p><a href="mailto:kbaldwin@valenciacollege.edu"> Kourtney Baldwin<br>
                                             </a>West Campus Facilitator<br>
                                          407-582-5619<br>
                                          kbaldwin@valenciacollege.edu
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><img alt="Julia Nudel" height="179" src="images/DonnaColwell_sm.jpg" width="128"></p>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="mailto:dcolwell@valenciacollege.edu">Donna Colwell <br>
                                             </a>West Campus Facilitator<br>
                                          407-582-1314<br>
                                          dcolwell@valenciacollege.edu 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <p><img alt="AMM" height="179" src="images/annie-marion15.jpg" width="128"></p>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="mailto:amarion2@valenciacollege.edu">Annie Marion </a><br>
                                          Coordinator<br>
                                          407-582-2788<br>
                                          amarion2@valenciacollege.edu
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><img alt="Shawn Pollgreen" height="179" src="images/ShawnPollgreen03.jpg" width="128"></div>
                                    
                                    <div>
                                       <div>
                                          <a href="mailto:spollgre@valenciacollege.edu">Shawn Pollgreen<br>
                                             </a>Osceola Campus Coordinator <br>
                                          407-582-4292<br>
                                          spollgre@valenciacollege.edu
                                       </div>
                                    </div>
                                    
                                    <div><img alt="Stella Ross" height="179" src="images/stella-ross.png" width="128"></div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><a href="mailto:sross40@valenciacollege.edu">Stella Ross</a><br> TLA Staff Assistant<br> 407-582-2442<br> sross40@valenciacollege.edu
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>  <img alt="TLA Team Picture" height="295" src="images/TLA-Team-pic.jpg" width="450">
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tla_team.pcf">©</a>
      </div>
   </body>
</html>