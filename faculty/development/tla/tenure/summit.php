<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/summit.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/tenure/">Tenure</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tenure Summit Minutes</h2>
                        
                        <p> The reform of the tenure process is the product of Valencia's   shared governance
                           process.&nbsp; All matters concerning tenure are endorsed by the   Faculty Association
                           and approved by the College Learning Council.&nbsp; The   development of the tenure process<strong> </strong>relied on highly collaborative 'Tenure   Summits' involving all the stakeholders.
                           These summit meetings are a dynamic,   open collaborative decision-making forums,
                           where difficult issues are resolved   expediently.&nbsp; 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong><font color="white">Summit Term </font></strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong><font color="white">Description</font></strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><span><a href="Summit_fall2003.html">Fall 2003</a></span></div>
                                 
                                 <div>Tenure Summit Summary, Submitted by Dr. Lisa Armour and Dr. Tracy Edwards, Co-chairs
                                    of the College Learning Council
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong><a href="Summit_fall2004.html">Fall 2004</a></strong></div>
                                 
                                 <div>
                                    
                                    <p>Tenure Summit Summary, Submitted by Professor Michael Shugg and Dr. Tracy Edwards,
                                       Co-chairs of the College Learning Council
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong><a href="Summit_summer2005.html">Summer 2005</a> </strong></div>
                                 
                                 <div>
                                    
                                    <p>Tenure Summit Summary, Submitted by Professor Amy Bosley and Dr. Louise Pitts, Co-chairs
                                       of the College Learning Council
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong><a href="Summit_summer2007.html">Summer 2007</a></strong></div>
                                 
                                 <div>Tenure Summit June 11, 2007, Sumitted by Helen Clarke and College Learning Council
                                    Co-Chairs: Dr. Kaye Walter and Ms. Rose Watson. Faculty Council: President Rose Watson,
                                    Vice-President Michael Shugg and President Ex-officio Tom Byrnes. 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong><a href="Summit_fall2010.html">Fall 2010</a></strong></div>
                                 
                                 <div>Tenure Summit Minutes November 30, 2010 (CJI): Submitted by:&nbsp; Faculty Council, President
                                    Lisa Macon Vice-President Bob Gessner; College Learning Council Co-chair Jean-Marie
                                    Fuhrman; and Celine Kavalec-Miller 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong><a href="Summit_summer2013.html">Summer 2013</a></strong></div>
                                 
                                 <div>Tenure Summit MinutesMay 30, 2013 (WC Special Events Center)</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                        
                        <p><strong></strong> 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/summit.pcf">©</a>
      </div>
   </body>
</html>