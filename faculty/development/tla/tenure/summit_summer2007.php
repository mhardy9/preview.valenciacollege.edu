<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/summit_summer2007.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/tenure/">Tenure</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tenure Summit Minutes Summer 2007 </h2>
                        
                        <p> The reform of the tenure process is the product of Valencia's   shared governance
                           process.&nbsp; All matters concerning tenure are endorsed by the   Faculty Association
                           and approved by the College Learning Council.&nbsp; The   development of the tenure process<strong> </strong>relied on highly collaborative 'Tenure   Summits' involving all the stakeholders.
                           These summit meetings are a dynamic,   open collaborative decision-making forums,
                           where difficult issues are resolved   expediently.&nbsp; 
                        </p>
                        
                        <h3>&nbsp; </h3>
                        
                        <h3><strong>Tenure     Summit June 11, 2007, Sumitted by Helen Clarke and College Learning Council
                              Co-Chairs: Dr. Kaye Walter and Ms. Rose Watson. Faculty     Council: President Rose
                              Watson, Vice-President Michael Shugg and President Ex-officio     Tom Byrnes. </strong></h3>
                        
                        <p><strong><em>Attendees: </em></strong><em>Ruth Prather, Helen Clarke,     Mildred Franceschi, Aida Diaz, Dennis Weeks, Joyce
                              Romano, Kim Long, Joe Lynn Look, Michele McArdle, Susan Craig, Joe Livingston,   
                              Shaw Robinson, Martha Williams, Michelle Foster, Jennifer Page, Linda Swaine,    
                              Della Paul, Cheryl Robinson, Chris Klinger, Karen Blondeau, Stan Stone, Jared    
                              Graber, Nasser Hedayat, Tom Byrnes, Rose Watson, Kaye Walter, Joseph Bivins,     Bill
                              Mullowney, Dan Dutkofski, Michael Shugg, Maryke Lee, Lisa Armour, Dale     Husbands</em></p>
                        
                        <p><strong>I.     POLICY AND PROCEDURE NUMBER: 6HX28:08-10 AWARD OF TENURE AND EVALUATION OF
                              TENURED     AND TENURE-TRACK FACULTY </strong></p>
                        
                        <p><strong>4.b.     Professional Performance: Student Assessment of Instruction </strong></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Party           Responsible </strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Document </strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Action </strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Faculty           Council </p>
                                 </div>
                                 
                                 <div>
                                    <p>Student           Assessment of Instruction Explanation </p>
                                 </div>
                                 
                                 <div>
                                    <p>Explanation           to students on new procedure concerning the Student Assessment
                                       of Instruction           as articulated in Professional Performance 4b 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Deans </p>
                                 </div>
                                 
                                 <div>
                                    <p>Student           Assessment of Instruction w/Explanation </p>
                                 </div>
                                 
                                 <div>
                                    <p>Distribute           to classes taught by tenure-track faculty </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>II.     PROCESSES FOR EFFECTIVELY IMPLEMENTING THE TENURE PROCESS REPORTING SYSTEM</strong></p>
                        
                        <p>A.     Tenure Review Committee (TRC) Role: A n     advisory committee composed of
                           two elected, tenured faculty members working     with the dean on tenure recommendations,
                           providing comments to the record.
                        </p>
                        
                        <p>B.     Guiding principles: Collaboration, inclusiveness, and transparency.</p>
                        
                        <p>•&nbsp; TRC     Issues Decided:</p>
                        
                        <p>1.     Performance-related documents 'to the record' that are considered by one person
                           must be considered by all members of the TRC. That is, dean and faculty committee
                           members should consider all information relevant to tenure process. Information  
                           used to make tenure decisions must be in writing. 
                        </p>
                        
                        <p>2.     People who participate on TRCs must follow the procedures as described in the
                           Handbook for Tenure Committee Members.
                        </p>
                        
                        <p>3.     Tenure Review Committee members will sign an Ethics Statement and complete
                           the     TRC training before they serve on a committee. Training should clearly emphasize
                           the TRC members realize the sensitivity and confidentiality of information.
                        </p>
                        
                        <p>4.     Deans should inform the candidates when their final portfolio is approved.</p>
                        
                        <p>&nbsp;5.     Candidates should be informed as soon as possible in cases where tenure is
                           not     recommended, either because of an unacceptable portfolio, Tenure Review Committee
                           decision, or provost/VP decision.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Party           Responsible </strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Document </strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Action </strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Office           of the CLO (TLA) </p>
                                 </div>
                                 
                                 <div>
                                    <p>TRC           Training </p>
                                 </div>
                                 
                                 <div>
                                    <p>Revise:           further emphasis on confidentiality, </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Office           of the CLO (TLA) </p>
                                 </div>
                                 
                                 <div>
                                    <p>TRC           Pre-nomination explanation of main responsibility </p>
                                 </div>
                                 
                                 <div>
                                    <p>Create           document for distribution to tenured faculty members interested in
                                       serving           on TRCs 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>General           Counsel </p>
                                 </div>
                                 
                                 <div>
                                    <p>Ethics           Statement </p>
                                 </div>
                                 
                                 <div>
                                    <p>Ethical           responsibility of TRC service to include, but not limited to, confidentiality,
                                       indemnity, and training obligation. 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Office           of the CLO (TLA) </p>
                                 </div>
                                 
                                 <div>
                                    <p>Tenure           Recommendation Form </p>
                                 </div>
                                 
                                 <div>
                                    <p>Change           'Student Evaluation' to 'Student Assessment of Instruction' </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Office           of the CLO (TLA) </p>
                                 </div>
                                 
                                 <div>
                                    <p>Tenure           Recommendation Form </p>
                                 </div>
                                 
                                 <div>
                                    <p>Add           a 'Comments' section for each TRC faculty member </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Office           of the CLO (TLA) </p>
                                 </div>
                                 
                                 <div>
                                    <p>Tenure           Review Report Schedule </p>
                                 </div>
                                 
                                 <div>
                                    <p>Add           2 week time table for TRC elections &amp; written feedback from colleagues
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <ul>
                           
                           <li>
                              
                              <p> The     College takes legal responsibility for TRC members. TRC members, who blatantly
                                 disregard TRC confidentiality, may lose indemnity. 
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong> III.     OTHER CONCERNS </strong></p>
                        
                        <p>&nbsp;&nbsp;A.     Appropriate     orientation and training for deans, other supervisors and
                           members of Tenure     Review Committees to enhance understanding of the legal environment
                           as well     as participation and procedure related to tenure review and recommendation.
                           In this context, specific workshops may be scheduled as part of next year's     Legal
                           Issues Conference or provided through the Office of the General Counsel. 
                        </p>
                        
                        <p><strong>IV.&nbsp; FURTHER   NOTE OF CLARIFICATION:</strong></p>
                        
                        <p><strong>ILP APPEALS:</strong> should an ILP Review Panel disapprove of a candidate's work on his or her ILP,  
                           the Faculty Association has established an appeals process.&nbsp;&nbsp; The   relevant passage
                           reads as follows:
                        </p>
                        
                        <p><em>Completion of an   'acceptable' portfolio is a necessary prerequisite for tenure.&nbsp;
                              Therefore, an   unacceptable portfolio disqualifies the candidate from further consideration
                              for   tenure.&nbsp; A candidate whose final portfolio is considered 'unacceptable' by his
                              or her ILP Review Panel may appeal to the Tenure Review Committee, which would   refer
                              the matter to another ILP Review Panel for consideration.&nbsp; The candidate's   portfolio
                              would then be reviewed by the second ILP Review Panel.&nbsp; The second   panel would consist
                              of one dean and three tenured faculty members selected by   the Tenure Review Committee
                              from the pool of trained ILP reviewers.&nbsp; None of the   members of the candidate's
                              original ILP Review Panel would be eligible to serve   on the second panel.&nbsp; If the
                              second panel found the candidate's portfolio to be   'acceptable,' that prerequisite
                              for tenure would be considered met.&nbsp; The   candidate's official record would include
                              both the original ILP Review Panel's   report and the second panel's report</em>. 
                        </p>
                        
                        <p>&nbsp;Tenure   Review Committees therefore have an important role in this appeals process,
                           and   must reassign the review of an ILP under appeal to a new set of ILP reviewers.
                        </p>
                        
                        <p>Note on composition of   Tenure Review Committees, from Tenure Summit   Final Notes:</p>
                        
                        <p> <em>In cases where the described configuration   for the Tenure Review Committee is not
                              feasible, equivalent arrangements can be   made.&nbsp; For example, some departments do
                              not have two tenured faculty members, or   some departments have so many tenure candidates
                              that it would not be feasible   for two tenured faculty members to provide written
                              remarks for all of them.&nbsp; It   is suggested that the Chief Learning Officer, Provost,
                              Faculty Association   Board, or College Learning Council serve to approve equivalent
                              arrangements of   the Tenure Review Committee.</em></p>
                        
                        <p><strong>Note on definition of <em>holistic</em></strong>:&nbsp; Evaluation   of professional work in which the judgment is based on the overall
                           quality of   the artifact or performance rather than the individual elements of performance.
                        </p>
                        
                        <ul>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Previous Tenure Summit Meeting Minutes </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><span><a href="Summit_fall2003.html" target="_blank">Fall 2003</a></span></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_fall2004.html" target="_blank">Fall 2004</a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="Summit_summer2005.html" target="_blank">Summer 2005</a> 
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_fall2010.html">Fall 2010 </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2013.html">Summer 2013 </a></div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/summit_summer2007.pcf">©</a>
      </div>
   </body>
</html>