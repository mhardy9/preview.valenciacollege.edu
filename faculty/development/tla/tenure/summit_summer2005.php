<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/summit_summer2005.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/tenure/">Tenure</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tenure Summit Minutes Summer 2005</h2>
                        
                        <p> The reform of the tenure process is the product of Valencia's   shared governance
                           process.&nbsp; All matters concerning tenure are endorsed by the   Faculty Association
                           and approved by the College Learning Council.&nbsp; The   development of the tenure process<strong> </strong>relied on highly collaborative 'Tenure   Summits' involving all the stakeholders.
                           These summit meetings are a dynamic,   open collaborative decision-making forums,
                           where difficult issues are resolved   expediently.&nbsp; 
                        </p>
                        
                        <h3> <strong>Tenure Summit Summary, Submitted     by Professor Amy Bosley and Dr. Louise Pitts,
                              Co-chairs of the College   Learning Council, Summer Term B, 2005</strong>
                           
                        </h3>
                        
                        <ol>
                           
                           <li>A Secondary ILP Review Panel will consult all primary     sources, including the original
                              panel's final report. The intent is for the     Secondary ILP Review Panel to assess
                              the portfolio holistically taking into     consideration the original panel's final
                              report. 
                           </li>
                           
                           <li>In the case where a portfolio warrants the review of a     Secondary Review Panel,
                              the original ILP Review Panel should have access to     the Secondary Review Panel's
                              final report. 
                           </li>
                           
                           <li>One unified report should be sent to the tenure     candidate by the dean after the
                              Year-one and     Year-two ILP/Portfolio Review Panel meetings.&nbsp; The report should
                              reflect the consensus of the ILP Review Panel with the full balance of     findings,
                              including the strengths and areas needed for improvement.&nbsp; 
                           </li>
                           
                           <li>&nbsp;An Action Research Project (ARP) is encouraged to be     included in each ILP; however,
                              an ARP is not a required element.&nbsp; Each dean     and candidate will make the appropriate
                              needs assessment regarding the value     of an ARP to the candidate's ILP. 
                           </li>
                           
                           <li>Continued training for ILP Review Panelists is needed,     especially in the understanding
                              and assessment of Action Research Projects.&nbsp; 
                           </li>
                           
                           <li>ILP reviews will take place on a college campus. </li>
                           
                           <li>Currently, student's evaluation of instruction is     reflected in the annual performance
                              reviews prepared by the candidate's dean.&nbsp;     When the college has adopted an electronic
                              instrument, the logistics of how     the Tenure Review Committee accesses this information
                              will be decided at a     future Summit. 
                           </li>
                           
                        </ol>
                        
                        <ul>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Previous Tenure Summit Meeting Minutes </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><span><a href="Summit_fall2003.html" target="_blank">Fall 2003</a></span></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_fall2004.html" target="_blank">Fall 2004</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2007.html">Summer 2007 </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_fall2010.html">Fall 2010 </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2013.html">Summer 2013 </a></div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/summit_summer2005.pcf">©</a>
      </div>
   </body>
</html>