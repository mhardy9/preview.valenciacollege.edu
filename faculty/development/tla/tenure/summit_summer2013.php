<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/summit_summer2013.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/tenure/">Tenure</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tenure Summit Minutes Summer 2013 </h2>
                        
                        <p> The reform of the tenure process is the product of Valencia's   shared governance
                           process.&nbsp; All matters concerning tenure are endorsed by the   Faculty Association
                           and approved by the College Learning Council.&nbsp; The   development of the tenure process<strong> </strong>relied on highly collaborative 'Tenure   Summits' involving all the stakeholders.
                           These summit meetings are a dynamic,   open collaborative decision-making forums,
                           where difficult issues are resolved   expediently.&nbsp; 
                        </p>
                        
                        <h3> <strong>Tenure Summit Summary May 30, 2103 West Campus, Special Events Center </strong>
                           
                        </h3>
                        
                        <h3> <u>Minutes<u>&nbsp; </u></u>
                           
                        </h3>
                        
                        <p>Welcome by College-wide Faculty Association president Deidre Holmes DuBois at 10:12
                           a.m. 
                        </p>
                        
                        <p>Attending: President Sandy Shugart, General Counsel Bill Mullowney, Teaching/Learning
                           Academy Director and personnel, Interim Vice President of HR Amy Bosley, West Campus
                           President Falecia Williams, Osceola/Lake Nona Campus President Kathleen Plinske, college-wide
                           representation of deans and faculty (tenured and tenure-track), Faculty Council leadership,
                           AVP Kurt Ewen, members of the College Learning Council (approximately 82 attendees)
                        </p>
                        
                        
                        <p>Review of Agenda</p>
                        
                        <ul>
                           
                           <li>Background on redesign: Deidre Holmes DuBois reviewed the principles of the tenure
                              process design, including the current three-year plan with annual evaluations through
                              the Human Resources/Dean process.
                           </li>
                           
                           <ul>
                              
                              <li>Reviewed State of Florida passing Rule 6A and read the paragraph in the legislation
                                 stating faculty need to complete at least five years (to a maximum of seven years
                                 in the case of a leave of absence) continuously and at the same college in order to
                                 be considered for tenure.
                              </li>
                              
                              <li>Reviewed criteria used to evaluate those eligible for continuing contracts; </li>
                              
                              <li>Rule 6A also provides for post-tenure review using the same criteria. Post tenure
                                 reviews will be conducted for all tenured faculty. Deidre noted this will be detailed
                                 in a future discussion.
                              </li>
                              
                           </ul>
                           
                           <li>Process for Discussion &amp; Voting: Attendees will be asked to vote on the draft of the
                              plan presented at this meeting.
                           </li>
                           
                           <li>Work accomplished so far (this new system must be implemented by August 2013 for those
                              30 newly hired tenure track candidates): 
                           </li>
                           
                           <ul>
                              
                              <li>Two surveys were administered to faculty, deans and administrators, collecting feedback
                                 on the elements that should be included in the 5-year tenure plan. Feedback was used
                                 to revise the plan. General themes included the following:
                              </li>
                              
                              <ul>
                                 
                                 <li>More time allowed for orientation to college and ILP development</li>
                                 
                                 <li>Annual benchmarks added</li>
                                 
                                 <li>Improve training of deans, panelists and TLA facilitators to improve consistency of
                                    feedback
                                 </li>
                                 
                                 <li>More time for project implementation</li>
                                 
                                 <li>Mentors –The infrastructure required for a formal college-wide mentor program is not
                                    currently sustainable.&nbsp; Deans are encouraged to continue division and discipline level
                                    mentorship relationships.&nbsp; It was noted that TLA facilitators and panelists are currently
                                    ILP/portfolio mentors. 
                                 </li>
                                 
                              </ul>
                              
                              <li>A draft of the new 5-year plan has been created by Tenure Redesign Writing Team (faculty
                                 from the major campuses, TLA representatives, current TLA Director, and former TLA
                                 Director and faculty member Helen Clarke).&nbsp; This draft was then presented to campus
                                 and college presidents, general counsel, Faculty Council, and deans.&nbsp; It was also
                                 discussed with faculty at the three major campuses.
                              </li>
                              
                           </ul>
                           
                           <li>
                              <a href="documents/DraftChartof5YearTenureProcessPostSummit.pdf" target="_blank">Overview of the 5 year plan draft</a> 
                           </li>
                           
                           <ul>
                              
                              <li>Y1:&nbsp; Candidate acclimates to the college:&nbsp; Y1 competency seminars, meetings with facilitators,
                                 development of learning-centered philosophy (philosophy can and often does evolve
                                 over the course of the tenure induction process), needs assessment, and future development
                                 plan.
                              </li>
                              
                              <li>Y2:&nbsp; dean convenes panel; candidate writes ILP, submits draft ILP to dean and TLA
                                 in December, and submits revised ILP to panel in spring.
                              </li>
                              
                              <li>Y3: &nbsp;Candidate implements FLOs, writes draft portfolio, and submits to panel in spring.</li>
                              
                              <li>Y4: &nbsp;&nbsp;Candidate refines final portfolio; submits Advanced Practice Plan to dean in
                                 October and begins to implement; submits final portfolio in January. 
                              </li>
                              
                              <li>Y5: &nbsp;Candidate continues to implement Advanced Practice Plan. Tenure Review Committee
                                 meets in January.
                              </li>
                              
                              <li>Annual benchmark discussion</li>
                              
                              <ul>
                                 
                                 <li>Review <a href="http://valenciacollege.edu/faculty/development/tla/tenure/documents/DraftModel5YTenrueHRYear-1Benchmarks.docx" target="_blank">Y-1 benchmarks</a> 
                                 </li>
                                 
                                 <ul>
                                    
                                    <li>Yearly performance evaluation</li>
                                    
                                    <li>Classroom observations (if applicable)</li>
                                    
                                    <li>Student Assessment of Instruction (if applicable)</li>
                                    
                                    <li>Service to the College</li>
                                    
                                 </ul>
                                 
                                 <li>Review <a href="http://valenciacollege.edu/faculty/development/tla/tenure/documents/DraftModel5YTenureHRYear-2Benchmarks.docx" target="_blank">Y-2 benchmarks </a>&nbsp;Y2 – continue advanced seminars. Draft of the ILP goes out to candidate’s dean and
                                    the TLA team.
                                 </li>
                                 
                                 <ul>
                                    
                                    <li>Yearly performance evaluation</li>
                                    
                                    <li>Classroom observations (if applicable)</li>
                                    
                                    <li>Student Assessment of Instruction (if applicable)</li>
                                    
                                    <li>Service to the College</li>
                                    
                                    <li>Written feedback from campus and/or college community is collected; the dean will
                                       discuss feedback with candidate. The call for feedback in year 2 will help make the
                                       process more formative and give the candidate time to adjust his/her behavior. The
                                       current process has one summative call for feedback in the final year.
                                    </li>
                                    
                                 </ul>
                                 
                                 <li>Review <a href="http://valenciacollege.edu/faculty/development/tla/tenure/documents/DraftModel5YTenureHRYear-3Benchmarks.docx" target="_blank">Y-3 benchmarks</a> 
                                 </li>
                                 
                                 <ul>
                                    
                                    <li>Yearly performance evaluation</li>
                                    
                                    <li>Classroom observations (if applicable)</li>
                                    
                                    <li>Student Assessment of Instruction (if applicable)</li>
                                    
                                    <li>Service to the College</li>
                                    
                                 </ul>
                                 
                                 <li>Review <a href="http://valenciacollege.edu/faculty/development/tla/tenure/documents/DraftModel5YTenureHRYear-4Benchmarks.docx" target="_blank">Y-4 benchmarks</a> 
                                 </li>
                                 
                                 <ul>
                                    
                                    <li>Yearly performance evaluation</li>
                                    
                                    <li>Classroom observations (if applicable)</li>
                                    
                                 </ul>
                                 
                              </ul>
                              
                           </ul>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Previous Tenure Summit Meeting Minutes </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><span><a href="Summit_fall2003.html">Fall 2003</a></span></div>
                                 </div>
                                 
                                 <div>
                                    <div><span><a href="Summit_fall2004.html">Fall 2004 </a></span></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2005.html">Summer 2005</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2007.html">Summer 2007 </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_fall2010.html">Fall 2010 </a></div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/summit_summer2013.pcf">©</a>
      </div>
   </body>
</html>