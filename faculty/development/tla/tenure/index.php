<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tenure | Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li>Tenure</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        					
                        <h2>Tenure Reviews and Program Assessment </h2>
                        					
                        <h3>Information and Documents</h3>
                        					
                        <ul>
                           						
                           <li><a href="../ilp/TenureReview.php">Tenure Review Committee</a></li>
                           						
                           <li>
                              							<a href="../Candidate/documents/5-year-Reporting-Schedule-2017-18.pdf" target="_blank">ILP, Portfolio, &amp; Tenure Review Reporting Schedule for 2017/18 Academic Year, All
                                 Classes</a> 
                           </li>
                           						
                           <li>
                              							<a href="../Candidate/documents/CandidateReportingSchedule2013-2018.pdf" target="_blank">ILP, Portfolio, &amp; Tenure Review Reporting Schedule for Class of 2018 only (All five
                                 years) </a> 
                              						
                           </li>
                           						
                           <li>
                              							<a href="documents/TenureRecommendationForm_000.pdf" target="_blank">Tenure Recommendation Form
                                 							</a> 
                           </li>
                           						
                           <li>
                              							<strong>Valencia's Tenure Path:&nbsp;</strong><br>
                              						
                           </li>
                           						
                           <ul>
                              							
                              <li>
                                 								<a href="documents/13TLA002-tenure-review-path-proof5.pdf" target="_blank">Graphic</a> 
                              </li>
                              							
                              <li><a href="http://valenciacollege.edu/faculty/development/tla/documents/TenureProcessNarrative_000.pdf" target="_blank">Narrative</a></li>
                              						
                           </ul>
                           					
                        </ul>
                        					
                        <ul>
                           						
                           <li> <a href="documents/HandbookTenureReview2012wjmcommentsd.pdf" target="_blank">Handbook for Tenure Review Committee</a>                  
                           </li>
                           						
                           <li> <a href="documents/3E-02-Award-of-Tenure-and-Evaluation-of-Tenured-and-Tenured-Track-Faculty.pdf" target="_blank">Policy &amp; Procedure:&nbsp; Award of Tenure and Evaluation of Tenured and Tenure-Track Faculty</a>                  
                           </li>
                           						
                           <li>
                              							<a href="Summit.php">Tenure Summit Minutes</a>                  
                           </li>
                           						
                           <li>                    <a href="TenureChangesApprovedThroughGovernance.php">Tenure Changes Approved through the Governance Process</a>                  
                           </li>
                           						
                           <li><a href="documents/2012-13ProgramAssessmentReport.pdf" target="_blank">Program Assessment Report 2012/13</a></li>
                           					
                        </ul>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/index.pcf">©</a>
      </div>
   </body>
</html>