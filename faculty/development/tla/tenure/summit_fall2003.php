<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/summit_fall2003.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/tenure/">Tenure</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tenure Summit Minutes Fall 2003 </h2>
                        
                        <p> The reform of the tenure process is the product of Valencia's   shared governance
                           process.&nbsp; All matters concerning tenure are endorsed by the   Faculty Association
                           and approved by the College Learning Council.&nbsp; The   development of the tenure process<strong> </strong>relied on highly collaborative 'Tenure   Summits' involving all the stakeholders.
                           These summit meetings are a dynamic,   open collaborative decision-making forums,
                           where difficult issues are resolved   expediently.&nbsp; 
                        </p>
                        
                        <h3> <strong>Tenure Summit Summary, Submitted by Dr. Lisa Armour and   Dr. Tracy Edwards, Co-chairs
                              of the College Learning Council,  Fall Term 2003</strong>
                           
                        </h3>
                        
                        <p> <strong>Principles for the Faculty Induction Process</strong></p>
                        
                        <p><strong>The   tenure decision-making process should be&nbsp;</strong>            
                        </p>
                        
                        <ul type="disc">
                           
                           <li> informed by formal input from   tenured faculty</li>
                           
                           <li>based on dean, peer, and student   observations 'to the record'</li>
                           
                           <li>public (details of individual   decisions treated with discretion)</li>
                           
                           <li>formative (in-progress; helpful, encouraging, supportive     – non-adversarial)</li>
                           
                           <li>collaborative and inclusive</li>
                           
                           <li>transparent (clear to all: candidates, peers,     administrators…)</li>
                           
                           <li>competency &amp; performance-based (demonstrating     abilities/capabilities of professional,
                              learning-centered faculty)
                           </li>
                           
                           <li>fair and consistent in application </li>
                           
                           <li>based upon teaching/learning literature</li>
                           
                           <li>tied to candidate's scholarship of discipline, academic     standards, and evidence
                              of student learning
                           </li>
                           
                        </ul>
                        
                        <p><strong>The induction process should &nbsp;</strong></p>
                        
                        <ul type="disc">
                           
                           <li>foster trust and respect</li>
                           
                           <li>be a mentored experience</li>
                           
                           <li>create on-going communication with the candidate (from     multiple sources)</li>
                           
                           <li>nurture long-term commitment to institutional goals </li>
                           
                           <li>produce individual growth (be developmental)</li>
                           
                           <li>be flexibly tailored and provide adequate resources for     learning </li>
                           
                           <li>reflect teaching/learning scholarship</li>
                           
                           <li>feature clear and consistent procedures and criteria for     evaluation</li>
                           
                        </ul>
                        
                        <p><strong>The Tenure Process</strong></p>
                        
                        <ul>
                           
                           <li>Each tenure candidate will have an official record that is   confidential, based upon
                              evidence of work toward tenure.
                           </li>
                           
                           <li>Deans have formal responsibility for establishing and maintaining   candidates' official
                              records.&nbsp; Deans will see that reviews are scheduled,&nbsp;report   upon them in written
                              form, and include them in the official record.&nbsp; &nbsp;
                           </li>
                           
                           <li>The official record goes forward with the candidate through the   entire tenure review
                              process.&nbsp; 
                           </li>
                           
                           <li>In addition to successful completion of an Individualized Learning   Plan (ILP), other
                              factors considered in the tenure decision should include but   not be limited to
                           </li>
                           
                           <ul>
                              
                              <li>Performance reviews including student assessment of instruction   and classroom observation
                                 reports as relevant
                              </li>
                              
                              <li>Contributions to the department, campus and college</li>
                              
                              <li>Feedback from tenured faculty</li>
                              
                           </ul>
                           
                           <li>In each department, two tenured faculty members will be elected   annually by the
                              department's tenured faculty to serve in an advisory role to the   dean in making
                              that year's departmental tenure recommendations.&nbsp; The dean and   the two elected faculty
                              members will form the department's Tenure Review   Committee.&nbsp; Faculty on the Tenure
                              Review Committee will provide written remarks   supported by primary information,
                              such as tenure candidates' performance   evaluations, student evaluations, records
                              of classroom observations,   artifacts/portfolio, Individualized Learning Plan (ILP),
                              ILP assessments, and   the like.&nbsp; Feedback from other tenured faculty also will be
                              considered by the   elected faculty members and the dean.&nbsp; The elected faculty members'
                              summaries   and remarks will become part of the official record, to be transmitted
                              with the   dean's recommendation to the provost, Chief Learning Officer and President.
                           </li>
                           
                           <li>In cases where the described configuration for the Tenure Review   Committee is not
                              feasible, equivalent arrangements can be made.&nbsp; For example,   some departments do
                              not have two tenured faculty, or some departments have so   many tenure candidates
                              that it would not be feasible for two tenured faculty to   provide written remarks
                              for all of them.&nbsp; It is suggested that the Chief   Learning Officer, Provost, Faculty
                              Association Board, or College Learning   Council serve to approve equivalent arrangements
                              of the Tenure Review Committee.   TRCs for counselors and librarians will be comprised
                              of the dean/director and   two college-wide, elected counselors or librarians.
                           </li>
                           
                           <li>Training is a mandatory prerequisite for all (dean and faculty   members) who wish
                              to serve on a Tenure Review Committee.
                           </li>
                           
                        </ul>
                        
                        <p> &nbsp;<strong>Assessment of the Individualized Learning Plan (ILP)&nbsp;</strong></p>
                        
                        <ul type="disc">
                           
                           <li>Each ILP is created and examined   in community (an ILP Review Panel consisting of
                              dean and tenured faculty).
                           </li>
                           
                           <li>Annual or more frequent review   of ILP progress (formative feedback) is conducted
                              by the ILP Review Panel.
                           </li>
                           
                           <li>To the extent possible, each ILP   Review Panel is consistent over time (dean and
                              same tenured faculty)
                           </li>
                           
                           <li>A college-wide process will be   developed and employed to ensure that ILPs involve
                              comparable effort and quality   of outcome in spite of their unique, individualized
                              nature.
                           </li>
                           
                           <li>The ILP Review Panel provides a   summative commentary on ILP completion <em>to the record.</em>
                              
                           </li>
                           
                           <li>Portfolios documenting     completion of the ILP may be submitted in paper, digital
                              or mixed media     formats according to the wishes of the candidate.&nbsp; Content must
                              be equivalent     across formats; guidelines should be established by the ILP Review
                              Panel with     room for flexibility based on the candidate's project.
                           </li>
                           
                           <li>Each major element of the portfolio must be present     [Goals (ILP, Philosophy, Professional
                              Background), Reflective Critique,     Analysis of all seven Essential Competencies,
                              and Documentation of Learning     Outcomes]; however, if one element is not at the
                              acceptable level the overall     review of the portfolio is not necessarily unacceptable.
                           </li>
                           
                           <li>The final portfolio will be     ranked 'not acceptable' or 'acceptable,' with comments
                              included to express     exemplary performance or substantial concerns.
                           </li>
                           
                           <li>A report that synthesizes     ILP Review Panel findings on the final portfolio will
                              be prepared for each     tenure candidate.&nbsp; If any member of the panel has views differing
                              from those     expressed in the report, an additional "minority" report should be
                              prepared.
                           </li>
                           
                           <li>A final portfolio will be considered unacceptable only     if a majority of the ILP
                              Review Panel members considers the portfolio as a     whole unacceptable.&nbsp; A portfolio
                              that is considered acceptable by two out of     four panel members will be considered
                              acceptable.
                           </li>
                           
                           <li>Each ILP Review Panel (and each   panelist) will base assessments on the standard
                              Portfolio Rubric.&nbsp;   The final portfolio will be assessed   holistically according
                              to the standards set in the Portfolio Rubric.&nbsp;   Professional ethics and open access
                              to all primary materials by the ILP Review   Panel, Tenure Review Committee, Provost,
                              Chief Learning Officer, and President   will ensure use of the standard Portfolio
                              Rubric for assessments.
                           </li>
                           
                           <li>Completion of a portfolio     is intended to be a three-year process.&nbsp; However, if
                              a candidate receives     'acceptable' or 'exemplary' on all portfolio elements at
                              the end of year two,     the candidate will not be required to modify the portfolio
                              for the year-three     summative evaluation.&nbsp; The candidate could, however, choose
                              to modify the     portfolio, for example to strengthen an 'acceptable' element to
                              make it     'exemplary.' 
                           </li>
                           
                           <li>With all of the following     stipulations met,&nbsp; a one-year extension for submission
                              of the final portfolio     could be recommended to the dean by the ILP Review Panel:
                              
                              <ul>
                                 
                                 <li>Extraordinary circumstances, and</li>
                                 
                                 <li>Documentation of good progress towards completion, and</li>
                                 
                                 <li> Timely appeal to the ILP Review Panel</li>
                                 
                              </ul>
                              Such a recommendation would be   forwarded by the dean and considered and acted upon
                              by the provost, chief   learning officer, and President.
                           </li>
                           
                           
                           <li>Completion of an     'acceptable' portfolio is a necessary prerequisite for tenure.&nbsp;
                              Therefore, an     unacceptable portfolio disqualifies the candidate from further consideration
                              for tenure.&nbsp; A candidate whose final portfolio is considered 'unacceptable' by   
                              his or her ILP Review Panel may appeal to the Tenure Review Committee, which     would
                              refer the matter to another ILP Review Panel for consideration.&nbsp; The     candidate's
                              portfolio would then be reviewed by the second ILP Review Panel.&nbsp;     The second panel
                              would consist of one dean and three tenured faculty members     selected by the Tenure
                              Review Committee from the pool of trained ILP     reviewers.&nbsp; None of the members
                              of the candidate's original ILP Review Panel     would be eligible to serve on the
                              second panel.&nbsp; If the second panel found the     candidate's portfolio to be 'acceptable,'
                              that prerequisite for tenure would     be considered met.&nbsp; The candidate's official
                              record would include both the     original ILP Review Panel's report and the second
                              panel's report.
                           </li>
                           
                        </ul>
                        
                        <p><strong>Infrastructure for Assessment of the   Individualized Learning Plan (ILP)&nbsp;</strong></p>
                        
                        <ul type="disc">
                           
                           <li>Deans will assemble ILP     Review Panels for newly hired tenure candidates by the
                              end of September of the     candidates' first tenure-track year.
                           </li>
                           
                           <li>Each ILP Review Panel     should be made up of one tenured colleague from the candidate's
                              discipline and     campus, one tenured colleague from the candidate's discipline but
                              from another     campus, and one tenured colleague from a different discipline on
                              any campus.&nbsp;     There will be some flexibility on membership due to circumstances
                              of     departmental diversity and specialization.
                           </li>
                           
                           <li>Deans will work with the     Office of Curriculum Design, Teaching, and Learning (Teaching/Learning
                              Support     as of 8/2004) each July to plan and     schedule ILP review team training.&nbsp;
                              All review panel members, including     selected faculty and deans, will participate
                              in mandatory ILP review team     training before they begin their work with tenure
                              candidates.&nbsp; CDTL will make     every effort to provide training opportunities.
                           </li>
                           
                           <li>Each year's ILP review team     training will result in a pool of tenured faculty
                              prepared to provide     formative assessment and summative evaluation of ILPs.&nbsp; That
                              pool should     expand each year until it includes an appropriate number of faculty.&nbsp;
                              Training     will be open to all tenured faculty.
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Previous Tenure Summit Meeting Minutes </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><span><a href="Summit_fall2004.html">Fall 2004 </a></span></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2005.html">Summer 2005</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2007.html">Summer 2007 </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_fall2010.html">Fall 2010 </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2013.html">Summer 2013 </a></div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/summit_fall2003.pcf">©</a>
      </div>
   </body>
</html>