<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/summit_fall2010.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/tenure/">Tenure</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tenure Summit Minutes Fall 2010 </h2>
                        
                        <p> The reform of the tenure process is the product of Valencia's   shared governance
                           process.&nbsp; All matters concerning tenure are endorsed by the   Faculty Association
                           and approved by the College Learning Council.&nbsp; The   development of the tenure process<strong> </strong>relied on highly collaborative 'Tenure   Summits' involving all the stakeholders.
                           These summit meetings are a dynamic,   open collaborative decision-making forums,
                           where difficult issues are resolved   expediently.&nbsp; 
                        </p>
                        
                        <h3> <strong>Tenure Summit Minutes November 30, 2010 (CJI):
                              Submitted by:&nbsp; Faculty Council, President Lisa Macon Vice-President Bob Gessner; College
                              Learning Council Co-chair Jean-Marie Fuhrman; and Celine Kavalec-Miller          
                              </strong>
                           
                        </h3>
                        
                        <p><em>Decided:&nbsp; All decisions reached in the 2010 Tenure Summit will become part of the
                              Tenure Policy and Procedures.&nbsp; </em></p>
                        
                        <p><strong>1. Amendments to Clarify and Resolve Ambiguities in Language<br>
                              </strong><em>Paragraph III.A. and Paragraph 5 III.B.</em> 5-9 <em>of the Procedures for Policy 6Hx28: 3E-02, Award of Tenure and Evaluation of Tenured
                              and Tenure Track Faculty </em>should be amended to include all language changes proposed in the "Discussion Points"
                           document, except for the language describing the number of committees for each division.&nbsp;
                           That language should be amended to state that each division needs only one TRC for
                           all eligible tenure candidates.&nbsp; 
                        </p>            
                        
                        <p><strong>2.</strong> <strong>Tenure Sequence<br>
                              </strong>The tenure sequence for counselors and SLS faculty will follow the tenure sequences
                           in the chart below.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <blockquote>
                                             
                                             <blockquote>
                                                
                                                <p><strong>Counselors</strong></p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                       </div>                      
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <blockquote><strong>SLS Professors</strong></blockquote>
                                          
                                       </div>                      
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p> 1) Tenure Review Committee<br>
                                          2) Dean/Director<br>
                                          3) Provost<br>
                                          4) VP Student Affairs<br>
                                          5) President<br>
                                          6) District Board of Trustees
                                       </p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p>1) Tenure Review Committee<br>
                                          2) Director<br>
                                          3) Provost<br>
                                          4) Asst VP, Curriculum Dev and Art<br>
                                          5) Chief Learning Officer <br>
                                          6) President<br>
                                          7) District Board of Trustees
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>3. Initial STudent Success TRC Election Process<br>
                              </strong>Due to the college-wide structure of Student Success, TRC elections for all eligible
                           SLS tenure candidates will follow the guidelines below until there are at least 2
                           tenured faculty members. 
                        </p>
                        
                        <ul>
                           
                           <li>The Director of Student Success will solicit volunteers from tenured faculty and form
                              one college-wide committee with faculty from different campuses. This committee will
                              review all eligible tenure candidates in this division.
                           </li>
                           
                           <li>Committee members will be chosen via an election by tenured faculty members college-wide,
                              organized by the Director of Student Success.
                           </li>
                           
                           <li>Once there are at least 2 tenured faculty members, Student Success will follow the
                              TRC election process described for Small Divisions.&nbsp; 
                           </li>
                           
                        </ul>
                        
                        <p><strong>4.</strong> <strong>TRC Elections for Small Divisions<br>
                              <br>
                              Current Small Divisions</strong>: Due to their unique status and small numbers, for the purposes of TRC elections
                           only, consider the college-wide community of counselors, librarians and SLS faculty
                           divisions.<br>
                           <br>
                           <strong>Future Small Divisions</strong> <em>(This decision was deferred, but the proposal is included in the minutes for reference
                              in future summits.)<br>
                              </em>This proposal will apply to new or small campus-based divisions.&nbsp; 
                        </p>            
                        <ul>
                           
                           <li>For faculty (with the exceptions of counselors, librarians) until there are more than
                              2 tenured faculty members in the division.
                           </li>
                           
                           <li>The Director or Dean will solicit volunteers from tenured faculty on that campus.
                              
                           </li>
                           
                           <li>Committee members will be chosen via an election of all tenured faculty members on
                              the campus, organized by the Directoror Dean.
                           </li>
                           
                        </ul>
                        
                        
                        <p><strong>5. &nbsp;These decisions should be implemented immediately.&nbsp;</strong>
                           
                        </p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Previous Tenure Summit Meeting Minutes </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><span><a href="Summit_fall2003.html" target="_blank">Fall 2003</a></span></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_fall2004.html" target="_blank">Fall 2004</a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="Summit_summer2005.html" target="_blank">Summer 2005</a> 
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2007.html" target="_blank">Summer 2007</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="Summit_summer2013.html">Summer 2013 </a></div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/summit_fall2010.pcf">©</a>
      </div>
   </body>
</html>