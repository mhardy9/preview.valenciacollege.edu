<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/tenure/tenurechangesapprovedthroughgovernance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/tenure/">Tenure</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tenure Review Changes Approved Through Governance</h2>
                        
                        <p> The reform of the tenure process is the product of Valencia's shared governance process.&nbsp;
                           All matters concerning tenure are endorsed by the Faculty Association and approved
                           by the College Learning Council. 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Submitted by Robert McCaffery, Faculty Council Presidert, and Celine Kavalec-Miller,
                                    TLA Director, Fall Term 2012
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>NOTES ON TENURE UPDATES<br>
                                          </strong><strong>ACADEMIC YEAR 2012/13 </strong></p>
                                    
                                    <p>In response to Valencia’s organizational restructuring, Faculty Council held conversations
                                       with all stakeholders in the academic community, including faculty (librarians held
                                       a separate college-wide discussion since the changes effected their process most directly),
                                       deans, and campus presidents. These conversations took place between August through
                                       November of 2012. The changes were then endorsed by Faculty Council and subsequently
                                       approved by the College Learning Council.&nbsp; These revisions clarify responsibilities
                                       while attending to campus uniqueness. 
                                    </p>
                                    
                                    <p><strong>Professors (including all future tenure-track SLS professors)</strong></p>
                                    
                                    
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <p><strong>Professors*</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <ul>
                                                      
                                                      <li>Tenure Review Committee</li>
                                                      
                                                      <li>Dean </li>
                                                      
                                                      <li>Campus President</li>
                                                      
                                                      <li>President</li>
                                                      
                                                      <li>District Board of Trustees</li>
                                                      
                                                   </ul>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <p>*In the case of the Class of 2013 SLS Candidates, the Director of Transfer Readiness,
                                       in consultation with the Dean of Learning Support, will continue to manage the portfolio/TRC
                                       process.
                                    </p>
                                    
                                    <p><strong>Librarians</strong></p>
                                    
                                    <p>Tenure candidates’ direct supervisors will manage the portfolio process.&nbsp; On campuses
                                       with library directors, this will be the Library Director.&nbsp; On campuses without a
                                       director this responsibility will fall to the Dean of Learning Support.&nbsp; 
                                    </p>
                                    
                                    
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <p><strong>Librarians </strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <ul>
                                                      
                                                      <li>Tenure Review Committee</li>
                                                      
                                                      <li>Director*</li>
                                                      
                                                      <li>Dean</li>
                                                      
                                                      <li>Campus President</li>
                                                      
                                                      <li>President</li>
                                                      
                                                      <li>District Board of Trustees</li>
                                                      
                                                   </ul>
                                                   
                                                   
                                                   <p><em>*On the applicable campuses</em></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/tenure/tenurechangesapprovedthroughgovernance.pcf">©</a>
      </div>
   </body>
</html>