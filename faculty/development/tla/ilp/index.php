<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ILP Panel Reviews | Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/ilp/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li>ILP Reviews</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        					
                        <h2>ILP Review Panel Information &amp; Documents</h2>
                        					
                        <ul>
                           						
                           <li><a href="../Candidate/documents/5-year-Reporting-Schedule-2017-18.pdf" target="_blank">Reporting Schedule 2017-18</a></li>
                           						
                           <li><a href="documents/ReviewPanelcomposition.pdf" target="_blank">Review Panel Composition</a></li>
                           					
                        </ul>
                        					
                        <!--*************************TAB 1***********************-->
                        					
                        <div>
                           						
                           <h3>Year-1 Review</h3>
                           						
                           <p>Candidates assess their needs this year and submit the Analysis of My Practice Form
                              to the dean and TLA. Candidates receive feedback from the dean and TLA on the form
                              below. Panelists are not selected by the dean until Septemeber of a candidate's second
                              year. 
                           </p>
                           						
                           <ul>
                              							
                              <li><a href="documents/AnalysisofMyPracticeFeedbackForm.docx" target="_blank">Analysis of My Practice Feedback Form</a></li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <!--*************************TAB 2***********************-->
                        					
                        <div>
                           						
                           <h3>Year-2 Review</h3>
                           						
                           <ul>
                              							
                              <li><a href="ilp_Y1_Workshops.php">ILP Panelist Training Schedule</a></li>
                              							
                              <li><a href="../..//documents/ILPReviewPanelAgenda1.24.17.pdf" target="_blank">Agenda for ILP Review Meeting</a></li>
                              							
                              <li><a href="documents/ILPReviewPanelReportForm1.26.15.doc" target="_blank"> ILP Report Form</a></li>
                              							
                              <li><a href="documents/ILPPanelReviewataglance7-19-2013.pdf" target="_blank">ILP Review Panel Meeting at a Glance</a></li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <!--*************************TAB 3***********************-->
                        					
                        <div>
                           						
                           <h3>Year-3 Review</h3>
                           						
                           <ul>
                              							
                              <li><a href="ilp_Y2_Workshops.php">Portfolio Panelist Training Schedule </a></li>
                              							
                              <li><a href="documents/PortfolioReviewPanelAgenda4.7.14.pdf" target="_blank">Agenda for Portfolio Review Meeting</a></li>
                              							
                              <li><a href="documents/PortfolioReportForm.docx">Portfolio Report Form</a></li>
                              							
                              <li><a href="documents/RequiredNewElementsofaValenciaFacultyPortfolio6.12.pdf" target="_blank">Portfolio Elements</a></li>
                              							
                              <li><a href="documents/FacultyPortfolioRubric.pdf" target="_blank">Rubric for Evaluating a Faculty Portfolio </a></li>
                              							
                              <li><a href="documents/WritingaUsefulRecommendation.pdf" target="_blank">How to Write Effective Portfolio Recommendations </a></li>
                              							
                              <li><a href="documents/PortfolioPanelReviewataglance7-19-2013.pdf" target="_blank">Portfolio Review Panel Meeting at a Glance</a></li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <!--*************************TAB 4***********************-->
                        					
                        <div>
                           						
                           <h3>Year-4 Review</h3>
                           						
                           <ul>
                              							
                              <li><a href="documents/FinalPortfolioReportForm.doc" target="_blank">Final Portfolio Report Form</a></li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <div>
                           						
                           <h3>Year-5 Review</h3>
                           						
                           <p>Coming soon. </p>
                           					
                        </div>
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/ilp/index.pcf">©</a>
      </div>
   </body>
</html>