<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/ilp/ilp_y1_workshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/ilp/">ILP Reviews</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>  ILP 
                           Review Panelist Training
                        </h2>
                        
                        <p>Large scale rubric-based assessments require regular norming  to maintain consistency
                           and integrity. The TLA recommends norming before each panel review cycle to meet the
                           challenge of applying the portfolio rubric criteria consistently, while allowing for
                           discipline flexibility. To meet the needs of faculty, the TLA now offers two levels
                           of ILP panelist training: 1) an 8-hour hybrid training for faculty either serving
                           on a panel for the first time or experienced panelists who did not complete training
                           recently and 2) a 2-hour face-to-face refresher training for those faculty who completed
                           the full training within the last year. 
                        </p>
                        
                        <p><strong>ASMT 3321: ILP Full Training<br>
                              (6 hours online, over an 18-day period, and a 2-hour face-to-face Wrap Up) <br>
                              8 PD hours</strong></p>
                        
                        <p>The purpose of this eight-hour hybrid training  is to help prepare  panelists to understand
                           1) the contents of an ILP and 2) the expectations &amp; framework of a ILP Review.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong><span><strong>Spring 2016 Dates </strong></span></strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Date</strong></div>
                                    
                                    <div><strong>CRN#</strong></div>
                                    
                                    <div>
                                       
                                       <p><strong>Wrap Ups<br>
                                             </strong>Panelists from all online courses will select <strong>one</strong> of the three Wrap Up times. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>January 19 - February 5, 2016</div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p>CRN 3101</p>
                                       
                                       <p>CRN 3103 <br>
                                          <br>
                                          
                                       </p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><br>
                                          February 11, West 6-202, 3:00-5:00
                                       </p>
                                       
                                       <p>February 15, East 3-113, 3:00-5:00</p>
                                       
                                       <p>February 19, Osceola 4-105, 3:00-5:00</p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>January 25  - February 9, 2016</div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p>CRN 3104</p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><strong><strong>ASMT 3324: </strong>ILP Norming <br>
                              (2 hours face-to-face)<br>
                              2 PD hours </strong></p>
                        
                        <p>This 2-hour training is reserved for those panelists who  completed the online training
                           within the last year. 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>Spring 2016 Dates</strong></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>Date</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div><strong>CRN#</strong></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>February 4, 2016, East Campus 3-113 , 3:00-5:00 </div>
                                    </div>
                                    
                                    <div>
                                       <div><span>CRN 2928 </span></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>February 5, 2016, West Campus, 6-202, 3:00-5:00 </div>
                                    </div>
                                    
                                    <div>
                                       <div>CRN 3228</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>February 9, 2016, Osceola Campus 4-105, 3:00-5:00</div>
                                    </div>
                                    
                                    <div>
                                       <div>CRN 3227</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>February 10, 2016, East Campus 3-113, 3:00-5:00</div>
                                    </div>
                                    
                                    <div>
                                       <div>CRN 3075 </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <p><strong>Here's all you need to get started.</strong></p>
                        
                        <p>1.&nbsp; Register for ASMT 3321  or ASMT 3324 in Atlas (<a href="../../howToRegisterForCourses.html" target="_blank">click here</a> for directions on how to register in Atlas). 
                        </p>
                        
                        <p>2. ASMT 3321 is a hybrid course. Enter Blackboard using your Atlas user name and password
                           (<a href="https://learn.valenciacollege.edu/">click here</a> to log in to Blackboard.)
                        </p>
                        
                        <p><br>
                           3. Click on "ILP Review Panelist Training"   (it may take about a minute to load).
                        </p>
                        
                        <p>4. Follow course directions </p>
                        
                        <p><strong>Target Audience:&nbsp;&nbsp; </strong>All faculty, deans, and provosts. 
                        </p>
                        
                        <p><strong>PD Compensation Plan Option:</strong> 8 hours PD credit for ASMT 3321 or 2 hours PD credit for ASMT 3324 
                        </p>
                        
                        <h3>Note:</h3>
                        ï¿½ The 8-hour workshop is required for panelists new to an ILP panel and experienced
                        panelists who did not complete training within the last two years.. 
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/ilp/ilp_y1_workshops.pcf">©</a>
      </div>
   </body>
</html>