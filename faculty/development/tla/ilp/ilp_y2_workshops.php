<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/ilp/ilp_y2_workshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/ilp/">ILP Reviews</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8">  
                                 <img alt="" height="8" src="arrow.gif" width="8">  
                                 <img alt="" height="8" src="arrow.gif" width="8">  
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <div>        
                                                         
                                                         
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                   </div> 
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>  
                                                   
                                                   <li><a href="../../../resources/index.html">Faculty Resources</a></li>
                                                   
                                                   <li><a href="http://valenciacollege.edu/faculty/association/">Faculty Association</a></li>
                                                   
                                                   <li><a href="../../../../irb/index.html">Institutional Review Board (IRB)</a></li>
                                                   
                                                   <li><a href="../../../resources/index.html">Faculty Handbook</a></li>
                                                   
                                                   <li><a href="../../../sabbatical/index.html">Sabbatical Leave</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <h2>
                                                         <br>
                                                         
                                                      </h2>
                                                      
                                                      <h2><strong>Portfolio Review  Panelist Training ASMT 3322</strong></h2>
                                                      
                                                      <p>These seminars are designed to help prepare Review Panelists for the second review
                                                         of tenure candidates’ ILP work. There are two offerings. Those panelists who have
                                                         never been trained are required to complete the hybrid course  (2 and ½ weeks online
                                                         with a face-to-face Wrap Up). Panelists who completed ASMT 3322 last year can attend
                                                         the face-to-face wrap up to refresh their understanding of the portfolio rubric. 
                                                         The college recommends norming each year to increase  evaluation consistency.
                                                      </p>
                                                      
                                                      <p>Panelists will examine the elements and requirements of the Faculty Portfolio. Panelists
                                                         will then develop a shared understanding of what constitutes a quality portfolio by
                                                         assessing sample artifacts using the Portfolio Report Form. 
                                                      </p>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong>Spring&nbsp; 2016 Dates <br>
                                                                           Hybrid Course<br>
                                                                           (Training and Norming)<br> 
                                                                           </strong></p>                        
                                                                     
                                                                     <p><strong>Required for all panelists who have not completed Portfoli Reveiw Panelist Training.</strong>                            
                                                                     </p>                         
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong>Online Portion<br>
                                                                           </strong></p>
                                                                     
                                                                     <p><strong><br>
                                                                           </strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong>Campus and Time for Face-to-Face Wrap Up <br>
                                                                           </strong>(Attend any of the Wrap Ups listed for the time period) <strong><br>
                                                                           </strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div><strong>CRN #</strong></div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>March 14 - April 1, 2016 <br>
                                                                        
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>April 1, 3:00-5:00, West Campus, HSB 211</p>
                                                                     
                                                                     <p>April 6, 3:00-5:00, East Campus, 6-203<br>
                                                                        <br>
                                                                        April 7, 3:00-5:00, Osceola Campus, 1-219B
                                                                     </p>
                                                                     
                                                                     <p>April 8, 3:00-5:00, West Campus, 6-202 <br>
                                                                        
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong>CRN 3311</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>March 21 - April 7, 2016 </div>                    
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong>CRN 3316</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>            
                                                      <p><strong>PD               Compensation Plan option:&nbsp;</strong>10&nbsp; hours credit 
                                                      </p>
                                                      
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <h3>
                                                                        
                                                                     </h3>
                                                                     
                                                                     <h3>Spring 2016 Dates<br>(Norming only)                        
                                                                     </h3>
                                                                     
                                                                     
                                                                     <p><span><strong>Recommended for   panelists who completed portfolio review panel training in the previous
                                                                              year. </strong></span><strong><br>
                                                                           The college recommends norming each year to increase  evaluation consistency.</strong></p>
                                                                     
                                                                     <p><strong>Training materials will be emailed to participants </strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     <div><strong>Date </strong></div>
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong>Campus, Time, and Room </strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     <div><strong>CRN # </strong></div>
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><br>
                                                                        April 1, 2016 <br>
                                                                        
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><br>
                                                                        3:00-5:00, West Campus, HSB 211<br>
                                                                        
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong><br>
                                                                           CRN 3318</strong><br>
                                                                        
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><br>
                                                                        April 6, 2016 
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><br>
                                                                        3:00-5:00, East Campus, 6-203
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><span><strong><br>
                                                                              CRN 3317</strong></span></p>
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><br>
                                                                        April 7, 2016 
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><br>
                                                                        3:00-5:00, Osceola Campus, 1-219B
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p><strong><br>
                                                                           CRN 3321 </strong><em></em></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     <div>
                                                                        <br>
                                                                        April 8, 2016 
                                                                     </div>
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     <div>
                                                                        
                                                                        <p><br>
                                                                           3:00-5:00, West Campus, 6-202 
                                                                        </p>
                                                                        
                                                                        
                                                                     </div>
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     <div><strong><br>
                                                                           CRN 3322 </strong></div>
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <p><strong>PD Compensation Plan option:&nbsp;</strong>3&nbsp; hours credit 
                                                      </p>
                                                      
                                                      <p><a href="../../howToRegisterForCourses.html" target="_blank">Click here</a> for directions on how to register.
                                                      </p>
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="770"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><img alt="" height="1" src="spacer.gif" width="162"></div>
                                       </div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><br></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/ilp/ilp_y2_workshops.pcf">©</a>
      </div>
   </body>
</html>