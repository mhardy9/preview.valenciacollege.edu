<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/diversity-statement.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h3>
                           
                        </h3>
                        
                        
                        
                        
                        
                        <h2>Teaching/Learning Academy Diversity Statement</h2>
                        
                        <p>Diversity adds to our community and professional environment in  many ways. Including
                           all voices  encourages free inquiry, civil discourse, more thoughtful conclusions,
                           and deeper  perspectives on teaching and learning, bringing excellence to our work
                           as  teachers, counselors, and librarians. <br>
                           TLA values diversity and seeks to create a community of  faculty who promote the college’s
                           mission of Inclusive Excellence. We advocate for the hiring and retention of  diverse
                           faculty and intentionally promote inclusion through the following  practices:
                        </p>
                        
                        <ul>
                           
                           <li>
                              <em>Encouraging</em> the expression of different  perspectives, 
                           </li>
                           
                           <li>
                              <em>Cultivating</em><strong> </strong>positive  interdependence amongst the TLA cohort, and
                           </li>
                           
                           <li>
                              <em>Promoting</em> active participation and  cooperative engagement in our curriculum. 
                           </li>
                           
                        </ul>
                        
                        <h3>The TLA director,  facilitators, and staff are committed to: </h3>
                        
                        <h4>              For ourselves</h4>
                        
                        <ul>
                           
                           <li>
                              <em>Refining</em> our skills and knowledge to  become better allies to people of historically underrepresented
                              groups. 
                           </li>
                           
                           <li>
                              <em>Developing</em><strong> </strong>our  diversity literacy through reading, intentional discussions, trainings, and 
                              attendance at conferences as well as applying those concepts and practices to  our
                              curriculum and processes. 
                           </li>
                           
                           <li>
                              <em>Identifying</em><strong> </strong>our  own biases and assumptions and reflecting on how these biases and assumptions
                              impact  or influence our interactions with others. 
                           </li>
                           
                           <li>
                              <em>Recognizing</em> and addressing inequity in  our systems and process.
                           </li>
                           
                           <li>
                              <em>Fostering</em> an atmosphere of respect  and appreciation for multiple perspectives
                           </li>
                           
                           <li>
                              <em>Sharing</em> current research on  inclusive practices to other departments and systems at the
                              college. 
                           </li>
                           
                        </ul>
                        
                        <h4>For tenure-track faculty</h4>
                        
                        <ul>
                           
                           <li>
                              <em>Providing</em> equity in tenure candidate  support at all stages of the process.
                           </li>
                           
                           <li>
                              <em>Acknowledgi</em>ng the curriculum  contributions of the faculty members in the process. 
                           </li>
                           
                           <li>
                              <em>Helping</em> faculty identify their own  biases and assumptions and reflect on how these biases
                              and assumptions impact  or influence interactions with peers and students. 
                           </li>
                           
                           <li>
                              <em>Encouraging</em> faculty to develop  curriculum that explores diversity as a strength acknowledges
                              differences, and  understands the problems of bias and the benefits of inclusion.
                           </li>
                           
                           <li>
                              <em>Guiding</em> individual faculty members  to pursue their personal educational interests and develop
                              curriculum that  intentionally includes perspectives of historically underrepresented
                              groups. 
                           </li>
                           
                           <li>
                              <em>Focusing</em> on individual faculty  member’s strengths and needs throughout the portfolio process.
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/diversity-statement.pcf">©</a>
      </div>
   </body>
</html>