<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/elementsadequatepreparation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Elements of Action Research </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="elementsprojectinformation.html">Project Information</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementscleargoals.html">Clear Goals </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsadequatepreparation.html">Adequate Preparation</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsappropriatemethods.html">Appropriate Methods </a></div>
                                 </div>
                                 
                                 <div><a href="elementssignificantresults.html">Significant Results </a></div>
                                 
                                 <div><a href="elementsreflectivecritique.html">Reflective Critque </a></div>
                                 
                                 <div><a href="elementseffectivepresentation.html">Effective Presentation </a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>Elements - Adequate Preparation</h2>
                        
                        <p><strong>A. Background from Multiple Perspectives</strong><br>
                           The  following prompts can help you gather background information to help you  sufficiently
                           explain your preparation for this project. Answer the questions  that are relevant
                           to your project as a way to focus your ideas.
                           
                        </p>
                        
                        <ol> 
                           
                           <li>
                              <strong>Student  Perspective</strong><ul>
                                 
                                 <li>What  do my students say about the situation&nbsp;I've identified  in my research question?</li>
                                 
                                 <li>What evidence do I have from students that has informed this project?</li>
                                 
                                 <li>Do they confirm or fail to support my  assumption about what I think is going on in
                                    the classroom?
                                 </li>
                                 
                                 <li>What strategies will I use to collect student opinions?</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong>&nbsp;  Colleague Perspective</strong>
                              
                              <ul>
                                 
                                 <li>What  do my peers think about the problem/situation/success I've identified in my
                                    research question? Do they confirm or fail to support my assumption about what  I
                                    think is going on in the classroom?
                                 </li>
                                 
                                 <li>Do  they experience similar things in their practices?</li>
                                 
                                 <li>Do  they perceive my research question as a worthwhile item for investigation?</li>
                                 
                                 <li>How  can I get this information from them?</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong>Expert  Perspective</strong>
                              
                              <ul>
                                 
                                 <li>What do experts in the field say? Do they confirm or fail to support my assumption
                                    about what I think is going on in the classroom?
                                 </li>
                                 
                                 <li>What does a review of relevant literature reveal? </li>
                                 
                                 <li>Is there professional literature or scholarly writing related to my  question?</li>
                                 
                                 <li>What kind of research can I do to assist me with my question? </li>
                                 
                                 <li>Is the literature consistent with my ideas and assumptions?</li>
                                 
                                 <li>Include  References at the end of this section </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li> <strong>Self  Perspective</strong>
                              
                              <ul>
                                 
                                 <li>What does my own personal experience tell me?</li>
                                 
                                 <li> Does it confirm or fail to support my  assumption about what I think is going on
                                    in the classroom? 
                                 </li>
                                 
                                 <li>Why have I identified the question above?</li>
                                 
                                 <li>What personal experiences inform me that this is a worthwhile question?</li>
                                 
                                 <li>What makes this question important to me?</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ol>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/elementsadequatepreparation.pcf">©</a>
      </div>
   </body>
</html>