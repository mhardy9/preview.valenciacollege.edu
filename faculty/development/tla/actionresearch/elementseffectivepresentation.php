<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/elementseffectivepresentation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Elements of Action Research </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="elementsprojectinformation.html">Project Information</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementscleargoals.html">Clear Goals </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsadequatepreparation.html">Adequate Preparation</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsappropriatemethods.html">Appropriate Methods </a></div>
                                 </div>
                                 
                                 <div><a href="elementssignificantresults.html">Significant Results </a></div>
                                 
                                 <div><a href="elementsreflectivecritique.html">Reflective Critque </a></div>
                                 
                                 <div><a href="elementseffectivepresentation.html">Effective Presentation </a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>Elements - Effective  Presentation</h2>
                        
                        <p><strong>A. Plan for  Dissemination</strong></p>
                        
                        <p>It is commonly  held that scholarly work should be made public. In one or two sentences,
                           describe how you will make your project public. The following prompts can help  you
                           identify how you plan to make your work public, beyond the Action Research  Builder.<br>
                           <br>
                           Will  I present my result to my ILP Review Panel?
                        </p>
                        
                        <ul>
                           
                           <li>Department  or discipline colleagues?</li>
                           
                           <li>Valencia faculty  development forums, such as Destination, Faculty-to-Faculty, Assessment
                              Initiatives?
                           </li>
                           
                           <li>Professional  journal article?</li>
                           
                           <li>Professional  conference presentation?</li>
                           
                        </ul>
                        
                        <p><strong>Remember, When Communicating Your Results:</strong></p>
                        
                        <ul>
                           
                           <li>Include  measures of central tendency and measures of variation in your statistics
                              when  you gather sufficient quantitative data
                           </li>
                           
                           <li>Use  charts and graphs when possible</li>
                           
                           <li>Provide  descriptive titles and clear labels</li>
                           
                           <li>Use  scales consistently</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/elementseffectivepresentation.pcf">©</a>
      </div>
   </body>
</html>