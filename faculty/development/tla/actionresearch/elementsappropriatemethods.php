<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/elementsappropriatemethods.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Elements of Action Research </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="elementsprojectinformation.html">Project Information</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementscleargoals.html">Clear Goals </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsadequatepreparation.html">Adequate Preparation</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsappropriatemethods.html">Appropriate Methods </a></div>
                                 </div>
                                 
                                 <div><a href="elementssignificantresults.html">Significant Results </a></div>
                                 
                                 <div><a href="elementsreflectivecritique.html">Reflective Critque </a></div>
                                 
                                 <div><a href="elementseffectivepresentation.html">Effective Presentation </a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>Elements - Appropriate Methods</h2>
                        
                        <p><strong>A. Methods and Assessment Plan</strong></p>
                        
                        <ol>
                           
                           <li>
                              <strong>Student Learning  Outcome Statement (SLO)</strong>
                              <br> 
                              <a href="documents/WritingMeasurableLearningOutcomes.pdf" target="_blank">Tutorial on how to write a measurable Student Learning Outcome 
                                 </a><br> 
                              A Student Learning Outcome states what a student should understand  and/or be able
                              to do as a result of what she has learned in a  course, library orientation, counseling
                              session.
                              
                              <ul>
                                 
                                 <li>What  will my students know and be able to do better as a result of the intervention,
                                    innnovation,  or strategy I employ here?
                                 </li>
                                 
                                 <li>Does  the SLO connect to or support the outcomes of a course or program?</li>
                                 
                                 <li>Does  the SLO describe learning that is meaningful in a real-world context?</li>
                                 
                              </ul>
                              Effective      Student Learning Outcomes should be results-oriented, clearly understood
                              by      colleagues and students, measurable, aligned with the Factulty Learning Outcome,
                              
                              
                              
                              and critical to teaching and learning.  
                              <br>
                              
                           </li>
                           
                           
                           <li>
                              <strong>Performance  Indicators of Student Learning Outcomes</strong><br>
                              
                              <p>With each  Student Learning Outcome, Performance Indicators identify the small steps
                                 students take to achieve the learning outcome.&nbsp; Performance Indicators are  pre-determined
                                 criteria, stated by you, that identify these steps.
                              </p>
                              
                              <p>Keep in mind,  many people think about the Student Learning Outcome (#1) before Performance
                                 Indicators (#2). This section, particularly, lends itself to recursive  thinking.
                              </p>
                              
                              <p>The following  questions can help you identify the performance indicators of the student
                                 learning outcomes for your project. Answer the questions that are relevant to  your
                                 project as a way to focus your ideas. 
                              </p>
                              
                              <ul>
                                 
                                 <li>What  specific qualities or evidence will I observe in the students'  work/performance/behavior
                                    that will demonstrate to me they have achieved this  competency or indicator?
                                 </li>
                                 
                                 <li>What  is the minimum level of performance I am willing to accept from a student to
                                    say he or she has achieved the learning outcome(s)? (This is your criteria.) 
                                 </li>
                                 
                                 <li>What  student core competencies and indicators (TVCA) are related to these outcomes?</li>
                                 
                              </ul>
                              
                              <p>Performance Indicators of Student Learning Outcomes are the qualitative  or quantitative
                                 interim measurements demonstrating that the meaningful steps in  student learning
                                 are being taken to achieve mastery. In other words, Indicators  identify the specific,
                                 incremental traits or features of successful student  mastery.
                              </p>
                              
                           </li>
                           
                           
                           <li>
                              <strong>Teaching,  Counseling, or Librarianship Strategies of Student Learning Outcomes</strong>
                              
                              <p>The following  questions can help you decide the methods, strategies, and/or techniques
                                 to  support student mastery of the student learning outcomes you identified in your
                                 project. Answer the questions that are relevant to your project as a way to  focus
                                 your ideas. 
                              </p>
                              
                              <ul>
                                 
                                 <li>What  are my learning activities? Will these activities prepare my students for  mastery?
                                    
                                 </li>
                                 
                                 <li>What  are the processes for taking students from beginning to end?</li>
                                 
                                 <li>How  can I establish an inclusive and safe learning environment for my students  during
                                    this process? 
                                 </li>
                                 
                                 <li>How  will my students make connections with the content, each other, and the  instructor,
                                    counselor, or librarian?
                                 </li>
                                 
                                 <li>How  am I going to keep records of the processes (student learning, teaching  strategies,
                                    etc.) for my action research project?&nbsp; For example, written  methods: personal journal
                                    or diary, field notes, surveys, questionnaires?&nbsp;  Live methods: Interviews, role play,
                                    video or audio tape?<br> 
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           
                           <li>
                              <strong>Assessment Strategies of Student Learning Outcomes</strong>
                              
                              <p>The following  questions can help you decide the most effective assessment methods.
                                 Answer the  questions that are relevant to your project as a way to focus your ideas.
                                 
                              </p>
                              
                              <ul>
                                 
                                 <li>  How  will I measure the performance indicators described in the Student Learning
                                    Outcomes section?
                                 </li>
                                 
                                 <li>  What  tool(s) am I going to use to measure/gauge how my students perform in relation
                                    to the indicators in the Student Learning Outcome?
                                 </li>
                                 
                                 <li>  Are  there tools I can use that will give the students formative feedback (prior
                                    to  receiving summative feedback)?
                                 </li>
                                 
                                 <li>  How  will my students know the standards or criteria their work will be evaluated
                                    against?
                                 </li>
                                 
                              </ul>
                              
                              <p>When choosing an  assessment technique, I should ask myself:</p>
                              
                              <ul>  
                                 
                                 <li>  Is  the assessment technique chosen appropriate to my goal?</li>
                                 
                                 <li>  Can  I integrate the assessment technique into my activities?</li>
                                 
                                 <li>  Will  it contribute to learning?</li>
                                 
                              </ul>
                              
                              <p>When applying an assessment technique, I should ask myself:</p>
                              
                              <ul>
                                 
                                 <li>  Have  I tried it? </li>
                                 
                                 <li>  Have  I done a run-through with a colleague? </li>
                                 
                                 <li>  How  will I make its purpose clear to students? </li>
                                 
                                 <li>  How  will I make its process clear to students? </li>
                                 
                                 <li>  How  will I provide the necessary practice for students? </li>
                                 
                                 <li>  Have  I allowed enough time to apply the technique?<br> 
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong>Action  Research Methodology Design</strong>
                              
                              <p>Action Research  is a scholarly inquiry to improve teaching and student learning.
                                 You have  already described your plan's student learning outcomes, performance  indicators,
                                 strategies, and assessment methods of the student learning outcome.  Now, you will
                                 design a plan to assess the effectiveness of the innovative  teaching and assessment
                                 methods you implemented in your project. Remember,  action research is about what
                                 you are doing, not what other people are doing. 
                              </p>
                              
                              <p>&nbsp;The  following questions can help you decide the most effective methods to measure
                                 the usefulness of your innovations. Answer the questions that are relevant to  your
                                 project as a way to focus your ideas.
                              </p>
                              
                              <ul>
                                 
                                 <li>How  will I know whether or not (to what degree) my innovations have worked?</li>
                                 
                                 <li>Have  I planned how I will analyze the data?</li>
                                 
                                 <li>Have  I collect a reasonable amount of data?</li>
                                 
                                 <li>Is  my process of analysis manageable?</li>
                                 
                                 <li>Have  I planned adequate time to do the analysis?</li>
                                 
                              </ul>
                              
                              <p>Consider the validity  of your results:&nbsp; </p>
                              
                              <p>What  kind of evidence will I produce to judge the value of my innovation(s)  incorporated
                                 in my action research project? 
                              </p>
                              
                              <p>For  example,</p>
                              
                              <ul>
                                 
                                 <li>If  applicable, compare the results of a base class and pilot class</li>
                                 
                                 <li>If  applicable, compare the results of a base assignment and pilot assignment</li>
                                 
                                 <li>If  applicable, compare the results of a base class with aggregate departmental  data</li>
                                 
                              </ul>
                              
                              <p>Consider the reliability  of your results:&nbsp; </p>
                              
                              <ul> 
                                 
                                 <li>Will  the statistics be the same if another researcher replicates your project?&nbsp; </li>
                                 
                                 <li>If  you replicate the project in other classes, how will you demonstrate  reliability
                                    and/or effectively describe your project for replication? 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ol>  
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/elementsappropriatemethods.pcf">©</a>
      </div>
   </body>
</html>