<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/elements.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Elements of an Action Research Project</h2>
                        
                        <p>This information is designed to serve as both a guide and a worksheet. Answering the
                           questions under each element will move you through the  steps to both design and then
                           implement an action research project in your practice. For additional information
                           on assessing the effectiveness of your project, consult the <a href="rubric.html">Action Research Project Rubric</a>. 
                        </p>
                        
                        <p><a href="http://valenciacollege.edu/faculty/development/tla/actionResearch/documents/ARPPlanTemplate.docx" target="_blank">Printer Friendly Action Research Project Plan Template </a></p>
                        
                        <p>Below the steps have been laid out to help you accomplish your project.</p>
                        
                        
                        
                        
                        <h3>RESOURCES</h3>
                        
                        <ul>
                           
                           <li><a href="elementsprojectinformation.html">Project Information</a></li>
                           
                           <li><a href="elementscleargoals.html">Clear Goals</a></li>
                           
                           <li><a href="elementsadequatepreparation.html">Adequate Preparation</a></li>
                           
                           <li>
                              <a href="elementsappropriatemethods.html">Appropriate Methods</a> 
                           </li>
                           
                           <li><a href="elementssignificantresults.html">Significant Results</a></li>
                           
                           <li><a href="elementsreflectivecritique.html">Reflective Critique</a></li>
                           
                           <li><a href="elementseffectivepresentation.html">Effective Presentation </a></li>              
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        <ul>
                           
                           <li><a href="elements.html#" title="Year-">Cycle of Action Research</a></li>
                           
                           <li><a href="elements.html#" title="Year 3">General Action Research Process</a></li>
                           
                           <li>       
                              
                           </li>
                        </ul>
                        
                        
                        
                        
                        <div>  
                           
                           
                           
                           <div>
                              
                              <div><img alt="Cycle of Action Research" border="0" height="755" src="spiralTable.jpg" title="Cycle of Action Research" width="560"></div>
                              
                           </div>    
                           
                           
                           
                           <div>
                              
                              <div><img alt="AR Cycle" border="0" height="755" src="fd_spiralTable.jpg" title="AR Cycle" width="560"></div>
                              
                           </div>  
                           
                           
                           
                           <div>
                              
                              
                           </div>  
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/elements.pcf">©</a>
      </div>
   </body>
</html>