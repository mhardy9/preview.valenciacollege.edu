<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Courses | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Courses</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               		
               <!-- BEGIN MAIN CONTENT -->
               
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Courses</h2>
                        
                        <h3>FACILITATED COURSES</h3>
                        
                        
                        <h4>SOTL2171 - Scholarship of Teaching and Learning (2 PD Hours)</h4>
                        
                        <p>This Essential Competency seminar examines Action Research as a method for educators
                           to continuously reflect on the effectiveness of their teaching, counseling, and librarianship.
                        </p>
                        
                        
                        <h4>SOTL2271 - Learning to Use the Action Research Project Builder (2 PD Hours)</h4>
                        
                        <p>In this hands-on workshop, participants will learn how to use the Action Research
                           Project Builder to plan, implement, assess, and publish (within the Valencia community)
                           their Action Research.
                        </p>
                        
                        <p>Not offered in 2014/15 </p>
                        
                        
                        <h4>SOTL2272 - Developing Effective Surveys (2 PD Hours)</h4>
                        
                        <p>In this hands-on workshop, participants will learn the benefits of the common survey
                           types, learn tips on how to write effective survey questions, and collaboratively
                           assess sample surveys. Participants will have the opportunity to apply these principles
                           to their own work.
                        </p>
                        
                        <p>East Campus: March 19, 2015, 2:00PM-4:00PM, <span>CRN 2305</span></p>
                        
                        <p>West Campus: March 20, 2015, 2:00PM-4:00PM, <span>CRN 2346</span></p>
                        
                        
                        <h4>SOTL2273 - IR and You: How IR Can Help Faculty Research (2 PD Hours)</h4>
                        
                        <p>This interactive course has been designed to familiarize participants with how Institutional
                           Research (IR) can support faculty research projects. Participants will receive immediate
                           feedback from IR on types of data analyses or studies that would assist in their data
                           collection. Participants will learn what resources are available for different types
                           of research. Participants will understand the relationship between information requests
                           for research and the Institutional Review Board (IRB) process.
                        </p>
                        
                        <p>West Campus: October 3, 2014, 2:00PM - 4:00PM, CRN 2862</p>
                        
                        
                        <h4>SOTL3272 - IR Requirements Part 1 &amp; SOTL3273 IR Requirements Part 2 (4 PD Hours)</h4>
                        
                        <p>Meeting one-hour face-to-face and three hours online, this course will provide an
                           overview of the Institutional Review Board (IRB) for all faculty members who are planning
                           to conduct research studies at Valencia College. The online portion of the course
                           will result in nationally-recognized certification from the National Institutes of
                           Health (NIH) on protecting human subjects. The online training is self-paced and required
                           for faculty members conducting research at Valencia College. It is recognized by most
                           institutions of higher education for three years after the date of completion.
                        </p>
                        
                        <p><strong>Part 1 </strong>(one hour, face-to-face)
                        </p>
                        
                        <p> No courses scheduled at this time. Please contact <a href="mailto:lblasi@valenciacollege.edu">Laura Blasi</a> for more information. 
                        </p>
                        
                        <p><strong>Part 2</strong> (3-hours, online)
                        </p>
                        
                        <p>No courses scheduled at this time. Please contact <a href="mailto:lblasi@valenciacollege.edu">Laura Blasi</a> for more information.
                           
                        </p>
                        
                        <h3>INDEPENDENT STUDY COURSES</h3>
                        
                        
                        <h4>SOTL3371 - Action Research: Project Design (20 PD Hours)</h4>
                        
                        <p>An independent study course where participants will design an <a href="./" target="_blank">Action Research Project</a> that articulates a research question, measures a learning outcome, and implements
                           innovative teaching strategies. <em>Note: Part 1 of a 2 part sequence. </em></p>
                        
                        <p>Successful completion of this independent study course is dependent upon submission
                           of the completed <a href="documents/ARPPlanTemplate.docx" target="_blank">Action Research Project Plan</a> (pages 1-4), approved by the faculty member's Dean or Director. This approval is
                           documented using the <a href="documents/IndependantStudyApprovalForm-SOTL3371-ActionResearchProjectDesign.pdf" target="_blank">SOTL 3371 Independent Study Approval Form</a>.
                        </p>
                        
                        <p> For more information, please contact <a href="mailto:wdew@valenciacollege.edu%20">Wendi Dew.</a></p>
                        
                        
                        <h4>SOTL3372 - Action Research: Project Implementation (20 PD Hours)</h4>
                        
                        <p>An independent study course where participants implement their <a href="./" target="_blank">Action Research Project</a>.&nbsp; Participants will record appropriate data, conduct analysis of that data, and reflect
                           on its meaning for the improvement of student learning.&nbsp; Results and reflective critique
                           are recorded in the Action Research Builder. <em>Note:&nbsp; Part 2 of a 2 part sequence.</em> 
                        </p>
                        
                        <p>Successful completion of this independent study course is dependent upon submission
                           of the fully completed <a href="documents/ARPPlanTemplate.docx" target="_blank">Action Research Project Plan</a>, approved by the faculty member's Dean or Director, and uploaded to Valencia's online
                           repository, the <a href="documents/ARPBuilderHowTO.pdf" target="_blank">Action Research Builder</a>. This approval is documented using the <a href="documents/IndependantStudyApprovalForm-SOTL3372-ActionResearchImplementation.pdf" target="_blank">SOTL 3372 Independent Study Approval Form</a>.
                        </p>
                        
                        <p> For more information, please contact <a href="mailto:wdew@valenciacollege.edu%20">Wendi Dew.</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="https://register.gotowebinar.com/register/3686033952182497025" target="_blank">Sign up for the Part-time Faculty Town Hall Webinar</a>
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>&nbsp;
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>&nbsp;
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>&nbsp;
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/courses.pcf">©</a>
      </div>
   </body>
</html>