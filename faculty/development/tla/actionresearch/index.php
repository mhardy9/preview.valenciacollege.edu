<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li>Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        					
                        					
                        <h2>About Action Research</h2>
                        
                        					
                        <blockquote><em>Action research can be described as a family of research methodologies which pursue
                              action (or change) and research (or understanding) at the same time. In most of its
                              forms it does this by using a cyclic or spiral process which alternates between action
                              and critical reflection and in the later cycles, continuously refining methods, data
                              and interpretation in the light of the understanding developed in the earlier cycles.
                              -- <a href="documents/Whatisactionresearch.pdf" target="_blank">Dick, Bob (1999)</a></em></blockquote>
                        					
                        <p>Action Research, a scholarly approach to improve teaching and learning, can be a central
                           piece of professional development. This project-based research can benefit professors,
                           counselors, and librarians by actively engaging them in the collaborative study of
                           learning as it takes place day by day in the context of their own practices. Through
                           this small-scale, practical research, faculty members can investigate questions regarding
                           student learning that directly impact their practices. At Valencia, action research
                           has become a significant aspect of faculty development, thereby reinforcing the College's
                           community of learners and culture of evidence. 
                        </p>
                        					
                        <p>We offer many opportunities to support faculty members who are interested in exploring
                           action research.  These resources and courses help faculty members develop a best
                           practices framework for action research and Valencia's Standards of Scholarship.
                        </p>
                        					
                        <h3>ACTION RESEARCH TUTORIAL</h3>
                        					
                        <ul class="list_style_1">
                           						
                           <li>
                              							<a href="ARP_softchalk/index.html" target="_blank"> Tutorial Web Site</a>
                              							
                              <p>This interactive tutorial walks you through the elements of action research and provides
                                 opportunities to self-assess your knowledge of the action research process.
                              </p>
                              						
                           </li>
                           						
                           <li>
                              							<a href="index.php#inline_content1">Action Research Builder Tutorial</a>
                              							
                              <p>This tutorial includes step-by-step instructions on loading your project to the Action
                                 Research Project Builder, which is located in the Faculty tab in Atlas. <br>
                                 								<em>Please use Internet Explorer 11, Chrome, or Firefox for the best experience. </em></p>
                              						
                           </li>
                           					
                        </ul>
                        
                        
                        					
                        <h3>RESOURCES</h3>
                        					
                        <ul>
                           						<span>
                              							
                              <li><a href="elements.php">Elements of an Action Research Project </a></li>
                              							
                              <li><a href="rubric.php">Action Research Project Rubric </a></li>
                              							
                              <li>
                                 								<a href="resources.php">Web Resources</a>                
                              </li>
                              							
                              <li><a href="courses.php">Courses to Support Action Research<br>
                                    								</a></li>
                              						</span>
                           						
                           <li>
                              							<strong>Model Action Research Projects <br>
                                 							</strong>The competencies addressed  in  the following AR projects were effective prior to
                              the 2016 update.
                           </li>
                           					
                        </ul>
                        						
                        <ul>
                           							
                           <li><a href="documents/CourtneyMoore_ARP.pdf" target="_blank">Model Librarian ARP: TLA Graduate, Courtney Moore</a></li>
                           							
                           <li><a href="documents/DerekSchorsch_ARP.pdf" target="_blank">Model Psychology ARP: TLA Graduate, Derek Schorsch </a></li>
                           							
                           <li><a href="documents/WandaStanek_ARP.pdf" target="_blank">Model Nursing ARP: TLA Graduate, Wanda Stanek </a></li>
                           							
                           <li><a href="documents/ZachHyde_ARP.pdf" target="_blank">Model English ARP: TLA Graduate, Zachary Hyde </a></li>
                           							
                           <li><a href="documents/DiegoDiaz_ARP.pdf" target="_blank">Model Chemistry ARP: TLA Graduate, Diego Diaz </a></li>
                           						
                        </ul>
                        					
                        <h3>Documents and Tools</h3>
                        					
                        <ul>
                           						
                           <li>
                              							<span><a href="http://valenciacollege.edu/faculty/development/tla/actionResearch/documents/ARPPlanTemplate.docx" target="_blank">Action Research Project Plan Template </a><br>
                                 							</span>This template is for faculty conducting action reserach outside the tenure process.
                              <br>
                              							Tenure candidates should refer to the portfolio template available under<em> Candidate Resourses</em>, <em>Year-3</em>. 
                           </li>
                           						
                           <li><a href="documents/TLAConsentForm.doc">Consent Form for Action Research Projects </a></li>
                           						
                           <li><a href="http://valenciacollege.qualtrics.com" target="_blank">Qualtrics (online survey tool)</a></li>
                           					
                        </ul>
                        
                        
                        				
                     </div>
                     				
                     <aside class="col-md-3">
                        
                        					
                        <div>
                           						<a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           					
                        </div>
                        
                        					
                        <div>
                           						
                           <h2>GET SOCIAL</h2>
                           						<a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           						<a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           						<a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           						<a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           					
                        </div>
                        
                        					
                        <p><a href="../../howToRegisterForCourses.php" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        					
                        <p><a href="../../CertificationPlanningDocuments.php" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        					
                        <p><a href="../../centers/locations.php" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        				
                     </aside>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/index.pcf">©</a>
      </div>
   </body>
</html>