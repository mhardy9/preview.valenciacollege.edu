<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Elements of Action Research | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk3.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Elements of Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		<a id="headingtaglink-1"></a><h1>Elements of an Action Research Project: Standards of Scholarship</h1>
               
               <p>&nbsp;</p>
               
               <ol>
                  
                  <li>Clear Goals</li>
                  
                  <li>Adequate Preparation</li>
                  
                  <li>Appropriate Methods</li>
                  
                  <li>Significant Results</li>
                  
                  <li>Reflective Critique</li>
                  
                  <li>Effective Presentation</li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <p>&nbsp; 
                  
               </p>
               
               <div class="hideqpstuff">
                  <a href="ARP_softchalk3.html#endofactivity5">
                     <img title="go to end of activity" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="width:0;height:0;border:0;" alt="spacer">
                     </a>
                  
               </div>
               
               
               <table class="table ">
                  
                  <tr>
                     
                     <td style="padding:0px;text-align:left;vertical-align:bottom;width:23px;"><a href="ada_files/ada_activity5.html" target="_blank"><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ada-access.png" style="padding-left:3px;width:20;height:20;border:0;" title="alternative accessible content" alt="alternative accessible content"></a></td>
                     
                     <td style="padding:0px 5px;">SELF ASSESSMENT ACTIVITY:  For each of the Elements of an Action Research Project,
                        select the correct components to include. Click the bottom right arrow to go to the
                        next set. Click the checkmark (available on the last set) to submit all your answers.
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td colspan="2" style="text-align:center;padding:0px;">
                        
                        <div id="activity-5" class="activityborder" style="width: 295px; height: 420px; border: 1px solid #3b274f; background-color: #eeeeee;">
                           
                           <div style="text-align:center;margin-top:200px;font-weight:bold;">This content requires JavaScript enabled.</div>
                           
                        </div>
                        
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <div id="endofactivity5"></div>
               &nbsp;
               
               
               
               
               <br>
               
               <p class="nav2"><a href="ARP_softchalk3.html#">return to top</a> | <a href="ARP_softchalk2.html">previous page</a> | <a href="ARP_softchalk4.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk3.pcf">©</a>
      </div>
   </body>
</html>