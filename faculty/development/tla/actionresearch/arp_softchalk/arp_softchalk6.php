<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Appropriate Methods | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk6.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Appropriate Methods</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="banner" role="banner">
                  
                  <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/banner_custom.png" alt="" style="border:0;" class="banner">
                  
                  
                  <h1>Action Research</h1>
                  
                  <h2>Scholarship of Teaching and Learning</h2>
                  
                  
               </div>
               
               
               
               <div id="nav" role="navigation">
                  
                  <div id="printall"><a href="ARP_softchalk_print.html" target="_blank">print all</a></div>
                  <a href="ARP_softchalk6.html#endofnav"><img title="skip navigation" alt="skip navigation" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0; float:left" width="0" height="0"></a>
                  
                  
                  <ul>
                     
                     <li>&nbsp;&nbsp;Page:</li>
                     
                     <li><a class="navnum" href="index.html" title="Purpose of Action Research">&nbsp;&nbsp;1&nbsp;&nbsp;</a></li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk2.html" title="Action Research versus Traditional Research">&nbsp;&nbsp;2&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk3.html" title="Elements of Action Research">&nbsp;&nbsp;3&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk4.html" title="Clear Goals">&nbsp;&nbsp;4&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk5.html" title="Adequate Preparation">&nbsp;&nbsp;5&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | &nbsp;&nbsp;6&nbsp;&nbsp;</li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk7.html" title="Significant Results">&nbsp;&nbsp;7&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk8.html" title="Effective Presentation">&nbsp;&nbsp;8&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk9.html" title="Effective Presentation">&nbsp;&nbsp;9&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk10.html" title="Page 10">&nbsp;&nbsp;10&nbsp;&nbsp;</a>
                        
                     </li>
                     
                  </ul>
                  
                  
               </div>
               
               
               <div id="contentWrap" class="clearfix">
                  
                  
                  <div id="sidebar" role="complementary">
                     
                     <div id="endofnav"></div>
                     
                     
                     <div class="feature">
                        <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/onthispage_custom.png" alt="On This Page sidebar">
                        
                        <ul>
                           
                           <li><a href="ARP_softchalk6.html#headingtaglink-1">Appropriate Methods</a></li>
                           
                           <li><a href="ARP_softchalk6.html#headingtaglink-2">Student Learning Outcomes</a></li>
                           
                           <li><a href="ARP_softchalk6.html#headingtaglink-3">Performance Indicators</a></li>
                           
                           <li><a href="ARP_softchalk6.html#headingtaglink-5">Teaching Strategies</a></li>
                           
                           <li><a href="ARP_softchalk6.html#headingtaglink-6">Assessment Plan</a></li>
                           
                           <li><a href="ARP_softchalk6.html#headingtaglink-7">AR Methodology Design</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  <div id="content" class="softchalkWrapper" role="main">
                     
                     
                     <a id="headingtaglink-1"></a><h1>Appropriate Methods</h1>
                     
                     <p>The appropriate methods should include student learning outcomes, performance indicators,
                        teaching strategies, the assessment plan, and the AR methodology design.
                     </p>
                     
                     <p>Methodologies should:</p>
                     
                     <ul>
                        
                        <li>be appropriate for achieving the FLO</li>
                        
                        <li>follow the rigors of the discipline</li>
                        
                        <li>have a clear description</li>
                        
                        <li>have an assessment plan that gauges the effectiveness of the FLO comprehensively</li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-2"></a><h2>Student Learning Outcomes</h2>
                     
                     <p>When considering methods for implementing the AR project, it is critical to articulate
                        the "Student Learning Outcomes" (SLO). A Student Learning Outcome states what a student
                        should understand and/or be able to do as a result of what she has learned in a course,
                        library orientation, counseling session. Consider the following questions:
                     </p>
                     
                     <ol>
                        
                        <li>What will my students know and be able to do better as a result of the intervention,
                           innovation, or strategy I employ here?
                        </li>
                        
                        <li>Does the SLO connect to or support the outcomes of a course or program?</li>
                        
                        <li>Does the SLO describe learning that is meaningful in a real-world context?</li>
                        
                     </ol>
                     
                     <p>Effective Student Learning Outcomes should be results-oriented, clearly understood
                        by colleagues and students, measurable, aligned with the Faculty Learning Outcome,
                        and critical to teaching and learning. Candidate may have multiple SLO's, if applicable.
                     </p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-3"></a><h2>Performance Indicators</h2>
                     
                     <p>With each Student Learning Outcome, Performance Indicators identify the small steps
                        students take to achieve the learning outcome. &nbsp; Performance Indicators are pre-determined
                        criteria, stated by you, that identify these steps. The following questions can help
                        you identify the performance indicators of the student learning outcomes for your
                        project. Answer the questions that are relevant to your project as a way to focus
                        your ideas:
                     </p>
                     
                     <ol>
                        
                        <li>What specific qualities or evidence will I observe in the students' work/performance/behavior
                           that will demonstrate to me they have achieved this competency or indicator?
                        </li>
                        
                        <li>What is the minimum level of performance I am willing to accept from a student to
                           say he or she has achieved the learning outcome(s)? (This is your criteria.)
                        </li>
                        
                        <li>What student core competencies and indicators (TVCA) are related to these outcomes?</li>
                        
                     </ol>
                     
                     <p>Performance Indicators of Student Learning Outcomes are the qualitative or quantitative
                        interim measurements demonstrating that the meaningful steps in student learning are
                        being taken to achieve mastery. In other words, Indicators identify the specific,
                        incremental traits or features of successful student mastery. Indicators should clearly
                        identify the incremental traits of mastery sequenced for optimum learning. If multiple
                        SLOs are identified, indicators should be listed for each SLO.
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp; 
                        
                     </p>
                     
                     <div class="hideqpstuff"><a href="ARP_softchalk6.html#endofactivity16"><img title="go to end of activity" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="width:0;height:0;border:0;" alt="spacer"></a></div>
                     
                     
                     <table class="table ">
                        
                        <tr>
                           
                           <td style="padding:0px;text-align:left;vertical-align:bottom;width:23px;"><a href="ada_files/ada_activity16.html" target="_blank"><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ada-access.png" style="padding-left:3px;width:20;height:20;border:0;" title="alternative accessible content" alt="alternative accessible content"></a></td>
                           
                           <td style="padding:0px 5px;">Click below the question to view the answer.</td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td colspan="2" style="text-align:center;padding:0px;">
                              
                              <div id="activity-16" class="activityborder" style="width: 800px; height: 200px; border: 1px solid #3b274f; background-color: #eeeeee;">
                                 
                                 <div style="text-align:center;margin-top:200px;font-weight:bold;">This content requires JavaScript enabled.</div>
                                 
                              </div>
                              
                              
                           </td>
                           
                        </tr>
                        
                     </table>
                     
                     <div id="endofactivity16"></div>
                     &nbsp;
                     
                     <h2>&nbsp;</h2>
                     <a id="headingtaglink-5"></a><h2>Teaching Strategies</h2>
                     
                     <p>Teaching strategies are the methods, strategies, and/or techniques to support student
                        mastery of the student learning outcomes you identified in your project. They should
                        be appropriate for achieving the SLO, should follow the rigors of the discipline,
                        and should be clearly explained step by step. Include reference to relevant artifacts.
                        The teaching strategies should be a step by step plan for the project (not for each
                        SLO).
                     </p>
                     
                     <p>The following questions can help you decide the methods, strategies, and/or techniques
                        to support student mastery of the student learning outcomes you identified in your
                        project. Answer the questions that are relevant to your project as a way to focus
                        your ideas.
                     </p>
                     
                     <ol>
                        
                        <li>What are my learning activities? Will these activities prepare my students for mastery?
                           What are the processes for taking students from beginning to end?
                        </li>
                        
                        <li>How can I establish an inclusive and safe learning environment for my students during
                           this process?
                        </li>
                        
                        <li>How will my students make connections with the content, each other, and the instructor,
                           counselor, or librarian?
                        </li>
                        
                        <li>How am I going to keep records of the processes (student learning, teaching strategies,
                           etc.) for my action research project? &nbsp; For example, written methods: personal journal
                           or diary, field notes, surveys, questionnaires? &nbsp; Live methods: Interviews, role play,
                           video or audio tape? Include reference to relevant artifacts.
                        </li>
                        
                     </ol>
                     
                     <p>In your portfolio you should refer to artifacts relevant in your teaching strategies,
                        such as rubrics, surveys, or activities you created. &nbsp; However, you would not include
                        the results from students.
                     </p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-6"></a><h2>Assessment Plan</h2>
                     
                     <p>Assessments should include a comprehensive set of formative and summative assessment
                        tools that adequately measure the SLO performance indicators. Include reference to
                        relevant artifacts.
                     </p>
                     
                     <ol>
                        
                        <li>How will I measure the performance indicators described in the SLOs section?</li>
                        
                        <li>What tool(s) am I going to use to measure/gauge how my students perform in relation
                           to the indicators in the SLOs? (papers, quizzes, rubrics, etc.)
                        </li>
                        
                        <li>Are there tools I can use that will give the students formative feedback (prior to
                           receiving summative feedback)?
                        </li>
                        
                        <li>How will my students know the standards or criteria their work will be evaluated against?</li>
                        
                        <li>When choosing an assessment technique, I should ask myself:</li>
                        
                     </ol>
                     
                     <ul>
                        
                        <li>Is the assessment technique chosen appropriate to my goal?</li>
                        
                        <li>Can I integrate the assessment technique into my activities?</li>
                        
                        <li>Will it contribute to learning?</li>
                        
                     </ul>
                     
                     
                     <li>When applying an assessment technique, I should ask myself:</li>
                     
                     <ul>
                        
                        <li>Have I tried it?</li>
                        
                        <li>Have I done a run-through with a colleague?</li>
                        
                        <li>How will I make its purpose/process clear to students?</li>
                        
                        <li>How will I provide the necessary practice for students?</li>
                        
                        <li>Have I allowed enough time to apply the technique?</li>
                        
                     </ul>
                     
                     
                     
                     <p>In your portfolio you should refer to artifacts relevant in your teaching strategies,
                        such as rubrics, surveys, or activities you created. &nbsp;However, you would not include
                        the results from students.
                     </p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-7"></a><h2>AR Methodology Design</h2>
                     
                     <p>Action Research is a scholarly inquiry to improve teaching and student learning. You
                        have already described your plan's student learning outcomes, performance indicators,
                        strategies, and assessment methods of the student learning outcome. Now, you will
                        design a plan to assess the effectiveness of the innovative teaching and assessment
                        methods you implemented in your project. Remember, action research is about what you
                        are doing, not what other people are doing.
                     </p>
                     
                     <p>The following questions can help you decide the most effective methods to measure
                        the usefulness of your innovations. Answer the questions that are relevant to your
                        project as a way to focus your ideas.
                     </p>
                     
                     <ol>
                        
                        <li>How will I know whether or not (to what degree) my innovations have worked?</li>
                        
                        <li>Have I planned how I will analyze the data?</li>
                        
                        <li>Have I collect a reasonable amount of data?</li>
                        
                        <li>Is my process of analysis manageable?</li>
                        
                        <li>Have I planned adequate time to do the analysis?</li>
                        
                     </ol>
                     
                     <p>Consider the validity of your results: &nbsp; What kind of evidence will I produce to judge
                        the value of my innovation(s) incorporated in my action research project? For example,
                     </p>
                     
                     <ul>
                        
                        <li>If applicable, compare the results of a base class and pilot class</li>
                        
                        <li>If applicable, compare the results of a base assignment and pilot assignment</li>
                        
                        <li>If applicable, compare the results of a base class with aggregate departmental data</li>
                        
                     </ul>
                     
                     <p>Consider the reliability of your results:</p>
                     
                     <ul>
                        
                        <li>Will the statistics be the same if another researcher replicates your project?</li>
                        
                     </ul>
                     
                     <ul>
                        
                        <li>If you replicate the project in other classes, how will you demonstrate reliability
                           and/or effectively describe your project for replication?&nbsp;
                        </li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     <p>In your portfolio you should refer to artifacts relevant in your methodology design,
                        such as rubrics, surveys, or activities you created. &nbsp; However, you would not include
                        the results from students.
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     
                     
                     
                     <br>
                     
                     <p class="nav2"><a href="ARP_softchalk6.html#">return to top</a> | <a href="ARP_softchalk5.html">previous page</a> | <a href="ARP_softchalk7.html">next page</a></p>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               <div id="footer" role="contentinfo">
                  
                  <p>
                     Content ©2016. All Rights Reserved.<br>
                     Date last modified: August 16, 2016.<br>
                     Created with <a target="_blank" href="http://www.softchalk.com">SoftChalk LessonBuilder</a><br>
                     <a class="textpopper" href="javascript:void(0);" onmouseover="return overlib('&amp;lt;div style=\'text-align:center;\'&amp;gt;SoftChalk 10.01.05&amp;lt;/div&amp;gt;', PADX, 3, 3, PADY, 3, 3, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 70, TEXTSIZE, 1);" onmouseout="nd();"><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0;" width="200" height="6" alt="hidden textpopper"></a></p>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk6.pcf">©</a>
      </div>
   </body>
</html>