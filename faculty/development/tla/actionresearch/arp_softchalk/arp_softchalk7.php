<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Significant Results | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Significant Results</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		<a id="headingtaglink-1"></a><h1>Significant Results</h1>
               
               <p><img alt="methods.png" height="213" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/methods.png" class="resizable" width="246" metadata="obj5" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"> The significant results provide primary evidence and analysis to justify your answer
                  to the research question. The results should be:
               </p>
               
               <ul>
                  
                  <li>sufficient to demonstrate your FLO</li>
                  
                  <li>clearly and insightfully explained</li>
                  
                  <li>includes student feedback/work, if applicable</li>
                  
                  <li>opens additional questions for further exploration</li>
                  
               </ul>
               
               <p>Project results should be analyzed in relation to the original FLO and research question,
                  and explanation of the evidence and supporting artifacts should demonstrate that the
                  candidate has learned, achieved, or accomplished the FLO. Explanation should include
                  description of how project results will inform your practice and impact student learning.
                  Evidence and supporting artifacts should be sufficient to demonstrate achievement
                  of the FLO. You should use an effective mix of text and graphics to clearly present
                  and explain the data/findings/results. Student work/feedback, if applicable, should
                  document the achievement of the goals of the FLO. Remove students' names from all
                  student work.
               </p>
               
               <p>The following prompts can help you explain the findings of your Action Research Project.
                  Answer the questions that are relevant to your project as a way to focus your ideas.
               </p>
               
               <ol>
                  
                  <li>How shall I describe the results of my project? (tables, graphs, narratives)</li>
                  
                  <li>How well have I labeled and explained my graphs/charts?</li>
                  
                  <li>How well did my students accomplish the SLOs?</li>
                  
                  <li>Did the methods remedy the problem in terms of changes in knowledge, abilities, attitudes,
                     and commitment of students and/or of myself?
                  </li>
                  
                  <li>Were the results what I expected?</li>
                  
               </ol>
               
               <p>In your portfolio you should refer to artifacts that document the results of your
                  research question.
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><a href="ARP_softchalk7.html#">return to top</a> | <a href="ARP_softchalk6.html">previous page</a> | <a href="ARP_softchalk8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk7.pcf">©</a>
      </div>
   </body>
</html>