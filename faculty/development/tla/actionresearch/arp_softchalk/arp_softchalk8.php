<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Effective Presentation | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk8.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Effective Presentation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="banner" role="banner">
                  
                  <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/banner_custom.png" alt="" style="border:0;" class="banner">
                  
                  
                  <h1>Action Research</h1>
                  
                  <h2>Scholarship of Teaching and Learning</h2>
                  
                  
               </div>
               
               
               
               <div id="nav" role="navigation">
                  
                  <div id="printall"><a href="ARP_softchalk_print.html" target="_blank">print all</a></div>
                  <a href="ARP_softchalk8.html#endofnav"><img title="skip navigation" alt="skip navigation" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0; float:left" width="0" height="0"></a>
                  
                  
                  <ul>
                     
                     <li>&nbsp;&nbsp;Page:</li>
                     
                     <li><a class="navnum" href="index.html" title="Purpose of Action Research">&nbsp;&nbsp;1&nbsp;&nbsp;</a></li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk2.html" title="Action Research versus Traditional Research">&nbsp;&nbsp;2&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk3.html" title="Elements of Action Research">&nbsp;&nbsp;3&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk4.html" title="Clear Goals">&nbsp;&nbsp;4&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk5.html" title="Adequate Preparation">&nbsp;&nbsp;5&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk6.html" title="Appropriate Methods">&nbsp;&nbsp;6&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk7.html" title="Significant Results">&nbsp;&nbsp;7&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | &nbsp;&nbsp;8&nbsp;&nbsp;</li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk9.html" title="Effective Presentation">&nbsp;&nbsp;9&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk10.html" title="Page 10">&nbsp;&nbsp;10&nbsp;&nbsp;</a>
                        
                     </li>
                     
                  </ul>
                  
                  
               </div>
               
               
               <div id="contentWrap" class="clearfix">
                  
                  
                  <div id="sidebar" role="complementary">
                     
                     <div id="endofnav"></div>
                     
                     
                     <div class="feature">
                        <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/onthispage_custom.png" alt="On This Page sidebar">
                        
                        <ul>
                           
                           <li><a href="ARP_softchalk8.html#headingtaglink-1">Reflective Critique</a></li>
                           
                           <li><a href="ARP_softchalk8.html#headingtaglink-2">General AR Project Reflection</a></li>
                           
                           <li><a href="ARP_softchalk8.html#headingtaglink-3">Essential Competencies of a Valencia Educator Reflection</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  <div id="content" class="softchalkWrapper" role="main">
                     
                     
                     <a id="headingtaglink-1"></a><h1>Reflective Critique</h1>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-2"></a><h2>General AR Project Reflection</h2>
                     
                     <p>In general, you should reflect on what was learned while completing the Action Research
                        Project and how this might improve future work. The reflection relates your project
                        to student learning and plans for revision of teaching practice in light of results
                        should be clearly stated.
                     </p>
                     
                     <ol>
                        
                        <li>Given my results, how will I use this information to improve student learning in the
                           future?&nbsp;To improve my practice?
                        </li>
                        
                        <li>What additional questions arose that I might want to pursue? How might I change my
                           research question in light of the results?
                        </li>
                        
                        <li>What other innovation might I try next time, based on these results?</li>
                        
                     </ol>
                     
                     <p>&nbsp;</p>
                     
                     <p><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/essentialCompetencies_new2016_sm.jpg" width="450" height="453" class="resizable" metadata="obj1" alt="essentialCompetencies.jpg" style="margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px; float: right; border: 0"></p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-3"></a><h2>Essential Competencies of a Valencia Educator Reflection</h2>
                     
                     <p>&nbsp;</p>
                     
                     <p>For each essential competency, reflect on:</p>
                     
                     <ul>
                        
                        <li>Explanation of how the methods used to demonstrate this Essential Competency in this
                           FLO aided student learning and/or helped the candidate to become a better counselor,
                           teacher, or librarian
                        </li>
                        
                        <li>Explanation of how the methods used in demonstrating this Essential Competency might
                           be improved
                        </li>
                        
                        <li>References to candidate's general practice outside the portfolio should be omitted</li>
                        
                        <li>Approximately 1/2 page to 1 page for each specified Essential Competency</li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     <p>Reflect on the <a href="../../Candidate/tla_competencies_LCF.html" target="_blank">Essential Competencies of a Valencia Educator</a> addressed in the AR Project.
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p><strong>&nbsp;</strong></p>
                     
                     <p>&nbsp;</p>
                     
                     
                     
                     
                     <br>
                     
                     <p class="nav2"><a href="ARP_softchalk8.html#">return to top</a> | <a href="ARP_softchalk7.html">previous page</a> | <a href="ARP_softchalk9.html">next page</a></p>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               <div id="footer" role="contentinfo">
                  
                  <p>
                     Content ©2016. All Rights Reserved.<br>
                     Date last modified: August 16, 2016.<br>
                     Created with <a target="_blank" href="http://www.softchalk.com">SoftChalk LessonBuilder</a><br>
                     <a class="textpopper" href="javascript:void(0);" onmouseover="return overlib('&amp;lt;div style=\'text-align:center;\'&amp;gt;SoftChalk 10.01.05&amp;lt;/div&amp;gt;', PADX, 3, 3, PADY, 3, 3, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 70, TEXTSIZE, 1);" onmouseout="nd();"><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0;" width="200" height="6" alt="hidden textpopper"></a></p>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk8.pcf">©</a>
      </div>
   </body>
</html>