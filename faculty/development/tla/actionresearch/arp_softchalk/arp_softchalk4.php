<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Clear Goals | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Clear Goals</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="banner" role="banner">
                  
                  <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/banner_custom.png" alt="" style="border:0;" class="banner">
                  
                  
                  <h1>Action Research</h1>
                  
                  <h2>Scholarship of Teaching and Learning</h2>
                  
                  
               </div>
               
               
               
               <div id="nav" role="navigation">
                  
                  <div id="printall"><a href="ARP_softchalk_print.html" target="_blank">print all</a></div>
                  <a href="ARP_softchalk4.html#endofnav"><img title="skip navigation" alt="skip navigation" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0; float:left" width="0" height="0"></a>
                  
                  
                  <ul>
                     
                     <li>&nbsp;&nbsp;Page:</li>
                     
                     <li><a class="navnum" href="index.html" title="Purpose of Action Research">&nbsp;&nbsp;1&nbsp;&nbsp;</a></li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk2.html" title="Action Research versus Traditional Research">&nbsp;&nbsp;2&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk3.html" title="Elements of Action Research">&nbsp;&nbsp;3&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | &nbsp;&nbsp;4&nbsp;&nbsp;</li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk5.html" title="Adequate Preparation">&nbsp;&nbsp;5&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk6.html" title="Appropriate Methods">&nbsp;&nbsp;6&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk7.html" title="Significant Results">&nbsp;&nbsp;7&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk8.html" title="Effective Presentation">&nbsp;&nbsp;8&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk9.html" title="Effective Presentation">&nbsp;&nbsp;9&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk10.html" title="Page 10">&nbsp;&nbsp;10&nbsp;&nbsp;</a>
                        
                     </li>
                     
                  </ul>
                  
                  
               </div>
               
               
               <div id="contentWrap" class="clearfix">
                  
                  
                  <div id="sidebar" role="complementary">
                     
                     <div id="endofnav"></div>
                     
                     
                     <div class="feature">
                        <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/onthispage_custom.png" alt="On This Page sidebar">
                        
                        <ul>
                           
                           <li><a href="ARP_softchalk4.html#headingtaglink-1">Clear Goals</a></li>
                           
                           <li><a href="ARP_softchalk4.html#headingtaglink-2">Abstract</a></li>
                           
                           <li><a href="ARP_softchalk4.html#headingtaglink-3">Action Research Question</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  <div id="content" class="softchalkWrapper" role="main">
                     
                     
                     <a id="headingtaglink-1"></a><h1>Clear Goals</h1>
                     
                     <p>&nbsp;The components of the Clear Goals section of an AR Project include:</p>
                     
                     <ol type="a">
                        
                        <li>Abstract of the study</li>
                        
                        <li>Action Research Question</li>
                        
                     </ol>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-2"></a><h2><u>Abstract</u></h2>
                     
                     <p>Your abstract should concisely summarize and highlight the primary points of your
                        Action Research Project. The purpose is to help colleagues quickly decide if your
                        project can support their own project or practice. The abstract should be concise
                        and include <u>purpose</u>, <u>methods</u>, and <u>results</u> of your project. An abstract should not be longer than ½ page or 125 words
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp; 
                        
                     </p>
                     
                     <div class="hideqpstuff"><a href="ARP_softchalk4.html#endofactivity10"><img title="go to end of activity" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="width:0;height:0;border:0;" alt="spacer"></a></div>
                     
                     
                     <table class="table ">
                        
                        <tr>
                           
                           <td style="padding:0px;text-align:left;vertical-align:bottom;width:23px;"><a href="ada_files/ada_activity10.html" target="_blank"><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ada-access.png" style="padding-left:3px;width:20;height:20;border:0;" title="alternative accessible content" alt="alternative accessible content"></a></td>
                           
                           <td style="padding:0px 5px;">Roll over the tabs to view more information.</td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td colspan="2" style="text-align:center;padding:0px;">
                              
                              <div id="activity-10" class="activityborder" style="width: 640px; height: 300px; border: 1px solid #3b274f; background-color: #eeeeee;">
                                 
                                 <div style="text-align:center;margin-top:200px;font-weight:bold;">This content requires JavaScript enabled.</div>
                                 
                              </div>
                              
                              
                           </td>
                           
                        </tr>
                        
                     </table>
                     
                     <div id="endofactivity10"></div>
                     &nbsp;
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-3"></a><h2><u>Action Research Question</u></h2>
                     <em>The research question must be included here and should be polished and clearly aligned
                        with the FLO. &nbsp; You may have more than one research question. The following prompts
                        can help you identify the question that will be the basis for your Action Research
                        Project. Answer the questions that are relevant to your project as a way to focus
                        your ideas. Remember, action research questions are about what you are doing, not
                        what other people are doing. &nbsp;</em> 
                     
                     <p>&nbsp;</p>
                     
                     <p>As with your Year 1 ILP, the Research Question should</p>
                     
                     <ul>
                        
                        <li>be clearly connected to the candidate's FLO.</li>
                        
                        <li>be significant and related to improving student learning.</li>
                        
                        <li>have methods that are under the candidate's control.</li>
                        
                        <li>be feasible in terms of time, effort and available resources.</li>
                        
                        <li>be clearly stated.</li>
                        
                        <li>address no more than one result/trait.</li>
                        
                        <li>be answered by the data that will be collected.</li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     
                     
                     
                     <br>
                     
                     <p class="nav2"><a href="ARP_softchalk4.html#">return to top</a> | <a href="ARP_softchalk3.html">previous page</a> | <a href="ARP_softchalk5.html">next page</a></p>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               <div id="footer" role="contentinfo">
                  
                  <p>
                     Content ©2016. All Rights Reserved.<br>
                     Date last modified: August 16, 2016.<br>
                     Created with <a target="_blank" href="http://www.softchalk.com">SoftChalk LessonBuilder</a><br>
                     <a class="textpopper" href="javascript:void(0);" onmouseover="return overlib('&amp;lt;div style=\'text-align:center;\'&amp;gt;SoftChalk 10.01.05&amp;lt;/div&amp;gt;', PADX, 3, 3, PADY, 3, 3, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 70, TEXTSIZE, 1);" onmouseout="nd();"><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0;" width="200" height="6" alt="hidden textpopper"></a></p>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk4.pcf">©</a>
      </div>
   </body>
</html>