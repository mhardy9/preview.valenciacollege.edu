<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>How to Use the Action Research Builder | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk10.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>How to Use the Action Research Builder</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <h1>&nbsp;</h1>
               <a id="headingtaglink-2"></a><h1>
                  <strong>How to Use the Action Research Builder</strong>
                  
               </h1>
               
               <p>It is recommended to enter your Action Research Project in the builder when the candidate
                  is completely finished with his/her project (when ready for final submission). As
                  you work on your ARP and get feedback from your panel, changes are inevitable. It
                  is best to wait until all changes have been made before entering it in the builder.
                  If you start earlier, you will need to go back into the builder and copy/paste any
                  changes.
               </p>
               
               <p><strong>Step 1</strong> Log into Atlas.
               </p>
               
               <p><strong>Step 2</strong> Click on the Faculty tab.
               </p>
               
               <p><strong>Step 3</strong> Scroll down to the section "Faculty Development" (bottom middle). Click on the Action
                  Research Project Builder link.
               </p>
               
               <p>
                  <img alt="ARP_B_Atlas_link.jpg" height="407" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_Atlas_link.jpg" class="resizable" width="450" metadata="obj9" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 4</strong> Click on the "start a new project" link.
               </p>
               
               <p>
                  <img alt="ARP_B_startProject.jpg" height="296" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_startProject.jpg" class="resizable" width="450" metadata="obj10" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 5</strong> Fill out the information on the Research Project page. On the left side, there is
                  a link with an example to demonstrate how the form is filled out.
               </p>
               
               <p>
                  <span style="background-color: #ffff66">Under the "Program/Initiative Supporting Project, all candidates in the tenure process
                     working on their FLO #1 Action Research Project should select Teaching/Learning Academy
                     (TLA).</span>
                  
               </p>
               
               <p>
                  <img alt="ARP_B_form.jpg" height="330" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_form.jpg" class="resizable" width="450" metadata="obj11" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 6</strong> Click "Launch Project".
               </p>
               
               <p><strong>Step 7</strong> From the upper menu, you will go through each of the menu items and copy/paste the
                  information from your completed project. Remember, there is an explanation and example
                  on each page you enter information. Each tab/link in the menu bar correlates with
                  a section of your Action Research Project. Note that in some sections (such as Adequate
                  Preparation and Methods/Assessments), there is a drop down to select the subcategories.
                  Make sure you fill out each section. On each page, you have the opportunity to upload
                  documents or graphics. Make sure you save your file frequently. Atlas times out fairly
                  frequently, and you will lose any data you did not save if it times out prior to saving.
               </p>
               
               <p>
                  <img alt="ARP_B_save.jpg" height="393" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_save.jpg" class="resizable" width="450" metadata="obj13" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 8</strong> When the project has been completely entered, and checked for accuracy, you have
                  one last step to publish the file for dissemination. <span style="background-color: #ffff66">On the "Project" page at the bottom you must select YES to the "Completed" (in red)
                     section. Once you select YES, the project will be visible to your committee to show
                     you have made your work public (required for Scholarship of Teaching and Learning).</span></p>
               
               <p>
                  <img alt="ARP_B_completed.jpg" height="373" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_completed.jpg" class="resizable" width="450" metadata="obj15" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><a href="ARP_softchalk10.html#">return to top</a> | <a href="ARP_softchalk9.html">previous page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk10.pcf">©</a>
      </div>
   </body>
</html>