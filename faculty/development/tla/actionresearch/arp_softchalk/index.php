<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Purpose of Action Research | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Arp Softchalk</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="banner" role="banner">
                  
                  <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/banner_custom.png" alt="" style="border:0;" class="banner">
                  
                  
                  <h1>Action Research</h1>
                  
                  <h2>Scholarship of Teaching and Learning</h2>
                  
                  
               </div>
               
               
               
               <div id="nav" role="navigation">
                  
                  <div id="printall"><a href="ARP_softchalk_print.html" target="_blank">print all</a></div>
                  <a href="index.html#endofnav"><img title="skip navigation" alt="skip navigation" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0; float:left" width="0" height="0"></a>
                  
                  
                  <ul>
                     
                     <li>&nbsp;&nbsp;Page:</li>
                     
                     <li>&nbsp;&nbsp;1&nbsp;&nbsp;</li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk2.html" title="Action Research versus Traditional Research">&nbsp;&nbsp;2&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk3.html" title="Elements of Action Research">&nbsp;&nbsp;3&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk4.html" title="Clear Goals">&nbsp;&nbsp;4&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk5.html" title="Adequate Preparation">&nbsp;&nbsp;5&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk6.html" title="Appropriate Methods">&nbsp;&nbsp;6&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk7.html" title="Significant Results">&nbsp;&nbsp;7&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk8.html" title="Effective Presentation">&nbsp;&nbsp;8&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk9.html" title="Effective Presentation">&nbsp;&nbsp;9&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="ARP_softchalk10.html" title="Page 10">&nbsp;&nbsp;10&nbsp;&nbsp;</a>
                        
                     </li>
                     
                  </ul>
                  
                  
               </div>
               
               
               <div id="contentWrap" class="clearfix">
                  
                  
                  <div id="sidebar" role="complementary">
                     
                     <div id="endofnav"></div>
                     
                     
                     <div class="feature">
                        <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/onthispage_custom.png" alt="On This Page sidebar">
                        
                        <ul>
                           
                           <li><a href="index.html#headingtaglink-1">Purpose of Action Research</a></li>
                           
                           <li><a href="index.html#headingtaglink-2">Action Research: A Definition</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  <div id="content" class="softchalkWrapper" role="main">
                     
                     
                     <a id="headingtaglink-1"></a><h1>Purpose of Action Research</h1>
                     <a id="headingtaglink-2"></a><h2>Action Research: A Definition</h2>
                     
                     <p>Action Research, a scholarly approach to improve teaching and learning, can be a central
                        piece of professional development. This project-based research can benefit professors,
                        counselors, and librarians by actively engaging them in the collaborative study of
                        learning as it takes place day by day in the context of their own practices. Through
                        this small-scale, practical research, faculty members can investigate questions regarding
                        student learning that directly impact their practices. As part of the portfolio in
                        the tenure process, Action Research demonstrates how some of the essential competencies
                        are evident in your practice.
                     </p>
                     
                     <p>Action Research (AR) attempts to provide some insight into how students learn. AR
                        encourages faculty members and professionals to use their classrooms, offices and
                        libraries as laboratories for the study of learning.
                     </p>
                     
                     <table class="table nonstyledtable">
                        
                        <tr>
                           
                           <td style="width: 347px">
                              
                              <p style="margin-left: 20.0px">Action research is a deliberate, solution-oriented investigation, characterized by
                                 <u>spiraling cycles</u> of:
                              </p>
                              
                              <ol>
                                 
                                 <li>problem identification &nbsp;</li>
                                 
                                 <li>data collection</li>
                                 
                                 <li>analysis</li>
                                 
                                 <li>reflection</li>
                                 
                                 <li>action</li>
                                 
                                 <li>redefinition</li>
                                 
                              </ol>
                              
                              <p style="margin-left: 20.0px">It is a process that continues throughout your career. Even as you report results
                                 and reflect on the process in your portfolio, you will continue to evaluate and improve
                                 upon the work you started with your Action Research.
                              </p>
                              
                           </td>
                           
                           <td style="width: 230px">
                              
                              <p><img alt="AR_spiral_new.jpg" height="400" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/AR_spiral_new.jpg" class="resizable" width="268" metadata="obj2" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"></p>
                              
                              <p>&nbsp;</p>
                              
                           </td>
                           
                        </tr>
                        
                     </table>
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp;The PDF document below illustrates how the cycles of reflect, plan, act, and observe
                        work in sequence with your Action Research Project.
                     </p>
                     
                     <p>&nbsp;<span id="inlinemedia4">
                           <a href="index.html#endofinline4"><img title="go to end of inline object" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0;" width="0" height="0"></a>
                           <iframe src="spiral_handout_Rev.pdf" width="800" height="400" style="border:1px solid black;" scrolling="auto"></iframe>
                           
                           <div id="endofinline4"></div>
                           </span>
                        
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     
                     
                     
                     <br>
                     
                     <p class="nav2"><a href="index.html#">return to top</a> | <a href="ARP_softchalk2.html">next page</a></p>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               <div id="footer" role="contentinfo">
                  
                  <p>
                     Content ©2016. All Rights Reserved.<br>
                     Date last modified: August 16, 2016.<br>
                     Created with <a target="_blank" href="http://www.softchalk.com">SoftChalk LessonBuilder</a><br>
                     <a class="textpopper" href="javascript:void(0);" onmouseover="return overlib('&amp;lt;div style=\'text-align:center;\'&amp;gt;SoftChalk 10.01.05&amp;lt;/div&amp;gt;', PADX, 3, 3, PADY, 3, 3, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 70, TEXTSIZE, 1);" onmouseout="nd();"><img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/spacer.gif" style="border:0;" width="200" height="6" alt="hidden textpopper"></a></p>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/index.pcf">©</a>
      </div>
   </body>
</html>