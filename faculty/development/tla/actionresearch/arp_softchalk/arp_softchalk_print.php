<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Action Research</strong><br>Scholarship of Teaching and Learning
               </p>
               
               
               <h1>Purpose of Action Research</h1>
               
               <h2>Action Research: A Definition</h2>
               
               <p>Action Research, a scholarly approach to improve teaching and learning, can be a central
                  piece of professional development. This project-based research can benefit professors,
                  counselors, and librarians by actively engaging them in the collaborative study of
                  learning as it takes place day by day in the context of their own practices. Through
                  this small-scale, practical research, faculty members can investigate questions regarding
                  student learning that directly impact their practices. As part of the portfolio in
                  the tenure process, Action Research demonstrates how some of the essential competencies
                  are evident in your practice.
               </p>
               
               <p>Action Research (AR) attempts to provide some insight into how students learn. AR
                  encourages faculty members and professionals to use their classrooms, offices and
                  libraries as laboratories for the study of learning.
               </p>
               
               <table class="table nonstyledtable">
                  
                  <tr>
                     
                     <td style="width: 347px">
                        
                        <p style="margin-left: 20.0px">Action research is a deliberate, solution-oriented investigation, characterized by
                           <u>spiraling cycles</u> of:
                        </p>
                        
                        <ol>
                           
                           <li>problem identification &nbsp;</li>
                           
                           <li>data collection</li>
                           
                           <li>analysis</li>
                           
                           <li>reflection</li>
                           
                           <li>action</li>
                           
                           <li>redefinition</li>
                           
                        </ol>
                        
                        <p style="margin-left: 20.0px">It is a process that continues throughout your career. Even as you report results
                           and reflect on the process in your portfolio, you will continue to evaluate and improve
                           upon the work you started with your Action Research.
                        </p>
                        
                     </td>
                     
                     <td style="width: 230px">
                        
                        <p><img alt="AR_spiral_new.jpg" height="400" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/AR_spiral_new.jpg" class="resizable" width="268" metadata="obj2" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"></p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;The PDF document below illustrates how the cycles of reflect, plan, act, and observe
                  work in sequence with your Action Research Project.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Action Research differs from traditional research in two important way</h1>
               
               <p>&nbsp;</p>
               
               <ol>
                  
                  <li>It is not an add-on activity. It is embedded in the regular ongoing work of the class.</li>
                  
                  <li>It makes the complete cycle from formulating relevant questions to making changes
                     in the practice of teaching.
                  </li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <table class="table bannerstyletable">
                  
                  <tr>
                     
                     <th scope="col" style="width: 106px; vertical-align: top"></th>
                     
                     <th scope="col" style="width: 316px; vertical-align: top">
                        
                        <p><strong>Traditional Research</strong></p>
                        
                     </th>
                     
                     <th scope="col" style="width: 340px; vertical-align: top">
                        
                        <p><strong>Action Research</strong></p>
                        
                     </th>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Purpose</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>To draw conclusions. Focus is on advancing knowledge in the field. Insights may be
                           generalized to other settings.
                        </p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>To make decisions. Focus is on the improvement of educational practice. Limited generalizability.</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Context</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>Theory: Hypotheses/research questions derive from more general theoretical propositions.</p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>Practice: Research questions derive from practice. Theory plays secondary role.</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Data Analysis</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>Rigorous statistical analysis.</p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>Focus on practical, not statistical significance.</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Sampling</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>Random or representative sample.</p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>Students with whom they work.</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>
                  <em>Adapted from: Mc Millan, J. H. &amp; Wergin. J. F. (1998). "Understanding and Evaluating
                     Educational Research."</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Elements of an Action Research Project: Standards of Scholarship</h1>
               
               <p>&nbsp;</p>
               
               <ol>
                  
                  <li>Clear Goals</li>
                  
                  <li>Adequate Preparation</li>
                  
                  <li>Appropriate Methods</li>
                  
                  <li>Significant Results</li>
                  
                  <li>Reflective Critique</li>
                  
                  <li>Effective Presentation</li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Clear Goals</h1>
               
               <p>&nbsp;The components of the Clear Goals section of an AR Project include:</p>
               
               <ol type="a">
                  
                  <li>Abstract of the study</li>
                  
                  <li>Action Research Question</li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <h2>
                  <u>Abstract</u>
                  
               </h2>
               
               <p>Your abstract should concisely summarize and highlight the primary points of your
                  Action Research Project. The purpose is to help colleagues quickly decide if your
                  project can support their own project or practice. The abstract should be concise
                  and include <u>purpose</u>, <u>methods</u>, and <u>results</u> of your project. An abstract should not be longer than ½ page or 125 words
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <h2>
                  <u>Action Research Question</u>
                  
               </h2>
               <em>The research question must be included here and should be polished and clearly aligned
                  with the FLO. &nbsp; You may have more than one research question. The following prompts
                  can help you identify the question that will be the basis for your Action Research
                  Project. Answer the questions that are relevant to your project as a way to focus
                  your ideas. Remember, action research questions are about what you are doing, not
                  what other people are doing. &nbsp;</em> 
               
               <p>&nbsp;</p>
               
               <p>As with your Year 1 ILP, the Research Question should</p>
               
               <ul>
                  
                  <li>be clearly connected to the candidate's FLO.</li>
                  
                  <li>be significant and related to improving student learning.</li>
                  
                  <li>have methods that are under the candidate's control.</li>
                  
                  <li>be feasible in terms of time, effort and available resources.</li>
                  
                  <li>be clearly stated.</li>
                  
                  <li>address no more than one result/trait.</li>
                  
                  <li>be answered by the data that will be collected.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Adequate Preparation</h1>
               
               <p>As part of adequate preparation for Action Research, the researcher must consider
                  the work from multiple perspectives. As you assemble your portfolio, you will include
                  your own reflection (Self-perspective) and consultation with others (Student, Colleague,
                  and Expert perspectives) that helped prepare you for this project. In your portfolio,
                  you will document relevant information from each of the perspectives (approximately
                  1/2 to 1 page for each perspective). Your reflection should integrate, and synthesize
                  the relevant information from the four perspectives in relation to the question.
               </p>
               
               <p>&nbsp;</p>
               
               <ol>
                  
                  <li>Student (class discussions/surveys/papers/classroom assessment techniques)</li>
                  
               </ol>
               
               <ul>
                  
                  <li>What do my students say about the situation I've identified in my research question?</li>
                  
                  <li>What evidence do I have from students that has informed this project?</li>
                  
                  <li>Do they confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>What strategies will I use to collect student opinions?</li>
                  
               </ul>
               
               
               <li>Colleagues (discussions/surveys/workshops)</li>
               
               <ul>
                  
                  <li>What do my peers think about the problem/situation/success I've identified in my research
                     question?
                  </li>
                  
                  <li>Do they confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>Do they experience similar things in their practices?</li>
                  
                  <li>Do they perceive my research question as a worthwhile item for investigation?</li>
                  
                  <li>How can I get this information from them?</li>
                  
               </ul>
               
               
               <li>Expert (review of the literature)</li>
               
               <ul>
                  
                  <li>
                     <img alt="13_2_lai_1.jpg" height="188" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/13_2_lai_1.jpg" class="resizable" width="400" metadata="obj7" style="border: 1px solid #000000; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"> What do experts in the field say?
                  </li>
                  
                  <li>Do they confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>What does a review of relevant literature reveal? Is there professional literature
                     or scholarly writing related to my question?
                  </li>
                  
                  <li>What kind of research can I do to assist me with my question?</li>
                  
                  <li>Is the literature consistent with my ideas and assumptions?</li>
                  
                  <li>Include References at the end of this section</li>
                  
               </ul>
               
               
               <li>Self-reflection (why do I want to know?)</li>
               
               <ul>
                  
                  <li>What does my own personal experience tell me?</li>
                  
                  <li>Does it confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>Why have I identified the question above?</li>
                  
                  <li>What personal experiences inform me that this is a worthwhile question?</li>
                  
                  <li>What makes this question important to me?</li>
                  
               </ul>
               
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Appropriate Methods</h1>
               
               <p>The appropriate methods should include student learning outcomes, performance indicators,
                  teaching strategies, the assessment plan, and the AR methodology design.
               </p>
               
               <p>Methodologies should:</p>
               
               <ul>
                  
                  <li>be appropriate for achieving the FLO</li>
                  
                  <li>follow the rigors of the discipline</li>
                  
                  <li>have a clear description</li>
                  
                  <li>have an assessment plan that gauges the effectiveness of the FLO comprehensively</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <h2>Student Learning Outcomes</h2>
               
               <p>When considering methods for implementing the AR project, it is critical to articulate
                  the "Student Learning Outcomes" (SLO). A Student Learning Outcome states what a student
                  should understand and/or be able to do as a result of what she has learned in a course,
                  library orientation, counseling session. Consider the following questions:
               </p>
               
               <ol>
                  
                  <li>What will my students know and be able to do better as a result of the intervention,
                     innovation, or strategy I employ here?
                  </li>
                  
                  <li>Does the SLO connect to or support the outcomes of a course or program?</li>
                  
                  <li>Does the SLO describe learning that is meaningful in a real-world context?</li>
                  
               </ol>
               
               <p>Effective Student Learning Outcomes should be results-oriented, clearly understood
                  by colleagues and students, measurable, aligned with the Faculty Learning Outcome,
                  and critical to teaching and learning. Candidate may have multiple SLO's, if applicable.
               </p>
               
               <p>&nbsp;</p>
               
               <h2>Performance Indicators</h2>
               
               <p>With each Student Learning Outcome, Performance Indicators identify the small steps
                  students take to achieve the learning outcome. &nbsp; Performance Indicators are pre-determined
                  criteria, stated by you, that identify these steps. The following questions can help
                  you identify the performance indicators of the student learning outcomes for your
                  project. Answer the questions that are relevant to your project as a way to focus
                  your ideas:
               </p>
               
               <ol>
                  
                  <li>What specific qualities or evidence will I observe in the students' work/performance/behavior
                     that will demonstrate to me they have achieved this competency or indicator?
                  </li>
                  
                  <li>What is the minimum level of performance I am willing to accept from a student to
                     say he or she has achieved the learning outcome(s)? (This is your criteria.)
                  </li>
                  
                  <li>What student core competencies and indicators (TVCA) are related to these outcomes?</li>
                  
               </ol>
               
               <p>Performance Indicators of Student Learning Outcomes are the qualitative or quantitative
                  interim measurements demonstrating that the meaningful steps in student learning are
                  being taken to achieve mastery. In other words, Indicators identify the specific,
                  incremental traits or features of successful student mastery. Indicators should clearly
                  identify the incremental traits of mastery sequenced for optimum learning. If multiple
                  SLOs are identified, indicators should be listed for each SLO.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;&nbsp;</p>
               
               
               <h2>Teaching Strategies</h2>
               
               <p>Teaching strategies are the methods, strategies, and/or techniques to support student
                  mastery of the student learning outcomes you identified in your project. They should
                  be appropriate for achieving the SLO, should follow the rigors of the discipline,
                  and should be clearly explained step by step. Include reference to relevant artifacts.
                  The teaching strategies should be a step by step plan for the project (not for each
                  SLO).
               </p>
               
               <p>The following questions can help you decide the methods, strategies, and/or techniques
                  to support student mastery of the student learning outcomes you identified in your
                  project. Answer the questions that are relevant to your project as a way to focus
                  your ideas.
               </p>
               
               <ol>
                  
                  <li>What are my learning activities? Will these activities prepare my students for mastery?
                     What are the processes for taking students from beginning to end?
                  </li>
                  
                  <li>How can I establish an inclusive and safe learning environment for my students during
                     this process?
                  </li>
                  
                  <li>How will my students make connections with the content, each other, and the instructor,
                     counselor, or librarian?
                  </li>
                  
                  <li>How am I going to keep records of the processes (student learning, teaching strategies,
                     etc.) for my action research project? &nbsp; For example, written methods: personal journal
                     or diary, field notes, surveys, questionnaires? &nbsp; Live methods: Interviews, role play,
                     video or audio tape? Include reference to relevant artifacts.
                  </li>
                  
               </ol>
               
               <p>In your portfolio you should refer to artifacts relevant in your teaching strategies,
                  such as rubrics, surveys, or activities you created. &nbsp; However, you would not include
                  the results from students.
               </p>
               
               <p>&nbsp;</p>
               
               <h2>Assessment Plan</h2>
               
               <p>Assessments should include a comprehensive set of formative and summative assessment
                  tools that adequately measure the SLO performance indicators. Include reference to
                  relevant artifacts.
               </p>
               
               <ol>
                  
                  <li>How will I measure the performance indicators described in the SLOs section?</li>
                  
                  <li>What tool(s) am I going to use to measure/gauge how my students perform in relation
                     to the indicators in the SLOs? (papers, quizzes, rubrics, etc.)
                  </li>
                  
                  <li>Are there tools I can use that will give the students formative feedback (prior to
                     receiving summative feedback)?
                  </li>
                  
                  <li>How will my students know the standards or criteria their work will be evaluated against?</li>
                  
                  <li>When choosing an assessment technique, I should ask myself:</li>
                  
               </ol>
               
               <ul>
                  
                  <li>Is the assessment technique chosen appropriate to my goal?</li>
                  
                  <li>Can I integrate the assessment technique into my activities?</li>
                  
                  <li>Will it contribute to learning?</li>
                  
               </ul>
               
               
               <li>When applying an assessment technique, I should ask myself:</li>
               
               <ul>
                  
                  <li>Have I tried it?</li>
                  
                  <li>Have I done a run-through with a colleague?</li>
                  
                  <li>How will I make its purpose/process clear to students?</li>
                  
                  <li>How will I provide the necessary practice for students?</li>
                  
                  <li>Have I allowed enough time to apply the technique?</li>
                  
               </ul>
               
               
               
               <p>In your portfolio you should refer to artifacts relevant in your teaching strategies,
                  such as rubrics, surveys, or activities you created. &nbsp;However, you would not include
                  the results from students.
               </p>
               
               <p>&nbsp;</p>
               
               <h2>AR Methodology Design</h2>
               
               <p>Action Research is a scholarly inquiry to improve teaching and student learning. You
                  have already described your plan's student learning outcomes, performance indicators,
                  strategies, and assessment methods of the student learning outcome. Now, you will
                  design a plan to assess the effectiveness of the innovative teaching and assessment
                  methods you implemented in your project. Remember, action research is about what you
                  are doing, not what other people are doing.
               </p>
               
               <p>The following questions can help you decide the most effective methods to measure
                  the usefulness of your innovations. Answer the questions that are relevant to your
                  project as a way to focus your ideas.
               </p>
               
               <ol>
                  
                  <li>How will I know whether or not (to what degree) my innovations have worked?</li>
                  
                  <li>Have I planned how I will analyze the data?</li>
                  
                  <li>Have I collect a reasonable amount of data?</li>
                  
                  <li>Is my process of analysis manageable?</li>
                  
                  <li>Have I planned adequate time to do the analysis?</li>
                  
               </ol>
               
               <p>Consider the validity of your results: &nbsp; What kind of evidence will I produce to judge
                  the value of my innovation(s) incorporated in my action research project? For example,
               </p>
               
               <ul>
                  
                  <li>If applicable, compare the results of a base class and pilot class</li>
                  
                  <li>If applicable, compare the results of a base assignment and pilot assignment</li>
                  
                  <li>If applicable, compare the results of a base class with aggregate departmental data</li>
                  
               </ul>
               
               <p>Consider the reliability of your results:</p>
               
               <ul>
                  
                  <li>Will the statistics be the same if another researcher replicates your project?</li>
                  
               </ul>
               
               <ul>
                  
                  <li>If you replicate the project in other classes, how will you demonstrate reliability
                     and/or effectively describe your project for replication?&nbsp;
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>In your portfolio you should refer to artifacts relevant in your methodology design,
                  such as rubrics, surveys, or activities you created. &nbsp; However, you would not include
                  the results from students.
               </p>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Significant Results</h1>
               
               <p><img alt="methods.png" height="213" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/methods.png" class="resizable" width="246" metadata="obj5" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"> The significant results provide primary evidence and analysis to justify your answer
                  to the research question. The results should be:
               </p>
               
               <ul>
                  
                  <li>sufficient to demonstrate your FLO</li>
                  
                  <li>clearly and insightfully explained</li>
                  
                  <li>includes student feedback/work, if applicable</li>
                  
                  <li>opens additional questions for further exploration</li>
                  
               </ul>
               
               <p>Project results should be analyzed in relation to the original FLO and research question,
                  and explanation of the evidence and supporting artifacts should demonstrate that the
                  candidate has learned, achieved, or accomplished the FLO. Explanation should include
                  description of how project results will inform your practice and impact student learning.
                  Evidence and supporting artifacts should be sufficient to demonstrate achievement
                  of the FLO. You should use an effective mix of text and graphics to clearly present
                  and explain the data/findings/results. Student work/feedback, if applicable, should
                  document the achievement of the goals of the FLO. Remove students' names from all
                  student work.
               </p>
               
               <p>The following prompts can help you explain the findings of your Action Research Project.
                  Answer the questions that are relevant to your project as a way to focus your ideas.
               </p>
               
               <ol>
                  
                  <li>How shall I describe the results of my project? (tables, graphs, narratives)</li>
                  
                  <li>How well have I labeled and explained my graphs/charts?</li>
                  
                  <li>How well did my students accomplish the SLOs?</li>
                  
                  <li>Did the methods remedy the problem in terms of changes in knowledge, abilities, attitudes,
                     and commitment of students and/or of myself?
                  </li>
                  
                  <li>Were the results what I expected?</li>
                  
               </ol>
               
               <p>In your portfolio you should refer to artifacts that document the results of your
                  research question.
               </p>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Reflective Critique</h1>
               
               <p>&nbsp;</p>
               
               <h2>General AR Project Reflection</h2>
               
               <p>In general, you should reflect on what was learned while completing the Action Research
                  Project and how this might improve future work. The reflection relates your project
                  to student learning and plans for revision of teaching practice in light of results
                  should be clearly stated.
               </p>
               
               <ol>
                  
                  <li>Given my results, how will I use this information to improve student learning in the
                     future?&nbsp;To improve my practice?
                  </li>
                  
                  <li>What additional questions arose that I might want to pursue? How might I change my
                     research question in light of the results?
                  </li>
                  
                  <li>What other innovation might I try next time, based on these results?</li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <p>
                  <img src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/essentialCompetencies_new2016_sm.jpg" width="450" height="453" class="resizable" metadata="obj1" alt="essentialCompetencies.jpg" style="margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px; float: right; border: 0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <h2>Essential Competencies of a Valencia Educator Reflection</h2>
               
               <p>&nbsp;</p>
               
               <p>For each essential competency, reflect on:</p>
               
               <ul>
                  
                  <li>Explanation of how the methods used to demonstrate this Essential Competency in this
                     FLO aided student learning and/or helped the candidate to become a better counselor,
                     teacher, or librarian
                  </li>
                  
                  <li>Explanation of how the methods used in demonstrating this Essential Competency might
                     be improved
                  </li>
                  
                  <li>References to candidate's general practice outside the portfolio should be omitted</li>
                  
                  <li>Approximately 1/2 page to 1 page for each specified Essential Competency</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>Reflect on the Essential Competencies of a Valencia Educator addressed in the AR Project.</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Effective Presentation</h1>
               
               <p>
                  <strong>The effective presentation of the entire project should be:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>
                     <strong>w</strong>ritten clearly and coherently
                  </li>
                  
                  <li>presented and edited professionally</li>
                  
                  <li>polished presentation</li>
                  
                  <li>includes plans for uploading in the ARP builder (see below)<img alt="indexTabs.jpg" height="306" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/indexTabs.jpg" class="resizable" width="475" metadata="obj8" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                     
                  </li>
                  
               </ul>
               
               <h2>Plan for Dissemination</h2>
               
               <p>It is commonly held that scholarly work should be made public. In one or two sentences,
                  describe how you will make your project public. You are required to upload the action
                  research project into the AR Builder in Atlas, and distribute the portfolio to the
                  dean and panel members. The following prompts can help you identify how you plan to
                  make your work public, both in the Action Research Builder and beyond.
               </p>
               
               <ul>
                  
                  <li>Department or discipline colleagues?</li>
                  
                  <li>Valencia faculty development forums, such as Destination, Assessment Initiatives,
                     etc.)?
                  </li>
                  
                  <li>Faculty-to-Faculty, Assessment Initiatives?</li>
                  
                  <li>Professional journal article?</li>
                  
                  <li>Professional conference presentation?&nbsp;</li>
                  
               </ul>
               
               <p>Remember, when communicating your results:&nbsp;</p>
               
               <ul>
                  
                  <li>Include measures of central tendency and measures of variation in your statistics
                     when you gather significant quantitative data
                  </li>
                  
                  <li>Use charts and graphs when possible</li>
                  
                  <li>Provide descriptive titles and clear labels</li>
                  
                  <li>Use scales consistently</li>
                  
               </ul>
               
               <h2>Valencia's Action Research Builder</h2>
               
               <ol>
                  
                  <li>Tutorial with examples and explanations for each element of an ARP</li>
                  
                  <li>Puts finished ARP's in a consistent, readable and printable format</li>
                  
                  <li>Professional quality (edit)</li>
                  
                  <li>Database for searching all Valencia's ARP s</li>
                  
               </ol>
               
               <p>To access the ARP Builder, log into Atlas and click on the "Faculty" tab. Directions
                  to log access the ARP builder can also be found on Valencia's website. Directions
                  can also be followed on the next page.
               </p>
               
               <p style="text-align: center"></p>
               
               
               <h1>
                  <strong>How to Use the Action Research Builder</strong>
                  
               </h1>
               
               <p>It is recommended to enter your Action Research Project in the builder when the candidate
                  is completely finished with his/her project (when ready for final submission). As
                  you work on your ARP and get feedback from your panel, changes are inevitable. It
                  is best to wait until all changes have been made before entering it in the builder.
                  If you start earlier, you will need to go back into the builder and copy/paste any
                  changes.
               </p>
               
               <p><strong>Step 1</strong> Log into Atlas.
               </p>
               
               <p><strong>Step 2</strong> Click on the Faculty tab.
               </p>
               
               <p><strong>Step 3</strong> Scroll down to the section "Faculty Development" (bottom middle). Click on the Action
                  Research Project Builder link.
               </p>
               
               <p>
                  <img alt="ARP_B_Atlas_link.jpg" height="407" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_Atlas_link.jpg" class="resizable" width="450" metadata="obj9" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 4</strong> Click on the "start a new project" link.
               </p>
               
               <p>
                  <img alt="ARP_B_startProject.jpg" height="296" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_startProject.jpg" class="resizable" width="450" metadata="obj10" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 5</strong> Fill out the information on the Research Project page. On the left side, there is
                  a link with an example to demonstrate how the form is filled out.
               </p>
               
               <p>
                  <span style="background-color: #ffff66">Under the "Program/Initiative Supporting Project, all candidates in the tenure process
                     working on their FLO #1 Action Research Project should select Teaching/Learning Academy
                     (TLA).</span>
                  
               </p>
               
               <p>
                  <img alt="ARP_B_form.jpg" height="330" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_form.jpg" class="resizable" width="450" metadata="obj11" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 6</strong> Click "Launch Project".
               </p>
               
               <p><strong>Step 7</strong> From the upper menu, you will go through each of the menu items and copy/paste the
                  information from your completed project. Remember, there is an explanation and example
                  on each page you enter information. Each tab/link in the menu bar correlates with
                  a section of your Action Research Project. Note that in some sections (such as Adequate
                  Preparation and Methods/Assessments), there is a drop down to select the subcategories.
                  Make sure you fill out each section. On each page, you have the opportunity to upload
                  documents or graphics. Make sure you save your file frequently. Atlas times out fairly
                  frequently, and you will lose any data you did not save if it times out prior to saving.
               </p>
               
               <p>
                  <img alt="ARP_B_save.jpg" height="393" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_save.jpg" class="resizable" width="450" metadata="obj13" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p><strong>Step 8</strong> When the project has been completely entered, and checked for accuracy, you have
                  one last step to publish the file for dissemination. <span style="background-color: #ffff66">On the "Project" page at the bottom you must select YES to the "Completed" (in red)
                     section. Once you select YES, the project will be visible to your committee to show
                     you have made your work public (required for Scholarship of Teaching and Learning).</span></p>
               
               <p>
                  <img alt="ARP_B_completed.jpg" height="373" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/ARP_B_completed.jpg" class="resizable" width="450" metadata="obj15" style="border: 0; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk_print.pcf">©</a>
      </div>
   </body>
</html>