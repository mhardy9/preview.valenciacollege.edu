<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research versus Traditional Research | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Action Research versus Traditional Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		<a id="headingtaglink-1"></a><h1>Action Research differs from traditional research in two important way</h1>
               
               <p>&nbsp;</p>
               
               <ol>
                  
                  <li>It is not an add-on activity. It is embedded in the regular ongoing work of the class.</li>
                  
                  <li>It makes the complete cycle from formulating relevant questions to making changes
                     in the practice of teaching.
                  </li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <table class="table bannerstyletable">
                  
                  <tr>
                     
                     <th scope="col" style="width: 106px; vertical-align: top"></th>
                     
                     <th scope="col" style="width: 316px; vertical-align: top">
                        
                        <p><strong>Traditional Research</strong></p>
                        
                     </th>
                     
                     <th scope="col" style="width: 340px; vertical-align: top">
                        
                        <p><strong>Action Research</strong></p>
                        
                     </th>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Purpose</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>To draw conclusions. Focus is on advancing knowledge in the field. Insights may be
                           generalized to other settings.
                        </p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>To make decisions. Focus is on the improvement of educational practice. Limited generalizability.</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Context</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>Theory: Hypotheses/research questions derive from more general theoretical propositions.</p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>Practice: Research questions derive from practice. Theory plays secondary role.</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Data Analysis</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>Rigorous statistical analysis.</p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>Focus on practical, not statistical significance.</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td style="width: 106px; vertical-align: top">
                        
                        <p>Sampling</p>
                        
                     </td>
                     
                     <td style="width: 316px; vertical-align: top">
                        
                        <p>Random or representative sample.</p>
                        
                     </td>
                     
                     <td style="width: 340px; vertical-align: top">
                        
                        <p>Students with whom they work.</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>
                  <em>Adapted from: Mc Millan, J. H. &amp; Wergin. J. F. (1998). "Understanding and Evaluating
                     Educational Research."</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><a href="ARP_softchalk2.html#">return to top</a> | <a href="index.html">previous page</a> | <a href="ARP_softchalk3.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk2.pcf">©</a>
      </div>
   </body>
</html>