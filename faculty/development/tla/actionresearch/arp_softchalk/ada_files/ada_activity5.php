<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Accessible Activity | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/ada_files/ada_activity5.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/ada_files/">Ada Files</a></li>
               <li>Accessible Activity</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>This is an alternate content page containing a Selection Activity.  It has opened
                  in a new window.<br>The items in this activity may be text or images.
               </p>
               
               <p>
                  
               </p>
               
               <p></p>
               
               <p>Set Title:Clear Goals
                  <br>Choices:
                  
               </p>
               
               <ul>
                  
                  <li>Expert Perspective</li>
                  
                  <li>Student Learning Outcome</li>
                  
                  <li>Abstract</li>
                  
                  <li>Essential Competency Reflect</li>
                  
                  <li>Methodology Design</li>
                  
                  <li>Action Research Question</li>
                  
                  <li>Student Perspective</li>
                  
                  <li>Colleague Perspective</li>
                  
                  <li>General Reflection</li>
                  
                  <li>Teaching Strategies</li>
                  
               </ul>
               
               <p>Set Title:Adequate Preparation
                  <br>Choices:
                  
               </p>
               
               <ul>
                  
                  <li>Expert Perspective</li>
                  
                  <li>Student Learning Outcome</li>
                  
                  <li>Self Perspective</li>
                  
                  <li>Teaching Strategies</li>
                  
                  <li>Methodology Design</li>
                  
                  <li>Action Research Question</li>
                  
                  <li>Student Perspective</li>
                  
                  <li>Colleague Perspective</li>
                  
                  <li>Abstract</li>
                  
                  <li>General Reflection</li>
                  
               </ul>
               
               <p>Set Title:Appropriate Methods
                  <br>Choices:
                  
               </p>
               
               <ul>
                  
                  <li>Assessment Plan</li>
                  
                  <li>Student Learning Outcome</li>
                  
                  <li>Self Perspective</li>
                  
                  <li>Teaching Strategies</li>
                  
                  <li>Methodology Design</li>
                  
                  <li>Action Research Question</li>
                  
                  <li>Student Perspective</li>
                  
                  <li>Colleague Perspective</li>
                  
                  <li>Abstract</li>
                  
                  <li>General Reflection</li>
                  
               </ul>
               
               <p>Set Title:Reflective Critique
                  <br>Choices:
                  
               </p>
               
               <ul>
                  
                  <li>Assessment Plan</li>
                  
                  <li>Student Learning Outcome</li>
                  
                  <li>Self Perspective</li>
                  
                  <li>Essential Competency Reflect</li>
                  
                  <li>Methodology Design</li>
                  
                  <li>Action Research Question</li>
                  
                  <li>Student Perspective</li>
                  
                  <li>Colleague Perspective</li>
                  
                  <li>Abstract</li>
                  
                  <li>General Reflection</li>
                  
               </ul>
               
               <br><a href="javascript:window.close();">Close this window.</a>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/ada_files/ada_activity5.pcf">©</a>
      </div>
   </body>
</html>