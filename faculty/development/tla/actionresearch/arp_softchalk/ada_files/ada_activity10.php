<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Accessible Activity | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/ada_files/ada_activity10.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/ada_files/">Ada Files</a></li>
               <li>Accessible Activity</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>This is an alternate content page containing a Tabbed Info Activity.  It has opened
                  in a new window.<br>The tabs have an information section with a title and might have additional content
                  and/or images.
               </p>
               
               <p>
                  
               </p>
               
               <p></p>
               
               <p>Tab Title: Abstract Example: Not Yet Acceptable or Incomplete</p>
               
               <p>Tab Content: </p>
               
               <p>The following is an example of an abstract that is not yet acceptable.</p>
               
               <p>"The goal of the project is to determine whether including applications as group activities
                  in College Algebra courses will improve student understanding of the material. Qualitative
                  and quantitative analysis of the results of student survey and grades on the chapter
                  test (exponential and logarithmic functions) compared to results from my previous
                  semesters' College Algebra courses demonstrate that the use of group projects involving
                  applications improves student understanding of material."
               </p>
               
               <p> </p>
               
               <p>Why is this abstract not yet acceptable?</p>
               
               <p> </p>
               
               <p>This abstract is not yet acceptable because it is lacking a summary of the methods
                  for the project.
               </p>
               
               <p>Tab Title: Abstract Example: Acceptable</p>
               
               <p>Tab Content: </p>
               
               <p>The following is an example of an abstract considered acceptable.</p>
               
               <p> </p>
               
               <p>"Authentic learning has long been regarded as a preferred teaching method. Incorporating
                  this type of authentic activity can increase students' engagement and improve their
                  quality of work. This is very evident in math, where students often have a disconnect
                  with how it applies to the real world. I have developed group projects involving real-life
                  applications of exponential and logarithmic functions for College Algebra courses.
                  The goal of the project is to determine whether including applications as group activities
                  in College Algebra courses will improve student understanding of the material. The
                  group projects consist of mortgage, bacterial growth and decay, hurricane (data of
                  loss of electricity), and forensics (determining time of death). Groups are determined
                  based on student indicated interests. The projects involve groups of students learning
                  about their chosen application, solving a specific application problem via exponential
                  or logarithmic functions, and presenting their findings in an interactive manner.
                  Qualitative and quantitative analysis of the results of student survey and grades
                  on the chapter test (exponential and logarithmic functions) compared to results from
                  my previous semesters' College Algebra courses demonstrate that the use of group projects
                  involving applications improves student understanding of material."
               </p>
               
               <p> </p>
               
               <p>Why is this acceptable, but not exemplary?</p>
               
               <p> </p>
               
               <p>This abstract contains all of the necessary components including the purpose, methods
                  and results. It is not considered exemplary because it is not concise. The beginning
                  of the abstract has several sentences relating to authentic learning. This is not
                  part of the research project. The goal of the project is to determine whether including
                  applications as group activities will improve student understanding of the material.
                  The project does not measure whether authentic activities improve student learning.
               </p>
               
               <p>Tab Title: Abstract Example: Exemplary</p>
               
               <p>Tab Content: </p>
               
               <p>The following is an example of an abstract considered exemplary.</p>
               
               <p> </p>
               
               <p>"I have developed group projects involving real-life applications of exponential and
                  logarithmic functions for College Algebra courses. The goal of the project is to determine
                  whether including applications as group activities in College Algebra courses will
                  improve student understanding of the material. The group projects consist of mortgage,
                  bacterial growth and decay, hurricane (data of loss of electricity), and forensics
                  (determining time of death). Groups are determined based on student indicated interests.
                  The projects involve groups of students learning about their chosen application, solving
                  a specific application problem via exponential or logarithmic functions, and presenting
                  their findings in an interactive manner. Qualitative and quantitative analysis of
                  the results of student survey and grades on the chapter test (exponential and logarithmic
                  functions) compared to results from my previous semesters' College Algebra courses
                  demonstrate that the use of group projects involving applications improves student
                  understanding of material."
               </p>
               
               <p> </p>
               
               <p>Why is this abstract exemplary?</p>
               
               <p> </p>
               
               <p>The abstract contains the purpose, methods, and results in a concise manner.</p>
               <br><a href="javascript:window.close();">Close this window.</a>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/ada_files/ada_activity10.pcf">©</a>
      </div>
   </body>
</html>