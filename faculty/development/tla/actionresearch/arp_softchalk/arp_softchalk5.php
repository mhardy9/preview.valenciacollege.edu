<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Adequate Preparation | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk5.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/arp_softchalk/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li><a href="/faculty/development/tla/actionresearch/arp_softchalk/">Arp Softchalk</a></li>
               <li>Adequate Preparation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		<a id="headingtaglink-1"></a><h1>Adequate Preparation</h1>
               
               <p>As part of adequate preparation for Action Research, the researcher must consider
                  the work from multiple perspectives. As you assemble your portfolio, you will include
                  your own reflection (Self-perspective) and consultation with others (Student, Colleague,
                  and Expert perspectives) that helped prepare you for this project. In your portfolio,
                  you will document relevant information from each of the perspectives (approximately
                  1/2 to 1 page for each perspective). Your reflection should integrate, and synthesize
                  the relevant information from the four perspectives in relation to the question.
               </p>
               
               <p>&nbsp;</p>
               
               <ol>
                  
                  <li>Student (class discussions/surveys/papers/classroom assessment techniques)</li>
                  
               </ol>
               
               <ul>
                  
                  <li>What do my students say about the situation I've identified in my research question?</li>
                  
                  <li>What evidence do I have from students that has informed this project?</li>
                  
                  <li>Do they confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>What strategies will I use to collect student opinions?</li>
                  
               </ul>
               
               
               <li>Colleagues (discussions/surveys/workshops)</li>
               
               <ul>
                  
                  <li>What do my peers think about the problem/situation/success I've identified in my research
                     question?
                  </li>
                  
                  <li>Do they confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>Do they experience similar things in their practices?</li>
                  
                  <li>Do they perceive my research question as a worthwhile item for investigation?</li>
                  
                  <li>How can I get this information from them?</li>
                  
               </ul>
               
               
               <li>Expert (review of the literature)</li>
               
               <ul>
                  
                  <li>
                     <img alt="13_2_lai_1.jpg" height="188" src="http://valenciacollege.edu/faculty/development/tla/actionResearch/ARP_softchalk/13_2_lai_1.jpg" class="resizable" width="400" metadata="obj7" style="border: 1px solid #000000; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"> What do experts in the field say?
                  </li>
                  
                  <li>Do they confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>What does a review of relevant literature reveal? Is there professional literature
                     or scholarly writing related to my question?
                  </li>
                  
                  <li>What kind of research can I do to assist me with my question?</li>
                  
                  <li>Is the literature consistent with my ideas and assumptions?</li>
                  
                  <li>Include References at the end of this section</li>
                  
               </ul>
               
               
               <li>Self-reflection (why do I want to know?)</li>
               
               <ul>
                  
                  <li>What does my own personal experience tell me?</li>
                  
                  <li>Does it confirm or fail to support my assumption about what I think is going on in
                     the classroom?
                  </li>
                  
                  <li>Why have I identified the question above?</li>
                  
                  <li>What personal experiences inform me that this is a worthwhile question?</li>
                  
                  <li>What makes this question important to me?</li>
                  
               </ul>
               
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><a href="ARP_softchalk5.html#">return to top</a> | <a href="ARP_softchalk4.html">previous page</a> | <a href="ARP_softchalk6.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/arp_softchalk/arp_softchalk5.pcf">©</a>
      </div>
   </body>
</html>