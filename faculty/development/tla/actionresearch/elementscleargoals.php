<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Action Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/elementscleargoals.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Action Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>Elements of Action Research </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="elementsprojectinformation.html">Project Information</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementscleargoals.html">Clear Goals </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsadequatepreparation.html">Adequate Preparation</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="elementsappropriatemethods.html">Appropriate Methods </a></div>
                                 </div>
                                 
                                 <div><a href="elementssignificantresults.html">Significant Results </a></div>
                                 
                                 <div><a href="elementsreflectivecritique.html">Reflective Critque </a></div>
                                 
                                 <div><a href="elementseffectivepresentation.html">Effective Presentation </a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h2>&nbsp;</h2>
                        
                        <h2>Elements - Clear Goals</h2>
                        
                        <p><strong>A.&nbsp;  Abstract </strong></p>
                        
                        <p>Your  abstract should concisely summarize and highlight the primary points of your
                           Action Research Project. The purpose is to help colleagues quickly decide if  your
                           project can support their own project or practice. Abstracts should  include the purpose,
                           methods, and results of the project only. The abstract  should be short.
                        </p>
                        
                        <p><strong>B.&nbsp; Research Question<br>
                              </strong>The  following prompts can help you identify the question that will be the basis for
                           your Action Research Project. Answer the questions that are relevant to your  project
                           as a way to focus your ideas. Remember, action research questions are  about what
                           you are doing, not what other people are doing.&nbsp; 
                        </p>
                        
                        <ol>
                           
                           <li>
                              <strong>What is the question?</strong>
                              
                              <ul> 
                                 
                                 <li>Is  there a persistent problem or area of concern in my class/professional setting?
                                    What is my concern? Why am I concerned?&nbsp; What kind of evidence can I produce to  show
                                    the situation as it is?&nbsp; 
                                 </li>
                                 
                                 <li>Is  there a teaching method I would like to explore/incorporate in my practice?</li>
                                 
                                 <li>Is there a  topic in my discipline I would like to teach or present differently?</li>
                                 
                                 <li>Would  I like to explore alternative methods of assessment?</li>
                                 
                                 <li>Would I like  to have evidence of the effectiveness of something I am&nbsp;currently doing?</li>
                                 
                                 <li>Have I seen  an Action Research question that I would like to alter/modify/investigate
                                    in my  practice?
                                 </li>
                                 
                                 <li>Will I be  able to follow this question through multiple semesters?</li>
                                 
                              </ul>
                              
                           </li>
                           
                           
                           <li>
                              <strong>Is the question appropriate for Action Research?</strong>
                              
                              <ul>
                                 
                                 <li>Is  it significant and related to improving student learning?</li>
                                 
                                 <li>How  will finding the answer inform my teaching?</li>
                                 
                                 <li>Is  it under my control (teaching strategies, classroom activities, student  assignments)?</li>
                                 
                                 <li>Is  it feasible in terms of time, effort and available resources?</li>
                                 
                                 <li>Can  I take action based on my results?</li>
                                 
                                 <li>Is  this an area where I am willing to make a change?</li>
                                 
                                 <li>Am  I willing to make a change based on my results?</li>
                                 
                                 <li>Does the question measure only one result/trait?</li>
                                 
                                 <li>Can the results be measured? </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong>How might I address the question?</strong>
                              
                              <ul>
                                 
                                 <li>Can  I clearly state the Research Question (or hypothesis) that I want to test?
                                    
                                 </li>
                                 
                                 <li>Can  I clearly state the goal or set the standard to which I want to compare my  students'
                                    achievements
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ol>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/elementscleargoals.pcf">©</a>
      </div>
   </body>
</html>