<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Project Rubric | Action Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/actionresearch/rubric.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/actionresearch/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Action Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/actionresearch/">Action Research</a></li>
               <li>Project Rubric</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <h2>Action Research Project Rubric</h2>
                  			
                  <p><a href="documents/ActionResearchProjectRubric.pdf" target="_blank">Printer Friendly Version of the Action Research Project Rubric</a> 
                  </p>
                  			
                  <table class="table table">
                     				
                     <caption>Levels of Achievement</caption>
                     				
                     <tr>
                        					
                        <th>Standards of Scholarship &amp;ARP Elements</th>
                        					
                        <th>Not Yet Acceptable</th>
                        					
                        <th>Acceptable</th>
                        					
                        <th>Exemplary</th>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <td>
                           						
                           <p><strong><a href="elementscleargoals.php">Clear Goals</a></strong>
                              							<br> a.&nbsp; Abstract
                              							<br> b.&nbsp; Research Question
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>a.&nbsp; Abstract not clearly stated
                              							<br>
                              						
                           </p>
                           						
                           <p>b.&nbsp; Research Question is not clearly stated and/or does not relate to student learning</p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>a.&nbsp;Abstract includes purpose,&nbsp;methods &amp; results of project
                              							<br> b. Research Question is clearly stated and relates to student learning
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p> a. Acceptable + clearly articulated and concise.
                              							<br>
                              							<br> b. Acceptable + polished 
                           </p>
                           					
                        </td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <td>
                           						
                           <p><strong><a href="elementsadequatepreparation.php">Adequate Preparation</a><br> </strong><strong><br>
                                 							Perspectives</strong>
                              							<br> &nbsp;&nbsp; 1. Student
                              							<br> &nbsp;&nbsp; 2. Colleague
                              							<br> &nbsp;&nbsp; 3. Expert
                              							<br> &nbsp;&nbsp; 4. Self 
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>Discussion of four perspectives insufficient to document relevant information; one
                              or more perspectives missing
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>&nbsp;Documents the relevant information from four perspectives in relation to the question</p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>Acceptable + integrates and synthesizes the relevant information </p>
                           					
                        </td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <td>
                           						
                           <p><span><strong><a href="elementsappropriatemethods.php">Appropriate Methods</a></strong></span><strong><br>
                                 							<br>
                                 							<span><strong>Methods &amp; Assessment</strong></span>
                                 							<br>
                                 							</strong>
                              							<br> 1. Student Learning Outcome (SLO)
                              							<br> 2. Performance Indicators
                              							<br> 3. Teaching Strategies of SLO
                              							<br> 4. Assessment Strategies of SLO
                              							<br> 5. Action Research Methodology Design
                              							<br> &nbsp; 
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>1.&nbsp; SLO not results-oriented, not clearly written nor measurable.
                              							<br>
                              							<br> 2. Indicators do not identify the discrete traits of mastery.
                              							<br>
                              							<br> 3. Strategies not appropriate for achieving the FL.
                              							<br>
                              							<br> 4.&nbsp; Assessment(s) do not adequately measure the identified indicators
                              							<br>
                              							<br>
                              							<br>
                              							<br> 5.&nbsp; The AR methodology design does not provide feedback that&nbsp; informs your practice
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>1.&nbsp; SLO results-oriented, clearly written &amp; measurable
                              							<br>
                              							<br> 2. Indicators identify the discrete traits of mastery
                              							<br>
                              							<br> 3. Teaching strategies appropriate for achieving the FLO
                              							<br>
                              							<br> 4.&nbsp; Assessment(s) measure the identified indicators; some formative &amp; summative assessment
                              tools evident.
                              							<br>
                              							<br>
                              							<br> 5.&nbsp; The AR methodology design provides feedback that&nbsp; informs your practice
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>1.&nbsp; SLO results-oriented, clearly written, measurable, and critical to teaching &amp;
                              learning
                              							<br> 2. Indicators clearly identify the discrete traits of mastery and sequenced for optimum
                              learning.
                              							<br> 3. Teaching strategies appropriate for achieving the FLO; follows the rigors of the
                              discipline
                              							<br> 4.&nbsp; Assessment(s) measure the identified indicators; comprehensive set of formative
                              and summative assessment tools evident
                              							<br>
                              							<br> 5.&nbsp; The AR methodology design provides feedback that&nbsp; informs your practice; feedback
                              informs you in areas you were seeking to improve
                           </p>
                           					
                        </td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <td>
                           						
                           <p><span><strong><a href="elementssignificantresults.php">Significant Results</a></strong></span><strong><br> </strong> Project Results
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>Results are not analyzed in relation to the original question</p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p> Results are analyzed in relation to the original question</p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>Acceptable + description of how results will inform your practice and impact student
                              learning are included
                           </p>
                           					
                        </td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <td>
                           						
                           <p><span><strong><a href="elementsreflectivecritique.php">Reflective Critique</a></strong></span><strong><br></strong>a.&nbsp; General Reflection on the ARP
                              							<br> b.&nbsp; TLA Candidates reflect on appropriate Essential Competencies
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>a.&nbsp; AR project&nbsp; is only summarized or reviewed</p>
                           						
                           <p>
                              							<br> b. Insufficient critical reflection of methods and results used to demonstrate each
                              specified Essential Competency in this FLO: little or no discussion of possible improvements
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>a.&nbsp; Reflection relates students' and candidate's learning to the research project
                              and includes plan for improvement
                              							<br> b. Competent critical reflection of methods and results used to demonstrate each
                              specified Essential Competency in this FLO; sufficient discussion of possible improvements
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>a.&nbsp; Acceptable + insightful analysis with clear plans for revison</p>
                           						
                           <p>
                              							<br> b. Acceptable + insightful discussion of possible improvements&nbsp;
                           </p>
                           					
                        </td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <td>
                           						
                           <p><strong><span><strong><a href="elementseffectivepresentation.php">Effective Presentation</a> </strong></span> </strong> 
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>Not written clearly or coherently: not presented and edited professionally to an appropriate
                              audience; not uploaded to the ARP Builder 
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>Written clearly and choherently; presented and edited professionally to an appropriate
                              audience; uploaded into the ARP Builder
                           </p>
                           					
                        </td>
                        					
                        <td>
                           						
                           <p>Acceptable + polished presentation; comprehensive</p>
                           					
                        </td>
                        				
                     </tr>
                     			
                  </table>
                  
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/actionresearch/rubric.pcf">©</a>
      </div>
   </body>
</html>