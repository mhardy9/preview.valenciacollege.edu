<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/seminar/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li>Seminar</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        
                        					
                        <h2>TLA Seminar Schedule</h2>
                        					
                        <h3>Foundations in Learning-centered Teaching</h3>
                        					
                        <p><strong>Understanding the Essential Com<strong>petencies</strong></strong> 
                        </p>
                        					
                        <p>This seminar series helps candidates examine the Essential Competencies of a Valencia
                           Educator in support of their practices and Individualized Learning Plans.
                           
                           					
                        </p>
                        					
                        <div>
                           
                           
                           
                           						
                           <div>Assessment as a Tool for Learning (ASMT 2121) 2 PD Hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Assessment at its best promotes students' learning; it doesn't just measure it. This
                                    seminar explores how to align learning outcomes with learning opportunities to create
                                    an effective assessment cycle. Participants will learn how to use formative and summative
                                    assessment methods, ensure alignment between learning activities and assessments,
                                    promote studentsâ€™ self-assessment, and use regular, systematic feedback in the learning
                                    cycle. <br>
                                    									Note:  This is part of the Year-1 TLA Seminar series.<br>
                                    									Audience: All faculty.
                                    								
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>Inclusion and Diversity (INDV2151) 2 PD Hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>In this seminar, participants will investigate learning opportunities that acknowledge,
                                    draw upon, and are enriched by student diversity and create atmospheres of inclusion
                                    and understanding. Participants will reflect on power differentials in the classroom,
                                    ways to increase inclusion and minimize exclusion, and some theoretical underpinnings
                                    of Inclusion and Diversity. <br>
                                    									Note:  This is part of the Year-1 TLA Seminar series. <br>
                                    									Audience: All faculty.
                                    								
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>Learning-centered Teaching Strategies (LCTS2111) 2 PD Hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>In this seminar, participants will examine the difference between group work and cooperative/collaborative
                                    learning strategies that promote active learning. Through an interactive approach
                                    that models-the-model of collaborative learning, participants will experience first-hand
                                    the power of the well-structured cooperative strategy. Participants will leave this
                                    seminar with strategies they can use in their next class.  <br>
                                    									Note:  This is part of the Year-1 TLA Seminar series. <br>
                                    									Audience: All faculty.
                                    								
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>LifeMap (LFMP2141) 2 PD Hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>This seminar explores the philosophy and practice of LifeMap. The focus is on the
                                    value of incorporating LifeMap strategies in one's practice to promote discipline
                                    learning while also promoting student life skills development.  A key question is
                                    "How can my practice enhance students' continued learning and planning in their academic,
                                    personal, and professional endeavors beyond my course?" <br>
                                    									Note:  This is part of the Year-1 TLA Seminar series. <br>
                                    									Audience: All faculty.  
                                    								
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           
                           						
                           <div>
                              							Scholarship of Teaching and Learning (SOTL2171) 3 PD Hours (complete in year-2)
                           </div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>This seminar examines strategies, particularly action research, that enable educators
                                    to continuously examine the effectiveness of their teaching/counseling/librarianship
                                    practices and assessment methodologies in terms of student learning. <br>
                                    									Note:  This is part of the Year-1 TLA Seminar series. <br>
                                    									Audience: Year-1 TLA candidates or faculty interested in designing and implementing
                                    an action research project. 
                                    								
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           						
                           <div>Creating an Individualized Learning Plan (PRFC 2161) 20 PD</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Creating an Individualized Learning Plan is designed for tenure-track counselors,
                                    librarians, and teaching faculty in the first year of the tenure process at Valencia.
                                    This curriculum provides participants with tools and resources enabling them to better
                                    understand the tenure process at Valencia.  By engaging with the story and its characters,
                                    assigned readings and activities, and collegial exchange, participants will expand
                                    their skills and knowledge to help them construct faculty learning outcomes for their
                                    Individualized Learning Plans.  This course will also deepen participants' understanding
                                    of the seven Essential Competencies of a Valencia Educator.   
                                    								
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           						
                           <div>Elective Learning-centered Teaching Courses</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>
                                    									Tenure candidates select two from the list below:
                                    								
                                 </p>
                                 								
                                 <ul>
                                    									
                                    <li>
                                       										<strong>LCTS2222 Case-based Teaching</strong><br>
                                       										Case studies can provide a rich basis for developing problem-solving and
                                       decision making skills, critical thinking skills which are necessary to meet higher
                                       level learning outcomes in all disciplines. This class will explore the most appropriate
                                       methods of integrating case-based teaching strategies into your course.
                                    </li>
                                    
                                    									
                                    <li>
                                       										<strong>INDV7311 Creating a Safe Space for Dialog kickoff *</strong><br> 
                                       										This course will demonstrate techniques for establishing a respectful and
                                       inclusive environment that promotes healthy classroom dialogue. Dialogue can assist
                                       students in moving information from a memorized or theoretical understanding to a
                                       more integrated, tangible and lasting knowledge. Participants will engage in best
                                       practices designed to promote discussion as a pedagogical tool. 
                                       										
                                       <p>
                                          											*Note: In registering for this course, you are committing to attend two
                                          meetings across two terms (one 3-hour and one 2-hour), complete the required reading,
                                          and integrate a conversation activity into your class.
                                       </p>
                                       									
                                    </li>
                                    
                                    									
                                    <li>
                                       										<strong>LCTS2910 Write to Learn</strong><br> 
                                       										Write-to-learn activities allow students to think through a course concept
                                       to demonstrate understanding. As a learning-centered strategy, it assesses whether
                                       learning has occurred and promotes confidence. In this workshop, participants will
                                       examine several write-to-learn activities designed to deepen discipline learning and
                                       create an activity for their course.
                                    </li>
                                    
                                    									
                                    <li>
                                       										<strong>LCTS2910 Asking the Right Questions</strong><br> 
                                       										Well-designed questions function as critical thinking prompts for students.
                                       This workshop will explore how to construction questions for class discussion and
                                       written work that encourages critical thinking. Participants will be introduced to
                                       a theoretical framework for "preparing, posing and pondering" questions, and then
                                       use that framework to examine sample questions and create questions for their classes.
                                       
                                    </li>
                                    
                                    									
                                    <li>
                                       										<strong>LCTS2214 Problem-based Learning</strong><br> 
                                       										Problem-based learning (PBL) is a learning-centered teaching strategy that
                                       places students in the active role as problem-solvers confronted with either an ill-structured
                                       situation or a problem that needs to be identified and solved using discipline-specific
                                       thinking.
                                    </li>
                                    
                                    								
                                 </ul>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           					
                        </div>
                        
                        
                        
                        
                        
                        					
                        <h3>Year-2 &amp; Year-3 Courses</h3>
                        					
                        <p>The TLA recommends a series of courses and opportunities in the second year <em>(see link below)</em> designed to support candidates in their portfolio development, and, more specifically,
                           their action research projects. In addition, candidates can select any of the courses
                           that help support their development of the Essential Competencies and general professional
                           growth. There are more than 200 courses that offer a deeper exploration of specific
                           aspects of each Essential Competency. Please use the course search at the link below
                           to search for courses by Essential Competency. 
                        </p>
                        
                        					
                        <div>
                           
                           						
                           <div>
                              							Professional Commitment (PRFC2264) 2 PD Hours (part of the Core Seminar series)
                              
                           </div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>This workshop will explore the importance of professional engagement as part of a
                                    faculty member's ongoing development.  Participants will discover the various professional
                                    opportunities available within their discipline, at the institution, and in the community.
                                    Activities are designed to assist participants in matching their interests and skills
                                    to the professional opportunities that best suit them. <br>
                                    									Note:  This is part of the Year-1 TLA Seminar series. <br>
                                    									Audience:  All faculty. 
                                    								
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>PRFC 2263: Creating an Evidence-based Portfolio 20 PD hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>This hybrid course provides TLA participants an overview and examination of the elements
                                    of a Valencia Faculty Portfolio. Participants also learn how to demonstrate learning
                                    through artifacts. The face-to-face orientation reviews Valencia's learning management
                                    system (LMS), the faculty portfolio concept, and the portfolio evaluation rubric.
                                    The wrap-up provides a face-to-face session where TLA participants review each other's
                                    portfolio artifacts.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>SOTL 2274: ARP Design and Data Collection 2 PD hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>
                                    									This seminar focuses on ARP research design and dataanalysis. Participants
                                    should bring their ARP implementation plans, assessment tools, and any data that has
                                    been collected.  Participants will:
                                    								
                                 </p>
                                 								
                                 <ul>
                                    									
                                    <li>learn how to align their Faculty Learning Outcome statements, Action Research Questions,
                                       and assessment strategies,
                                    </li>
                                    									
                                    <li>explore different methods of data analysis,</li>
                                    									
                                    <li>learn effective methods of data presentation, and</li>
                                    									
                                    <li>review technological tools useful in analyzing data.</li>
                                    								
                                 </ul>
                                 
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>PRFC 2910: Portfolio Planning Workshop 0 PD hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>In this workshop participants will review the elements of a portfolio and learn how
                                    to use the portfolio templates. Tenure candidates will also share their implementation
                                    plans for one faculty learning outcome and discuss best practices for collecting information
                                    to be included in the portfolio. Please bring a copy of your implementation plan to
                                    the workshop.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>SOTL 2272: Developing Effective Surveys 2 PD hours</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Workshop participants will learn the benefits of common survey types, review tips
                                    on how to write effective survey questions, collaboratively assess sample surveys,
                                    and have the opportunity to apply these principles to their own survey work. This
                                    workshop is appropriate for faculty in all disciplines investigating methods to improve
                                    student learning through formal and informal interventions. 
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>PRFC2231: Write a Better Tenure Portfolio </div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>This course is designed to teach the writing of a portfolio; rather than focusing
                                    on the content (what you research), we'll examine what makes a strong writing style
                                    for the actual writing (what you say).  This will include a discussion of academic
                                    voice, audience engagement, synthesis of source material, documentation of sources,
                                    and some common errors in mechanics and grammar.  We'll look at exemplary portfolios,
                                    and less than exemplary portfolios, specifically studying how the writing does --
                                    and doesn't -- work. Essentially, we're going to talk about how to package and sell
                                    your ideas to everyone else. NOTE:  This session is limited to tenure candidates.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           						
                           <div>SOTL2275: ARP Data Analysis and Presentation 2 PD HOURS</div>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>This seminar focuses on ARP data analysis and presentation. Participants will explore
                                    methods of analysis appropriate for specific types of data. Technological tools useful
                                    in analyzing and displaying data will be demonstrated. Participants will learn principles
                                    of effective data presentation. Participants are welcome to bring any data they have
                                    collected relevant to their ARP. NOTE: This session is limited to tenure candidates.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           
                           
                           
                           					
                        </div>
                        
                        					
                        <h3>Y-2 &amp; Y-3  Special Interest </h3>
                        					
                        <p><strong><a href="/faculty/development/programs/learningPartners/" target="_blank">Peer Observation of Teaching</a></strong> <br>
                           						This program provides the opportunity for you to sit in on each other's classes,
                           observe different teaching techniques and reflect on your own practice...then return
                           to your own class with a fresh outlook.
                        </p>
                        					
                        <p><strong><a href="/faculty/development/programs/online/" target="_blank">Introduction to Online Teaching</a></strong><br>
                           						Workshops designed for faculty members who are thinking about, or who are new
                           to, online teaching.&nbsp;These workshops, offered in partnership with the Online Faculty
                           Development department, invite you to explore the fundamentals of key issues of teaching
                           and learning in the online environment
                        </p>
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/seminar/index.pcf">©</a>
      </div>
   </body>
</html>