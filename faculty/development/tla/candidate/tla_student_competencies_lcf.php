<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/tla_student_competencies_lcf.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h3><strong>Outcomes-based Practice</strong></h3>
                        <strong><br><p><strong>Competency Video</strong></p>
                           <iframe allowfullscreen="" frameborder="0" height="274" src="http://www.youtube.com/embed/ChxUrZskSNs?rel=0" width="360"></iframe><br><br>
                           <iframe allowfullscreen="" frameborder="0" height="274" src="http://www.youtube.com/embed/MT0RT-FgjbE?rel=0" width="360"></iframe>
                           
                           </strong>
                        
                        
                        <p><strong>Competency &nbsp;</strong></p>
                        
                        <p><em>The Essential Competency areas of Outcomes-based Practice and Assessment work hand
                              in hand, but they are not the same thing. Outcomes-based Practice is the process of
                              identifying what the learner should be able to do as a direct result of teaching/learning
                              activities. Effective assessment helps us measure the level at which students achieve
                              these desired outcomes. Creating appropriate outcomes is a different area for study
                              and practice, crucial in establishing expectations for students</em>. 
                        </p>
                        
                        <p>Valencia educators will design and implement learning activities that intentionally
                           lead students towards mastery in the Student Core Competencies (Think, Value, Communicate,
                           and Act) as well as the related course and program outcomes. 
                        </p>
                        
                        <p>The key question is “What will students be able to do as a result of the instruction?”</p>
                        
                        <p><strong>Performance Indicators:  Evidence of Learning<br>
                              </strong><span><em>Tenure candidates demonstrating this competency must select at least one indicator
                                 that
                                 includes the student core competencies (Think, Value, Communicate, and Act).</em></span></p>
                        
                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>The faculty member will</strong></p>
                        
                        <ul>
                           
                           <li>create a new, or revised, learning outcome for a unit, course or program that
                              meets the criteria for learning outcomes (this performance indicator must be used
                              in conjunction with at least one other Outcomes-based Practice indicator for
                              demonstration in faculty portfolios)
                           </li>
                           
                           <li>align unit, course, and/or program outcomes with one or more student core
                              competencies (Think, Value, Communicate &amp; Act)
                           </li>
                           
                           <li>collect evidence of progress toward student achievement of unit, course, or
                              program learning outcomes
                           </li>
                           
                           <li>sequence learning opportunities and assessments throughout units, courses,
                              programs, and developmental advising to build student understanding and
                              knowledge
                           </li>
                           
                           <li>help students understand their growth in the acquisition of student core
                              competencies (Think, Value, Communicate &amp; Act) and program learning
                              outcomes
                           </li>
                           
                           <li>use evidence of student learning to review and improve units, courses, and
                              programs (in classroom, counseling and library settings)
                           </li>
                           
                           <li>ensure that unit, course, and program learning outcomes are current and
                              relevant for future academic work and/or vocational and employment
                              opportunities.
                           </li>
                           
                        </ul>            
                        <p><br>
                           
                        </p>
                        
                        <p><em>Indicators provide examples of how competencies can be demonstrated in an Individualized
                              Learning Plan. While all competencies need to be demonstrated, only some  indicators
                              need to be employed in the ILP process.</em></p>
                        
                        <p><a href="../../../../competencies/default.html" target="_blank">Click here for Valencia Student Core Competencies and General Education Outcomes.
                              </a></p>
                        
                        <p><a href="tla_lcore_tvca_lcf.html" target="_blank">Click here for additional resources on the Student Core Competencies.</a></p>
                        
                        <p><a href="../../coursesResources/index.html" target="_blank">Click here for additional resources on Outcomes-based Practice.</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/tla_student_competencies_lcf.pcf">©</a>
      </div>
   </body>
</html>