<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Information for Tenure Candidates</h2>
                        
                        
                        <ul>
                           
                           <li><a href="documents/class-of-2022-year-1-syllabus.pdf" target="_blank">Class of 2022 Syllabus</a></li>
                           
                           <li><a href="documents/class-of-2021-year-2-syllabus.pdf" target="_blank">Class of 2021 Syllabus</a></li>
                           
                           <li>
                              <a href="documents/class-of-2020-year-3-syllabus.pdf" target="_blank">Class of 2020 Syllabus</a> 
                           </li>
                           
                           <li><a href="documents/class-of-2019-year-4-syllabus.pdf" target="_blank">Class 2019 Syllabus</a></li>
                           
                           <li>
                              <a href="documents/class-of-2018-year-5-syllabus.pdf" target="_blank">Class 2018 Syllabus</a>  
                           </li>
                           
                           <li><a href="documents/CylceofSupport.pdf" target="_blank">Cycle of Support</a></li>
                           
                           <li><a href="../../centers/locations.html" target="_blank">Centers for Teaching/Learning Innovation</a></li>
                           
                           <li><a href="documents/5-year-Reporting-Schedule-2017-18.pdf" target="_blank">Reporting Schedule 2017-18</a></li>
                           
                        </ul>            
                        
                        <p>These pages contain the necessary information a candidate needs to design and develop
                           the Analysis of My Practice (AMP) in year-1, which develops into the Individualized
                           Learning Plan (ILP) in year-2, and culminates in the Faculty Portfolio in year-3.
                           
                        </p>
                        
                        <div>
                           
                           <ul>
                              
                              <li data-id="#tab-1">Year-1: </li>
                              
                              <li data-id="#tab-2">Year-2:</li>
                              
                              <li data-id="#tab-3">Year-3: </li>
                              
                              <li data-id="#tab-4">Year-4/5:</li>
                              
                              <li data-id="#tab-5">Tenure Classes: </li>
                              
                              
                           </ul>
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <p><a href="documents/Tenure-Candidate-Workbook.pdf" target="_blank">Foundations in Learning Centered Teaching: Year 1 Round-table Workbook</a></p>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><strong>Needs Assessment </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/Analysis-of-My-Practice-Template.docx">Anaylsis of My Practice Template</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/AnalysisofMyPracticeFeedbackForm.docx">Analysis of My Practice Feedback Form</a> 
                                             </li>
                                             
                                             
                                             <li><a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/2016-Revised_MicroTeachTemplate_7.28_sp.docx">Micro-teach Instructions &amp; Template</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <div>                      
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="tla_competencies_LCF.html">Essential Competencies of a Valencia Educator </a><br>
                                                
                                             </li>
                                             
                                             <li><a href="documents/EssentialCompetenciesIndicatorsWorksheet9.29.16.doc" target="_blank">Essential Competencies Indicators Worksheet</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><strong>Models/Samples </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                <a href="documents/AMP-Tom-Baselice.pdf" target="_blank">Sample Accounting Analysis of My Practice </a> 
                                             </li>
                                             
                                             <li><a href="documents/AMP-Rita-Luther.pdf" target="_blank">Sample Biology Analysis of My Practice</a></li>
                                             
                                          </ul>
                                       </div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                <a href="documents/AMP-Laura-Magness.pdf" target="_blank">Sample Psychology Analysis of My Practice </a> 
                                             </li>
                                             
                                             <li><a href="documents/AMP-Jennifer-Papoula.pdf" target="_blank">Sample Counselor Analysis of My Practice</a></li>
                                             
                                          </ul>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           
                           <div>
                              
                              <h3>Developing Your ILP</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><strong>ILP Explanations </strong></div>
                                       </div>
                                       
                                       <div>
                                          <div><strong>Constructing an ILP Step-by-Step </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                <a href="ilp_what.html">What is an ILP?</a> <br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/graphicDescriptionOfILP.pdf" target="_blank">What is an ILP? Graphic</a><br>
                                                
                                             </li>
                                             
                                             <li><a href="documents/ILP-instructions.pdf" target="_blank">Constructing and ILP Step-by-step</a></li>
                                             
                                             <li><a href="documents/ILP-Template-Diana.pdf" target="_blank">Final ILP Humanities</a></li>
                                             
                                          </ul>
                                       </div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                <a href="documents/DraftChartof5YearTenureProcessPostSummit.pdf" target="_blank">Cycle of Support</a> <br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/ILPinstructions.pdf" target="_blank">Creating an ILP Instructions</a><br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="PhilosophyDevelopment.html">Developing a Philosophy Statement </a>
                                                
                                             </li>                           
                                             
                                             <li><a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/ILP-Template.docx" target="_blank">ILP Submission Form </a></li>
                                             
                                          </ul>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><strong>ILP Elements </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                
                                                <p><a href="FLO_definitions.html">Faculty Learning Outcomes: Definition &amp; Examples</a></p>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <p><a href="documents/THEsaurusofVerbsandMLOCriteria-1.pdf" target="_blank">Learning Outcomes: Verb Thesaurus &amp; Criteria</a> 
                                                </p>
                                                
                                             </li>
                                             
                                          </ul>
                                       </div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                
                                                <p><a href="documents/WritingMeasurableLearningOutcomesUpdated9.4.12_000.pdf" target="_blank">How to Write a Measurable Learning Outcome for an ILP </a></p>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <p><a href="documents/MeasurableFLOWorksheet.pdf" target="_blank">Worksheet to Assess a Faculty Learning Outcome</a></p>
                                                
                                             </li>
                                             
                                          </ul>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><strong>Sample Resume and ILPs </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                <a href="documents/HaroldLewisVITArevised10.20.16.pdf" target="_blank">Sample Resume</a><br>
                                                
                                             </li>
                                             
                                             <li><a href="documents/ILP-Template-Baselice.pdf" target="_blank">Sample Accounting ILP Submission Form</a></li>
                                             
                                          </ul>
                                       </div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/ILP-Template-Bowling.pdf" target="_blank">Sample Tech Draft/Design ILP Submission Form</a></li>
                                             
                                          </ul>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/ILP-Template-Lacoste.pdf" target="_blank">Sample Math ILP Submission Form</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Developing Your Portfolio</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><strong>Constructing Your Portfolio </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="documents/RequiredNewElementsofaValenciaFacultyPortfolio6.12.pdf" target="_blank">Portfolio Elements</a> <br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/PortfolioReportForm6.9.14.docx" target="_blank">Portfolio Report Form</a><br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="tla_standards_schol.html">Standards of Scholarship</a><br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/HowtoLogintotheActionResearchBuilder.pdf" target="_blank">Access the Action Research Project Builder</a> <em><br>
                                                   </em>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/WP_portfolios-1.docx" target="_blank">Word Processing Requirements</a><em><br>
                                                   This document describes the requirements and deadlines for using Word Processing for
                                                   portfolio duplication. </em> 
                                             </li>
                                             
                                          </ul>                    
                                       </div>
                                       
                                       <div>
                                          <ul>
                                             
                                             <li>
                                                
                                                <p><a href="documents/WritingMeasurableLearningOutcomesUpdated9.4.12.pdf" target="_blank">How to Write a Measurable Learning Outcome for an ILP </a></p>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <p><a href="documents/MeasurableFLOWorksheet.pdf" target="_blank">Worksheet to Assess a Faculty Learning Outcome</a></p>
                                                
                                             </li>
                                             
                                          </ul>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><strong>Templates</strong></div>
                                       </div>
                                       
                                       <div>
                                          <div><strong>Models/Samples</strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                <div>
                                                   <a href="http://bit.ly/TLA-FacultyPortfolio" target="_blank">Website Portfolio Shell</a><br>
                                                   <em>For assistance in using this shell, consult the Faculty Support Center Manager on
                                                      your campus. Using this shell requires the submission of a Web Development Request
                                                      Form (see below). <br>
                                                      </em>
                                                   
                                                   <ul>
                                                      
                                                      <li><em><a href="http://net4.valenciacollege.edu/forms/office-of-information-technology/web/request.cfm" target="_blank">Web Development Request Form</a><br>
                                                            <em>Complete this online form to request space for your electronic porfolio on the Valencia
                                                               server.</em> <br>
                                                            </em></li>
                                                      
                                                   </ul>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   <a href="PortfolioTemplate_Final.onepkg" target="_blank">Electronic (OneNote) Portfolio Template</a><br>
                                                   <em>For assistance in using this template, consult the Faculty Support Center Manager
                                                      on your campus.</em>
                                                   
                                                   <ul>
                                                      
                                                      <li>
                                                         <a href="documents/OneNotePortfolioTemplateTutorial.pdf" target="_blank">Directions for OneNote Portfolio Template</a><br>
                                                         
                                                      </li>
                                                      
                                                      <li>
                                                         
                                                         <div>
                                                            <a href="PortfolioTemplateModel_final.onepkg" target="_blank">Sample OneNote Mathematics Portfolio: Julia Nudel, Class of 2011</a> (<strong>OneNote</strong> Template)<br><p>
                                                               <em></em></p>
                                                            
                                                         </div>
                                                         
                                                      </li>
                                                      
                                                      
                                                   </ul>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/COMPLETED_PortfolioTemplate_Word_Revised_7182013.docx" target="_blank">Word Portfolio Template</a><br>
                                                   <em>This template provides content instructions for each element of the portfolio and
                                                      follows the recommended organization format. Simply download this Word document to
                                                      cut and paste content into the appropriate sections. </em>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/PortfolioTemplate_Revised_7172012.docx">Word Portfolio Template</a> 
                                             </li>
                                             
                                          </ul>                    
                                          
                                          <blockquote>
                                             
                                             <p>(with Word generated Table of Contents)</p>
                                             
                                             <p><em>This template includes a Table of Contents that can populate page numbers automatically.
                                                   Review the Tip Sheet and Video Tutorial for help with using the Table of Contents.</em> 
                                             </p>
                                             
                                             <div>
                                                
                                                <blockquote>
                                                   
                                                   <p><a href="documents/FacultyTipSheetEditingPortfolioTemplate_Jan2013.pdf" target="_blank">Tip Sheet for Editing the Portfolio Template</a> 
                                                   </p>
                                                   
                                                   <p><a href="http://screencast.com/t/Auhp3ADlhT" target="_blank">Video Tutorial on Editing the Template </a></p>
                                                   
                                                   <p><a href="documents/HowtoCreateaBookmarkedPDF.pdf">How to Create a Bookmarked PDF Portfolio from the Template</a></p>
                                                   
                                                </blockquote>
                                                
                                             </div>
                                             
                                          </blockquote>                    
                                       </div>
                                       
                                       <div>
                                          
                                          <p>Model Action Research Projects <br>
                                             
                                          </p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CourtneyMoore_ARP.pdf" target="_blank">Model Librarian ARP: Courtney Moore</a></li>
                                             
                                             <li><a href="documents/DerekSchorsch_ARP.pdf" target="_blank">Model Pyschology ARP: Derek Schorsch</a></li>
                                             
                                             <li><a href="documents/WandaStanek_ARP.pdf" target="_blank">Model Nursing ARP: Wanda Stanek </a></li>
                                             
                                             <li><a href="documents/ZachHyde_ARP2.pdf" target="_blank">Model English ARP: Zachary Hyde </a></li>
                                             
                                             <li><a href="documents/DiegoDiaz_ARP.pdf" target="_blank">Model Chemistry ARP: Diego Diaz </a></li>
                                             
                                          </ul>                    
                                          
                                          <p>Model Portfolios <br>
                                             
                                          </p>
                                          
                                          <p><em>A sample portfolio provides an example of the portfolio structue  in a specific template.
                                                Model portfolios provide guidance on current standards of both structure and content.
                                                </em></p>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                <div>
                                                   <span>Model Portfolios:&nbsp; Counseling, Librarianship, English, Chemistry (Library Reserve
                                                      Desk, each campus)</span><br>
                                                   <br>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   <a href="documents/PortfolioLauraSessions_Model1.pdf" target="_blank"> Model Chemistry Portfolio: Laura Sessionsl, Class of 2014 </a>(<strong>Word</strong> Template) 
                                                </div>
                                                <br>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   <a href="https://onedrive.live.com/view.aspx?resid=64B300D31DEA50D!466&amp;ithint=onenote,&amp;app=OneNote&amp;authkey=!AKtib1wL9b4kzjA" target="_blank">Model Librarian Portfolio: Karene Best, Class of 2015</a> (OneNote Template) <br>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>  
                           
                           
                           
                           
                           <div>
                              
                              <h3> Advanced Practice Plan and Final Portfolio </h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="documents/AdvancedPracticePlanTemplate.withevalcomments.doc" target="_blank">Advanced Practice Plan</a> (to be completed by candidate)<br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/AdvancedPracticePlanReport.docx" target="_blank">Advanced Practice Plan Report</a> (to be completed by dean)<br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/AdvancedPracticeReflection.doc" target="_blank">Advanced Practice Reflection</a> (to be completed by candidate)<br>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://valenciacollege.edu/faculty/development/tla/Candidate/documents/AdvancedPracticeReflectionReport.docx" target="_blank">Advanced Practice Reflection Report</a> (to be completed by dean) <br>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>  
                           
                           
                           
                           <div>
                              
                              <h3>Tenure Colleagues</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li> <a href="documents/class-of-2018-tenure-roster.pdf" target="_blank">Tenure Class of 2018</a>
                                                
                                             </li>
                                             
                                             <li><a href="documents/class-of-2019-tenure-roster.pdf" target="_blank">Tenure Class of 2019</a></li>
                                             
                                             <li><a href="documents/class-of-2020-tenure-roster.pdf" target="_blank">Tenure Class of 2020</a></li>
                                             
                                             <li><a href="documents/class-of-2021-tenure-roster.pdf" target="_blank">Tenure Class of 2021</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <h3>&nbsp;</h3>
                              
                           </div>  
                           
                           
                        </div>
                        
                        
                        
                        
                        <h3>Click the image to view the Portfolio Development Overview Summary</h3>
                        
                        
                        <div>
                           <img alt="Portfolio Overview" border="0" height="760" src="portfolioOverview_new2016_000.jpg" title="Portfolio Overview" width="760">
                           <img alt="Portfolio Overview Summary" border="0" height="760" src="portfolioOverviewText_new2016.jpg" title="Portfolio Overview Summary" width="760">
                           
                        </div>       
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/default.pcf">©</a>
      </div>
   </body>
</html>