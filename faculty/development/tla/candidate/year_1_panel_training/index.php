<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Year-1 ILP Review Panel Meeting | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/year_1_panel_training/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/candidate/year_1_panel_training/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Year 1 Panel Training</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="banner" role="banner">
                  
                  <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/banner_custom.png" alt="" style="border:0;" class="banner">
                  
                  
                  <h1>Year 1 Panel Training</h1>
                  
                  <h2>Teaching and Learning Academy</h2>
                  
                  
               </div>
               
               
               
               <div id="nav" role="navigation">
                  
                  <div id="printall">
                     <a href="javascript:courseScore()">score</a><a href="Year_1_Panel_Training_print.html" target="_blank">print all</a>
                     
                  </div>
                  <a href="index.html#endofnav"><img title="skip navigation" alt="skip navigation" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/spacer.gif" style="border:0; float:left" width="0" height="0"></a>
                  
                  
                  <ul>
                     
                     <li>&nbsp;&nbsp;Page:</li>
                     
                     <li>&nbsp;&nbsp;1&nbsp;&nbsp;</li>
                     
                     <li> | <a class="navnum" href="Year_1_Panel_Training2.html" title="Candidate's Context">&nbsp;&nbsp;2&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="Year_1_Panel_Training3.html" title="Faculty Learning Outcomes">&nbsp;&nbsp;3&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="Year_1_Panel_Training4.html" title="Effective Presentation of the Entire ILP">&nbsp;&nbsp;4&nbsp;&nbsp;</a>
                        
                     </li>
                     
                  </ul>
                  
                  
               </div>
               
               
               <div id="contentWrap" class="clearfix">
                  
                  
                  <div id="sidebar" role="complementary">
                     
                     <div id="endofnav"></div>
                     
                     
                     <div class="feature">
                        <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/onthispage_custom.png" alt="On This Page sidebar">
                        
                        <ul>
                           
                           <li><a href="index.html#headingtaglink-1">Year-1 ILP Review Panel Meeting</a></li>
                           
                           <li><a href="index.html#headingtaglink-2">Year-1 Review: The Individualized Learning Plan At-A-Glance</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                     <div class="feature2">
                        <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/resources_custom.png" alt="More Resources sidebar">
                        
                        <p><a target="_blank" href="../../../programs/tla/ILP/index.html">Panel Information</a></p>
                        
                        <p><a target="_blank" href="../../../programs/tla/Candidate/dean_resources.html">Sample Y-1 Report Form</a></p>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div id="content" class="softchalkWrapper" role="main">
                     
                     
                     <a id="headingtaglink-1"></a><h1>Year-1 ILP Review Panel Meeting</h1>
                     
                     <p><img width="350" height="262" alt="meeting_room.jpg" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/meeting_room.jpg" metadata="obj2" class="resizable" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"></p>
                     
                     <p>This presentation will help you understand the criteria used to assess an Individualized
                        Learning Plan (ILP). Panel members need to understand the directions candidates receive
                        for developing their ILPs. &nbsp; In each section, you will read the directions candidates
                        receive and the criteria statements from the Year-1 Report Form.
                     </p>
                     
                     <p>In addition to the written ILP, candidates are required to discuss their understanding
                        of the Essential Competencies of a Valencia Educator and answer questions about their
                        FLOs, philosophy, and Context. &nbsp; Panels should follow the agenda provided by the TLA.
                        &nbsp; The candidate provides only the written ILP; no other portfolio artifacts are required
                        at this time. The majority of the ILP the panel will review and provide comments or
                        recommendations for improvements. &nbsp; Only the FLO statements and the candidate's understanding
                        of the Essential Competencies are assessed for achievement level.
                     </p>
                     <a id="headingtaglink-2"></a><h3>Year-1 Review: The Individualized Learning Plan At-A-Glance</h3>
                     
                     <p><strong>Candidate Explains</strong></p>
                     
                     <ul>
                        
                        <li>Workload Context</li>
                        
                        <li>Philosophy of teaching, counseling or librarianship</li>
                        
                        <li>FLOs as they reflect candidate's professional need</li>
                        
                        <li>Understanding of Essential Competencies (artifacts not necessary at this point)</li>
                        
                        <li>Professional Development efforts/plans</li>
                        
                     </ul>
                     
                     <p><strong>Panelists Verify (yes or no) and offer suggestions</strong></p>
                     
                     <ul>
                        
                        <li>Workload Context discussed</li>
                        
                        <li>Philosophy described</li>
                        
                        <li>Faculty Learning Outcomes discussed</li>
                        
                        <li>Professional efforts described</li>
                        
                     </ul>
                     
                     <p><strong>Panelists Assess</strong></p>
                     
                     <ul>
                        
                        <li>Candidate's understanding of Essential Competencies (Artifacts not necessary at this
                           point; candidates explain understanding in general terms and by giving examples from
                           their practices and often from their FLOs)
                        </li>
                        
                        <li>Faculty Learning Outcomes Statements</li>
                        
                     </ul>
                     
                     <p><strong>Effective Written Reports</strong></p>
                     
                     <ul>
                        
                        <li>Provide specific and constructive feedback/assessment</li>
                        
                     </ul>
                     
                     <p><strong>Report Schedule</strong></p>
                     
                     <ul>
                        
                        <li>Report sent to candidate, campus president, and TLA within 2 weeks of the review</li>
                        
                        <li>See the Reporting Schedule for complete reporting process (<a href="../default.html" target="_blank">Candidate resources</a> in the list at the top of the page)
                        </li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     
                     
                     
                     <br>
                     
                     <p class="nav2"><a href="index.html#">return to top</a> | <a href="Year_1_Panel_Training2.html">next page</a></p>
                     
                     
                  </div>
                  
                  
                  
                  <div id="floatingscore" style="text-align:center;position:fixed;bottom:0px;right:0px;margin:0 60px 100px 0;overflow:auto;" role="status">
                     <a href="javascript:closebar()"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/close_float.png" style="float:right;border:0;" alt="Click to close"></a><br>
                     <span id="my_score_span" style="font-family: Arial, Helvetica, sans-serif;"></span><p></p>
                     
                  </div>
                  
               </div>
               
               
               
               <div id="footer" role="contentinfo">
                  
                  <p>
                     Content ©2016. All Rights Reserved.<br>
                     Date last modified: September 8, 2016.<br>
                     <a class="textpopper" href="javascript:void(0);" onmouseover="return overlib('&amp;lt;div style=\'text-align:center;\'&amp;gt;SoftChalk 10.01.05&amp;lt;/div&amp;gt;', PADX, 3, 3, PADY, 3, 3, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 70, TEXTSIZE, 1);" onmouseout="nd();"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/spacer.gif" style="border:0;" width="200" height="6" alt="hidden textpopper"></a></p>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/year_1_panel_training/index.pcf">©</a>
      </div>
   </body>
</html>