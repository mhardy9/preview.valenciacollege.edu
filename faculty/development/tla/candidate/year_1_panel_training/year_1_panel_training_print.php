<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Year 1 Panel Training | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/year_1_panel_training/year_1_panel_training_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/candidate/year_1_panel_training/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li><a href="/faculty/development/tla/candidate/year_1_panel_training/">Year 1 Panel Training</a></li>
               <li>Year 1 Panel Training</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Year 1 Panel Training</strong><br>Teaching and Learning Academy
               </p>
               
               
               <h1>Year-1 ILP Review Panel Meeting</h1>
               
               <p>
                  <img width="350" height="262" alt="meeting_room.jpg" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/meeting_room.jpg" metadata="obj2" class="resizable" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p>This presentation will help you understand the criteria used to assess an Individualized
                  Learning Plan (ILP). Panel members need to understand the directions candidates receive
                  for developing their ILPs. &nbsp; In each section, you will read the directions candidates
                  receive and the criteria statements from the Year-1 Report Form.
               </p>
               
               <p>In addition to the written ILP, candidates are required to discuss their understanding
                  of the Essential Competencies of a Valencia Educator and answer questions about their
                  FLOs, philosophy, and Context. &nbsp; Panels should follow the agenda provided by the TLA.
                  &nbsp; The candidate provides only the written ILP; no other portfolio artifacts are required
                  at this time. The majority of the ILP the panel will review and provide comments or
                  recommendations for improvements. &nbsp; Only the FLO statements and the candidate's understanding
                  of the Essential Competencies are assessed for achievement level.
               </p>
               
               <h3>Year-1 Review: The Individualized Learning Plan At-A-Glance</h3>
               
               <p>
                  <strong>Candidate Explains</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Workload Context</li>
                  
                  <li>Philosophy of teaching, counseling or librarianship</li>
                  
                  <li>FLOs as they reflect candidate's professional need</li>
                  
                  <li>Understanding of Essential Competencies (artifacts not necessary at this point)</li>
                  
                  <li>Professional Development efforts/plans</li>
                  
               </ul>
               
               <p>
                  <strong>Panelists Verify (yes or no) and offer suggestions</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Workload Context discussed</li>
                  
                  <li>Philosophy described</li>
                  
                  <li>Faculty Learning Outcomes discussed</li>
                  
                  <li>Professional efforts described</li>
                  
               </ul>
               
               <p>
                  <strong>Panelists Assess</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Candidate's understanding of Essential Competencies (Artifacts not necessary at this
                     point; candidates explain understanding in general terms and by giving examples from
                     their practices and often from their FLOs)
                  </li>
                  
                  <li>Faculty Learning Outcomes Statements</li>
                  
               </ul>
               
               <p>
                  <strong>Effective Written Reports</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Provide specific and constructive feedback/assessment</li>
                  
               </ul>
               
               <p>
                  <strong>Report Schedule</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Report sent to candidate, campus president, and TLA within 2 weeks of the review</li>
                  
                  <li>See the Reporting Schedule for complete reporting process (Candidate resources in
                     the list at the top of the page)
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Candidate's Context</h1>
               
               <p>The Candidate's Context is discussed during the Year 1-ILP Review Meeting. Panelists
                  will provide comments and recommendations for improvements. It is not until the year
                  two meeting that the panel will mark the philosophy as exemplary, acceptable, or not
                  yet acceptable. &nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp; <img id="inlineqp1" src="ph_qp1.png.html" style="border: 0"></p>
               
               <h2>Educational &amp; Professional Background</h2>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>This is a brief resume that merely highlights your professional background. The purpose
                     is for your panelists to recognize your accomplishment at Valencia outside your ILP
                     work. This is not an extensive resume. This is part of the written ILP and is not
                     evaluated by your panel.</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following information: Does the Candidate's Context
                  offer insight into the relevant educational &amp; professional background (not as an extensive
                  resume)?
               </p>
               
               <p>&nbsp;</p>
               
               <h2>Candidate's Workload Context</h2>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>Explain your workload. Try to be as specific as possible. If you are a professor,
                     mention the number and types of classes (preparations) that you have, as well as any
                     lab or clinical work you do. If you are a program coordinator, mention that here.
                     Librarians and counselors should explain the particular work they are doing and will
                     be expected to do. Describe any other professional duties that you have as part of
                     your work at Valencia, including your participation in TLA activities<strong>.</strong></em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following information: Does the Candidate's Context
                  include course titles, preps, labs, clinicals, coordinator positions, etc.
               </p>
               
               <p>&nbsp;</p>
               
               <p>The panel should have a clear picture of the candidate's workload to help identify
                  if the faculty learning outcomes are reasonable to fit within the candidate's schedule.
                  Clarification of preparations, student engagement hours, and department requirements
                  may be suggested if it is not clear.
               </p>
               
               <p>&nbsp;</p>
               
               <h2>Candidate's Professional Strengths</h2>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>Describe what you consider to be your strengths as an instructor, counselor or librarian.
                     Describe in broad terms the skills, abilities &amp; experiences you bring to your position.</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>On the Panel Report Form, <u>the pane</u>l will provide feedback based on the following criteria: Candidate describes in broad
                  terms the skills, abilities &amp; experiences brought to the position.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <h2>Candidate's Professional Philosophy</h2>
               
               <p>
                  <img width="300" height="189" alt="philosophy.gif" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/philosophy.gif" metadata="obj1" class="resizable" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>The professional philosophy describes how and why you conduct your professional practice.
                     It should overtly influence your course products, such as syllabi, policies, and daily
                     lessons, and it should be unique to you and your field/discipline. More specifically,
                     the philosophy provides concrete examples reflecting your role (teacher, librarian,
                     or counselor), the role of your students, your instructional strategies, and your
                     assessment methods. The FLO's in your ILP should be reflected in the Philosophy statement.</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: &nbsp; In 1-2 pages, candidate
                  explains "how do I conduct my professional practice," and "why do I choose that way."
                  Examples to illustrate how the candidate's philosophy is reflected in his/her practice
                  are provided.
               </p>
               
               <p>&nbsp;</p>
               
               <p>At this meeting, the panel will offer suggestions for improvements. The goal is to
                  help the candidate work toward an acceptable/exemplary philosophy in the year two
                  meeting. &nbsp; By the year two meeting, the philosophy should be insightful, clearly written,
                  and support teaching and learning. &nbsp; In the same way, the portfolio work should align
                  with aspects of the philosophy. &nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Faculty Learning Outcomes</h1>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>Faculty Learning Outcome, in terms of a tenure candidate's ILP, is a statement of
                     what you should learn and be able to do to improve student learning during your tenure-track
                     process. FLO statements articulate the major goals that you defined, in collaboration
                     with your dean. The FLO statement answers "What does the tenure candidate need to
                     learn at this point in the development of his or her practice?" The FLO should be
                     compatible with division needs. The FLO statement must be learning-centered, assessable,
                     specific to the individual, and related to the Essential Competencies. It must also
                     be demonstrable in a product or performance that can be judged according to explicit
                     criteria. Clearly stated FLO statements should be understandable to colleagues across
                     disciplines. The description of the FLO is expressed in one sentence. Three Faculty
                     Learning Outcomes are recommended; however, in some cases two are sufficient.</em>
                  
               </p>
               
               <h2>Guidelines for Assessing the Faculty Learning Outcomes</h2>
               
               <p>The Year-1 ILP Review Panel Report form identifies the criteria to guide the panel's
                  comments and recommendations for improvements. &nbsp; Before assessing each Faculty Learning
                  Outcome, the panel will answer the following questions:
               </p>
               
               <p>&nbsp;</p>
               
               <ul>
                  
                  <li><strong>Is the FLO clearly connected to the candidate's Needs Assessment?</strong></li>
                  
                  <li><strong>Is the FLO describing a learning result related to improving student learning ?</strong></li>
                  <em>Explains what the faculty member will be able to do in terms of improving student
                     learning, as applicable.</em>
                  
                  <li><strong>Is the FLO specific?</strong></li>
                  <em>It should measure no more than one result/trait.</em>
                  
                  <li><strong>Is the FLO Action-oriented?</strong></li>
                  <em>The faculty member should be able to take action as a result of findings.</em>
                  
                  <li><strong>Is the FLO Cognitively Appropriate?</strong></li>
                  <em>The FLO should have an action verb that identifies desired cognitive level of faculty
                     and student learning.</em>
                  
                  <li><strong>Is the FLO Clearly stated?</strong></li>
                  <em>The meaning should be clear to all disciplines.</em>
                  
                  <li>
                     <strong>Is the FLO Assessable?</strong> The measurable results should be achievable.
                  </li>
                  
               </ul>
               
               <p>These questions will help the panel evaluate each FLO as acceptable or incomplete/not
                  yet acceptable. &nbsp; The panel may mark "no" on some questions, but still assess the
                  FLO as acceptable. &nbsp; Panelists may have suggestions for the candidate's FLO, but still
                  assess the FLO as acceptable. &nbsp; If the panel marks a FLO as incomplete/not yet acceptable,
                  the candidate is required to make the changes and resubmit the ILP to his/her dean
                  for approval before continuing with his/her portfolio process (see below under the
                  heading "Evaluation of FLO's for Achievement Level").
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp; <img id="inlineqp2" src="ph_qp2.png.html" style="border: 0"></p>
               
               <p>The faculty learning outcome is written in one sentence and measures only one result.
                  Panelists should be careful in asking faculty members to be too specific in the type
                  of learning activities they choose. &nbsp; Many candidates are still researching activities
                  and learning what will work best in their disciplines and teaching style. &nbsp; Once the
                  FLO is marked acceptable by the panel, candidates are locked into that FLO (unless
                  changes are approved by the panel).
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/act_icon202.png" alt="Show activity" class="popupimagecontainer">&nbsp;
               </p>
               
               <h2>Evaluation of FLO's for Achievement Level</h2>
               
               <p>In the Year-1 ILP Review Panel Meeting, <u>the panel</u> will identify each of the FLOs as acceptable or incomplete/not yet acceptable. The
                  criteria statement for determining if the overall FLO is acceptable is the following:
                  FLO is clear, assessable, relates to teaching &amp; learning and needs assessment.
               </p>
               
               <p>If an FLO is marked 'not yet acceptable', the candidate is not able to move forward
                  with his/her FLO. Once the candidate receives the completed Year-1 ILP Review Panel
                  Report, he/she will have two weeks to make the appropriate changes and resubmit to
                  the dean. The dean will approve or suggest additional changes for the candidate to
                  make, and forward to the other panel members.
               </p>
               
               <p>If an FLO is marked 'acceptable' but recommendations for improvements are made, the
                  candidate is allowed to proceed with his/her FLO. The candidate will be instructed
                  by the panel and TLA facilitator to make all changes suggested by the panel before
                  proceeding.
               </p>
               
               <p>&nbsp;</p>
               
               <h2>Feedback on the FLO</h2>
               
               <p>
                  <strong>Essential Competencies:</strong>
                  
               </p>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>List the Essential Competencies and Indicators you plan on demonstrating in the FLO.
                     Explain how each Competency will be addressed in the FLO by identifying the indicator(s)
                     for each Essential Competency listed. See the indicator worksheet.</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: Are the applicable Essential
                  Competencies identified?
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<strong>Conditions:</strong></p>
               
               <p>
                  <img width="340" height="280" alt="College_Course_Schedules_ALL_340x280.jpg" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/College_Course_Schedules_ALL_340x280.jpg" metadata="obj6" class="resizable" style="border: 1px solid #000000; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px">
                  
               </p>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>Explain how you will limit or narrow the scope of your FLO (i.e., for a course, unit,
                     lesson, etc.). Include course and/or unit and time line for implementation and completion.)</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: Are the conditions identified?
                  &nbsp; Examples of conditions that provide focus to the FLOs include the following:
               </p>
               
               <ul>
                  
                  <li>Course and/or unit of study</li>
                  
                  <li>Time line for implementation and completion</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Products of Learning:</strong>
                  
               </p>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>What evidence of learning will you produce to demonstrate achievement in your FLO?
                     Examples: learning unit(s), rubric(s), elsson/unit plan(s), conference presentation(s),
                     Valencia presentation(s), scholarly publication(s), plan for Action Research Project,
                     professional certification(s), formative and summative assessment instruments, course
                     design(s).</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: Are the products/evidence
                  of learning identified? Examples of products and/or performances that reflect evidence
                  of learning (candidate learning in terms of student learning):&nbsp; &nbsp;
               </p>
               
               <ul>
                  
                  <li>Learning unit(s)</li>
                  
                  <li>Rubric(s)</li>
                  
                  <li>Lesson/unit plan(s)</li>
                  
                  <li>Conference presentation(s)</li>
                  
                  <li>Valencia presentation(s)</li>
                  
                  <li>Scholarly publication(s)</li>
                  
                  <li>Plan for Action Research Project</li>
                  
                  <li>Professional certification(s)</li>
                  
                  <li>Formative and summative assessment instruments</li>
                  
                  <li>Course design(s)</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Professional Development:</strong>
                  
               </p>
               
               <p><img width="250" height="114" alt="TLA_logo.png" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/TLA_logo.png" metadata="obj5" class="resizable" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"> On the Panel Report Form <u>the panel</u> will provide feedback based on the following criteria: Are the professional development
                  efforts/plans described? Examples of the professional development plans that should
                  be addressed in the ILP include the following:
               </p>
               
               <ul>
                  
                  <li>Year-1 Seminar Series the candidate has successfully completed.</li>
                  
                  <li>Year-2 and Year-3 seminars and courses the candidate plans to attend.</li>
                  
                  <li>Any other professional development activities such as graduate courses completed,
                     conferences attended, books read, and/or journal articles read.
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <h2>Research Question</h2>
               
               <p>
                  <span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span>
                  
               </p>
               
               <p>
                  <em>The research question must be included here and should be polished and clearly align
                     with the FLO. You may have more than one research question. The following prompts
                     can help you identify the question that will be the basis for your Action Research
                     Project. Answer the questions that are relevant to your project as a way to focus
                     your ideas. Remember, action research questions are about what you are doing, not
                     what other people are doing.</em>
                  
               </p>
               
               <ul>
                  
                  <li>What is the question?</li>
                  
                  <li>Is there a persistent problem or area of concern in my class/professional setting?</li>
                  
                  <li>What is my concern? Why am I concerned?</li>
                  
                  <li>What kind of evidence can I produce to show the situation as it is?</li>
                  
                  <li>Is there a teaching method I would like to explore/incorporate in my practice?</li>
                  
                  <li>Is there a topic in my discipline I would like to teach or present differently?</li>
                  
                  <li>Would I like to explore alternative methods of assessment?</li>
                  
                  <li>Would I like to have evidence of the effectiveness of something I am currently doing?</li>
                  
                  <li>Have I seen an Action Research question that I would like to alter/modify/investigate
                     in my practice?
                  </li>
                  
                  <li>Will I be able to follow this question through multiple semesters? Is the question
                     appropriate for Action Research?
                  </li>
                  
                  <li>Is it significant and related to improving student learning?</li>
                  
                  <li>How will finding the answer inform my teaching?</li>
                  
                  <li>Is it under my control (teaching strategies, classroom activities, student assignments)?</li>
                  
                  <li>Is it feasible in terms of time, effort and available resources?</li>
                  
                  <li>Can I take action based on my results?</li>
                  
                  <li>Is this an area where I am willing to make a change?</li>
                  
                  <li>Am I willing to make a change based on my results?</li>
                  
                  <li>How might I address the question?</li>
                  
                  <li>Can I clearly state the Research Question (or hypothesis) that I want to test?</li>
                  
                  <li>Can I clearly state the goal or set the standard to which I want to compare my students'
                     achievements?
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>Depending on the FLO identified in the Action Research Plan, the candidate may have
                  more than one research question. The panel will only be providing strengths and/or
                  recommendations for improvements rather than evaluating for an achievement level.
                  The Year-1 ILP Review Panel Report form identifies the following criteria to provide
                  feedback on the Research Question (s):
               </p>
               
               <ul>
                  
                  <li>Clearly connected to the candidate's FLO?</li>
                  
                  <li>Significant and related to improving student learning?</li>
                  
                  <li>Methods under the candidate's control?</li>
                  
                  <li>Feasible in terms of time, effort and available resources?</li>
                  
                  <li>Clearly stated?</li>
                  
                  <li>Addressing no more than one result/trait (in each RQ)?</li>
                  
                  <li>A question that can be answered by the data that will be collected?</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center"></p>
               
               <h1>Effective Presentation of the Entire ILP</h1>
               
               <p>In the Year-1 ILP Review Panel Meeting, <u>the panel</u> will identify the Effective Presentation of the Entire ILP as exemplary, acceptable
                  or incomplete/not yet acceptable. The criteria statement for determining if the overall
                  FLO is acceptable includes the following: The ILP is written clearly and coherently;
                  presented &amp; edited professionally. To be marked as exemplary the ILP should also be
                  a polished presentation.
               </p>
               
               <p>
                  <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/essentialCompetencies_new2016_webSM.jpg" width="360" height="363" class="resizable" metadata="obj3" alt="essentialCompetencies_sm.jpg" style="margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px; float: right; border: 0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <h2>Oral Discussion of the Essential Competencies</h2>
               
               <p>Candidates will explain their understanding of each Essential Competency in general
                  terms and give examples from their practices and/or their FLO ideas. Since candidates
                  may have only conceptualized and not begun to implement their FLOs at the end of Y-1,
                  they may not be able to provide specific examples from their FLOs. Candidates should
                  be able to describe how they plan to add depth and specificity to their understanding
                  of the Essential Competencies as they work on their FLOs and complete their portfolios.
               </p>
               
               <p>On the Panel Report Form, <u>the panel</u> will evaluate the candidate's comprehension-level understanding of the Essential
                  Competancies as exemplary, acceptable or not yet acceptable. The criteria used to
                  evaluate the candidate's understanding includes the following:
               </p>
               
               <p>
                  <strong>Exemplary:</strong>
                  
               </p>
               
               <p>Candidate has gained comprehension-level understanding of the Essential Competency
                  and can relate this understanding to classroom / professional practice.
               </p>
               
               <p>
                  <strong>Acceptable:</strong>
                  
               </p>
               
               <p>Candidate has gained comprehension-level understanding of the Essential Competency.</p>
               
               <p>
                  <strong>Not Yet Acceptable:</strong>
                  
               </p>
               
               <p>Candidate has not yet gained comprehension-level understanding of the Essential Competency.</p>
               
               <p>&nbsp;</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/year_1_panel_training/year_1_panel_training_print.pcf">©</a>
      </div>
   </body>
</html>