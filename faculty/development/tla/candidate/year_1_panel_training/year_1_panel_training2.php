<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Candidate's Context | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/year_1_panel_training/year_1_panel_training2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/candidate/year_1_panel_training/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li><a href="/faculty/development/tla/candidate/year_1_panel_training/">Year 1 Panel Training</a></li>
               <li>Candidate's Context</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="banner" role="banner">
                  
                  <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/banner_custom.png" alt="" style="border:0;" class="banner">
                  
                  
                  <h1>Year 1 Panel Training</h1>
                  
                  <h2>Teaching and Learning Academy</h2>
                  
                  
               </div>
               
               
               
               <div id="nav" role="navigation">
                  
                  <div id="printall">
                     <a href="javascript:courseScore()">score</a><a href="Year_1_Panel_Training_print.html" target="_blank">print all</a>
                     
                  </div>
                  <a href="Year_1_Panel_Training2.html#endofnav"><img title="skip navigation" alt="skip navigation" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/spacer.gif" style="border:0; float:left" width="0" height="0"></a>
                  
                  
                  <ul>
                     
                     <li>&nbsp;&nbsp;Page:</li>
                     
                     <li><a class="navnum" href="index.html" title="Year-1 ILP Review Panel Meeting">&nbsp;&nbsp;1&nbsp;&nbsp;</a></li>
                     
                     <li> | &nbsp;&nbsp;2&nbsp;&nbsp;</li>
                     
                     <li> | <a class="navnum" href="Year_1_Panel_Training3.html" title="Faculty Learning Outcomes">&nbsp;&nbsp;3&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | <a class="navnum" href="Year_1_Panel_Training4.html" title="Effective Presentation of the Entire ILP">&nbsp;&nbsp;4&nbsp;&nbsp;</a>
                        
                     </li>
                     
                  </ul>
                  
                  
               </div>
               
               
               <div id="contentWrap" class="clearfix">
                  
                  
                  <div id="sidebar" role="complementary">
                     
                     <div id="endofnav"></div>
                     
                     
                     <div class="feature">
                        <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/onthispage_custom.png" alt="On This Page sidebar">
                        
                        <ul>
                           
                           <li><a href="Year_1_Panel_Training2.html#headingtaglink-1">Candidate's Context</a></li>
                           
                           <li><a href="Year_1_Panel_Training2.html#headingtaglink-2">Educational &amp; Professional Background</a></li>
                           
                           <li><a href="Year_1_Panel_Training2.html#headingtaglink-3">Candidate's Workload Context</a></li>
                           
                           <li><a href="Year_1_Panel_Training2.html#headingtaglink-4">Candidate's Professional Strengths</a></li>
                           
                           <li><a href="Year_1_Panel_Training2.html#headingtaglink-5">Candidate's Professional Philosophy</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                     <div class="feature2">
                        <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/resources_custom.png" alt="More Resources sidebar">
                        
                        <p><a target="_blank" href="../../../programs/tla/ILP/index.html">Panel Information</a></p>
                        
                        <p><a target="_blank" href="../../../programs/tla/Candidate/dean_resources.html">Sample Y-1 Report Form</a></p>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div id="content" class="softchalkWrapper" role="main">
                     
                     
                     <a id="headingtaglink-1"></a><h1>Candidate's Context</h1>
                     
                     <p>The Candidate's Context is discussed during the Year 1-ILP Review Meeting. Panelists
                        will provide comments and recommendations for improvements. It is not until the year
                        two meeting that the panel will mark the philosophy as exemplary, acceptable, or not
                        yet acceptable. &nbsp;
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp; 
                        
                     </p>
                     
                     <div id="quizpopper1" style="display:inline-block;width:520px;">
                        
                        <div class="qpqvalue" style="padding: 5px 10px;">Value: 1</div>
                        
                        <div class="qpq" style="border:1px solid #c00000;background:#eeeeee;line-height:1.5em;padding:10px 15px;">
                           
                           <form name="f1" id="f1">
                              
                              <p style="display:inline;">In the year one meeting the panel should mark achievement level (exemplary/acceptable/not
                                 yet acceptable) for which of the following candidate's context?
                              </p>
                              
                              <div style="margin: 1em 15px;">
                                 <input type="radio" name="q1" value="a" id="q1a">&nbsp;<label for="q1a">
                                    <p style="display:inline;"><strong>a.</strong>&nbsp;Educational &amp; Professional Background
                                    </p></label><br>
                                 <input type="radio" name="q1" value="b" id="q1b">&nbsp;<label for="q1b">
                                    <p style="display:inline;"><strong>b.</strong>&nbsp;Candidate's Workload
                                    </p></label><br>
                                 <input type="radio" name="q1" value="c" id="q1c">&nbsp;<label for="q1c">
                                    <p style="display:inline;"><strong>c.</strong>&nbsp;Candidate's Professional Strength
                                    </p></label><br>
                                 <input type="radio" name="q1" value="d" id="q1d">&nbsp;<label for="q1d">
                                    <p style="display:inline;"><strong>d.</strong>&nbsp;Teaching Philosophy
                                    </p></label><br>
                                 <input type="radio" name="q1" value="e" id="q1e">&nbsp;<label for="q1e">
                                    <p style="display:inline;"><strong>e.</strong>&nbsp;All of the above
                                    </p></label><br>
                                 <input type="radio" name="q1" value="f" id="q1f">&nbsp;<label for="q1f">
                                    <p style="display:inline;"><strong>f.</strong>&nbsp;None of the above
                                    </p></label><br>
                                 
                              </div>
                              
                              <div style="text-align:center;">
                                 <input onclick="check_q(1, 6, 1, true)" type="button" name="Check" value="Check Answer">
                                 
                              </div>
                              
                           </form>
                           
                           <div class="collapse qpqfeedback" id="f_done1" style="border-top: 1px solid #c00000; margin: 1em;"></div>
                           
                           <div class="collapse" id="feed1" style="font-family: Comic Sans MS; margin: 1em;"></div>
                           
                        </div>
                        
                     </div>
                     
                     <p> </p>
                     <a id="headingtaglink-2"></a><h2>Educational &amp; Professional Background</h2>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>This is a brief resume that merely highlights your professional background. The purpose
                           is for your panelists to recognize your accomplishment at Valencia outside your ILP
                           work. This is not an extensive resume. This is part of the written ILP and is not
                           evaluated by your panel.</em></p>
                     
                     <p>&nbsp;</p>
                     
                     <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following information: Does the Candidate's Context
                        offer insight into the relevant educational &amp; professional background (not as an extensive
                        resume)?
                     </p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-3"></a><h2>Candidate's Workload Context</h2>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>Explain your workload. Try to be as specific as possible. If you are a professor,
                           mention the number and types of classes (preparations) that you have, as well as any
                           lab or clinical work you do. If you are a program coordinator, mention that here.
                           Librarians and counselors should explain the particular work they are doing and will
                           be expected to do. Describe any other professional duties that you have as part of
                           your work at Valencia, including your participation in TLA activities<strong>.</strong></em></p>
                     
                     <p>&nbsp;</p>
                     
                     <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following information: Does the Candidate's Context
                        include course titles, preps, labs, clinicals, coordinator positions, etc.
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>The panel should have a clear picture of the candidate's workload to help identify
                        if the faculty learning outcomes are reasonable to fit within the candidate's schedule.
                        Clarification of preparations, student engagement hours, and department requirements
                        may be suggested if it is not clear.
                     </p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-4"></a><h2>Candidate's Professional Strengths</h2>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>Describe what you consider to be your strengths as an instructor, counselor or librarian.
                           Describe in broad terms the skills, abilities &amp; experiences you bring to your position.</em></p>
                     
                     <p>&nbsp;</p>
                     
                     <p>On the Panel Report Form, <u>the pane</u>l will provide feedback based on the following criteria: Candidate describes in broad
                        terms the skills, abilities &amp; experiences brought to the position.
                     </p>
                     
                     <p>&nbsp;&nbsp;</p>
                     <a id="headingtaglink-5"></a><h2>Candidate's Professional Philosophy</h2>
                     
                     <p><img width="300" height="189" alt="philosophy.gif" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/philosophy.gif" metadata="obj1" class="resizable" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"></p>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>The professional philosophy describes how and why you conduct your professional practice.
                           It should overtly influence your course products, such as syllabi, policies, and daily
                           lessons, and it should be unique to you and your field/discipline. More specifically,
                           the philosophy provides concrete examples reflecting your role (teacher, librarian,
                           or counselor), the role of your students, your instructional strategies, and your
                           assessment methods. The FLO's in your ILP should be reflected in the Philosophy statement.</em></p>
                     
                     <p>&nbsp;</p>
                     
                     <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: &nbsp; In 1-2 pages, candidate
                        explains "how do I conduct my professional practice," and "why do I choose that way."
                        Examples to illustrate how the candidate's philosophy is reflected in his/her practice
                        are provided.
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>At this meeting, the panel will offer suggestions for improvements. The goal is to
                        help the candidate work toward an acceptable/exemplary philosophy in the year two
                        meeting. &nbsp; By the year two meeting, the philosophy should be insightful, clearly written,
                        and support teaching and learning. &nbsp; In the same way, the portfolio work should align
                        with aspects of the philosophy. &nbsp;
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     
                     
                     
                     <br>
                     
                     <p class="nav2"><a href="Year_1_Panel_Training2.html#">return to top</a> | <a href="index.html">previous page</a> | <a href="Year_1_Panel_Training3.html">next page</a></p>
                     
                     
                  </div>
                  
                  
                  
                  <div id="floatingscore" style="text-align:center;position:fixed;bottom:0px;right:0px;margin:0 60px 100px 0;overflow:auto;" role="status">
                     <a href="javascript:closebar()"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/close_float.png" style="float:right;border:0;" alt="Click to close"></a><br>
                     <span id="my_score_span" style="font-family: Arial, Helvetica, sans-serif;"></span><p></p>
                     
                  </div>
                  
               </div>
               
               
               
               <div id="footer" role="contentinfo">
                  
                  <p>
                     Content ©2016. All Rights Reserved.<br>
                     Date last modified: September 8, 2016.<br>
                     <a class="textpopper" href="javascript:void(0);" onmouseover="return overlib('&amp;lt;div style=\'text-align:center;\'&amp;gt;SoftChalk 10.01.05&amp;lt;/div&amp;gt;', PADX, 3, 3, PADY, 3, 3, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 70, TEXTSIZE, 1);" onmouseout="nd();"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/spacer.gif" style="border:0;" width="200" height="6" alt="hidden textpopper"></a></p>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/year_1_panel_training/year_1_panel_training2.pcf">©</a>
      </div>
   </body>
</html>