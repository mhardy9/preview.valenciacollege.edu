<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Learning Outcomes | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/year_1_panel_training/year_1_panel_training3.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/tla/candidate/year_1_panel_training/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li><a href="/faculty/development/tla/candidate/year_1_panel_training/">Year 1 Panel Training</a></li>
               <li>Faculty Learning Outcomes</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="banner" role="banner">
                  
                  <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/banner_custom.png" alt="" style="border:0;" class="banner">
                  
                  
                  <h1>Year 1 Panel Training</h1>
                  
                  <h2>Teaching and Learning Academy</h2>
                  
                  
               </div>
               
               
               
               <div id="nav" role="navigation">
                  
                  <div id="printall">
                     <a href="javascript:courseScore()">score</a><a href="Year_1_Panel_Training_print.html" target="_blank">print all</a>
                     
                  </div>
                  <a href="Year_1_Panel_Training3.html#endofnav"><img title="skip navigation" alt="skip navigation" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/spacer.gif" style="border:0; float:left" width="0" height="0"></a>
                  
                  
                  <ul>
                     
                     <li>&nbsp;&nbsp;Page:</li>
                     
                     <li><a class="navnum" href="index.html" title="Year-1 ILP Review Panel Meeting">&nbsp;&nbsp;1&nbsp;&nbsp;</a></li>
                     
                     <li> | <a class="navnum" href="Year_1_Panel_Training2.html" title="Candidate's Context">&nbsp;&nbsp;2&nbsp;&nbsp;</a>
                        
                     </li>
                     
                     <li> | &nbsp;&nbsp;3&nbsp;&nbsp;</li>
                     
                     <li> | <a class="navnum" href="Year_1_Panel_Training4.html" title="Effective Presentation of the Entire ILP">&nbsp;&nbsp;4&nbsp;&nbsp;</a>
                        
                     </li>
                     
                  </ul>
                  
                  
               </div>
               
               
               <div id="contentWrap" class="clearfix">
                  
                  
                  <div id="sidebar" role="complementary">
                     
                     <div id="endofnav"></div>
                     
                     
                     <div class="feature">
                        <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/onthispage_custom.png" alt="On This Page sidebar">
                        
                        <ul>
                           
                           <li><a href="Year_1_Panel_Training3.html#headingtaglink-1">Faculty Learning Outcomes</a></li>
                           
                           <li><a href="Year_1_Panel_Training3.html#headingtaglink-2">Guidelines for Assessing the Faculty Learning Outcomes</a></li>
                           
                           <li><a href="Year_1_Panel_Training3.html#headingtaglink-3">Evaluation of FLO's for Achievement Level</a></li>
                           
                           <li><a href="Year_1_Panel_Training3.html#headingtaglink-4">Feedback on the FLO</a></li>
                           
                           <li><a href="Year_1_Panel_Training3.html#headingtaglink-5">Research Question</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                     <div class="feature2">
                        <img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/resources_custom.png" alt="More Resources sidebar">
                        
                        <p><a target="_blank" href="../../../programs/tla/ILP/index.html">Panel Information</a></p>
                        
                        <p><a target="_blank" href="../../../programs/tla/Candidate/dean_resources.html">Sample Y-1 Report Form</a></p>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div id="content" class="softchalkWrapper" role="main">
                     
                     
                     <a id="headingtaglink-1"></a><h1>Faculty Learning Outcomes</h1>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>Faculty Learning Outcome, in terms of a tenure candidate's ILP, is a statement of
                           what you should learn and be able to do to improve student learning during your tenure-track
                           process. FLO statements articulate the major goals that you defined, in collaboration
                           with your dean. The FLO statement answers "What does the tenure candidate need to
                           learn at this point in the development of his or her practice?" The FLO should be
                           compatible with division needs. The FLO statement must be learning-centered, assessable,
                           specific to the individual, and related to the Essential Competencies. It must also
                           be demonstrable in a product or performance that can be judged according to explicit
                           criteria. Clearly stated FLO statements should be understandable to colleagues across
                           disciplines. The description of the FLO is expressed in one sentence. Three Faculty
                           Learning Outcomes are recommended; however, in some cases two are sufficient.</em></p>
                     <a id="headingtaglink-2"></a><h2>Guidelines for Assessing the Faculty Learning Outcomes</h2>
                     
                     <p>The Year-1 ILP Review Panel Report form identifies the criteria to guide the panel's
                        comments and recommendations for improvements. &nbsp; Before assessing each Faculty Learning
                        Outcome, the panel will answer the following questions:
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <ul>
                        
                        <li><strong>Is the FLO clearly connected to the candidate's Needs Assessment?</strong></li>
                        
                        <li><strong>Is the FLO describing a learning result related to improving student learning ?</strong></li>
                        <em>Explains what the faculty member will be able to do in terms of improving student
                           learning, as applicable.</em>
                        
                        <li><strong>Is the FLO specific?</strong></li>
                        <em>It should measure no more than one result/trait.</em>
                        
                        <li><strong>Is the FLO Action-oriented?</strong></li>
                        <em>The faculty member should be able to take action as a result of findings.</em>
                        
                        <li><strong>Is the FLO Cognitively Appropriate?</strong></li>
                        <em>The FLO should have an action verb that identifies desired cognitive level of faculty
                           and student learning.</em>
                        
                        <li><strong>Is the FLO Clearly stated?</strong></li>
                        <em>The meaning should be clear to all disciplines.</em>
                        
                        <li>
                           <strong>Is the FLO Assessable?</strong> The measurable results should be achievable.
                        </li>
                        
                     </ul>
                     
                     <p>These questions will help the panel evaluate each FLO as acceptable or incomplete/not
                        yet acceptable. &nbsp; The panel may mark "no" on some questions, but still assess the
                        FLO as acceptable. &nbsp; Panelists may have suggestions for the candidate's FLO, but still
                        assess the FLO as acceptable. &nbsp; If the panel marks a FLO as incomplete/not yet acceptable,
                        the candidate is required to make the changes and resubmit the ILP to his/her dean
                        for approval before continuing with his/her portfolio process (see below under the
                        heading "Evaluation of FLO's for Achievement Level").
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp; 
                        
                     </p>
                     
                     <div id="quizpopper2" style="display:inline-block;width:520px;">
                        
                        <div class="qpqvalue" style="padding: 5px 10px;">Value: 1</div>
                        
                        <div class="qpq" style="border:1px solid #c00000;background:#eeeeee;line-height:1.5em;padding:10px 15px;">
                           
                           <form name="f2" id="f2">
                              
                              <p style="display:inline;">If the panel marks 'no' and/or provides comments in the Guidlines for Assessing the
                                 Faculty Learning Outcome, the FLO should be "not yet acceptable".
                              </p>
                              
                              <div style="margin: 1em 15px;">
                                 <input type="radio" name="q2" value="true" id="q2a">&nbsp;<label for="q2a">
                                    <p style="display:inline;">True</p></label><br>
                                 <input type="radio" name="q2" value="false" id="q2b">&nbsp;<label for="q2b">
                                    <p style="display:inline;">False</p></label><br>
                                 
                              </div>
                              
                              <div style="text-align:center;">
                                 <input onclick="check_q(2, 2, 2, true)" type="button" name="Check" value="Check Answer">
                                 
                              </div>
                              
                           </form>
                           
                           <div class="collapse qpqfeedback" id="f_done2" style="border-top: 1px solid #c00000; margin: 1em;"></div>
                           
                           <div class="collapse" id="feed2" style="font-family: Comic Sans MS; margin: 1em;"></div>
                           
                        </div>
                        
                     </div>
                     
                     <p> </p>
                     
                     <p>The faculty learning outcome is written in one sentence and measures only one result.
                        Panelists should be careful in asking faculty members to be too specific in the type
                        of learning activities they choose. &nbsp; Many candidates are still researching activities
                        and learning what will work best in their disciplines and teaching style. &nbsp; Once the
                        FLO is marked acceptable by the panel, candidates are locked into that FLO (unless
                        changes are approved by the panel).
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp;<a href="Year_1_Panel_Training3.html#" class="openModal" ref="activityref_6" id="y6_"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/act_icon202.png" alt="Show activity" class="popupimagecontainer picnofloat"></a>&nbsp;
                     </p>
                     
                     
                     <div class="hideqpstuff"><a href="Year_1_Panel_Training3.html#endofactivity6"><img title="go to end of activity" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/spacer.gif" style="width:0;height:0;border:0;" alt="spacer"></a></div>
                     
                     <div id="activityref_6" class="greymatter-inner" style="width:345px;margin:0 auto;">
                        
                        
                        <table class="table ">
                           
                           <tr>
                              
                              <td style="padding:0px;text-align:left;vertical-align:bottom;width:23px;"><a href="ada_files/ada_activity6.html" target="_blank"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/ada-access.png" style="padding-left:3px;width:20;height:20;border:0;" title="alternative accessible content" alt="alternative accessible content"></a></td>
                              
                              <td style="padding:0px 5px;">Click the bottom arrows to view the slides. Click the text icon to bring up more information.</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td colspan="2" style="text-align:center;padding:0px;">
                                 
                                 <div id="activity-6" class="activityborder" style="width: 295px; height: 420px; border: 1px solid #c00000; background-color: #eeeeee;">
                                    
                                    <div style="text-align:center;margin-top:200px;font-weight:bold;">This content requires JavaScript enabled.</div>
                                    
                                 </div>
                                 
                                 
                              </td>
                              
                           </tr>
                           
                        </table>
                        
                     </div>
                     
                     <div id="endofactivity6"></div>
                     
                     
                     <a id="headingtaglink-3"></a><h2>Evaluation of FLO's for Achievement Level</h2>
                     
                     <p>In the Year-1 ILP Review Panel Meeting, <u>the panel</u> will identify each of the FLOs as acceptable or incomplete/not yet acceptable. The
                        criteria statement for determining if the overall FLO is acceptable is the following:
                        FLO is clear, assessable, relates to teaching &amp; learning and needs assessment.
                     </p>
                     
                     <p>If an FLO is marked 'not yet acceptable', the candidate is not able to move forward
                        with his/her FLO. Once the candidate receives the completed Year-1 ILP Review Panel
                        Report, he/she will have two weeks to make the appropriate changes and resubmit to
                        the dean. The dean will approve or suggest additional changes for the candidate to
                        make, and forward to the other panel members.
                     </p>
                     
                     <p>If an FLO is marked 'acceptable' but recommendations for improvements are made, the
                        candidate is allowed to proceed with his/her FLO. The candidate will be instructed
                        by the panel and TLA facilitator to make all changes suggested by the panel before
                        proceeding.
                     </p>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-4"></a><h2>Feedback on the FLO</h2>
                     
                     <p><strong>Essential Competencies:</strong></p>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>List the Essential Competencies and Indicators you plan on demonstrating in the FLO.
                           Explain how each Competency will be addressed in the FLO by identifying the indicator(s)
                           for each Essential Competency listed. See the indicator worksheet.</em></p>
                     
                     <p>&nbsp;</p>
                     
                     <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: Are the applicable Essential
                        Competencies identified?
                     </p>
                     
                     <p>&nbsp;</p>
                     
                     <p>&nbsp;<strong>Conditions:</strong></p>
                     
                     <p><img width="340" height="280" alt="College_Course_Schedules_ALL_340x280.jpg" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/College_Course_Schedules_ALL_340x280.jpg" metadata="obj6" class="resizable" style="border: 1px solid #000000; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"></p>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>Explain how you will limit or narrow the scope of your FLO (i.e., for a course, unit,
                           lesson, etc.). Include course and/or unit and time line for implementation and completion.)</em></p>
                     
                     <p>&nbsp;</p>
                     
                     <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: Are the conditions identified?
                        &nbsp; Examples of conditions that provide focus to the FLOs include the following:
                     </p>
                     
                     <ul>
                        
                        <li>Course and/or unit of study</li>
                        
                        <li>Time line for implementation and completion</li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     <p><strong>Products of Learning:</strong></p>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>What evidence of learning will you produce to demonstrate achievement in your FLO?
                           Examples: learning unit(s), rubric(s), elsson/unit plan(s), conference presentation(s),
                           Valencia presentation(s), scholarly publication(s), plan for Action Research Project,
                           professional certification(s), formative and summative assessment instruments, course
                           design(s).</em></p>
                     
                     <p>&nbsp;</p>
                     
                     <p>On the Panel Report Form, <u>the panel</u> will provide feedback based on the following criteria: Are the products/evidence
                        of learning identified? Examples of products and/or performances that reflect evidence
                        of learning (candidate learning in terms of student learning):&nbsp; &nbsp;
                     </p>
                     
                     <ul>
                        
                        <li>Learning unit(s)</li>
                        
                        <li>Rubric(s)</li>
                        
                        <li>Lesson/unit plan(s)</li>
                        
                        <li>Conference presentation(s)</li>
                        
                        <li>Valencia presentation(s)</li>
                        
                        <li>Scholarly publication(s)</li>
                        
                        <li>Plan for Action Research Project</li>
                        
                        <li>Professional certification(s)</li>
                        
                        <li>Formative and summative assessment instruments</li>
                        
                        <li>Course design(s)</li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     <p><strong>Professional Development:</strong></p>
                     
                     <p><img width="250" height="114" alt="TLA_logo.png" src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/TLA_logo.png" metadata="obj5" class="resizable" style="border: 0; float: right; margin-top: 10px; margin-bottom: 10px; margin-left: 30px; margin-right: 30px"> On the Panel Report Form <u>the panel</u> will provide feedback based on the following criteria: Are the professional development
                        efforts/plans described? Examples of the professional development plans that should
                        be addressed in the ILP include the following:
                     </p>
                     
                     <ul>
                        
                        <li>Year-1 Seminar Series the candidate has successfully completed.</li>
                        
                        <li>Year-2 and Year-3 seminars and courses the candidate plans to attend.</li>
                        
                        <li>Any other professional development activities such as graduate courses completed,
                           conferences attended, books read, and/or journal articles read.
                        </li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     <a id="headingtaglink-5"></a><h2>Research Question</h2>
                     
                     <p><span style="background-color: #fafab5">The directions given to <u>the candidate</u> include the following:</span></p>
                     
                     <p><em>The research question must be included here and should be polished and clearly align
                           with the FLO. You may have more than one research question. The following prompts
                           can help you identify the question that will be the basis for your Action Research
                           Project. Answer the questions that are relevant to your project as a way to focus
                           your ideas. Remember, action research questions are about what you are doing, not
                           what other people are doing.</em></p>
                     
                     <ul>
                        
                        <li>What is the question?</li>
                        
                        <li>Is there a persistent problem or area of concern in my class/professional setting?</li>
                        
                        <li>What is my concern? Why am I concerned?</li>
                        
                        <li>What kind of evidence can I produce to show the situation as it is?</li>
                        
                        <li>Is there a teaching method I would like to explore/incorporate in my practice?</li>
                        
                        <li>Is there a topic in my discipline I would like to teach or present differently?</li>
                        
                        <li>Would I like to explore alternative methods of assessment?</li>
                        
                        <li>Would I like to have evidence of the effectiveness of something I am currently doing?</li>
                        
                        <li>Have I seen an Action Research question that I would like to alter/modify/investigate
                           in my practice?
                        </li>
                        
                        <li>Will I be able to follow this question through multiple semesters? Is the question
                           appropriate for Action Research?
                        </li>
                        
                        <li>Is it significant and related to improving student learning?</li>
                        
                        <li>How will finding the answer inform my teaching?</li>
                        
                        <li>Is it under my control (teaching strategies, classroom activities, student assignments)?</li>
                        
                        <li>Is it feasible in terms of time, effort and available resources?</li>
                        
                        <li>Can I take action based on my results?</li>
                        
                        <li>Is this an area where I am willing to make a change?</li>
                        
                        <li>Am I willing to make a change based on my results?</li>
                        
                        <li>How might I address the question?</li>
                        
                        <li>Can I clearly state the Research Question (or hypothesis) that I want to test?</li>
                        
                        <li>Can I clearly state the goal or set the standard to which I want to compare my students'
                           achievements?
                        </li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     <p>Depending on the FLO identified in the Action Research Plan, the candidate may have
                        more than one research question. The panel will only be providing strengths and/or
                        recommendations for improvements rather than evaluating for an achievement level.
                        The Year-1 ILP Review Panel Report form identifies the following criteria to provide
                        feedback on the Research Question (s):
                     </p>
                     
                     <ul>
                        
                        <li>Clearly connected to the candidate's FLO?</li>
                        
                        <li>Significant and related to improving student learning?</li>
                        
                        <li>Methods under the candidate's control?</li>
                        
                        <li>Feasible in terms of time, effort and available resources?</li>
                        
                        <li>Clearly stated?</li>
                        
                        <li>Addressing no more than one result/trait (in each RQ)?</li>
                        
                        <li>A question that can be answered by the data that will be collected?</li>
                        
                     </ul>
                     
                     <p>&nbsp;</p>
                     
                     
                     
                     
                     <br>
                     
                     <p class="nav2"><a href="Year_1_Panel_Training3.html#">return to top</a> | <a href="Year_1_Panel_Training2.html">previous page</a> | <a href="Year_1_Panel_Training4.html">next page</a></p>
                     
                     
                  </div>
                  
                  
                  
                  <div id="floatingscore" style="text-align:center;position:fixed;bottom:0px;right:0px;margin:0 60px 100px 0;overflow:auto;" role="status">
                     <a href="javascript:closebar()"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/close_float.png" style="float:right;border:0;" alt="Click to close"></a><br>
                     <span id="my_score_span" style="font-family: Arial, Helvetica, sans-serif;"></span><p></p>
                     
                  </div>
                  
               </div>
               
               
               
               <div id="footer" role="contentinfo">
                  
                  <p>
                     Content ©2016. All Rights Reserved.<br>
                     Date last modified: September 8, 2016.<br>
                     <a class="textpopper" href="javascript:void(0);" onmouseover="return overlib('&amp;lt;div style=\'text-align:center;\'&amp;gt;SoftChalk 10.01.05&amp;lt;/div&amp;gt;', PADX, 3, 3, PADY, 3, 3, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 70, TEXTSIZE, 1);" onmouseout="nd();"><img src="http://valenciacollege.edu/faculty/development/tla/Candidate/Year_1_Panel_Training/spacer.gif" style="border:0;" width="200" height="6" alt="hidden textpopper"></a></p>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/year_1_panel_training/year_1_panel_training3.pcf">©</a>
      </div>
   </body>
</html>