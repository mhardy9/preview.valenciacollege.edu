<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/tla_standards_schol.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Valencia's Standards of Scholarship</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Clear Goals:</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Does the author of the project/initiative</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <blockquote>
                                       
                                       <p>State the basic purpose of his or her work clearly?<br>
                                          Define objectives that are realistic and assessable?
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Adequate Preparation:</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Does the author of the project/initiative</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <blockquote>
                                       
                                       <p>Show an understanding of relevant scholarship?<br>
                                          Bring together the necessary resources (time, financial support,  specialized training)?
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Appropriate Methods:</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Does the author of the project/initiative</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <blockquote>
                                       
                                       <p>Use methods appropriate to the goals?<br>
                                          Modify procedures in response to changing circumstances?
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Significant Results:</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Does the author of the project/initiative</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <blockquote>
                                       
                                       <p>Achieve the goals (a failed experiment is a success if learning  goals are achieved)?<br>
                                          Open additional questions for further exploration?
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Reflective Critique:</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Does the author of the project/initiative</p>
                                    
                                    <blockquote>
                                       
                                       <p>Critically evaluate his or her work?<br>
                                          Use evaluation to improve the quality of future work?
                                       </p>
                                       
                                    </blockquote>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Effective Presentation:</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Does the author of the project/initiative</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <blockquote>
                                       
                                       <p>Use a suitable style and effective organization to present his or her work?<br>
                                          Use appropriate forums for communicating work to its intended audiences?
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>Adapted From:</strong> Glassick, C. E., Huber, M.H., Maeroff, F.W. (1997). <u>Scholarship assessed evaluation of the professoriate.</u> An Ernest L. Boyer Project of the Carnegie Foundation for the Advancement of Teaching.
                           San Francisco: Jossey-Bass.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/tla_standards_schol.pcf">©</a>
      </div>
   </body>
</html>