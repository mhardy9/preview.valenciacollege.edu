<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/how_think_essay.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Essays on Teaching Excellence<br>
                           Toward the Best in the Academy
                           
                        </h2>
                        
                        <hr>
                        
                        <p><span>A   publication of The Professional &amp; Organizational Development Network in Higher
                              Education. The posting and use of this publication on your institution's WWW   server
                              is covered by an End User License Agreement (EULA). By the terms of this   agreement,
                              the essay must be located on a secure server that limits access to   your institution's
                              students, faculty, or staff. Use beyond your institutional   boundaries, including
                              the linking of this page to any site other than this one,   requires the express permission
                              of the POD Network. To view the terms of the End   User License Agreement, <a href="file://A:/Packet3/EULA.html"> click here</a>. </span><br>
                           &nbsp;
                        </p>
                        
                        <hr>
                        
                        <div> <strong>But How Do We Get Them to Think?</strong> <br>
                           <span><strong>Carol A.     Weiss, <em>Philadelphia College of Pharmacy and Science</em></strong></span>
                           
                        </div>
                        
                        <p>Whether we call it critical thinking, problem solving, analytical reasoning,   or
                           creative thinking, faculty members in all disciplines and at all levels of   higher
                           education share a common goal:&nbsp; we would like our students to be able to   perform
                           the complex mental operations that will allow them to be successful in   our classes
                           as well as their future careers.&nbsp; While improving our students'   thinking skills
                           is a clear goal, there seems to be no single recipe that will   succeed for each of
                           us and for our particular students.&nbsp; It is apparent,   however, that at least two
                           main ingredients are essential:&nbsp; we must convince   students that improving their
                           thinking skills is necessary, and we must teach   students the thinking skills that
                           they need. 
                        </p>
                        
                        <p><strong>How do we convince students that improving their thinking skills is   necessary?</strong> <br>
                           Instructors of upper-level undergraduate and graduate students may not need to   ask
                           this question.&nbsp; Instructors who attempt to teach thinking skills to   beginning-level
                           college students, however, often encounter the plea "Just tell   me what you want."&nbsp;
                           Students who are at the dualistic level of intellectual   development described by
                           Perry (1970) regard us, their instructors, as the   ultimate authorities.&nbsp; These students
                           often consider the development of their   own thinking skills to be at best a "frill"
                           and at worst an intrusion in the   REAL substance of learning as they conceptualize
                           it. 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <em>Show how thinking and learning are connected</em>. One effective method     of convincing our student skeptics that thinking and learning
                              are inseparable     processes is to emphasize from the beginning that our course has
                              two     complementary goals:&nbsp; to promote knowledge of the subject and to develop 
                              thinking skills.&nbsp; We can then demonstrate how thinking skills are used <em>for     the purpose of</em> learning the subject.&nbsp; For example, we can assist our     beginning-level students
                              in developing strategies to pick out the major     concepts or the most relevant information
                              as they approach a section of text.&nbsp;     If, as a result of practicing this skill,
                              students' performance on tests and     assignments in the course improves, their doubts
                              about the importance of     learning thinking skills will diminish.&nbsp; In addition,
                              we will have helped them     learn an ability that they will need in future courses:&nbsp;
                              identifying the most     important points in a chapter of an introductory biology
                              textbook, for     example, calls for many of the same thinking skills that identifying
                              the     salient issues in a case analysis demands in an upper-level or graduate  
                              course. 
                           </li>
                           
                           <li>
                              <em>Create a sense of "mental disequilibrium."</em> This is another     technique for motivating students at all levels to think about
                              course content     rather than merely to memorize it.&nbsp; At the beginning of each class,
                              pose a     problem or raise a controversial issue related to the day's readings and/or
                              class activities.&nbsp; Then spend a few minutes at the end of the class soliciting   
                              students' ideas about how the problem or issue might be addressed.&nbsp; Students     can
                              work individually or in groups and can be asked to present their ideas     either
                              orally or in writing.&nbsp; This brief exercise will give students the     opportunity
                              to practice applying their knowledge and will also hold them     accountable for doing
                              so.&nbsp; The technique has two additional advantages; first,     raising a question or
                              issue in advance helps students by giving them a focus     or framework for organizing
                              the material that emerges during the class     session.&nbsp; Second, student responses
                              at the end of the class help you by giving     immediate feedback about how well they
                              grasped and applied the concepts     introduced. 
                           </li>
                           
                           <li>
                              <em>Assess thinking as well as content knowledge</em>. Assignments and     examinations probably constitute the most powerful means of
                              persuading our     students that thinking skills are a necessary component for success
                              in our     classes.&nbsp; (Is there any college instructor who has not been confronted
                              with     the query "Will this be on the test?"). If we are serious about encouraging
                              our students to think more effectively, we must ask them to demonstrate both     knowledge
                              of content and mastery of thinking skills. 
                           </li>
                           
                        </ul>
                        
                        <p>There is no one "correct" type of assignment or examination question for this   purpose.&nbsp;
                           Term-paper assignments can be structured to guide even beginning-level   students
                           beyond summarizing the research and ideas of others into offering and   justifying
                           their own ideas and conclusions. <br>
                           &nbsp; 
                        </p>
                        
                        <p>Multiple-choice and short-answer test questions, if carefully constructed,   can require
                           students to go beyond lower-level thinking skills of recall and   recognition of factual
                           information (Bloom, 1956) into application and analysis.&nbsp;   For example, after reading
                           a paragraph describing the situation faced by a   manufacturing company, students
                           answering a multiple-choice question in   economics are asked to select the response
                           that will allow the company to   maximize profits.&nbsp; In order to arrive at the correct
                           answer, students must be   able to choose and apply relevant rules and calculations
                           to the situation   described (Cameron, 1991).&nbsp; Another way to make multiple-choice
                           tests into more   thought-stimulating exercises is, for a few selected questions,
                           to have students   describe their reasons for choosing or not choosing each answer
                           (Statkiewicz and   Allen, 1983). 
                        </p>
                        
                        <p>Because essay-type questions and assignments ask for open-ended response,   they are
                           useful for assessing higher level thinking and problem-solving skills.&nbsp;   Essay questions
                           are not without their pitfalls, however; a weighty question that   appears to call
                           for students to demonstrate sophisticated thinking skills may   actually ask for a
                           reiteration of information covered in class.&nbsp; The most   effective question or assignment
                           for assessing thinking skills is one that   provides opportunities for students to
                           use course-related knowledge and skills   in a new situation or on a problem that
                           they have not encountered before (Hart,   1989). 
                        </p>
                        
                        <p><strong>How do we teach our students the thinking skills they need?</strong> <br>
                           A detailed answer to this question is beyond the scope of this article; however, 
                           the following ideas can be useful for instructors in all disciplines. 
                        </p>
                        
                        <ul>
                           
                           <p><em>Model our own thinking processes</em>. This is one very powerful, but     often overlooked, technique.&nbsp; Our students are
                              often intimidated by us,     convinced that we were born competent in our subject
                              area.&nbsp; We reinforce that     illusion when we come into class time after time and
                              unerringly analyze     problems or issues in a definitive way that leads inevitably
                              to the correct or     most plausible solution.&nbsp; Most students have no idea how long
                              it has taken us     to reach our current level of competence. 
                           </p>
                           
                           <p>One writing instructor came upon a dramatic way to demonstrate that good     prose
                              does not flow magically from the author's brain onto the printed page.&nbsp;     His students
                              had been resisting the revision process.&nbsp; After asking them to     read a published
                              short story of his own, he brought to class a wastebasket     overflowing with crumpled
                              early drafts of that story.&nbsp; The whole atmosphere of     the class changed:&nbsp; seeing
                              the physical evidence of their instructor's     struggles gave the students the courage
                              to begin revising their own work. 
                           </p>
                           
                           <p>While many of us lack such tangible artifacts of our own past endeavors, we     can
                              occasionally work aloud through a problem or issue that is new to us.&nbsp;     This will
                              give students a more realistic picture of the mental efforts we must     put forth
                              when we approach novel problems or material:&nbsp; "Well, that seemed to     lead to a
                              dead end, so I better back up and see what happens when I do     this..."&nbsp; We can
                              point out the actual steps in our thinking process as we go     along:&nbsp; "At this point
                              I usually try to think of the different ways I know to     approach problems like
                              this..."&nbsp; In this way we can provide our students not     only with answers, but also
                              with valuable insights into our own individual and     discipline-related thinking
                              processes. 
                           </p>
                           
                           <li>
                              <em>Have students work together</em>. We can build on the previous     suggestion by giving students a problem or issue
                              and asking them to work in     pairs.&nbsp; one partner thinks through the problem aloud,
                              while the other partner     encourages accuracy and thoroughness by asking questions
                              such as "Why did you     take that particular step?" or "Can you explain that in more
                              detail?" (Whimbey     and Lochhead, 1986).&nbsp; In addition to involving the entire class
                              in active     participation, asking students to work aloud transforms thinking into
                              a     visible operation that can be more readily evaluated.&nbsp; This technique can help
                              students learn how to monitor the effectiveness of their own thinking     processes,
                              so that they are no longer dependent solely on us for feedback. <br>
                              &nbsp; 
                           </li>
                           
                           <li>
                              <em>Give many practice opportunities</em>. Students do not automatically     apply thinking skills they have learned to other
                              problem situations even in     the same class, much less to other courses (Salomon
                              and Perkins, 1989).&nbsp;     Whatever thinking skills we emphasize, it is critical to
                              give students many     opportunities to practice applying those skills to a diversity
                              of     course-related issues and problems. <br>
                              &nbsp; 
                           </li>
                           
                           <li>
                              <em>Collaborate with colleagues</em>. As stated above, college students seem     to have difficulty transferring thinking
                              skills from one setting to another.&nbsp;     We can work with faculty members in our own
                              and in other disciplines to     identify thinking skills common to our courses.&nbsp; We
                              can then build into the     courses assignments that will reinforce students' use
                              of those skills in a     variety of classroom and clinical situations. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>If we take the time to teach thinking skills, how will we cover all of the   content?</strong> Most of us are reluctant to omit, condense, or defer teaching any   part of the discipline
                           to which we have dedicated our professional lives.&nbsp; It   may help us resolve the dilemma,
                           however, if we view the teaching of thinking   skills as an exchange, rather than
                           as "giving up" something.&nbsp; We will be trading   a small amount of course content for
                           skills that will foster a deeper   understanding of the discipline and that will allow
                           our students to continue   learning long after they have left our classrooms. 
                        </p>
                        
                        <p><strong>References</strong></p>
                        
                        <p>Bloom, B.S. (1956)&nbsp; <em>Taxonomy of Educational Objectives</em>.&nbsp; New York:&nbsp;   Longman. 
                        </p>
                        
                        <p>Cameron, B.J. (1991) Using Tests to Teach.&nbsp; <em>College Teaching</em>, 39 (4),   154-155. 
                        </p>
                        
                        <p>Hart, K.A. (1989).&nbsp; <em>Assessing Growth in Thinking in College Classes:&nbsp; A   Caveat.&nbsp; Accent on Improving
                              College Teaching and Learning</em>.&nbsp; Ann Arbor:&nbsp;   National Center for Researh to Improve Postsecondary Teaching and
                           Learning. 
                        </p>
                        
                        <p>Perry, W.G. (1970).&nbsp; <em>Forms of Intellectual and Ethical Development in the   College Years</em>.&nbsp; New York:&nbsp; Holt, Rinehart and Winston. 
                        </p>
                        
                        <p>Salomon, G. and Perkins, D.N. (1989).&nbsp; Rocky Roads to Transfer:&nbsp; Rethinking   Mechanisms
                           of a Neglected Phenomenon.&nbsp; <em>Educational Psychologist</em>, 24(2),   113-142. 
                        </p>
                        
                        <p>Statkiewicz, W.R. and Allen, R.D. (1983).&nbsp; Practice Exercies to Develop   Critical
                           Thinking Skills.&nbsp; <em>Journal of College Science Teaching</em>, 12,   262-266. 
                        </p>
                        
                        <p>Whimbey, A., and Lochhead, J. (1986).&nbsp; <em>Problem Solving and Comprehension</em>.&nbsp;   (4th ed.).&nbsp; Hillsdale, NJ:&nbsp; Lawrence Erlbaum.&nbsp; (1744) 
                        </p>
                        
                        <p>Griffith, C. W. <em>Teaching Writing in All Disciplines. </em>New Directions   for Teaching and Learning No. 12. San Francisco: Jossey-Bass. 1982.
                           
                        </p>
                        
                        <p>Kurfiss, <strong>J. </strong>C. <em>Critical Thinking: Theory, Research, Practice, and   Possibilities. </em>ASHE-ERIC Higher Education Report No.2. Washington, D.C.:   Association for the Study
                           of Higher Education. 1988. 
                        </p>
                        
                        <p>Newell, G. "Learning from Writing in Two Content Areas: A Case Study/Protocol   Analysis."
                           <em>Research in the Teaching of English, </em>18 (3), 265-287. 1984. 
                        </p>
                        
                        <p>Weaver, F.W., ed. <em>Promoting Inquiry in Undergraduate Learning. </em>New   Directions for Teaching and Learning No. 38. San Francisco: Jossey-Bass. 1989.
                        </p>
                        
                        <hr>
                        
                        <p><span>This publication is part   of an 8-part series of essays originally published by The
                              Professional &amp;   Organizational Development Network in Higher Education. For more
                              information   about the POD Network, browse to&nbsp;</span> <a href="file://A:/lamar.colostate.edu/%7Eckfgill">http:/lamar.colostate.edu/~ckfgill</a> or <a href="file://A:/www.podweb.org">http:/www.podweb.org</a>.
                        </p>
                        
                        <p><br>
                           &nbsp;
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/how_think_essay.pcf">©</a>
      </div>
   </body>
</html>