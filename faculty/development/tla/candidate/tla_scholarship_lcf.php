<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/tla_scholarship_lcf.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h3>Scholarship of Teaching &amp; Learning </h3>
                        
                        
                        
                        <p><strong>Competency Video</strong></p>
                        <iframe allowfullscreen="" frameborder="0" height="274" src="http://www.youtube.com/embed/31F4_yt_spY?rel=0" width="360"></iframe><br>
                        
                        <p>Valencia educators will continuously examine the effectiveness of their teaching,
                           counseling, librarianship and assessment methodologies in terms of student learning.&nbsp;
                           They also will keep abreast of the current scholarship in the fields of teaching and
                           learning.
                        </p>
                        
                        <p>For tenure-track candidates, all indicators must be addressed to demonstrate this
                           Essential
                           Competency. An <a href="../actionResearch/index.html" target="_blank">Action Research Project</a> is an efficient method of demonstrating all indicators.
                        </p>
                        
                        <p> <strong>Performance Indicators:&nbsp; Evidence of Learning</strong></p>
                        
                        <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The faculty member will</strong></p>
                        
                        <ul>
                           
                           <li>produce professional scholarly work (action research or traditional research
                              related to teaching and learning, that meets the Valencia <a href="tla_standards_schol.html" target="_blank">Standards of Scholarship</a> 
                           </li>
                           
                           <li>build upon the work of others (consult experts, colleagues, self, students)</li>
                           
                           <li>be open to constructive critique (by both colleagues and students)</li>
                           
                           <li>make professional scholarly work public to college and broader
                              audiences through Valencia’s research repository and other means
                           </li>
                           
                           <li>collect evidence of the relationship of SoTL to improved teaching and learning</li>
                           
                           <li>demonstrate use of current teaching and learning theory &amp; practice</li>
                           
                        </ul>            
                        
                        <p><em>Indicators provide examples of how the Essential Competencies can be demonstrated
                              in an Individualized Learning Plan.</em></p>
                        
                        <p><a href="../../coursesResources/index.html" target="_blank">Click here for additional resources on this         Essential Competency.</a> 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/tla_scholarship_lcf.pcf">©</a>
      </div>
   </body>
</html>