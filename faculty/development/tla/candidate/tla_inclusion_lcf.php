<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/tla_inclusion_lcf.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h3>Inclusion &amp; Diversity </h3>
                        
                        
                        
                        <p><strong>Competency Video</strong></p>
                        <iframe allowfullscreen="" frameborder="0" height="274" src="http://www.youtube.com/embed/oZ1355Ts9Nw?rel=0" width="360"></iframe><br><br>
                        <iframe allowfullscreen="" frameborder="0" height="274" src="http://www.youtube.com/embed/M-jhQO_VOU8?rel=0" width="360"></iframe><br><br>
                        <iframe allowfullscreen="" frameborder="0" height="274" src="http://www.youtube.com/embed/Dm9zO4_wvQg?rel=0" width="360"></iframe>
                        
                        
                        <p>Valencia educators will design learning opportunities that acknowledge, draw upon
                           and are enriched by student diversity. Diversity has many dimensions, including sex,
                           gender identity, sexual orientation, race, ethnicity, socio-economic background, disability,
                           cognitive style, skill level, age, religion, etc. An atmosphere of inclusion and understanding
                           will be promoted in all learning environments.&nbsp;
                        </p>  
                        
                        <p><strong>Performance Indicators:&nbsp; Evidence of Learning</strong></p>
                        
                        <p><strong>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The faculty member will</strong> 
                        </p>
                        
                        <ul>
                           
                           <li>design and support learning experiences that address students’ unique strengths and/or
                              needs
                           </li>
                           
                           <li> diversify the curricular and/or co-curricular activities to increase the presence
                              of historically underrepresented groups
                           </li>
                           
                           <li>use diverse perspectives to engage and deepen critical thinking</li>
                           
                           <li>create a learning atmosphere with respect, understanding, and appreciation of individual
                              and group differences
                           </li>
                           
                           <li>challenge students to identify and question their assumptions and consider how these
                              affect, limit, and/or shape their viewpoints
                           </li>
                           
                           <li>ensure accessibility of course content in alignment with federal law and Valencia<br>
                              standards
                           </li>
                           
                        </ul>  
                        
                        <p>*Note:  diversity has many dimensions (culture, gender, race/ethnicity,  socio-economic
                           circumstances, learning styles, education background,  skill level, etc.)
                        </p>
                        
                        <p><em>Indicators  provide examples of how competencies can be demonstrated in an Individualized
                              Learning Plan. While all Essential Competencies need to be demonstrated, only  some&nbsp;
                              indicators need to be employed in the ILP process.</em></p>
                        
                        <p><a href="../../coursesResources/index.html" target="_blank">Click  here for additional resources on this competency.</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/tla_inclusion_lcf.pcf">©</a>
      </div>
   </body>
</html>