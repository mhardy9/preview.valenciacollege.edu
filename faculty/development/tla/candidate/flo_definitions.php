<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/flo_definitions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>
                           <strong>Learning Outcomes: Definitions &amp; Examples</strong> 
                        </h2>
                        
                        <p><strong>A global definition of a Learning Outcome (LO) </strong>is a statement of what the student should understand and be able to do as a result
                           of what she has learned in a course or learning program.&nbsp; We refer to these as Student
                           Learning Outcomes (SLOs).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        </p>
                        
                        <p><strong>A Learning Outcome in terms of a tenure candidate’s ILP </strong>is a statement of what the faculty member wants to learn or be able to do to improve
                           student learning. We refer to these Faculty Learning Outcomes (FLOs). 
                        </p>
                        
                        <p>The Faculty Learning Outcome answers “What does the tenure candidate need to learn
                           at this point in the development of his or her practice?” The FLO should be compatible
                           with departmental needs and defined in collaboration with one’s dean or director.
                           
                        </p>
                        
                        <p>The FLO must be learning-centered, assessable, specific to the individual, and related
                           to the <u>Essential Competencies</u>. It must also be demonstrable in a <u>product or performance</u> that can be judged according to <u>explicit criteria</u>. 
                        </p>
                        
                        <p><strong>Faculty Learning Outcomes for an ILP are phrased with the candidate’s learning in
                              mind. </strong></p>
                        
                        <p> <img alt="structure  FLO" height="166" src="FLO_structure_005.jpg" width="490">&nbsp; 
                        </p>
                        
                        <p> <em>This example would clearly demonstrate the <strong>Assessment</strong> competency, but it could also demonstrate the <strong>Outcomes-based Learning</strong> competency by focusing on the development of students’ written <strong><u>c</u></strong>ommunication skills, which is the C in TV<u>C</u>A: &nbsp;Think Value, Communicate, and Act, our Student Core Competencies.</em></p>
                        
                        <blockquote>
                           
                           <blockquote>&nbsp;</blockquote>
                           
                        </blockquote>            
                        
                        <h3><strong>Examples from Different Disciplines:</strong></h3>
                        
                        <p><strong>Mathematics Instructor’s Example Learning Outcome</strong></p>
                        
                        <p><u>Faculty Learning Outcome</u>:&nbsp; Construct alternative assessment techniques to improve students’ abilities to solve
                           quadratic equations
                        </p>
                        
                        
                        <p><strong>Humanities Instructor’s Example Learning Outcome</strong> 
                        </p>
                        
                        <p><u>Faculty Learning Outcome</u><strong>:</strong> Generate formative and summative assessment strategies to improve students’ abilities
                           to apply the core competency “value” to a unit on western religion.
                        </p>
                        
                        
                        <p><strong>Computer Programming Instructor’s Example Learning Outcome</strong> 
                        </p>
                        
                        <p><u>Faculty Learning Outcome</u><strong>:</strong> Obtain A Plus certification. 
                        </p>
                        
                        <p><em>(Note that this outcome could address only the Professional Commitment competency
                              because there is no student learning product.)</em></p>
                        
                        
                        <p><strong>Librarian’s Example Learning Outcome</strong></p>
                        
                        <p><u>Faculty Learning Outcome</u><strong>:</strong> Develop active learning activities to improve students’ abilities to conduct research
                           using the library databases.
                        </p>
                        
                        
                        <p><strong>Speech Instructor’s Example Learning Outcome</strong></p>
                        
                        <p><u>Faculty Learning Outcome</u><strong>:</strong> Develop an interactive online activity to improve students’ abilities to construct
                           a speech outline.
                        </p>
                        
                        
                        <p><strong>Counselor’s Example Learning Outcome</strong>
                           
                        </p>
                        
                        <p><u>Faculty Learning Outcome</u>:Create instructional strategies utilizing campus Career Center resources to improve
                           students’ abilities to articulate career choices.
                        </p>
                        
                        
                        <p><strong>Composition Instructor’s Example Learning Outcome</strong></p>
                        
                        <p><u>Faculty Learning Outcome</u><strong>:</strong> Design active learning strategies with appropriate assessment techniques to improve
                           students’ abilities to write a sound argument essay.
                        </p>            
                        
                        <h2>&nbsp;</h2>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/flo_definitions.pcf">©</a>
      </div>
   </body>
</html>