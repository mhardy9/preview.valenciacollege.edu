<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/tla_lifemap_lcf.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h3>LifeMap</h3>
                        
                        
                        
                        <p><strong>Competency Video</strong></p>
                        <iframe allowfullscreen="" frameborder="0" height="274" src="http://www.youtube.com/embed/hjUQ2Vk1Qqc?rel=0" width="360"></iframe><br>
                        
                        
                        <p>Valencia educators will design learning opportunities that promote student life skills
                           
                           development while enhancing discipline learning. Through intentional inclusion of
                           growthpromoting<br>
                           strategies, faculty will facilitate the students’ gradual assumption of responsibility
                           for 
                           making informed decisions and formulating and executing their educational, career,
                           and life 
                           plans.<strong> </strong>&nbsp;
                        </p>
                        
                        <p><strong>Performance Indicators:&nbsp; Evidence of Learning</strong></p>
                        
                        <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The faculty member will</strong></p>
                        
                        <ul>
                           
                           <li>establish student &amp; faculty contact that contributes to students’ academic,
                              personal, and professional growth
                           </li>
                           
                           <li>employ digital tools to aid student contact (e.g., Atlas, MyPortfolio, Blackboard,
                              Ask-A-Librarian, email, etc.)
                           </li>
                           
                           <li>seek out struggling students and identify options through dialog and appropriate
                              referrals
                           </li>
                           
                           <li>help students assume responsibility for making informed academic decisions
                              (e.g., degree requirements, transfer options, financial aid, etc.)
                           </li>
                           
                           <li>guide students in developing academic behaviors for college success (e.g., time
                              management, study, test and note taking strategies, etc.)
                           </li>
                           
                           <li>help students identify academic behaviors that can be adapted as life skills (e.g.,
                              library search skills, decision-making, communication skills, scientific         
                              
                              understanding, etc.)
                           </li>
                           
                           <li>assist students in clarifying and developing purpose (attention to life, career,
                              education goals)
                           </li>
                           
                        </ul>            
                        
                        <p> <em>Indicators                 provide examples of how competencies can be demonstrated
                              in an Individualized                 Learning Plan. While all Essential Competencies
                              need to be demonstrated, only                 some indicators need to be employed
                              in the ILP process.</em></p>
                        
                        <p><a href="../../coursesResources/index.html" target="_blank">Click                 here for additional resources on this competency.</a></p>            
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/tla_lifemap_lcf.pcf">©</a>
      </div>
   </body>
</html>