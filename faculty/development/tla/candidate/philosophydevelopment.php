<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/candidate/philosophydevelopment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/tla/">Teaching/Learning Academy</a></li>
               <li><a href="/faculty/development/tla/candidate/">Candidate</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Developing a Philosophy Statement</h2>
                        
                        
                        <p><strong>How do I conduct my professional practice, and why do I choose that way?&nbsp;</strong></p>
                        
                        <p><span><strong>The following articles may help as you write your philosophy of teaching, counseling,
                                 or librarianship:</strong></span><strong></strong></p>
                        
                        <ul>
                           
                           <li><a href="http://www.learning-theories.com" target="_blank">Common Learning Theories</a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="documents/ExamplePhilosphyStatement.pdf" target="_blank">Example Philosophy Statement </a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <a href="http://fm.iowa.uiowa.edu/fmi/xsl/tgi/data_entry.xsl?-db=tgi_data&amp;-lay=Layout01&amp;-view" target="_blank">Teaching Goals Inventory</a><a href="http://www.umsl.edu/technology/frc/DEID/destination2adultlearning/2dlearningtheories.html" target="_blank"> </a>
                              
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="documents/PhilosophyWorksheet.pdf" target="_blank">Worksheet for Developing a Professional Philososphy</a></li>
                           
                        </ul>
                        
                        <p>The next articles&nbsp;help with understanding fractal patterns; they enthusiastically
                           affirm Valencia's tenure process. Tenure earning years are often viewed as "paying
                           dues" rather than "receiving." These articles present a new perspective on that front.
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="documents/DevelopingFractalPatterns-green.pdf" target="_blank">Developing in Fractal Patterns III:&nbsp; A Guide for Composing Teaching Philosophies</a> (Good for counseling and librarianship philosophies, also!)
                           </li>
                           
                        </ul>
                        
                        <h2>
                           <br>
                           &nbsp;
                        </h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/candidate/philosophydevelopment.pcf">©</a>
      </div>
   </body>
</html>