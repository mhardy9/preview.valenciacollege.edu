<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teaching/Learning Academy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/tla/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Teaching/Learning Academy</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li>Teaching/Learning Academy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <p>The Teaching/Learning Academy, a <a href="documents/CommunityofPractice.pdf" target="_blank">community of practice</a>, supports new professors, counselors, and librarians as they develop Individualized
                           Learning Plans, a fundamental phase of the tenure process designed to assist tenure
                           candidates to expand and improve their professional practices and students' learning.
                           The goal of the TLA is to help tenure-track faculty develop a reflective approach
                           to their teaching that is anchored in the tenets of action research and the Essential
                           Competencies of a Valencia Educator. The TLA provides support on pedagogy, course
                           design, student development, and professional portfolio development.
                        </p>
                        
                        <p><a href="diversity-statement.html">TLA Diversity Statement</a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/tla/index.pcf">©</a>
      </div>
   </body>
</html>