<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/howtoregisterforcourses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li>Faculty Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>How to Search and Register for Courses and View Your Transcript</h2>
                        
                        <p>Visit the Valencia EDGE to search for, find information on and register for Faculty
                           Development and Teaching/Learning Academy courses, as well as view your transcripts.
                        </p>
                        
                        <p><a href="https://youtu.be/esE8HGWCap4" target="_blank">View a video tutorial</a> <a href="https://youtu.be/esE8HGWCap4" target="_blank"><img alt="video icon" height="25" src="youtube-512.jpg" width="25"></a></p>
                        
                        <p><strong>To access the Valencia EDGE:</strong></p>
                        
                        <ul>
                           
                           <li>Log in to<strong> Atlas.</strong>
                              
                           </li>
                           
                           <li>Click on the<strong> Faculty tab.</strong>
                              
                           </li>
                           
                           <li>Click on the<strong> Access Valencia EDGE </strong>graphic in the<strong> Faculty Development Channel.</strong> A new window will open.
                           </li>
                           
                           <li>Click on<strong> My Courses</strong> on the top right. <br>
                              Here you can view the course calendar, access your transcript, search and register
                              for courses, browse the course catalog, seek advising and consultation services and
                              check your training in progress. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>To access your Faculty Development transcript:</strong></p>
                        
                        <ul>
                           
                           <li>Log in to<strong> Atlas.</strong>
                              
                           </li>
                           
                           <li>Click on the<strong> Faculty tab.</strong>
                              
                           </li>
                           
                           <li>Click on the<strong> Access Valencia EDGE </strong>graphic in the<strong> Faculty Development Channel.</strong> A new window will open.
                           </li>
                           
                           <li>Click on<strong> My Courses</strong>.
                           </li>
                           
                           <li>Click on <strong>Faculty Development Transcript</strong>.
                           </li>
                           
                           <li>Next, click on the <strong>Transcript</strong> button.
                           </li>
                           
                           <li>On the dialog box, select <strong>Ok</strong> to open, view and print your Faculty Development transcript<br>
                              
                           </li>
                           
                        </ul>
                        
                        <p>If you have questions regarding registering for courses, accessing your Faculty Development
                           transcript or the Valencia EDGE, please call or visit the <a href="centers/locations.html">Center for Teaching/Learning Innovation</a> on your campus:
                        </p>
                        
                        <p><strong>East Campus<br>
                              </strong>Building 4, Room 133<br>
                           (407) 582-2425
                        </p>
                        
                        <p><strong>Lake Nona Campus<br>
                              </strong>Building 1, Room 344 <br>
                           (407) 582-7128
                        </p>
                        
                        <p><strong>Osceola Campus<br>
                              </strong>Building 4, Room 124 <br>
                           Phone: (407) 582-4918
                        </p>
                        
                        <p><strong>Poinciana Campus<br>
                              </strong>Room 305 <br>
                           Phone: (407) 582-6131
                        </p>           
                        
                        <p><strong>West Campus<br>
                              </strong>Building 6 Room 326<br>
                           Phone: (407) 582-5826
                        </p>
                        
                        <p><strong>Winter Park Campus <br>
                              </strong>Room 126<br>
                           Phone: (407) 582-6919 
                        </p>
                        
                        <p><a href="howToRegisterForCourses.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/howtoregisterforcourses.pcf">©</a>
      </div>
   </body>
</html>