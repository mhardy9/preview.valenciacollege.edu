<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Online Teaching &amp; Learning | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/concentrations/online/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Online Teaching &amp; Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/concentrations/">Concentrations</a></li>
               <li>Online</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <p>Valencia's Office of Faculty and Instructional Development offers many opportunities
                           for faculty members to explore key issues regarding teaching and learning in the web-enhanced,
                           hybrid and online environments. Our courses are designed to meet the needs of diverse
                           experience levels in teaching online, ranging from the novice to experienced.
                        </p>
                        
                        <p>These courses and programs support faculty members in developing a framework of best
                           practice strategies for engaging, instructing, supporting and communicating with students
                           in the online environment. Our courses involve the following topics: 
                        </p>
                        
                        <ul>
                           
                           <li>Characteristics and unique needs of the online learner</li>
                           
                           <li>Learning styles and diversity in an online class</li>
                           
                           <li>Formative and summative assessment techniques</li>
                           
                           <li>Strategies for promoting academic integrity</li>
                           
                           <li>Strategies for engaging the online learner</li>
                           
                           <li>Online and hybrid course design and delivery</li>
                           
                           <li>Strategies for communication and interaction</li>
                           
                           <li>Current legal issues</li>
                           
                           <li>Universal design for learning</li>
                           
                           <li>Building online learning communities</li>
                           
                           <li>Facilitating online discussions </li>
                           
                           <li>Integration of learning technologies</li>
                           
                        </ul>                
                        
                        
                        
                        
                        <h3>Course Series &amp; Programs:</h3>
                        
                        <p><strong><a href="../Blackboard.html">Blackboard Fundamentals</a> - </strong>These courses are designed for faculty new to Valencia's learning management system
                           (LMS), Blackboard.
                        </p>
                        
                        <p><strong><a href="BootCampOnlineInstruction.html">Boot Camp for Online Instruction</a> - </strong>This three-day intensive course is designed for faculty members from novice to intermediate
                           experience with teaching online or Blackboard and will address the pedagogy and technology
                           essentials for teaching online.
                        </p>
                        
                        <p><strong><a href="../../programs/DigitalProfessorCertification.html">Digital Professor Certification Program</a> - </strong> This course series is designed to culminate in the certification of faculty as "Digital
                           Professors."&nbsp;This designation will indicate completion of 20 hours of professional
                           development in online/hybrid teaching and learning.&nbsp; 
                        </p>
                        
                        
                        
                        <h3>Contact for Questions or More Information</h3>    
                        
                        <div>
                           
                           <p><strong>Page Jerzak </strong></p>
                           
                           <p> Associate Director for Online Teaching and Learning <br>
                              <a href="mailto:pjerzak@valenciacollege.edu">pjerzak@valenciacollege.edu</a><br>
                              (407) 582-3865<br>
                              
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/concentrations/online/index.pcf">©</a>
      </div>
   </body>
</html>