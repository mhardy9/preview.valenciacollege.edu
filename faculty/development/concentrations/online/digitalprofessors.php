<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Online Teaching &amp; Learning | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/concentrations/online/digitalprofessors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Online Teaching &amp; Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/concentrations/">Concentrations</a></li>
               <li><a href="/faculty/development/concentrations/online/">Online</a></li>
               <li>Online Teaching &amp; Learning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Congratulations Digital Professors! </h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>James Adamski (3/2014)<br>
                                       Judith Addelston (10/2014)<br>
                                       Haris Ahmad (7/2017) <br>
                                       Maysa Alasad (12/2016)<br>
                                       Concepcion Alcaide Cranston (2/2015)<br>
                                       Joan Alexander (11/2014)<br>
                                       Malcolm Alexis (3/2011)<br>
                                       Hector Alfaro (11/2010)<br>
                                       John Allender (11/2012)<br>
                                       Jessamyn Almenas-Garcia (4/2017)<br>
                                       Benjamin Alonzo (3/2016)<br>
                                       Ruby Alvarez (6/2013)<br>
                                       Billie Amstadt (6/2014)<br>
                                       Marguerite Anderson (11/2016)<br>
                                       Jessica Anfinson (7/2017) <br>
                                       Lorie Anne Sablad (11/2013)<br>
                                       Remy Ansiello (6/2013)<br>
                                       Aron Apanah (6/2013)<br>
                                       Mea Arline (6/2016)<br>
                                       Aryan Ashkani (7/2017) <br>
                                       Alia Asi (2/2012)<br>
                                       Rhonda Atkinson (4/2010)<br>
                                       Seher Awan (6/2012)<br>
                                       Sharon Bailey (6/2015)<br>
                                       Ronda Bailey (12/2016)<br>
                                       Kate Baldridge-Hale (9/2015)<br>
                                       Jason Balserait (6/2014)<br>
                                       Victoria Barnett (11/2010)<br>
                                       Cassandra Barnhill (7/2017) <br>
                                       Deborah Barr (2/2014)<br>
                                       Dezso Bartha (6/2017) <br>
                                       Kristin Bartholomew (5/2014)<br>
                                       Jennifer Baselice (6/2015)<br>
                                       Thomas Baselice (10/2013)<br>
                                       Jeremy Bassetti (3/2016)<br>
                                       Sadia Baxter (4/2017)<br>
                                       Renee Becker (4/2015)<br>
                                       Donna Beck-Erickson (12/2013)<br>
                                       Sameena Beg (5/2016)<br>
                                       Nicholas Bekas (3/2017)<br>
                                       Megan Benson (12/2013)<br>
                                       Aaron Bergeson (10/2015)<br>
                                       Joel Berman (4/2010)<br>
                                       George Bernard (11/2011)<br>
                                       Karene Best (3/2014)<br>
                                       Peggy Bivins (4/2011)<br>
                                       Tatiana Bizon (2/2013)<br>
                                       Rebecca Blackburn (3/2014)<br>
                                       Michael Blackburn (6/2015)<br>
                                       Angela Blewitt (6/2015)<br>
                                       Karen Blondeau (5/2012)<br>
                                       Eileen Bobeck (9/2016)<br>
                                       Victor Bondzie (5/2013)<br>
                                       Eve Bouchard (5/2012)<br>
                                       Nicolle Boujaber-Diederichs (6/2012)<br>
                                       Abdallah Boumarate (2/2014)<br>
                                       Kenneth Bourgoin (5/2016)<br>
                                       Sandra Bowling (4/2017)<br>
                                       Maureen Branham (3/2016)<br>
                                       Tyler Branz (6/2017) <br>
                                       Kim Brewster (7/2017) <br>
                                       Houston Briggs (4/2011)<br>
                                       Betty Bright (11/2011)<br>
                                       Jed Broitman (01/2016)<br>
                                       Kaitlyn Brooker (3/2015)<br>
                                       Timothy Brookshire (6/2016)<br>
                                       Steve Browdy (4/2017)<br>
                                       Steve Browdyn (5/2017) <br>
                                       Veronica Brown (2/2012)<br>
                                       Barbara Browning (3/2014)<br>
                                       David Brunick (11/2009)<br>
                                       Tatyana Brusentsova (3/2013)<br>
                                       Elizabeth Bryant (6/2015)<br>
                                       Diana Budach (7/2017) <br>
                                       Lisa Bugden (6/2009)<br>
                                       Radu Bunea (6/2013)<br>
                                       Barry Bunn (11/2011)<br>
                                       Wendy Bush (11/2009)<br>
                                       Tullio Bushrui (11/2016)<br>
                                       Marsha Butler (9/2015)<br>
                                       Mia Cancarevic (2/2015)<br>
                                       Roberta Carew (5/2012)<br>
                                       Bonnie Carmack (6/2016)<br>
                                       Tracy Carpenter (2/2010)<br>
                                       Julia Carpenter (3/2016)<br>
                                       Trudi Carroll (6/2017) <br>
                                       Debra Carruth (5/2015)<br>
                                       Jennifer Carter (12/2013)<br>
                                       Sonia Casablanca (6/2015)<br>
                                       Matthew Casada (7/2017) <br>
                                       Rose Casterline (4/2017)<br>
                                       Samira Chater (2/2015)<br>
                                       Jessica Chisholm (12/2013)<br>
                                       Flora Chrisholm (6/2016)<br>
                                       Francie Chu (5/2014)<br>
                                       Nelly Cintron-Lorenzo (3/2012)<br>
                                       Lisa Cohen (11/2012)<br>
                                       Kevin Colwell (6/2012)<br>
                                       Donna Colwell (3/2016)<br>
                                       Amy Comerford (2/2010)<br>
                                       Esther Coombes (7/2017) <br>
                                       Angela Cortes (4/2013)<br>
                                       Tracey Council (9/2016)<br>
                                       Chris Cranston (12/2016)<br>
                                       Candace Cravaritis (5/2014)<br>
                                       Scott Creamer (4/2012)<br>
                                       Lois Crichlow (2/2016)<br>
                                       Joel Crichlow (6/2016)<br>
                                       Derek Croad (4/2015)<br>
                                       Elvin Cruz-Vargas (6/2013)<br>
                                       Caroline Cully (5/2013)<br>
                                       Dawna Culpepper (6/2009)<br>
                                       Nardia Cumberbatch (7/2017) <br>
                                       Cheri Cutter (4/2016)<br>
                                       Celena Cutts (7/2017) <br>
                                       Melchior Cyprien (9/2016)<br>
                                       Laura D'Alessio (4/2016)<br>
                                       Tiffany Daniels (6/2015)<br>
                                       Charles Davis (6/2012)<br>
                                       John De Graca (6/2013)<br>
                                       Angela Dean (2/2014)<br>
                                       Martin Denizard Anglero (10/2014)<br>
                                       Dan DeRosa (1/2017)<br>
                                       Marsela Dervishi (9/2016)<br>
                                       Lynn Desjarlais (3/2015)<br>
                                       Richard Dexter (4/2013)<br>
                                       Aida Diaz (10/2009)<br>
                                       Aprilis Diaz (7/2017) <br>
                                       Diego Diaz Lopez (6/2014)<br>
                                       Hillary Dickens (6/2015)<br>
                                       John DiDonna (5/2013)<br>
                                       Colleen Dieckmann (2/2012)<br>
                                       Stacey DiLiberto (10/2012)<br>
                                       Sarah Dockray (5/2013)<br>
                                       Jeffery Donley (6/2014)<br>
                                       Amy Downs (12/2013)<br>
                                       Danielle Driscoll (7/2017) <br>
                                       Debbie Drobney (10/2013)<br>
                                       Desmond Duncan (11/2011)<br>
                                       Susan Dunn (3/2012)<br>
                                       Daniel Dutkofski (10/2015)<br>
                                       LaShonda Eaddy (6/2013)<br>
                                       Elizabeth Earle (7/2010)<br>
                                       Erin Ebanks (6/2015)<br>
                                       Russell Edwards (12/2016)<br>
                                       Masood Ejaz (5/2014)<br>
                                       Dina El Musa (6/2015)<br>
                                       Holly Elliott (4/2015)<br>
                                       Emily Elrod (10/2016)<br>
                                       Magdala Emmanuel (6/2014)<br>
                                       Amita Engineer (5/2014)<br>
                                       Amber Epperson (4/2015)<br>
                                       Lauren Ermel (7/2017) <br>
                                       Hector Esteban (7/2017) <br>
                                       Dina Fabery (4/2010)<br>
                                       Debra Fahey (12/2016)<br>
                                       Elizabeth Faulcon (2/2014)<br>
                                       Corinne Fennessy (5/2014)<br>
                                       Amarilis Fernandez (7/2017) <br>
                                       Catherine Ferrer (5/2013)<br>
                                       Terri Fine (5/2013)<br>
                                       Pilar Florenz (9/2016)<br>
                                       George Flores (6/2016)<br>
                                       Andrea Foley (4/2017)<br>
                                       Kimberly Foster (4/2017)<br>
                                       Karen Fowler (4/2015)<br>
                                       Patrick Fowler (3/2012)<br>
                                       Steven Francis (4/2015)<br>
                                       David Freeman (2/2016)                
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p> Stephanie Freuler (4/2012)<br>
                                       William Freyer (2/2014)<br>
                                       Amanda Froelich (9/2015)<br>
                                       Richard Gair (4/2011)<br>
                                       Keri Gallagher (7/2017) <br>
                                       Javier Garces (4/2011)<br>
                                       Carmen Garcia (4/2016)<br>
                                       Judith Garcia Palomares (7/2017) <br>
                                       Mary Garner (2/2010)<br>
                                       Samuel Gates (4/2017)<br>
                                       Edie Gaythwaite (2/2013)<br>
                                       Courtney Gegenheimer (6/2016)<br>
                                       Claudia Genovese Martinez (9/2015)<br>
                                       Jill Geraghty (4/2011)<br>
                                       Melissa Giblin (6/2012)<br>
                                       Dalia Gil (4/2011)<br>
                                       Marlene Gillies (7/2017) <br>
                                       Kevin Giordano (9/2015)<br>
                                       Diane Gomez (4/2017)<br>
                                       Margaret Gonzalez (2/2012)<br>
                                       Yolanda Gonzalez (4/2010)<br>
                                       Jose Gonzalez (4/2011)<br>
                                       Randall Gordon (2/2017)<br>
                                       Susan Gosnell (5/2014)<br>
                                       Mahendra Gossai (11/2014)<br>
                                       Victoria Grajeda (2/2012)<br>
                                       Lauren Grant (11/2011)<br>
                                       Lisa Gray (6/2008)<br>
                                       Marilyn Greaves (3/2013)<br>
                                       Suzzanne Green (5/2014)<br>
                                       Annamarie Greller (4/2011)<br>
                                       Jeannette Griffith (6/2016)<br>
                                       Stephanie Grimes (4/2010)<br>
                                       Albert Groccia (2/2011)<br>
                                       Timothy Grogan (4/2017)<br>
                                       Barbara Gutierrez (12/2013)<br>
                                       Debbie Hall (6/2012)<br>
                                       Damion Hammock (11/2012)<br>
                                       Cathleen Hansen (3/2013)<br>
                                       Meghan Hatfield (7/2017) <br>
                                       Glenna Heath (2/2016)<br>
                                       Angela Hefka (7/2017) <br>
                                       Larry Herndon (6/2013)<br>
                                       John Hilston (3/2012)<br>
                                       Jeffrey Hogan (6/2014)<br>
                                       Heather Holden (5/2015)<br>
                                       Deidre Holmes DuBois (4/2015)<br>
                                       Natalie Holter (6/2013)<br>
                                       Mayra Holzer (3/2012)<br>
                                       Marie Howard (3/2012)<br>
                                       Lynn Howard (4/2011)<br>
                                       Deymond Hoyte (7/2017) <br>
                                       Ivan Hristov (4/2017)<br>
                                       Nely Hristova (11/2012)<br>
                                       Leann Hudson (12/2016)<br>
                                       Dennis Hunchuck (11/2016)<br>
                                       Zachary Hyde (2/2014)<br>
                                       Elizabeth Ingram (12/2013)<br>
                                       Jesusa Jackson (6/2012)<br>
                                       Judy Jackson (9/2011)<br>
                                       Debra Jacobs (12/2016)<br>
                                       Olga Jacome Utreras (6/2017) <br>
                                       Ralf Jenne (11/2011)<br>
                                       Margaret Jennings (10/2014)<br>
                                       Ying Jenny Hu (4/2014)<br>
                                       Amy Jo Felshaw (6/2016)<br>
                                       Valentine Johns (2/2012)<br>
                                       Daniela Johnson (12/2015)<br>
                                       Coleen Jones (6/2013)<br>
                                       Karen Jorgensen (6/2015)<br>
                                       Jerrid Kalakay (6/2016)<br>
                                       Rajeshwari Kalicharan (6/2014)<br>
                                       Mohua Kar (11/2010)<br>
                                       Gordana Katusic (7/2017) <br>
                                       Jennifer Keefe (10/2012)<br>
                                       Lisa Keeton (6/2015)<br>
                                       Lauren Kelley (11/2016)<br>
                                       Michael Kiely (3/2013)<br>
                                       Lisabeth King (6/2016)<br>
                                       Amanda Kirchner (6/2015)<br>
                                       Gary Kokaisel (2/2016)<br>
                                       Stephanie Kokaisel (4/2017)<br>
                                       Rachel Kolman (10/2016)<br>
                                       Shari Koopmann (2/2012)<br>
                                       Leonard Kornblau (3/2016)<br>
                                       Lindi Kourtellis (3/2012)<br>
                                       Stephen Kuiper (6/2013)<br>
                                       Vishma Kunu (4/2016)<br>
                                       Paul Labedz (2/2017)<br>
                                       Alan LaCerra (5/2012)<br>
                                       Carmen Laguer Diaz (6/2015)<br>
                                       Eric Lane (10/2012)<br>
                                       Pamela Lapinski (4/2017)<br>
                                       Laurie Larson (3/2011)<br>
                                       Andrew Lash (4/2016)<br>
                                       Jennifer Lawhon (3/2011)<br>
                                       James Lee (6/2016)<br>
                                       Shara Lee (11/2014)<br>
                                       Sonya Lenhof (5/2014)<br>
                                       Kelli Lewis (4/2016)<br>
                                       Melissa Lombardi (4/2010)<br>
                                       Kim Long (1/2017)<br>
                                       Particia Lopez (4/2011)<br>
                                       Laurinda Lott (5/2014)<br>
                                       Karen Lougheed (10/2013)<br>
                                       Dennis Lucius (6/2015)<br>
                                       Yasmin Lugo Morales (1/2017)<br>
                                       Julie Lux (4/2010)<br>
                                       Stephanie Luzoro (7/2017) <br>
                                       Joe Lynn Look (6/2008)<br>
                                       Mabel Machin (4/2011)<br>
                                       Fiona Mackay (10/2016)<br>
                                       Brian Macon (6/2008)<br>
                                       Lisa Macon (6/2008)<br>
                                       Laura Magness (5/2014)<br>
                                       Jane Maguire (3/2015)<br>
                                       Kim Mancas (11/2011)<br>
                                       Jan Mangos (10/2012)<br>
                                       Jean Marie Fuhrman (2/2010)<br>
                                       Angelica Marie Vagle (12/2015)<br>
                                       Kathleen Marquis (11/2014)<br>
                                       Amy Martello (4/2011)<br>
                                       Dewaun Martin (3/2015)<br>
                                       Lateshia Martin (11/2015)<br>
                                       Cynthia Massaad (6/2016)<br>
                                       Adrienne Mathews (3/2013)<br>
                                       Richard Matias (4/2011)<br>
                                       James May (2/2011)<br>
                                       Sharon May (2/2015)<br>
                                       Dianna McBride (1/2016)<br>
                                       Lee McCain (11/2013)<br>
                                       Kelli McCarthy (10/2015)<br>
                                       Nikita McCaskill (7/2017) <br>
                                       Jennifer McCormick (5/2013)<br>
                                       Viktoryia McGrath (2/2015)<br>
                                       Elizabeth McKenna (4/2013)<br>
                                       Scott McKenzie (12/2013)<br>
                                       Carla McKnight (5/2016)<br>
                                       Melba Medina (12/2013)<br>
                                       Michael Mendoza (6/2014)<br>
                                       Joseph Menig (6/2015)<br>
                                       Ashley Miller (2/2014)<br>
                                       Carol Miller (10/2012)<br>
                                       Katherine Miller (5/2015)<br>
                                       Tasha Mills-Foster (6/2016)<br>
                                       Mary Minion (6/2012)<br>
                                       Eric Model (5/2012)<br>
                                       Genie Mogollon (7/2017) <br>
                                       Jenny Mohess (5/2013)<br>
                                       Michael Moniz (4/2017)<br>
                                       Maria Montalvo (4/2010)<br>
                                       Julie Montione (2/2016)<br>
                                       Julianna Moring (12/2016)<br>
                                       Linda Morris-Henry (9/2016)<br>
                                       Charlotte Mortimer (5/2013)<br>
                                       Steven Muller (7/2017) <br>
                                       Brad Mundt (6/2016)<br>
                                       Antoinette Munroe (2/2017)<br>
                                       Josh Murdock (4/2011)<br>
                                       Brian Murphy (7/2017) <br>
                                       Scha-Jani Murrell (6/2013)<br>
                                       Chelsea Myers (11/2016)<br>
                                       Soheyla Nakhai (3/2013)<br>
                                       Elizabeth Nappo (2/2015)<br>
                                       Buddy Nash (6/2010)<br>
                                       Nicolas Navarro Castellanos (6/2016)<br>
                                       Joanna Nazario Grzechowiak (9/2015)<br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p> Linda Neal (6/2009)<br>
                                       Carole Nevels (6/2008)<br>
                                       Rebecca Newman (6/2013)<br>
                                       Erin O'Brien Grogan (10/2009)<br>
                                       Nalini Odapalli (12/2016)<br>
                                       Jennifer Odom (6/2017) <br>
                                       Rhonda Oehlrich (7/2017) <br>
                                       Cassandra O'Little (3/2013)<br>
                                       Kathleen O'Neal (4/2015)<br>
                                       Luisa Ortiz Balza (6/2017) <br>
                                       Melanie Osborn (2/2017)<br>
                                       Katherine Oses (2/2014)<br>
                                       Bonnie Osgood (7/2017) <br>
                                       Ian O'Toole (5/2014)<br>
                                       Ivan Padron (11/2012)<br>
                                       Gabriela Palou De Jesus (3/2012)<br>
                                       Sanika Paranjape (6/2015)<br>
                                       Juan Parra (4/2014)<br>
                                       Terry Pasfield (2/2010)<br>
                                       Ellen Pastorino (6/2014)<br>
                                       Brian Pate (7/2017) <br>
                                       Hemangi Patil (12/2015)<br>
                                       Scott Paxton (10/2015)<br>
                                       Melissa Pedone (4/2017)<br>
                                       Vanessa Permaul (7/2017) <br>
                                       Kathleen Perri (3/2013)<br>
                                       Barbara Peterson (5/2015)<br>
                                       Patra Petroongrad (11/2012)<br>
                                       Tony Philcox (12/2016)<br>
                                       Neal Phillips (10/2012)<br>
                                       Pilloud Pierre (2/2017)<br>
                                       Florence Pierre-Louis (4/2015)<br>
                                       Brian Polk (3/2011)<br>
                                       Faulys Ponceano (7/2017) <br>
                                       Robin Poole (2/2017)<br>
                                       Fritzlaine Powell (6/2016)<br>
                                       Jamie Prusak (2/2013)<br>
                                       Marva Pryor (4/2010)<br>
                                       Kamran Qadri (7/2017) <br>
                                       Terry Rafter-Carles (6/2012)<br>
                                       Kinyel Ragland (4/2014)<br>
                                       Veeramuthu Rajaravivarma (5/2014)<br>
                                       Cristian Ramirez (4/2012)<br>
                                       Heidi Ramirez (3/2013)<br>
                                       April Raneri (6/2016)<br>
                                       Blanca Rangel-Godinez (10/2012)<br>
                                       Mamta Rao (3/2015)<br>
                                       Craig Rapp (3/2014)<br>
                                       Meredith Ratliff (4/2017)<br>
                                       George Rausch (12/2013)<br>
                                       Andy Ray (6/2010)<br>
                                       Gerald Reed (5/2012)<br>
                                       Nancy Reed (3/2014)<br>
                                       Stanton Reed (3/2015)<br>
                                       Denise Richardson (6/2015)<br>
                                       Juan Rivera (5/2017)<br>
                                       Nancy Rizzo (5/2012)<br>
                                       Michael Robbins (4/2013)<br>
                                       Cheryl Robinson (6/2008)<br>
                                       Kris Rodgers (3/2014)<br>
                                       Travis Rodgers (12/2016)<br>
                                       Rachael Root (12/2016)<br>
                                       Daniel Rubin (6/2008)<br>
                                       Kevin Rushing (6/2013)<br>
                                       Yasser Saad (9/2016)<br>
                                       Kleber Saavedra (11/2012)<br>
                                       Brian Sage (2/2014)<br>
                                       Anna Saintil (3/2009)<br>
                                       Caroline Salvador (3/2016)<br>
                                       Edwin Sanchez Velez (3/2016)<br>
                                       Lester Sandres Rapalo (10/2009)<br>
                                       Liza Schellpfeffer (6/2009)<br>
                                       Derek Schorsch (6/2012)<br>
                                       Melissa Schreiber (4/2010)<br>
                                       Edward Schultz (6/2014)<br>
                                       Katrina Schultz (2/2010)<br>
                                       Regina Seguin (12/2016)<br>
                                       Janet Selitto (7/2017) <br>
                                       Laura Sessions (11/2015)<br>
                                       Melanie Sexton (9/2016)<br>
                                       Anna Sezonenko (6/2016)<br>
                                       Vasudha Sharma (2/2014)<br>
                                       Mariza Shelbrook (1/2014)<br>
                                       Sharon Shenton (3/2016)<br>
                                       Kathryn Shepard (6/2010)<br>
                                       Rebecca Shevlin (2/2013)<br>
                                       Jamie Shipley (5/2013)<br>
                                       Nichole Shorter (4/2012)<br>
                                       Keri Siler (9/2014)<br>
                                       Les Simmonds (2/2010)<br>
                                       Gaye Simpkins (7/2017) <br>
                                       Lynn Sims (6/2015)<br>
                                       Uma Singh (10/2016)<br>
                                       Allison Sloan (6/2009)<br>
                                       Crystal Smith (11/2015)<br>
                                       Mark Smith (6/2017)<br>
                                       Calvin Snyder (6/2012)<br>
                                       Michael Snyder (6/2012)<br>
                                       Keyma Sobratti (5/2015)<br>
                                       Linda Speranza (1/2017)<br>
                                       Nicole Spottke (9/2015)<br>
                                       JaNise Standberry (4/2017)<br>
                                       Wanda Stanek (3/2015)<br>
                                       Nicole Stark (7/2017) <br>
                                       Margaret Staton (2/2010)<br>
                                       Laura Stevens (4/2016)<br>
                                       Peter Stoepker (4/2015)<br>
                                       Jeremy Storm Russo (6/2008)<br>
                                       Irina Struganova (3/2012)<br>
                                       Isabella Stryker (5/2015)<br>
                                       Loree Studevan (11/2016)<br>
                                       Ronald Stumpf (5/2012)<br>
                                       Mary Sue Gausz (4/2013)<br>
                                       Michael Szalma (5/2015)<br>
                                       Patrick Szymanski (3/2014)<br>
                                       Christina Tan (6/2015)<br>
                                       Benjamin Taylor (5/2016)<br>
                                       Tori Taylor (7/2017) <br>
                                       Daeri Tenery (4/2012)<br>
                                       Ann Testa (2/2017)<br>
                                       Teresa Tharp (11/2016)<br>
                                       Lynta Thomas (3/2014)<br>
                                       Stephen Thomas (10/2014)<br>
                                       Richard Thomas (7/2017)<br>
                                       Jacquelyn Thompson (2/2017)<br>
                                       Mary Thompson (10/2014)<br>
                                       Dana Thurmond (12/2016)<br>
                                       Nelson Torres Arroyo (12/2013)<br>
                                       Ileana Trautwein (10/2014)<br>
                                       Summer Trazzera (3/2015)<br>
                                       Heather Trees (4/2015<br>
                                       Adriene Tribble (3/2013)<br>
                                       Adrienne Trier-Bieniek (7/2017) <br>
                                       Marie Trone (4/2016)<br>
                                       Samuel Tsegaye (6/2013)<br>
                                       Daniel Turner (3/2012)<br>
                                       Meenawattie Udho (4/2015)<br>
                                       Vanessa Vale Feliciano (3/2015)<br>
                                       Nicole Valentino (4/2011)<br>
                                       Sidra Van De Car (7/2017) <br>
                                       Carol Van Horn (10/2014)<br>
                                       Henry Van Putten, Jr. (5/2015)<br>
                                       Sarah Verron-Bassetti (3/2016)<br>
                                       Sylvana Vester (4/2016)<br>
                                       Nancy Vinces (3/2014)<br>
                                       James Vrhovac (1/2016)<br>
                                       LaVonda Walker (4/2016)<br>
                                       Reneva Walker (3/2013)<br>
                                       Selwyn Walters (2/2013)<br>
                                       Betty Wanielista (6/2008)<br>
                                       Russell Ward (11/2010)<br>
                                       Chris Weidman (4/2011)<br>
                                       Michael Wheaton (6/2016)<br>
                                       Lisa Whitmore (6/2016)<br>
                                       Jane Wiese (6/2010)<br>
                                       Sharalyn Wight (6/2014)<br>
                                       Dave Williams (7/2008)<br>
                                       Tarteashia Williams (6/2013)<br>
                                       Brian Williamson (2/2015)<br>
                                       Lauren Wilson (1/2016)<br>
                                       Valerie Woldman (4/2011)<br>
                                       Shannon Word (6/2013)<br>
                                       Chrysalis Wright (5/2014)<br>
                                       Geni Wright (6/2015)<br>
                                       Marcelle Wycha (6/2016)<br>
                                       Abdelbassit Yacoub (9/2015)<br>
                                       Claire Yates (3/2016)<br>
                                       Susan Yawn (1/2016)<br>
                                       Laurie Youngman (2/2014)<br>
                                       Wael Yousif (9/2015)<br>
                                       Ashley Zehel (12/2016)<br>
                                       Areeje Zufari (2/2012)<br>
                                       Jacquelyn Zuromski (3/2016)
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/concentrations/online/digitalprofessors.pcf">©</a>
      </div>
   </body>
</html>