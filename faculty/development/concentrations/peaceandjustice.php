<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Concentrations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/concentrations/peaceandjustice.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Concentrations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/concentrations/">Concentrations</a></li>
               <li>Concentrations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Peace and Justice Practitioner</h2>
                        
                        <p>The Peace and Justice Institute at Valencia College promotes peace and justice for
                           all. Our aim is to nurture an inclusive, caring and respectful environment on campus
                           and within our community--one where conflict leads to growth and transformation, rather
                           than violence or aggression. Faculty members who wish to integrate practices promoting
                           these values into their classrooms are encouraged to engage in these faculty development
                           courses.
                        </p>
                        
                        
                        <h3>Foundational Courses </h3>
                        
                        
                        <p>These foundational courses model inclusive instructional practices and introduce core
                           principles and instructional strategies that are necessary to create a truly engaging,
                           inclusive, and respectful learning environment. For an integrated, comprehensive introductory
                           experience, we recommend completing the five courses below within one academic year.
                        </p>
                        
                        
                        <p>Register for each of the following classes.</p>
                        
                        
                        <div>
                           
                           
                           <div><a href="peaceandjustice.html#">INDV 2151   Inclusion and Diversity<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    In this seminar, participants will investigate learning opportunities that acknowledge,
                                    draw upon, and are enriched by student diversity and create atmospheres of inclusion
                                    and understanding. Participants will reflect on power differentials in the classroom,
                                    ways to increase inclusion and minimize exclusion, and some theoretical underpinnings
                                    of Inclusion and Diversity. <br>
                                    NOTE: This course is part of the TLA Core
                                    Seminar series and is open to all faculty. 
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <div><a href="peaceandjustice.html#">INDV 7310  Working with Conflict<span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    
                                    In this course, participants will learn about the components, roles and needs active
                                    in conflict, identify their own conflict style, and be introduced to conflict resolution
                                    practices for the purpose of more productive and positive outcomes. Participants will
                                    engage in various exercises, including a self-assessments and mock negotiation for
                                    the purpose of integrating these skills into a working knowledge.                
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="peaceandjustice.html#">INDV 7311  Creating a Safe Space for Dialogue<span>10 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    This course will demonstrate techniques for establishing a respectful and inclusive
                                    environment that promotes healthy classroom dialogue. Dialogue can assist students
                                    in moving information from a memorized or theoretical understanding, to a more integrated,
                                    tangible and lasting knowledge. Participants will engage in best practices designed
                                    to promote discussion as a pedagogical tool. In registering for this  course, you
                                    are committing to attend 2 meetings across two terms (one 4-hour and one 2-hour),
                                    complete the required reading, and integrate a conversation activity into your class.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <div><a href="peaceandjustice.html#">INDV 7312 Mindfulness Tools for Educators<span>3 PD Hour</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Contemplative pedagogy involves a wide range of teaching methods designed to cultivate
                                    a capacity for deeper awareness, concentration, and insight that create demonstrable
                                    neurobiological changes. Participants will be introduced to the nature, history, and
                                    status of contemplative practices used in mainstream education as a complementary
                                    pedagogic tool that fosters depth in learning. Participants will have the opportunity
                                    to experiment with and create a contemplative-based classroom practice and reflect
                                    upon how contemplation may innovatively meet the needs of today's students.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="peaceandjustice.html#">INDV7316 How We Treat Each Other <span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> How We Treat Each Other gives participants effective tools for engaging in difficult
                                    conversations, empathetic listening, perspective taking, self-reflection and relationship
                                    building. By using the principles and their practices, we increase our capacity to
                                    be peace builders. Participants will learn how to implement these techniques in Valencia's
                                    varied learning environments. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3>Advanced Courses</h3>
                        
                        
                        <p>These courses will deepen your ability to create an inclusive and respectful learning
                           environment.
                        </p>
                        
                        
                        <div>
                           
                           
                           <div><a href="peaceandjustice.html#">INDV2255 Multiple Perspectives<span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 In this workshop, participants will investigate teaching strategies to improve students'
                                 abilities to engage in conversation with alternative viewpoints. Participants will
                                 reflect on ways to reveal the importance of recognizing and engaging multiple perspectives
                                 as well as ways to motivate students to learn from reputable sources. This is an elective
                                 course in the Seneff Certification program.
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="peaceandjustice.html#">INDV3351 Creating an Environment for Inclusive Excellence<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 The diversity of our student perspectives grows each year, and the ways in which we
                                 create successful learning environments subsequently evolves. Through this course,
                                 you will engage with ideas and scenarios to help you create an environment inclusive
                                 of all learners while in consideration of individual differences.
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="peaceandjustice.html#">INDV7315 Danger of A Single Story <span>25 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> The purpose of this course is to invite faculty participants to begin/continue to
                                    examine their professional and personal experiences using the lenses of race and ethnicity.
                                    You will be afforded the opportunity to tell your story while hearing the stories
                                    of others. In order to create a hospitable and accountable community, our journey
                                    will be guided by the 13 Principles of How We Treat Each Other of the Peace and Justice
                                    Institute. Participants will meet face-to-face and interact online with their colleagues
                                    using personal stories, assigned readings, discussion forums, videos and a field experience.
                                    In addition, each participant will design a new or transformed lesson/unit plan to
                                    be shared with peers. As a result of completing this course, participants will be
                                    better equipped with tools to work effectively in a pluralistic society, by improving
                                    the multicultural experience for themselves and their students. This is a hybrid course.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="peaceandjustice.html#">Peace and Justice Spring Retreat (annually)</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Contemplative pedagogy involves a wide range of teaching methods designed to cultivate
                                    a capacity for deeper awareness, concentration, and insight that create demonstrable
                                    neurobiological changes. Participants will be introduced to the nature, history, and
                                    status of contemplative practices used in mainstream education as a complementary
                                    pedagogic tool that fosters depth in learning. Participants will have the opportunity
                                    to experiment with and create a contemplative-based classroom practice and reflect
                                    upon how contemplation may innovatively meet the needs of today's students.
                                    
                                    
                                 </p>
                                 
                                 <p>. This retreat is typically in the Spring Term. Please call the Peace and Justice
                                    Institute for more information. 
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>For More Information</h3>
                        
                        <p>Please contact  <a href="mailto:rallen39@valenciacollege.edu">Rachel Allen</a>, or visit the<a href="../../../PJI/index.html"> Peace and Justice Institute webpage</a>.
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/concentrations/peaceandjustice.pcf">©</a>
      </div>
   </body>
</html>