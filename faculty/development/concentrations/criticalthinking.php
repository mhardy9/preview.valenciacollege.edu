<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Concentrations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/concentrations/criticalthinking.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Concentrations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/concentrations/">Concentrations</a></li>
               <li>Concentrations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Critical Thinking</h2>
                        
                        <p>Critical Thinking is skillful, responsible thinking that is conducive to good judgment
                           because it is sensitive to context, considers multiple perspectives, relies on criteria,
                           and is self-correcting.
                        </p>
                        
                        
                        <h3>Courses</h3>
                        
                        
                        <div>
                           
                           
                           <div>
                              <a href="criticalThinking.html#">ASMT2229 Assessing Higher Order Thinking Through Multiple Choice Questions<br>&nbsp;<span>Variable PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    In this course, faculty members will enhance their ability to develop, test, and revise
                                    multiple choice questions drawing hands on activities that use examples from Valencia
                                    College Faculty. Issues of validity including reliability will be included in the
                                    conversation.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="criticalThinking.html#">INDV2254 The Art and Science of Learning and the Brain<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    In this course, participants will examine the relationship between the ways people
                                    learn and the biology of the brain. NOTE: This is an elective course in the Seneff
                                    Certification program.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="criticalThinking.html#">INDV2255 Multiple Perspectives<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    In this workshop, participants will investigate teaching strategies to improve studentsâ€™
                                    abilities to engage in conversation with alternative viewpoints. Participants will
                                    reflect on ways to reveal the importance of recognizing and engaging multiple perspectives
                                    as well as ways to motivate students to learn from reputable sources. NOTE: This is
                                    an elective course in the Seneff Certification program.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="criticalThinking.html#">LCTS2214: Problem-based Learning<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    This course explores the learning of subject matter and skill acquisition through
                                    collaborative problem-solving. Emphasis is placed on using this method in community
                                    college courses.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="criticalThinking.html#">LCTS6311 Interdisciplinary Teaching: Pedagogical Practices that Encourage Critical
                                 Thinking and Action<span>3 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    This course will explore the benefits and challenges of designing and implementing
                                    interdisciplinary teaching techniques in the classroom. Emphasis will be placed on
                                    developing instructional activities that nurture studentsâ€™ critical thinking skills
                                    by constructing alternative perspectives to course questions. In addition, participants
                                    will discuss strategies to meet challenges of interdisciplinary teaching and to maximize
                                    benefits to students. NOTE: This is an elective course for the Seneff Faculty Development
                                    program.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="criticalThinking.html#">LOBP2230 Core Competencies: Think, Value, Communicate, Act (TVCA)<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    This seminar examines strategies that facilitate student growth in thinking critically;
                                    communicating effectively; articulating and applying personal values and those of
                                    the various disciplines and appreciating the values of others; and applying learning
                                    and understanding effectively and responsibly.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="criticalThinking.html#">LOBP3230 Thinking Things Through: Critical Thinking Theory and Practice<span>10 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Over 2 two-hour face-to-face meetings, participants will discuss and apply the concepts
                                    of Paul and Elder's critical thinking model, with a focus on the elements of critical
                                    thinking that can be applied to any discipline to teach students how to think more
                                    critically. In registering for this course, you are committing to attend a kick-off
                                    meeting, develop a lesson plan that incorporates the critical thinking elements, and
                                    present your lesson to workshop participants in the wrap-up meeting. NOTE: This will
                                    be of interest to those teaching in the General Education program. It is also an elective
                                    course in the LifeMap and Seneff Certifications.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="criticalThinking.html#">LOBP3231 Critical Thinking: Intellectual Standards<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Faculty will deepen their understanding of the Paul and Elder critical thinking model
                                    by examining the personal qualities of a strong critical thinker and learning the
                                    standards by which to analyze a position. Participants will identify strategies to
                                    incorporate these skills into class activities, course assignments, and assessment
                                    methods. NOTE: This course is recommended for those who have completed LOBP 3230.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/concentrations/criticalthinking.pcf">©</a>
      </div>
   </body>
</html>