<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Concentrations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/concentrations/actionresearch.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Concentrations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/concentrations/">Concentrations</a></li>
               <li>Concentrations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Action Research</h2>
                        
                        <p>Action research can be described as a family of research methodologies which pursue
                           action (or change) and research (or understanding) at the same time. In most of its
                           forms it does this by using a cyclic or spiral process which alternates between action
                           and critical reflection and in the later cycles, continuously refining methods, data
                           and interpretation in the light of the understanding developed in the earlier cycles.
                           -- Dick, Bob (1999)
                        </p>
                        
                        
                        <p>Action Research, a scholarly approach to improve teaching and learning, can be a central
                           piece of professional development. This project-based research can benefit professors,
                           counselors, and librarians by actively engaging them in the collaborative study of
                           learning as it takes place day by day in the context of their own practices. Through
                           this small-scale, practical research, faculty members can investigate questions regarding
                           student learning that directly impact their practices. At Valencia, action research
                           has become a significant aspect of faculty development, thereby reinforcing the College's
                           community of learners and culture of evidence.
                        </p>
                        
                        
                        <p>We offer many opportunities to support faculty members who are interested in exploring
                           action research. These resources and courses help faculty members develop a best practices
                           framework for action research and Valencia's Standards of Scholarship.
                        </p>
                        
                        
                        <h3>Courses</h3>
                        
                        
                        <div>
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL2171 Scholarship of Teaching and Learning<span>3 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    This Essential Competencies seminar examines Action Research as a method for educators
                                    to continuously reflect on the effectiveness of their teaching, counseling and librarianship.
                                    NOTE: This course is part of the TLA Core Seminar series and open to all faculty.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL2271 Learning to Use the ARP Builder<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    In this hands-on workshop, participants will learn how to use the Action Research
                                    Project Builder to plan, implement, assess, and publish (within the Valencia community)
                                    their Action Research.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL2272 Developing Effective Surveys<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    In this hands-on session, participants will learn the benefits of the common survey
                                    types, learn tips on how to write effective survey questions, and collaboratively
                                    assess sample surveys. Participants will have the opportunity to apply these principles
                                    to their own work.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL2273 IR and You: How IR Can Help Faculty Research<span>2 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    This interactive course has been designed to familiarize participants with how Institutional
                                    Research (IR) can support faculty research projects. Participants will receive immediate
                                    feedback from IR on types of data analyses or studies that would assist in their data
                                    collection. Participants will learn what resources are available for different types
                                    of research. Participants will understand the relationship between information requests
                                    for research and the Institutional Review Board (IRB) process.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL3271 Principles of Good Practice<span>1 PD Hour</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    The purpose of this workshop is to introduce participants to the ethical considerations
                                    of conducting research in an educational setting.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL3272 IRB Requirements and Your Course Part I<span>1 PD Hour</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Meeting one hour face to face , this course will provide an overview of the Institutional
                                    Review Board (IRB) for all faculty members who are planning to conduct research studies
                                    at Valencia College.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL3273 IRB Requirements and Your Course Part II <span>3 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    This online course will result in nationally recognized certification from the National
                                    Institutes of Health (NIH) on protecting human subjects. The online training is self-paced
                                    and required for faculty members conducting research at Valencia College. It is recognized
                                    by most institutions of higher education for three years after the dates of completion.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL3371 Action Research: Project Design<span>20 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    An independent study course where participants will design an Action Research Project
                                    that articulates a research question, measures a learning outcome, and implements
                                    innovative teaching strategies. NOTE: Part 1 of a two-part sequence. Successful completion
                                    of this independent study course is dependent upon submission of the completed Action
                                    Research Project Plan (pages 1-4), approved by the faculty member's dean or director.
                                    This approval is documented using SOTL3371 Independent Study Approval Form.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL3372 Action Research: Implementation<span>20 PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Prerequisite: SOTL3371 or consent of facilitator. An independent study course where
                                    participants implement their Action Research Project. Participants will record appropriate
                                    data, conduct analysis of that data, and reflect on its meaning for the improvement
                                    of student learning. Results and reflective critique are recorded in the Action Research
                                    Builder. NOTE: Part 2 of a two-part sequence. Successful completion of this independent
                                    study course is dependent upon submission of the fully completed Action Research Project
                                    Plan, approved by the faculty member's dean or director, and uploaded to Valencia's
                                    online repository, the Action Research Builder. This approval is documented using
                                    the SOTL3372 Independent Study Approval Form. This course is an elective course in
                                    the Seneff Certification program.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              <a href="actionResearch.html#">SOTL4270 Community of Scholars<span>Variable PD Hours</span></a>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    A selected group of faculty will collaboratively engage in scholarly inquiry. Each
                                    Community of Scholars will be specialized "community of practice" that will explore
                                    a shared topic related to the innovation of teaching and learning at Valencia. Led
                                    by a faculty facilitator, each community will engage in collaborative activities and
                                    disseminate their work to campus and college colleagues. NOTE: Participants may be
                                    selected through an application process. For more information please contact the Office
                                    of Faculty and Instructional Development. This course is an elective course in the
                                    Seneff Certification program.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/concentrations/actionresearch.pcf">©</a>
      </div>
   </body>
</html>