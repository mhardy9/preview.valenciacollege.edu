<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Concentrations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/concentrations/experiential/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Concentrations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/concentrations/">Concentrations</a></li>
               <li>Experiential</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Experiential Learning</h2>
                        
                        <p>
                           Experiential Learning is characterized by highly immersive learning through experiences
                           coupled with intentional reflection. This concentration of courses help to take active
                           learning to a deep level through immersion and infusion strategies.
                           
                        </p>
                        
                        
                        <h3>Service Learning</h3>
                        
                        
                        <div>
                           
                           <div><a href="index.html#">LCTS7220: Introduction to Service Learning <span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Service Learning is a high-impact educational practice that creates opportunities
                                    for students to achieve the learning outcome(s) through partnership with the community.
                                    In this course, you will be certified to teach the Service Learning (SLS2940 and SLS2940H)
                                    independent study courses and gain access to the materials needed to successfully
                                    integrate an existing Service Learning project into your courses. NOTE: Successful
                                    completion of this course is required for faculty to teach SLS2940 and SLS2940H.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#">LCTS3213 Service Learning Across the Curriculum <span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Incorporate service learning in any course to enhance student learning and make a
                                    difference in the community.
                                    
                                 </p>
                                 
                                 <p>
                                    Participants in this hybrid course will create a plan to infuse service learning into
                                    a current course by linking course outcomes to meaningful service in the community.
                                    Participants will explore service learning opportunities and create assessment plans
                                    to measure student learning. 
                                    
                                 </p>
                                 
                                 
                                 <h3>How It Works</h3>
                                 
                                 <p>
                                    This is a hybrid course which will involve two face-to-face workshops in addition
                                    to online activities.
                                    
                                 </p>
                                 
                                 <h3>Workshop 1</h3>
                                 
                                 <p>
                                    A three hour session will introduce the fundamentals of service learning and the framework
                                    for incorporating service learning into the classroom. Participants will develop the
                                    initial plan for their service learning project.
                                    
                                 </p>
                                 
                                 <h3>Workshop 2</h3>
                                 
                                 <p>
                                    A two hour session will focus on reflection and assessment of the service learning
                                    project. There will be time to share projects for peer feedback. Participants will
                                    develop the initial plan for reflection and assessment.
                                    
                                 </p>
                                 
                                 <h3>Online Activities</h3>
                                 
                                 <p>
                                    Participants will complete the plan for their service learning project, reflection
                                    and assessment and post it in the online Blackboard course for peer feedback and discussion.
                                    
                                    
                                 </p>
                                 
                                 
                                 <h3>What is Service Learning Recognition?</h3>
                                 
                                 <p>
                                    Faculty who integrate Service Learning into their courses can have the course designated
                                    as a "Service Learning Integrated Course." Students who take this course can then
                                    earn credit toward the Service Learning Recognition. 
                                 </p>
                                 
                                 
                                 <p>
                                    For questions about the program or to learn more, please contact Shara Lee at <a href="mailto:slee84@valenciacollege.edu">slee84@valenciacollege.edu</a> or call 321-682-4723.
                                    
                                 </p>
                                 
                                 
                                 <h3>Related Links</h3>
                                 
                                 <ul>
                                    
                                    <li><a href="../../../../service-learning/index.html">Service Learning at Valencia</a></li>
                                    
                                    <li><a href="http://www.aacc.nche.edu/servicelearning">American Association of Community Colleges</a></li>
                                    
                                    <li><a href="http://www.servicelearning.org/">National Service Learning Clearinghouse</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3>Sustainability </h3>
                        
                        <div>
                           
                           <div><a href="index.html#">LCTS3219 Sustainability Across Curriculum <span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    In this course, participants will examine sustainability concepts, explore available
                                    resources, and develop a plan to infuse sustainability and ethical responsibility
                                    into a current course. Participants will learn a variety of strategies and techniques
                                    they can use to make sustainability concepts relevant to students' lives and academic
                                    work. Through collaborative learning, including face-to-face and online group work,
                                    participants will prepare assessment plans to measure student learning for the sustainability
                                    focused module or unit.
                                    
                                 </p>
                                 
                                 <p>
                                    This course received <a href="http://wp.valenciacollege.edu/thegrove/sustainability-across-the-curriculum-course-receives-national-recognition/">national recognition</a> by the American Association for the Advancement of Sustainability in Higher Education
                                    (AASHE).
                                    
                                 </p>
                                 
                                 
                                 <h3>How It Works</h3>
                                 
                                 <p>
                                    This is a hybrid course with a face-to-face orientation and a wrap-up session. Participants
                                    will develop a formal plan to infuse sustainability concepts into at least one unit
                                    of a course they currently teach.
                                    
                                 </p>
                                 
                                 
                                 <h3>What is Sustainability?</h3>
                                 
                                 <p>
                                    Sustainability is a search for ecological stability and human progress that can last
                                    over the long term. 
                                    
                                 </p>
                                 
                                 <p>
                                    For questions about the program or to learn more, please contact Shara Lee at <a href="mailto:slee84@valenciacollege.edu">slee84@valenciacollege.edu</a> or call 321-682-4723.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        <h3>Global Competence</h3>
                        
                        <div>
                           
                           <div><a href="index.html#">INDV3351 Internationalizing the Curriculum at Home <span>20 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This hybrid course is designed to provide faculty members with the knowledge and resources
                                    to internationalize their courses through the development of a course internationalization
                                    toolkit, which will be shared with colleagues college-wide through the hosting of
                                    materials on the new INZ SharePoint: <a href="http://site.valenciacollege.edu/inz/SitePages/Home.aspx" target="_blank">http://site.valenciacollege.edu/inz/SitePages/Home.aspx</a></p>
                                 
                                 
                                 <p>Using an add-on, infusion, or transformational approach to internationalization, participants
                                    will work in small groups to develop the student learning outcomes, course content,
                                    materials, activities and assignments, and assessments for a specific course. Faculty
                                    are strongly encouraged to attend the course with at least one other person from their
                                    discipline in order to develop the toolkit together. 
                                 </p>
                                 
                                 <p>NOTE: This course is an elective course in the Seneff Faculty Development program.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="index.html#">INDV3353 Cross-Cultural Awareness and Infusion in the College Classroom <span>6 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Since cultural variations can have a profound impact on teaching and learning, this
                                    course focuses on cross-cultural awareness and some of the challenges with engaging
                                    students from diverse backgrounds. Participants will engage in a cross-cultural simulation
                                    role play in order to explore differences in belief and value systems. Strategies
                                    for building a more inclusive learning community both inside and outside of the classroom
                                    will be provided. 
                                 </p>
                                 
                                 <p>NOTE: This course is an elective course in the Seneff Faculty Development program.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/concentrations/experiential/index.pcf">©</a>
      </div>
   </body>
</html>