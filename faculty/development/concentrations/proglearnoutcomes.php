<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Concentrations | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/concentrations/proglearnoutcomes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Concentrations</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/concentrations/">Concentrations</a></li>
               <li>Concentrations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Program Learning Outcomes</h2>
                        
                        <p>The goal of outcomes-based practice is student learning. Valencia has established
                           "what students should know or do" upon their graduation through the Student Core Competencies
                           and Program Learning Outcomes.  The following course help participants design, implement
                           and assess program learning outcomes.
                        </p>
                        
                        
                        <h3> Courses </h3>
                        
                        
                        <div>
                           
                           <div><a href="ProgLearnOutcomes.html#">LOBP2131 Learning Outcomes-based Practice <span>2 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> This seminar focuses on student learning as the central goal of a Valencia educator’s
                                    practice. Participants will be introduced to the construction, implementation and
                                    alignment of learning outcomes with a focus on the student core competencies (Think,
                                    Value, Communicate, Act). Participants will examine teaching and learning through
                                    the lens of the key questions: “What will the students be able to know or do?” and
                                    “How will I know they can do it?” Note: This course is part of the TLA Core Seminar
                                    series. 
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div>
                              <a href="ProgLearnOutcomes.html#">LOBP3331 Prgm Outcomes Assess.: Dev. Meaningful Assessment Plans <span> 3 PD Hours</span></a> 
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Focused on a series 
                                    of interactive activities, this course will help participants develop essential skills
                                    for the program learning outcomes development process.  Participants will work with
                                    examples and exercises to develop or contribute to the development of program learning
                                    outcomes plans, the collection and analysis of data across courses, and the subsequent
                                    creation of program, improvement 
                                    plans. 
                                 </p>
                                 
                                 <p><em>Note</em>: Sessions will be offered to discipline and/or division groups upon request. Please
                                    contact <a href="mailto:lblasi@valenciacollege.edu">Laura Blasi</a> or <a href="mailto:wdew@valenciacollege.edu">Wendi Dew</a>.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div><a href="ProgLearnOutcomes.html#">LOBP3332 Prgm Outcomes Assess.: Models and Strategies that Work<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>As a showcase of current and emerging best practices at Valencia, this course will
                                    offer examples of models and strategies for learning outcomes assessment drawn from
                                    diverse academic disciplines. The session will feature speakers from programs across
                                    the college who will share the process they experienced over time along with breakthroughs
                                    and lessons learned. 
                                 </p>
                                 <em>Note</em>: Sessions will be offered to discipline and/or division groups upon request. Please
                                 contact <a href="mailto:lblasi@valenciacollege.edu">Laura Blasi</a> or <a href="mailto:wdew@valenciacollege.edu">Wendi Dew</a>.
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ProgLearnOutcomes.html#"><span>3 PD Hours</span>ASMT2229 Assess Higher Order Thinking: Multiple Choice Question Dev.</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> In this course, faculty members will enhance their ability to develop, test, and
                                    revise multiple choice questions drawing on hands-on activities using examples from
                                    Valencia College faculty. Issues of validity including reliability will be included
                                    in the conversation. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="ProgLearnOutcomes.html#">ASMT3220: Implementing Rubrics for Program Assessment<span>3 PD Hours</span></a></div>
                           
                           <div>
                              
                              <div>
                                 This course has been designed as an interactive working session for faculty implementing
                                 rubrics for the review of student work across course sections / courses. Faculty members
                                 participating from the same programs will be invited to use their own student work.
                                 The session will cover sampling, inter-rater reliability, norming of review teams,
                                 and the consistent application of criteria to student artifacts and assignments for
                                 the assessment of program learning outcomes.
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        <h3>For More Information</h3>
                        
                        <p>Please contact  <a href="mailto:lblasi@valenciacollege.edu">Laura Blasi</a> or visit the <a href="../../../academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/index.html">Program Learning Outcomes Assessment</a> webpage.
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/concentrations/proglearnoutcomes.pcf">©</a>
      </div>
   </body>
</html>