<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/about/team.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/about/">About</a></li>
               <li>Faculty Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Our Team</h2>
                        
                        <h3><strong>District Office</strong></h3>
                        
                        <ul>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=wdew.html" target="_blank">Wendi Dew</a>, Assistant Vice President of Teaching &amp; Learning and Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=rbenjamin3.html">Rashida Benjamin</a>, Administrative Assistant, Teaching &amp; Learning
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=tchiao.html">Trish Chiao</a>, Coordinator, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=grolle3.html">Glendisha Rolle</a>, Staff Assistant II, Faculty &amp; Instructional Development
                           </li>
                           
                        </ul>
                        
                        <h3>Online Teaching &amp; Learning</h3>
                        
                        <ul>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=pjerzak.html">Page Jerzak</a>, Director, Online Teaching &amp; Learning
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=gwright31.html">Geni Wright</a>, Assistant Director, Online Teaching &amp; Learning
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=rsnyder13.html">Rick Snyder</a>, Faculty Developer/Instructional Designer, Online Teaching &amp; Learning
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=rtran5.html">Rose Tran</a>, Faculty Developer/Instructional Designer, Online Teaching &amp; Learning
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=dholton5.html">Doug Holton</a>, Faculty Developer/Instructional Designer, Online Teaching &amp; Learning
                           </li>
                           
                           <li>Vacant, Faculty Developer/Instructional Designer, Online Teaching &amp; Learning</li>
                           
                           <li>Vacant, Faculty Developer/Instructional Designer, Online Teaching &amp; Learning</li>
                           
                           <li>Vacant, Faculty Developer/Instructional Designer, Online Teaching &amp; Learning</li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=tfagan4.html">Tommy Fagan</a>, Multimedia Designer, Online Teaching &amp; Learning
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=jguthrie10.html">Jesse Guthrie</a>, Multimedia Designer, Online Teaching &amp; Learning
                           </li>
                           
                        </ul>
                        
                        <h3>East and Winter Park Campuses</h3>
                        
                        <ul>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=sspong.html">Stephanie Spong</a>, Campus Director, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=moteroolan.html">Migdalia Otero-Olan</a>, Faculty Developer / Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=abergeson1.html">Aaron Bergeson</a>, Faculty Developer / Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=cwallace28.html">Celicia M. Wallace</a>, Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=amolina12.html">Amanda Molina</a>, Faculty Developer/ Instructional Designer
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=acabrera43.html">Ashley Cabrera</a>, Faculty &amp; Instructional Support Specialist
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=jcalderon38.html">Jennifer Calderon</a>, Staff Assistant II
                           </li>
                           
                        </ul>
                        
                        <h3>Osceola, Lake Nona, and Poinciana Campuses</h3>
                        
                        <ul>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=slee84.html">Shara Lee</a>, Campus Director, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=gkokaisel.html">Gary Kokaisel</a>, Faculty Developer / Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=kcolwell1.html">Kevin Colwell</a>, Faculty Developer / Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=lkelley8.html">Lauren Kelley</a>, Faculty Developer / Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=dshimizu.html">Dominique Shimizu</a>, Faculty Developer/Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li>Wendy Dye, Faculty &amp; Instructional Support Specialist, Faculty &amp; Instructional Development</li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=drodriguez170.html">Denisha Rodriguez-Pflucker</a>, Administrative Assistant
                           </li>
                           
                        </ul>
                        
                        <h3>West Campus</h3>
                        
                        <ul>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=dhaggerty2.html">Dori Haggerty</a>, Campus Director, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=glooney1.html">Hunter Looney</a>, Faculty Developer / Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=cyates6.html">Claire Yates</a>, Faculty Developer/Instructional Designer, Faculty &amp; Instructional Development
                           </li>
                           
                           <li><a href="/contact/Search_Detail2.cfm-ID=geugene4.html">Greg Eugene</a>, Faculty &amp; Instructional Support Specialist, Faculty &amp; Instructional Development
                           </li>
                           
                           <li>Eliza Knight, Staff Assistant II, Faculty &amp; Instructional Development</li>
                           
                        </ul>
                        
                        <p><a href="/faculty/development/tla/tla_team.html">Teaching/Learning Academy Team</a></p>
                        
                        <p><a href="/faculty/development/about/team.html#top">TOP</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div><a title="Faculty Development Catalog" href="/faculty/development/documents/faculty-development-catalog-2017-18.pdf" target="_blank"><img src="/faculty/development/about/faculty-development-catalog.jpg" alt="Faculty Development Catalog" width="245" height="210" border="0"></a></div>
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img src="/faculty/development/about/icon_facebook_24.png" alt="TeachValencia Facebook"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://twitter.com/teachvalencia" target="_blank"><img src="/faculty/development/about/icon_twitter_24.png" alt="TeachValencia Twitter"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img src="/faculty/development/about/icon_youtube_24.png" alt="TeachValencia YouTube"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img src="/faculty/development/about/icon_circles_24.png" alt="Circles Of Innovation"></a></div>
                        
                        <p><a title="How to Register" href="/faculty/development/howToRegisterForCourses.html"><img src="/faculty/development/about/button-how-to-register.png" alt="How to Registers" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Certification Planning Documents" href="/faculty/development/CertificationPlanningDocuments.html"><img src="/faculty/development/about/button-certification-docs.png" alt="Certification Planning Documents" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Have Questions? Contact a CTLI Member" href="/faculty/development/centers/locations.html"><img src="/faculty/development/about/button-contact-ctli.png" alt="Have Questions? Contact a CTLI Member" width="245" height="60" border="0"></a></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/about/team.pcf">©</a>
      </div>
   </body>
</html>