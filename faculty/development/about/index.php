<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/about/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>              </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li>About</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               
               
               <div class="container" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>About Us</h2>
                        
                        
                        <h3><a href="team.php">Our Team</a></h3>
                        
                        
                        <h3>Vision</h3>
                        
                        <p>We are an evolving community of practice for learning leaders.</p>
                        
                        
                        <h3>Mission</h3>
                        
                        <p>We build the capacity of the college to improve student learning.</p>
                        
                        
                        <h3>Values</h3>
                        
                        <ul>
                           
                           <li>We are dedicated to supporting growth in the Essential Competencies of a Valencia
                              Educator for all faculty members at all stages of their career.
                           </li>
                           
                           <li>We base our practice on a reliable evidence base that is informed by theory.</li>
                           
                           <li>We collaborate in teams and across organizational divisions and campuses, appreciating
                              the strengths of individuals.
                           </li>
                           
                           <li>We are dedicated to the standards of scholarship.</li>
                           
                           <li>We value integrity in word and deed as we model good practices of communication and
                              teaching.&nbsp; 
                           </li>
                           
                           <li>We value authenticity as evidenced by honest and direct communication.</li>
                           
                           <li>We are optimistic; we believe that all people can learn under the right conditions.</li>
                           
                           <li>We are accountable to our community through assessment reports and revisions of our
                              programs.
                           </li>
                           
                           <li>We seek to always be learners, to keep ourselves open to new ideas, skills, and solutions.</li>
                           
                           <!--  test -->
                           
                        </ul>
                        
                        
                        <h3>Faculty Development Primary Roles in Teaching and Learning</h3>
                        
                        <ul>
                           
                           <li>
                              <em>Faculty Development </em>- Supports all faculty members as they expand their professional practices and examine
                              their ongoing development in the seven <a href="documents/EssentialCompetenciesNew04-2016.pdf" target="_blank">Essential Competencies of a Valencia Educator</a>.&nbsp;
                           </li>
                           
                           <li>
                              <em>Instructional Development </em>- Supports all faculty members as they develop course and curriculum experiences,
                              in all modalities, to improve student learning.
                           </li>
                           
                           <li>
                              <em>Continuous Improvement Processes</em> - Engages teachers, scholars and practitioners in continuous improvement processes
                              that result in student learning.
                           </li>
                           
                           <li>
                              <em>Innovation</em> - Serves as a catalyst for advancing best practices and innovation in teaching, learning
                              and technology, supported by evidence.
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Faculty Development Program Learning Outcomes</h3>
                        
                        <ul>
                           
                           <li>Faculty members will critically evaluate their ongoing development needs in the Essential
                              Competencies of a Valencia Educator.
                           </li>
                           
                           <li>Faculty members will complete an individualized development plan to improve their
                              practices, in terms of student learning, and according to professional need. 
                           </li>
                           
                           <li>Faculty members will apply the Essential Competencies of a Valencia Educator to improve
                              their practice.
                           </li>
                           
                           <li>Faculty members will engage in continuous improvement processes with the goal of enhanced
                              student learning.
                           </li>
                           
                           <li>Faculty members will develop professional relationships throughout the division, campus,
                              and college.&nbsp; 
                           </li>
                           
                           <li>Faculty members will contribute to an evolving community of peers focused on reflection,
                              innovation, and enhanced student learning.
                           </li>
                           
                        </ul>
                        
                        
                        <p><a href="documents/DevelopingFacultyforStudentSuccess-OurWorkingTheories.pdf">Developing Faculty for Student Success: Our Working Theories</a></p>
                        
                        <h3>Related Documents</h3>
                        
                        <ul>
                           
                           <li><a href="documents/2014-15AnnualReport.pdf">2014-2015 Annual Report</a></li>
                           
                           <li>
                              <a href="images/facdevannualrep2013-14.png" target="_blank">2013-2014 Annual Report</a> 
                           </li>
                           
                           <li><a href="documents/DevelopingFacultyforStudentSuccess-OurWorkingTheories.pdf">Developing Faculty for Student Success: Out Working Theories</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank"><img alt="Download the Faculty Development Catalog (PDF)" height="210" src="/faculty/development/images/faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div class="add_bottom_30">
                           
                           <h3>Get Social</h3>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank" aria-label="TeachValencia Facebook"><i class="fab fa-facebook fa-2x fa-fw social-padding" style="vertical-align: middle;"></i></a>
                           <a href="https://twitter.com/teachvalencia" target="_blank" aria-label="TeachValencia Twitter"><i class="fab fa-twitter-square fa-2x fa-fw social-padding" style="vertical-align: middle;"></i></a>
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank" aria-label="TeachValencia Youtube"><i class="fab fa-youtube fa-2x fa-fw social-padding" style="vertical-align: middle;"></i></a>
                           						<a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="/faculty/development/images/icon_circles_24.png"></a>
                           
                        </div>
                        
                        					
                        <!-- <p><a href="/faculty/development/howToRegisterForCourses.php"><img alt="How to Registers"  height="60" src="/faculty/development/images/button-how-to-register.png" width="245" /></a></p>
                    <p><a href="/faculty/development/CertificationPlanningDocuments.php"><img alt="Certification Planning Documents"  height="60" src="/faculty/development/images/button-certification-docs.png" width="245" /></a></p>
                    <p><a href="/faculty/development/centers/locations.php"><img alt="Have Questions? Contact a CTLI Member"  height="60" src="/faculty/development/images/button-contact-ctli.png" width="245" /></a></p> -->
                        					
                        					
                        <div>
                           						<a href="/faculty/development/howToRegisterForCourses.php" class="button-outline">How to Register for Courses</a><br>
                           						<a href="/faculty/development/CertificationPlanningDocuments.php" class="button-outline">Certification Planning Documents</a><br>
                           						<a href="/faculty/development/centers/locations.php" class="button-outline">Have questions? <br>Contact a CTLI Member</a><br>
                           					
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/about/index.pcf">©</a>
      </div>
   </body>
</html>