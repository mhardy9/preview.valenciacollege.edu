<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/about/awards.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/about/">About</a></li>
               <li>Faculty Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h3>&nbsp;</h3>
                        
                        <h3><strong>Awards</strong></h3>
                        
                        <ul>
                           
                           <li>Circles of Innovation - <a href="http://circlesofinnovation.valenciacollege.edu/2014/01/25/circles-of-innovation-bellwether-finalist/" target="_blank">2014 Bellwether Finalist for Instructional Programs and Services</a></li>
                           
                           <li>Circles of Innovation Team - <a href="http://wp.valenciacollege.edu/thegrove/april-2014-kudos/" target="_blank">Winner of Valencia's Inaugural Innovation of the Year Award, 2014</a></li>
                           
                           <li>Sustainability Across the Curriculum course - <a href="http://wp.valenciacollege.edu/thegrove/sustainability-across-the-curriculum-course-receives-national-recognition/" target="_blank">recognized by The American Association for the Advancement of Sustainability in Higher
                                 Education (AASHE</a>)
                           </li>
                           
                           <li>Study Abroad Program Leader Certification - <a href="http://wp.valenciacollege.edu/thegrove/prestigious-andrew-heiskell-award-cites-valencias-innovation-in-international-education-2/" target="_blank">recognized by The Institute of International Education (IIE)</a></li>
                           
                        </ul>
                        
                        <h3><strong>Publications</strong></h3>
                        
                        <ul>
                           
                           <li><a href="https://www.aspeninstitute.org/publications/creating-faculty-culture-student-success/" target="_blank">Aspen Guide on Valencia's TLA and Faculty Programs</a></li>
                           
                           <li><a href="http://www.ccsse.org/docs/PTF_Special_Report.pdf" target="_blank">CCSSE Recognition for Part-Time Faculty Development Programs</a></li>
                           
                        </ul>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>&nbsp;</h3>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div><a title="Faculty Development Catalog" href="/faculty/development/documents/faculty-development-catalog-2017-18.pdf" target="_blank"><img src="/faculty/development/about/faculty-development-catalog.jpg" alt="Faculty Development Catalog" width="245" height="210" border="0"></a></div>
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img src="/faculty/development/about/icon_facebook_24.png" alt="TeachValencia Facebook"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://twitter.com/teachvalencia" target="_blank"><img src="/faculty/development/about/icon_twitter_24.png" alt="TeachValencia Twitter"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img src="/faculty/development/about/icon_youtube_24.png" alt="TeachValencia YouTube"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img src="/faculty/development/about/icon_circles_24.png" alt="Circles Of Innovation"></a></div>
                        
                        <p><a title="How to Register" href="/faculty/development/howToRegisterForCourses.html"><img src="/faculty/development/about/button-how-to-register.png" alt="How to Registers" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Certification Planning Documents" href="/faculty/development/CertificationPlanningDocuments.html"><img src="/faculty/development/about/button-certification-docs.png" alt="Certification Planning Documents" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Have Questions? Contact a CTLI Member" href="/faculty/development/centers/locations.html"><img src="/faculty/development/about/button-contact-ctli.png" alt="Have Questions? Contact a CTLI Member" width="245" height="60" border="0"></a></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/about/awards.pcf">©</a>
      </div>
   </body>
</html>