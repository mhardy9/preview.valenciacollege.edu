<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Faculty Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li>Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <ul>
                                 
                                 <li data-id="#tab-1">About Us</li>
                                 
                                 <li data-id="#tab-2">News</li>
                                 
                                 <li data-id="#tab-3">Featured Courses</li>
                                 
                              </ul>
                              
                              
                              
                              
                              
                              <div>
                                 
                                 
                                 <h3><a href="http://thegrove.valenciacollege.edu/registering-for-faculty-development-courses-visit-the-valencia-edge/">Register for Faculty Development Courses in the Valencia EDGE</a></h3>
                                 
                                 <p><strong>Faculty Development:&nbsp; A Comprehensive Program for Valencia's Learning Leaders </strong></p>
                                 
                                 <p>Are you interested in enhancing your practice, developing new and innovative strategies,
                                    and improving your students' learning?&nbsp; How about learning from your colleagues and
                                    sharing your expertise in a collaborative community within your campus, discipline
                                    or division?&nbsp; 
                                 </p>
                                 
                                 <p> Valencia's comprehensive, competency-based Faculty Development Program provides a
                                    variety of opportunities, from face-to-face seminars to online and hybrid courses.
                                    Courses are uniquely designed and facilitated by Valencia experts . . . your colleagues.&nbsp;
                                    
                                 </p>
                                 
                                 <p>Faculty Development seeks to support all faculty members as they expand their professional
                                    practices and examine their ongoing development in the seven <a href="../documents/EssentialCompetenciesNew04-2016.pdf" target="_blank">Essential Competencies of a Valencia Educator</a>.&nbsp; The intention of Valencia's Faculty Development Program is to engage teachers,
                                    scholars and practitioners in continuous improvement processes that result in student
                                    learning. 
                                 </p>
                                 
                                 <p><strong>Create your plan for development today! </strong>Whether you are looking for a single course or interested in working towards a certificate
                                    program, you can begin  by taking one or more of the following steps:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li> Review our current <a href="documents/faculty-development-catalog-2017-18.pdf" target="_blank">Catalog</a> of Courses
                                    </li>
                                    
                                    <li>Review all of our Faculty Development <a href="programs/index.html" target="_blank">Programs</a>
                                       
                                    </li>
                                    
                                    
                                    <li>Contact a member of our <a href="about/team.html" target="_blank">Faculty Development Team</a>
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="howToRegisterForCourses.html" target="_blank">Register Now</a>! 
                                    </li>
                                    
                                 </ul> 
                                 <br>           
                                 
                              </div>    
                              
                              
                              <div>
                                 
                                 <div> 
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <ul>
                                       
                                       
                                       <li>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeFacultyDevelopment/~3/8R_gW6B5yUA/" target="_blank">September 2017 West Campus Center for Teaching/Learning Innovation Best Practices</a> <br>
                                          
                                          
                                          
                                          
                                          
                                          <span>Sep 21, 2017</span><br>
                                          
                                          
                                       </li>
                                       
                                       
                                       <li>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeFacultyDevelopment/~3/Krr6hJZR8wY/" target="_blank">Faculty Development Opportunities</a> <br>
                                          
                                          
                                          
                                          
                                          
                                          <span>Sep 21, 2017</span><br>
                                          
                                          
                                       </li>
                                       
                                       
                                       <li>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeFacultyDevelopment/~3/oWZ45V_Mcl0/" target="_blank">CTLI Grand Opening Rescheduled for October 3 and 4</a> <br>
                                          
                                          
                                          
                                          
                                          
                                          <span>Sep 19, 2017</span><br>
                                          
                                          
                                       </li>
                                       
                                       
                                       <li>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <a href="http://feedproxy.google.com/~r/TheGroveValenciaCollegeFacultyDevelopment/~3/qECymWQVq6Q/" target="_blank">Bulletin Board</a> <br>
                                          
                                          
                                          
                                          
                                          
                                          <span>Sep 19, 2017</span><br>
                                          
                                          
                                       </li>
                                       
                                       
                                    </ul>
                                    
                                    
                                 </div>  
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 
                                 
                                 There are no events at this time.
                                 
                                 
                                 
                                 <p>
                                    <a href="http://events.valenciacollege.edu/group/facultydevelopment" target="_blank">View More Featured Courses </a>                    
                                 </p>
                                 
                              </div>  
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/index.pcf">©</a>
      </div>
   </body>
</html>