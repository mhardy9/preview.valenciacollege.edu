<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources for Essential Competencies | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/coursesresources/lifemap.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Resources for Essential Competencies</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/coursesresources/">Coursesresources</a></li>
               <li>Resources for Essential Competencies</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>LifeMap </h2>
                        
                        
                        <p><strong><em>Valencia educators will design learning opportunities that promote student life skills
                                 development while enhancing discipline learning.&nbsp; Through intentional inclusion of
                                 growth-promoting strategies, instructors, counselors and librarians will facilitate
                                 the students’ reflection, knowledge, and appreciation for self and others; gradual
                                 assumption of responsibility for making informed decisions; and formulation and execution
                                 of their educational, career, and life plans.&nbsp; As a result, students can transfer
                                 those life skills to continued learning and planning in their academic, personal,
                                 and professional endeavors.</em></strong></p>
                        
                        <h3>Resources and Courses</h3>
                        
                        <ul>
                           
                           <li><span><strong>Resources </strong>for LifeMap include professional literature, practical examples, and applications
                                 in diverse disciplines.&nbsp;</span></li>
                           
                           <li><em> Faculty development<strong> Courses</strong> are designed to expand your expertise in LifeMap.&nbsp; All courses related to LifeMap
                                 begin with the prefix, LFMP.&nbsp; </em></li>
                           
                           <li><em>To search for the following LFMP course offerings click <u><a href="../courseSearch.html" target="_blank">here</a></u>. Please note:&nbsp; Not all courses are offered every year.</em></li>
                           
                        </ul>
                        
                        
                        
                        
                        <div>
                           
                           <div><a href="lifeMap.html#">Overview of LifeMap</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="http://www.kaltura.com/tiny/xutn0" target="_blank" title="Overview of LifeMap - Narrated Presentation">Overview of LifeMap - Narrated Presentation</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>LFMP 2141 LifeMap<br>
                                    This seminar explores the philosophy and practice of LifeMap. The focus is on the
                                    value of incorporating LifeMap strategies in one’s practice to promote discipline
                                    learning while also promoting student life skills development. A key question is “How
                                    can my practice enhance students’ continued learning and planning in their academic,
                                    personal, and professional endeavors beyond my course?” Note: This course is part
                                    of the TLA Core Seminar series and open to all faculty.
                                 </p>
                                 
                                 <p> LFMP 3340 Conceptual Frameworks of LifeMap<br>
                                    LifeMap is Valencia’s name for a developmental advising system designed to increase
                                    students’ social and academic integration, development of education and career plans,
                                    and the acquisition of study and life skills. LifeMap describes the ideal progression
                                    of a student in a five-stage model. This course provides the LifeMap conceptual framework
                                    including an in-depth review of the developmental theories on which LifeMap was created.
                                    Note: This course is required for the LifeMap Certification Program and must be taken
                                    first in the series of required courses.
                                 </p>
                                 
                                 <p>                          LCTS 1110 Teaching in Our Learning College<br>
                                    This scenarios-based course introduces the Essential Competencies of a Valencia Educator
                                    and provides faculty with tools and resources to become more effective, learning-centered
                                    instructors. Note: This course is required for the Associate Faculty Certification
                                    Program.                       
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="lifeMap.html#">Other Topics</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/LifeMap-LifeMap.pdf">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>                        LFMP 3115 Designing an Effective Online Student Orientation<br>
                                    Getting students off on the right foot in an online class can mean the difference
                                    between success and failure. This workshop will engage participants in addressing
                                    the orientation needs of the virtual student. Participants will explore the essential
                                    elements and develop a plan for an effective online orientation. In addition, participants
                                    will review the resources in place at Valencia to help faculty provide a comprehensive
                                    student orientation. Note: This course is optional for Digital Professor Certification.
                                 </p>
                                 
                                 <p>                          LFMP 3341 Advising is Teaching<br>
                                    This course will lay the foundation for participants to extend the professor-student
                                    relationship beyond the classroom into faculty advising. Using LifeMap tools and Atlas
                                    resources, the participant will be able to guide students in using their learning
                                    skills in self-discovery and career exploration to build realistic academic and life
                                    plans. Participants will also be able to assist students in developing effective academic
                                    behaviors. Note: This course is required for the LifeMap Certification Program. Participants
                                    must successfully complete the required prerequisite, Conceptual Frameworks of LifeMap
                                    (LFMP3340), prior to enrolling in this course.
                                 </p>
                                 
                                 <p>                          LFMP 3344 Infusing College Success Skills<br>
                                    Faculty from all disciplines who are committed to enhancing student learning will
                                    benefit from this interactive, project-based course. Participants will develop the
                                    knowledge and strategies to effectively infuse college success skills into student-
                                    learning experiences and will view examples from Valencia experts who successfully
                                    infuse these strategies. Note: This course is required for the LifeMap Certification
                                    Program.
                                 </p>
                                 
                                 <p>                          LFMP 3345 Learning Support Services on Your Campus<br>
                                    This course is designed to help participants learn more about campus Learning Support
                                    Services available to students. This course will benefit faculty members from all
                                    disciplines. Note: This course is an elective course for the LifeMap Certification
                                    Program.
                                 </p>
                                 
                                 <p>                          LFMP 3346 LifeMap Certificate Capstone: LifeMap Infusion
                                    Project<br>
                                    This LifeMap Certificate Capstone course is designed as the last course within the
                                    LifeMap Certification Program and involves the completion of a LifeMap Infusion Project.
                                    Participants will be asked to integrate what they learned about the LifeMap model,
                                    developmental advising, LifeMap Tools, and LifeMap College Success Skills into a learning
                                    opportunity for one of their courses. Participants will study LifeMap infusion examples
                                    and develop learning outcomes and assessments for their own discipline. The faculty
                                    will implement the project and assess results during the current term (participants
                                    must be teaching in the term registered for this course). Each faculty member will
                                    share the project and the results in implementation and student learning. Note: This
                                    course is required for the LifeMap Certification Program. Participants must successfully
                                    complete the following required prerequisites prior to enrolling in this course: Conceptual
                                    Frameworks of LifeMap (LFMP3340), Advising is Teaching (LFMP3341), Infusing College
                                    Success Skills (LFMP3344) and a minimum of 4 hours of LifeMap electives coursework.
                                 </p>
                                 
                                 <p>                          LFMP 3347 Engaging Students Thru Mentorship<br>
                                    Mentorship is a vital and successful method to engage students. This course will help
                                    define mentorship in higher education, provide mentoring techniques, and include a
                                    discussion of incorporating mentorship in different fields. Note: This course is optional
                                    for the LifeMap Certification Program.
                                 </p>
                                 
                                 <p>                          LFMP 3348 Continuous Assessment &amp; Responsive Engagement
                                    (CARE) Strategies<br>
                                    Faculty will research and develop intervention strategies for struggling students
                                    that are grounded in continuous assessment and responsive engagement for the purpose
                                    of ensuring student learning. Participants in this course will employ best practices
                                    in the area of early alert to design and implement a CARE strategy in their course.
                                    <br>
                                    Note: This course is an elective for the LifeMap Certification Program.
                                 </p>
                                 
                                 <p>                          LFMP 6340 Mentoring Students in Undergraduate Research<br>
                                    This course will provide the essential skills required to mentor honors students enrolled
                                    in the Undergraduate Research Track (IDH2912) course. Mentoring students in this track
                                    requires that the faculty member facilitate students in determining their research,
                                    establishing a research question, and conducting research specific to the mentor's
                                    own discipline. In addition, mentors will be responsible for guiding students in the
                                    presentation of their research in a formal setting in accordance with the guidelines
                                    set by that forum. Note: This is an optional course for the Seneff Faculty Development
                                    Program. This course is required for faculty mentoring students in the Honors Undergraduate
                                    Research Track who do not meet alternative credentialing.                       
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                        </div>           
                        
                        
                        
                        <h3>One-to-One Consultations</h3>
                        
                        <ul>
                           
                           <li>Meet with a member of our <a href="../Consulation.html">Faculty Development Team</a> on your campus
                           </li>
                           
                           <li>Partner with a colleague to observe different teaching techniques and reflect on your
                              own practice. <a href="../programs/learningPartners/index.html">Click here</a> to learn more. 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/coursesresources/lifemap.pcf">©</a>
      </div>
   </body>
</html>