<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources for Essential Competencies | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/coursesresources/assessmenttools.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Resources for Essential Competencies</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/coursesresources/">Coursesresources</a></li>
               <li>Resources for Essential Competencies</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        <h2>Assessment as a Tool for Learning</h2>
                        
                        <p><strong>Valencia educators will develop student growth through consistent, timely formative
                              and summative measures, and promote students’ abilities to self-assess. Assessment
                              practices will invite student feedback on the teaching and learning process as well
                              as on student achievement.&nbsp;&nbsp; </strong></p>
                        
                        <h3>Resources and Courses</h3>            
                        
                        <ul>
                           
                           <li>
                              <span><strong>Resources </strong></span>for Assessment include professional literature, practical examples, and applications
                              in diverse disciplines.
                           </li>
                           
                           <li> Faculty development<strong> Courses</strong> are designed to expand your expertise in Assessment.&nbsp; All courses related to Assessment
                              begin with the prefix, ASMT.&nbsp; 
                           </li>
                           
                           <li>To search for the following ASMT course offerings click <u><a href="http://www.valenciacollege.edu/faculty/development/courseSearch.cfm" target="_blank">here</a></u>. Please note:&nbsp; Not all courses are offered every year.
                           </li>
                           
                        </ul> 
                        
                        
                        
                        
                        <div>
                           
                           <div><a href="assessmentTools.html#">Overview of Assessment</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="http://www.kaltura.com/tiny/uhcu4" target="_blank" title="Overview of Assessment- Narrated Presentation">Overview of Assessment- Narrated Presentation</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 2121 Assessment as a Tool for Learning <br>
                                    Assessment at its best promotes students’ learning; it doesn’t just measure it. This
                                    seminar explores how to align learning outcomes with learning opportunities to create
                                    an effective assessment cycle. Participants will learn how to use formative and summative
                                    assessment methods, ensure alignment between learning activities and assessments,
                                    promote students’ self-assessment, and use regular, systematic feedback in the learning
                                    cycle. NOTE: This course is part of the TLA Core 
                                    Seminar series and open to all faculty.
                                 </p>
                                 
                                 <p>                          LCTS 1110 Teaching in Our Learning College<br>
                                    This scenarios-based course introduces the Essential Competencies of a Valencia Educator
                                    and provides faculty with tools and resources to become more effective, learning-centered
                                    instructors. Note: This course is required for the Associate Faculty Certification
                                    Program.                          <br>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="assessmentTools.html#">Authentic Assessment</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Assessment-AuthenticAssessment.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 2228 Authentic Assessment (not currently offered)<br>
                                    In this course, participants will examine assessment methods that require students
                                    to perform real-world tasks that demonstrate meaningful application of essential knowledge
                                    and skills.
                                 </p>
                                 
                                 <p>                          <br>
                                    ASMT 3353 Authentic Learning and Online Assessment<br>
                                    Concepts and best practice strategies from current literature about learning-centered
                                    online testing and grading are presented. Participants will share strategies and suggestions
                                    to design and improve online authentic assessments. Note: This online course is required
                                    for Digital Professor Certification.                          <br>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="assessmentTools.html#">Classroom Assessment Techniques</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Assessment-ClassroomAssessmentTechniques.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 2122 Classroom Assessment Techniques <br>
                                    This introductory course examines classroom assessment techniques that promote student
                                    learning through consistent, timely, formative measures. Also examined will be assessment
                                    practices that invite student feedback on the teaching and learning process, as well
                                    as on student achievement. In order to complete the weekly assignments, participants
                                    should be teaching a course while enrolled.                          <br>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="assessmentTools.html#">Formative Assessment</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Assessment-FormativeAssessment_001.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 2122 Classroom Assessment Techniques <br>
                                    This introductory course examines classroom assessment techniques that promote student
                                    learning through consistent, timely, formative measures. Also examined will be assessment
                                    practices that invite student feedback on the teaching and learning process, as well
                                    as on student achievement. In order to complete the weekly assignments, participants
                                    should be teaching a course while enrolled.                          <br>
                                    
                                 </p>
                                 
                                 <p>                        ASMT 3222 Strengthening Teaching and Learning through the
                                    Results of Your Student Assessment of Instruction<br>
                                    This course will introduce faculty members to the basics of student assessment of
                                    instruction at Valencia College through our online course evaluation system, including
                                    ways of increasing student participation.&nbsp; Advanced topics will also be covered such
                                    as the development of customized evaluation questions, the development of formative
                                    midterm assessment measures, and strategies for acting on your results in terms of
                                    teaching practices and professional portfolio development.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="assessmentTools.html#">Rubrics</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Assessment-Rubrics.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 2227 Understanding and Designing Rubrics<br>In this online, interactive course, participants will learn the elements of rubric
                                    construction and examine a variety of models to use in creating their own discipline-specific
                                    rubrics.
                                 </p>
                                 
                                 <p>ASMT 3220 Implementing Rubrics for Program Assessment <br>
                                    This course has been designed as an interactive working session for faculty implementing
                                    rubrics for the review of student work across course sections / courses.&nbsp; Faculty
                                    members participating from the same programs will be invited to use their own student
                                    work.&nbsp; The session will cover sampling, inter-rater reliability, norming of review
                                    teams, and the consistent application of criteria to student artifacts and assignments
                                    for the assessment of program learning outcomes.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="assessmentTools.html#">Summative Assessment</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Assessment-SummativeAssessment.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 2226 Online Course Summative Assessment<br>
                                    This course is designed to introduce the topic of assessment in online courses to
                                    those who are thinking about teaching online or who are in the initial stages of teaching
                                    online. It is designed to acquaint participants with best practices in online assessment,
                                    concentrating on summative assessment, and to have participants apply what they have
                                    learned in an online environment.
                                 </p>
                                 
                                 <p>                          ASMT 2229 Assess Higher Order Thinking Through Multiple
                                    Choice Questions<br>
                                    In this course, faculty members will enhance their ability to develop, test, and revise
                                    multiple choice questions drawing on hands-on activities using examples from Valencia
                                    College faculty. Issues of validity including reliability will be included in the
                                    conversation.                        
                                 </p>
                                 
                                 <p><br>
                                    See also <strong>Learning-centered Teaching Strategies</strong></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="assessmentTools.html#">Multiple Choice Question Development</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <h3>Faculty Resources for Multiple Choice Question Design:</h3>
                                 
                                 <p>Multiple Choice Question Development and Design- Facilitated by Steven Downing</p>
                                 
                                 <p><em>On Friday, October 12, 2012 Dr. Steven Downing visited Valencia to facilitate two
                                       faculty trainings on Multiple Choice Question Design. Links to resources from these
                                       trainings are available below.</em></p>
                                 
                                 <p><em> Please check out the Faculty Development catalog or contact their office for future
                                       workshops on multiple choice questions. </em></p>
                                 
                                 <p><a href="http://valenciacollege.edu/faculty/development/coursesResources/documents/SteveDowning_ItemWriting_Valencia_101212.pptx" target="_blank">Presentation Power Point </a></p>
                                 
                                 <p><br>
                                    
                                 </p>
                                 
                                 <h3>Resources:</h3>
                                 <br>
                                 <a href="documents/SteveDowningHandout1.pdf" target="_blank">Written Tests:  Constructed-Response and Selected-Response Formats- Downing<br>
                                    </a><a href="documents/SteveDowningHandout2.pdf" target="_blank">The Effects of Violating Standard Item Writing Principles on Tests and Students- Downing</a><br>
                                 <a href="documents/SteveDowningHandout3.pdf" target="_blank">Revised Taxonomy of Multiple Choice Item Writing Guidelines- Downing</a><br>
                                 <a href="documents/SteveDowningHandout4.pdf" target="_blank">A Review of Multiple-Choice Item-Writing Guidelines Classroom Assessment- Haladyna,
                                    et al</a>. <br>
                                 <a href="http://www.nbme.org/PDF/ItemWriting_2003/2003IWGwhole.pdf" target="_blank">Constructing Written Test Questions- Case and Swanson </a><br>
                                 <br>
                                 <a href="http://www.biomedexperts.com/Profile.bme/340166/Steven_M_Downing" target="_blank">About Steven Downing</a> <a href="documents/SteveDowningBio.pdf" target="_blank">Steven's Biography</a> <a href="documents/SteveDowning_CV.pdf" target="_blank">Steven's CV</a>
                                 
                                 
                                 <p><em>Want to learn more about resources<br>
                                       </em><em>to support faculty use of scanners, scantron forms, <br>
                                       and other support to individually create and score multiple<br>
                                       choice question exams in your classes?</em></p>
                                 
                                 <p><em>Please contact your campus representative to find out more. <br>
                                       East Campus: <a href="mailto:alangevin@valenciacollege.edu">Alison Langevin</a><br>
                                       Osceloa Campus: <a href="mailto:nscoltock@valenciacollege.edu">Nancy Scoltock</a><br>
                                       West Campus: <a href="mailto:cfox17@valenciacollege.edu">Charles Fox</a></em></p>
                                 
                                 <h4>&nbsp;</h4>
                                 
                                 <h4>
                                    <a href="../../../academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/default.html#Dates" target="_blank"><img alt="Star" height="56" src="GoldStar.jpg.html" width="59"><br>
                                       Important Program Assessment Planning Dates for Fall and Spring</a>  
                                 </h4>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="assessmentTools.html#">Quality Matters</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="https://www.qualitymatters.org/applying-rubric-15/download/QM_Overview_for%20Current%20Subscribers_AE2013.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 3232 Quality Matters: Peer Reviewer Training<br>
                                    This course will explore the Quality Matters project and processes and will prepare
                                    the participant to be part of an initiative that impacts the design of online courses
                                    and ultimately, student success. After successfully completing this course, participants
                                    will be eligible to serve on a Valencia Quality Matters peer course review. Note:
                                    This course is required for Digital Professor Certification.      
                                 </p>
                                 
                                 <p>ASMT 3234 Quality Matters: Refresher Training<br>
                                    This course is designed to highlight the changes to the Quality Matters Rubric and
                                    prepare participants to conduct internal Quality Matters reviews using the updated
                                    Quality Matters Rubric. Prerequisite: this course is for participants who have already
                                    taken ASMT 3232, QM: Peer Reviewer Training.                          
                                 </p>
                                 
                                 <p>ASMT 3235 Quality Matters: Peer Review Panel Training<br>
                                    The purpose of this course is to prepare faculty members to serve on an internal Quality
                                    Matters peer review team. This course is required for faculty who will serve as committee
                                    members or as the committee chair. Prerequisite: This course is for participants who
                                    have already taken ASMT3232 QM Peer Reviewer Training and have been selected to serve
                                    on a peer review team.                        
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>            
                        
                        
                        
                        <h3>One-to-One Consultations</h3>
                        
                        <ul>
                           
                           <li>Meet with a member of our <a href="../Consulation.html">Faculty Development Team</a> on your campus.
                           </li>
                           
                           <li>Partner with a colleague to observe different teaching techniques and reflect on your
                              own practice. <a href="../programs/learningPartners/index.html" target="_blank">Click here</a> to learn more. 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/coursesresources/assessmenttools.pcf">©</a>
      </div>
   </body>
</html>