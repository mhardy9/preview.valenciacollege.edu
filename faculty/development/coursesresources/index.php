<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources for Essential Competencies | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/coursesresources/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Resources for Essential Competencies</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li>Coursesresources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Essential Competencies of a Valencia Educator</h2>
                        
                        <p>Teaching is a profession defined by essential competencies that develop from the changing
                           needs of our educational community. These changes emerge organically from the community
                           of scholars, practitioners, students, and others we serve. At Valencia, we have defined
                           seven Essential Competencies that were developed by our academic community of deans
                           and faculty. These competencies serve as the foundation of our ongoing development
                           as teachers, counselors and librarians. Over the course of an entire career, faculty
                           members continually develop their expertise in these essential competencies.
                        </p>
                        
                        <p>Faculty Development and the Teaching/Learning Academy support all faculty members
                           as they expand their professional practices, examine their ongoing development of
                           the Essential Competencies of a Valencia Educator, and engage in continuous improvement
                           processes that result in student learning. As such, these seven Essential Competencies
                           of a Valencia Educator form the foundation of all of our faculty development opportunities.
                        </p>
                        
                        <div>
                           
                           <div><a href="/faculty/development/coursesresources/index.html">Assessment as a Tool for Learning</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia educators will develop student growth through consistent, timely formative
                                    and summative measures, and promote students' abilities to self-assess. Assessment
                                    practices will invite student feedback on the teaching and learning process as well
                                    as on student achievement.
                                 </p>
                                 
                                 <p>The faculty member will:</p>
                                 
                                 <ul>
                                    
                                    <li>Design and employ a variety of assessment measures and techniques, both formative
                                       and summative, to form a more complete picture of learning (e.g., classroom assessment
                                       techniques, authentic assessments, oral presentations, exams, student portfolios,
                                       journals, projects, etc.)
                                    </li>
                                    
                                    <li>Design activities to help students refine their abilities to self-assess their learning</li>
                                    
                                    <li>Employ formative feedback to assess the effectiveness of teaching, counseling, and
                                       librarianship practices
                                    </li>
                                    
                                    <li>Employ formative feedback loops that assess student learning and inform students of
                                       their learning progress
                                    </li>
                                    
                                    <li>Communicate assessment criteria to students and colleagues</li>
                                    
                                    <li>Give timely feedback on learning activities and assessments</li>
                                    
                                    <li>Evaluate effectiveness of assessment strategies and grading practices</li>
                                    
                                    <li>Align formative and summative assessments with learning activities and outcomes</li>
                                    
                                 </ul>
                                 
                                 <p>Resources:</p>
                                 
                                 <ul>
                                    
                                    <li><strong> <a href="http://circlesofinnovation.valenciacollege.edu/?s=assessment" target="_blank">Circles of Innovation: Assessment </a></strong></li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/CarolBostonFormativeAssessment.pdf" target="_blank"><strong>The Concept of Formative Assessment</strong></a>" by Carol Boston of the Center for the Study of Assessment Validity and Evaluation
                                       at the University of Maryland provides a scholarly definition of formative assessment,
                                       its benefits and examples of formative assessment.
                                    </li>
                                    
                                    <li><a href="http://edglossary.org/formative-assessment/" target="_blank"><strong>Formative Assessment</strong>.</a> Glossary of Educational Reform.
                                    </li>
                                    
                                    <li><a href="http://edglossary.org/summative-assessment" target="_blank"><strong>Summative Assessment.</strong></a> Glossary of Educational Reform.
                                    </li>
                                    
                                    <li>"<a href="https://www.cmu.edu/teaching/assessment/basics/" target="_blank"><strong>Assessment Basics</strong></a>" from the Eberly Center for Teaching Excellence at Carnegie Mellon University. Includes
                                       topics such as "Aligning Assessments with Objectives" (aka learning ) and "Assessing
                                       Your Teaching." Also includes examples of CATs and rubrics.
                                    </li>
                                    
                                    <li>"<a href="http://aacu.org/value/rubrics" target="_blank"><strong>AAC &amp; U VALUE rubrics</strong></a>" Valid Assessment of Learning in Undergraduate Education from the Association of
                                       American Colleges and Universities.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/SelectingmethodsofassessmentOxfordBrookesUniversity.pdf" target="_blank"><strong>Selecting Methods of Assessment</strong></a>" from the Oxford Brookes University lists assessment methods organized by learning
                                       outcomes.
                                    </li>
                                    
                                    <li>"<a href="http://www.ion.uillinois.edu/resources/otai/" target="_blank"><strong>Online Activity Index</strong></a>" from Illinois Online Network provides an extensive list of links to various types
                                       of online learning activities ranging from podcasting to jigsaws, with rationales
                                       and methods of assessing each. While aimed at K-12, the site is equally valid for
                                       college and will be particularly useful for faculty members using technologically
                                       to enhance instruction.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/CIQ_brookfield.pdf" target="_blank"><strong>Critical Incident Questionnair</strong>e</a>" by Stephen Brookfield from Becoming a Critically Reflective Teacher is a classic
                                       student input assessment tool to gather feedback on classroom climate.
                                    </li>
                                    
                                    <li>"<a href="http://course1.winona.edu/shatfield/air/rubrics.htm" target="_blank"><strong>Assessment Resources: Sample Rubrics</strong></a>" from Winona State University is a metasite with links to examples from a variety
                                       of disciplines and learning activities.
                                    </li>
                                    
                                    <li><a href="http://jfmueller.faculty.noctrl.edu/toolbox/" target="_blank"><strong>The Authentic Assessment Toolbox</strong></a> by Jonathan Mueller is a one-stop center for all things related to authentic assessment,
                                       including explanations, research, and examples.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/14RulesforWritingMultiple-ChoiceQuestions2009.pdf" target="_blank"><strong>14 Rules for Writing Multiple Choice Questions</strong></a>, Penn State University Center for Teaching &amp; Learning.&nbsp;
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Professional Organizations &amp; Conferences:</p>
                                 
                                 <ul>
                                    
                                    <li><strong><a href="http://www.aalhe.org/">Association for the Assessment of Learning in Higher Education</a> </strong></li>
                                    
                                    <li><strong><a href="http://www.learningoutcomeassessment.org/LOAcommunities.htm">National Institute for Learning Outcomes Assessment</a></strong></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div><a href="/faculty/development/coursesresources/index.html">Inclusion and Diversity</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia educators will design learning opportunities that acknowledge, draw upon
                                    and are enriched by student diversity. Diversity has many dimensions, including sex,
                                    gender identity, sexual orientation, race, ethnicity, socio-economic background, disability,
                                    cognitive style, skill level, age, religion, etc. An atmosphere of inclusion and understanding
                                    will be promoted in all learning environments.
                                 </p>
                                 
                                 <p>The faculty member will:</p>
                                 
                                 <ul>
                                    
                                    <li>design and support learning experiences that address students' unique strengths and/or
                                       needs
                                    </li>
                                    
                                    <li>diversify the curricular and/or co-curricular activities to increase the presence
                                       of historically underrepresented groups
                                    </li>
                                    
                                    <li>use diverse perspectives to engage and deepen critical thinking</li>
                                    
                                    <li>create a learning atmosphere with respect, understanding, and appreciation of individual
                                       and group differences
                                    </li>
                                    
                                    <li>challenge students to identify and question their assumptions and consider how these
                                       affect, limit, and/or shape their viewpoints
                                    </li>
                                    
                                    <li>ensure accessibility of course content in alignment with federal law and Valencia
                                       standards
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Resources:</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/?s=diversity" target="_blank"><strong>Circles of Innovation: Inclusion &amp; Diversity </strong></a></li>
                                    
                                    <li><a href="http://cft.vanderbilt.edu/guides-sub-pages/diversity/" target="_blank"><strong>Diversity and Inclusive Teaching</strong>.</a> Vanderbilt University provides an extensive explanation and rationale for diversity
                                       and inclusion in higher education.
                                    </li>
                                    
                                    <li><strong>"<a href="http://www.udlcenter.org/aboutudl/udlguidelines" target="_blank">UDL Guidelines - Version 2.0</a>"</strong> Introduction" from the National Center on the Universal Design for Learning. UDL
                                       Guidelines are organized according to the three main principles of UDL.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/EffectsPersonalityTypesFelder.pdf" target="_blank"><strong>The Effects of Personality Type on Engineering Student Performance and Attitudes</strong></a>" by Richard M. Felder, Dept.of Chemical Engineering, NCSU, G.N. Felder, Dept. of
                                       Physics, Stanford, and E.J. Dietz, Dept.of Statistics, NCSU. While this study examines
                                       chemical engineering students and found that "the MBTI is a useful tool for helping
                                       engineering instructors understand their students, the findings can be transferred
                                       to other disciplines.
                                    </li>
                                    
                                    <li>Valencia College <a href="/office-for-students-with-disabilities/default.html" target="_blank"><strong>Office for Students with Disabilities</strong></a>. Provides faculty resources for unlocking the door to access.
                                    </li>
                                    
                                    <li><a href="http://www.cte.cornell.edu/teaching-ideas/building-inclusive-classrooms/inclusive-teaching-strategies.html" target="_blank"><strong>Inclusive Teaching Strategies</strong></a>. Cornell University provides rationale as well as inclusive teaching strategies (see
                                       left navigation bar also).
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/4StageModelofMathLearningKolb.pdf" target="_blank"><strong>A Four-Stage Model of Mathematical Learning</strong></a>" by Jeff Kinsley, Department of Mathematics, East Tennessee State University. An
                                       application of Kolb's theory of learning to the teaching of mathematics but can be
                                       applied to other disciplines.
                                    </li>
                                    
                                    <li><a href="http://serc.carleton.edu/NAGTWorkshops/affective/motivation.html" target="_blank">"<strong>Motivation</strong></a>" written and compiled by Karen Kirk, from the Cutting Edge: Professional Development
                                       for Geoscience Faculty. This website provides discipline specific resources applicable
                                       to all disciplines.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/RedefiningDiversityLibrary.pdf" target="_blank"><strong>Redefining Diversity: Creating an Inclusive Academic Library through Diversity Initiatives</strong></a>" by Ann T. Switzer Oakland University presents a rationale and methodology for reaching
                                       a wide range of students.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/BridgingBrevityDiversityINDVcounselors.pdf" target="_blank"><strong>A Multicontextual Model of Counseling: Bridging Brevity and Diversity</strong></a> by Brett N. Steenbarger outlines and illustrates an integrative and contextual model
                                       of counseling.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Professional Organizations &amp; Conferences</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.nadohe.org/" target="_blank"><strong>National Association of Diversity Officers in Higher Education</strong></a></li>
                                    
                                    <li><strong><a href="https://www.naspa.org" target="_blank">National Association of Student Personnel and Administrators (NASPA)</a></strong></li>
                                    
                                    <li><a href="http://www.lgbtcampus.org/" target="_blank"><strong>Consortium of Higher Education LGBT Resource Professionals</strong></a></li>
                                    
                                    <li><a href="https://www.ahead.org/" target="_blank"><strong>Association of Higher Education and Disability (AHEAD</strong></a>)
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div><a href="/faculty/development/coursesresources/index.html">Learning-centered Teaching Practice</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia educators will implement diverse teaching and learning strategies that promote
                                    active learning and that foster both acquisition and application of knowledge and
                                    understanding.
                                 </p>
                                 
                                 <p>The faculty member will:</p>
                                 
                                 <ul>
                                    
                                    <li>employ strategies that engage students to become more active learners (e.g., reference
                                       interviews, counseling inquiry, engaging lectures, classroom discussions, case studies,
                                       scenarios, role-play, problem-based learning, inquiry-based learning, manipulatives,
                                       etc.)
                                    </li>
                                    
                                    <li>encourage students to challenge ideas and sources (e.g., debates, research critiques,
                                       reaction reports, etc.)
                                    </li>
                                    
                                    <li>use cooperative/collaborative learning strategies (e.g., peer to peer review, team
                                       projects, think/pair/share, etc.)
                                    </li>
                                    
                                    <li>incorporate concrete, real-life situations into learning activities</li>
                                    
                                    <li>invite student input on their educational experience (e.g., choice among assignment
                                       topics, classroom assessment techniques, etc.)
                                    </li>
                                    
                                    <li>employ methods that develop student understanding of discipline's thinking, practice,
                                       and procedures
                                    </li>
                                    
                                    <li>employ methods that increase the students' academic literacy within the discipline
                                       or field (e.g., reading, writing, numeracy, technology skills, information literacy,
                                       etc.)
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Resources:</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/?s=+teaching+strategies" target="_blank"><strong>Circles of Innovation: Learning-centered Teaching Practice</strong></a></li>
                                    
                                    <li><a href="http://cei.umn.edu/support-services/tutorials/what-active-learning/making-active-learning-work" target="_blank"><strong>Making Active Learning Work</strong></a> from the Center for Teaching and Learning, University of Minnesota.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/NavigatingtheBumpyRoadFelderandBrent.pdf" target="_blank"><strong>Navigating the Bumpy Road to Student Centered Instruction</strong></a>" by Richard M. Felder and Rebecca Brent, North Carolina State University. This article
                                       addresses common faculty concerns about using student centered instruction.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/LCTSLibrary.pdf" target="_blank"><strong>Information Literacy and higher Education: Placing the Academic Library and the Center
                                             of the Comprehensive Solution</strong></a> by Edward K. Owusu-Ansah addresses the significance of the close academic relationship
                                       between librarian and classroom faculty especially in terms of information literacy.
                                       This article illustrates Valencia's forward-thinking philosophy of viewing professors,
                                       counselors and librarians all as equal members of the faculty.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/SevenPrinciples.pdf" target="_blank"><strong>Seven Principles for Good Practice In Undergraduate Education</strong></a> by Arthur W. Chickering and Zelda F. Gamson is a classic article providing tenets
                                       for learning-centered teaching. This article clearly illustrates the overlap of the
                                       Essential Competencies of a Valencia Educator.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/CooperativeLearninginHigherEdLedlow.pdf" target="_blank"><strong>Cooperative Learning in Higher Education</strong></a>" by Susan Ledlow of the Center for Teaching and Learning Excellence. A scholarly
                                       article on the basics of cooperative learning from one of the leading experts on the
                                       subject.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/CLStrategiesFelderBrent.pdf" target="_blank"><strong>Effective Strategies for Cooperative Learning</strong>.</a> By Richard M. Felder and Rebecca Brent. This article provides tips on forming teams,
                                       dealing with dysfunctional teams, grading team assignments, and using cooperative
                                       learning in a distance learning environment.
                                    </li>
                                    
                                    <li><a href="http://www.cord.org/cord_ctl_react.php" target="_blank"><strong>REACT Strategy</strong>.</a> From Texas Collaborative for Teaching Excellence. Explains how the acronym REACT
                                       (Relating, Experiencing, Applying, Cooperating, and Transferring) helps structure
                                       contextual learning.
                                    </li>
                                    
                                    <li>"<a href="http://www.criticalthinking.net/teaching.html" target="_blank">T<strong>eaching Critical Thinking</strong>:<strong> A Few Suggestions</strong></a>" by Robert H. Ennis, University of Illinois. This is a list of 13 practical suggestions
                                       from a larger work on critical thinking.
                                    </li>
                                    
                                    <li>"<a href="http://www.ion.uillinois.edu/resources/tutorials/pedagogy/instructionalstrategies.asp#COLLABORATIVE%20LEARNING" target="_blank"><strong>Instructional Strategies for Online Courses</strong></a>" from Illinois Online Network. Identifies ways online faculty can customize instruction
                                       for differing learning styles,
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/ActiveLearningLibrary.pdf" target="_blank"><strong>Active Learning and Library Instruction</strong></a>" by Michael Lorenzen , Michigan State University describes the importance and role
                                       of active learning in the academic library environment.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/EmbeddedLibraryinstuction.pdf" target="_blank"><strong>The 'Embedded' Librarian in a Freshman Speech Class: Information Literacy Instruction
                                             in Action</strong></a>" by R. A. Hall. This article, published in the College &amp; Research Libraries News,
                                       provides an example of how librarians can contribute to information literacy and critical
                                       thinking in the context of a course.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/LessonDeliveryEffectiveLectures.pdf" target="_blank"><strong>Effective Lecturing</strong></a> provides some guidelines for appropriate uses of the lecture, lecturing with PowerPoint,
                                       and making lectures more active.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Professional Organizations &amp; Conferences</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.nea.org/" target="_blank"><strong>National Education Association (NEA) </strong></a></li>
                                    
                                    <li><a href="https://www.aaup.org/" target="_blank"><strong>American Association of University Professors</strong></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div><a href="/faculty/development/coursesresources/index.html">LifeMap</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia educators will design learning opportunities that promote student life skills
                                    development while enhancing discipline learning. Through intentional inclusion of
                                    growth-promoting strategies, faculty will facilitate the students' gradual assumption
                                    of responsibility for making informed decisions and formulating and executing their
                                    educational, career, and life plans.
                                 </p>
                                 
                                 <p>The faculty member will:</p>
                                 
                                 <ul>
                                    
                                    <li>establish student &amp; faculty contact that contributes to students' academic, personal,
                                       and professional growth
                                    </li>
                                    
                                    <li>employ digital tools to aid student contact (e.g., Atlas, MyPortfolio, Blackboard,
                                       Ask-A-Librarian, email, etc.)
                                    </li>
                                    
                                    <li>seek out struggling students and identify options through dialog and appropriate referrals</li>
                                    
                                    <li>help students assume responsibility for making informed academic decisions (e.g.,
                                       degree requirements, transfer options, financial aid, etc.)
                                    </li>
                                    
                                    <li>guide students in developing academic behaviors for college success (e.g., time management,
                                       study, test and note taking strategies, etc.)
                                    </li>
                                    
                                    <li>help students identify academic behaviors that can be adapted as life skills (e.g.,
                                       library search skills, decision-making, communication skills, scientific understanding,
                                       etc.)
                                    </li>
                                    
                                    <li>assist students in clarifying and developing purpose (attention to life, career, education
                                       goals)
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Resources:</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/?s=lifemap" target="_blank"><strong>Circles of Innovation: LifeMap </strong></a></li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/DevelopmentalAdvising_1993SusanFrost.pdf" target="_blank"><strong>Developmental Advising: Practices and Attitudes of Faculty Advisors</strong></a> by Susan H. Frost, Emory University. This article poses the theoretical framework
                                       from which LifeMap was designed.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/DevViewAcademicAdvisingasTeaching.pdf" target="_blank"><strong>A Developmental View of Academic Advising as Teaching</strong></a> by Burns B. Crookston, University of Connecticut.
                                    </li>
                                    
                                    <li><a href="/lifemap/index.html"><strong>LifeMap</strong></a> from Valencia College. This site includes the LifeMap definition, a list of student
                                       benefits, an explanation of how students get started with LifeMap, and more.
                                    </li>
                                    
                                    <li>"<a href="http://serc.carleton.edu/NAGTWorkshops/affective/efficacy.html" target="_blank"><strong>Self-Efficacy: Helping Students Believe in Themselves</strong></a>" by K. Kirk. This webpage, part of a professional development website for geosciences
                                       faculty, includes research and teaching strategies for helping struggling students.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/IntrinsicStudentMotivationMcKinney.pdf" target="_blank"><strong>Encouraging Students' Intrinsic Motivation</strong></a>" by Kathleen McKinney, Illinois State This article, from the National Forum for Teaching
                                       and Learning provides tips for emphasizing intrinsic, rather than, extrinsic motivation.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/LifeMapCollegeSuccessSkills.pdf" target="_blank"><strong>LifeMap: College Success Skills</strong></a> connects Valencia's learning-centered paradigm to student's cognitive, knowledge
                                       and skills, academic behaviors, and planning/decision making development. Concrete
                                       student success skills are outlined.
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div><a href="/faculty/development/coursesresources/index.html">Outcomes-based Practice</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The Essential Competency areas of Outcomes-based Practice and Assessment work hand
                                    in hand, but they are not the same thing. Outcomes-based Practice is the process of
                                    identifying what the learner should be able to do as a direct result of teaching/learning
                                    activities. Effective assessment helps us measure the level at which students achieve
                                    these desired outcomes. Creating appropriate outcomes is a different area for study
                                    and practice, crucial in establishing expectations for students.
                                 </p>
                                 
                                 <p>Valencia educators will design and implement learning activities that intentionally
                                    lead students towards mastery in the Student Core Competencies (Think, Value, Communicate,
                                    and Act) as well as the related course and program outcomes. The key question is "What
                                    will students be able to do as a result of the instruction?"
                                 </p>
                                 
                                 <p>The faculty member will:</p>
                                 
                                 <ul>
                                    
                                    <li>create a new, or revised, learning outcome for a unit, course or program that meets
                                       the criteria for learning outcomes (this performance indicator must be used in conjunction
                                       with at least one other Outcomes-based Practice indicator for demonstration in faculty
                                       portfolios)
                                    </li>
                                    
                                    <li>align unit, course, and/or program outcomes with one or more student core competencies
                                       (Think, Value, Communicate &amp; Act)
                                    </li>
                                    
                                    <li>collect evidence of progress toward student achievement of unit, course, or program
                                       learning outcomes
                                    </li>
                                    
                                    <li>sequence learning opportunities and assessments throughout units, courses, programs,
                                       and developmental advising to build student understanding and knowledge
                                    </li>
                                    
                                    <li>help students understand their growth in the acquisition of student core competencies
                                       (Think, Value, Communicate &amp; Act) and program learning outcomes
                                    </li>
                                    
                                    <li>use evidence of student learning to review and improve units, courses, and programs
                                       (in classroom, counseling and library settings)
                                    </li>
                                    
                                    <li>ensure that unit, course, and program learning outcomes are current and relevant for
                                       future academic work and/or vocational and employment opportunities.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Resources:</p>
                                 
                                 <ul>
                                    
                                    <li><strong><a href="http://circlesofinnovation.valenciacollege.edu/?s=outcomes+practice" target="_blank">Circles of Innovation: Outcomes-based Practice </a></strong></li>
                                    
                                    <li><strong><a href="http://www.kaltura.com/tiny/r8v5b" target="_blank">Outcomes-based Practice, An Essential Competency of a Valencia Educator</a> </strong>presentation by Helen Clarke and Wendi Dew
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/HowtowriteaMeasureableLearningOutcomeFLOandSLO.pdf" target="_blank"><strong>Guidelines for Writing Measurable Learning Outcomes (FLO and SLO) </strong></a></li>
                                    
                                    <li><strong><a href="/faculty/development/coursesresources/documents/THEsaurusofVerbsandMLOCriteria.pdf" target="_blank">Thesaurus of Verbs Helpful in Writing Measurable Learning Outcomes</a></strong></li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/SLOTutorialforprogramoutcomes_000.pdf" target="_blank"><strong>Writing a Measurable Program Learning Outcome</strong></a></li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/BarrTagg.pdf" target="_blank">"<strong>From Teaching to Learning: A New Paradigm for Undergraduate Education</strong>"</a> by R. B. Barr and J. Tagg. This 1995 article from <u>Change Magazine</u> laid the theoretical and pedagogical foundation for he learning-centered paradigm
                                       in higher education.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/FocusonLearningOBanion.pdf" target="_blank"><strong>Focus on Learning: The Core Mission of Higher Education</strong></a> by Terry O'Banion, a pioneer of the learning-centered paradigm, discusses the concept
                                       and provides a comprehensive reference list.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/TeachingvLearningEnvironmentRogersEssay.pdf" target="_blank"><strong>Teaching vs. Learning Environments</strong></a> David Rogers, Professor of English, Osceola Campus presents a jargon-free explanation
                                       of Valencia's learning-centered paradigm.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/CTFrameworkArticle.pdf" target="_blank"><strong>Critical Thinking Framework for Any Discipline</strong></a>" by Robert Duron, Barbara Limbach and Wendy Waugh. This article, from the <u>International Journal for Teaching and Learning</u>, uses accounting as a model for demonstrating how to infuse critical thinking into
                                       any discipline. It also illustrates how Outcomes-based Practice connect learning outcomes,
                                       teaching strategies and assessments.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/WhatisBackwardDesignWigginsMctighe.pdf" target="_blank"><strong>What is Backward Design</strong></a> by Grant Wiggins and Jay McTighe offers a thorough introduction to outcomes-based
                                       paradigm and assessment planning.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/ConstructanLDSequence.pdf" target="_blank">"<strong>Guidelines to Construct a Learning Design Sequence</strong>."</a> Graphic explanation of learning opportunities and assessments to achieve learning
                                       outcomes.
                                    </li>
                                    
                                    <li><a href="http://avillage.web.virginia.edu/iaas/assess/resources/learningoutcomes.shtm" target="_blank">"<strong>Student Learning Outcomes Assessment</strong>"</a> from the University of Virginia. Examples of program level and general education
                                       learning outcomes.
                                    </li>
                                    
                                    <li><strong>"<a href="/faculty/development/coursesresources/documents/DeeFinkCourseDesign.pdf" target="_blank">A Self-Directed Guide to Designing Courses for Significant Learning</a></strong>." Dee Fink. Rationale and instruction on how to design a Outcomes-based course.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/OBPLibraryTech.pdf" target="_blank"><strong>Using Outcome-Based Education in the Planning and Teaching of New Information Technologies</strong></a>" by Michael Lorenzen, Michigan State University Libraries describes the rationale
                                       and mythology for designing library learning outcomes.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Professional Organizations &amp; Conferences</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.nea.org/" target="_blank"><strong>National Education Association (NEA) </strong></a></li>
                                    
                                    <li><a href="https://www.aaup.org/" target="_blank"><strong>American Association of University Professors</strong></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div><a href="/faculty/development/coursesresources/index.html">Professional Commitment</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia educators will stay current and continually improve their mastery of discipline/academic
                                    field, their excellence in pedagogy, and their active participation in the college's
                                    learning mission.
                                 </p>
                                 
                                 <p>The faculty member will:</p>
                                 
                                 <ul>
                                    
                                    <li>stay current in discipline/academic field (e.g., professional organizations, conferences,
                                       journals, reading in the discipline, field work or clinical experience, etc.)
                                    </li>
                                    
                                    <li>contribute to discipline/academic field (e.g., publications, presentations at discipline-based
                                       conference, poster sessions, writing articles, editing learning material, curriculum
                                       development, field work, sharing clinical experience, contributing to textbooks, sharing
                                       research with peers, etc.)
                                    </li>
                                    
                                    <li>participate in faculty development programs, resources or classes, whether Valencia-based
                                       or external university/college-based
                                    </li>
                                    
                                    <li>stay current with technological tools and/or platforms within discipline and at the
                                       college
                                    </li>
                                    
                                    <li>engage in ongoing discourse surrounding division, campus, and college work (e.g.,
                                       meetings, ongoing committees, work teams, task forces, "Big Meetings," governing councils,
                                       etc.).
                                    </li>
                                    
                                    <li>collaborate with peers both in and out of discipline/academic field (e.g., develop
                                       educational materials to be shared; participate in peer observation of teaching, mentoring
                                       programs, or learning partners, etc.).
                                    </li>
                                    
                                    <li>engage in expanding and building institutional, programmatic and personal connections
                                       to the wider community (e.g., community involvement, service learning, civic engagement,
                                       board of [museums, hospital, etc.], partner K12 schools, student development leadership
                                       or activities, etc.).
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Resources:</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/?s=work+commitment" target="_blank"><strong>Circles of Innovation: Professional Commitment</strong></a></li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/HeartofaTeacherParkerPalmer.pdf" target="_blank"><strong>The Heart of a Teacher: Identity and Integrity in Teaching</strong></a> by Parker J Palmer the widely published educational philosopher.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/625.full.pdf" target="_blank"><strong>Cultivating your Leadership Skills: Online Resources to Develop and Strengthen your
                                             Leadership Role</strong></a>" by R Kear. This article, published in the College &amp; Research Libraries News, provides
                                       links to resources for the following aspects of leadership: modeling, inspiring a
                                       shared vision, enabling others to act, challenging the process, and encouraging the
                                       heart.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/TwoYearswithaTeeBallBatLibrarianasFacultySenateChair.pdf" target="_blank"><strong>Two Years with a Tee Ball Bat: The Librarian as Faculty Senate Chair</strong></a>" by L.S. Crieder. Although this article, published in the College &amp; Research Libraries
                                       News, is written by a college librarian, the practical tips and recommendations it
                                       provides are useful for anyone in a post-secondary leadership role.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/WritingPeerReviewedArticle.pdf" target="_blank"><strong>Developing as a Writer: Refereeing Manuscripts for Peer-Reviewed LIS Journals</strong></a>" by P. M. Edwards. Although this article, published in the College &amp; Research Libraries
                                       News, is writtenfrom the perspective on LIS journals, the rationale for professional
                                       benefits of being a referee, recommendations for becoming a referee, and suggestions
                                       for reviewing manuscripts apply to anyone in post-secondary education.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/coursesresources/documents/ReadingCommunities.pdf" target="_blank"><strong>A Librarian's Journal Club: A Forum for Sharing Ideas and Experiences</strong></a>" by T. Hickman and L. Allen. This article, published in the College &amp; Research Libraries
                                       News, describes how to create a collegial group for reading and sharing professional
                                       literature.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/MentoringPRFCcounseling.pdf" target="_blank"><strong>Mentoring Promotion/Tenure-Seeking Faculty: Principles of Good Practice Within a Counselor
                                             Education Program</strong></a> by L. DiAnne Borders, et. al. presents mentoring principles for mentoring junior
                                       faculty in a counseling environment but easily applies to other disciplines faculty.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Professional Organizations &amp; Conferences</p>
                                 
                                 <ul>
                                    
                                    <li><a href="https://www.aacc.nche.edu/" target="_blank"><strong>American Association of Community Colleges</strong></a></li>
                                    
                                    <li><span><a href="http://associationdatabase.com/aws/NCDA/pt/sp/home_page" target="_blank">National Career Development Association</a></span></li>
                                    
                                    <li><strong><a href="http://www.ashe.ws/" target="_blank">Association of the Study of Higher Education (ASHE)</a></strong></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div><a href="/faculty/development/coursesresources/index.html">Scholarship of Teaching and Learning</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia educators will continuously examine the effectiveness of their teaching,
                                    counseling, librarianship and assessment methodologies in terms of student learning.
                                    They also will keep abreast of the current scholarship in the fields of teaching and
                                    learning.
                                 </p>
                                 
                                 <p>The faculty member will:</p>
                                 
                                 <ul>
                                    
                                    <li>produce professional scholarly work (action research or traditional research) related
                                       to teaching and learning, that meets the Valencia Standards of Scholarship
                                    </li>
                                    
                                    <li>build upon the work of others (consult experts, colleagues, self, students)</li>
                                    
                                    <li>be open to constructive critique (by both colleagues and students)</li>
                                    
                                    <li>make professional scholarly work public to college and broader audiences through Valencia's
                                       research repository and other means
                                    </li>
                                    
                                    <li>collect evidence of the relationship of SoTL to improved teaching and learning</li>
                                    
                                    <li>demonstrate use of current teaching and learning theory &amp; practice</li>
                                    
                                 </ul>
                                 
                                 <p>Resources:</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/?s=scholarship+for+teaching+and+learning" target="_blank"><strong>Circles of Innovation: Scholarship of Teaching &amp; Learning </strong></a></li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/ScholarlyTeachingRichieSOTL.pdf" target="_blank"><strong>Scholarly Teaching and the Scholarship of Teaching</strong></a> by Laurie Richlin discusses the ongoing assessment cycles that lead to improved teaching
                                       and more.
                                    </li>
                                    
                                    <li>"<a href="http://www.fctl.ucf.edu/researchandscholarship/sotl/" target="_blank"><strong>What is the Scholarship of Teaching and Learning?</strong></a>" from the Faculty Center at the University of Central Florida. This website provides
                                       a brief definition of SoTL, as well as links for drilling down to more specific topics
                                       and information.
                                    </li>
                                    
                                    <li>"<a href="/faculty/development/concentrations/actionResearch.html" target="_blank"><strong>Action Research</strong></a>" from Faculty Development at Valencia College provides an explanation of the nature
                                       and elements of action research, compete with templates for getting started and rubrics
                                       for evaluating projects.
                                    </li>
                                    
                                    <li>"<a href="http://cadres.pepperdine.edu/ccar/define.html" target="_blank"><strong>Understanding Action Research</strong></a>" from the Center for Collaborative Action Research provides a detailed definition
                                       of action research principles and methods, including specific examples of good action
                                       research questions.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/AssessingSOTL.pdf" target="_blank"><strong>Assessing the Scholarship of Teaching: Valid Decisions from Valid Evidence</strong></a> by Michael Theall and John A. Centra exemplifies how so many of the faculty competencies
                                       overlap; this article could easily be part of the selected resources of Outcome-based
                                       Learning and Assessment as a Tool for Learning.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/ActionResearchProjectHolzer.pdf" target="_blank"><strong>Example of an Action Research Project</strong></a> by Professor Mayra Holzer, Speech West Campus.
                                    </li>
                                    
                                    <li><a href="/faculty/development/coursesresources/documents/Brookfield_summary.pdf" target="_blank"><strong>Brookfield's Four Lenses: Becoming a Critically Reflective Teacher</strong></a> for the University of Sydney Center for Teaching Excellence. This provides a snapshot
                                       into Brookfield's view on reflective teaching and strategies for developing a reflective
                                       practice.
                                    </li>
                                    
                                    <li><a href="/faculty/development/programs/learningPartners/index.html" target="_blank"><strong>Peer Observation of Teaching at Valencia College</strong></a>. This gives an explanation of peer observation of teaching, as well as how to engage
                                       in Valencia's Learning Partners peer observation program. Scholars, Margery B. Ginsberg,
                                       Ph.D., co-author Diversity &amp; Motivation and Ian Solomonides, Ph.D., Macquarie University,
                                       Sydney, Australia contributed to the design of Valencia's program.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Professional Organizations &amp; Conferences</p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.aacc.nche.edu/Pages/default.aspx" target="_blank"><strong>American Association of Community Colleges</strong></a></li>
                                    
                                    <li><span><a href="http://associationdatabase.com/aws/NCDA/pt/sp/home_page" target="_blank">National Career Development Association</a></span></li>
                                    
                                    <li><strong><a href="http://www.ashe.ws/" target="_blank">Association of the Study of Higher Education (ASHE)</a></strong></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div><a title="Faculty Development Catalog" href="/faculty/development/documents/faculty-development-catalog-2017-18.pdf" target="_blank"><img src="/faculty/development/coursesresources/faculty-development-catalog.jpg" alt="Faculty Development Catalog" width="245" height="210" border="0"></a></div>
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img src="/faculty/development/coursesresources/icon_facebook_24.png" alt="TeachValencia Facebook"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://twitter.com/teachvalencia" target="_blank"><img src="/faculty/development/coursesresources/icon_twitter_24.png" alt="TeachValencia Twitter"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img src="/faculty/development/coursesresources/icon_youtube_24.png" alt="TeachValencia YouTube"></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img src="/faculty/development/coursesresources/icon_circles_24.png" alt="Circles Of Innovation"></a></div>
                        
                        <p><a title="How to Register" href="/faculty/development/howToRegisterForCourses.html"><img src="/faculty/development/coursesresources/button-how-to-register.png" alt="How to Registers" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Certification Planning Documents" href="/faculty/development/CertificationPlanningDocuments.html"><img src="/faculty/development/coursesresources/button-certification-docs.png" alt="Certification Planning Documents" width="245" height="60" border="0"></a></p>
                        
                        <p><a title="Have Questions? Contact a CTLI Member" href="/faculty/development/centers/locations.html"><img src="/faculty/development/coursesresources/button-contact-ctli.png" alt="Have Questions? Contact a CTLI Member" width="245" height="60" border="0"></a></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/coursesresources/index.pcf">©</a>
      </div>
   </body>
</html>