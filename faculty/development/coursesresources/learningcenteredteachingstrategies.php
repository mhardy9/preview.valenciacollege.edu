<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources for Essential Competencies | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/coursesresources/learningcenteredteachingstrategies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Resources for Essential Competencies</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/coursesresources/">Coursesresources</a></li>
               <li>Resources for Essential Competencies</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Learning-centered Teaching Strategies </h2>
                        
                        <p><strong>Valencia educators will implement diverse teaching and learning strategies that accommodate
                              the learning styles of students and that promote both acquisition and applications
                              of knowledge and understanding.&nbsp;&nbsp; </strong></p>
                        
                        
                        
                        <h3>Resources and Courses</h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Resources </strong>for Learning-centered Teaching Strategies include professional literature, practical
                              examples, and applications in diverse disciplines.&nbsp;
                           </li>
                           
                           <li> Faculty development<strong> Courses</strong> are designed to expand your expertise in Learning-centered Teaching Strategies.&nbsp;
                              All courses related to Learning-centered Teaching Strategies begin with the prefix,
                              LCTS.&nbsp; 
                           </li>
                           
                           <li>To search for the following LCTS course offerings click <u><a href="http://www.valenciacollege.edu/faculty/development/courseSearch.cfm" target="_blank">here</a></u>. Please note:&nbsp; Not all courses are offered every year.
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        <div>
                           
                           <div><a href="LearningcenteredTeachingStrategies.html#">Overview of Learning-centered Teaching Strategies</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="http://www.kaltura.com/tiny/vp2r7" target="_blank" title="Overview of Learning-centered Teaching Strategies - Narrated Presentation">Overview of Learning-centered Teaching Strategies - Narrated Presentation</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>LCTS 2111 Cooperative Learning in the College Classroom<br>
                                    In this seminar, participants will examine the difference between group work and cooperative/collaborative
                                    learning strategies that promote active learning. Through an interactive approach
                                    that models-the-model of collaborative learning, participants will experience first-hand
                                    the power of the well-structured cooperative strategy. Participants will leave this
                                    seminar with strategies they can use in their next class. NOTE: This course is par
                                    of the TLA Core Seminar series and is open to all faculty.
                                 </p>
                                 
                                 <p>                            LCTS 1110 Teaching in Our Learning College<br>
                                    This hybrid course introduces the Essential Competencies of a Valencia Educator and
                                    provides faculty with tools and resources to become more effective, learning-centered
                                    instructors. NOTE: This course is required for the Associate Faculty Certification.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LearningcenteredTeachingStrategies.html#">Active Learning</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/LCTS-ActiveLearning.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>LCTS 1117 Reading and Writing: Strategies for YOUR Course <br>
                                    This online course focuses on practical strategies and techniques to enhance students’
                                    reading and writing skills in any course. Participants will develop and revise a plan
                                    for implementation of these strategies. NOTE: This course is an elective course in
                                    the LifeMap Certification program.
                                 </p>
                                 
                                 <p>LCTS 2211 
                                    Hands-On Math <br>
                                    Math manipulatives can promote student engagement and comprehension of mathematical
                                    concepts. This course demonstrates how to employ hands-on math activities in classroom
                                    instruction. The metacognitive processes of hands-on learning will also be explored
                                    and discussed.
                                 </p>
                                 
                                 <p>                            LCTS 2212 Engaging Lectures<br>
                                    This workshop will explore effective lecture techniques including the use of media
                                    and interactive learning strategies in planning, organizing, and assessing engaging
                                    lectures. Participants will discuss and reflect on communication delivery style and
                                    will leave with practical application for engaging student learning through the lecture
                                    format.
                                 </p>
                                 
                                 <p>LCTS 2214 Problem-based Learning<br>
                                    This course explores the learning of subject matter and skill acquisition through
                                    collaborative problem-solving. Emphasis is placed on using this method in community
                                    college courses.
                                 </p>
                                 
                                 <p>                            LCTS 2217 Project-based Learning<br>
                                    In project-based learning, students answer a complex, open-ended question or solve
                                    an authentic problem through a collaborative process of investigation. Unlike traditional
                                    instruction where students are presented with knowledge and given an opportunity to
                                    apply that knowledge, project-based learning starts with the end in mind – a product
                                    that requires the learning of essential content and skills. In this workshop, faculty
                                    will be introduced to the seven characteristics of project-based learning, as well
                                    as techniques for managing a project-based learning experience in one’s classroom.
                                    Workshop participants will draft a driving-question and will begin developing ideas
                                    for project-based learning experiences in their disciplines.
                                 </p>
                                 
                                 <p>LCTS 2221 Impacting Student Motivation<br>
                                    In this course, participants will discuss and apply strategies for helping students
                                    to understand motivation and how it influences their thoughts and behaviors. Participants
                                    will read Daniel Pink’s Drive as background for the discussions. In registering for
                                    this course, you are committing to attend two meetings, read the book, and develop
                                    one activity/lesson on motivation. NOTE: This is an elective course in the LifeMap
                                    Certification program and Seneff Certification program.<br>
                                    
                                 </p>
                                 
                                 <p>LCTS 2222 Case-based Teaching<br>
                                    Case studies can provide a rich basis for developing problem-solving and decision
                                    making skills. These critical thinking skills are necessary to meet higher level learning
                                    outcomes in the health sciences. This class will explore the most appropriate methods
                                    of integrating cased-based teaching strategies into your course.
                                 </p>
                                 
                                 <p>                            LCTS 3125 Engaging the Online Learner<br>
                                    Engagement is the critical element for student success and retention in online classes.
                                    In this course, participants will explore virtual student engagement and develop a
                                    variety of resources and activities to connect with the online learner. Note: This
                                    course is optional for Digital Professor Certification.
                                 </p>
                                 
                                 <p>                            LCTS 3213 Service Learning Across the Curriculum<br>
                                    Participants will create a plan to infuse service learning into a current course by
                                    linking course outcomes to meaningful service in the community. Participants will
                                    explore service learning opportunities and create assessment plans to measure student
                                    learning. Successful completion of this course will enable participants to add a service
                                    learning designation to their courses.
                                 </p>
                                 
                                 <p>LCTS 3242 Developing Interactive Web-based Courses<br>
                                    This orientation to online pedagogy includes an analysis of the best practices of
                                    online education from the perspectives of students, faculty and institutions as it
                                    relates to adult learning theory, (continued) development of online learning communities,
                                    technology usage, expectations, assessment, evaluation, communication and diversity.
                                    The participant will establish an understanding of best practices and establish practical
                                    applications for use in their online classrooms. NOTE: This course is required for
                                    Digital Professor Certification.
                                 </p>
                                 
                                 <p>                            LCTS 6313 City as Text: A Model for Active Learning<br>
                                    City as Text™ is the National Collegiate Honors Council signature program that provides
                                    the opportunity for participants to “read” an urban landscape and find meaning through
                                    walkabouts and active exploration of the environment. Basic tenants of active learning
                                    will also be covered. NOTE: This is an optional course for the Seneff Faculty Development
                                    program.                          
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LearningcenteredTeachingStrategies.html#">Collaborative and Cooperative Learning</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/LCTS-CollaborativeandCooperativeLearning.pdf" target="_blank"> Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>LCTS 1112 Succeeding with Online Group Work <br>
                                    This online course focuses on designing and facilitating group projects in the online
                                    environment. The focus is using student groups in online classes, but the insights
                                    will translate to the face-to-face classroom as well. NOTE: This course is optional
                                    for Digital Professor Certification.
                                 </p>
                                 
                                 <p>                            LCTS 2111 Learning-centered Teaching Strategies<br>
                                    In this seminar, participants will examine the difference between group work and cooperative/collaborative
                                    learning strategies that promote active learning. Through an interactive approach
                                    that models-the-model of collaborative learning, participants will experience first-hand
                                    the power of the well-structured cooperative strategy. Participants will leave this
                                    seminar with strategies they can use in their next class. NOTE: This course is par
                                    of the TLA Core Seminar series and is open to all faculty. 
                                    
                                 </p>
                                 
                                 <p>LCTS 2211 Hands-On Math <br>
                                    Math manipulatives can promote student engagement and comprehension of mathematical
                                    concepts. This course demonstrates how to employ hands-on math activities in classroom
                                    instruction. The metacognitive processes of hands-on learning will also be explored
                                    and discussed.
                                 </p>
                                 
                                 <p>                            LCTS 2220 Using Collaborative Writing Strategies Across
                                    the Disciplines<br>
                                    In this course, participants in all disciplines examine strategies to teach students
                                    how to write more effectively for their specific course using collaborative writing
                                    as a teaching method. Participants will be given resources and the opportunity to
                                    create a lesson or activity incorporating collaborative writing into their discipline.
                                 </p>
                                 
                                 <p>                            LCTS 3216 Supplemental Learning to Enhance Student Learning
                                    <br>
                                    Faculty will learn how Supplemental Learning is used at Valencia to enhance student
                                    learning through SL strategies such as facilitated learning, the integration of college
                                    success skills with course material, and group activities. This interactive course
                                    focuses on the essentials of Supplemental Learning, as well as strategies for integrating
                                    SL into your course and creating a dynamic partnership with the SL Leader to facilitate
                                    learning. Participation in this course does not guarantee a faculty member will be
                                    able to teach a course enhanced by supplemental learning. Supplemental Learning is
                                    currently focused on pre-, beginning, intermediate and college algebra; college prep
                                    English 1 and 2 and freshman comp 1 and 2, and US government. NOTE: This course is
                                    an elective course in the LifeMap Certification program                          
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LearningcenteredTeachingStrategies.html#">Critical Thinking</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/LCTS-CriticalThinking.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>ASMT 2229 Assess Higher Order Thinking Through Multiple Choice Questions<br>
                                    In this course, faculty members will enhance their ability to develop, test, and revise
                                    multiple choice questions drawing on hands-on activities using examples from Valencia
                                    College faculty. Issues of validity including reliability will be included in the
                                    conversation.
                                 </p>
                                 
                                 <p>                          INDV 2254 The Art and Science of Learning and the Brain
                                    <br>
                                    In this course, participants will examine the relationship between the ways people
                                    learn and the biology of the brain.                        
                                 </p>
                                 
                                 <p>                          LCTS 1117 Reading and Writing: Strategies for YOUR Course<br>
                                    This online course focuses on practical strategies and techniques to enhance students’
                                    reading and writing skills in any course. Participants will develop and revise a plan
                                    for implementation of these strategies. NOTE: This course is an elective course in
                                    the LifeMap Certification program.
                                 </p>
                                 
                                 <p>LCTS 2214 Problem-based Learning <br>
                                    This course explores the learning of subject matter and skill acquisition through
                                    collaborative problem-solving. Emphasis is placed on using this method in community
                                    college courses.
                                 </p>
                                 
                                 <p>                            LCTS 2216 Reading Across the Disciplines <br>
                                    In this course, participants will investigate practical strategies for strengthening
                                    students’ reading comprehension in any discipline.
                                 </p>
                                 
                                 <p>                            LCTS 2217 Project-based Learning<br>
                                    In project-based learning, students answer a complex, open-ended question or solve
                                    an authentic problem through a collaborative process of investigation. Unlike traditional
                                    instruction where students are presented with knowledge and given an opportunity to
                                    apply that knowledge, project-based learning starts with the end in mind – a product
                                    that requires the learning of essential content and skills. In this workshop, faculty
                                    will be introduced to the seven characteristics of project-based learning, as well
                                    as techniques for managing a project-based learning experience in one’s classroom.
                                    Workshop participants will draft a driving-question and will begin developing ideas
                                    for project-based learning experiences in their disciplines.
                                 </p>
                                 
                                 <p>                            LCTS 2218 Speaking Across the Disciplines<br>
                                    This workshop explores how to integrate oral presentations into a course in any discipline.
                                    It will focus on how to develop an assignment, how to prepare students, and how to
                                    assess the presentation. Participants will practice assessing a sample student presentation,
                                    brainstorm ideas for their class, and leave with sample assignments and rubrics for
                                    reference.
                                 </p>
                                 
                                 <p>                            LCTS 6310 Best Practices in Honors Education<br>
                                    This course will review some of the established best practices and cutting-edge ideas
                                    in honors education, as well as encourage instructors to develop techniques and assignments
                                    that challenge students to engage course materials in new and inventive ways. NOTE:
                                    This is a <br>
                                    required course for the Seneff Faculty Development program.
                                 </p>
                                 
                                 <p>                          LCTS 6311 Interdiscplinary Teaching: Pedagogical Practices
                                    that Encourage Critical Thinking <br>
                                    This course will explore the benefits and challenges of designing and implementing
                                    interdisciplinary teaching techniques in the classroom. Emphasis will be placed on
                                    developing instructional activities that nurture students’ critical thinking skills
                                    by constructing alternative perspectives to course questions. In addition, participants
                                    will discuss strategies to meet challenges of interdisciplinary teaching and to maximize
                                    benefits to students. NOTE: This is an optional course for the Seneff Faculty Development
                                    program.
                                 </p>
                                 
                                 <p>                          LCTS 6312 Great Books (And Other Masterpieces in Human Thought)
                                    <br>
                                    This course will review ways to include primary source materials into lower division,
                                    general education courses. While emphasis will be placed on canonical ‘great books,’
                                    methods for incorporating other primary source materials such as historical documents
                                    and artifacts will also be discussed. Note: This is an optional course for the Seneff
                                    Faculty Development program.
                                 </p>
                                 
                                 <p>                          LCTS 6314 What is Undergraduate Research? <br>
                                    This course will define and outline expectations of undergraduate research at Valencia.
                                    In addition, the course will address the increasing need for two-year students to
                                    be research-ready upon transfer to four-year schools. NOTE: This is an optional course
                                    for the Seneff Faculty Development Program. This course is required for faculty mentoring
                                    students in the Honors Undergraduate Research Track who do not meet alternative credentialing.
                                 </p>
                                 
                                 <p>                          LOBP 2230 Core Competencies: Think, Value, Communicate,
                                    Act (TVCA)<br>
                                    This seminar examines strategies that facilitate student growth in thinking critically;
                                    communicating effectively; articulating and applying personal values and those of
                                    the various disciplines and appreciating the values of others; and applying learning
                                    and understanding effectively and responsibly.
                                 </p>
                                 
                                 <p>                          LOBP 3230 Thinking Things Through: Critical Thinking Theory
                                    and Practice<br>
                                    Over 2 two-hour meetings, participants will discuss and apply the concepts of Paul
                                    and Elder’s critical thinking model outlined in Gerald Nosich’s book<em> Learning to Think Things Through: A Guide to Critical Thinking Across the Disciplines</em>. The text offers both a practical model that can be applied to any discipline as
                                    well as exercises and activities for teaching students how to think more critically.
                                    Each participant will receive a copy of the book after registering. In registering
                                    for this course, you are committing to attend two meetings, read the book, and integrate
                                    the model into one activity/lesson in your class. NOTE: This course is an elective
                                    course in the LifeMap Certification program and Seneff Certification program.
                                 </p>
                                 
                                 <p>                          LOBP 3231 Critical Thinking: Intellectual Standards<br>
                                    Faculty will deepen their understanding of the Paul and Elder critical thinking model
                                    by examining the personal qualities of a strong critical thinker and learning the
                                    standards by which to analyze a position. Participants will identify strategies to
                                    incorporate these skills into class activities, course assignments, and assessment
                                    methods. 
                                    NOTE: This course is recommended for those who have completed LOBP 3230.         
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="LearningcenteredTeachingStrategies.html#">Motivation and Engagement</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/LCTS-Motivation.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>Courses:<br>
                                    LCTS 2212 Engaging Lectures<br>
                                    This workshop will explore effective lecture techniques including the use of media
                                    and interactive learning strategies in planning, organizing, and assessing engaging
                                    lectures. Participants will discuss and reflect on communication delivery style and
                                    will leave with practical application for engaging student learning through the lecture
                                    format.
                                 </p>
                                 
                                 <p>                              LCTS 2221 Impacting Student Motivation<br>
                                    Over  two-hour meetings, participants will discuss and apply strategies for helping
                                    students to understand motivation and how it influences their thoughts and behaviors.
                                    Participants will read Daniel Pink’s Drive as background for the discussions. In registering
                                    for this course, you are committing to attend 2 meetings, read the book, and develop
                                    one activity/lesson on motivation. NOTE: This is an elective course for the LifeMap
                                    Certification program.                        
                                 </p>
                                 
                                 <p>LCTS 3125 Engaging the Online Learner<br>
                                    Engagement is the critical element for student success and retention in online classes.
                                    In this course, participants will explore virtual student engagement and develop a
                                    variety of resources and activities to connect with the online learner. NOTE: This
                                    course is optional for Digital Professor Certification.
                                 </p>
                                 
                                 <p>                              LCTS 3242 Developing Interactive Web-based Courses<br>
                                    This orientation to online pedagogy includes an analysis of the best practices of
                                    online education from the perspectives of students, faculty and institutions as it
                                    relates to adult learning theory, (continued) development of online learning communities,
                                    technology usage, expectations, assessment, evaluation, communication and diversity.
                                    The participant will establish an understanding of best practices and establish practical
                                    applications for use in their online classrooms. NOTE: This course is required for
                                    Digital Professor Certification.
                                 </p>
                                 
                                 <p>                              LCTS 3280 Teaching and Learning with Social Media Part
                                    I<br>
                                    Participants will explore teaching and learning strategies utilizing social media
                                    to enhance student learning. This course will showcase various applications and how
                                    they can be used to enhance communication, collaboration, and student engagement.
                                    Social and legal issues relating to the use of social media in higher education will
                                    also be explored. Participants will develop a plan to appropriately integrate social
                                    media into a specific course. NOTE: This course is optional for the Digital Professor
                                    Certification and is part one of a two-part series on social media. Part II LTAD 3383,
                                    provides an in-depth overview of various social media tools.                     
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="LearningcenteredTeachingStrategies.html#">Other Topics </a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><strong><a href="LearningcenteredTeachingStrategies.html#">Learning-</a>centered<a href="LearningcenteredTeachingStrategies.html#"> Teaching Strategies for Online Learning </a></strong></p>
                                 
                                 <p>LCTS 3137 Facilitating Online Learning<br>
                                    This online course is designed for instructors who are currently teaching online or
                                    are planning to in the near future. This course will also be of great value to those
                                    involved in designing and developing online courses. It focuses on understanding the
                                    characteristics and needs of the online student and developing strategies to facilitate
                                    effective online discussions, assessment and ultimately the development of an online
                                    learning community. NOTE: This course is optional for Digital Professor Certification.
                                 </p>
                                 
                                 <p>                              LCTS 3211 Strategies for Academic Integrity<br>
                                    This course will explore plagiarism and academic integrity. Participants will develop
                                    a comprehensive plan to avoid and detect plagiarism and encourage students to engage
                                    in behaviors supportive of academic integrity. NOTE: This course is optional for Digital
                                    Professor Certification.
                                 </p>
                                 
                                 <p>                              LCTS 3212 Boot Camp for Online Instruction<br>
                                    This intensive course addresses the pedagogy and technology essentials for teaching
                                    online using Valencia’s learning management system (LMS). Participants will develop
                                    a framework of online best practice strategies for engaging, instructing, communicating
                                    and assessing students in the online environment.                            
                                 </p>
                                 
                                 <p>LCTS 3242 Developing Interactive Web-based Courses<br>
                                    This orientation to online pedagogy includes an analysis of the best practices of
                                    online education from the perspectives of students, faculty and institutions as it
                                    relates to adult learning theory, (continued) development of online learning communities,
                                    technology usage, expectations, assessment, evaluation, communication and diversity.
                                    The participant will establish an understanding of best practices and establish practical
                                    applications for use in their online classrooms. NOTE: This course is required for
                                    Digital Professor Certification.
                                 </p>
                                 
                                 <p>                            LCTS 3247 Maximizing Hybrid Learning <br>
                                    Participants will examine best practices and models for developing a hybrid course.
                                    Particular attention will be paid to appropriately leveraging the face-to-face and
                                    online components of hybrid course design. Participants will develop a plan for the
                                    delivery of their course. NOTE: This course is optional for the Digital Professor
                                    Certification.                        
                                 </p>
                                 
                                 <p>LCTS 3280 Teaching and Learning with Social Media Part I<br>
                                    Participants will explore teaching and learning strategies utilizing social media
                                    to enhance student learning. This course will showcase various applications and how
                                    they can be used to enhance communication, collaboration, and student engagement.
                                    Social and legal issues relating to the use of social media in higher education will
                                    also be explored. Participants will develop a plan to appropriately integrate social
                                    media into a specific course. NOTE: This course is optional for the Digital Professor
                                    Certification and is part one of a two-part series on social media. Part II, LTAD
                                    3383, provides an in-depth overview of various social media tools.               
                                    
                                 </p>
                                 
                                 <p><span><strong>Learning-centered Teaching Strategies for Honors </strong></span></p>
                                 
                                 <p>LCTS 6310 Best Practices in Honors Education<br>
                                    This course will review some of the established best practices and cutting-edge ideas
                                    in honors education, as well as encourage instructors to develop techniques and assignments
                                    that challenge students to engage course materials in new and inventive ways. NOTE:
                                    This is a <br>
                                    required course for the Seneff Faculty Development program.
                                 </p>
                                 
                                 <p>                            LCTS 6311 Interdiscplinary Teaching: Pedagogical Practices
                                    that Encourage Critical Thinking<br>
                                    This course will explore the benefits and challenges of designing and implementing
                                    interdisciplinary teaching techniques in the classroom. Emphasis will be placed on
                                    developing instructional activities that nurture students’ critical thinking skills
                                    by constructing alternative perspectives to course questions. In addition, participants
                                    will discuss strategies to meet challenges of interdisciplinary teaching and to maximize
                                    benefits to students. NOTE: This is an elective  course for the Seneff Faculty Development
                                    program. 
                                 </p>
                                 
                                 <p>                            LCTS 6312 Great Books (And Other Masterpieces in Human
                                    Thought)<br>
                                    This course will review ways to include primary source materials into lower division,
                                    general education courses. While emphasis will be placed on canonical ‘great books,’
                                    methods for incorporating other primary source materials such as historical documents
                                    and artifacts will also be discussed. NOTE: This is an elective course for the Seneff
                                    Faculty Development program.
                                 </p>
                                 
                                 <p>                            LCTS 6313 City as Text: A Model for Active Learning <br>
                                    City as Text™ is the National Collegiate Honors Council signature program that provides
                                    the opportunity for participants to “read” an urban landscape and find meaning through
                                    walkabouts and active exploration of the environment. Basic tenants of active learning
                                    will also be covered. NOTE: This is an elective course for the Seneff Faculty Development
                                    program.
                                 </p>
                                 
                                 <p>                            LCTS 6314 What is Undergraduate Research? <br>
                                    This course will define and outline expectations of undergraduate research at Valencia.
                                    In addition, the course will address the increasing need for two-year students to
                                    be research-ready upon transfer to four-year schools. NOTE: This is an optional course
                                    for the Seneff Faculty Development program. This course is required for faculty mentoring
                                    students in the Honors Undergraduate Research Track who do not meet alternative credentialing.<br>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/coursesresources/learningcenteredteachingstrategies.pcf">©</a>
      </div>
   </body>
</html>