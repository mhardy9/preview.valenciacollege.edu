<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources for Essential Competencies | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/faculty/development/coursesresources/inclusionanddiversity.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/faculty/development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Resources for Essential Competencies</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/faculty/">Faculty</a></li>
               <li><a href="/faculty/development/">Development</a></li>
               <li><a href="/faculty/development/coursesresources/">Coursesresources</a></li>
               <li>Resources for Essential Competencies</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        <h2>Inclusion &amp; Diversity </h2>
                        
                        <p><strong> Valencia educators will design learning opportunities that acknowledge, draw upon
                              and are enriched by student diversity.&nbsp; An atmosphere of inclusion and understanding
                              will be promoted in all learning environments. </strong></p>
                        
                        <h3>Resources and Courses </h3>
                        
                        <ul>
                           
                           <li>
                              <span><strong>Resources </strong></span>for Inclusion and Diversity include professional literature, practical examples, and
                              applications in diverse disciplines.&nbsp;
                           </li>
                           
                           <li> Faculty development<strong> Courses</strong> are designed to expand your expertise in Inclusion and Diversity.&nbsp; All courses related
                              to Inclusion and Diversity begin with the prefix, INDV.&nbsp; 
                           </li>
                           
                           <li>To search for the following INDV course offerings click <u><a href="http://www.valenciacollege.edu/faculty/development/courseSearch.cfm" target="_blank">here</a></u>. Please note:&nbsp; Not all courses are offered every year.
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div><a href="InclusionandDiversity.html#">Overview of Inclusion and Diversity</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="http://www.kaltura.com/tiny/snll1" target="_blank" title="Overview of Inclusion and Diversity- Narrated Presentation">Overview of Inclusion and Diversity- Narrated Presentation</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>INDV 2151 Inclusion and Diversity<br>
                                    In this seminar, participants will investigate learning opportunities that acknowledge,
                                    draw upon, and are enriched by student diversity and create atmospheres of inclusion
                                    and understanding. Participants will reflect on power differentials in the classroom,
                                    ways to increase inclusion and minimize exclusion, and some theoretical underpinnings
                                    of Inclusion and Diversity. NOTE: This course is part of the TLA Core 
                                    Seminar series and is open to all faculty.                        
                                 </p>
                                 
                                 <p>LCTS 1110 Teaching in Our Learning College<br>
                                    This scenarios-based course introduces the Essential Competencies of a Valencia Educator
                                    and provides faculty with tools and resources to become more effective, learning-centered
                                    instructors. Note: This course is required for the Associate Faculty Certification
                                    Program.                          
                                 </p>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="InclusionandDiversity.html#">Inclusion/Diversity</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Inc-Div-Diversity.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>INDV 1152 Global Community College<br>
                                    Virtually all faculty members find themselves working with students from many cultures.
                                    This online scenarios-based course focuses on increasing our awareness of student
                                    cultural and linguistic differences. Participants will acquire strategies for engaging
                                    our diverse student population and enabling all students to reach their academic goals.
                                 </p>
                                 
                                 <p>                              INDV 3352 Classroom Management<br>
                                    Through a case-based approach, participants will examine effective community college
                                    classroom management processes and strategies that establish positive student achievement
                                    and behavior. Participants will discuss best practices and apply what they have learned
                                    to their classroom settings.
                                 </p>
                                 
                                 <p>                              INDV 3358 The Art of Facilitating Online Discussions<br>
                                    This course is designed to enhance participants’ ability to effectively facilitate
                                    discussions in the online environment. Participants will examine the facilitator’s
                                    role in online discussions that build community and enhance student learning. Participants
                                    will enhance their facilitation skills through activities with their peers that simulate
                                    facilitator-student interaction. Note: This course is optional for Digital Professor
                                    Certification.
                                 </p>
                                 
                                 <p>                            INDV 3351 Fostering Cultural Responsibility<br>
                                    This course is designed to provide faculty members with the knowledge and resources
                                    to internationalize their courses through the development of a course internationalization
                                    toolkit, which will be shared with colleagues college-wide. Using an add-on, infusion,
                                    or transformational approach to internationalization, participants will work in small
                                    groups to develop the student learning outcomes, course content, materials, activities
                                    and assignments, and assessments for a specific course. Faculty are strongly encouraged
                                    to attend the course with at least one other person from their discipline in order
                                    to develop the toolkit together. Note: This course is an elective course in the Seneff
                                    Certification program.                          
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="InclusionandDiversity.html#">Learning Communities</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>INDV 3248 Building Online Learning Communities<br>
                                    This course will engage participants in the concepts, effective strategies and best
                                    practices in creating and maintaining an online learning community. Note: This course
                                    is required for Digital Professor Certification.
                                 </p>
                                 
                                 <p>                              INDV 3259 LinC: Integrating a High-Impact Practice<br>
                                    Learning communities are regarded as a high impact educational practice that has shown
                                    to be beneficial to college students from many backgrounds. This course will review
                                    the foundation of Valencia’s LinC initiative, along with providing the necessary guidelines
                                    and strategies to build community in the classroom and implement integrated learning
                                    with two or more joined courses. The course will help you design a joint syllabus
                                    with a faculty partner from another discipline and partner with a Success Coach to
                                    support student learning. Note: This course is an elective course in the LifeMap Certification
                                    Program. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="InclusionandDiversity.html#">Learning Styles and Personality Types</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Inc-Div-LearningStylesandPersonalityTypes.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>INDV 2253 Personality and Learning Styles<br>
                                    This is an introductory course to personality &amp; learning styles. Understanding one’s
                                    own style can lead to deeper meta-cognition and an appreciation for diverse learning
                                    styles. This seminar offers information regarding free online personality, learning
                                    and thinking styles inventories. Participants are provided examples of discipline-specific
                                    classroom assignments that facilitate this understanding.
                                 </p>
                                 
                                 <p>                              INDV 2254 The Art and Science of Learning and the Brain<br>
                                    In this course, participants will examine the relationship between the ways people
                                    learn and the biology of the brain.
                                 </p>
                                 
                                 <p>                            INDV 3113 Learning Styles Online (not currently offered)<br>
                                    Participants will explore how learning styles impact students in online classrooms.
                                    Participants will also examine other aspects of diversity, such as gender, culture,
                                    lifestyle, and geography as factors that contribute to the unique nature of the online
                                    classroom.                            <br>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <div><a href="InclusionandDiversity.html#">Peace and Justice Institute</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>INDV 7310 Working with Conflict<br>
                                    In this course, faculty will learn about the components, roles, and needs active in
                                    conflict, identify their own conflict style, and be introduced to conflict resolution
                                    practices for the purpose of more productive &amp; positive outcomes. Participants will
                                    engage in various exercises, including a self-assessments &amp; mock negotiation for the
                                    purpose of integrating these skills into a working knowledge.
                                 </p>
                                 
                                 <p>                            INDV 7311 Creating a Safe Space for Dialogue<br>
                                    This course will demonstrate techniques for establishing a respectful and inclusive
                                    environment that promotes healthy classroom dialogue. Dialogue can assist students
                                    in moving information from a memorized or theoretical understanding, to a more integrated,
                                    tangible and lasting knowledge. Participants will engage in best practices designed
                                    to promote discussion as a pedagogical tool. In registering for this course, you are
                                    committing to attend 2 meetings across two terms (one 4-hour and one 2- hour), complete
                                    the required reading, and integrate a conversation activity into your class.
                                 </p>
                                 
                                 <p>                            INDV 7312  Cultivating the Contemplative Mind in Education
                                    Through Research, Practice and Personal Insight<br>
                                    Contemplative pedagogy involves a wide range of teaching methods designed to cultivate
                                    a capacity for deeper awareness, concentration, and insight that create demonstrable
                                    neurobiological changes. Participants will be introduced to the nature, history, and
                                    status of contemplative practices used in mainstream education as a complementary
                                    pedagogic tool that fosters depth in learning. Participants will have the opportunity
                                    to experiment with and create a contemplative-based classroom practice and reflect
                                    upon how contemplation may innovatively meet the needs of today’s students.      
                                    
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="InclusionandDiversity.html#">Universal Design for Learning</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/Inc-Div-UniversalDesignforLearning.pdf" target="_blank">Resources</a></p>
                                 
                                 <p><strong>Courses</strong></p>
                                 
                                 <p>INDV 1153 Making Learning Accessible<br>
                                    This online scenario-based course is designed for faculty and staff who wish to develop
                                    an enhanced awareness of learner diversity including those with disabilities. This
                                    course will assist faculty in identifying and utilizing strategies in order to create
                                    an optimal learning environment to teach all learners.
                                 </p>
                                 
                                 <p>                              INDV 3246 Universal Design for Online Learning<br>
                                    Universal design for learning is “the design of products and environments useable
                                    by all people, to the greatest extent possible, without the need for adaptation or
                                    specialized design.” Universal design benefits all users including students with unique
                                    learning needs. This course will explore the creation of digital course content that
                                    can be used by the broadest audience possible. Note: This course is required for Digital
                                    Professor Certification.
                                 </p>
                                 
                                 <p>                              INDV 3256 Universal Design for Learning<br>
                                    Participants will explore strategies for “universal design for learning” in a face-to-face
                                    classroom setting. This seminar will engage participants in effective strategies for
                                    designing an optimal learning environment that accommodates learner differences and
                                    unique learning needs.                          
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>        
                        
                        <h3>One-to-One Consultations</h3>
                        
                        
                        
                        
                        <ul>
                           
                           <li>Meet with a member of our<a href="../Consulation.html"> Faculty Development Team</a> on your campus.
                           </li>
                           
                           <li>Partner with a colleague to observe different teaching techniques and reflect on your
                              own practice. <a href="../programs/learningPartners/index.html">Click here</a> to learn more. 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="../documents/faculty-development-catalog-2017-18.pdf" target="_blank" title="Faculty Development Catalog"><img alt="Faculty Development Catalog" border="0" height="210" src="faculty-development-catalog.jpg" width="245"></a>
                           
                        </div>
                        
                        
                        <div>
                           
                           <h2>GET SOCIAL</h2>
                           <a href="https://www.facebook.com/TeachValencia-1676296052593510/?fref=nf" target="_blank"><img alt="TeachValencia Facebook" src="icon_facebook_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://twitter.com/teachvalencia" target="_blank"><img alt="TeachValencia Twitter" src="icon_twitter_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="https://www.youtube.com/channel/UCPCjjQEYULz9fI5tCEqDebw" target="_blank"><img alt="TeachValencia YouTube" src="icon_youtube_24.png"></a>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <a href="http://circlesofinnovation.valenciacollege.edu/circles/" target="_blank"><img alt="Circles Of Innovation" src="icon_circles_24.png"></a>  
                           
                        </div>
                        
                        
                        <p><a href="../howToRegisterForCourses.html" title="How to Register"><img alt="How to Registers" border="0" height="60" src="button-how-to-register.png" width="245"></a></p>
                        
                        
                        <p><a href="../CertificationPlanningDocuments.html" title="Certification Planning Documents"><img alt="Certification Planning Documents" border="0" height="60" src="button-certification-docs.png" width="245"></a></p>
                        
                        <p><a href="../centers/locations.html" title="Have Questions? Contact a CTLI Member"><img alt="Have Questions? Contact a CTLI Member" border="0" height="60" src="button-contact-ctli.png" width="245"></a></p>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/faculty/development/coursesresources/inclusionanddiversity.pcf">©</a>
      </div>
   </body>
</html>