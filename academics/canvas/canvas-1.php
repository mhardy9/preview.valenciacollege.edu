<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Canvas Resources | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/canvas/canvas-1.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/canvas/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Canvas Resources</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/canvas/">Canvas Education</a></li>
               <li>Canvas Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="col-md-4 row wrapper_indent box_style_1">
                           
                           <h3>Canvas is Coming!</h3>
                           
                           <p>For more information, please refer to the article in The Grove.</p>
                           
                        </div>
                        
                        <div class="row wrapper_indent countdown">
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-days"></span><span class="step_to_do_text">DAYS</span></div>
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-hours"></span><span class="step_to_do_text">HOURS</span></div>
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-minutes"></span><span class="step_to_do_text">MINUTES</span></div>
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-seconds"></span><span class="step_to_do_text">SECONDS</span></div>
                           
                        </div>
                        
                        <hr>
                        
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="https://online.valenciacollege.edu/login/canvas" target=""><i class="far fa-users"></i><h3>Canvas Login</h3>
                                 <p>Login into online.valenciacollege.edu using your Atlas credentials.</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="https://ptl5pd-prod.valenciacollege.edu/web/home-community/employees" target=""><i class="far fa-desktop"></i><h3>Canvas Webinars</h3>
                                 <p>Webinars are available for faculty and staff. Please log into Atlas and then open
                                    the Valencia EDGE.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="https://community.canvaslms.com/welcome" target=""><i class="far fa-cubes"></i><h3>Canvas Resources</h3>
                                 <p>A variety of Canvas resources are available, including the Canvas Guides and Canvas
                                    Community. Additonal tips, tricks, and showcase resources are listed at the bottom
                                    of this page.
                                 </p></a>
                              
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-rocket"></i><h3>Canvas Mobile Apps</h3>
                                 <p>
                                    
                                    Download Apple
                                    Download Android
                                    Download App Guide
                                    
                                    
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="http://blogs.valenciacollege.edu/canvas/home/canvas-support/" target=""><i class="far fa-mobile"></i><h3>Canvas Help</h3>
                                 <p>
                                    By Phone: 407-582-5600
                                    Click on the button below to find out the multiple ways you can access Canvas Help.
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;
                                    
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="" target=""><i class="far fa-graduation-cap"></i><h3>5 Things You Need to Know about Canvas</h3>
                                 <p>
                                    Know the Basic
                                    Starting Out
                                    Preparing for Launch
                                    A Transition Update
                                    The Clock is Ticking
                                    Get Rolling on Your Content
                                    Resources Available to Help Your Transition
                                    More 5 Things
                                    
                                 </p></a>
                              
                           </div>
                        </div>
                        
                        <hr>
                        
                        
                        <div class="container margin-30">
                           
                           <div class="container margin-60">
                              <div class="main-title">
                                 
                                 <h2>Frequently questions</h2>
                                 
                                 <p>&nbsp;</p>
                                 
                              </div>
                              
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       
                                       <h3>How do I access Canvas?</h3>
                                       
                                       <p>At Valencia College, you can access Canvas directly by visiting <a href="https://online.valenciacollege.edu" target="_blank" rel="noopener">online.valenciacollege.edu</a>.
                                       </p>
                                       
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       
                                       <h3>When will classes be offered in Canvas?</h3>
                                       
                                       <p>All classes will be offered in Canvas starting in May 2018. Some faculty are currently
                                          piloting Canvas with students and sharing lessons learned with the college community
                                          in preparation for full launch.
                                       </p>
                                       
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       
                                       <h3>What development options are available for Canvas?</h3>
                                       
                                       <p>There are a variety of development opportunities to learn more about Canvas. You can
                                          register for Canvas webinars through the <a href="http://thegrove.valenciacollege.edu/have-you-explored-canvas/" target="_blank" rel="noopener">Valencia EDGE</a>, participate in a <a href="http://circlesofinnovation.valenciacollege.edu/" target="_blank" rel="noopener">Circles of Innovation</a> session, visit your campus <a href="http://valenciacollege.edu/faculty/development/centers/locations.cfm" target="_blank" rel="noopener">Center for Teaching/Learning Innovation</a>, and browse the 24/7 available resources through <a href="https://community.canvaslms.com/" target="_blank" rel="noopener">Canvas Community</a> and <a href="https://community.canvaslms.com/community/answers/commons" target="_blank" rel="noopener">Canvas Commons</a>.
                                       </p>
                                       
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       
                                       <h3>How do I get help with Canvas?</h3>
                                       
                                       <p>You can chat live 24/7 with Canvas support by using the “Help” link on the bottom
                                          left inside Canvas. You can also call Valencia’s Canvas Support 24/7 at <a href="tel:407-582-5600">(407) 582-600</a> or email at <a href="mailto:onlinehelp@valenciacollege.com">onlinehelp@valenciacollege.edu</a> during regular business hours.
                                       </p>
                                       
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       
                                       <h3>How do I create a sandbox course space?</h3>
                                       
                                       <p>As part of the initial transition to Canvas, all faculty receive 5 sandbox course
                                          spaces and all staff recieve 1 sandbox course space. Contact <a href="mailto:onlinehelp@valenciacollege.com">onlinehelp@valenciacollege.edu</a> to request additional sandboxes. Specify the course name, your username, and that
                                          the sandbox is for Canvas.
                                       </p>
                                       
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       
                                       <h3>What strategies have Canvas pilot faculty learned to prepare courses for migration?</h3>
                                       
                                       <p>Valencia is currently piloting Canvas in the fall and spring semesters to prepare
                                          for college-wide launch in May 2018. During the pilot, faculty members learned the
                                          best strategies for migration include planning and thoughtful review of their current
                                          Blackboard courses. Several pilot faculty participated in a panel discussion hosted
                                          by Circles of Innovation to discuss the lessons learned during migration. The webinar,
                                          <a href="https://www.youtube.com/watch?v=BZLRl-LVGdw&amp;feature=youtu.be&amp;t=3m22s" target="_blank" rel="noopener"> Lessons Learned in Designing Courses in Canvas: A Conversation</a> is available on the Circles of Innovation website.
                                       </p>
                                       
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr>
                        
                        <div class="container margin-30">
                           
                           <div class="main-title">
                              
                              <h3>Learn More</h3>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-4 add_bottom_30 clearfix">
                                 
                                 <h4>Getting Started</h4>
                                 
                                 <ul>
                                    
                                    <li><a href="https://drive.google.com/file/d/1MWqTEc8mEsxq9BK3vHxpQknjOKlGHIv_/view?usp=sharing" target="_blank" rel="noopener">Canvas Transition Planning Document</a></li>
                                    
                                    <li><a href="https://community.canvaslms.com/docs/DOC-3891">Canvas Video Guide</a></li>
                                    
                                    <li><a href="https://community.canvaslms.com/docs/DOC-10460">Canvas Instructor Guide</a></li>
                                    
                                    <li><a href="https://drive.google.com/open?id=0B6eyM_Y5GMnkd3k1SGZKOHM5d00">Draft of Rubric for Online Competencies</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-4">
                                 
                                 <h4>Tips &amp; Tricks</h4>
                                 
                                 <ul>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/category/canvas/">Circles of Innovation – Canvas Information</a></li>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/2017/11/13/sample-canvas-courses/">Quick Start with Valencia Sample Courses</a></li>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/2017/10/25/adding-a-gif-to-canvas-course-cards/">Add GIF to Canvas Course Card</a></li>
                                    
                                    <li><a href="https://online.valenciacollege.edu/courses/20638">Canvas Migration Resources Course</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-4">
                                 
                                 <h4>Showcase</h4>
                                 
                                 <ul>
                                    
                                    <li><a href="https://online.valenciacollege.edu/courses/18902">Valencia Sample Course – Weekly Modules</a></li>
                                    
                                    <li><a href="https://online.valenciacollege.edu/courses/3293">Valencia Sample Course – Unit Modules</a></li>
                                    
                                    <li><a href="https://youtu.be/TdDS6gVdI10">Cool Things You Can Do With Canvas</a></li>
                                    
                                    <li><a href="http://circlesofinnovation.valenciacollege.edu/2017/10/21/lessons-learned-in-designing-courses-in-canvas-a-conversation/">Lessons Learned in Designing Courses in Canvas</a></li>
                                    
                                 </ul>
                                 	
                              </div>
                              	
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/canvas/canvas-1.pcf">©</a>
      </div>
   </body>
</html>