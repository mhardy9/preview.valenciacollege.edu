<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, canvas, frequently, asked, questions, faq">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/canvas/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/canvas/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/canvas/">Canvas Education</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="main-title">
                  
                  <h2>Frequently Asked Questions</h2>
                  
               </div>
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>When Can Faculty Access Canvas</h4>
                        
                        <p>Faculty can start using Canvas on June 30. Login using your regular credentials. The
                           new login for Canvas
                           will be at online.valenciacollege.edu.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>What is the status of the publisher/vendor integrations within Canvas?</h4>
                        
                        <p>To view a list of requested integrations with Canvas as well as their integration
                           status, go <a href="/documents/academics/canvas-education/canvas-integrations.pdf" target="_blank">here</a>.
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Te pri facete latine salutandi?</h4>
                        
                        <p>Content goes here.</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Et ius tota recusabo democritum?</h4>
                        
                        <p>Content goes here.</p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Mediocritatem sea ex, nec id agam?</h4>
                        
                        <p>Content goes here.</p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Te pri facete latine salutandi?</h4>
                        
                        <p>Content goes here.</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/canvas/faq.pcf">©</a>
      </div>
   </body>
</html>