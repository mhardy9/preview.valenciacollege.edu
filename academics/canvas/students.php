<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Canvas Resources | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.min.css">
      <link rel="stylesheet" href="/_resources/ldp/forms/css/ou-forms.bootstrap.validation.min.css"><script>
					var page_url="https://preview.valenciacollege.edu/academics/canvas/students.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/canvas/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-landing_1">
         <div id="intro-txt">
            <h1>Canvas Resources</h1>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/canvas/">Canvas Education</a></li>
               <li>Students</li>
            </ul>
         </div>
      </div>
      <main role="main">
         <div class="container_gray_bg">
            <div class="container margin-60">
               <div class="main-title">
                  <td class="main-title">
                     
                     <h2>Canvas for Students</h2>
                     
                     <p>Resources for Canvas... </p>
                     
                  </td>
               </div>
               
               <div class="row">
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="https://online.valenciacollege.edu/login/canvas" target="_blank"><i class="far fa-users"></i><h3>Canvas Login</h3>
                        <p>Login to&nbsp;online.valenciacollege.edu&nbsp;using your regular credentials. Access is available
                           to all current faculty and staff at the college.
                        </p></a>
                     
                  </div>
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="https://ptl5pd-prod.valenciacollege.edu/web/home-community/employees" target="_blank"><i class="far fa-desktop"></i><h3>Canvas Webinars</h3>
                        <p>Webinars are available for faculty and staff. Please log into Atlas and then open
                           the Valencia EDGE.  &nbsp;&nbsp;&nbsp;
                        </p></a>
                     
                  </div>
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="https://community.canvaslms.com/welcome" target="_blank"><i class="far fa-cubes"></i><h3>Canvas Resources</h3>
                        <p>A variety of Canvas resources are available, including the Canvas Guides and Canvas
                           Community. Additonal tips, tricks, and showcase resources are listed at the bottom
                           of this page. 
                        </p></a>
                     
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="" target=""><i class="far fa-rocket"></i><h3>Canvas Mobile Apps</h3>
                        <p>
                           Download Apple
                           Download Android
                           Download App Guide
                           
                        </p></a>
                     
                  </div>
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="/" target=""><i class="far fa-bullseye"></i><h3>Focus on target</h3>
                        <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                           cum no alii option, cu sit mazim libris.
                        </p></a>
                     
                  </div>
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="/" target=""><i class="far fa-graduation-cap"></i><h3>Focus on success</h3>
                        <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                           cum no alii option, cu sit mazim libris.
                        </p></a>
                     
                  </div>
               </div>
               
               <p>&nbsp;</p>
               
               <p style="text-align: center;"><a class="button-outline large" href="/tour.html">Take a tour of Valencia</a></p>
               
               <!--End container -->
               
               <p>&nbsp;</p>
            </div>
         </div>
         <div class="container margin-60">
            		
            		
            <div class="main-title">
               <td class="main-title">
                  						
                  <h2>Some words about Valencia ...</h2>
                  						
                  <p>
                     							Cum doctus civibus efficiantur in imperdiet deterruisset.
                     						
                  </p>
                  					
               </td>
            </div>
            		
            		
            <div class="row">
               <div class="col-md-8 col-sm-8">
                  						
                  <h3>Founded in 1967</h3>
                  						
                  <p>
                     							His nonumes maluisset delicatissimi ut, <strong>no ius odio putent vocibus</strong>. Oratio quidam essent his ei, at animal argumentum nam. Sale inani sapientem sit
                     at. Quo an vocent mediocritatem, eu natum argumentum pri. Deleniti invenire his ei,
                     ut mei liber ancillae, pri et fugit interpretaris. Quod unum democritum per no, nam
                     nostrud conclusionemque ad.
                     						
                  </p>
                  						
                  <p>
                     							His nonumes maluisset delicatissimi ut, no ius odio putent vocibus. Oratio
                     quidam essent his ei, at animal argumentum nam. Sale inani sapientem sit at. Quo an
                     vocent mediocritatem, eu natum argumentum pri. Deleniti invenire his ei, ut mei liber
                     ancillae, pri et fugit interpretaris. Quod unum democritum per no, nam nostrud conclusionemque
                     ad.
                     						
                  </p>
                  						
                  						
                  <div class="row">
                     <div class="col-md-5">
                        <ul class="list_4">
                           <li>Putent vocibus</li>
                           <li>Inani sapientem sit</li>
                           <li>Nonumes maluisset delicatissimi</li>
                        </ul>
                     </div>
                     <div class="col-md-5">
                        <ul class="list_4">
                           <li>Putent vocibus</li>
                           <li>Inani sapientem sit</li>
                           <li>Nonumes maluisset delicatissimi</li>
                        </ul>
                     </div>
                  </div>
                  						
                  						
                  <h3>Founders and Directors</h3>
                  						
                  <p>
                     							His nonumes maluisset delicatissimi ut, no ius odio putent vocibus. Oratio
                     quidam essent his ei, at animal argumentum nam. Sale inani sapientem sit at. Quo an
                     vocent mediocritatem, eu natum argumentum pri. Deleniti invenire his ei, ut mei liber
                     ancillae, pri et fugit interpretaris.
                     						
                  </p>
                  						
                  						
                  <ul class="list_staff">
                     <li>
                        <figure><img src="/_resources/images/teacher_1_thumb.jpg" alt="Valencia image description" class="img-circle"></figure>
                        <h4>Maria Hegel</h4>
                        <p>Director</p>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/teacher_2_thumb.jpg" alt="Valencia image description" class="img-circle"></figure>
                        <h4>Tomas John</h4>
                        <p>General Manager</p>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/teacher_3_thumb.jpg" alt="Valencia image description" class="img-circle"></figure>
                        <h4>Frank Alberti</h4>
                        <p>Student Manager</p>
                     </li>
                  </ul>
                  					
               </div>
               <div class="col-md-4 col-sm-4">
                  <div class="box_style_4">
                     						
                     <h4>Mission</h4>
                     						
                     <p>
                        							Sale inani sapientem sit at. Quo an vocent mediocritatem, eu natum argumentum
                        pri. Deleniti invenire his ei, ut mei liber ancillae, pri et fugit interpretaris.
                        						
                     </p>
                     						
                     						
                     <ul class="list_order">
                        <li><span>1</span>At errem clita officiis sed
                        </li>
                        <li><span>2</span>Sale inani sapientem sit at
                        </li>
                        <li><span>3</span>Deleniti invenire his ei
                        </li>
                        <li><span>4</span>Quod unum democritum per no
                        </li>
                        <li><span>5</span>Oportere efficiendi eu vim
                        </li>
                     </ul>
                     					
                  </div>
               </div>
            </div>
            		
            		
            <hr class="more_margin">
            		
            		
            <div class="row">
               <div class="col-md-4 col-sm-4">
                  						
                  <p><img src="/_resources/images/locations.jpg" alt="Campus Area" class="img-responsive"></p>
                  <h4>Campus Area</h4>
                  <p>Lorem ipsum dolor sit amet, ne vis suas harum nonumy, at enim vocent delicatissimi
                     eos, vocent inermis veritus mel no. Vix in offendit forensibus cotidieque.
                  </p>
                  					
               </div>
               <div class="col-md-4 col-sm-4">
                  						
                  <p><img src="/_resources/images/sport.jpg" alt="Sports Activities" class="img-responsive"></p>
                  <h4>Sport activites</h4>
                  <p>Lorem ipsum dolor sit amet, ne vis suas harum nonumy, at enim vocent delicatissimi
                     eos, vocent inermis veritus mel no. Vix in offendit forensibus cotidieque.
                  </p>
                  					
               </div>
               <div class="col-md-4 col-sm-4">
                  						
                  <p><img src="/_resources/images/conference_hall.jpg" alt="Conference Hall" class="img-responsive"></p>
                  <h4>Conference Hall</h4>
                  <p>Lorem ipsum dolor sit amet, ne vis suas harum nonumy, at enim vocent delicatissimi
                     eos, vocent inermis veritus mel no. Vix in offendit forensibus cotidieque.
                  </p>
                  					
               </div>
            </div>
            	
         </div>
         	
         		
         		
         <div class="container margin-30">
            <h4 class="stories"><strong>photo gallery</strong></h4>
         </div>
         <div class="grid">
            						
            <ul class="magnific-gallery">
               <li>
                  <figure><img src="/_resources/images/gallery/pic_8.jpg" alt="Image Gallery Picture 8"><figcaption>
                        <div class="caption-content"><a href="/_resources/images/gallery/large/pic_8.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                 										
                                 									 Your caption 
                                 										
                                 									
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
               <li>
                  <figure><img src="/_resources/images/gallery/pic_9.jpg" alt="Image Gallery Picture 9"><figcaption>
                        <div class="caption-content"><a href="/_resources/images/gallery/large/pic_9.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                 										
                                 									 Your caption 
                                 										
                                 									
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
               <li>
                  <figure><img src="/_resources/images/gallery/pic_10.jpg" alt="Image Gallery Picture 10"><figcaption>
                        <div class="caption-content"><a href="/_resources/images/gallery/large/pic_10.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                 										
                                 									 Your caption 
                                 										
                                 									
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
               <li>
                  <figure><img src="/_resources/images/gallery/pic_11.jpg" alt="Image Gallery Picture 1"><figcaption>
                        <div class="caption-content"><a href="/_resources/images/gallery/large/pic_1.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                 										
                                 									 Your caption 
                                 										
                                 									
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
            </ul>
            					
         </div>
         
         		
         <div class="container margin-30">
            <h4 class="stories"><strong>Stories gallery</strong></h4>
         </div>
         <div class="grid">
            						
            <ul>
               <li>
                  <figure><img src="/_resources/images/gallery/pic_8.jpg" alt="Image Gallery Picture 8"><figcaption>
                        <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/alumni/ask-an-alum-orange-county-commissioner-emily-bonilla/" target="" title="Story title"><i class="far fa-external-link-square"></i><p>
                                 										
                                 									 Story Title Link
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
               <li>
                  <figure><img src="/_resources/images/gallery/pic_9.jpg" alt="Image Gallery Picture 9"><figcaption>
                        <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/madelyn-young-1973-valencia-graduate-recalls-colleges-early-days/" target="" title="Story title"><i class="far fa-external-link-square"></i><p>
                                 										
                                 									 Story Title Link
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
               <li>
                  <figure><img src="/_resources/images/gallery/pic_10.jpg" alt="Image Gallery Picture 10"><figcaption>
                        <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/alumni/qa-series-orange-county-commissioner-betsyvanderley/" target="" title="Story title"><i class="far fa-external-link-square"></i><p>
                                 										
                                 									 Story Title Link
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
               <li>
                  <figure><img src="/_resources/images/gallery/pic_11.jpg" alt="Image Gallery Picture 1"><figcaption>
                        <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/taking-a-turn-for-the-better-inmates-graduate-from-valencias-construction-training-program/" target="" title="Story title"><i class="far fa-external-link-square"></i><p>
                                 										
                                 									 Story Title Link
                              </p></a></div>
                     </figcaption>
                  </figure>
               </li>
            </ul>
            					
         </div>
         		
         		
         <section class="grid">
            <div class="container margin-60">
               <div class="main-title">
                  						
                  <h2>Canvas Videos</h2>
                  						
                  <p>preview steps on how to use Canvas....</p>
                  					
               </div>
               <div class="row">
                  						
                  <ul class="magnific-gallery">
                     <li>
                        <figure><img src="/_resources/images/academics/canvas-education/video-thumbnail-student-overview.png" alt="Canvas Overview for Students"><figcaption>
                              <div class="caption-content"><a href="https://vimeo.com/74677642" class="video_pop" title="Canvas Overview"><i class="far fa-film"></i><p>
                                       										
                                       									  Canvas Overview
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/academics/canvas-education/video-thumbnail-cool-things.png" alt="Cool Things You Can Do With Canvas screenshot"><figcaption>
                              <div class="caption-content"><a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y" class="video_pop" title="Canvas Overview"><i class="far fa-film"></i><p>
                                       										
                                       									  Cool Things You Can Do With Canvas
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/academics/canvas-education/Canvas101-HelpResouces-Video_thumbAsset-1.png" alt="Canvas Help at Valencia"><figcaption>
                              <div class="caption-content"><a href="https://www.youtube.com/watch?v=apQ6G9-nUJw" class="video_pop" title="Canvas Help at Valencia"><i class="far fa-film"></i><p>
                                       										
                                       									  Canvas Help at Valencia
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                  </ul>
                  					
               </div>
            </div>
         </section>
         		
         		
         <section class="grid">
            <div class="container margin-60">
               <div class="main-title">
                  						
                  <h2>Photos ...</h2>
                  						
                  <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                  					
               </div>
               <div class="row">
                  						
                  <ul class="magnific-gallery">
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_4.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_4.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_5.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_5.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_6.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_6.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_7.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_7.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_8.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_8.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_9.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_9.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_10.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_10.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                     <li>
                        <figure><img src="/_resources/images/gallery/pic_11.jpg" alt="Valencia image description"><figcaption>
                              <div class="caption-content"><a href="/_resources/images/gallery/large/pic_1.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                       										
                                       									 Your caption 
                                       										
                                       									
                                    </p></a></div>
                           </figcaption>
                        </figure>
                     </li>
                  </ul>
                  					
               </div>
            </div>
         </section>
         	
      </main>
      <div class="ou-form container_gray_bg" id="newsletter_container">
         <div class="container margin-60">
            <div class="row">
               <div class="col-md-8 col-md-offset-2 text-center">
                  <h3>Subscribe to our Newsletter for latest news.</h3>
                  <div id="message-newsletter">
                     <div id="status_74643119-339c-4781-be67-88d76c494736"></div>
                  </div>
                  <form id="form_74643119-339c-4781-be67-88d76c494736" name="newsletter" method="post" class="form-inline" autocomplete="off"><span class="hp74643119-339c-4781-be67-88d76c494736" style="display:none; margin-left:-1000px;"><label for="hp74643119-339c-4781-be67-88d76c494736" class="hp74643119-339c-4781-be67-88d76c494736">If you see this don't fill out this input box.</label><input type="text" id="hp74643119-339c-4781-be67-88d76c494736"></span><input type="hidden" name="form_uuid" value="74643119-339c-4781-be67-88d76c494736"><input type="hidden" name="site_name" value="www"><input type="hidden" name="pageurl" value="https://preview.valenciacollege.edu/academics/canvas/students.php"><input name="email" id="email" type="email" value="" placeholder="Your Email" class="form-control"><button type="submit" id="btn_74643119-339c-4781-be67-88d76c494736" class="button"> Subscribe</button></form>
               </div>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><script type="text/javascript" src="/_resources/ldp/forms/js/ou-forms.js"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/canvas/students.pcf">©</a>
      </div>
   </body>
</html>