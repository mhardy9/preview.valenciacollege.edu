<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Canvas Education  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, canvas">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/canvas/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/canvas/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Canvas Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Five Things You Need to Know - Canvas Technology</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              <img src="/_resources/img/academics/canvas-education/five-things.png" alt="5 Things Logo" style="float: right; margin-left: 10">
                              
                              
                           </p>
                           
                           <ol>
                              
                              <li style="margin: 10px 0">Access to Canvas for faculty starting June 30. Login using your regular
                                 credentials. The new login for Canvas will be at online.valenciacollege.edu.
                                 
                              </li>
                              
                              
                              <li style="margin: 10px 0">All faculty members will have access to webinar training (provided directly
                                 from Canvas) starting June 30. These webinars can be taken for professional development
                                 credit. A
                                 separate email will be sent to you with detailed directions on accessing the webinars.
                                 
                              </li>
                              
                              
                              <li style="margin: 10px 0">All faculty accounts in Canvas have 5 development spaces (or “sandboxes”) in
                                 them. This means you can start playing around in Canvas as soon as you have access.
                                 
                              </li>
                              
                              
                              <li style="margin: 10px 0">Plan before you move! First, take the webinar training. Next, review your
                                 Blackboard courses and files before you start moving any content over. The goal is
                                 to move only the
                                 content that you still want and use in your courses!
                                 
                              </li>
                              
                              
                              <li style="margin: 10px 0">The Centers for Teaching/Learning Innovation (located on every campus) will be
                                 happy to assist you in transitioning to Canvas. 24/7 help from Canvas is available
                                 now to answer your
                                 questions.
                                 
                              </li>
                              
                           </ol>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Five Things You Need to Know - Canvas Pedagogy</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              <img src="/_resources/img/academics/canvas-education/five-things.png" alt="5 Things Logo" style="float: right; margin-left: 10">
                              
                           </p>
                           
                           <ul>
                              
                              <li>Item 1</li>
                              
                              <li>Item 2</li>
                              
                              <li>Item 3</li>
                              
                              <li>Item 4</li>
                              
                              <li>Item 5</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Recent Blog Posts</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              
                              
                           </p>
                           
                           <div class="post">
                              <a href="/blog_post_right_sidebar.html"><img src="/_resources/img/blog-1.jpg" alt="" class="img-responsive"></a>
                              
                              <div class="post_info clearfix">
                                 
                                 <div class="post-left">
                                    
                                    <ul>
                                       
                                       <li>
                                          <i class="icon-calendar-empty"></i>12/05/2015 <em>by Mark</em>
                                          
                                       </li>
                                       
                                       <li>
                                          <i class="icon-inbox-alt"></i><a href="#">Category</a>
                                          
                                       </li>
                                       
                                       <li>
                                          <i class="icon-tags"></i><a href="#">Works</a>, <a href="#">Personal</a>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div class="post-right">
                                    <i class="icon-comment"></i><a href="#">25 </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h2>Blog Post #1</h2>
                              
                              <p>
                                 Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius
                                 lucilius, hinc
                                 eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei
                                 pri quaerendum
                                 intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium
                                 duo......
                                 
                              </p>
                              
                              <p>
                                 Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius
                                 lucilius, hinc
                                 eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei
                                 pri quaerendum
                                 intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium
                                 duo......
                                 
                              </p>
                              <a href="blog_post.html" class="button">Read more</a>
                              
                           </div>
                           
                           
                           
                           <div class="post">
                              <a href="/blog_post_right_sidebar.html"><img src="/_resources/img/blog-2.jpg" alt="" class="img-responsive"></a>
                              
                              <div class="post_info clearfix">
                                 
                                 <div class="post-left">
                                    
                                    <ul>
                                       
                                       <li>
                                          <i class="icon-calendar-empty"></i>12/05/2015 <em>by Mark</em>
                                          
                                       </li>
                                       
                                       <li>
                                          <i class="icon-inbox-alt"></i><a href="#">Category</a>
                                          
                                       </li>
                                       
                                       <li>
                                          <i class="icon-tags"></i><a href="#">Works</a>, <a href="#">Personal</a>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div class="post-right">
                                    <i class="icon-comment"></i><a href="#">25 </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h2>Blog Post #2</h2>
                              
                              <p>
                                 Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius
                                 lucilius, hinc
                                 eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei
                                 pri quaerendum
                                 intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium
                                 duo......
                                 
                              </p>
                              
                              <p>
                                 Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius
                                 lucilius, hinc
                                 eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei
                                 pri quaerendum
                                 intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium
                                 duo......
                                 
                              </p>
                              <a href="blog_post.html" class="button">Read more</a>
                              
                           </div>
                           
                           
                           
                           <div class="post">
                              <a href="/blog_post_right_sidebar.html"><img src="/_resources/img/blog-3.jpg" alt="" class="img-responsive"></a>
                              
                              <div class="post_info clearfix">
                                 
                                 <div class="post-left">
                                    
                                    <ul>
                                       
                                       <li>
                                          <i class="icon-calendar-empty"></i>12/05/2015 <em>by Mark</em>
                                          
                                       </li>
                                       
                                       <li>
                                          <i class="icon-inbox-alt"></i><a href="#">Category</a>
                                          
                                       </li>
                                       
                                       <li>
                                          <i class="icon-tags"></i><a href="#">Works</a>, <a href="#">Personal</a>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div class="post-right">
                                    <i class="icon-comment"></i><a href="#">25 </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h2>Blog Post #3</h2>
                              
                              <p>
                                 Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius
                                 lucilius, hinc
                                 eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei
                                 pri quaerendum
                                 intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium
                                 duo......
                                 
                              </p>
                              
                              <p>
                                 Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius
                                 lucilius, hinc
                                 eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei
                                 pri quaerendum
                                 intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium
                                 duo......
                                 
                              </p>
                              <a href="blog_post.html" class="button">Read more</a>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/canvas/index.pcf">©</a>
      </div>
   </body>
</html>