<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Canvas Resources | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/canvas/canvas.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/canvas/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Canvas Resources</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/canvas/">Canvas Education</a></li>
               <li>Canvas Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        	
                        <div class="col-md-4 row wrapper_indent box_style_1">
                           	
                           <h3>Canvas is Coming!</h3>
                           
                           <p>For more information, please refer to the article in The Grove.</p>
                           	
                        </div>
                        
                        <div class="row wrapper_indent countdown">
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-days"></span><span class="step_to_do_text">DAYS</span></div>
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-hours"></span><span class="step_to_do_text">HOURS</span></div>
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-minutes"></span><span class="step_to_do_text">MINUTES</span></div>
                           
                           <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number" id="countdown-seconds"></span><span class="step_to_do_text">SECONDS</span></div>
                           
                        </div>
                        
                        
                        		
                        		
                        		
                        <hr>
                        		
                        <div class="container margin-30">
                           		
                           <div class="row">
                              <div class="col-md-4 col-sm-4">
                                 						<a class="box_feat" href="https://online.valenciacollege.edu/login/canvas" target=""><i class="far fa-users"></i><h3>Canvas Login</h3>
                                    <p>Login into online.valenciacollege.edu using your Atlas credentials. </p></a>
                                 					
                              </div>
                              <div class="col-md-4 col-sm-4">
                                 						<a class="box_feat" href="https://community.canvaslms.com/community/answers/" target=""><i class="far fa-desktop"></i><h3>Canvas Guides</h3>
                                    <p>Use the Instructor and Student Guides to help find basic Canvas information.</p></a>
                                 					
                              </div>
                              <div class="col-md-4 col-sm-4">
                                 						<a class="box_feat" href="Community " target=""><i class="far fa-cubes"></i><h3>Canvas Community</h3>
                                    <p>Coming soon! Canvas resources will be linked here to help you master Canvas.</p></a>
                                 					
                              </div>
                           </div>
                           		
                           <div class="container margin-30">
                              			
                              <div class="main-title">
                                 
                                 <h2>How to Get Help with Canvas</h2>
                                 
                                 <p>A variety of 24/7 help options are available. Just click on the Help button at the
                                    bottom of the left-hand menu inside Canvas to get access these options.
                                 </p>
                              </div>
                              	
                              <div class="col-md-6 col-md-offset-3">
                                 <div id="graph"><img src="images/graphic1.png" class="wow zoomIn" data-wow-delay="0.1s" alt="Canvas Login Screenshot"><div class="features step_1 wow flipInX" data-wow-delay="1s">
                                       							
                                       <h4> Ask Your Instructor a Questions</h4>
                                       							
                                       <p>Send direct messages to your instructor using Canvas’ internal system. Select your
                                          course and then you can send a message.
                                       </p>
                                       						
                                    </div>
                                    <div class="features step_2 wow flipInX" data-wow-delay="1.5s">
                                       							
                                       <h4>Chat with Canvas Support</h4>
                                       							
                                       <p>Chat live with someone from Canvas to get support for any issues you are experiencing</p>
                                       						
                                    </div>
                                    <div class="features step_3 wow flipInX" data-wow-delay="2s">
                                       							
                                       <h4>Canvas Support </h4>
                                       							
                                       <p><strong>Support Hotline 407-582-5600</strong> – Speak 24/7 with a Canvas support agent. <br><strong>Create a Support Ticket</strong> – Submit a ticket when you need to ask a question but can’t stay on the phone or
                                          chat live.
                                          
                                       </p>
                                       						
                                    </div>
                                    <div class="features step_4 wow flipInX" data-wow-delay="2.5s">
                                       							
                                       <h4>Search the Canvas Guides </h4>
                                       							
                                       <p>Locate the answers to most general Canvas questions.</p>
                                       						
                                    </div>
                                 </div>
                              </div>
                              			
                           </div>
                           			
                        </div>
                        		
                        	  
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/canvas/canvas.pcf">©</a>
      </div>
   </body>
</html>