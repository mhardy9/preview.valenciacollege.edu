<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Canvas Resources | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/canvas/resources1.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/canvas/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      		
      		
      <div class="bg-content testimonials">
         <div class="row">
            <div class="col-md-offset-1 col-md-10">
               <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                  <ol class="carousel-indicators">
                     <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                     <li data-target="#quote-carousel" data-slide-to="1"></li>
                     <li data-target="#quote-carousel" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                     <div class="item active">
                        <blockquote>
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                              eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                           </p>
                        </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Stefany</small></div>
                     <div class="item">
                        <blockquote>
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                              eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                           </p>
                        </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_2.jpg" alt="Valencia image description">Karla</small></div>
                     <div class="item">
                        <blockquote>
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                              eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                           </p>
                        </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Maira</small></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      	
      		
      		
      <div class="container_gray_bg" id="home-feat-1">
         <div class="container">
            <div class="row">
               <div class="col-md-4 col-sm-4">
                  						
                  <div class="home-feat-1-box"><a href="/" target="" title=""><img src="/_resources/images/home-feat-1_1.jpg" alt="Valencia image description" class="img-responsive"><div class="short_info">
                           <h3>Plan a visit</h3><i class="arrow_carrot-right_alt2"></i></div></a></div>
                  					
               </div>
               <div class="col-md-4 col-sm-4">
                  						
                  <div class="home-feat-1-box"><a href="/" target="" title=""><img src="/_resources/images/home-feat-1_2.jpg" alt="Valencia image description" class="img-responsive"><div class="short_info">
                           <h3>Study Programs</h3><i class="arrow_carrot-right_alt2"></i></div></a></div>
                  					
               </div>
               <div class="col-md-4 col-sm-4">
                  						
                  <div class="home-feat-1-box"><a href="/" target="" title=""><img src="/_resources/images/home-feat-1_3.jpg" alt="Valencia image description" class="img-responsive"><div class="short_info">
                           <h3>Admissions</h3><i class="arrow_carrot-right_alt2"></i></div></a></div>
                  					
               </div>
            </div>
         </div>
      </div>
      	
      <div class="container margin-60">
         			
         			
         <div class="main-title">
            <td class="main-title">
               							
               <h2>Valencia core feautures</h2>
               							
               <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
               						
            </td>
         </div>
         			
         			
         <div class="row">
            <div class="col-md-6 col-sm-6">
               							
               <div class="box-feat-home"><i class="iconcustom-certificate"></i><h3>Quality Certifications</h3>
                  <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                     Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique
                     an usu.
                  </p>
               </div>
               						
            </div>
            <div class="col-md-6 col-sm-6">
               							
               <div class="box-feat-home"><i class="iconcustom-certificate"></i><h3>Learning Best Practice</h3>
                  <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                     Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique
                     an usu.
                  </p>
               </div>
               						
            </div>
         </div>
         			
         <div class="row">
            <div class="col-md-6 col-sm-6">
               							
               <div class="box-feat-home"><i class="iconcustom-certificate"></i><h3>Online Resources</h3>
                  <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                     Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique
                     an usu.
                  </p>
               </div>
               						
            </div>
            <div class="col-md-6 col-sm-6">
               							
               <div class="box-feat-home"><i class="iconcustom-certificate"></i><h3>Study Plan Tutors</h3>
                  <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                     Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique
                     an usu.
                  </p>
               </div>
               						
            </div>
         </div>
         			
         <div class="row">
            <div class="col-md-6 col-sm-6">
               							
               <div class="box-feat-home"><i class="iconcustom-certificate"></i><h3>Advanced Practice</h3>
                  <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                     Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique
                     an usu.
                  </p>
               </div>
               						
            </div>
            <div class="col-md-6 col-sm-6">
               							
               <div class="box-feat-home"><i class="iconcustom-certificate"></i><h3>Research</h3>
                  <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                     Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique
                     an usu.
                  </p>
               </div>
               						
            </div>
         </div>
         			
         <hr class="more_margin">
         			
         		
         <div class="row add_bottom_60">
            		
            			
            <div class="main-title">
               <td class="main-title">
                  							
                  <h2>Valencia focus on these ....</h2>
                  							
                  <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                  						
               </td>
            </div>
            
            			
            <div class="col-md-6 col-md-offset-3">
               <div id="graph"><img src="/_resources/images/graphic.jpg" class="wow zoomIn" data-wow-delay="0.1s" alt="Valencia image description"><div class="features step_1 wow flipInX" data-wow-delay="1s">
                     							
                     <h4><strong>01.</strong> Students growth
                     </h4>
                     							
                     <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                        Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                     </p>
                     						
                  </div>
                  <div class="features step_2 wow flipInX" data-wow-delay="1.5s">
                     							
                     <h4><strong>02.</strong> Best learning practice
                     </h4>
                     							
                     <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                        Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                     </p>
                     						
                  </div>
                  <div class="features step_3 wow flipInX" data-wow-delay="2s">
                     							
                     <h4><strong>03.</strong> Focus on targets
                     </h4>
                     							
                     <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                        Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                     </p>
                     						
                  </div>
                  <div class="features step_4 wow flipInX" data-wow-delay="2.5s">
                     							
                     <h4><strong>04.</strong> Interdisciplanary model
                     </h4>
                     							
                     <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                        Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                     </p>
                     						
                  </div>
               </div>
            </div>
            	
         </div>
         <p style="text-align: center;"><a class=" button-outline large" href="/academics/tour.html">Take a tour of Valencia</a></p>
      </div>
      		
      		
      <div class="bg-content testimonials">
         <div class="row">
            <div class="col-md-offset-1 col-md-10">
               <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                  <ol class="carousel-indicators">
                     <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                     <li data-target="#quote-carousel" data-slide-to="1"></li>
                     <li data-target="#quote-carousel" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                     <div class="item active">
                        <blockquote>
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                              eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                           </p>
                        </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Stefany</small></div>
                     <div class="item">
                        <blockquote>
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                              eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                           </p>
                        </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_2.jpg" alt="Valencia image description">Karla</small></div>
                     <div class="item">
                        <blockquote>
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                              eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                           </p>
                        </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Maira</small></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      	
      <div class="container margin-60">
         		
         		
         <div class="main-title">
            <td class="main-title">
               						
               <h2>Latest from Valencia ....</h2>
               						
               <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
               					
            </td>
         </div>
         		
         		
         <div id="tabs" class="tabs">
            <nav>
               <ul>
                  <li><a href="#section-1"><i class="far fa-book"></i>Courses</a></li>
                  <li><a href="#section-2"><i class="far fa-newspaper-o"></i>News</a></li>
                  <li><a href="#section-3"><i class="far fa-calendar"></i>Events</a></li>
               </ul>
            </nav>
            <div class="content">
               <section id="section-1">
                  						
                  <div class="container-fluid">
                     <div class="container margin-60">
                        <div class="row">
                           <div class="col-md-4">
                              										
                              <div class="list_courses_tabs">
                                 <h2>Diploma Courses</h2>
                                 <ul>
                                    														
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_1_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Mathemacis</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_2_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Sciences</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_3_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Litterature</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_4_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Arts</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_5_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Music</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    													
                                    <li>
                                       <div><a href="/" target="" title="" class="link-normal">View all Diploma courses</a></div>
                                    </li>
                                 </ul>
                              </div>
                              									
                           </div>
                           <div class="col-md-4">
                              										
                              <div class="list_courses_tabs">
                                 <h2>Graduate Courses</h2>
                                 <ul>
                                    														
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_6_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Engineering</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_7_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Biology</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_8_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Psicology</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_9_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Economy</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_10_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Architecture</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    													
                                    <li>
                                       <div><a href="/" target="" title="" class="link-normal">View all Diploma courses</a></div>
                                    </li>
                                 </ul>
                              </div>
                              									
                           </div>
                           <div class="col-md-4">
                              										
                              <div class="list_courses_tabs">
                                 <h2>Master Courses</h2>
                                 <ul>
                                    														
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_11_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Information tecnology</strong> master
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_12_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Digital media</strong> master
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_13_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Sculpture</strong> master
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    													
                                    <li>
                                       <div><a href="/" target="" title="" class="link-normal">View all Diploma courses</a></div>
                                    </li>
                                 </ul>
                              </div>
                              									
                           </div>
                        </div>
                     </div>
                  </div>
                  					
               </section>
               <section id="section-2">
                  						
                  <div class="container-fluid">
                     <div class="container margin-60">
                        <div class="row">
                           <div class="col-md-4">
                              										
                              <div class="list_news_tabs">
                                 <p><a href="/" target="" title=""><img src="/_resources/images/news_1_thumb.jpg" alt="Valencia image description" class="img-responsive"></a></p><span class="date_published">20 Agusut 2015</span><h3><a href="/" target="" title="">Success Stories for Valencia College in 2015's</a></h3>
                                 <p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque
                                    omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.
                                 </p><a href="/" target="" title="" class="button small">Read more</a></div>
                              									
                           </div>
                           <div class="col-md-4">
                              										
                              <div class="list_news_tabs">
                                 <p><a href="/" target="" title=""><img src="/_resources/images/news_2_thumb.jpg" alt="Valencia image description" class="img-responsive"></a></p><span class="date_published">20 Agusut 2015</span><h3><a href="/" target="" title="">Boost in A-Level Grades in 2015 for Students</a></h3>
                                 <p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque
                                    omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.
                                 </p><a href="/" target="" title="" class="button small">Read more</a></div>
                              									
                           </div>
                           <div class="col-md-4">
                              										
                              <div class="list_news_tabs">
                                 <p><a href="/" target="" title=""><img src="/_resources/images/news_3_thumb.jpg" alt="Valencia image description" class="img-responsive"></a></p><span class="date_published">20 Agusut 2015</span><h3><a href="/" target="" title="">Caps Off To Sport at Valencia</a></h3>
                                 <p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque
                                    omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.
                                 </p><a href="/" target="" title="" class="button small">Read more</a></div>
                              									
                           </div>
                        </div>
                     </div>
                  </div>
                  					
               </section>
               <section id="section-3">
                  						
                  <div class="container-fluid">
                     <div class="container margin-60">
                        <div class="row">
                           <div class="col-md-4">
                              										
                              <div class="list_news_tabs">
                                 <p><a href="/" target="" title=""><img src="/_resources/images/event_1_thumb.jpg" alt="Valencia image description" class="img-responsive"></a></p><span class="date_published">20 Agusut 2015</span><h3><a href="/" target="" title="">Next students meeting</a></h3>
                                 <p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque
                                    omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.
                                 </p><a href="/" target="" title="" class="button small">Read more</a></div>
                              									
                           </div>
                           <div class="col-md-4">
                              										
                              <div class="list_news_tabs">
                                 <p><a href="/" target="" title=""><img src="/_resources/images/event_2_thumb.jpg" alt="Valencia image description" class="img-responsive"></a></p><span class="date_published">20 Agusut 2015</span><h3><a href="/" target="" title="">10 October Open day</a></h3>
                                 <p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque
                                    omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.
                                 </p><a href="/" target="" title="" class="button small">Read more</a></div>
                              									
                           </div>
                           <div class="col-md-4">
                              										
                              <div class="list_news_tabs">
                                 <p><a href="/" target="" title=""><img src="/_resources/images/event_3_thumb.jpg" alt="Valencia image description" class="img-responsive"></a></p><span class="date_published">20 Agusut 2015</span><h3><a href="/" target="" title="">Photography workshop</a></h3>
                                 <p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque
                                    omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.
                                 </p><a href="/" target="" title="" class="button small">Read more</a></div>
                              									
                           </div>
                        </div>
                     </div>
                  </div>
                  					
               </section>
            </div>
         </div>
         	
      </div>
      		
      		
      <div class="bg-content magnific" style="url(/_resources/images/testimonial_1.jpg) no-repeat center center;">
         <div>
            <h3>Discover the Campus</h3>
            <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.</p>
         </div>
      </div>
      	<?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/canvas/resources1.pcf">©</a>
      </div>
   </body>
</html>