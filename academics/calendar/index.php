<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Academic Calendar | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/calendar/index.php";
				</script></head>
   <body>
      	<?php //First set of Feeds
			$number_feeds = '7';

			$event_feed_url_array= 
				array(	'https://events.valenciacollege.edu/api/2/events?featured=1&distinct=true&days=365&pp=3&exclude_type=13225&venues=EastCampus&num=50',
						'https://events.valenciacollege.edu/api/2/events?featured=1&distinct=true&days=365&pp=3&exclude_type=13225&venues=lakenonacampus&num=50',
						'https://events.valenciacollege.edu/api/2/events?featured=1&distinct=true&days=365&pp=3&exclude_type=13225&venues=OsceolaCampus&num=50',
						'https://events.valenciacollege.edu/api/2/events?featured=1&distinct=true&days=365&pp=3&exclude_type=13225&venues=poinciana&num=50',
						'https://events.valenciacollege.edu/api/2/events?featured=1&distinct=true&days=365&pp=3&exclude_type=13225&venues=WestCampus&num=50',
						'https://events.valenciacollege.edu/api/2/events?featured=1&distinct=true&days=365&pp=3&exclude_type=13225&venues=WinterParkCampus&num=50', 
						'http://events.valenciacollege.edu/api/2/events?schools=valencia&days=365&types=13121&num=50'); 
			$feed_titles_array =
				array(	'East Campus', 'Lake Nona Campus', 'Osceola Campus','Poinciana Campus',  'West Campus', 'Winter Park Campus', 'Arts, Culture &amp; Entertainment Events' );
			$link_array =
				array ('http://events.valenciacollege.edu/EastCampus',
						'http://events.valenciacollege.edu/LakeNonaCampus',
						'http://events.valenciacollege.edu/OsceolaCampus',
						'http://events.valenciacollege.edu/Poinciana',
						'http://events.valenciacollege.edu/WestCampus',
						'http://events.valenciacollege.edu/WinterParkCampus',
						'http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121');

		//Second set of Feeds
		$number_feeds_second_pp = '4';

			$event_feed_url_array_second_pp= 
				array(	'https://events.valenciacollege.edu/api/2/events?event_types%5B%5D=13132',
						'https://events.valenciacollege.edu/api/2/events?event_types%5B%5D=13165',
						'https://events.valenciacollege.edu/api/2/events?event_types%5B%5D=13225',
						'https://events.valenciacollege.edu/api/2/events?event_types%5B%5D=13145'); 
			$feed_titles_array_second_pp =
				array(	'Meetings and Notices', 'Faculty Development Calendar', 'Employee Activities',  'Valencia EDGE' );
			$link_array_second_pp =
				array ('http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13132',
						'http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13165',
						'http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13225',
						'http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13145');
		?>
      
      ?&gt;
      	<?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Academic Calendar</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Calendar</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  			
                  <h3>Academic Calendars</h3>
                  			
                  <div class="row">
                     				 
                     <div class="col-md-4 col-sm-4  box_style_2"><span class="far fa-check-square fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">Exam Schedules</h3>
                        
                        <ul>
                           			 
                           <li><a href="/academics/calendar/final-exams-spring-2018.php">Spring Final Exam Schedule 2018</a></li>
                           			 
                           <li>Summer Final Exam Schedule 2018<br><em>Given during regular class meetings.</em></li>
                           		  	
                           <li>Fall Final Exam Schedule 2018<br><em>To be posted x/x/xx</em></li>
                           	  
                        </ul><br></div>
                     
                     <div class="col-md-4 col-sm-4 box_style_2"><span class="far fa-calendar-alt fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">Academic Calendars</h3>
                        
                        <ul>
                           		  
                           <li><a href="documents/2017-18-Academic-Calendar.pdf" target="_blank">Academic Calendar 2017-2018</a></li>
                           		  
                           <li><a href="documents/2018-19-Academic-Calendar.pdf" target="_blank">Academic Calendar 2018-2019</a></li>
                           	  
                        </ul><br></div>
                     				 
                     <div class="col-md-4 col-sm-4 box_style_2"><span class="far fa-clock fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">Important Dates &amp; Deadlines</h3>
                        
                        <ul>
                           		  
                           <li><a href="/academics/calendar/important-dates-17-18.php" target="_blank">Important Dates &amp; Deadlines 2017-2018</a></li>
                           		  
                           <li><a href="/academics/calendar/important-dates-18-19.php" target="_blank">Important Dates &amp; Deadlines 2018-2019</a></li>
                           	  
                        </ul><br></div>
                     				
                  </div>
                  			
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4 box_style_2"><span class="far fa-question-circle fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">About Final Exams</h3>
                        
                        <p>Students should expect a final examination in all Valencia courses. It is the student's
                           responsibility to know the time and location for each course's final examination,
                           to be present and on time for the scheduled exam, and to be familiar with each instructor's
                           policy and procedure for the exam; additional information can be found in the individual
                           instructor's course syllabus. 
                        </p>
                     </div>
                     				 
                     <div class="col-md-4 col-sm-4 box_style_2"><span class="far fa-dollar-sign fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">Payment Deadlines</h3>
                        
                        <ul>
                           		  
                           <li><a href="/students/business-office/important-deadlines.php"><strong>Business Office Payment Deadlines</strong></a></li>
                           	  
                        </ul><br></div>
                     
                     <div class="col-md-4 col-sm-4 box_style_2"><span class="far fa-share-alt fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">DRAFT Academic Calendars</h3>
                        
                        <ul>
                           		  
                           <li><a href="documents/Web2018-2021-Rev.5-4-16.pdf" target="_blank">Academic Calendars 2018-2021 Draft (Rev. 5-4-16)</a></li>
                           	  
                        </ul><br></div>
                     				
                     				
                  </div>
                  
                  
                  			
                  <h3>On Campus This Week</h3>
                  	<?php include ($_SERVER["DOCUMENT_ROOT"]."/_staging/meh-test/multi-event-feed.php"); 
		?>
                  			
                  			
                  <h3>Faculty &amp; Staff Event Calendars</h3>
                  			<?php include ($_SERVER["DOCUMENT_ROOT"]."/_staging/meh-test/multi-event-feed-second-pp.php"); 
		?>
                  		
               </div>
               	
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/calendar/index.pcf">©</a>
      </div>
   </body>
</html>