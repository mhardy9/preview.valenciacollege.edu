
<ul>
	<li><a href="/academics/calendar/index.php">Academic Calendars</a></li>
	<li class="submenu">
		<a class="show-submenu" href="">Exam Schedules <span class="caret"></span></a>
		<ul>
			<li><a href="/academics/calendar/final-exams-spring-2018.php">Spring Final Exam Schedule 2018</a></li>
		</ul>			
	</li>
	<li class="submenu">
		<a class="show-submenu" href="">Academic Calendars <span class="caret"></span></a>
		<ul>
			<li><a href="documents/2017-18-Academic-Calendar.pdf" target="_blank">Academic Calendar 2017-2018</a></li>
			<li><a href="documents/2018-19-Academic-Calendar.pdf" target="_blank">Academic Calendar 2018-2019</a></li>
		</ul>			
	</li>
	<li class="submenu">
		<a class="show-submenu" href="">Important Dates &amp; Deadlines <span class="caret"></span></a>
		<ul>
			<li><a href="/academics/calendar/important-dates-17-18.php" target="_blank">Important Dates &amp; Deadlines 2017-2018</a></li>
			<li><a href="/academics/calendar/important-dates-18-19.php" target="_blank">Important Dates &amp; Deadlines 2018-2019</a></li>
		</ul>			
	</li>
	<li class="submenu">
		<a class="show-submenu" href="">Payment Deadlines <span class="caret"></span></a>
		<ul>
			<li><a href="/students/business-office/important-deadlines.php"><strong>Business Office Payment Deadlines</strong></a></li>
		</ul>			
	</li>
	<li class="submenu">
		<a class="show-submenu" href="">Draft Calendars <span class="caret"></span></a>
		<ul>
			<li><a href="documents/Web2018-2021-Rev.5-4-16.pdf" target="_blank">Academic Calendars 2018-2021 Draft (Rev. 5-4-16)</a></li>
		</ul>			
	</li>

</ul>