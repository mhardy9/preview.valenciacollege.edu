<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Final Exams Spring 2018 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/calendar/final-exams-spring-2018.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Final Exams Spring 2018</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/calendar/">Calendar</a></li>
               <li>Final Exams Spring 2018</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30">
                  
                  <h2>Spring 2018 Class Meeting Times and Final Exams</h2>
                  		
                  <p>For classes scheduled in the standard 3 contact hour blocks.</p>
                  		
                  <p>Last revised: 2/6/18</p>
                  		
                  <p>NOTE: If your class time is not listed you should select the exam timeslot that includes
                     the start time of your class.  You may have to work with individual students who have
                     exam conflicts.
                  </p>
                  
                  		
                  <table class="table table">
                     		
                     <tbody>
                        			
                        <th colspan="2" scope="col">Fall and Spring Day(s) of Week and Class Meeting Time</th>
                        <th colspan="3" scope="col">Final Exam Day/Date of the Week and Time</th>
                        			
                        <tr>
                           <td>MWF</td>
                           <td>7:00 – 7:50</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>8:00 – 8:50</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>9:00 – 9:50</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>10:00 – 10:50</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>11:00 – 11:50</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>12:00 – 12:50</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>13:00 – 13:50</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>14:00 – 14:50</td>
                           <td>W</td>
                           <td>April 25, 2018,</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>15:00 – 15:50</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>MWF</td>
                           <td>17:30 – 20:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>17:00 – 19:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>7:00 – 8:15</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>8:30 – 9:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>10:00 – 11:15</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>11:30 – 12:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>13:00 – 14:15</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>14:30 – 15:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>16:00 – 17:15</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>17:00 – 19:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>17:30 – 18:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>17:00 – 19:30</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>19:00 – 20:15</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>MW</td>
                           <td>20:30 – 21:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>7:00 – 8:15</td>
                           <td>T</td>
                           <td>April 24, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>8:30 – 9:45</td>
                           <td>R</td>
                           <td>April 26, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>10:00 – 11:15</td>
                           <td>T</td>
                           <td>April 24, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>11:30 – 12:45</td>
                           <td>R</td>
                           <td>April 26, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>13:00 – 14:15</td>
                           <td>T</td>
                           <td>April 24, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>14:30 – 15:45</td>
                           <td>R</td>
                           <td>April 26, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>16:00 – 17:15</td>
                           <td>T</td>
                           <td>April 24, 2018</td>
                           <td>17:00 – 19:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>17:30 – 18:45</td>
                           <td>R</td>
                           <td>April 26, 2018</td>
                           <td>17:00 – 19:30</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>19:00 – 20:15</td>
                           <td>T</td>
                           <td>April 24, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>TR</td>
                           <td>20:30 – 21:45</td>
                           <td>R</td>
                           <td>April 26, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>M</td>
                           <td>17:30 – 20:45</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>17:30 – 20:00</td>
                        </tr>
                        
                        <tr>
                           <td>M</td>
                           <td>19:00 – 21:45</td>
                           <td>M</td>
                           <td>April 23, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>T</td>
                           <td>17:30 – 20:45</td>
                           <td>T</td>
                           <td>April 24, 2018</td>
                           <td>17:30 – 20:00</td>
                        </tr>
                        
                        <tr>
                           <td>T</td>
                           <td>19:00 – 21:45</td>
                           <td>T</td>
                           <td>April 24, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>W</td>
                           <td>17:30 – 20:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>17:30 – 20:00</td>
                        </tr>
                        
                        <tr>
                           <td>W</td>
                           <td>19:00 – 21:45</td>
                           <td>W</td>
                           <td>April 25, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>R</td>
                           <td>17:30 – 20:45</td>
                           <td>R</td>
                           <td>April 26, 2018</td>
                           <td>17:30 – 20:00</td>
                        </tr>
                        
                        <tr>
                           <td>R</td>
                           <td>19:00 – 21:45</td>
                           <td>R</td>
                           <td>April 26, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>F</td>
                           <td>7:00 – 9:45</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>F</td>
                           <td>8:30 – 11:15</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>F</td>
                           <td>10:00 – 12:45</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>F</td>
                           <td>13:00 – 15:45</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>F</td>
                           <td>16:00 – 18:45</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>17:00 – 19:30</td>
                        </tr>
                        
                        <tr>
                           <td>F</td>
                           <td>17:30 – 20:45</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>17:30 – 20:00</td>
                        </tr>
                        
                        <tr>
                           <td>F</td>
                           <td>19:00 – 21:45</td>
                           <td>F</td>
                           <td>April 27, 2018</td>
                           <td>19:45 – 22:15</td>
                        </tr>
                        
                        <tr>
                           <td>S</td>
                           <td>7:00 – 9:45</td>
                           <td>S</td>
                           <td>April 28, 2018</td>
                           <td>7:00 – 9:30</td>
                        </tr>
                        
                        <tr>
                           <td>S</td>
                           <td>10:00 -12:45</td>
                           <td>S</td>
                           <td>April 28, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>S</td>
                           <td>13:00 – 15:45</td>
                           <td>S</td>
                           <td>April 28, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        <tr>
                           <td>U</td>
                           <td>10:00 – 12:45</td>
                           <td>U</td>
                           <td>April 29, 2018</td>
                           <td>10:00 – 12:30</td>
                        </tr>
                        
                        <tr>
                           <td>U</td>
                           <td>13:00 – 15:45</td>
                           <td>U</td>
                           <td>April 29, 2018</td>
                           <td>13:00 – 15:30</td>
                        </tr>
                        
                        			
                     </tbody>
                     		
                     		
                  </table>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/calendar/final-exams-spring-2018.pcf">©</a>
      </div>
   </body>
</html>