<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Important Dates 2017-2018 | Valencia College</title>
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/calendar/important-dates-17-18.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Important Dates 2017-2018</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/calendar/">Calendar</a></li>
               <li>Important Dates 2017-2018</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#fall"> <i class="far fa-pencil-alt"></i>
                           					
                           <h3>Fall</h3>
                           					
                           <p>Important dates for Fall 2017 Term</p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#spring"> <i class="far fa-book"></i>
                           					
                           <h3>Spring</h3>
                           					
                           <p>Important dates for Spring 2018 Term</p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#summer"> <i class="far fa-sun"></i>
                           					
                           <h3>Summer</h3>
                           					
                           <p>Important dates for Summer 2018 Term</p>
                           					</a></div>
                     			
                  </div>
                  			<a id="fall"></a>
                  			
                  <div class="main-title">
                     				
                     <h2>Fall 2017 Important Dates</h2>
                     				
                     <p>Revised 10/04/17</p>
                     			
                  </div>
                  			
                  <div>
                     				
                     <table class="table table table-striped">
                        					
                        <tr>
                           <td></td>
                           <th scope="col">FULL TERM 1</th>
                           <th scope="col">1ST 8 WEEKS<br>H1
                           </th>
                           <th scope="col">2ND 8 WEEKS<br>H2
                           </th>
                           <th scope="col">1ST 10 WEEKS<br>TWJ
                           </th>
                           <th scope="col">2ND 10 WEEKS<br>TWK
                           </th>
                           <th scope="col">MIDDLE 8 WEEKS LSC</th>
                           					
                        </tr>
                        					
                        <tr>
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Advanced Registration Begins Returning Students</div>
                              </div>
                           </td>
                           						
                           <td>23-May-17</td>
                           						
                           <td>23-May-17</td>
                           						
                           <td>23-May-17</td>
                           						
                           <td>23-May-17</td>
                           						
                           <td>23-May-17</td>
                           						
                           <td>23-May-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Advanced Registration Begins New Students</div>
                              </div>
                           </td>
                           						
                           <td>30-May-17</td>
                           						
                           <td>30-May-17</td>
                           						
                           <td>30-May-17</td>
                           						
                           <td>30-May-17</td>
                           						
                           <td>30-May-17</td>
                           						
                           <td>30-May-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Open Registration Begins  New and Returning Students</div>
                              </div>
                           </td>
                           						
                           <td>02-Jun-17</td>
                           						
                           <td>02-Jun-17</td>
                           						
                           <td>02-Jun-17</td>
                           						
                           <td>02-Jun-17</td>
                           						
                           <td>02-Jun-17</td>
                           						
                           <td>02-Jun-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Interntional Student Application Deadline<br>Student-Out of Country
                                 </div>
                              </div>
                           </td>
                           						
                           <td>15-Jun-17</td>
                           						
                           <td>15-Jun-17</td>
                           						
                           <td>15-Jun-17</td>
                           						
                           <td>15-Jun-17</td>
                           						
                           <td>15-Jun-17</td>
                           						
                           <td>15-Jun-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">International Student Application Deadline<br>Student-In Country
                                 </div>
                              </div>
                           </td>
                           						
                           <td>15-Jul-17</td>
                           						
                           <td>15-Jul-17</td>
                           						
                           <td>15-Jul-17</td>
                           						
                           <td>15-Jul-17</td>
                           						
                           <td>15-Jul-17</td>
                           						
                           <td>15-Jul-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Financial Aid Priority Deadline for Upcoming Term <a href="#fall_note1"><sup>1</sup></a></div>
                              </div>
                           </td>
                           						
                           <td>21-Jul-17</td>
                           						
                           <td>21-Jul-17</td>
                           						
                           <td>21-Jul-17</td>
                           						
                           <td>21-Jul-17</td>
                           						
                           <td>21-Jul-17</td>
                           						
                           <td>21-Jul-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Application Priority Deadline Baccalaureate  Degree</div>
                              </div>
                           </td>
                           						
                           <td>28-Jul-17</td>
                           						
                           <td>28-Jul-17</td>
                           						
                           <td>28-Jul-17</td>
                           						
                           <td>28-Jul-17</td>
                           						
                           <td>28-Jul-17</td>
                           						
                           <td>28-Jul-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Fourth Attempt (AA/AS) &amp;Third Attempt (BS/ATC) Appeal Deadline</div>
                              </div>
                           </td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Suspension Readmission Request Deadline</div>
                              </div>
                           </td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           						
                           <td>04-Aug-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Application Priority Deadline Associate Degree</div>
                              </div>
                           </td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Fee Payment Deadlilne:  FRIDAY (5:00 P.M.) <a href="#fall_note2"><sup>2</sup></a></div>
                              </div>
                           </td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Financial Aid SAP Appeal Priority Deadline</div>
                              </div>
                           </td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           						
                           <td>11-Aug-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Late Registration Begins -- Fees Assessed With Initial Enrollment</div>
                              </div>
                           </td>
                           						
                           <td>12-Aug-17</td>
                           						
                           <td>12-Aug-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>12-Aug-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>N/A</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Faculty Report</div>
                              </div>
                           </td>
                           						
                           <td>22-Aug-17</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Senior Citizen and State Employee Registration Begins</div>
                              </div>
                           </td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>28-Sep-17</td>
                           						
                           <td>21-Sep-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Academic Assembly (College Wide)</div>
                              </div>
                           </td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>24-Aug-17</td>
                           						
                           <td>24-Aug-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Assisted Registration Begins</div>
                              </div>
                           </td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>20-Oct-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>02-Oct-17</td>
                           						
                           <td>25-Sep-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-home fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Florida Residency Criteria Must Be Met By This Date</div>
                              </div>
                           </td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="fas fa-shield-check fa-fw fa-lg v-red"></span></div>
                                 <div class="col-sm-11">Day &amp;Evening Classes Begin (First Day of Classes for each Term)</div>
                              </div>
                           </td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>20-Oct-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>02-Oct-17</td>
                           						
                           <td>25-Sep-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-home fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Documentation of Florida Residency Due By This Date</div>
                              </div>
                           </td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>20-Oct-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>02-Oct-17</td>
                           						
                           <td>25-Sep-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Full Cost of Instruction Appeal Deadline</div>
                              </div>
                           </td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>20-Oct-17</td>
                           						
                           <td>28-Aug-17</td>
                           						
                           <td>02-Oct-17</td>
                           						
                           <td>25-Sep-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Drop/Refund Deadline (11:59 p.m. )</div>
                              </div>
                           </td>
                           						
                           <td>05-Sep-17</td>
                           						
                           <td>05-Sep-17</td>
                           						
                           <td>27-Oct-17</td>
                           						
                           <td>05-Sep-17</td>
                           						
                           <td>09-Oct-17</td>
                           						
                           <td>02-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Credit to Audit Deadline</div>
                              </div>
                           </td>
                           						
                           <td>05-Sep-17</td>
                           						
                           <td>05-Sep-17</td>
                           						
                           <td>27-Oct-17</td>
                           						
                           <td>05-Sep-17</td>
                           						
                           <td>09-Oct-17</td>
                           						
                           <td>02-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">No Show Reporting Period</div>
                              </div>
                           </td>
                           						
                           <td>6-Sep-17 to<br>15-Sep-17
                           </td>
                           						
                           <td>6-Sep-17 to<br>15-Sep-17
                           </td>
                           						
                           <td>28-Oct to<br>6-Nov-17
                           </td>
                           						
                           <td>6-Sep-17 to<br>15-Sep-17
                           </td>
                           						
                           <td>10-Oct-17 to<br> 19-Oct-17
                           </td>
                           						
                           <td>3 Oct-12 Oct</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Graduation Application Deadline</div>
                              </div>
                           </td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Change of Program of Study Deadline for Current Semester</div>
                              </div>
                           </td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           						
                           <td>15-Sep-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">College Night at Osceola <a href="#fall_note3"><sup>3</sup></a> (No classes Lake Nona, Osceola, or Poinciana)
                                 </div>
                              </div>
                           </td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>10-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">College Night at East <a href="#fall_note3"><sup>3</sup></a> (No classes East, West, or Winter Park)
                                 </div>
                              </div>
                           </td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>12-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Spirit Day</div>
                              </div>
                           </td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>19-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Veteran's Affairs Deferral Deadline</div>
                              </div>
                           </td>
                           						
                           <td>03-Nov-17</td>
                           						
                           <td>03-Nov-17</td>
                           						
                           <td>03-Nov-17</td>
                           						
                           <td>03-Nov-17</td>
                           						
                           <td>03-Nov-17</td>
                           						
                           <td>03-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Withdrawal Deadline  - "W" Grade (11:59 p.m.)</div>
                              </div>
                           </td>
                           						
                           <td>10-Nov-17</td>
                           						
                           <td>06-Oct-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>13-Oct-17</td>
                           						
                           <td>17-Nov-17</td>
                           						
                           <td>03-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Faculty Final Date to Enter Withdrawal</div>
                              </div>
                           </td>
                           						
                           <td>10-Dec-17</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="fas fa-shield-check fa-fw fa-lg v-red"></span></div>
                                 <div class="col-sm-11">Day and Evening Classes End</div>
                              </div>
                           </td>
                           						
                           <td>10-Dec-17</td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>17-Dec-17</td>
                           						
                           <td>05-Nov-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>19-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-edit fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Final Exams</div>
                              </div>
                           </td>
                           						
                           <td>11-Dec-17 to<br> 17-Dec-17
                           </td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Term Ends</div>
                              </div>
                           </td>
                           						
                           <td>17-Dec-17</td>
                           						
                           <td>19-Oct-17</td>
                           						
                           <td>17-Dec-17</td>
                           						
                           <td>05-Nov-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>19-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Grades Due (9:00 a.m.) Faculty Submission Deadline</div>
                              </div>
                           </td>
                           						
                           <td>18-Dec-17</td>
                           						
                           <td>18-Dec-17</td>
                           						
                           <td>18-Dec-17</td>
                           						
                           <td>18-Dec-17</td>
                           						
                           <td>18-Dec-17</td>
                           						
                           <td>18-Dec-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar-check fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Final Grades Viewable in ATLAS</div>
                              </div>
                           </td>
                           						
                           <td>19-Dec-17</td>
                           						
                           <td>19-Dec-17</td>
                           						
                           <td>19-Dec-17</td>
                           						
                           <td>19-Dec-17</td>
                           						
                           <td>19-Dec-17</td>
                           						
                           <td>19-Dec-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Faculty Work Day (Credit Classes Do Not Meet)  Collegewide</div>
                              </div>
                           </td>
                           						
                           <td>22‑Aug‑17&nbsp;to<br>25-Aug-17
                           </td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Faculty Work Day (Credit Classes Do Not Meet) Osceola, Lake Nona, or Poinciana</div>
                              </div>
                           </td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>10-Oct-17</td>
                           						
                           <td>10-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Faculty Work Day (Credit Classes Do Not Meet) West,East, Winter Park</div>
                              </div>
                           </td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>12-Oct-17</td>
                           						
                           <td>12-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar-times fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Labor Day <br>College Closed (Credit Classes Do Not Meet)
                                 </div>
                              </div>
                           </td>
                           						
                           <td>4-Sept-17</td>
                           						
                           <td>4-Sept-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>4-Sept-17</td>
                           						
                           <td>N/A</td>
                           						
                           <td>N/A</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar-times fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Thanksgiving <br>College Closed (Credit Classes Do Not Meet)
                                 </div>
                              </div>
                           </td>
                           						
                           <td>22-Nov-17 to<br>26-Nov-17 
                           </td>
                           						
                           <td>22-Nov-17 to<br>26-Nov-17 
                           </td>
                           						
                           <td>22-Nov-17 to<br>26-Nov-17 
                           </td>
                           						
                           <td>N/A</td>
                           						
                           <td>22-Nov-17 to<br>26-Nov-17 
                           </td>
                           						
                           <td>N/A</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 <div class="col-sm-1"><span class="far fa-calendar-times fa-fw fa-lg"></span></div>
                                 <div class="col-sm-11">Winter Break <br>College Closed (Credit Classes Do Not Meet)
                                 </div>
                              </div>
                           </td>
                           						
                           <td>21-Dec-17 to<br> 1-Jan-18
                           </td>
                           						
                           <td>N/A</td>
                           						
                           <td>N/A</td>
                           						
                           <td>N/A</td>
                           						
                           <td>N/A</td>
                           						
                           <td>N/A</td>
                           					
                        </tr>
                        
                        				
                     </table>
                     				
                     <p><a id="fall_note1"></a><sup>1</sup>&nbsp;&nbsp;Must meet all eligibility criteria including verification and transcript evaluation.
                     </p>
                     				
                     <p><a id="fall_note2"></a><sup>2</sup>&nbsp;&nbsp;After the fee payment deadline, fees are due the same day as registration.  Failure
                        to pay may result in you being dropped from all your courses.  Ultimately you are
                        responsible for dropping the courses you do not intend to take. If you do not drop
                        courses by the published drop/refund deadline, even if you have an outstanding balance
                        or do not attend class, you will remain responsible for paying for these courses.
                     </p>
                     				
                     <p><a id="fall_note3"></a><sup>3</sup>&nbsp;&nbsp;College Night dates are determined by a state wide committee.  Dates published here
                        are tentative and may be changed once the state finalizes the dates.
                     </p>
                     			
                  </div>
                  			
                  <hr>
                  
                  			<a id="spring"></a>
                  			
                  <div class="main-title">
                     				
                     <h2>Spring 2018 Important Dates</h2>
                     				
                     <p>Revised 10/04/17</p>
                     			
                  </div>
                  			
                  <div>
                     				
                     <table class="table table table-striped">
                        					
                        <tr>
                           <td></td>
                           <th scope="col">FULL TERM 1</th>
                           <th scope="col">1ST 8 WEEKS
                              						H1
                           </th>
                           <th scope="col">2ND 8 WEEKS
                              						H2
                           </th>
                           <th scope="col">1ST 10 WEEKS
                              						TWJ
                           </th>
                           <th scope="col">2ND 10 WEEKS
                              						TWK
                           </th>
                           <th scope="col">MIDDLE 8
                              						WEEKS LSC
                           </th>
                        </tr>
                        
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Advanced Registration Begins Returning Students</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>31-Oct-17</td>
                           						
                           <td>31-Oct-17</td>
                           						
                           <td>31-Oct-17</td>
                           						
                           <td>31-Oct-17</td>
                           						
                           <td>31-Oct-17</td>
                           						
                           <td>31-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Advanced Registration Begins New Students</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>07-Nov-17</td>
                           						
                           <td>07-Nov-17</td>
                           						
                           <td>07-Nov-17</td>
                           						
                           <td>07-Nov-17</td>
                           						
                           <td>07-Nov-17</td>
                           						
                           <td>07-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Open Registration Begins  New and Returning Students</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>10-Nov-17</td>
                           						
                           <td>10-Nov-17</td>
                           						
                           <td>10-Nov-17</td>
                           						
                           <td>10-Nov-17</td>
                           						
                           <td>10-Nov-17</td>
                           						
                           <td>10-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Interntional Student Application Deadline<br>
                                    								Student-Out of Country
                                 </div>
                                 							
                              </div>
                           </td>
                           						
                           <td>15-Oct-17</td>
                           						
                           <td>15-Oct-17</td>
                           						
                           <td>15-Oct-17</td>
                           						
                           <td>15-Oct-17</td>
                           						
                           <td>15-Oct-17</td>
                           						
                           <td>15-Oct-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">International Student Application Deadline<br>
                                    								Student-In Country
                                 </div>
                                 							
                              </div>
                           </td>
                           						
                           <td>15-Nov-17</td>
                           						
                           <td>15-Nov-17</td>
                           						
                           <td>15-Nov-17</td>
                           						
                           <td>15-Nov-17</td>
                           						
                           <td>15-Nov-17</td>
                           						
                           <td>15-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Financial Aid Priority Deadline for Upcoming Term <a href="#spring_note1"><sup>1</sup></a></div>
                                 							
                              </div>
                           </td>
                           						
                           <td>17-Nov-17</td>
                           						
                           <td>17-Nov-17</td>
                           						
                           <td>17-Nov-17</td>
                           						
                           <td>17-Nov-17</td>
                           						
                           <td>17-Nov-17</td>
                           						
                           <td>17-Nov-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Application Priority Deadline Baccalaureate  Degree</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Fourth Attempt (AA/AS) &amp;Third Attempt (BS/ATC) Appeal Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Suspension Readmission Request Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           						
                           <td>01-Dec-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Application Priority Deadline Associate Degree</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Fee Payment Deadlilne:  FRIDAY (5:00 P.M.) <a href="#spring_note2"><sup>2</sup></a></div>
                                 							
                              </div>
                           </td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           						
                           <td>15-Dec-17</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Late Registration Begins -- Fees Assessed With Initial Enrollment</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>16-Dec-17</td>
                           						
                           <td>16-Dec-17</td>
                           						
                           <td>NA</td>
                           						
                           <td>16-Dec-17</td>
                           						
                           <td>NA</td>
                           						
                           <td>NA</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Financial Aid SAP Appeal Priority Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Faculty Report</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>01-Mar-18</td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>08-Feb-18</td>
                           						
                           <td>18-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Senior Citizen and State Employee Registration Begins</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>04-Jan-18</td>
                           						
                           <td>04-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Assisted Registration Begins</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>01-Mar-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>12-Feb-18</td>
                           						
                           <td>22-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-home fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Florida Residency Criteria Must Be Met By This Date</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="fas fa-shield-check fa-fw fa-lg v-red"></span></div>
                                 							
                                 <div class="col-sm-11">Day &amp;Evening Classes Begin (First Day of Classes for each Term)</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>01-Mar-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>12-Feb-18</td>
                           						
                           <td>22-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-home fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Documentation of Florida Residency Due By This Date</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>01-Mar-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>12-Feb-18</td>
                           						
                           <td>22-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Full Cost of Instruction Appeal Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>01-Mar-18</td>
                           						
                           <td>08-Jan-18</td>
                           						
                           <td>12-Feb-18</td>
                           						
                           <td>22-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Drop/Refund Deadline (11:59 p.m. )</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>16-Jan-18</td>
                           						
                           <td>16-Jan-18</td>
                           						
                           <td>08-Mar-18</td>
                           						
                           <td>16-Jan-18</td>
                           						
                           <td>19-Feb-18</td>
                           						
                           <td>29-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Credit to Audit Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>16-Jan-18</td>
                           						
                           <td>16-Jan-18</td>
                           						
                           <td>08-Mar-18</td>
                           						
                           <td>16-Jan-18</td>
                           						
                           <td>19-Feb-18</td>
                           						
                           <td>29-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">No Show Reporting Period</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>17 Jan-26 Jan</td>
                           						
                           <td>17 Jan-26 Jan</td>
                           						
                           <td>9 Mar-25 Mar</td>
                           						
                           <td>17 Jan-26 Jan</td>
                           						
                           <td>20 Feb-1 Mar</td>
                           						
                           <td>30 Jan-8 Feb</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Graduation Application Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Change of Program of Study Deadline for Current Semester</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           						
                           <td>26-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-calendar fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Learning Day</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>09-Feb-18</td>
                           						
                           <td>09-Feb-18</td>
                           						
                           <td>09-Feb-18</td>
                           						
                           <td>09-Feb-18</td>
                           						
                           <td>NA</td>
                           						
                           <td>09-Feb-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Veteran's Affairs Deferral Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>09-Mar-18</td>
                           						
                           <td>09-Mar-18</td>
                           						
                           <td>09-Mar-18</td>
                           						
                           <td>09-Mar-18</td>
                           						
                           <td>09-Mar-18</td>
                           						
                           <td>09-Mar-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Withdrawal Deadline  - "W" Grade (11:59 p.m.)</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>30-Mar-18</td>
                           						
                           <td>16-Feb-18</td>
                           						
                           <td>13-Apr-18</td>
                           						
                           <td>23-Feb-18</td>
                           						
                           <td>06-Apr-18</td>
                           						
                           <td>02-Mar-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Faculty Final Date to Enter Withdrawal</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>22-Apr-18</td>
                           						
                           <td>28-Feb-18</td>
                           						
                           <td>29-Apr-18</td>
                           						
                           <td>25-Mar-18</td>
                           						
                           <td>29-Apr-18</td>
                           						
                           <td>25-Mar-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="fas fa-shield-check fa-fw fa-lg v-red"></span></div>
                                 							
                                 <div class="col-sm-11">Day and Evening Classes End</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>22-Apr-18</td>
                           						
                           <td>28-Feb-18</td>
                           						
                           <td>29-Apr-18</td>
                           						
                           <td>25-Mar-18</td>
                           						
                           <td>29-Apr-18</td>
                           						
                           <td>25-Mar-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-edit fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Final Exams</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>23 Apr-29 Apr</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           						
                           <td>Last class meeting</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Term Ends</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>29-Apr-18</td>
                           						
                           <td>28-Feb-18</td>
                           						
                           <td>29-Apr-18</td>
                           						
                           <td>25-Mar-18</td>
                           						
                           <td>29-Apr-18</td>
                           						
                           <td>25-Mar-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Grades Due (9:00 a.m.) Faculty Submission Deadline</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>30-Apr-18</td>
                           						
                           <td>30-Apr-18</td>
                           						
                           <td>30-Apr-18</td>
                           						
                           <td>30-Apr-18</td>
                           						
                           <td>30-Apr-18</td>
                           						
                           <td>30-Apr-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-calendar-check fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Final Grades Viewable in ATLAS</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>01-May-18</td>
                           						
                           <td>01-May-18</td>
                           						
                           <td>01-May-18</td>
                           						
                           <td>01-May-18</td>
                           						
                           <td>01-May-18</td>
                           						
                           <td>01-May-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-calendar fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Commencement</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>06-May-18</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           						
                           <td>00-Jan-00</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Faculty Work Day (Credit Classes Do Not Meet)  Collegewide</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>4-Jan-18 to<br>
                              							5-Jan-18
                           </td>
                           						
                           <td>4-Jan-18 to<br>
                              							5-Jan-18
                           </td>
                           						
                           <td>4-Jan-18 to<br>
                              							5-Jan-18
                           </td>
                           						
                           <td>4-Jan-18 to<br>
                              							5-Jan-18
                           </td>
                           						
                           <td>4-Jan-18 to<br>
                              							5-Jan-18
                           </td>
                           						
                           <td>4-Jan-18 to<br>
                              							5-Jan-18
                           </td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Faculty Work Day (Credit Classes Do Not Meet)  Collegewide</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>9-Feb-18</td>
                           						
                           <td>9-Feb-18</td>
                           						
                           <td>9-Feb-18</td>
                           						
                           <td>9-Feb-18</td>
                           						
                           <td>9-Feb-18</td>
                           						
                           <td>9-Feb-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-calendar-times fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">        College Closed (Credit Classes Do Not Meet)</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>15-Jan-18</td>
                           						
                           <td>15-Jan-18</td>
                           						
                           <td>15-Jan-18</td>
                           						
                           <td>15-Jan-18</td>
                           						
                           <td>15-Jan-18</td>
                           						
                           <td>15-Jan-18</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-calendar-times fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">        College Closed (Credit Classes Do Not Meet)</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>12-Mar-18 to<br>
                              							18-Mar-18
                           </td>
                           						
                           <td>12-Mar-18 to<br>
                              							18-Mar-18
                           </td>
                           						
                           <td>12-Mar-18 to<br>
                              							18-Mar-18
                           </td>
                           						
                           <td>12-Mar-18 to<br>
                              							18-Mar-18
                           </td>
                           						
                           <td>12-Mar-18 to<br>
                              							18-Mar-18
                           </td>
                           						
                           <td>12-Mar-18 to<br>
                              							18-Mar-18
                           </td>
                           					
                        </tr>
                        				
                     </table>
                     				
                     <p><a id="spring_note1"></a><sup>1</sup>&nbsp;&nbsp;Must meet all eligibility criteria including verification and transcript evaluation.
                     </p>
                     				
                     <p><a id="spring_note2"></a><sup>2</sup>&nbsp;&nbsp;After the fee payment deadline, fees are due the same day as registration.  Failure
                        to pay may result in you being dropped from all your courses.  Ultimately you are
                        responsible for dropping the courses you do not intend to take. If you do not drop
                        courses by the published drop/refund deadline, even if you have an outstanding balance
                        or do not attend class, you will remain responsible for paying for these courses.
                     </p>
                     			
                  </div>
                  			
                  <hr>
                  
                  			<a id="summer"></a>
                  			
                  <div class="main-title">
                     				
                     <h2>Summer 2018 Important Dates</h2>
                     				
                     <p>Revised 10/04/17</p>
                     			
                  </div>
                  			
                  <div>
                     				
                     <table class="table table table-striped">
                        					
                        <tr>
                           <td></td>
                           <th scope="col">FULL TERM 1</th>
                           <th scope="col">1ST 6 WEEKS
                              						H1
                           </th>
                           <th scope="col">2ND 6 WEEKS
                              						H2
                           </th>
                           <th scope="col">1ST 8 WEEKS
                              						TWJ
                           </th>
                           <th scope="col">2ND 8 WEEKS
                              						TWK
                           </th>
                        </tr>
                        					
                        <tr>
                           						
                           <td>
                              <div class="row">
                                 							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 							
                                 <div class="col-sm-11">Advanced Registration Begins Returning Students</div>
                                 							
                              </div>
                           </td>
                           						
                           <td>20-Feb-18</td>
                           						
                           <td>20-Feb-18</td>
                           						
                           <td>20-Feb-18</td>
                           						
                           <td>20-Feb-18</td>
                           						
                           <td>20-Feb-18</td>
                           							
                        </tr>
                        							
                        <tr>
                           								
                           <td>
                              <div class="row">
                                 									
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 									
                                 <div class="col-sm-11">Advanced Registration Begins New Students</div>
                                 									
                              </div>
                           </td>
                           								
                           <td>27-Feb-18</td>
                           								
                           <td>27-Feb-18</td>
                           								
                           <td>27-Feb-18</td>
                           								
                           <td>27-Feb-18</td>
                           								
                        </tr>
                        								
                        <tr>
                           									
                           <td>
                              <div class="row">
                                 										
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 										
                                 <div class="col-sm-11">Open Registration Begins  New and Returning Students</div>
                                 										
                              </div>
                           </td>
                           									
                           <td>02-Mar-18</td>
                           									
                           <td>02-Mar-18</td>
                           									
                           <td>02-Mar-18</td>
                           									
                           <td>02-Mar-18</td>
                           									
                        </tr>
                        									
                        <tr>
                           										
                           <td>
                              <div class="row">
                                 											
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 											
                                 <div class="col-sm-11">Interntional Student Application Deadline<br>
                                    												Student-Out of Country
                                 </div>
                                 											
                              </div>
                           </td>
                           										
                           <td>15-Mar-18</td>
                           										
                           <td>15-Mar-18</td>
                           										
                           <td>15-Mar-18</td>
                           										
                           <td>15-Mar-18</td>
                           										
                        </tr>
                        										
                        <tr>
                           											
                           <td>
                              <div class="row">
                                 												
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 												
                                 <div class="col-sm-11">International Student Application Deadline<br>
                                    													Student-In Country
                                 </div>
                                 												
                              </div>
                           </td>
                           											
                           <td>15-Apr-18</td>
                           											
                           <td>15-Apr-18</td>
                           											
                           <td>15-Apr-18</td>
                           											
                           <td>15-Apr-18</td>
                           											
                        </tr>
                        											
                        <tr>
                           												
                           <td>
                              <div class="row">
                                 													
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 													
                                 <div class="col-sm-11">Financial Aid Priority Deadline for Upcoming Term <a href="#spring_note1"><sup>1</sup></a></div>
                                 													
                              </div>
                           </td>
                           												
                           <td>23-Mar-18</td>
                           												
                           <td>23-Mar-18</td>
                           												
                           <td>23-Mar-18</td>
                           												
                           <td>23-Mar-18</td>
                           												
                        </tr>
                        												
                        <tr>
                           													
                           <td>
                              <div class="row">
                                 														
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 														
                                 <div class="col-sm-11">Application Priority Deadline Baccalaureate  Degree</div>
                                 														
                              </div>
                           </td>
                           													
                           <td>06-Apr-18</td>
                           													
                           <td>06-Apr-18</td>
                           													
                           <td>06-Apr-18</td>
                           													
                           <td>06-Apr-18</td>
                           													
                        </tr>
                        													
                        <tr>
                           														
                           <td>
                              <div class="row">
                                 															
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 															
                                 <div class="col-sm-11">Fourth Attempt (AA/AS) &amp;Third Attempt (BS/ATC) Appeal Deadline</div>
                                 															
                              </div>
                           </td>
                           														
                           <td>13-Apr-18</td>
                           														
                           <td>13-Apr-18</td>
                           														
                           <td>13-Apr-18</td>
                           														
                           <td>13-Apr-18</td>
                           														
                        </tr>
                        														
                        <tr>
                           															
                           <td>
                              <div class="row">
                                 																
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																
                                 <div class="col-sm-11">Suspension Readmission Request Deadline</div>
                                 																
                              </div>
                           </td>
                           															
                           <td>13-Apr-18</td>
                           															
                           <td>13-Apr-18</td>
                           															
                           <td>13-Apr-18</td>
                           															
                           <td>13-Apr-18</td>
                           															
                        </tr>
                        															
                        <tr>
                           																
                           <td>
                              <div class="row">
                                 																	
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																	
                                 <div class="col-sm-11">Application Priority Deadline Associate Degree</div>
                                 																	
                              </div>
                           </td>
                           																
                           <td>20-Apr-18</td>
                           																
                           <td>20-Apr-18</td>
                           																
                           <td>20-Apr-18</td>
                           																
                           <td>20-Apr-18</td>
                           																
                        </tr>
                        																
                        <tr>
                           																	
                           <td>
                              <div class="row">
                                 																		
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																		
                                 <div class="col-sm-11">Fee Payment Deadlilne:  FRIDAY (5:00 P.M.) <a href="#spring_note2"><sup>2</sup></a></div>
                                 																		
                              </div>
                           </td>
                           																	
                           <td>20-Apr-18</td>
                           																	
                           <td>20-Apr-18</td>
                           																	
                           <td>20-Apr-18</td>
                           																	
                           <td>20-Apr-18</td>
                           																	
                        </tr>
                        																	
                        <tr>
                           																		
                           <td>
                              <div class="row">
                                 																			
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 																			
                                 <div class="col-sm-11">Late Registration Begins -- Fees Assessed With Initial Enrollment</div>
                                 																			
                              </div>
                           </td>
                           																		
                           <td>21-Apr-18</td>
                           																		
                           <td>21-Apr-18</td>
                           																		
                           <td>21-Apr-18</td>
                           																		
                           <td>NA</td>
                           																		
                        </tr>
                        																		
                        <tr>
                           																			
                           <td>
                              <div class="row">
                                 																				
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																				
                                 <div class="col-sm-11">Financial Aid SAP Appeal Priority Deadline</div>
                                 																				
                              </div>
                           </td>
                           																			
                           <td>04-May-18</td>
                           																			
                           <td>04-May-18</td>
                           																			
                           <td>04-May-18</td>
                           																			
                           <td>04-May-18</td>
                           																			
                        </tr>
                        																			
                        <tr>
                           																				
                           <td>
                              <div class="row">
                                 																					
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 																					
                                 <div class="col-sm-11">Faculty Report</div>
                                 																					
                              </div>
                           </td>
                           																				
                           <td>03-May-18</td>
                           																				
                           <td>&nbsp;</td>
                           																				
                           <td>&nbsp;</td>
                           																				
                           <td>&nbsp;</td>
                           																				
                        </tr>
                        																				
                        <tr>
                           																					
                           <td>
                              <div class="row">
                                 																						
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 																						
                                 <div class="col-sm-11">Senior Citizen and State Employee Registration Begins</div>
                                 																						
                              </div>
                           </td>
                           																					
                           <td>03-May-18</td>
                           																					
                           <td>03-May-18</td>
                           																					
                           <td>03-May-18</td>
                           																					
                           <td>31-May-18</td>
                           																					
                        </tr>
                        																					
                        <tr>
                           																						
                           <td>
                              <div class="row">
                                 																							
                                 <div class="col-sm-1"><span class="far fa-sign-in fa-fw fa-lg"></span></div>
                                 																							
                                 <div class="col-sm-11">Assisted Registration Begins</div>
                                 																							
                              </div>
                           </td>
                           																						
                           <td>07-May-18</td>
                           																						
                           <td>07-May-18</td>
                           																						
                           <td>07-May-18</td>
                           																						
                           <td>04-Jun-18</td>
                           																						
                        </tr>
                        																						
                        <tr>
                           																							
                           <td>
                              <div class="row">
                                 																								
                                 <div class="col-sm-1"><span class="far fa-home fa-fw fa-lg"></span></div>
                                 																								
                                 <div class="col-sm-11">Florida Residency Criteria Must Be Met By This Date</div>
                                 																								
                              </div>
                           </td>
                           																							
                           <td>07-May-18</td>
                           																							
                           <td>07-May-18</td>
                           																							
                           <td>07-May-18</td>
                           																							
                           <td>07-May-18</td>
                           																							
                        </tr>
                        																							
                        <tr>
                           																								
                           <td>
                              <div class="row">
                                 																									
                                 <div class="col-sm-1"><span class="fas fa-shield-check fa-fw fa-lg v-red"></span></div>
                                 																									
                                 <div class="col-sm-11">Day &amp;Evening Classes Begin (First Day of Classes for each Term)</div>
                                 																									
                              </div>
                           </td>
                           																								
                           <td>07-May-18</td>
                           																								
                           <td>07-May-18</td>
                           																								
                           <td>07-May-18</td>
                           																								
                           <td>04-Jun-18</td>
                           																								
                        </tr>
                        																								
                        <tr>
                           																									
                           <td>
                              <div class="row">
                                 																										
                                 <div class="col-sm-1"><span class="far fa-home fa-fw fa-lg"></span></div>
                                 																										
                                 <div class="col-sm-11">Documentation of Florida Residency Due By This Date</div>
                                 																										
                              </div>
                           </td>
                           																									
                           <td>07-May-18</td>
                           																									
                           <td>07-May-18</td>
                           																									
                           <td>07-May-18</td>
                           																									
                           <td>04-Jun-18</td>
                           																									
                        </tr>
                        																									
                        <tr>
                           																										
                           <td>
                              <div class="row">
                                 																											
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																											
                                 <div class="col-sm-11">Full Cost of Instruction Appeal Deadline</div>
                                 																											
                              </div>
                           </td>
                           																										
                           <td>07-May-18</td>
                           																										
                           <td>07-May-18</td>
                           																										
                           <td>07-May-18</td>
                           																										
                           <td>04-Jun-18</td>
                           																										
                        </tr>
                        																										
                        <tr>
                           																											
                           <td>
                              <div class="row">
                                 																												
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																												
                                 <div class="col-sm-11">Drop/Refund Deadline (11:59 p.m. )</div>
                                 																												
                              </div>
                           </td>
                           																											
                           <td>14-May-18</td>
                           																											
                           <td>14-May-18</td>
                           																											
                           <td>14-May-18</td>
                           																											
                           <td>11-Jun-18</td>
                           																											
                        </tr>
                        																											
                        <tr>
                           																												
                           <td>
                              <div class="row">
                                 																													
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																													
                                 <div class="col-sm-11">Credit to Audit Deadline</div>
                                 																													
                              </div>
                           </td>
                           																												
                           <td>14-May-18</td>
                           																												
                           <td>14-May-18</td>
                           																												
                           <td>14-May-18</td>
                           																												
                           <td>11-Jun-18</td>
                           																												
                        </tr>
                        																												
                        <tr>
                           																													
                           <td>
                              <div class="row">
                                 																														
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 																														
                                 <div class="col-sm-11">No Show Reporting Period</div>
                                 																														
                              </div>
                           </td>
                           																													
                           <td>15 May-24 May</td>
                           																													
                           <td>15 May-24 May</td>
                           																													
                           <td>15 May-24 May</td>
                           																													
                           <td>12 Jun-21 Jun</td>
                           																													
                        </tr>
                        																													
                        <tr>
                           																														
                           <td>
                              <div class="row">
                                 																															
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																															
                                 <div class="col-sm-11">Graduation Application Deadline</div>
                                 																															
                              </div>
                           </td>
                           																														
                           <td>25-May-18</td>
                           																														
                           <td>25-May-18</td>
                           																														
                           <td>25-May-18</td>
                           																														
                           <td>25-May-18</td>
                           																														
                        </tr>
                        																														
                        <tr>
                           																															
                           <td>
                              <div class="row">
                                 																																
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																																
                                 <div class="col-sm-11">Change of Program of Study Deadline for Current Semester</div>
                                 																																
                              </div>
                           </td>
                           																															
                           <td>25-May-18</td>
                           																															
                           <td>25-May-18</td>
                           																															
                           <td>25-May-18</td>
                           																															
                           <td>25-May-18</td>
                           																															
                        </tr>
                        																															
                        <tr>
                           																																
                           <td>
                              <div class="row">
                                 																																	
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																																	
                                 <div class="col-sm-11">Veteran's Affairs Deferral Deadline</div>
                                 																																	
                              </div>
                           </td>
                           																																
                           <td>06-Jul-18</td>
                           																																
                           <td>06-Jul-18</td>
                           																																
                           <td>06-Jul-18</td>
                           																																
                           <td>06-Jul-18</td>
                           																																
                        </tr>
                        																																
                        <tr>
                           																																	
                           <td>
                              <div class="row">
                                 																																		
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 																																		
                                 <div class="col-sm-11">Withdrawal Deadline  - "W" Grade (11:59 p.m.)</div>
                                 																																		
                              </div>
                           </td>
                           																																	
                           <td>06-Jul-18</td>
                           																																	
                           <td>08-Jun-18</td>
                           																																	
                           <td>15-Jun-18</td>
                           																																	
                           <td>13-Jul-18</td>
                           																																	
                        </tr>
                        																																	
                        <tr>
                           																																		
                           <td>
                              <div class="row">
                                 																																			
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 																																			
                                 <div class="col-sm-11">Faculty Final Date to Enter Withdrawal</div>
                                 																																			
                              </div>
                           </td>
                           																																		
                           <td>31-Jul-18</td>
                           																																		
                           <td>18-Jun-18</td>
                           																																		
                           <td>01-Jul-18</td>
                           																																		
                           <td>29-Jul-18</td>
                           																																		
                        </tr>
                        																																		
                        <tr>
                           																																			
                           <td>
                              <div class="row">
                                 																																				
                                 <div class="col-sm-1"><span class="fas fa-shield-check fa-fw fa-lg v-red"></span></div>
                                 																																				
                                 <div class="col-sm-11">Day and Evening Classes End</div>
                                 																																				
                              </div>
                           </td>
                           																																			
                           <td>31-Jul-18</td>
                           																																			
                           <td>18-Jun-18</td>
                           																																			
                           <td>01-Jul-18</td>
                           																																			
                           <td>29-Jul-18</td>
                           																																			
                        </tr>
                        																																			
                        <tr>
                           																																				
                           <td>
                              <div class="row">
                                 																																					
                                 <div class="col-sm-1"><span class="far fa-edit fa-fw fa-lg"></span></div>
                                 																																					
                                 <div class="col-sm-11">Final Exams</div>
                                 																																					
                              </div>
                           </td>
                           																																				
                           <td>Last class meeting</td>
                           																																				
                           <td>Last class meeting</td>
                           																																				
                           <td>Last class meeting</td>
                           																																				
                           <td>Last class meeting</td>
                           																																				
                        </tr>
                        																																				
                        <tr>
                           																																					
                           <td>
                              <div class="row">
                                 
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 
                                 <div class="col-sm-11">Term Ends</div>
                                 
                              </div>
                           </td>
                           																																					
                           <td>31-Jul-18</td>
                           																																					
                           <td>18-Jun-18</td>
                           																																					
                           <td>01-Jul-18</td>
                           																																					
                           <td>29-Jul-18</td>
                           																																					
                        </tr>
                        																																					
                        <tr>
                           
                           <td>
                              <div class="row">
                                 	  
                                 <div class="col-sm-1"><span class="far fa-hourglass-end fa-fw fa-lg"></span></div>
                                 	  
                                 <div class="col-sm-11">Grades Due (9:00 a.m.) Faculty Submission Deadline</div>
                                 	  
                              </div>
                           </td>
                           
                           <td>01-Aug-18</td>
                           
                           <td>01-Aug-18</td>
                           
                           <td>01-Aug-18</td>
                           
                           <td>02-Aug-18</td>
                           
                        </tr>
                        
                        <tr>
                           	
                           <td>
                              <div class="row">
                                 		
                                 <div class="col-sm-1"><span class="far fa-calendar-check fa-fw fa-lg"></span></div>
                                 		
                                 <div class="col-sm-11">Final Grades Viewable in ATLAS</div>
                                 		
                              </div>
                           </td>
                           	
                           <td>03-Aug-18</td>
                           	
                           <td>03-Aug-18</td>
                           	
                           <td>03-Aug-18</td>
                           	
                           <td>03-Aug-18</td>
                           	
                        </tr>
                        	
                        <tr>
                           		
                           <td>
                              <div class="row">
                                 			
                                 <div class="col-sm-1"><span class="far fa-flag fa-fw fa-lg"></span></div>
                                 			
                                 <div class="col-sm-11">Faculty Work Day (Credit Classes Do Not Meet)  Collegewide</div>
                                 			
                              </div>
                           </td>
                           		
                           <td>3-4 May</td>
                           		
                           <td>3-4 May</td>
                           		
                           <td>01-Aug-18</td>
                           		
                           <td>&nbsp;</td>
                           		
                        </tr>
                        		
                        <tr>
                           			
                           <td>
                              <div class="row">
                                 				
                                 <div class="col-sm-1"><span class="far fa-calendar-times fa-fw fa-lg"></span></div>
                                 				
                                 <div class="col-sm-11"> College Closed (Credit Classes Do Not Meet)</div>
                                 				
                              </div>
                           </td>
                           			
                           <td>28-May-18</td>
                           			
                           <td>28-May-18</td>
                           			
                           <td>28-May-18</td>
                           			
                           <td>N/A</td>
                           			
                        </tr>
                        			
                        <tr>
                           				
                           <td>
                              <div class="row">
                                 					
                                 <div class="col-sm-1"><span class="far fa-calendar-times fa-fw fa-lg"></span></div>
                                 					
                                 <div class="col-sm-11"> College Closed (Credit Classes Do Not Meet)</div>
                                 					
                              </div>
                           </td>
                           				
                           <td>04-Jul-18</td>
                           				
                           <td>N/A</td>
                           				
                           <td>N/A</td>
                           				
                           <td>04-Jul-18</td>
                           				
                        </tr>
                        				
                     </table>
                     
                     			
                     <p><a id="summer_note1"></a><sup>1</sup>&nbsp;&nbsp;Must meet all eligibility criteria including verification and transcript evaluation.
                     </p>
                     			
                     <p><a id="summer_note2"></a><sup>2</sup>&nbsp;&nbsp;After the fee payment deadline, fees are due the same day as registration.  Failure
                        to pay may result in you being dropped from all your courses.  Ultimately you are
                        responsible for dropping the courses you do not intend to take. If you do not drop
                        courses by the published drop/refund deadline, even if you have an outstanding balance
                        or do not attend class, you will remain responsible for paying for these courses.
                     </p>
                     			
                  </div>
                  		
                  <hr>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/calendar/important-dates-17-18.pcf">©</a>
      </div>
   </body>
</html>