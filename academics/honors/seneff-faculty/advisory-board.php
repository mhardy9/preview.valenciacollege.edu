<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/seneff-faculty/advisory-board.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/seneff-faculty/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/seneff-faculty/">Seneff Faculty</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        <h2>Honors Advisory Board</h2>
                        
                        <p>The Honors Advisory Board is composed of Valencia faculty, staff members, and representatives
                           from the Honors Student Advisory Committee. The board meets at least once each semester
                           to make policy decisions and recommendations concerning the program. 
                        </p>
                        
                        
                        <h3>2017 - 18 Membership</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>Dr. Nicholas Bekas </td>
                                 
                                 <td>Dean of Academic Affairs, West Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Karen Borglum </td>
                                 
                                 <td>Assistant Vice President, Curriculum &amp; Assessment</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>B. Clyburn </td>
                                 
                                 <td>Honors Program Assistant </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Kera Coyer</td>
                                 
                                 <td>Honors Program Advisor</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Susan Dunn </td>
                                 
                                 <td>Manager, Credit Programs, Winter Park</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Michelle Foster </td>
                                 
                                 <td>Dean of Academic Affairs, East Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Hannah Hosler </td>
                                 
                                 <td>Program&nbsp; Advisor, Winter Park Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Shara Lee </td>
                                 
                                 <td>Campus Director, Faculty and Instructional Development </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jane Maguire </td>
                                 
                                 <td>Honors Coordinator, East Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Melissa Pedone </td>
                                 
                                 <td>Dean of Mathematics and Science, Osceola Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Cheryl Robinson </td>
                                 
                                 <td>Honors Director </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Marcia Roman </td>
                                 
                                 <td>Counselor, Osceola Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Travis Rodgers </td>
                                 
                                 <td>Coordinator, Interdisciplinary Studies Program, West Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Ylliany Santana</td>
                                 
                                 <td>Honors Student Advisory Committee President </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Jill Szentmiklosi </td>
                                 
                                 <td>Dean of Students, Osceola Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Travis Taylor</td>
                                 
                                 <td>Academic Advisor, West Campus </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jennifer Washick </td>
                                 
                                 <td>Academic Advisor, East Campus </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <h3>Documents</h3>
                        
                        <ul>
                           
                           <li><a href="documents/2015-16SeneffHonorsCollegeAnnualReport.docx">Valencia Honors Annual Report, 2015-16</a></li>
                           
                           <li><a href="documents/ValenciaHonorsAnnualReport2013-2014.pdf" target="_blank">Valencia Honors Annual Report, 2013-2014 </a></li>
                           
                           <li><a href="documents/ValenciaHonorsAnnualReport2012-2013.pdf" target="_blank">Valencia Honors Annual Report, 2012-2013</a></li>
                           
                           <li><a href="documents/ValenciaHonorsAnnualReport2011-2012.pdf" target="_blank">Valencia Honors Annual Report, 2011-2012</a></li>
                           
                           <li>
                              <a href="documents/ValenciaHonorsAnnualReport2010-2011.pdf" target="_blank">Valencia Honors Annual Report, 2010-2011</a> 
                           </li>
                           
                           <li><a href="documents/ValenciaHonorsAnnualReport2009-2010.pdf" target="_blank">Valencia Honors Annual Report, 2009-2010</a></li>
                           
                        </ul>             
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/seneff-faculty/advisory-board.pcf">©</a>
      </div>
   </body>
</html>