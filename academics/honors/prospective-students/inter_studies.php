<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/prospective-students/inter_studies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/prospective-students/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/prospective-students/">Prospective Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Interdisciplinary Studies Track - West Campus</h2>
                        
                        <p>The <strong>Interdisciplinary Studies Track </strong>is designed to create an integrative, holistic education experience that empowers
                           a global and diverse perspective.
                        </p>
                        
                        <p>Students who complete the requirements of the Interdisciplinary Studies track will
                           be able to:
                        </p>
                        
                        <ul>
                           
                           <li>Apply principles of dialogic argument in written and verbal communication.</li>
                           
                           <li>Compare, contrast, and explain the significance of different historic ages.</li>
                           
                           <li>Compare, contrast, and integrate knowledge of diverse cultures and disciplines.</li>
                           
                        </ul>
                        
                        <p>The Interdisciplinary Studies Honors curriculum (IDH) covers 24 hours of the student's
                           general education requirement by integrating the disciplines of English, humanities,
                           the social sciences, and the natural sciences. Students can take all or part of the
                           program.
                        </p>
                        
                        <p>Interdisciplinary Studies is team-taught by four faculty members from several academic
                           disciplines, and provides students with a strong background for further college work.
                           Courses in the IDH Program are offered in Fall Term and Spring Term each year.
                        </p>
                        
                        
                        <h3>Curricular Outline</h3>
                        
                        <p>Students must complete a total of 24 credits of honors course work, at least 18 of
                           which must be in three of the following IDH courses:
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/idh/">IDH&nbsp;1111</a></td>
                                 
                                 <td>INTERDISCIPLINARY STUDIES IN GENERAL EDUCATION II - HONORS (Offered only Spring Term)
                                    <sup>1</sup>
                                    
                                 </td>
                                 
                                 <td>6</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/idh/">IDH&nbsp;2120</a></td>
                                 
                                 <td>INTERDISCIPLINARY STUDIES IN GENERAL EDUCATION III - HONORS (Offered only Fall Term)
                                    <sup>2</sup>
                                    
                                 </td>
                                 
                                 <td>6</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/idh/">IDH&nbsp;2121</a></td>
                                 
                                 <td>INTERDISCIPLINARY STUDIES IN GENERAL EDUCATION IV - HONORS (Offered only Spring Term)
                                    <sup>3</sup>
                                    
                                 </td>
                                 
                                 <td>6</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="2">Select one of the following:</td>
                                 
                                 <td>6</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/idh/">IDH&nbsp;1110</a></td>
                                 
                                 <td colspan="2">INTERDISCIPLINARY STUDIES IN GENERAL EDUCATION I - HONORS (Offered only Fall Term)
                                    <sup>4</sup>
                                    
                                 </td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/idh/">IDH&nbsp;1112</a></td>
                                 
                                 <td colspan="2">INTERDISCIPLINARY STUDIES IN GENERAL EDUCATION - HONORS (Offered only Fall Term)</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="3">Total Credit Hours: <strong>24</strong>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <p><sup>1</sup> <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/">ENC&nbsp;1102</a> FRESHMAN COMPOSITION II and Core Science
                        </p>
                        
                        <p><sup>2</sup> Institutional Social Science (GR) and Science (core or institutional)
                        </p>
                        
                        <p><sup>3</sup> Institutional Humanities (GR) and Core Social Science
                        </p>
                        
                        <p><sup>4</sup> <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/">ENC&nbsp;1101</a> FRESHMAN COMPOSITION I and Core Humanities
                        </p>
                        
                        <p><sup>(GR)</sup> Denotes a Gordon Rule course.
                        </p>
                        
                        
                        <h3>Co-Curricular Component</h3>
                        
                        <p>Students will be required to participate in a minimum of 15 hours of approved co-curricular
                           activities each term of enrollment (excluding summer). This may include international
                           travel experiences, field trips to cultural locations, colloquia, reading circles,
                           speaker series, and/or service to the Honors College.
                        </p>            
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/prospective-students/inter_studies.pcf">©</a>
      </div>
   </body>
</html>