<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/prospective-students/undergraduateresearch.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/prospective-students/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/prospective-students/">Prospective Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        <h2>Undergraduate Research Track   - East Campus</h2>
                        
                        
                        <p>The <strong>Undergraduate Research  Track </strong>is designed to create students who are familiar with the process, practice, and principles
                           of scholarly inquiry in an academic community.
                        </p>
                        
                        
                        <h3>Program Outcomes</h3>
                        
                        <ol>
                           
                           <li>Complete a discipline-specific research project.</li>
                           
                           <li>Present research in a peer-reviewed, academic setting.</li>
                           
                        </ol>
                        
                        <h3>Curricular Outline</h3>
                        
                        <p>To satisfy the curricular aspect of the Undergraduate Research Track, students must
                           complete:
                        </p>
                        
                        <ol>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">IDH 2911</a>-&nbsp;Honors Research Process&nbsp;(2 cr)&nbsp;- This honors course will introduce students to the
                              process of research, tools, concepts, and resources necessary to search, evaluate,
                              and use information in a variety of formats and subject disciplines. The focus will
                              be to analyze and utilize information critically using a broad range of materials
                              and interdisciplinary concepts needed for honors research and academic/professional
                              success.
                           </li>
                           
                           <li>Individual Honors Study Plan &nbsp;(12-15 cr)&nbsp;– honors courses designed to enhance the
                              individual student’s research plan, developed in consultation with the Honors Director
                              and a Faculty Advisor. Approved study plans are kept on file in the Honors Office.
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">IDH 2912</a>- Honors Project&nbsp;(1 cr)&nbsp;– the student will complete under the guidance of a faculty
                              advisor. The project will define and execute a research question. Guidelines will
                              be established regarding format, standards, and review of projects.
                           </li>
                           
                           <li>Additional Honors Coursework&nbsp;(6-9 cr)</li>
                           
                        </ol>
                        
                        
                        <h3>Co-Curricular Component</h3>
                        
                        <p>Students will be required to participate in a minimum of 15 hours of approved co-curricular
                           activities per term of enrollment (excluding summer). This may include participation
                           in the editing and publication of a Valencia Honors research journal, presentation
                           of original research at local, regional, and national honors and/or discipline conferences,
                           and/or presentation of original research at Valencia.
                        </p>            
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/prospective-students/undergraduateresearch.pcf">©</a>
      </div>
   </body>
</html>