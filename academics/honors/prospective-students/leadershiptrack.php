<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/prospective-students/leadershiptrack.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        <h2>Leadership Track  - Osceola Campus</h2>
                        
                        <p>The <strong>Leadership Track </strong>is designed to create citizen scholars -- individuals whose action is informed by
                           theory. Students completing this track will graduate with a <a href="/students/service-learning/index.html">Service Learning</a> recognition.
                        </p>
                        
                        
                        <h3>Program Outcomes</h3>
                        
                        <ul>
                           
                           <li>Develop a working, evolving and individual leadership theory or style.</li>
                           
                           <li>Practice leadership principles through their campus activities and continued community
                              involvement.
                           </li>
                           
                           <li>Identify opportunities to make social change.</li>
                           
                        </ul>
                        
                        
                        <h3>Curricular Outline</h3>
                        
                        <p>To satisfy the curricular aspect of the Leadership Program, students must complete
                           the following courses:
                        </p>
                        
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">SLS&nbsp;2261H</a></td>
                                 
                                 <td>LEADERSHIP DEVELOPMENT - HONORS</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">SLS&nbsp;2940H</a></td>
                                 
                                 <td>SERVICE LEARNING</td>
                                 
                                 <td>2</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>or&nbsp;<a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">SLS&nbsp;2941</a>
                                    
                                 </td>
                                 
                                 <td colspan="2">ORGANIZATIONAL LEADERSHIP/INTERNSHIP EXPLORATION</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="2">Select at least two of the following:</td>
                                 
                                 <td>6</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/">SPC&nbsp;1608H</a></td>
                                 
                                 <td colspan="2">FUNDAMENTALS OF SPEECH - HONORS or </td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/">SPC 1017H</a></td>
                                 
                                 <td colspan="2">INTERPERSONAL COMMUNICATION - HONORS </td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/">ENC&nbsp;1101H</a></td>
                                 
                                 <td colspan="2">FRESHMAN COMPOSITION I - HONORS</td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/">ENC&nbsp;1102H</a></td>
                                 
                                 <td colspan="2">FRESHMAN COMPOSITION II - HONORS</td>
                                 
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>Humanities</td>
                                 
                                 <td colspan="2">See&nbsp;<a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Gen. Ed.</a>&nbsp;Institutional Hours&nbsp;<a href="http://catalog.valenciacollege.edu/degrees/associateinarts/gordonrule/">(GR)</a>&nbsp;+*~
                                 </td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Humanities</td>
                                 
                                 <td colspan="2">See&nbsp;<a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Gen. Ed.</a>&nbsp;Institutional Hours<a href="http://catalog.valenciacollege.edu/degrees/associateinarts/gordonrule/">&nbsp;(GR)</a>&nbsp;+*~
                                 </td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="2">Select at least one of the following:</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/amh/">AMH&nbsp;2020H</a></td>
                                 
                                 <td colspan="2">UNITED STATES HISTORY 1877 TO PRESENT - HONORS&nbsp;~</td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/euh/">EUH 2001H</a></td>
                                 
                                 <td colspan="2">MODERN WESTERN CIVILIZATION - HONORS </td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/search/?P=INR%202002Hhttp%3A//catalog.valenciacollege.edu/coursedescriptions/coursesoffered/inr/">INR&nbsp;2002H</a></td>
                                 
                                 <td colspan="2">INTERNATIONAL POLITICS HONORS&nbsp;+*~</td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/pos/">POS&nbsp;2041H</a></td>
                                 
                                 <td colspan="2">U.S. GOVERNMENT - HONORS&nbsp;~</td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/psy">PSY&nbsp;2012H</a></td>
                                 
                                 <td colspan="2">GENERAL PSYCHOLOGY - HONORS&nbsp;~</td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/syg/">SYG&nbsp;2000H</a></td>
                                 
                                 <td colspan="2">INTRODUCTORY SOCIOLOGY- HONORS.&nbsp;~</td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="2">Additional honors work (students are encouraged to work with an advisor to choose
                                    honors courses that fulfill general education and prerequisite requirements for their
                                    intended majors.
                                 </td>
                                 
                                 <td>9</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>Note:&nbsp;At least 23 credits as outlined above are needed to earn an honors diploma.</p>
                        
                        <h3>Co-Curricular Component</h3>
                        
                        <p>Students will be required to participate in a minimum of 15 hours of approved co-curricular
                           activities each term of enrollment (excluding summer). This may include community
                           service, campus leadership, involvement with Phi Theta Kappa activities, mentoring
                           programs, speaker series, and/or service to the Honors College.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/prospective-students/leadershiptrack.pcf">©</a>
      </div>
   </body>
</html>