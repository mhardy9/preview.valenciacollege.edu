<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <img alt="Inaugural Class" height="322" src="SeneffInauguralClassResized.JPG" width="490">
                        
                        
                        <p>Inaugural Class of the Seneff Honors College</p>
                        
                        <p>“I will act as if what I do will make a difference.” <br>
                           <em>William James</em></p>
                        
                        <p>Aim higher. Become a part of the new Seneff Honors College at Valencia, offering four
                           distinct paths to an honors degree. This program is for students who want more from
                           their college experience—more challenges, more opportunities, and more connections
                           with fellow students and great professors. The Seneff Honors College is for people
                           with a passion for learning. 
                        </p>
                        
                        <p>Valencia offers this and more, all in a setting that nurtures the whole individual.
                           
                        </p>
                        
                        <ul>
                           
                           <li>smaller classes special scholarships</li>
                           
                           <li>recognition at commencement 
                              
                           </li>
                           
                           <li>special service learning opportunities</li>
                           
                           <li>co-curricular events that contribute to learning and the community</li>
                           
                           <li>scholarships</li>
                           
                           <li>domestic and international travel</li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <a href="prospective-students/howtoapply.html" target="_blank" class="button_action_outline">How To Apply</a>
                        
                        
                        
                        <center>
                           <strong>Spring 2018 Application deadline: November 3rd, 2017</strong><p> For admission criteria, visit <br><a href="prospective-students/index.html">Prospective Students</a><br> For more information, contact <br>the Seneff Honors College at 407-582-1729 <strong>or <a href="mailto:honors@valenciacollege.edu">honors@valenciacollege.edu</a> </strong></p>
                           
                        </center>
                        
                        <strong>
                           
                           <div id="fb-root"></div>
                           
                           
                           
                           <div class="fb-like-box" data-href="https://www.facebook.com/ValenciaHonors" data-width="245" data-show-faces="false" data-stream="false" data-header="false"></div>
                           
                           </strong></aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/default.pcf">©</a>
      </div>
   </body>
</html>