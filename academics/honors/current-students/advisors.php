<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/current-students/advisors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/current-students/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/current-students/">Current Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        <h2>Honors Advisors</h2>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Advisor</th>
                                 
                                 <th scope="col">Students/Track</th>
                                 
                                 <th scope="col">Location</th>
                                 
                                 <th scope="col">E-mail</th>
                                 
                                 <th scope="col">Phone</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><strong>Kera Coyer </strong></td>
                                 
                                 <td>
                                    
                                    <p>All Honors Students - Collegewide Advisor </p>
                                    
                                    <p><em>See appointment information below </em></p>
                                    
                                 </td>
                                 
                                 <td>West 3-136 </td>
                                 
                                 <td><a href="mailto:kcoyer@valenciacollege.edu">kcoyer@valenciacollege.edu</a></td>
                                 
                                 <td>407-582-1442</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><strong>Jennifer Washick <br>
                                       </strong></td>
                                 
                                 <td>Undergraduate Research </td>
                                 
                                 <td>East 5-211</td>
                                 
                                 <td><a href="mailto:jwashick@valenciacollege.edu">jwashick@valenciacollege.edu</a></td>
                                 
                                 <td>407-582-2453</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    <p><strong>Travis Taylor</strong></p>
                                 </td>
                                 
                                 <td>Interdisciplinary Studies </td>
                                 
                                 <td>
                                    <p>West SSB-110</p>
                                 </td>
                                 
                                 <td>
                                    <p><a href="mailto:ttaylor78@mail.valenciacollege.edu" rel="noreferrer">ttaylor78@mail.valenciacollege.edu</a></p>
                                 </td>
                                 
                                 <td>
                                    <p>407-582-1027</p>
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><strong>Gwendolyn Noel </strong></td>
                                 
                                 <td>Leadership</td>
                                 
                                 <td>Lake Nona 149 </td>
                                 
                                 <td>
                                    <p><a href="mailto:Gnoel1@valenciacollege.edu">gnoel1@valenciacollege.edu</a></p>
                                 </td>
                                 
                                 <td>407-582-7132</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    <p><strong>Dr. Marcia Roman </strong></p>
                                 </td>
                                 
                                 <td>Leadership</td>
                                 
                                 <td>
                                    <p>Osceola 2-140</p>
                                 </td>
                                 
                                 <td>
                                    <p><a href="mailto:mroman20@valenciacollege.edu">mroman20@valenciacollege.edu</a></p>
                                 </td>
                                 
                                 <td>
                                    <p>407-582-4152</p>
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><strong>Hannah Hosler</strong></td>
                                 
                                 <td>Global Studies </td>
                                 
                                 <td>Winter Park 207</td>
                                 
                                 <td><a href="mailto:hhosler@valenciacollege.edu">hhosler@valenciacollege.edu</a></td>
                                 
                                 <td>407-582-6882</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>If you're interested in booking an advising appointment with Kera Coyer, Honors Program
                           Advisor (college-wide), you can do so via her scheduling calendar! Appointments are
                           available at East, Osceola, West and Winter Park Campuses.
                        </p>
                        
                        <p>Appointments can be booked up to 45 days in advance, and will be offered as follows:</p>
                        
                        <ul>
                           
                           <li>West Campus (primary office)</li>
                           
                           <li>East Campus (2 days a month)</li>
                           
                           <li>Osceola Campus (2 days a month)</li>
                           
                           <li>Winter Park Campus (1 day a month)</li>
                           
                        </ul>
                        
                        <p>Please <a href="http://bit.ly/SHCAdvising">check the online calendar</a> for available days and times at your preferred location.
                        </p>
                        
                        <p>For appointments with your track-based Honors Advisor, please contact them directly.</p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/current-students/advisors.pcf">©</a>
      </div>
   </body>
</html>