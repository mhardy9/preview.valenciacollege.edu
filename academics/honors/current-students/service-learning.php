<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/current-students/service-learning.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/current-students/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/current-students/">Current Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Service Learning</h2>
                        <img alt="Honors Students in the Dominican Republic" height="332" src="/_resources/img/admissions/honors-program/current-students/service-learning.JPG" width="250" class="img-responsive pull-right">
                        
                        <p>
                           Service Learning is a teaching and learning strategy that integrates meaningful community
                           service with instruction and reflection to enrich the learning experience, teach civic
                           responsibility, and strengthen communities. Service Learning allows students and faculty
                           to blend educational goals with their passions.  Honors students can earn honors credit
                           for Service Learning experience and have the opportunity to explore interdisciplinary
                           connections between different fields of study and thought.  For more information,
                           visit the <a href="/STUDENTS/service-learning/index.html">Service Learning page</a>.  If you are interested in Honors Service Learning, contact the Honors College at
                           <a href="mailto:honors@valenciacollege.edu">honors@valenciacollege.edu</a>. 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/current-students/service-learning.pcf">©</a>
      </div>
   </body>
</html>