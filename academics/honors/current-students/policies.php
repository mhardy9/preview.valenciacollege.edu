<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/current-students/policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/current-students/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/current-students/">Current Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Seneff Honors College </h2>
                        
                        <h3>Policies</h3>
                        
                        <p>If you were admitted to the program prior to May 7, 2012, <a href="documents/2011-2012_Valencia_Honors_Policies.pdf" target="_blank">click here</a> for policies.
                        </p>
                        
                        
                        <h3>Seneff Honors College Honor Code</h3>
                        
                        <p>Honors students are responsible for adhering to the Seneff Honors College Honor Code.
                           The Seneff Honors College Honor Code outlines student expectations and procedures
                           for sanctioning if student expectations are violated.
                        </p>
                        
                        <p><a href="documents/Seneff-Honors-College-Honor-Code.pdf">Seneff Honors College Honor Code</a></p>
                        
                        
                        <h3>Graduation </h3>
                        
                        <h4>Requirements </h4>
                        
                        <p>Honors students will be offered three levels of graduation distinction: </p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>The term <strong>SENEFF HONORS COLLEGE SCHOLAR</strong> is reserved for those students who successfully complete the curricular requirements
                                 of one of the program tracks, who earn no less than a "C" in each honors class*, who
                                 graduate with a cumulative overall GPA of at least 3.5, and who complete three approved
                                 co-curricular activities each term of enrollment (fall and spring).
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The term <strong>VALENCIA HONORS SCHOLAR</strong> is reserved for those students who successfully complete at least 18 hours of honors
                                 coursework, who earn no less than a "C" in each honors class*, who graduate with a
                                 cumulative overall GPA of at least 3.33, and who complete three approved co-curricular
                                 activities each term of enrollment (fall and spring).
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Students who successfully complete at least 12 hours of honors coursework, who earn
                                 no less than a "C" in each honors class*, and who maintain a cumulative GPA of at
                                 least 3.25 will earn an <strong>HONORS CERTIFICATE</strong>. * <a href="http://catalog.valenciacollege.edu/academicpoliciesprocedures/courseattemptscoursewithdrawal/#repeatedcoursesgradeforgiveness" target="_blank">See college grade forgiveness policy</a>. At the Commencement Ceremony, honors graduates walk first in the A.A. student processional
                                 and wear stoles, cords, medallions, and/or tassels depending on the program they complete.
                                 
                              </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p>To qualify for honors graduation, students must complete both a regular Valencia College
                           graduation application and a <a href="http://net4.valenciacollege.edu/forms/honors/gradapp.cfm" target="_blank">Seneff Honors College graduation application</a> by the published deadline. For more about Valencia College graduation, visit the
                           <a href="/students/graduation/">Graduation webpage</a>.
                           
                        </p>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/honors/gradapp.cfm" target="_blank">Access the Graduation Application</a>.
                        </p>
                        
                        
                        <h3>Honors Registration</h3>
                        
                        <p>A student who fails to take an honors class for two consecutive sessions (not including
                           summer term) will be inactivated.  Students who have been inactivated for non-enrollment
                           may request to be reactivated so long as they enroll in an honors course and have
                           a minimum overall GPA of 3.0.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/current-students/policies.pcf">©</a>
      </div>
   </body>
</html>