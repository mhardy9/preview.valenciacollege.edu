<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/current-students/honors-calendar.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/current-students/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/current-students/">Current Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Honors Calendar</h2>
                        
                        <p>Students in the Seneff Honors College are required to do three approved co-curricular
                           activities each term and, along with taking honors classes and earning a high Grade
                           Point Average, are a part of the requirement to graduate with an honors diploma. You
                           can search the available co-curricular options at valenciacollege.edu/honors/currentstudents/
                           honors-calendar.cfm. The calendar will show the title and location of the activity,
                           along with the number of hours that activity will earn. In order to earn credit for
                           participating in the activity, you must complete the Co-curricular Activity Confirmation
                           form located on the honors calendar website (see above). The form should be completed
                           within 7 days of the activity to make sure you keep an up-to-date list of your co-curricular
                           activities, but must be submitted before the term ends.
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><a href="https://www.google.com/calendar/embed?src=honors%40valenciacollege.edu&amp;ctz=America/New_York" target="_blank">Honors Co-Curricular Calendar </a></td>
                                 
                                 <td>
                                    
                                    <p>Contains a full list of co-curricular activities and other upcoming program events.
                                       To count toward the 3 activity per term requirement, co-curricular activities must
                                       be completed by the last day of class for the full term (not the final exam period).
                                       
                                    </p>
                                    
                                    <p>Some events are offered more than one time (plays, resource fairs, etc.). You can
                                       only receive credit for attending an event one time - even if the event is repeated.
                                       You cannot receive duplicate credit. 
                                    </p>
                                    
                                    <p>Many of these events are offered by other departments or programs at Valencia. We
                                       include them to provide a wide array of options. Time and location are posted as submitted
                                       to us by the departments.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_2sDhjZb3WXhYl0N" target="_blank">Co-curricular Activity Confirmation Form </a></td>
                                 
                                 <td>This form should be completed within <em>7 days</em> of a calendar event to earn co-curricular credit. To count toward the 3 activity
                                    per term requirement, co-curricular activities must be completed by the last day of
                                    class for the full term (not the final exam period). 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    <a href="documents/Non-CalendarEventForm_000.pdf" target="_blank">Co-curricular Form for Non-calendar Events</a> 
                                 </td>
                                 
                                 <td>
                                    
                                    <p>This form should be completed within <em>7 days</em> of an event to request co-curricular credit for events not listed on the honors calendar.
                                       Prior approval from your Track Coordinator is required. The Track Coordinators are:<br>
                                       <strong>Global Studies</strong> - Dr. Susan Dunn (<a href="mailto:sdunn18@valenciacollege.edu">sdunn18@valenciacollege.edu</a>)<br>
                                       <strong>Interdisciplinary Studies</strong> - Dr. Travis Rodgers (<a href="mailto:trodgers6@valenciacollege.edu">trodgers6@valenciacollege.edu</a>)<br>
                                       <strong>Leadership</strong> - Dr. Melissa Pedone (<a href="mailto:mpedone@valenciacollege.edu">mpedone@valenciacollege.edu</a>)<br>
                                       <strong>Undergraduate Research</strong> - Prof. Jane Maguire (<a href="mailto:jmaguire1@valenciacollege.edu">jmaguire1@valenciacollege.edu</a>)
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_3jusAzr9JsVHWjX" target="_blank">Request to Add Event to Honors Calendar </a></td>
                                 
                                 <td>Complete this form if you know of an event that you think should be added to the honors
                                    calendar. 
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <a class="button_action_outline" href="../prospective-students/howtoapply.html" target="_blank">How To Apply</a>
                        
                        
                        
                        <center>
                           <strong>Spring 2018 Application deadline: November 3rd, 2017</strong><p> For admission criteria, visit <br><a href="../prospective-students/index.html">Prospective Students</a><br> For more information, contact <br>the Seneff Honors College at 407-582-1729 <strong>or <a href="mailto:honors@valenciacollege.edu">honors@valenciacollege.edu</a> </strong></p>
                           
                        </center>
                        
                        <strong>
                           
                           <div id="fb-root"></div>
                           
                           
                           
                           <div class="fb-like-box" data-header="false" data-href="https://www.facebook.com/ValenciaHonors" data-show-faces="false" data-stream="false" data-width="245"></div>
                           
                           </strong></aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/current-students/honors-calendar.pcf">©</a>
      </div>
   </body>
</html>