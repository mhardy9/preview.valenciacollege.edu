<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/honors/current-students/advisory-committee.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/honors-program/current-students/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/honors/">Honors Program</a></li>
               <li><a href="/academics/honors/current-students/">Current Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        <h2>Honors Student Advisory Committee</h2>
                        
                        <p>The mission of the Honors Student Advisory Committee (HSAC) shall be to promote academic
                           excellence among honors students at Valencia. The HSAC shall seek to foster communication,
                           to plan events, to make decisions, to develop partnerships, and to coordinate outreach
                           with the sole goal of enriching the academic experience and quality of education for
                           honors students at all of Valencia's campuses.
                        </p>
                        
                        <p>The authority of the HSAC shall be to act as the sole advisory and communication body
                           representing Valencia honors students on issues relating to academics, fellowship,
                           cultural enrichment, and the honors student activities budget. It is the aim of the
                           HSAC to provide a voice to all active members of the Valencia Honors Program in all
                           matters that directly impact the same.
                        </p>
                        
                        <p>Membership in the HSAC is open at all times. All Valencia honors students are invited
                           and welcome to attend any and all public meetings of the HSAC so long as they meet
                           the aforementioned criteria. Students are welcome to attend as many, or as few, of
                           the HSAC meetings as they so choose.
                        </p>
                        
                        <p>For the next meeting, please visit the <a href="https://www.google.com/calendar/embed?src=valenciahonors%40gmail.com&amp;ctz=America/New_York" target="_blank">honors calendar</a>. Questions? Contact 2017-18 HSAC President, Ylliany Santana, at <a href="mailto:HSACPresident@valenciacollege.edu">HSACPresident@valenciacollege.edu</a>. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/honors/current-students/advisory-committee.pcf">©</a>
      </div>
   </body>
</html>