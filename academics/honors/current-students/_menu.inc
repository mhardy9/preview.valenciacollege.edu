<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://net1.valenciacollege.edu/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../index.php">Seneff Honors College</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3">
<div class="col-md-4"><ul>
<a href="../prospective-students/index.php"><h3>Prospective Students</h3></a><li><a href="../prospective-students/program.php">Honors Options</a></li>
<li><a href="../prospective-students/admission.php">Admission</a></li>
<li><a href="../prospective-students/finaid.php">Scholarships</a></li>
<li><a href="../prospective-students/benefits.php">Benefits</a></li>
<li><a href="../prospective-students/transfer.php">Transfer</a></li>
<li><a href="../prospective-students/faqs.php">FAQs</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="index.php"><h3>Current Students</h3></a><li><a href="advisors.php">Advising</a></li>
<li><a href="finaid.php">Scholarships</a></li>
<li><a href="study-abroad.php">Study Abroad</a></li>
<li><a href="service-learning.php">Service Learning</a></li>
<li><a href="advisory-committee.php">Honors Student Advisory Committee</a></li>
<li><a href="policies.php">Policies</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/honors/gradapp.cfm" target="_blank">Graduation Application</a></li>
<li><a href="honors-calendar.php">Honors Co-Curricular Calendar</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="../seneff-faculty/index.php"><h3>Honors Faculty</h3></a><li><a href="../seneff-faculty/development-program.php"> Seneff Faculty Development Program</a></li>
<li><a href="../seneff-faculty/advisory-board.php"> Honors Advisory Board</a></li>
<li><a href="../connect.php">Connect</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/honors/contact.cfm" target="_blank">Contact Us</a></li>
</ul></div>
</div>
</li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
