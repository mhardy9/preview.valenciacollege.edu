<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Information  | Valencia College</title>
      <meta name="Description" content="LinC provides several benefits to faculty, such as professional development, peer-to-peer connections across academic disciplines, development of engagement strategies to help at-risk students, and engaging with academic and non-academic resources.">
      <meta name="Keywords" content="faculty, professional, development, learning, communities, linc, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/learning-in-community/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/learning-in-community/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Learning in Community</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/learning-in-community/">Learning In Community</a></li>
               <li>Faculty Information </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Faculty Information</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Faculty Benefits</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Professional development</li>
                              
                              <li>Peer to peer connections across academic disciplines and Student Affairs</li>
                              
                              <li>Develop engagement strategies to help at-risk students</li>
                              
                              <li>Engage with academic and non-academic resources</li>
                              
                              <li>Additional monetary compensation for teaching LinC</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Professional Development</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>LinC: Integrating a High Impact Practice (INDV3259)</h4>
                           
                           <p><strong>Course description:</strong> Learning communities are regarded as a high impact educational practice that has
                              shown to be beneficial to college students from many backgrounds. This course will
                              review the foundation of Valencia's LinC initiative, along with providing the necessary
                              guidelines and strategies to build community in the classroom and implement integrated
                              learning with two or more joined courses. The course will help you design a joint
                              syllabus with a faculty partner from another discipline and partner with a Success
                              Coach to support student learning.
                           </p>
                           
                           <p><strong>Registration:</strong> Faculty are required to complete a 1 hour consultation with The Office of Curriculum
                              Initiatives prior to registering for the course. Please contact learningcommunities@valenciacollege.edu
                              to set up your consultation today.
                           </p>
                           
                           
                           <h4>Destination 2016 (Registration Closed)</h4>
                           
                           <p><strong>Track Name:</strong> Learning in Your Community: Effective Practices for Curriculum Integration
                           </p>
                           
                           <p><strong>Facilitators:</strong> Robyn Brighton, Christy Cheney and Joshua Guillemette
                           </p>
                           
                           <p><strong>Track Description:</strong> Learning communities is regarded as a high impact educational practice that has shown
                              to be beneficial for college students from many backgrounds. One of the key goals
                              of a learning community is to develop an environment that fosters collaboration and
                              interdisciplinary learning. Participants will see this teaching and learning practice
                              has shown to increase student's success in college and can be applied to build effective
                              and efficient pathways to degree completion. Additionally, participants will discover
                              how learning in partnership with the local community and engaging in on-campus activities
                              can increase student retention and develop personal connection.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Getting Involved</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Step 1: Contact <a href="learningcommunities@valenciacollege.edu">learningcommunities@valenciacollege.edu</a> to set up a 1 hour meeting.
                              </li>
                              
                              <li>Step 2: Submit a Schedule Request Form (see 'Resources' tab).</li>
                              
                              <li>Step 3: Complete the LinC development course, <em>LinC: Integrating a High Impact Practice (INDV3259)</em> prior to teaching a LinC.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Connections to Strategic Planning</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>2008-2013 Strategic Plan — Goal One: Build Pathways</h4>
                           
                           <p>Objective 1.2: Persistence - Increase the percentage of students who persist at Valencia
                              through key academic thresholds.
                           </p>
                           
                           
                           <h4>2008-2013 Strategic Plan — Goal Two: Learning Assured</h4>
                           
                           <p>Objective 2.1: Learning Outcomes - Develop, align, and review program learning outcomes
                              to assure a cohesive curricular and co-curricular experience that enhances student
                              learning.
                           </p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Resources</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/academics/learning-in-community/LinC-Schedule-Request-Form.pdf" target="_blank">Schedule Request Form - East Campus</a></li>
                              
                              <li><a href="/documents/academics/learning-in-community/LinC-Schedule-Request-Form-East.pdf" target="_blank">Schedule Request Form - All Other Campuses</a></li>
                              
                              <li><a href="/documents/academics/learning-in-community/Integrated-Lesson-Template.pdf" target="_blank">Integrated Lesson Template</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Upcoming Courses</h3>
                           <a href="http://net5.valenciacollege.edu/schedule/?_ga=2.48628064.798479015.1494874286-67023854.1466708865" class="button btn-block text-center">Course Search</a>
                           
                        </div>
                        
                        <hr class="styled">
                        
                        
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Contact</h3>
                           
                           
                           <address>
                              1768 Park Center Drive<br>
                              Orlando, FL 32835
                              
                           </address>
                           Phone: <a href="tel://4075823896">407-582-3896</a><br>
                           Mail Code: DO-35
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  	
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/learning-in-community/faculty.pcf">©</a>
      </div>
   </body>
</html>