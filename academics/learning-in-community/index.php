<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning in Community (LinC)  | Valencia College</title>
      <meta name="Description" content="Valencia College offers a variety of learning communities for students to participate in. Learning communities are regarded as a High Impact Educational Practices and have shown to increase student's retention and success in college. Students often build stronger bonds with their peers, including classmates, faculty and staff at the college because they spend more time together.">
      <meta name="Keywords" content="learning, communities, linc, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/learning-in-community/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/learning-in-community/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Learning in Community</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Learning In Community</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>About Learning in Community (LinC)</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>What is LinC?</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <p>Valencia College offers a variety of learning communities for students to participate
                              in. Some of the most popular models include <a href="https://valenciacollege.edu/REACH/">REACH</a>, <a href="/documents/academics/learning-in-community/Camino-Report-2015.pdf" target="_blank">CAMINO</a>, <a href="first-30.html">First 30</a>, and LinC. Learning communities are regarded as a <a href="http://www.aacu.org/leap/hips">High Impact Educational Practices</a> and have shown to increase student's retention and success in college. The key goals
                              for learning communities are to encourage integration of learning across courses and
                              to involve students with "big questions" that matter beyond the classroom (AAC&amp;U,
                              2008). Students often build stronger bonds with their peers, including classmates,
                              faculty and staff at the college because they spend more time together.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>LinC Program Components</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The following components distinguish LinC from other learning communities offered
                              at the college. In most cases, LinCs are team taught by two faculty that integrate
                              their course curriculum throughout the term. One cohort of students enroll in both
                              courses and are provided academic and non-academic support through Learning Support
                              and Student Affairs offices.
                           </p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>Faculty completion of the LinC development course: LinC: Integrating a High Impact
                                 Practice
                              </li>
                              
                              <li>Same cohort of students in both courses</li>
                              
                              <li>Integrated curriculum throughout the semester</li>
                              
                              <li>Team taught courses (optional)</li>
                              
                              <li>Success Coach (optional)</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Institutional Effectiveness</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Making data-informed decisions is critical to the planning of LinC offerings. The
                              Office of Curriculum Initiatives works along-side the Offices of <a href="http://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a> and <a href="http://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a> to collect and evaluate data on LinC and generate reports.
                           </p>
                           
                           
                           <p>In 2015, LinC completed the Academic Initiative Review (AIR) process. The AIR is an
                              evaluative review process that was developed drawing on related research literature
                              (Grayson, 2012) and prevalent tools in the field. Each year at least one initiative
                              at the college undertakes an AIR in order to better understand the impact of the work
                              and to inform decisions and plans being made college-wide.
                           </p>
                           
                           
                           <p>Click the links below to access reports on LinC.</p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/academics/learning-in-community/AIR-Report-2015.pdf" target="_blank">2015 Academic Initiative Review (AIR) Report</a></li>
                              
                              <li><a href="/documents/academics/learning-in-community/LinC-Research-Report-2009.pdf" target="_blank">2009 LinC Research Report</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Upcoming Courses</h3>
                           <a href="http://net5.valenciacollege.edu/schedule/?_ga=2.48628064.798479015.1494874286-67023854.1466708865" class="button btn-block text-center">Course Search</a>
                           
                        </div>
                        
                        <hr class="styled">
                        
                        
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Contact</h3>
                           
                           
                           <address>
                              1768 Park Center Drive<br>
                              Orlando, FL 32835
                              
                           </address>
                           Phone: <a href="tel://4075823896">407-582-3896</a><br>
                           Mail Code: DO-35
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  	
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/learning-in-community/index.pcf">©</a>
      </div>
   </body>
</html>