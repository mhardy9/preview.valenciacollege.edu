<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>REACH  | Valencia College</title>
      <meta name="Description" content="R.E.A.C.H. (Reaching Every Academic Challenge Head-On) consists of a small group of students who take their first year of college together, with the same courses and instructors. This sort of learning community helps students connect with their classmates and professors, providing more individualized instruction, increased student success, and higher graduation rates.">
      <meta name="Keywords" content="REACH, osceola, learning, communities, linc, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/learning-in-community/reach.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/learning-in-community/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Learning in Community</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/learning-in-community/">Learning In Community</a></li>
               <li>REACH </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>Reaching Every Academic Challenge Head-On (REACH)</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>About REACH</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>R.E.A.C.H. (Reaching Every Academic Challenge Head-On) consists of a small group of
                              students who take their first year of college together, with the same courses and
                              instructors. This sort of learning community helps students connect with their classmates
                              and professors, providing more individualized instruction, increased student success,
                              and higher graduation rates.
                           </p>
                           
                           
                           <h4>REACH Program Components</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Faculty obtain a certificate of completion from the LinC development course</li>
                              
                              <li>The same cohort of students in four courses and over multiple terms</li>
                              
                              <li>Faculty integrate curriculum across disciplines</li>
                              
                              <li>Faculty sit in on each other's classes and team teach</li>
                              
                              <li>Co-curricular integrated within the curriculum</li>
                              
                              <li>Community-based learning</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-download"></i>
                           
                           <h3>Sample Schedules</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Coming Soon!</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Osceola Campus</li>
                              
                           </ul>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Upcoming Courses</h3>
                           <a href="http://net5.valenciacollege.edu/schedule/?_ga=2.48628064.798479015.1494874286-67023854.1466708865" class="button btn-block text-center">Course Search</a>
                           
                        </div>
                        
                        <hr class="styled">
                        
                        
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Contact</h3>
                           
                           
                           <address>
                              1768 Park Center Drive<br>
                              Orlando, FL 32835
                              
                           </address>
                           Phone: <a href="tel://4075823896">407-582-3896</a><br>
                           Mail Code: DO-35
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  	
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/learning-in-community/reach.pcf">©</a>
      </div>
   </body>
</html>