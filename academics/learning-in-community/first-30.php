<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>First 30  | Valencia College</title>
      <meta name="Description" content="The First 30 model offers students a clear academic pathway and a plan to graduate in their college career. Students are guaranteed courses for their declared major and are pre-registered in thirty college-level credits throughout the first three terms. Through co-curricular and community-based learning, students engage in real-world application of content to strengthen their academic, pre-professional and personal learning goals.">
      <meta name="Keywords" content="first, 30, thirty, learning, communities, linc, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/learning-in-community/first-30.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/learning-in-community/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Learning in Community</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/learning-in-community/">Learning In Community</a></li>
               <li>First 30 </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <h2>First 30</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>About First 30</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>In 2016, Valencia College developed and implemented a new learning community model
                              called First 30. The First 30 model offers students a clear academic pathway and a
                              plan to graduate in their college career. Students are guaranteed courses for their
                              declared major and are pre-registered in thirty college-level credits throughout the
                              first three terms. Through co-curricular and community-based learning, students engage
                              in real-world application of content to strengthen their academic, pre-professional
                              and personal learning goals.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>First 30 Program Components</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Program Components</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Faculty obtain a certificate of completion from the LinC development course</li>
                              
                              <li>The same cohort of students in four courses and over multiple terms</li>
                              
                              <li>Faculty integrate curriculum across disciplines</li>
                              
                              <li>Faculty sit in on each other's classes and team teach</li>
                              
                              <li>Co-curricular integrated within the curriculum</li>
                              
                              <li>Community-based learning</li>
                              
                              <li>Success Coach - Program Advisor</li>
                              
                           </ul>  
                           
                           
                           <h4>Program Outcomes</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Develop clear and efficient pathways to learning and educational progress for students</li>
                              
                              <li>Implement optimal learning environments for students</li>
                              
                              <li>Establish learning and learning support systems and techniques that will reduce achievement
                                 gabs among groups of learners from diverse backgrounds
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Student Benefits</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Added support for students who opt out of developmental education courses</li>
                              
                              <li>Pre-registered for courses in their first year</li>
                              
                              <li>Completion of 30 college-level courses after year one</li>
                              
                              <li>Integrated opportunities to engage with the community</li>
                              
                              <li>Cohort model increases peer to peer support</li>
                              
                              <li>LinC courses show to increase student success in Start Right courses</li>
                              
                           </ul>           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Requirements</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Not previously taken courses offered through First 30</li>
                              
                              <li>Commit to taking all courses during the fall, spring, and summer terms that are outlined
                                 on the schedule
                              </li>
                              
                              <li>Meet with the program coordinator assigned to the First 30 track</li>
                              
                              <li>Complete an approved college placement test (eg. PERT, SAT, ACT)</li>
                              
                              <li>Attend the First 30 orientation</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in"> <i class="pe-7s-download"></i>
                           
                           <h3>Sample Schedules</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Program flyers for 2017 are coming soon!</p>
                           
                           
                           <h4>East Campus:</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Business Cohort</li>
                              
                              <li>STEM Cohort</li>
                              
                           </ul>
                           
                           
                           <h4>West Campus:</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Social Behavioral Sciences &amp; Human Services Cohort</li>
                              
                              <li>Business Cohort</li>
                              
                           </ul>  
                           
                           <p>NOTE: If you are registering for the First 30 cohort please contact <a href="mailto:learningcommunities@valenciacollege.edu">learningcommunities@valenciacollege.edu</a>.
                           </p>
                           
                        </div>
                        
                        
                        
                        <div class="wrapper_indent">
                           <a href="http://net4.valenciacollege.edu/forms/linc/first30.cfm" class="button btn-block text-center">Apply Here!</a>
                           
                        </div>
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Upcoming Courses</h3>
                           <a href="http://net5.valenciacollege.edu/schedule/?_ga=2.48628064.798479015.1494874286-67023854.1466708865" class="button btn-block text-center">Course Search</a>
                           
                        </div>
                        
                        <hr class="styled">
                        
                        
                        
                        
                        <div class="box_side">
                           
                           <h3 class="add_bottom_30">Contact</h3>
                           
                           
                           <address>
                              1768 Park Center Drive<br>
                              Orlando, FL 32835
                              
                           </address>
                           Phone: <a href="tel://4075823896">407-582-3896</a><br>
                           Mail Code: DO-35
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  	
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/learning-in-community/first-30.pcf">©</a>
      </div>
   </body>
</html>