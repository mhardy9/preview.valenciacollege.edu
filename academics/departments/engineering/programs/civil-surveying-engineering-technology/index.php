<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Civil/Surveying Engineering Technology | Valencia College</title>
      <meta name="Description" content="Civil/Surveying Engineering Technology">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/civil-surveying-engineering-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Civil Surveying Engineering Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Civil/Surveying Engineering Technology</h2>
                        				
                        <p>The program has been planned to provide theoretical and classroom experience which
                           closely parallels on-the-job activities. The Civil/Surveying Engineering Technology
                           Associate in Science degree simulates job situations found in a civil engineering
                           and land surveying offices. This program applies to the design of highways, airports,
                           water control systems, and field layout of property boundary lines, subdivision, residential
                           building construction, and commercial building construction.
                        </p>
                        				
                        <h3>Associate in Science Degree</h3>
                        				
                        <ul>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/civilsurveyingengineeringtechnology/#text">Catalog Information</a></li>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/civilsurveyingengineeringtechnology/#programrequirementstext">Program Requirements</a></li>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/civilsurveyingengineeringtechnology/#coursedescriptionstext">Course Descriptions</a></li>
                           				
                        </ul>
                        				
                        <h3>Links</h3>
                        				
                        <ul>
                           <!-- 						<li><a href="http://preview.valenciacollege.edu/academics/programs/as-degree/engineering/cset.php">A.S. Degree Program Overview</a></li> -->
                           					
                           <li><a href="/students/future-students/degree-options/associates/civil-surveying-engineering-technology/">A.S. Degree Program Overview</a></li>
                           				
                        </ul>
                        				
                        <p>&nbsp;</p>
                        				
                        <p><strong>Career Program Advisor:</strong><span><span>&nbsp;</span>Beverly Johnson</span></p>			
                        
                        
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/civil-surveying-engineering-technology/index.pcf">©</a>
      </div>
   </body>
</html>