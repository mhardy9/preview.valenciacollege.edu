<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Articulated Pre-Major: Engineering Associate in Arts Degree | Valencia College</title>
      <meta name="Description" content="Articulated Pre-Major: Engineering Associate in Arts Degree">
      <meta name="Keywords" content="college, school, educational, engineering, articulation, associate">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/premajor-engineering/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Pre-Major: Engineering</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Articulated Pre-Major: Engineering Associate in Arts Degree</h2>
                        
                        <!-- cdavis73 -->
                        
                        <p><strong>Career Program Advisor:</strong> Charles Davis
                        </p>
                        
                        <p>This pre-major is designed for the student who plans to transfer to one of the Universities
                           listed below as a junior to complete a four-year Bachelor's degree in the College
                           of Engineering. They are individually based upon articulation agreements in Engineering
                           with the listed University. Students who plan to transfer are responsible for completing
                           the admission requirements of the individual University.
                        </p>
                        
                        <h3>Associate in Arts Degree</h3>
                        <strong> Articulated Pre-Major: Engineering at Florida Institute of Technology</strong>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringfloridainstituteoftechnology/#text">Catalog Information</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringfloridainstituteoftechnology/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringfloridainstituteoftechnology/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        <br> <strong> Articulated Pre-Major: Engineering at Polytechnic University of Puerto Rico, Orlando
                           Campus</strong>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringpolytechnicuniversity/#text">Catalog Information</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringpolytechnicuniversity/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringpolytechnicuniversity/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        <br> <strong> Articulated Pre-Major: Engineering at University of Central Florida</strong>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofcentralflorida/#text">Catalog Information</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofcentralflorida/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofcentralflorida/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        <br> <strong> Articulated Pre-Major: Engineering at University of Miami</strong>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofmiami/#text">Catalog Information</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofmiami/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofmiami/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        <h3>Documents</h3>
                        
                        <ul>
                           
                           <li><a href="/academics/departments/engineering/documents/engineering-courses.pdf">Engineering Courses</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        <h3>Faculty</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aaltawam">Aref Altawam</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=sbidigaray">Stefan Bidigaray</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mkar">Mohua Kar</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?mkar" target="_blank">http://frontdoor.valenciacollege.edu?mkar</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=hregis">Hank Regis</a></td>
                                 
                                 <td><a href="http://faculty.valenciacollege.edu/hregis/" target="_blank">http://faculty.valenciacollege.edu/hregis/</a><span>&nbsp;</span><br><a href="http://frontdoor.valenciacollege.edu/?hregis" target="_blank">http://frontdoor.valenciacollege.edu?hregis</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        <br>
                        
                        <h3>Adjunct</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=rallen56">Randy Allen</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ccadenasvasquez">Carmen Cadenas Vasquez</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?ccadenasvasquez" target="_blank">http://frontdoor.valenciacollege.edu?ccadenasvasquez</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mmonreal">Myra Monreal</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=kofosu">Kwabena Ofosu</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=rparray">Ravi Parray</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=osolano1">Oscar Solano</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        <br>
                        
                        <h3>Staff</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="220px">Position</th>
                                 
                                 <th width="80px">Phone</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=avercamen">Amanda Assuncao</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=gcamero">Gustavo Camero</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ldickinson1">Lucas Dickinson</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg-9057987041879972469.PNG" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dflorencz">Dylan Florencz</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=fmartinez40">Francisco Martinez</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=msmith351">Max Smith</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cstephan">Caleb Stephan</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=bwindish">Benjamin Windish</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p><a href="http://fesvalencia.com/" target="_blank"><img src="/academics/departments/engineering/images/FES-Valencia.png" alt="Florida Engineering Society -- Valencia Chapter"></a> <a href="http://nsbevalencia.weebly.com/" target="_blank"><img src="/academics/departments/engineering/images/NSBE_ProLogo_Color_FNL.jpg" alt="National Society of Black Engineers"></a></p>
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/premajor-engineering/index.pcf">©</a>
      </div>
   </body>
</html>