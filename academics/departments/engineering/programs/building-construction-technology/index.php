<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Building Construction Technology | Valencia College</title>
      <meta name="Description" content="Building Construction Technology">
      <meta name="Keywords" content="college, school, educational, building, construction, technology">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/building-construction-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Building Construction Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <h3 class="col-md-12">&nbsp;</h3>
                     
                     <h2>Building Construction Technology</h2>
                     
                     <p>The program has been planned to provide theoretical and classroom experience which
                        closely parallels on-the-job activities. The Building Construction program is designed
                        to train competent technicians capable of working with architects, engineers, contractors,
                        building officials and others. The program will accommodate architectural drafting,
                        construction estimators, schedulers, and supervisors, as well as persons just entering
                        the field of construction.
                     </p>
                     
                     <hr>
                     
                     <h3>Associate in Science Degree</h3>
                     
                     <ul>
                        
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/buildingconstructiontechnology/#text">Catalog Information</a></li>
                        
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/buildingconstructiontechnology/#programrequirementstext">Program Requirements</a></li>
                        
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/buildingconstructiontechnology/#coursedescriptionstext">Course Descriptions</a></li>
                        
                     </ul>
                     
                     <h3>Technical Certificates</h3>
                     
                     <ul>
                        
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/buildingconstructiontechnology/#certificatetext" target="_blank">Building Construction Specialist <strong>(T-Certificate)</strong></a></li>
                        
                     </ul>
                     
                     <h3>Links</h3>
                     
                     <ul>
                        
                        <li><a href="http://preview.valenciacollege.edu/asdegrees/engineering/bct.cfm">A.S. Degree Program Overview</a></li>
                        
                     </ul>
                     
                     <p><strong>Career Program Advisor:</strong> Beverly Johnson
                     </p>
                     
                     <hr class="styled_2">
                     
                     <h3>Faculty</h3>
                     
                     <table class="table ">
                        
                        <tbody>
                           
                           <tr>
                              
                              <th width="50px">&nbsp;</th>
                              
                              <th width="200px">Name</th>
                              
                              <th width="300px">Website</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg-3060339189659002295.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aray">Andy Ray</a></td>
                              
                              <td><a href="http://frontdoor.valenciacollege.edu/?aray" target="_blank">http://frontdoor.valenciacollege.edu?aray</a></td>
                              
                           </tr>
                           
                        </tbody>
                        
                     </table>
                     
                     <h3>Adjunct</h3>
                     
                     <table class="table ">
                        
                        <tbody>
                           
                           <tr>
                              
                              <th width="50px">&nbsp;</th>
                              
                              <th width="200px">Name</th>
                              
                              <th width="300px">Website</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=hjames7">Tommy James</a></td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cjones259">Chad Jones</a></td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ilocks">Ira Locks</a></td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=lpowers">Lamar Powers</a></td>
                              
                              <td><a href="http://frontdoor.valenciacollege.edu/?lpowers" target="_blank">http://frontdoor.valenciacollege.edu?lpowers</a></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mshalaby1">Adel Shalaby</a></td>
                              
                              <td>&nbsp;</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dwilliams319">Donald Williams</a></td>
                              
                           </tr>
                           
                        </tbody>
                        
                     </table>
                     
                     <h3>Staff</h3>
                     
                     <table class="table ">
                        
                        <tbody>
                           
                           <tr>
                              
                              <th width="50px">&nbsp;</th>
                              
                              <th width="200px">Name</th>
                              
                              <th width="220px">Position</th>
                              
                              <th width="80px">Phone</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg5764064647377846734.PNG" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                              
                              <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=tdillen">Thomas Dillen</a></td>
                              
                              <td>Lab Supervisor</td>
                              
                              <td>(407) 582-1599</td>
                              
                           </tr>
                           
                        </tbody>
                        
                     </table>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/building-construction-technology/index.pcf">©</a>
      </div>
   </body>
</html>