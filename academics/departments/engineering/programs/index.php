<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Programs | Engineering, Computer Programming, &amp; Technology | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li>Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        					
                        <h2>Programs</h2>
                        					
                        <ul>
                           						
                           <li><a href="premajor-computer-science/">Articulated Pre-Major: Computer Science</a></li>
                           						
                           <li><a href="premajor-engineering/">Articulated Pre-Major: Engineering</a></li>
                           						
                           <li><a href="premajor-information-technology/">Articulated Pre-Major: Information Technology</a></li>
                           						
                           <li><a href="bachelor-electrical-computer-engineering-technology/">Electrical &amp; Computer Engineering Technology - Bachelor of Science</a></li>
                           						
                           <li><a href="building-construction-technology/">Building Construction Technology </a></li>
                           						
                           <li><a href="civil-surveying-engineering-technology/">Civil/Surveying Engineering Technology</a></li>
                           						
                           <li><a href="computer-information-technology/">Computer Information Technology</a></li>
                           						
                           <li><a href="computer-programming-and-analysis/">Computer Programming &amp; Analysis</a></li>
                           						
                           <li><a href="drafting-design-technology/">Drafting &amp; Design Technology</a></li>
                           						
                           <li><a href="electronics-engineering-technology/">Electronics Engineering Technology</a></li>
                           						
                           <li><a href="network-engineering-technology/">Network Engineering Technology</a></li>
                           						
                           <li><a href="energy-management-and-controls-technology/">Energy Management and Controls Technology</a></li>
                           
                           					
                        </ul>
                        
                        
                        				
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>1800 S. Kirkman Road Orlando, FL 32811<br>
                           Building 9 Room 140<br>
                           Phone: 407-582-1904 Fax: 407-582-1900<br><br>
                           
                           <strong>Division Dean: </strong><a href="../../includes/UserInfo.cfm-username=lmacon&amp;program=Computer%20Programming%20and%20Analysis,%20Information%20Technology,%20Computer%20Science%20&amp;position=Program%20Chair%20&amp;specialization=CPA.html"><strong>Dr. Lisa Macon</strong></a>
                           
                           
                        </div>
                        
                        
                        <div><a href="mission-statement.html" title="Division Mission Statement">Division Mission Statement</a></div>
                        
                        
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FvalenciaECPT&amp;width=250&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false"></iframe>
                        
                        <a href="http://ewh.ieee.org/sb/orlando/valencia/" target="_blank"><img src="../../programs/engineering/IEEE-Valencia.png"></a>
                        
                        <iframe allowtransparency="true" frameborder="0" height="70" scrolling="no" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaIEEE%2F&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250"></iframe>
                        
                        <a href="http://fesvalencia.com/" target="_blank"><img alt="Florida Engineering Society - Valencia College Student Chapter" src="../../programs/engineering/FES-Valencia.png" title="Florida Engineering Society - Valencia College Student Chapter"></a>
                        
                        
                        <div><a href="https://www.facebook.com/groups/FESatValenciaCollege" target="_blank" title="FES Facebook Group">FES Facebook Group</a></div>
                        
                        <a href="http://swevalencia.weebly.com" target="_blank"><img alt="http://swevalencia.weebly.com" src="../../programs/engineering/swe.png" title="Society of Women Engineers"></a>
                        
                        <iframe allowtransparency="true" frameborder="0" height="70" scrolling="no" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSWE.valenciacollege&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250"></iframe>
                        
                        <a href="http://nsbevalencia.weebly.com/" target="_blank"><img src="../../programs/engineering/NSBE_ProLogo_Color_FNL.jpg"></a>
                        
                        <iframe allowtransparency="true" frameborder="0" height="70" scrolling="no" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FNSBE-Valencia-College-Student-Chapter-908400825881637%2F&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250"></iframe>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/index.pcf">©</a>
      </div>
   </body>
</html>