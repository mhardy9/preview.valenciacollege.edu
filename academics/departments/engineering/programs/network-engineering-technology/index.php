<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Network Engineering Technology | Valencia College</title>
      <meta name="Description" content="Network Engineering Technology">
      <meta name="Keywords" content="college, school, educational, network, engineering, technology">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/network-engineering-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Network Engineering Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Network Engineering Technology</h2>
                        				
                        <!-- jsowell -->
                        				
                        <p><strong>Career Program Advisor:</strong> Jon Sowell
                        </p>
                        				
                        <h3>Associate in Science Degree</h3>
                        				
                        <ul>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#text" target="_blank">Catalog Information</a></li>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#programrequirementstext" target="_blank">Program Requirements</a></li>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#coursedescriptionstext" target="_blank">Course Descriptions</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/net_cisco.pdf" target="_blank">Cisco Routing and Switching Specialization</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/net-microsoft.pdf" target="_blank">Microsoft Systems Administration Specialization</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/net_cybersecurity.pdf" target="_blank">Cybersecurity and Digital Forensics Specialization </a></li>
                           				
                        </ul>
                        				
                        <h3>Technical Certificates</h3>
                        				
                        <ul>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificatestext" target="_blank">Click for Catalog Information on all Technical Certificates</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/networksupport.pdf" target="_blank">Network Support</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/networkadministration.pdf" target="_blank">Network Administration</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/advancednetworkadministration.pdf" target="_blank">Advanced Network Administration</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/networkinfrastructure.pdf" target="_blank">Network Infrastructure</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/advancednetworkinfrastructure.pdf" target="_blank">Advanced Network Infrastructure</a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/cybersecurity.pdf" target="_blank">Cybersecurity </a></li>
                           					
                           <li><a href="/academics/departments/engineering/programs/network-engineering-technology/documents/digitalforensics.pdf" target="_blank"> Forensics </a></li>
                           				
                        </ul>
                        				
                        <h3>Links</h3>
                        				
                        <ul>
                           					
                           <li><a href="/students/future-students/degree-options/associates/network-engineering-technology/" target="_blank">A.S. Degree Program Overview</a></li>
                           					
                           <li><a href="/academics/programs/as-degree/transfer-agreements.php" target="_blank">Articulation Agreements</a></li>
                           				
                        </ul>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Faculty</h3>
                        				
                        <table class="table ">
                           					
                           <tbody>
                              						
                              <tr>
                                 							
                                 <th width="50px">&nbsp;</th>
                                 							
                                 <th width="200px">Name</th>
                                 							
                                 <th width="300px">Website</th>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg2535583732281861367.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jchisholm2">Jessica Chisholm</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?jchisholm2" target="_blank">http://frontdoor.valenciacollege.edu?jchisholm2</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg1706073828402169845.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aeisler">Andrew Eisler</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?aeisler" target="_blank">http://frontdoor.valenciacollege.edu?aeisler</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg6795758065980834813.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=snakhai">Soheyla Nakhai</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?snakhai" target="_blank">http://frontdoor.valenciacollege.edu?snakhai</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg6465977425858215102.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=wyousif">Wael Yousif</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?wyousif" target="_blank">http://frontdoor.valenciacollege.edu?wyousif</a></td>
                                 						
                              </tr>
                              					
                           </tbody>
                           				
                        </table>
                        				<br>
                        				
                        <h3>Adjunct</h3>
                        				
                        <table class="table ">
                           					
                           <tbody>
                              						
                              <tr>
                                 							
                                 <th width="50px">&nbsp;</th>
                                 							
                                 <th width="200px">Name</th>
                                 							
                                 <th width="300px">Website</th>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=tbower">Thomas Bower</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mciena">Miguel Ciena</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mdaniel11">Maged Daniel</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jdelgadonavarro">John Delgado-Navarro</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=yfawzi">Yahia Fawzi</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cmaitland1">Chris Maitland</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=bmundt">Brad Mundt</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?bmundt" target="_blank">http://frontdoor.valenciacollege.edu?bmundt</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=drehman1">Doug Rehman</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?drehman1" target="_blank">http://frontdoor.valenciacollege.edu?drehman1</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=groberts21">Gerri Roberts</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?groberts21" target="_blank">http://frontdoor.valenciacollege.edu?groberts21</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=overamoronta">Oscar Vera Moronta</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?overamoronta" target="_blank">http://frontdoor.valenciacollege.edu?overamoronta</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ryoung38">Robert Young</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?ryoung38" target="_blank">http://frontdoor.valenciacollege.edu?ryoung38</a></td>
                                 						
                              </tr>
                              					
                           </tbody>
                           				
                        </table>
                        				<br>
                        				
                        <h3>Staff</h3>
                        				
                        <table class="table ">
                           					
                           <tbody>
                              						
                              <tr>
                                 							
                                 <th width="50px">&nbsp;</th>
                                 							
                                 <th width="200px">Name</th>
                                 							
                                 <th width="220px">Position</th>
                                 							
                                 <th width="80px">Phone</th>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=amelendez">Angel Melendez</a></td>
                                 							
                                 <td>Lab Supervisor</td>
                                 							
                                 <td>(407) 582-1983</td>
                                 						
                              </tr>
                              					
                           </tbody>
                           				
                        </table>
                        				
                        <hr class="styled_2">
                        				
                        <p>Valencia is a National Center Of Excellence</p>
                        				<a href="/academics/departments/engineering/programs/network-engineering-technology/documents/fullpagefaxprint.pdf" target="_blank"> <img src="/academics/departments/engineering/programs/network-engineering-technology/images/national-center-of-acad-cert.jpg" alt="Valenica College is designated as a National Center of Academic Excellence in Information Assurance 2 Year Education for Academic Years 2012-2017. "></a>
                        				
                        <p>
                           <!-- <a href="http://www.nsa.gov/ia/academic_outreach/nat_cae/institutions.shtml#fl" target="_blank"> --> <img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/DHS.png" alt="Department of Homeland Security" width="172"> 
                           <!-- </a> -->
                        </p>
                        				
                        <p><a href="http://www.nsa.gov/ia/academic_outreach/nat_cae/institutions.shtml#fl" target="_blank"><img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/NSA.png" alt="NSA Information Assurance Outreach"></a></p>
                        				
                        <h3>Partners</h3>
                        				
                        <p><a href="http://cisco.netacad.com" target="_blank"> <img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/PartnerLogo_360px_72_RGB.png" alt="Cisco Networking Academy" width="100"></a> <a href="http://partners.comptia.org/Academy-Partner.aspx" target="_blank"><img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/CAPP_Academy_100.png" alt="CompTIA Authorized Academy" width="100"> </a> <br><br> <a href="http://www.cnss.gov/"><img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/CNSS_Logo_New.png" alt="Committee on National Security Systems" width="100"></a> <a href="http://www.cyberwatchcenter.org/" target="_blank"> <img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/cyberwatch-logo.png" alt="CyberWatch Center" width="100"></a> <br><br> <a href="https://education.emc.com/academicalliance/default.aspx" target="_blank"><img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/emc_academic_alliance_220.png" alt="EMC Academic Alliance" width="100"> </a></p>
                        				<br>
                        				
                        <p><a href="http://www.vmware.com/partners/programs/vap/" target="_blank"><img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/vmware_authorized_academy_282_40.png" alt="VMware Authorized Academy" width="200"></a></p>
                        				<br>
                        				
                        <p><a href="https://www.microsoft.com/en-us/education/imagine-academy/default.aspx" target="_blank"><img src="/academics/departments/engineering/programs/network-engineering-technology/images/logos/msit.png" alt="Microsoft IT Academy Program" width="200"></a></p>
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/network-engineering-technology/index.pcf">©</a>
      </div>
   </body>
</html>