<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Drafting &amp; Design Technology | Valencia College</title>
      <meta name="Description" content="Drafting &amp; Design Technology">
      <meta name="Keywords" content="college, school, educational, drafting, design, technology">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/drafting-design-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Drafting Design Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Drafting &amp; Design Technology</h2>
                        				
                        <p>This program is designed to train technicians to assist engineers by translating ideas,
                           rough sketches, specifications and calculations into complete and accurate working
                           drawings. In addition, instruction is given in three CADD (Computer Aided Drafting
                           and Design) courses which prepares the student for employment with institutions using
                           computer assisted engineering and design.
                        </p>
                        				
                        <h3>Associate in Science Degree</h3>
                        				
                        <ul>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/draftinganddesigntechnology//#text">Catalog Information</a></li>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/draftinganddesigntechnology//#programrequirementstext">Program Requirements</a></li>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/draftinganddesigntechnology//#coursedescriptionstext">Course Descriptions</a></li>
                           				
                        </ul>
                        				
                        <h3>Technical Certificates</h3>
                        				
                        <ul>
                           					
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/draftinganddesigntechnology//#certificatestext" target="_blank">Click for Catalog Information on all Technical Certificates</a></li>
                           				
                        </ul>
                        				
                        <h3>Links</h3>
                        				
                        <ul>
                           					
                           <li><a href="/students/future-students/degree-options/associates/drafting-and-design-technology/">A.S. Degree Program Overview</a></li>
                           				
                        </ul>
                        				
                        <p><strong>Career Program Advisor:</strong> Beverly Johnson
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Faculty</h3>
                        				
                        <table class="table ">
                           					
                           <tbody>
                              						
                              <tr>
                                 							
                                 <th width="50px">&nbsp;</th>
                                 							
                                 <th width="200px">Name</th>
                                 							
                                 <th width="300px">Website</th>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg-2796023887840889672.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=sbowling6">Sandra Bowling</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?sbowling6" target="_blank">http://frontdoor.valenciacollege.edu?sbowling6</a></td>
                                 						
                              </tr>
                              					
                           </tbody>
                           				
                        </table>
                        				
                        <h3>Adjunct</h3>
                        				
                        <table class="table ">
                           					
                           <tbody>
                              						
                              <tr>
                                 							
                                 <th width="50px">&nbsp;</th>
                                 							
                                 <th width="200px">Name</th>
                                 							
                                 <th width="300px">Website</th>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cbollinger2">Cheryl Bollinger</a></td>
                                 							
                                 <td><a href="http://frontdoor.valenciacollege.edu/?cbollinger2" target="_blank">http://frontdoor.valenciacollege.edu?cbollinger2</a></td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aemigh">April Emigh</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ffountain">Frank Fountain</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mtaylor118">Matt Taylor</a></td>
                                 							
                                 <td>&nbsp;</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=rwatson17">Bob Watson</a></td>
                                 						
                              </tr>
                              					
                           </tbody>
                           				
                        </table>
                        				
                        <h3>Staff</h3>
                        				
                        <table class="table ">
                           					
                           <tbody>
                              						
                              <tr>
                                 							
                                 <th width="50px">&nbsp;</th>
                                 							
                                 <th width="200px">Name</th>
                                 							
                                 <th width="220px">Position</th>
                                 							
                                 <th width="80px">Phone</th>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=adefreitashahnepa">Ana De Freitas Hahne Paes</a></td>
                                 							
                                 <td>Laboratory Assistant I</td>
                                 							
                                 <td>(407) 582-1599</td>
                                 						
                              </tr>
                              						
                              <tr>
                                 							
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 							
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=vkangalova">Vedda Kangalova</a></td>
                                 							
                                 <td>Laboratory Assistant I</td>
                                 							
                                 <td>(407) 582-1599</td>
                                 						
                              </tr>
                              					
                           </tbody>
                           				
                        </table>
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/drafting-design-technology/index.pcf">©</a>
      </div>
   </body>
</html>