<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Energy Management and Controls Technology | Valencia College</title>
      <meta name="Description" content="Energy Management and Controls Technology">
      <meta name="Keywords" content="college, school, educational, energy, management, controls, technology">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/energy-management-and-controls-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Energy Management And Controls Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Energy Management and Controls Technology</h2>
                        
                        <h3>Want to learn more about careers in Energy Management and Controls Technology?</h3>
                        
                        <p>
                           <!-- fixme: video link -->
                        </p>
                        
                        <h3>Links</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.bestctr.org/why-building-performance/" target="_blank">Why Building Performance?</a></li>
                           
                           <li><a href="http://www.womeninhvacr.com/webapp/p/244/whvacr-scholarships" target="_blank">Women in HVACR Scholarship Program</a></li>
                           
                           <li><a href="https://www.ashrae.org/membership--conferences/student-zone" target="_blank">American Society of Heating, Refrigerating and Air-Conditioning Engineers (ASHRAE)
                                 Student Zone</a></li>
                           
                           <li><a href="http://www.careersinhvacr.org/site/1/Home" target="_blank">Careers in HVACR</a></li>
                           
                           <li><a href="http://www.hondasmarthome.com/" target="_blank">Take a Virtual Smart Building Tour</a></li>
                           
                           <li><a href="https://steamrocks.wordpress.com/" target="_blank">STEAM (Science, Technology, Engineering, Art, and Math) Rocks</a></li>
                           
                        </ul>
                        
                        <h3>EMCT Industry News</h3>
                        
                        <ul>
                           
                           <li><a href="https://www.theguardian.com/environment/2016/dec/06/google-powered-100-renewable-energy-2017" target="_blank">Google Powered Renewable Energy</a></li>
                           
                           <li><a href="http://www.businessinsider.com/swedish-icehotel-365-frozen-year-round-2016-12" target="_blank">Swedish icehotel 365 frozen year round</a></li>
                           
                           <li><a href="http://mashable.com/2017/02/14/dubai-worlds-first-rotating-skyscraper/#2vmcytuGzmq9">Dubai is bringing the world its first rotating skyscraper</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Location</h3>
                        
                        <address>1800 S. Kirkman Road Orlando, FL 32811<br>Building 9 Room 140
                        </address>
                        
                        <p>Phone: 407-582-1904<span>&nbsp;</span><br>Fax: 407-582-1900
                        </p>
                        
                        <hr>
                        
                        <h3>&nbsp;Social media</h3>
                        <a href="https://www.facebook.com/valenciaECPT/">Valencia Engineering, Computer Programming &amp; Technology Facebook Group</a><hr><a href="http://ewh.ieee.org/sb/orlando/valencia/" target="_blank"><img src="/academics/departments/engineering/images/IEEE-Valencia.png" alt="Valencia College IEEE Student Branch"></a> 
                        <!-- 					<div class="aspect-ratio"><iframe allowtransparency="true" frameborder="0" height="70" scrolling="no" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaIEEE%2F&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250"></iframe></div> -->
                        
                        <div><a href="https://www.facebook.com/ValenciaIEEE/">Valencia College IEEE Facebook Group</a></div>
                        
                        <hr><a href="http://fesvalencia.com/" target="_blank"><img src="/academics/departments/engineering/images/FES-Valencia.png" alt="Florida Engineering Society - Valencia College Student Chapter"></a>
                        
                        <div><a href="https://www.facebook.com/groups/FESatValenciaCollege" target="_blank">FES Facebook Group</a></div>
                        
                        <hr><a href="http://swevalencia.weebly.com" target="_blank"><img src="/academics/departments/engineering/images/swe.png" alt="Society of Women Engineers"></a> 
                        <!-- 					<div class="aspect-ratio"><iframe allowtransparency="true" frameborder="0" height="70" scrolling="no" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSWE.valenciacollege&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250"></iframe></div> -->
                        
                        <div><a href="https://www.facebook.com/SWE.valenciacollege/">SWE Valencia Facebook Group</a></div>
                        
                        <hr><a href="http://nsbevalencia.weebly.com/" target="_blank"><img src="/academics/departments/engineering/images/NSBE_ProLogo_Color_FNL.jpg" alt="National Society of Black Engineers"></a> 
                        <!-- 					<div class="aspect-ratio"><iframe allowtransparency="true" frameborder="0" height="70" scrolling="no" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FNSBE-Valencia-College-Student-Chapter-908400825881637%2F&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250"></iframe></div> -->
                        
                        <div><a href="https://www.facebook.com/NSBE-Valencia-College-Student-Chapter-908400825881637/">NSBE Valencia Student Chapter Facebook Group</a></div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/energy-management-and-controls-technology/index.pcf">©</a>
      </div>
   </body>
</html>