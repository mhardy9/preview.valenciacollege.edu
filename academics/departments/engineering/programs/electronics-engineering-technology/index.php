<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Electronics Engineering Technology | Valencia College</title>
      <meta name="Description" content="Electronics Engineering Technology">
      <meta name="Keywords" content="college, school, educational, electronics, engineering, technology">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/electronics-engineering-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Electronics Engineering Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="col-md-6">
                           
                           <h2>Electronics Engineering Technology</h2>
                           
                           <p>This program is designed to produce highly skilled technicians capable of assisting
                              in the design, production, operation and servicing of electronics, optics, photonics,
                              lasers, telecommunication and wireless systems and equipment. The specializations
                              will provide an up-to-date curriculum in electronics engineering, lasers and photonics,
                              and telecommunication and wireless technology. Valencia is a Center of Electronics
                              Emphasis in Florida and is equipped with special test equipment and advanced laboratories,
                              which provide the latest in hands-on experience.
                           </p>
                           
                           <h3>Associate in Science Degree</h3>
                           
                           <ul>
                              
                              <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#text">Catalog Information</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#programrequirementstext">Program Requirements</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#coursedescriptionstext">Course Descriptions</a></li>
                              
                           </ul>
                           
                           <h3>Technical Certificates</h3>
                           
                           <ul>
                              
                              <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#certificatestext" target="_blank">Click for Catalog Information on all Technical Certificates</a></li>
                              
                           </ul>
                           
                           <h3>Documents</h3>
                           
                           <ul>
                              
                              <li><a title="Understanding Resistor Color Code" href="/academics/departments/engineering/programs/electronics-engineering-technology/documents/understandingtheresistorcolorcode.pdf" target="_blank">Understanding Resistor Color Code</a></li>
                              
                           </ul>
                           
                           <h3>Links</h3>
                           
                           <ul>
                              
                              <li><a href="/students/future-students/degree-options/associates/electronics-engineering-technology/">A.S. Degree Program Overview</a></li>
                              
                           </ul>
                           
                           <hr class="styled_2">
                           
                           <h3>Faculty</h3>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th width="50px">&nbsp;</th>
                                    
                                    <th width="200px">Name</th>
                                    
                                    <th width="300px">Website</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=gandrews">Gordon Andrews</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?gandrews" target="_blank">http://frontdoor.valenciacollege.edu?gandrews</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=rbunea">Radu Bunea</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?rbunea" target="_blank">http://frontdoor.valenciacollege.edu?rbunea</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aevans57">Ashley Evans</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?aevans57" target="_blank">http://frontdoor.valenciacollege.edu?aevans57</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg-8330756096143991053.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dhall">Deb Hall</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?dhall" target="_blank">http://frontdoor.valenciacollege.edu?dhall</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=arafay">Arif Rafay</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?arafay" target="_blank">http://frontdoor.valenciacollege.edu?arafay</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg8293690089687029144.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=vrajaravivarma">Veeramuthu Rajaravivarma</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?vrajaravivarma" target="_blank">http://frontdoor.valenciacollege.edu?vrajaravivarma</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br>
                           
                           <h3>Adjunct</h3>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th width="50px">&nbsp;</th>
                                    
                                    <th width="200px">Name</th>
                                    
                                    <th width="300px">Website</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cbakalov">Christian Bakalov</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=abhalkikar">Abhijeet Bhalkikar</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jcarstensen">Jim Carstensen</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?jcarstensen" target="_blank">http://frontdoor.valenciacollege.edu?jcarstensen</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ffontancolon">Felix Fontan Colon</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=rgrimming">Robert Grimming</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?rgrimming" target="_blank">http://frontdoor.valenciacollege.edu?rgrimming</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=akuguoglu">Akin Kuguoglu</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dmarchetti">David Marchetti</a></td>
                                    
                                    <td><a href="http://frontdoor.valenciacollege.edu/?dmarchetti" target="_blank">http://frontdoor.valenciacollege.edu?dmarchetti</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=fmawudzro">Fred Mawudzro</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mquintanilla1">Michael Quintanilla</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=wrivastorres">Wilfredo Rivas Torres</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=gsewnath">Gajadhar Sewnath</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dsowri">Doss Sowri</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br>
                           
                           <h3>Staff</h3>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th width="50px">&nbsp;</th>
                                    
                                    <th width="200px">Name</th>
                                    
                                    <th width="220px">Position</th>
                                    
                                    <th width="80px">Phone</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mdammar">Melissa Dammar</a></td>
                                    
                                    <td>Laboratory Aide I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ldookhie">Luke Dookhie</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=afeliciano26">Abimelec Feliciano</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=wgoodman1">Will Goodman</a></td>
                                    
                                    <td>Lab Supervisor</td>
                                    
                                    <td>(407) 582-1906</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=shurst4">Sabrina Hurst</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=timran">Sera Imran</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jjacobs29">Jontavius Jacobs</a></td>
                                    
                                    <td>Laboratory Aide I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=nmaurer2">Nathan Maurer</a></td>
                                    
                                    <td>Laboratory Aide I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=emontanez6">Emanuel Montanez</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=smourfik">Said Mourfik</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>(407) 582-1904</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jmurphy54">Jathan Murphy</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>(407) 582-1904</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=anikitin">Alexander Nikitin</a></td>
                                    
                                    <td>Laboratory Aide II</td>
                                    
                                    <td>(407) 582-5793</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=vpetrillo">Victor Petrillo</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jpulver1">Jonathan Pulver</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=srafay">Sahir Rafay</a></td>
                                    
                                    <td>Laboratory Aide I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=yriver10">Yamil Rivera Casanova</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=arodriguezsemidey">Amanda Rodriguez-Semidey</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aschroeder4">Andrew Schroeder</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mtorres139">Maria Torres Ramon</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>(407) 582-1904</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=streesh">Steven Treesh</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>(407) 582--1902</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=swhittington4">Seth Whittington</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                    
                                    <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jwolfe12">Justin Wolfe</a></td>
                                    
                                    <td>Laboratory Assistant I</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <div class="col-md-6">
                           
                           <div class="aspect-ratio" style="height: 500px;"><iframe style="border: none;" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaIEEE&amp;tabs=timeline&amp;width=500&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId=203940866824764" width="300" height="150" frameborder="0" scrolling="no"></iframe></div>
                           
                           <div><a href="http://nsbevalencia.weebly.com/" target="_blank"><img src="/academics/departments/engineering/images/NSBE_ProLogo_Color_FNL.jpg" alt="National Society of Black Engineers"></a></div>
                           
                           <!-- jsowell -->
                           
                           <p><strong>Career Program Advisor:</strong> Jon Sowell
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/electronics-engineering-technology/index.pcf">©</a>
      </div>
   </body>
</html>