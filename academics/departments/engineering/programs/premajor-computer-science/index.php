<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Pre-Major: Computer Science | Valencia College</title>
      <meta name="Description" content="Pre-Major: Computer Science">
      <meta name="Keywords" content="college, school, educational, bachelors, computer, science">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/programs/premajor-computer-science/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/engineering/">Engineering</a></li>
               <li><a href="/academics/departments/engineering/programs/">Programs</a></li>
               <li>Premajor Computer Science</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Pre-Major: Computer Science</h2>
                        
                        <p>This pre-major is designed for the student who plans to transfer to the University
                           of Central Florida (UCF) in Orlando, Florida as a junior to complete a four-year Bachelor's
                           degree in the College of Engineering and Computer Science. It is based upon an articulation
                           agreement in Computer Science with UCF. Students who plan to transfer are responsible
                           for completing the admission requirements of UCF.
                        </p>
                        
                        <h3>Associate in Arts Degree</h3>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/computerscienceuniversityofcentralflorida/#text">Catalog Information</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/computerscienceuniversityofcentralflorida/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/computerscienceuniversityofcentralflorida/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        <p><strong>Career Program Advisor:</strong> Charles Davis
                        </p>
                        
                        <p><a href="https://www.youtube.com/watch?v=Mj0WbUI-Luw"><img src="/academics/departments/engineering/programs/premajor-computer-science/images/computer_programming_video.png" alt="Department Overview video"> <br><strong>Department Overview</strong> <i class="fab fa-youtube"></i></a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Faculty</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jalexander">Joan Alexander</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?jalexander" target="_blank">http://frontdoor.valenciacollege.edu?jalexander</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=carchibald">Colin Archibald</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mgossai">Mahendra Gossai</a></td>
                                 
                                 <td><a href="http://web.valenciacollege.edu/cpa/mgossai" target="_blank">http://web.valenciacollege.edu/cpa/mgossai</a><span>&nbsp;</span><br><a href="http://frontdoor.valenciacollege.edu/?mgossai" target="_blank">http://frontdoor.valenciacollege.edu?mgossai</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg-759537919294136552.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=greed9">Jerry Reed</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?greed9" target="_blank">http://frontdoor.valenciacollege.edu?greed9</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dsanchez">Dimas Sanchez De Leon</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?dsanchez" target="_blank">http://frontdoor.valenciacollege.edu?dsanchez</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        <br>
                        
                        <h3>Adjunct</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=callen1">Cynthia Allen</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?callen1" target="_blank">http://frontdoor.valenciacollege.edu?callen1</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=talqolaghassi">Tamara Alqolaghassi</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mantonovich">Michael Antonovich</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jcrichlow">Joel Crichlow</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?jcrichlow" target="_blank">http://frontdoor.valenciacollege.edu?jcrichlow</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jlook">Joe Look</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?jlook" target="_blank">http://frontdoor.valenciacollege.edu?jlook</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=josborne17">Joan Osborne</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=eschweitzer2">Elyce Schweitzer</a></td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=btaylor57">Benjamin Taylor</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        <br>
                        
                        <h3>Staff</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="220px">Position</th>
                                 
                                 <th width="80px">Phone</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg1213996023165709647.PNG" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=marnold21">Marcus Arnold</a></td>
                                 
                                 <td>Laboratory Aide I</td>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=bhargeralves">Bruno Harger Alves</a></td>
                                 
                                 <td>Laboratory Aide II</td>
                                 
                                 <td>(407) 582--1587</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg-6888580847315881328.PNG" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=iotoole">Ian O'Toole</a></td>
                                 
                                 <td>Lab Supervisor</td>
                                 
                                 <td>(407) 582-1484</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=breed4">Ben Reed</a></td>
                                 
                                 <td>Programmer Analyst</td>
                                 
                                 <td>(407) 582-5414</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=curciolo">Carl Urciolo</a></td>
                                 
                                 <td>Laboratory Assistant I</td>
                                 
                                 <td>(407) 582-1587</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/programs/premajor-computer-science/index.pcf">©</a>
      </div>
   </body>
</html>