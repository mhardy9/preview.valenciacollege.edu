
<ul>
	<li><a href="/academics/departments/engineering/index.php">Engineering | Computer Programming | Technology</a></li>
	<li class="submenu"><a href="/academics/departments/engineering/programs/" class="show-submenu">Programs <i class="fas fa-chevron-down" aria-hidden="true"> </i> </a><ul>
		<li><a href="/academics/departments/engineering/programs/premajor-computer-science/">Articulated Pre-Major: Computer Science</a></li>
		<li><a href="/academics/departments/engineering/programs/premajor-engineering/">Articulated Pre-Major: Engineering</a></li>
		<li><a href="/academics/departments/engineering/programs/premajor-information-technology/">Articulated Pre-Major: Information Technology</a></li>
		<li><a href="/academics/departments/engineering/programs/bachelor-electrical-computer-engineering-technology/">Electrical &amp; Computer Engineering Technology - Bachelor of Science</a></li>
		<li><a href="/academics/departments/engineering/programs/building-construction-technology/">Building Construction Technology </a></li>
		<li><a href="/academics/departments/engineering/programs/energy-management-and-controls-technology/">Energy Management and Controls Technology</a></li>
		<li><a href="/academics/departments/engineering/programs/civil-surveying-engineering-technology/">Civil/Surveying Engineering Technology</a></li>
		<li><a href="/academics/departments/engineering/programs/computer-information-technology/">Computer Information Technology</a></li>
		<li><a href="/academics/departments/engineering/programs/computer-programming-and-analysis/">Computer Programming &amp; Analysis</a></li>
		<li><a href="/academics/departments/engineering/programs/drafting-design-technology/">Drafting &amp; Design Technology</a></li>
		<li><a href="/academics/departments/engineering/programs/electronics-engineering-technology/">Electronics Engineering Technology</a></li>
		<li><a href="/academics/departments/engineering/programs/network-engineering-technology/">Network Engineering Technology</a></li>
		</ul></li>	
	<li><a href="/academics/departments/engineering/labhours.php">Labs</a></li>
	<li><a href="http://net4.valenciacollege.edu/forms/west/engineering/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
