<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Engineering, Computer Programming, &amp; Technology | Valencia College</title>
      <meta name="Description" content="Engineering, Computer Programming, &amp; Technology">
      <meta name="Keywords" content="college, school, educational, engineering, computer, programming, technology">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/engineering/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/engineering/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Engineering, Computer Programming, &amp; Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Engineering</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Division Mission Statement</h2>
                        
                        <p>Consistent with the mission of Valencia College, the Division of Engineering, Computer
                           Programming, and Technology (ECPT) seeks to prepare students to perform competently
                           in the fields of building construction, drafting and design, software development,
                           information technology, engineering, and a variety of engineering technologies by
                           providing high quality programs for Technical Certificates, Associate in Science Degrees,
                           and Bachelor of Science Degrees.
                        </p>
                        
                        <h2>Objectives</h2>
                        
                        <p><em>To achieve the goals of this mission, we will:</em></p>
                        
                        <ul>
                           
                           <li>Develop innovative instructional methods by dedicated faculty involved in the scholarship
                              of teaching via new techniques for learning and assessment.
                           </li>
                           
                           <li>Teach students to apply the fundamentals of science, mathematics, and communications,
                              as well as advanced technical concepts in specific technology fields.
                           </li>
                           
                           <li>Provide a foundation and opportunity for life-long learning and adaptation to a changing,
                              sustainable world, with a continuing emphasis on open access and diversity in a global
                              economy.
                           </li>
                           
                           <li>Provide students with professionally-delivered learning experiences that are appropriate
                              and innovative.
                           </li>
                           
                           <li>Encourage students to develop a solid basis of professional and ethical standards.</li>
                           
                           <li>Provide the experiences necessary for students to strengthen their analytical, critical,
                              and reflective thinking and to develop their collaborative and communication skills.
                           </li>
                           
                           <li>Serve the community through education and service programs which utilize and showcase
                              the unique talents of Valencia faculty, staff, and students.
                           </li>
                           
                           <li>Maintain close relationships with industry through active advisory committees and
                              customized corporate training and education.
                           </li>
                           
                           <li>Provide a strong foundation upon which students may achieve additional educational
                              goals.
                           </li>
                           
                        </ul>
                        
                        <hr>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Department Chairs:</h3>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jalexander">Joan Alexander</a></div>
                           
                           <div class="col-xs-9">Computer Programming &amp; Analysis - Associate of Science; Computer Information Technology
                              - Associate of Science; Computer Science - Associate of Arts Premajor
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mkar">Dr. Mohua Kar</a></div>
                           
                           <div class="col-xs-9">Articulated Pre-Major: Engineering</div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mejaz">Dr. Masood Ejaz</a></div>
                           
                           <div class="col-xs-9">Electrical &amp; Computer Engineering Technology (ECET) - Bachelor of Science</div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aray">Andy Ray</a></div>
                           
                           <div class="col-xs-9">Building Construction Technology; Drafting and Design Technology; Civil / Surveying
                              Engineering Technology
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=wyousif">Dr. Wael Yousif </a></div>
                           
                           <div class="col-xs-9">Network Engineering Technology - Associate of Science</div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dhall">Dr. Deb Hall</a></div>
                           
                           <div class="col-xs-9">Electronics Engineering Technology - Associate of Science</div>
                           
                        </div>
                        
                        <hr>
                        
                        <h3>Career Program Advisors:</h3>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ngirgis">Nancy Girgis</a></div>
                           
                           <div class="col-xs-9">B.S. Electrical and Computer Engineering Technology Degree Programs A.A. Pre-Majors
                              Engineering, Computer Science, and Information Technology
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=bjohnson">Beverly Johnson</a></div>
                           
                           <div class="col-xs-9">Building Construction Technology; Drafting and Design Technology; Civil / Surveying
                              Engineering Technology
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jsowell">Jon Sowell</a></div>
                           
                           <div class="col-xs-9">Computer Programming &amp; Analysis; Information Technology; Electrical Engineering Technology;
                              Network Engineering Technology
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-xs-3"><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cdemings">Courtney Demings</a></div>
                           
                           <div class="col-xs-9">Articulated Pre-Major Engineering</div>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Florida Tech Scholarships for Valencia Grads</h4>
                        <a title="Florida Tech Valencia Presidential Scholarship" href="/academics/academic-affairs/career-workforce-education/documents/Florida-Tech-Valencia-Presidential-Scholarship.pdf" target="_blank"> <img style="float: right;" src="/academics/academic-affairs/career-workforce-education/images/Florida-Tech-Valencia-Presidential-Scholarship.png" alt="Florida Tech Valencia Presidential Scholarship" width="125" height="165"> </a>
                        
                        <p>Each year, Florida Tech, a private university located in Melbourne that has been named
                           one of the top technological colleges in the Southeast, and Valencia College make
                           15 Presidential Transfer Scholarships available to exceptional students who are graduating
                           from Valencia College and wish to continue their undergraduate studies at Florida
                           Tech’s Melbourne campus.
                        </p>
                        
                        <p>The Presidential Transfer Scholarship is awarded annually for students entering in
                           the fall semester only.
                        </p>
                        
                        <p>Download the <a href="/academics/academic-affairs/career-workforce-education/documents/Florida-Tech-Valencia-Presidential-Scholarship.pdf">Florida Tech/Valencia College Presidential Scholarship flyer</a> for more information.
                        </p>
                        
                        <hr>
                        
                        <h3>Contact</h3>
                        
                        <div>1800 S. Kirkman Road Orlando, FL 32811<span>&nbsp;</span><br>Building 9 Room 140<span>&nbsp;</span><br>Phone: 407-582-1904 Fax: 407-582-1900<span>&nbsp;</span></div>
                        
                        <div><br><br><strong>Division Dean:<span>&nbsp;</span></strong><a href="http://valenciacollege.edu/includes/UserInfo.cfm?username=lmacon"><strong>Dr. Lisa Macon</strong></a></div>
                        
                        <div>&nbsp;</div>
                        
                        <div>&nbsp;</div>
                        
                        <p><a href="http://ewh.ieee.org/sb/orlando/valencia/" target="_blank"><img src="/academics/programs/engineering-technology/images/IEEE-Valencia.png" alt="IEEE" width="250" height="75"></a><span></span><a title="IEEE Valencia College Student Branch" href="/academics/departments/engineering/images/IEEE-Valencia.png"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaIEEE%2F&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250" height="70" frameborder="0" scrolling="no"></iframe></a><span></span><a href="http://fesvalencia.com" target="_blank"><img title="Florida Engineering Society - Valencia College Student Chapter" src="/academics/programs/engineering-technology/images/FES-Valencia.png" alt="Florida Engineering Society - Valencia College Student Chapter" width="250" height="75"></a></p>
                        
                        <div><a title="FES Facebook Group" href="https://www.facebook.com/groups/FESatValenciaCollege" target="_blank">FES Facebook Group</a></div>
                        
                        <p><a title="Society of Women Engineers" href="/academics/departments/engineering/images/swe.png" target="_blank"><img title="Society of Women Engineers" src="/academics/programs/engineering-technology/images/swe.png" alt="http://swevalencia.weebly.com" width="250" height="150"></a><span><span>&nbsp;</span></span><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSWE.valenciacollege&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250" height="70" frameborder="0" scrolling="no"></iframe><span><span>&nbsp;</span></span><a href="http://nsbevalencia.weebly.com/" target="_blank"><img src="/academics/programs/engineering-technology/images/NSBE_ProLogo_Color_FNL.jpg" alt="NSBE" width="250" height="75"></a><span><span>&nbsp;</span></span><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FNSBE-Valencia-College-Student-Chapter-908400825881637%2F&amp;tabs&amp;width=250&amp;height=70&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=true&amp;show_facepile=false&amp;appId" width="250" height="70" frameborder="0" scrolling="no"></iframe></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/engineering/index.pcf">©</a>
      </div>
   </body>
</html>