<ul>
<li><a href="/academics/departments/health-sciences/advising/">Advising </a></li>
<li><a href="/academics/departments/health-sciences/allied-health/">Division of Allied Health</a></li>
<li><a href="/academics/departments/health-sciences/nursing/">Division of Nursing</a></li>
<li><a class="show-submenu" href="/academics/departments/health-sciences/continuing-education/">Continuing Education <i class="far fa-angle-down"></i> </a>
<ul>
<li><a href="/academics/departments/health-sciences/continuing-education/course-catalog.php">Course Catalog</a></li>
<li><a href="/academics/departments/health-sciences/continuing-education/register.php">Ways to Register</a></li>
<li><a href="/academics/departments/health-sciences/continuing-education/professional-resources.php">Professional Resources</a></li>
<li><a href="http://net4.valenciacollege.edu/west/health/cehealth/Info/contact.cfm" target="_blank">Request Information</a></li>
</ul>
</li>
	<li><a href="/academics/departments/health-sciences/compliance.php">Orientation &amp; Compliance</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm" target="_blank">Contact Us</a></li>
</ul>