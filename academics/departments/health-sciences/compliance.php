<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Health Sciences Department | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/compliance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Health Sciences Department</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li>Health Sciences Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <h2>Orientation &amp; Compliance</h2>
                  			
                  <h3>Spring 2018 - January</h3>
                  			
                  <p>Congratulations on your conditional acceptance to your chosen Health Sciences Program.
                     Your final acceptance is contingent on your attending the mandatory orientation  and
                     meeting the clinical compliance requirements by the specified deadline. 
                  </p>
                  
                  			
                  <p>For orientation, please find the specific program in which you are enrolled and follow
                     these steps:
                  </p>
                  			
                  <h4>Prepare for Orientation &amp; Compliance</h4>
                  
                  			
                  <ul class="list_order">
                     
                     				
                     <li><span>1</span>Find your specific program in the list below.
                     </li>
                     
                     				
                     <li><span>2</span>Click on each document listed under "Orientation and Compliance Information" and print
                        each.
                     </li>
                     
                     				
                     <li><span>3</span>Note the Date and Time of the mandatory orientation for your program.
                     </li>
                     
                     				
                     <li><span>4</span>Note the Compliance Deadline.  All Compliance requirements must be completed by this
                        date.
                     </li>
                     			
                  </ul>
                  			
                  <p>All Compliance Questions should be directed to <a href="mailto:hscompliance@valenciacollege.edu">hscompliance@valenciacollege.edu</a> 
                  </p>
                  
                  
                  			
                  <table class="table table">
                     				
                     <tr scope="col">
                        					
                        <th>Program</th>
                        					
                        <th>Orientation and Compliance Information</th>
                        					
                        <th>Mandatory Orientation</th>
                        					
                        <th>Compliance Deadline</th>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">BS CARDIOPULMONARY: ECHO</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>N/A</td>
                        					
                        <td>12/04/2017</td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">COMPUTED TOMOGRAPHY</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-CT-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/10/2017               11 A.M.-1 P.M.            HSB 107</strong></p>
                           						
                           <p>Centra Care, FIT testing and uniforms follow the formal orientation HSB 225/228</p>
                        </td>
                        					
                        <td>N/A</td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">EMT</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-EMT-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>12/8/2017            OR                   1/5/2018 1-3 P.M.                    HSB
                                 105</strong></p>
                           						
                           <p>Centra Care, FIT testing and uniforms follow the formal orientation HSB 225/228</p>
                        </td>
                        					
                        <td>
                           <p>12/22/2017</p>
                        </td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">MAMMOGRAPHY</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-Mammo-Spring-2018-092717.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/10/2017               11 A.M.-1 P.M.            HSB 107</strong></p>
                           						
                           <p>Centra Care, FIT testing and uniforms follow the formal orientation HSB 225/228</p>
                        </td>
                        					
                        <td>
                           <p>N/A</p>
                        </td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">NURSING, ACCELERATED </th>
                        					
                        <td>
                           <ul class="style22">
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-ATN-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/NursingCostsforOrientationCompliancewebsite.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs </a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/10/2017               9 -11 A.M.                    4-236/237</strong></p>
                           						
                           <p>Centra Care, FIT testing and uniforms follow the formal orientation HSB 225/228</p>
                        </td>
                        					
                        <td>12/4/2017</td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">NURSING, ACCELERATED/ ALTERNATES ONLY</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-ATN-Alternate-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs </a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/10/2017               9 -11 A.M.                    4-236/237</strong></p>
                           						
                           <p>Centra Care, FIT testing and uniforms follow the formal orientation HSB 225/228</p>
                        </td>
                        					
                        <td>12/4/2017</td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">NURSING, TRADITIONAL and Valencia/UCF Concurrent</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-NURSING-TRADITIONAL-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter </a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs </a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/10/2017               9 -11 A.M.                    HSB 105</strong></p>
                           						
                           <p>Centra Care, FIT testing and uniforms follow the formal orientation HSB 225/228</p>
                        </td>
                        					
                        <td>12/4/2017</td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">PARAMEDIC OCFRD/ORANGE COUNTY FIRE RESCUE DEPARTMENT</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-OCFD-Paramedic-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs </a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/16/2017 OR                  11/17/2017              <br>
                                 						10A.M.-1 P.M.                    OSC 1-123</strong></p>
                        </td>
                        					
                        <td>12/11/2017</td>
                        				
                     </tr>
                     
                     				
                     <tr>
                        					
                        <th scope="row">PARAMEDIC/TRADITIONAL</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-Paramedic-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs </a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/16/2017   OR  11/17/2017 <br>
                                 						10A.M.-1 P.M. OSC 1-123</strong></p>
                        </td>
                        					
                        <td>12/11/2017</td>
                        				
                     </tr>
                     				
                     <tr>
                        					
                        <th scope="row">RESPIRATORY CARE</th>
                        					
                        <td>
                           <ul>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Orientation-Letter-RESPIRATORY-January-2018.pdf" target="_blank" class="icon_for_pdf">Orientation Letter</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-disclosure-of-costs.pdf" target="_blank" class="icon_for_pdf">Disclosure of Costs </a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/valencia-health-compliance-documents.pdf" target="_blank" class="icon_for_pdf">Compliance Documents</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Clinical-Compliance-FAQ.pdf" target="_blank" class="icon_for_pdf">Clinical Compliance FAQ</a></li>
                              						
                              <li><a href="/documents/academics/departments/health-sciences/Valencia-Health-Sciences-Instructions-for-Background-Check.pdf" target="_blank" class="icon_for_pdf">Instructions for Background Check, Drug Screening</a></li>
                              						
                           </ul>
                        </td>
                        					
                        <td>
                           <p><strong>11/10/2017               9 -11 A.M.                    AHS 226</strong></p>
                           						
                           <p>Centra Care, FIT testing and uniforms follow the formal orientation HSB 225/228</p>
                        </td>
                        					
                        <td>12/4/2017</td>
                        				
                     </tr>
                     			
                  </table>
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/compliance.pcf">©</a>
      </div>
   </body>
</html>