<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Division of Allied Health  | Valencia College</title>
      <meta name="Description" content="Division of Allied Health | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/allied-health/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Division of Allied Health</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li>Allied Health</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <h2>Division of Allied Health</h2>
                     				
                     <h3>Office and Administration</h3>
                     				
                     <div class="col-md-6">
                        					
                        					
                        <h4 class="val">Dean of Allied Health</h4>
                        					<i class="far fa-address-card fa-fw"></i>Jamy Chulak<br>
                        					
                        <h4 class="val">Administrative Staff</h4>
                        					<i class="far fa-address-card fa-fw"></i>Sandy Ragsdale, Administrative Assistant to the Dean<br>
                        					<i class="far fa-address-card fa-fw"></i>Nancy Scoltock, Administrative Assistant for Allied Health<br>
                        					
                        <h4 class="val">Technology Support</h4>
                        					<i class="far fa-address-card fa-fw"></i>Jeff Hogan, MS, Functional IS Support Specialist 
                        					
                        <hr class="styled_2">
                        					
                        <h4 class="val">Location</h4>
                        					<i class="far fa-map-marker fa-fw"></i>West Campus, Building 2-208 <br>
                        					
                        <h4 class="val">Hours</h4>
                        					<i class="far fa-calendar fa-fw"></i>Monday - Thursday 8 a.m. - 5 p.m.<br>
                        					<i class="far fa-calendar fa-fw"></i>Friday 9 a.m. – 5 p.m. <br>
                        					
                        <h4 class="val">Contact Us</h4>
                        					<i class="far fa-envelope fa-fw"></i><a href="mailto:#">#@valenciacollege.edu</a> <br>
                        					<i class="far fa-phone fa-fw"></i>407-582-xxxx<br>
                        					<i class="far fa-fax fa-fw"></i>407-582-xxxx (fax)
                        				
                     </div>
                     				
                     <div class="col-md-6">
                        					
                        <div class="aspect-ratio" style="height: 500px;">
                           						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaHealthSciences&amp;tabs=timeline&amp;width=500&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId=203940866824764" min-height="500 px;" style="border:none;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <hr class="styled_2">
                  			
                  <div class="row">
                     				
                     <h3>Health Science Programs</h3>
                     				
                     <h4>Associate in Science</h4>
                     				
                     <p>The A.S. degree&nbsp;prepares you to enter a specialized career field in about two years.&nbsp;It
                        also transfers to the Bachelor of Applied Science program offered at some universities.
                        If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                        at a designated university.
                     </p>
                     				
                     <ul>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Sarah Powers, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/dental-hygiene/">Dental Hygiene</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Pam Sandy, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/diagnostic-medical-sonography/">Diagnostic Medical Sonography</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Carolyn DeLeo, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/emergency-medical-services-tech/">Emergency Medical Services Technology</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Cindy Bell , Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/health-information-technology/">Health Information Technology</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Kelli Lewis, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/radiography/">Radiography</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Beverly Bond, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/respiratory-care/">Respiratory Care</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Jamy Chulak, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/veterinary-tech/">Veterinary Technology</a><br>
                           						Offered through a Cooperative Agreement with <a href="http://www.spcollege.edu/vt/">St. Petersburg College</a>.  Students interested in this program may take the general education requirements
                           at Valencia and complete the rest of the degree online through St. Petersburg College.
                           					
                        </li>
                        				
                     </ul>
                     				
                     <h4>Bachelor of Science</h4>
                     				
                     <p>The B.S. degree&nbsp;prepares you to enter an advanced position within a specialized career
                        field.&nbsp;The upper-division courses build on an associate degree and take about two
                        years to complete.
                     </p>
                     				
                     <ul>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/cardiopulmonary-sciences/">Cardiopulmonary Sciences</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Sharon Shenton, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/radiologic-imaging-sciences/">Radiologic and Imaging Sciences</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Susan Gosnell, Chair
                        </li>
                        				
                     </ul>
                     				
                     <h4>Advanced Technical Certificate</h4>
                     				
                     <p>The <a href="https://preview.valenciacollege.edu/future-students/degree-options/advanced-technical-certificates/" target="_blank">Advanced Technical Certificate</a>, an extension of a specific Associate degree program, consists of at least nine (9),
                        but less than 45, credits of college-level course work. Students who have already
                        received an eligible Associate degree and are seeking a specialized program of study
                        to supplement their associate degree may seek an Advanced Technical Certificate.
                     </p>
                     				
                     <ul>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/computed-tomography/">Computed Tomography</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Beverly Bond, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/echocardiography/">Echocardiography</a><br>
                           					
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/leadership-healthcare/">Leadership in Healthcare</a><br>
                           					
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/magnetic-resonance-imaging/">Magnetic Resonance Imaging</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Beverly Bond, Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/mammography/">Mammography</a><br>
                           					
                        </li>
                        					
                        <li><a href="/academics/programs/health-sciences/vascular-sonography/">Vascular Sonography</a></li>
                        				
                     </ul>
                     				
                     <h4>Certificate</h4>
                     				
                     <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                        for job advancement. Most can be completed in one year or less. Credits earned can
                        be applied toward a related A.S. degree program.
                     </p>
                     				
                     <ul>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/emergency-medical-services-tech/certificate.html">Emergency Medical Technology</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Cindy Bell , Chair
                        </li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/medical-info-coder/index.php">Medical Information Coder/Biller</a><br></li>
                        					
                        <li>
                           						<a href="/academics/programs/health-sciences/paramedic-tech/">Paramedic Technology</a><br>
                           						<i class="far fa-address-card fa-fw"></i>Cindy Bell , Chair
                        </li>
                        				
                     </ul>
                     				
                     <h4>Continuing Education</h4>
                     				
                     <p><a href="/academics/departments/health-sciences/continuing-education/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                        development, language courses, certifications and custom course development for organizations.
                        Day, evening, weekend and online learning opportunities are available.<br>
                        					<i class="far fa-address-card fa-fw"></i>Carol Millenson, Operations Manager - HSB 219
                     </p>
                     			
                  </div>
                  			
                  <hr class="styled_2">
                  			
                  <div class="container margin-60 box_style_1">
                     				
                     <div class="main-title">
                        					
                        <h2>Health Sciences Advising</h2>
                        					
                        <p></p>
                        				
                     </div>
                     				
                     <div class="row">
                        					
                        <p>Health Sciences Advising staff offer specialized educational advisors who can guide
                           students in the successful completion of an Associate in Science Degree or Bachelor
                           of Science Degree or Certificate Program in allied health or nursing. Attendance at
                           a Health Sciences Information Session is mandatory before meeting with a Health Sciences
                           Advisor for an associate-level program but not for meeting about a bachelor-level
                           program.
                        </p>
                        					
                        <p>The advising staff can answer program-specific questions, assist in career and educational
                           planning, and offer expert educational guidance for students seeking career opportunities
                           through the completion of an A.S. or B.S. Degree or a Certificate program.
                        </p>
                        					
                        <p>Associate in Science students are expected to complete their educational goals and
                           be professionally marketable in a much shorter period of time than many others. This
                           is why the early development of career and education plans is essential to a successful
                           transition from school to work.
                        </p>
                        					
                        <div class="row">
                           						
                           <div class="col-md-8 col-sm-8">
                              							<img src="/images/academics/programs/health-sciences/health-sciences-advising.jpg" class="img-responsive"> 
                           </div>
                           						
                           <div class="col-md-4 col-sm-4">
                              							
                              <h5 class="val">Location</h5>
                              							<i class="far fa-map-marker fa-fw"></i>West Building 1, Room 130 <br>
                              							
                              <h5 class="val">Hours</h5>
                              							<i class="far fa-calendar fa-fw"></i>Monday - Thursday 8 a.m. - 5 p.m.<br>
                              							<i class="far fa-calendar fa-fw"></i>Friday 9 a.m. – 5 p.m. <br>
                              							
                              <h5 class="val">Contact Us</h5>
                              							<i class="far fa-envelope fa-fw"></i><a href="mailto:HealthScienceAdvising@valenciacollege.edu">HealthScienceAdvising@valenciacollege.edu</a> <br>
                              							<i class="far fa-phone fa-fw"></i>407-582-1228<br>
                              							<i class="far fa-fax fa-fw"></i>407-582-5462 (fax) 
                           </div>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/allied-health/index.pcf">©</a>
      </div>
   </body>
</html>