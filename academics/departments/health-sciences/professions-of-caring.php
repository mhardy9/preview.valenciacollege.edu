<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>HSC 1004: Professions of Caring - Valencia College | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/professions-of-caring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li>HSC 1004: Professions of Caring - Valencia College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div id="containerMain">
                  
                  
                  
                  <div id="header">
                     
                     
                     
                     <div id="sub_brand_title_internal">HSC 1004: Professions of Caring</div>
                     
                  </div>
                  
                  
                  
                  <img src="http://preview.valenciacollege.edu/west/health/images/featured-health-1.png" width="930" height="314" alt="Health Sciences" border="0">
                  
                  
                  
                  <h3 style="text-shadow: 1px 1px 1px rgba(0,0,0,0.6);">
                     <span style="color: #bf311a">So Many Health Careers!  </span><span style="color:#FDB913">How can I choose which one is right for me?</span>
                     
                  </h3>
                  
                  <h3>HSC 1004: Professions of Caring - 3 College Credits</h3>
                  
                  
                  <h4>Professions of Caring classes will help you:</h4>
                  
                  <ul>
                     
                     <li>Explore health related program costs, admission requirements, and starting salary.</li>
                     
                     <li>Acquire helpful skills to be successful in college.</li>
                     
                     <li>Become comfortable, empowered, and supported in your college experience &amp; future career.</li>
                     
                  </ul>
                  
                  <h2>Professions of Caring classes will give you:</h2>
                  
                  <ul>
                     
                     <li>Valuable admission points you can use to enter selected health programs.</li>
                     
                     <li>For students pending acceptance to health science programs, HSC 1004 Professions of
                        Caring will substitute for SLS 1122.
                     </li>
                     
                  </ul>
                  
                  
                  
                  
                  
                  
                  
                  <h3>HSC 1004 Courses for 
                     Fall 2017 
                     
                  </h3>
                  
                  <div class="button-color2-245"><a href="https://atlas.valenciacollege.edu" title="Login to Atlas to Register">REGISTER TODAY!</a></div>
                  
                  
                  
                  
                  <p></p>
                  
                  <table class="table ">
                     
                     <tr>
                        
                        <th scope="col">CRN</th>
                        
                        <th scope="col">Days</th>
                        
                        <th scope="col">Time</th>
                        
                        <th scope="col">Instructor</th>
                        
                        <th scope="col">Location</th>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=11533" target="_blank">11533</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Brae McIntire</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=11770" target="_blank">11770</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Christina Aleksic</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=11789" target="_blank">11789</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Jeff Hogan</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=13189" target="_blank">13189</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Brae McIntire</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=13330" target="_blank">13330</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Christina Aleksic</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=15802" target="_blank">15802</a></td>
                        
                        <td> &nbsp;T&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td>0400P - 0700P</td>
                        
                        <td>Joan Gibson</td>
                        
                        <td>WC-HSB-221</td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=16202" target="_blank">16202</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Brae McIntire</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=16356" target="_blank">16356</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Bonnie Carmack</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=16357" target="_blank">16357</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Bonnie Carmack</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=17035" target="_blank">17035</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp;F&nbsp; &nbsp; </td>
                        
                        <td>1000A - 1245P</td>
                        
                        <td>Christina Aleksic</td>
                        
                        <td>PC-001-202</td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=17242" target="_blank">17242</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;S&nbsp; </td>
                        
                        <td>0830A - 1115A</td>
                        
                        <td>Maelyn Arroyo Osorio</td>
                        
                        <td>PC-001-300</td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=17456" target="_blank">17456</a></td>
                        
                        <td>M&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td>0600P - 0900P</td>
                        
                        <td>Joan Gibson</td>
                        
                        <td>WC-HSB-221</td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=17618" target="_blank">17618</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Keisa Boykin</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=17632" target="_blank">17632</a></td>
                        
                        <td> &nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td>0700P - 0945P</td>
                        
                        <td>Lori Yates</td>
                        
                        <td>EC-OO1-257</td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=17706" target="_blank">17706</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                        
                        <td>0200P - 0450P</td>
                        
                        <td>Pamela Lapinski</td>
                        
                        <td>WC-HSB-220</td>
                        
                     </tr>
                     
                     
                     <tr>
                        
                        <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1004&amp;crn_in=17899" target="_blank">17899</a></td>
                        
                        <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                        
                        <td> - </td>
                        
                        <td>Fiona Mackay</td>
                        
                        <td>ONLINE- </td>
                        
                     </tr>
                     
                     
                  </table>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/professions-of-caring.pcf">©</a>
      </div>
   </body>
</html>