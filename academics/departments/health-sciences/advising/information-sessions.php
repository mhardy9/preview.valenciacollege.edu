<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Health Sciences | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/advising/information-sessions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Health Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/departments/health-sciences/advising/">Advising</a></li>
               <li>Health Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Health Sciences Information Sessions</h2>
                        
                        <p><strong>Please register for a Health Sciences Information Session  via the chart below. If
                              the registration link is not active for your  session, please check again later. <u>Please note the TOPIC/DATE/TIME/LOCATION (INCLUDING CAMPUS) of your desired session
                                 before selecting your date via the registration link below, as the registration calendar
                                 will display all dates on all campuses.</u></strong></p>
                        
                        <p><strong>Some sessions focus on specific programs, while other sessions cover all Health Science
                              programs. Please read the title of the session carefully before registering. </strong><strong>Health Sciences Information Sessions provide an overview of the limited access Health
                              Science programs at Valencia College and the procedures for enrolling in them. They
                              are open to current and prospective students. </strong></p>
                        
                        
                        <p><strong><u>Attendance at a Health Sciences Information Session is</u></strong> <u><strong>mandatory* if you are requesting to see a Health Sciences Advisor in-person for the
                                 following Health Science Programs</strong></u><strong>:</strong>&nbsp;&nbsp;
                        </p>
                        
                        <p><em>Allied Health </em></p>
                        
                        <ul>
                           
                           <li>Cardiovascular Technology, A.S. </li>
                           
                           <li>Dental Hygiene, A.S. </li>
                           
                           <li>Diagnostic Medical Sonography, A.S.</li>
                           
                           <li>Health Information Technology, A.S. </li>
                           
                           <li>Paramedic Technical Certificate </li>
                           
                           <li>Radiography, A.S. </li>
                           
                           <li>Respiratory Care, A.S. </li>
                           
                        </ul>
                        
                        <p><em>Nursing</em></p>
                        
                        <ul>
                           
                           <li>Nursing, Traditional Track, A.S.</li>
                           
                           <li>Accelerated Track in Nursing (ATN), A.S. </li>
                           
                           <li><em>Nursing, Valencia/UCF Concurrent ASN-BSN (check schedule below for specific dates)
                                 </em></li>
                           
                        </ul>
                        
                        <p><strong>*Students interested in pursuing EMT are strongly encouraged to attend a Health Sciences
                              Information Session.</strong></p>
                        
                        <p><strong>*Students pursuing the B.S. in Cardiopulomnary Sciences or the B.S. in Radiologic
                              &amp; Imaging Sciences or an Advanced Technical Certificate in CT or MRI are not required
                              to attend a Health Sciences Information Session. </strong></p>
                        
                        
                        
                        <p>The Information Sessions are generally held on a weekly basis. <span>Dates and times are subject to change; please check schedule before attending your
                              selected information session.</span> Additional dates will be added, please check back...
                        </p>
                        
                        <p><strong>INFORMATION SESSIONS BEGIN PROMPTLY AT THE START TIMES INDICATED BELOW. PLEASE PLAN
                              TO STAY FOR THE ENTIRE SESSION. THE INFO SESSION MAY BE CLOSED OR AT CAPACITY IF YOU
                              ARRIVE LATE. INFORMATION WILL NOT BE REPEATED FOR STUDENTS WHO ARRIVE LATE OR LEAVE
                              EARLY. <strong>WHAT TO BRING: The <a href="../../../../locations/west/departments/health-sciences/advising/admissionupdates.html">PROGRAM GUIDE</a> of the program you are interested in and a pen. </strong></strong></p>
                        
                        <h3>Information Sessions</h3>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><em>Topic</em></div>
                                 
                                 <div><em>Date/Time (2 hour session) </em></div>
                                 
                                 <div><em>Location</em></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>All Health Sciences Programs (Allied Health &amp; Nursing)</strong>                  
                                    </p>
                                    
                                    <p>Registration is not required for this session.  Please arrive early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p>August 14, 2017 - 5:30pm</p>
                                 </div>
                                 
                                 <div>West, 3-111</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Nursing Only:<br>
                                          Traditional Track, Accelerated Track in Nursing (ATN)<br>
                                          &amp;<br>
                                          Valencia/UCF Concurrent ASN-BSN Program</strong>                  
                                    </p>
                                    
                                    <p> Registration is not required for this session. Please arrive early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p>August 22nd, 2017 -  10:00am</p>
                                 </div>
                                 
                                 <div>West, 4-120 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Nursing Only:</strong> <br>
                                       <strong>Traditional Track,  Accelerated Track in Nursing (ATN)</strong><br>
                                       <strong>&amp;</strong><br>
                                       <strong>Valencia/UCF  Concurrent ASN-BSN Program</strong></p>
                                    
                                    <p>Registration is not required for this session. Please arrive  early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p>September 5th - 10:00AM</p>
                                 </div>
                                 
                                 <div>West, 3-111</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Allied  Health Only</strong></p>
                                    
                                    <h3>This Session is Cancelled</h3>
                                    
                                    <p>Registration is not required for this session. Please arrive  early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>September 11th - 2:30 PM<br>
                                    
                                    <h3>This Session is Cancelled</h3>
                                    
                                 </div>
                                 
                                 <div>East, 8-101 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Allied  Health Only</strong></p>
                                    
                                    <p>Registration is not required for this session. Please arrive  early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>September 19th - 2:30 PM</div>
                                 
                                 <div>Osceola, 1-100</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Nursing Only:</strong> <br>
                                       <strong>Traditional Track,  Accelerated Track in Nursing (ATN)</strong> <br>
                                       <strong>&amp;</strong><br>
                                       <strong>Valencia/UCF  Concurrent ASN-BSN Program</strong></p>
                                    
                                    <p>Registration is not required for this session. Please arrive  early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>September       20th - 10:00 AM</div>
                                 
                                 <div>East,       &nbsp;3-Atrium</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>All  Health Sciences Programs (Allied Health &amp; Nursing):</strong></p>
                                    
                                    <p>Registration is not required for this session. Please arrive  early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>September       25th&nbsp;- 5:30 PM</div>
                                 
                                 <div>West, 3-111</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Nursing Only:</strong> <br>
                                       <strong>Traditional Track,  Accelerated Track in Nursing (ATN)</strong> <br>
                                       <strong>&amp;</strong><br>
                                       <strong>Valencia/UCF  Concurrent ASN-BSN Program</strong></p>
                                    
                                    <p>Registration is not required for this session. Please arrive  early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>September 26th - 2:30 PM</div>
                                 
                                 <div>West, 3-111</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p dir="auto"><strong>All Health Sciences Programs (Allied Health &amp; Nursing)</strong></p>
                                    
                                    <p dir="auto"> Registration is not required for this session. Please arrive early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>October 3rd, 2017 at 10:00 am</div>
                                 
                                 <div>West, HSB 105</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p dir="auto"><strong>Allied Health Only</strong></p>
                                    
                                    <p dir="auto"> Registration is not required for this session. Please arrive early as seats will
                                       be offered on a first-come, first-served basis.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>October 10th, 2017 at 5:30 pm</div>
                                 
                                 <div>East, 8-101</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>Additional dates may be added, please check back...</strong></p>
                        
                        <p><a href="../../../../locations/west/departments/health-sciences/advising/InfoSession.html#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/advising/information-sessions.pcf">©</a>
      </div>
   </body>
</html>