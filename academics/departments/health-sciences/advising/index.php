<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Health Sciences Advising | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/advising/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Health Sciences Advising</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li>Advising</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div>
                        
                        <h2>Welcome to Health Sciences Advising</h2>
                        
                        <p>Health Sciences Advising staff offer specialized educational advisors who can guide
                           students in the successful completion of an Associate in Science Degree or Bachelor
                           of Science Degree or Certificate Program in allied health or nursing. Attendance at
                           a Health Sciences Information Session is mandatory before meeting with a Health Sciences
                           Advisor for an associate-level program but not for meeting about a bachelor-level
                           program.
                        </p>
                        
                        <p>The advising staff can answer program-specific questions, assist in career and educational
                           planning, and offer expert educational guidance for students seeking career opportunities
                           through the completion of an A.S. or B.S. Degree or a Certificate program.
                        </p>
                        
                        <p>Associate in Science students are expected to complete their educational goals and
                           be professionally marketable in a much shorter period of time than many others. This
                           is why the early development of career and education plans is essential to a successful
                           transition from school to work.
                        </p>
                        
                        <div class="row">
                           
                           <div class="col-md-8 col-sm-8">
                              <img src="/images/academics/programs/health-sciences/health-sciences-advising.jpg" class="img-responsive">
                              
                           </div>
                           
                           <div class="col-md-4 col-sm-4">
                              
                              <h5 class="val">Location</h5>
                              <i class="far fa-map-marker fa-fw"></i>West Building 1, Room 130 <br>
                              
                              <h5 class="val">Hours</h5>
                              <i class="far fa-calendar fa-fw"></i>Monday - Thursday 8 a.m. - 5 p.m.<br>
                              <i class="far fa-calendar fa-fw"></i>Friday 9 a.m. – 5 p.m. <br>
                              
                              <h5 class="val">Contact Us</h5>
                              <i class="far fa-envelope fa-fw"></i><a href="mailto:HealthScienceAdvising@valenciacollege.edu">HealthScienceAdvising@valenciacollege.edu</a> <br>
                              <i class="far fa-phone fa-fw"></i>407-582-1228<br>
                              <i class="far fa-fax fa-fw"></i>407-582-5462 (fax) 
                           </div>
                           
                           
                        </div>
                        
                        <div>
                           
                           <p>Please  note that advising staff may not always be available. Students ready to submit
                              their Health Sciences program application, may  schedule an appointment with a Health
                              Sciences Advisor or Health Sciences  Admissions Representative:
                           </p>
                           
                           <ul>
                              
                              <li>They must be a Valencia student who has  attended a Health Sciences Information Session
                                 (date of Health Sciences  Information Session attended and name of presenter are required).
                                 
                              </li>
                              
                              <li>They must send an Atlas email to <a href="mailto:HealthScienceApplications@valenciacollege.edu">HealthScienceApplications@valenciacollege.edu</a> including the date of the information session, name of the presenter, your VID  #,
                                 name of program, and a few dates/times one week ahead that would be  convenient for
                                 your appointment.
                              </li>
                              
                           </ul>
                           
                           <p>NOTE: Valencia students who are <em>pending acceptance</em> for a health sciences  program should seek advising assistance from the Allied Health
                              Program Advisor  or the Nursing Program Advisor located in the advising center of
                              their home  campus.
                           </p>
                           
                           <h3>Health Science Programs</h3>
                           
                           <h4>Associate in Science</h4>
                           
                           <p>The A.S. degree&nbsp;prepares you to enter a specialized career field in about two years.&nbsp;It
                              also transfers to the Bachelor of Applied Science program offered at some universities.
                              If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                              at a designated university.
                           </p>
                           
                           <ul>
                              
                              <li><a href="/academics/programs/health-sciences/biotechnology-lab-sciences/index.html">Biotechnology Laboratory Sciences</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/cardiovascular-technology/index.html">Cardiovascular Technology</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/dental-hygiene/index.html">Dental Hygiene</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/diagnostic-medical-sonography/index.html">Diagnostic Medical Sonography</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/emergency-medical-services-tech/index.html">Emergency Medical Services Technology</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/health-information-technology/index.html">Health Information Technology</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/nursing/nursing.html">Nursing</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/nursing/nursing-atn.html">Nursing, Accelerated Track in Nursing</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/nursing/nursing-asn.html">Nursing, Valencia/UCF Concurrent A.S.N. to B.S.N. Track</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/radiography/index.html">Radiography</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/respiratory-care/index.html">Respiratory Care</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/">Veterinary Technology</a>
                                 		
                                 <ul>
                                    <li>This AS Degree is offered through a Cooperative Agreement with <a href="http://www.spcollege.edu/vt/">St. Petersburg College</a>.  Students interested in this program may take the general education requirements
                                       at Valencia and complete the rest of the degree online through St. Petersburg College.
                                       Valencia Catalog has more information about the <a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/">Cooperative Agreement with St. Petersburg College</a>.
                                       
                                    </li>
                                 </ul>
                              </li>
                              
                           </ul>
                           
                           <h4>Bachelor of Science</h4>
                           
                           <p>The B.S. degree&nbsp;prepares you to enter an advanced position within a specialized career
                              field.&nbsp;The upper-division courses build on an associate degree and take about two
                              years to complete.
                           </p>
                           
                           <ul>
                              
                              <li><a href="/academics/programs/health-sciences/cardiopulmonary-sciences/index.html">Cardiopulmonary Sciences</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/radiologic-imaging-science/indexs.html">Radiologic and Imaging Sciences</a></li>
                              
                           </ul>
                           
                           <h4>Advanced Technical Certificate</h4>
                           
                           <p>The <a href="https://preview.valenciacollege.edu/future-students/degree-options/advanced-technical-certificates/" target="_blank">Advanced Technical Certificate</a>, an extension of a specific Associate degree program, consists of at least nine (9),
                              but less than 45, credits of college-level course work. Students who have already
                              received an eligible Associate degree and are seeking a specialized program of study
                              to supplement their associate degree may seek an Advanced Technical Certificate.
                           </p>
                           
                           <ul>
                              
                              <li><a href="/academics/programs/health-sciences/computed-tomography/index.html">Computed Tomography</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/echocardiography/index.html">Echocardiography</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/leadership-healthcare/index.html">Leadership in Healthcare</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/magnetic-resonance-imaging/index.html">Magnetic Resonance Imaging</a></li>
                              
                              <li><a href="/academics/departments/health-sciences/mammography/index.html">Mammography</a></li>
                              
                           </ul>
                           
                           <h4>Certificate</h4>
                           
                           <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                              for job advancement. Most can be completed in one year or less. Credits earned can
                              be applied toward a related A.S. degree program.
                           </p>
                           
                           <ul>
                              
                              <li><a href="/academics/programs/health-sciences/emergency-medical-services-tech/certificate.html">Emergency Medical Technology</a></li>
                              
                              <li><a href="/academics/programs/health-sciences/paramedic-tech/index.html">Paramedic Technology</a></li>
                              
                           </ul>
                           
                           <h4>Continuing Education</h4>
                           
                           <p><a href="/academics/programs/health-sciences/continuing-education/index.html" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                              development, language courses, certifications and custom course development for organizations.
                              Day, evening, weekend and online learning opportunities are available.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/advising/index.pcf">©</a>
      </div>
   </body>
</html>