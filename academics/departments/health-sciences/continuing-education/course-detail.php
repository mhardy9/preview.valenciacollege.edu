<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Continuing Education for Healthcare Professionals | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-3" role="banner">
                  <div id="logo"><a href="/index.php"><img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College Logo" data-retina="true"></a></div>
               </div>
               <nav class="col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu">
                     <div id="site_menu"><img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College" data-retina="true"></div><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div><div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Continuing Education for Healthcare Professions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/locations/">Locations</a></li>
               <li><a href="/locations/west-campus/">West Campus</a></li>
               <li><a href="/locations/west-campus/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/index.html">Health Sciences</a></li>
               <li>Continuing Education </li>
                <li>Upcoming Courses</li>
            </ul>
         </div>
      </div>
      <div>
         <main role="main">
  <div class="container margin_30">
	  <div class="main_title">
    <h2>Continuing Education - Upcoming Courses</h2>
  </div>
      <?php
$serverName = "10.10.64.76\continuity"; //serverName\instanceName
$connectionInfo = array( "Database"=>"PROD", "UID"=>"c2k_web", "PWD"=>"Institut3!");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

$MaxTitleLen="25";
$ShowEvents="7";
/*dateToDisplay will take today's date*/
    
$datetime_variable = new DateTime();
$dateToDisplay= date_format($datetime_variable, 'm/d/Y');

/*create date that the SQL will use to filter the results */

$LastDay=date_add($datetime_variable,date_interval_create_from_date_string("90 days"));
$LastDay = $LastDay->format('m/d/Y');

$sql = "SELECT 
          	SCHEDULE_NUMBER, 
            COURSE_AREA, 
            COURSE_NUMBER, 
            COURSE_SUBTITLE, 
            CRS_TITLE, 
            description_web_long, 
            LOCATION, 
            MASTER_ID, 
            MASTER_VERSION,  
            SCHEDULE_TEXT, 
            WEB_END_DATE, 
            WEB_START_DATE
          FROM 
          	vcc_CNH_Classes
          WHERE 
          	(WEB_START_DATE BETWEEN '$dateToDisplay' AND '$LastDay')   
          ORDER BY 
          	WEB_START_DATE";

$stmt = sqlsrv_query ($conn , $sql );
if( $stmt === false) {
    die( print_r( sqlsrv_errors(), true) );
}

echo "
<div class='row'>
<table class='table'>
<th scope='col'>Course</th>
<th scope='col'>Course<br>Number</th>
<th scope='col'>Dates</th>
<th scope='col'>Schedule<br>Number</th>
<th scope='col'>Location</th>
<th scope='col'>Description</th>";
while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
    {
    $CourseStartDate = $row["WEB_START_DATE"]->format('M d, Y');
    echo "
    <tr>
    <td><a href='https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=".$row["MASTER_ID"]."&master_version=".$row["MASTER_VERSION"]."&course_area=".$row["COURSE_AREA"]."&course_number=".$row["COURSE_NUMBER"]."&course_subtitle=".$row["COURSE_SUBTITLE"]."'>".$row["CRS_TITLE"]."</a></td>
    <td>".$row["COURSE_AREA"]." ".$row["COURSE_NUMBER"]."</td>
    <td>".$row["SCHEDULE_TEXT"]."</td>
    <td>".$row["SCHEDULE_NUMBER"]."</td>
    <td>".$row["LOCATION"]."</td>
    <td>".$row["description_web_long"]."</td></tr>
    ";
   
}
 echo "</table></div>";
sqlsrv_free_stmt( $stmt);
?>
      
      </div>
</main>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/locations/west-campus/departments/health-sciences/continuing-education/index.pcf">©</a>
      </div>
   </body>
</html>