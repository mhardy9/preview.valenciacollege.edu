<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>
         
         Continuing Education for Healthcare Professions | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/dietetics-nutrition.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Continuing Education for Healthcare Professions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/departments/health-sciences/continuing-education/">Continuing Education</a></li>
               <li>
                  
                  Continuing Education for Healthcare Professions
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href=""></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>Dietetics and Nutrition Online Continuing Education</p>
                        
                        <p><strong>Valencia's 
                              Department of Continuing Education for Health Professions</strong>, 
                           has long enjoyed a reputation as a provider of quality continuing 
                           education for health professionals. In keeping with that history 
                           and in response to the demand for convenient anytime courses, Valencia 
                           has sought the partnership of online content providers who share 
                           our commitment to quality and to statutory compliance in behalf 
                           of licensed health professionals.
                        </p>
                        
                        <p><strong>CEU4U</strong> 
                           offers high quality continuing education to all mental health professionals 
                           as well as several other health care professions. The <strong>CEU4U</strong> 
                           course offerings allow Valencia Enterprises to vastly expand our 
                           catalog and to continue to provide our students with the very best 
                           in professional development courses and programs. Included among 
                           the <strong>CEU4U</strong> professional offering categories are: 
                           
                        </p>
                        
                        <p><a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=3">CE 
                              for Clinical Social Workers </a>(NASW Approved)
                        </p>
                        
                        <p><a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=4%20">CE 
                              for Marriage and Family Therapists </a>(NBCC Approved)
                        </p>
                        
                        <p><a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=4">CE 
                              for Mental Health Counselors </a>(NBCC Approved)
                        </p>
                        
                        <p><a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=2">CE 
                              for Psychologists </a> (APA Approved)
                        </p>
                        
                        <p><a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=1%20">CE 
                              for Nursing </a>(FBN Approved)
                        </p>
                        
                        <p><a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=6">CE 
                              for Dieticians and Nutritionists</a>&nbsp; (CDR Approved)
                        </p>
                        
                        <p>As 
                           a Valencia Continuing Education student, you will be re-directed 
                           from this page to the <strong>CEU4U </strong>website where you will 
                           register, complete the course or courses of your choice, and receive 
                           your certificate. Presently, <strong>CEU4U </strong>will report 
                           continuing education contact hours for Nurses to CE Broker in compliance 
                           with the statutory requirements of the Florida Department of Health. 
                           All others may self report to CE Broker to meet licensing requirements. 
                           
                        </p>
                        
                        <p>Thank 
                           you for continuing to entrust your professional development needs 
                           to Valencia's Department of Continuing Education for Health Professions. 
                           &nbsp; We look forward to your feedback and welcome your suggestions 
                           regarding future training.
                        </p>
                        
                        <p><strong>In agreement with</strong><br>
                           <img alt="CEU4U" height="75" src="../../../../locations/west/departments/health-sciences/continuing-education/lgceu4u.jpg" width="160">
                           
                        </p>
                        
                        
                        <p><strong>For 
                              more information or to register, please call 407-582-1793. <br>
                              To register for courses via web: 
                              <a href="../../../../locations/west/departments/health-sciences/continuing-education/Courses/index.html">Course Dates &amp; Registration </a></strong></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>APPROVED PROVIDERS</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 <div>
                                    <a href="http://cebroker.com/">Florida Department of Health</a><br>
                                    <strong>Provider # 50-2109</strong><br><br>
                                    <a href="http://floridacertificationboard.org/">Florida Certification for Addiction Professionals</a><br> <strong>Provider #16-SS</strong><br> 
                                 </div>
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Dentistry</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Clinical Social Work, Marriage &amp; Family Therapy, Mental Health Counseling</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Nursing</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Nutritionists and Dietitians</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Respiratory Therapists</div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        <h3>COURSE CATALOG</h3>
                        
                        <p>Valencia currently offers courses and programs in areas such as Basic Life Support,
                           Healthcare Mandatory and License Renewal Courses, and Dental Education.
                        </p>
                        
                        <p><a href="../../../../locations/west/departments/health-sciences/continuing-education/Courses/index.html">View Course Catalog</a></p>
                        
                        
                        
                        
                        
                        
                        
                        <h3>UPCOMING COURSES</h3>
                        
                        <div>
                           
                           <div>
                              <div>
                                 
                                 <div>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=8153%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 8:30 a.m. - 12:30 p.m. -  West Campus Bldg HSB Rm 118 - ">
                                          
                                          AIDS 104 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=8153%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 8:30 a.m. - 12:30 p.m. -  West Campus Bldg HSB Rm 118 - ">
                                          
                                          AIDS 104 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=736&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9500%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 9 a.m. - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers course covers core material such as adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS Health Care Provider 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3105&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9510%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 9 a.m. - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers course covers core material such as adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS HC PRO for A.S. Students 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=737&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9501%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 1 - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers Recertification course reviews core material, paying attention to protocol changes. For the purpose of recertification, the instructor performs a BLS skill check off for adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS Health Care Provider Recert 
                                          </a></p>
                                    
                                    
                                    
                                    <br><a href="../../../../locations/west/departments/health-sciences/continuing-education/incl_calendar_continuity_Detail.html">View All Course</a> 
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/continuing-education/dietetics-nutrition.pcf">©</a>
      </div>
   </body>
</html>