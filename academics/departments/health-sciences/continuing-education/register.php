<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>How to Register  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/register.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Continuing Education for Healthcare Professions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/departments/health-sciences/continuing-education/">Continuing Education</a></li>
               <li>How to Register </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        <h2>How to Register</h2>
                        
                        <h3>Online Registration</h3>
                        
                        <p>To register online:</p>
                        
                        <ol>
                           
                           <li>Visit the course <a href="course-catalog.html">catalog</a> and select the course or program you desire.
                           </li> 
                           
                           <li>Choose the section (date) and click on the shopping cart to proceed.</li>
                           
                           <li>Pay online after you have registered for your courses in order to secure your seat.</li>
                           
                           <li>Print your confirmation after you have paid for your course(s).</li>        
                           
                        </ol>
                        
                        <h3>Fax your Registration Form</h3>
                        
                        <ol>
                           
                           <li>Print: <a href="documents/valencia-continuing-education-healthcare-registration-form.pdf" target="_blank">Registration Form</a>
                              
                           </li>
                           
                           <li>Fax to: (407) 582-1580 (Secure Fax)</li>
                           
                        </ol>
                        
                        <h3>Mail your Registration Form</h3>
                        
                        <ol>
                           
                           <li>Print:<a href="documents/valencia-continuing-education-healthcare-registration-form.pdf" target="_blank">Registration Form</a>
                              
                           </li>
                           
                           <li>Mail with payment to:
                              
                              <blockquote>Valencia College<br>
                                 Continuing 
                                 Education for Health Professions<br>
                                 CN-Cashier<br>
                                 P.O. Box 3028, MC 4-47 <br>
                                 Orlando, FL 32802-3028 <br>
                                 
                              </blockquote>
                              
                           </li>
                           
                        </ol>
                        
                        <h3>Call 407-582-1793</h3>
                        
                        <p>Please have course name, schedule number and credit card information available.<br>
                           Course confirmations are mailed to the preferred address provided.
                        </p>
                        
                        <h3>In Person</h3>
                        
                        <p>Visit us at <a href="http://maps.google.com/maps?q=1800+South+Kirkman+Road,+Orlando,+FL+32811&amp;iwloc=A&amp;hl=en">West 
                              Campus</a>, 1800 S Kirkman Rd, Orlando, Building HSB 200
                        </p>
                        
                        
                        
                        <h2>How to Pay</h2>
                        
                        <ul>
                           
                           <li>You can pay for courses online, over the phone, in person, or through the mail. </li>
                           
                           <li>Valencia accepts cash, money orders, checks, VISA, MasterCard, 
                              American Express and Discover.
                              
                           </li>
                           
                           <li>Checks and money orders must be payable to Valencia College.
                              
                           </li>
                           
                           <li>Cash payments may only be made in person at the Business Office located at the West,
                              East, or Osceola campus 
                           </li>
                           
                        </ul>
                        
                        
                        <h2>Course Registration Information</h2>
                        
                        <ul>                
                           
                           <li>Students must be at least 16 years of age
                              
                           </li>
                           
                           <li>Walk-in registrations are accepted at the <a href="http://maps.google.com/maps?q=1800+South+Kirkman+Road,+Orlando,+FL+32811&amp;iwloc=A&amp;hl=en">West 
                                 Campus</a>, Building 2-208
                              
                           </li>
                           
                           <li>Payment must accompany registration form </li>
                           
                        </ul>
                        
                        
                        <h2>Cancellation</h2>
                        
                        <ul>
                           
                           <li>If a course cancels, and you have preregistered, we will 
                              attempt to notify you.
                              
                           </li>
                           
                           <li>A notice will also be placed at the entrance to the classroom.
                              
                           </li>
                           
                           <li>Your tuition will be refunded according to our refund 
                              policy. 
                           </li>
                           
                        </ul>
                        
                        
                        <h2>Early Registration</h2>
                        
                        <ul>
                           
                           <li>Encouraged due to limited course size
                              
                           </li>
                           
                           <li>Registration at the door is based on space availability
                              
                           </li>
                           
                           <li>Enrollment is on a first-come basis
                              
                           </li>
                           
                           <li>Low enrollment may result in course cancellation <br>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h2>Refund Policy</h2>
                        
                        <ul>
                           
                           <li>Full refunds are given when a class is canceled by the 
                              college. Course fees will be fully refunded if request is 
                              received at least 5 business days prior to course start 
                              date. Please note that course fees for eLearning courses 
                              are non refundable once the course has been accessed.<br>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h2>Fees</h2> 
                        
                        <ul>
                           
                           <li>Fees are subject to change without notice. </li>
                           
                        </ul>
                        
                        
                        <h2>Refund of Registration Fees</h2>
                        
                        <ul>
                           
                           <li>Refund checks will be mailed within 15 working days.
                              
                           </li>
                           
                           <li>A refund will be issued back to the credit card. </li>
                           
                        </ul>
                        
                        
                        <h2>Businesses/Organizations:</h2>
                        
                        <p>In addition to the payment 
                           options listed above, businesses/organizations may submit purchase 
                           orders and/or formal letters of authorization that must accompany 
                           registration requests. 
                        </p>
                        
                        <p>Faxed registrations must 
                           include a copy of the purchase order or formal letter of authorization 
                           with originals sent to the Business Office. 
                        </p>
                        
                        <p>Purchase orders and/or 
                           formal letters of authorization must include: 
                        </p>
                        
                        <ul>
                           
                           <li>Company letterhead with a typed billing address, not handwritten
                              
                           </li>
                           
                           <li>Contact name, title, and phone number of person authorized to 
                              purchase for business/organization
                              
                           </li>
                           
                           <li>State the following: "This letter authorizes Valencia 
                              College to bill [company name] for:"
                              
                           </li>
                           
                           <li>What is being authorized; i.e., tuition, books, etc. Student(s) 
                              name(s), Social Security number(s), course number(s), and dates 
                              of class(es)
                              
                           </li>
                           
                           <li>NOTE: Letters of authorization signed by students are not accepted. </li>
                           
                        </ul>
                        
                        <h2>Confirmations </h2>
                        
                        <p>All confirmations are mailed for all registrations except Web.</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/continuing-education/register.pcf">©</a>
      </div>
   </body>
</html>