<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Course Catalog  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/course-catalog.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Continuing Education for Healthcare Professions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/departments/health-sciences/continuing-education/">Continuing Education</a></li>
               <li>Course Catalog </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <h2>Course Catalog</h2>
                     
                     <p>Valencia currently offers courses and programs in the areas listed below.&nbsp; We anticipate
                        expanding the list of professions served and adding new course and program offerings.
                        Whether you are seeking continuing education to meet your license renewal requirements
                        or enhance your professional skills, Valencia seeks to be your provider of choice.
                        Visit this growing catalog frequently. 
                     </p>
                     
                     <h3>
                        <a name="aha" id="aha"></a>American Heart Association
                     </h3>
                     <a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=348">CPR/BLS for Healthcare Providers - Available Courses </a>
                     
                     <h4>Basic Life Support for Healthcare Providers </h4>
                     
                     <p>AHA Training Center: Education with Heart, Alice Forthman</p>
                     
                     <p>AHA Training Site: Valencia College</p>
                     
                     <p>Valencia College  conducts live certification and re-certification classes.  Presently,
                        we only provide American Heart Association Basic Life Support for Healthcare Providers
                        and no other AHA certifications.
                     </p>
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <h4>Valencia Students (must have Valid Valencia VID)</h4>
                           
                           <ul>
                              
                              <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3105&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9510%20%20%20&amp;course_subtitle=00">CPR/BLS HC PRO Certification for Valencia Students</a></li>
                              
                              <li>
                                 <a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3106&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9511%20%20%20&amp;course_subtitle=00">CPR/BLS HC PRO Re-certification for Valencia Students</a> 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <h4>Licensed Healthcare Providers</h4>
                           
                           <ul>
                              
                              <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=736&amp;master_version=1&amp;course_area=CNH&amp;course_number=9500&amp;course_subtitle=00">CPR/BLS Health Care Provider Certification</a></li>
                              
                              <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=737&amp;master_version=1&amp;course_area=CNH&amp;course_number=9501&amp;course_subtitle=00">CPR/BLS Health Care Provider Re-certification </a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <p> You must take the full certification if you do not:</p>
                           
                           <ul>
                              
                              <li>hold a current, unexpired,  AHA BLS for Healthcare Provider card</li>
                              
                              <li>present your current card  upon arrival for class</li>   
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <p>Visit <a href="http://cebroker.com">CE Broker </a> to learn if this course is approved for contining education by your licensing board.
                     </p>
                     
                     <hr class="styled_2">          
                     
                     <h3>
                        <a name="social" id="social"></a>Clinical Social Work, Marriage &amp; Family / Mental Health 
                     </h3>
                     
                     <p><a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=202">Licensed Clinical Counseling - Available Courses </a></p>          
                     
                     <p>Valencia is a well-know provider of excellent courses and programs for mental health
                        professionals licensed under Florida Board 491. We offer continuing education that
                        meets license renewal requirements and focuses on trends in professional development.
                        
                     </p>
                     
                     
                     <hr class="styled_2">
                     
                     <h3>
                        <a name="dental" id="dental"></a>Dental Hygiene
                     </h3>
                     
                     <p><a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=199">Dental Education - Available Courses </a></p>
                     
                     <h4>Featured: Local Anesthesia for Dental Hygienists</h4>
                     
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <p><strong>This is a limited access course. This course is not currently scheduled. Call 407-582-1793
                                 for information. </strong></p>
                           
                           <p>Both Parts 1 and 2 are required for completion of the course and achievement of certification.
                              
                           </p>
                           
                           <p>Per the  Florida Board of Dentistry,  Local Anesthesia certification for Dental Hygienists
                              requires completion of 60 hours of didactic and clinical components. The didactic
                              portion of the course is delivered via an online course and two days of lecture. The
                              exam will be administered during this part of the course and prior to the clinical
                              course; a passing score of 75% must be achieved.
                           </p>    
                           
                        </div>      
                        
                        <div class="col-md-4">
                           
                           <h4>PART 1</h4>
                           
                           <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3515&amp;master_version=1&amp;course_area=CNH&amp;course_number=8102&amp;course_subtitle=00">Local Anesthesia for Dental Hygienists</a><br> Online &amp; Clinical Learning Experience
                           </p>
                           
                           <p>Student must complete all requirements in this course and pass the exam with a score
                              of 75% or better to proceed to the clinical portion of the course.
                           </p>
                           
                           <p>Required textbook is included in the course fee and will be delivered to the student
                              during the mandatory orientation: <u>Local Anesthesia for the Dental Hygienist</u> ISBN 9780323073714
                           </p>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <h4>PART 2</h4>                
                           
                           <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3527&amp;master_version=1&amp;course_area=CNH&amp;course_number=8103&amp;course_subtitle=00">Local Anesthesia for Dental Hygienists</a><br>Clinical Application
                           </p>
                           
                           <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3515&amp;master_version=1&amp;course_area=CNH&amp;course_number=8102&amp;course_subtitle=00">Local Anesthesia for Dental Hygienists</a><br>Online &amp; Clinical Learning Experience
                           </p>
                           
                           <p>NOTE: Participants must sign an informed consent to receive multiple injections during
                              this portion of the course. If they are unable to participate as a patient, they will
                              need to identify a person to attend the clinical as their patient for this purpose.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     
                     <hr class="styled_2">
                     
                     <h3>
                        <a name="mandatory" id="mandatory"></a>Florida Mandatory/Renewal for Health Professions
                     </h3>
                     
                     <p><a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=200">Health Care License Renewal - Avaliable Courses</a></p>
                     
                     <p>The courses in this category are designed to meet the Florida mandatory requirements
                        for health professionals. We have designed a one day seminar style presentation of
                        the most common of those requirements. Consult your licensing board for requirements
                        specific to your profession. 
                     </p>
                     
                     <ul>
                        
                        <li>
                           <a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH&amp;course_number=8153&amp;course_subtitle=00">AIDS 104</a> (Four Hours)
                        </li>
                        
                        <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=578&amp;master_version=1&amp;course_area=CNH&amp;course_number=8161&amp;course_subtitle=00">Domestic Violence for Healthcare Professionals</a></li>
                        
                        <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=585&amp;master_version=1&amp;course_area=CNH&amp;course_number=8182&amp;course_subtitle=00">Prevention of Medical Errors</a></li>
                        
                     </ul>
                     
                     
                     
                     <hr class="styled_2">
                     
                     <h3>
                        <a name="mental-health" id="mental-health"></a>Mental Health Professions
                     </h3>
                     <a href="#"> - Available Courses</a>
                     
                     <h4>Professional Ethics and Boundaries</h4>
                     
                     <p>This advanced course meets the Board 491 requirement for a three-hour ethics course
                        as required in Rule Chapter 64B4-6.001 (2)(a) for Renewal of Active License.
                     </p>
                     
                     
                     <p>The course explores difficult issues such as</p>
                     
                     <ul>
                        
                        <li>multiple/dual relationships</li>
                        
                        <li>boundary crossings and transgressions</li>
                        
                        <li>application of several ethical decision-making models</li>
                        
                        <li>asymmetrical power dynamics</li>
                        
                     </ul>
                     
                     
                     <p>Participants receive current information about their profession’s Code of Ethics and
                        work directly with professional codes in case scenarios. Participants also explore
                        some of their own vulnerabilities as part of this course.
                     </p>
                     
                     <hr class="styled_2">
                     
                     <h3>
                        <a name="nursing" id="nursing"></a>Nursing
                     </h3>
                     
                     <p><a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=204">Nursing - Available Courses</a></p> 
                     
                     <p>Quality nursing courses and specialty programs are conveniently available in an online
                        venue. The catalog is continually expanded to reflect current workforce trends in
                        knowledge demands. 
                     </p>
                     
                     <hr class="styled_2">
                     
                     <h3>
                        <a name="nutrition" id="nutrition"></a>Personal Fitness, Dietetics, and Nutrition
                     </h3>
                     
                     <p><a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=219">Dietetics and Nutrition - Available Courses</a></p>
                     
                     <p><a href="http://www.valencia.efslibrary.net/">Personal Fitness - Available Courses</a></p>
                     
                     <p>Valencia now offers a large selection of online courses which are approved  for Continuing
                        Education by the Commission on Dietetic Registration (CDR), the credentialing agency
                        for the American Dietetic Association (ADA) and the Florida Council of Dietetics and
                        Nutrition.
                     </p>
                     
                     
                     <div class="row">
                        
                        <div class="col-md-6">
                           
                           <h4>Continuing Education University for You (CEU4U)</h4>
                           
                           <p>Valencia's Department of Continuing Education for Health Professions, has long enjoyed
                              a reputation as a provider of quality continuing education for health professionals.
                              In keeping with that history and in response to the demand for convenient anytime
                              courses, Valencia has sought the partnership of online content providers who share
                              our commitment to quality and to statutory compliance in behalf of licensed health
                              professionals.
                           </p>
                           
                           
                           <p>CEU4U offers high quality continuing education to all mental health professionals
                              as well as several other health care professions. The CEU4U course offerings allow
                              Valencia Enterprises to vastly expand our catalog and to continue to provide our students
                              with the very best in professional development courses and programs. Included among
                              the CEU4U professional offering categories are:
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=3">CE 
                                    for Clinical Social Workers </a>(NASW Approved)
                              </li>
                              
                              <li>
                                 <a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=4%20">CE 
                                    for Marriage and Family Therapists </a>(NBCC Approved)
                              </li>
                              
                              <li>
                                 <a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=4">CE 
                                    for Mental Health Counselors </a>(NBCC Approved)
                              </li>
                              
                              <li>
                                 <a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=2">CE 
                                    for Psychologists </a> (APA Approved)
                              </li>
                              
                              <li>
                                 <a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=1%20">CE 
                                    for Nursing </a>(FBN Approved)
                              </li>
                              
                              <li>
                                 <a href="http://www.ceu4u.com/v2/catalog.php?pcode=valencia&amp;aid=6">CE 
                                    for Dieticians and Nutritionists</a>&nbsp; (CDR Approved)
                              </li>
                              
                           </ul>
                           
                           <p>As a Valencia Continuing Education student, you will be re-directed from this page
                              to the CEU4U website where you will register, complete the course or courses of your
                              choice, and receive your certificate. Presently, CEU4U will report continuing education
                              contact hours for Nurses to CE Broker in compliance with the statutory requirements
                              of the Florida Department of Health. All others may self report to CE Broker to meet
                              licensing requirements.
                           </p>
                           
                           
                        </div>
                        
                        <div class="col-md-6">
                           
                           
                        </div>
                        
                     </div>   
                     
                     <hr class="styled_2">
                     
                     <h3>
                        <a name="pharmacy" id="pharmacy"></a>Pharmacy Technician
                     </h3>        
                     
                     <p><a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=226">Pharmacy Technician - Available Courses</a></p>
                     
                     <p>This nationally recognized Pharmacy Technician online course and training program
                        teaches the skills needed to gain employment as a Pharmacy Tech in either the hospital
                        or retail setting. The objective of the Pharmacy Technician Program is to provide
                        graduates with the skills and knowledge that will enable them to qualify for entry-level
                        positions in pharmacies as well as prepare for national certification. This program
                        pairs students with an instructor for one-on-one assistance and includes a forty-hour
                        externship. All materials are included. This online certificate program is offered
                        in partnership with major colleges, universities, and other accredited education providers.
                        
                     </p>
                     
                     <hr class="styled_2">
                     
                     
                     <h3>
                        <a name="psychologists" id="psychologists"></a>Psychologists
                     </h3> 
                     
                     <p><a href="https://c2k.valenciacollege.edu/ce_health/Heading.asp?heading_id=227">Psychologists - Available Courses</a></p>
                     
                     <p>Valencia has introduced a catalog of quality online courses approved by the American
                        Psychological Association for professionals licensed under Florida's Board 490.
                     </p>
                     
                     <hr class="styled_2">
                     
                     <p>For registration options, visit<a href="register.html"> Ways to Register </a></p>
                     
                     <p>For more information, please call 407-582-1793.</p>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/continuing-education/course-catalog.pcf">©</a>
      </div>
   </body>
</html>