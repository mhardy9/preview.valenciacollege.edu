<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Professional Resources  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/professional-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Continuing Education for Healthcare Professions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/departments/health-sciences/continuing-education/">Continuing Education</a></li>
               <li>Professional Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <h2>Professional Resources</h2>
                     
                     <h3>Professional Associations</h3>
                     
                     <ul>
                        
                        <li>
                           <a href="http://www.mhccf.com">Mental Health Counselors of Central Florida </a> 
                        </li>
                        
                        <li><a href="http://www.floridamhca.org">Florida Mental Health Counselors Association</a></li>
                        
                        <li><a href="http://www.famft.org/">Florida Association of Marriage and Family Therapists</a></li>
                        
                        <li><a href="http://www.naswfl.org/">National Association of Social Workers: Florida Chapter</a></li>
                        
                        <li><a href="http://www.cdda.org/">Central Florida District Dental Association </a></li>
                        
                        <li><a href="http://www.fdha.org/">Florida Association of Dental Hygenists</a></li>
                        
                        <li><a href="http://www.floridanurse.org/">Florida Nurses Association</a></li>
                        
                        <li><a href="http://www.fadaa.org/">Florida Alcohol and Drug Abuse Association</a></li>
                        
                        <li><a href="http://www.flapsych.com/">Florida Psychological Association </a></li>
                        
                     </ul>
                     
                     <h3>Licensing and Certification Websites</h3>
                     
                     <ul>
                        
                        <li><a href="https://www.cebroker.com/public/pb_index.asp">CE Broker: Florida's Continuing Education (CE) Compliance Determination System</a></li>
                        
                        <li><a href="http://www.doh.state.fl.us/mqa/">Department of Health /licensure requirements </a></li>
                        
                        <li><a href="http://www.floridacertificationboard.org/">Requirements for Florida Addiction Certification </a></li>
                        
                     </ul>
                     
                     <h3>Valencia Approved Continuing Education Provider</h3>
                     
                     <p>Florida Department of Health Provider #50-2109<br>
                        
                     </p>
                     
                     <ul>
                        
                        <li>Dentistry </li>
                        
                        <li>Clinical Social Work, Marriage and Family Therapy, Mental Health Counseling </li>
                        
                        <li>Nursing </li>
                        
                        <li>Nutritionists and Dietitians </li>
                        
                        <li>Respiratory Therapists </li>
                        
                     </ul>
                     
                     <p>Many of our courses are approved on a complementary basis by other boards.  To determine
                        if specific courses are approved for your Department of Health licensing board, visit
                        <a href="http://www.cebroker.com">http://www.cebroker.com</a></p>
                     
                     <p>Florida Certification Board for Addiction Professionals Provider #16SS </p>
                     
                     <p>American Heart Association Training Site </p>        
                     
                     <h3>Education</h3>
                     
                     <h3>Graduate Degrees Preparatory to Licensure in Counseling  </h3>
                     
                     <ul>
                        
                        <li> <a href="http://www.rollins.edu/holt/graduatecounseling/">Rollins College </a> 
                        </li>
                        
                        <li> <a href="http://www.stetson.edu/academics/custom/program.php?id=158">Stetson University </a> 
                        </li>
                        
                        <li> <a href="http://www.ucfcounselored.org/">UCF </a> 
                        </li>
                        
                        <li> <a href="http://www.cas.usf.edu/social-work">USF </a> 
                        </li>
                        
                     </ul>
                     
                     <h3>Preparatory to Licensure in Nursing and Allied Health Professions  </h3>
                     
                     <ul>
                        
                        <li> <a href="../index.html">Valencia College A.S. Degrees in Health Sciences</a> 
                        </li>
                        
                     </ul>
                     
                     <h3>Technical Health Care Workers: Orange County </h3>
                     
                     <ul>
                        
                        <li> <a href="https://www.ocps.net/cs/workforce/Documents/Programs/HealthSci.pdf">Offered through  Technical Centers</a> 
                        </li>
                        
                     </ul>
                     
                     <h3>Technical Health Care Workers: Osceola County </h3>
                     
                     <ul>
                        
                        <li> <a href="http://www.teco.osceola.k12.fl.us/programs/health/patientcare.aspx">Patient Care Occupations</a> 
                        </li>
                        
                        <li><a href="http://www.teco.osceola.k12.fl.us/programs/health/nursing.aspx">Practical Nursing </a></li>
                        
                        <li> <a href="http://www.teco.osceola.k12.fl.us/programs/health/medical.aspx">Medical Assistant </a> 
                        </li>
                        
                        <li> <a href="http://www.teco.osceola.k12.fl.us/programs/health/phlebotomy.aspx">Phlebotomy</a> 
                        </li>
                        
                     </ul>
                     
                     <h3>Technical Health Care Workers: Seminole County </h3>
                     
                     <ul>
                        
                        <li> <a href="http://www.scc-fl.edu/nursing/degreesdiplomas/practicalnursing/lpn-vc.php">Practical Nursing </a> 
                        </li>
                        
                        <li>
                           <a href="http://www.scc-fl.edu/nursing/degreesdiplomas/nursingassistant/">Certified Nursing Assistant</a> 
                        </li>
                        
                        <li> <a href="http://www.scc-fl.edu/pta/">Physical Therapist Assistant (PTA)</a> 
                        </li>
                        
                     </ul>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/continuing-education/professional-resources.pcf">©</a>
      </div>
   </body>
</html>