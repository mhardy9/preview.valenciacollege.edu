<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Continuing Education for Healthcare Professionals | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Continuing Education for Healthcare Professionals</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li>Continuing Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Continuing Education</h2>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>Valencia currently offers courses and programs in multiple healthcare areas. We are
                        always expanding the list of professions served and adding new course and program
                        offerings. Whether you are seeking continuing education to meet your license renewal
                        requirements or enhance your professional skills, Valencia seeks to be your provider
                        of choice.
                     </p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <h3>Featured Continuing Education Health Sciences Courses</h3>
                     
                     <div class="row">
                        
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/continuing-education/course-catalog.php#BLS"><img src="/images/academics/programs/health-sciences/featured-health-info-tech.jpg" alt="Featured Course - Basic Life Support (BLS) for Healthcare Profiders"></a>
                           
                           <h3>American Heart Association</h3>
                           
                           <p>Valencia College conducts live certification and re-certification classes for Basic
                              Life Support (BLS) for Healthcare Profiders.
                           </p>
                           
                           <div class="button-outline">Learn More</div>
                           
                        </div>
                        
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/continuing-education/course-catalog.php#mandatory"><img src="/images/academics/programs/health-sciences/featured-ems.png" alt="Featured Program - Emergency Medical Services Technology"></a>
                           
                           <h3>Florida Mandatory/Renewal for Health Professions</h3>
                           
                           <p>The courses in this category are designed to meet the Florida mandatory requirements
                              for health professionals.
                           </p>
                           
                           <div class="button-outline">Learn More</div>
                           
                        </div>
                        
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/continuing-education/course-catalog.php#mentalhealth"><img src="/images/academics/programs/health-sciences/featured-cardiovascular.png" alt="Featured Program - Cardiovascular Technology"></a>
                           
                           <h3>Mental Health Professions</h3>
                           
                           <p>Professional Ethics and Boundaries -This advanced course meets the Board 491 requirement
                              for a three-hour ethics course as required in Rule Chapter 64B4-6.001 (2)(a) for Renewal
                              of Active License.
                           </p>
                           
                           <div class="button-outline">Learn More</div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               <!--End row-->
               
               <div class="container margin-30 ">
                  
                  <div class="row">
                     
                     <div class="col-md-6">
                        
                        <h3>Upcoming Courses</h3>
                        <?php include ($_SERVER["DOCUMENT_ROOT"]."/_resources/php/ce-health-course-short.inc"); 
		?>
                        <a href="/academics/departments/health-sciences/continuing-education/course-detail.php">
                           <div class="button-outline">View All Courses</div></a>
                        
                     </div>
                     
                     <div class="col-md-6">
                        
                        <h3>Course Sections for Valencia A.S. Degree Students</h3>
                        
                        <p>Health Sciences Continuing Education offers specific course sections for currently
                           enrolled Valencia A.S. degree seeking students. Student course fees are available
                           with a valid Valencia ID for the following Courses.
                        </p>
                        
                        <ul>
                           
                           <li><strong><a href="/academics/departments/health-sciences/BLSforHealthcareProviders.php">Certification</a> 8 hours </strong></li>
                           
                           <li><strong><a href="/academics/departments/health-sciences/BLSforHealthcareProviders.php">Re-Certification </a>4 hours </strong></li>
                           
                           <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH&amp;course_number=8153&amp;course_subtitle=00">AIDS 104</a> 4 Hours
                           </li>
                           
                           <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=578&amp;master_version=1&amp;course_area=CNH&amp;course_number=8161&amp;course_subtitle=00">Domestic Violence for Healthcare Professionals </a>2 hours
                           </li>
                           
                           <li><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=585&amp;master_version=1&amp;course_area=CNH&amp;course_number=8182&amp;course_subtitle=00">Prevention of Medical Errors</a> 2 hours
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
                  <hr class="styled_2">
               </div>
               
               <div class="container margin-30 ">
                  
                  <div class="row">
                     
                     <div class="col-md-8">
                        
                        <h3>Prescription for the Perfect Job</h3>
                        
                        <p>How do you define a "good day at work"? By saving a life? Nursing someone back to
                           health? Offering comfort and encouragement in tense situations? Are you the type of
                           person who feels you can make a difference and longs to do something personally meaningful?
                           If so, the health sciences field is just the place where that can happen.
                        </p>
                        
                        <p>There are few occupations where your day-to-day activities can impact so many people
                           in such a significant way. Whether you're an RN caring for newborns in a Pediatrics
                           ward, a Radiographer taking x-rays of an injured patient, or a Paramedic making split
                           second decisions where your actions may determine life or death, you'll be involved
                           in a field that is as extraordinarily rewarding as it is challenging.
                        </p>
                        
                        <p>At Valencia, you can choose from some of the fastest-growing high-wage, high-skill
                           occupations in the country. From Dental Hygiene and Cardiovascular Technology to Radiography,
                           Nursing and Respiratory Care, you'll be on your way to an exciting career. During
                           the next 10 years, every healthcare profession in Florida is projected to increase
                           at a rate of 3.5% annually - creating new jobs every year. And as humans are living
                           longer, the demand for these types of jobs will grow exponentially.
                        </p>
                        
                        <p>People have their reasons for going into the healthcare field, but one thing they
                           all have in common, is the desire to help others. With today's high demand for skilled
                           healthcare workers, Valencia can help you get on the right track and develop the skills
                           and expertise you need to be successful in this "hot" career field.
                        </p>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <h3>Job Outlook</h3>
                        
                        <ul>
                           
                           <li>Occupations in the healthcare field continue to be among the fastest-growing due to
                              technological advances in patient care, an increase in the demand for healthcare services,
                              and the long-term care needs of an increasing elderly population.
                           </li>
                           
                           <li>Health Services will add 2.8 million new jobs nationally in the next decade as demand
                              for health care increases because of an aging population and longer life expectancy.
                           </li>
                           
                           <li>Skilled health care workers are in great demand, and the career options for these
                              workers continue to increase.
                           </li>
                           
                           <li>Registered nurses are projected to have the second largest number of new jobs among
                              all occupations.
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
                  <hr class="styled_2">
               </div>
               
               <div class="container margin-30 ">
                  
                  <div class="row">
                     
                     <h3>Meet Our Team</h3>
                     
                     <div class="col-md-6"><img class="img-circle styled_2" src="/images/academics/programs/health-sciences/carol-millenson.jpg" alt="Carol Millenson">
                        
                        <h4>Carol J. Millenson</h4>
                        
                        <p>Manager, Continuing Education and Clinical Compliance</p>
                        
                        <p>I have been with Valencia College since September, 1998 and have managed Continuing
                           Education for Health Professions. I am currently the Manager for continuing Education
                           and Clinical Compliance for our Nursing and Allied Health Divisions. My work includes
                           collaboratively developing and managing the delivery of continuing education and specialized
                           programs for a variety of health care fields. I also manage all aspects of clinical
                           compliance for our A.S. degree programs.
                        </p>
                        
                        <p>I hold a Masters Degree in Marriage and Family Counseling from Stetson University
                           and a Bachelors Degree in Sociology and Education from Eastern Nazarene College .
                           My specialized training includes the CAPT Qualified Administrator for the Myers-Briggs
                           Type Indicator© and the Organizational Team and Culture Indicator©. I am also a certified
                           facilitator for Development Dimensions, International.
                        </p>
                        
                        <p>My career spans three industries including bank data processing management, family
                           counseling, and education. Prior to joining Valencia in September 1998, I served for
                           five years as Director of Patient and Family Services in Hospice care and served clients
                           in private practice counseling. I came to Valencia with a broad health care perspective,
                           having spent several years in medical settings. Education and training are common
                           threads throughout my 30 years of professional experience.
                        </p>
                        
                        <p>I am the mother of one adult child, Clay, who lives in New Smyrna Beach, FL. I live
                           with my sweet black lab, Abby. I enjoy reading, writing, gardening, nurturing friendships,
                           and walking on our beautiful Volusia County beaches.
                        </p>
                        
                     </div>
                     
                     <div class="col-md-6"><img class="img-circle styled_2" src="/images/academics/programs/health-sciences/ozella-knox.jpg" alt="Ozella Knox">
                        
                        <h4>Ozella Knox</h4>
                        
                        <p>Training Support Specialist</p>
                        
                        <p>As Training Support Specialist for Continuing Education for Health Professions, I
                           assist the Manager, instructors and students in all the health professions. Our courses
                           are geared toward licensed professionals maintaining their licensure renewal and students
                           seeking licensure in the professional health field.
                        </p>
                        
                        <p>I ensure that students are appropriately registered, classes are properly scheduled,
                           instructors are up to date with their assignments, and the classrooms are equipped
                           with presentation materials and handouts for an efficient class. Upon completion of
                           the courses, I ensure students receive their certificates and their credits are properly
                           reported to regulatory agencies.
                        </p>
                        
                        <p>I have a B.S. in Business Administration from the University of Central Florida and
                           an M.S. in Leadership from Nova Southeastern University. I am originally from Melbourne
                           and currently reside in Winter Park, Florida.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <hr class="styled_2">
               </div>
               
               <div class="container margin-30">
                  
                  <h3>Approved Providers</h3>
                  
                  <p><a href="http://cebroker.com/" target="_blank">Florida Department of Health</a><br> Provider # 50-2109
                  </p>
                  
                  <p><a href="http://floridacertificationboard.org/" target="_blank">Florida Certification for Addiction Professionals</a><br> Provider #16-SS
                  </p>
                  
                  <ul>
                     
                     <li>Dentistry</li>
                     
                     <li>Clinical Social Work, Marriage &amp; Family Therapy, Mental Health Counseling</li>
                     
                     <li>Nursing</li>
                     
                     <li>Nutritionists and Dietitians</li>
                     
                     <li>Respiratory Therapists</li>
                     
                  </ul>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/continuing-education/index.pcf">©</a>
      </div>
   </body>
</html>