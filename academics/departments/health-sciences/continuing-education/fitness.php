<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>
         
         Continuing Education for Healthcare Professions | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/fitness.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Continuing Education for Healthcare Professions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/departments/health-sciences/continuing-education/">Continuing Education</a></li>
               <li>
                  
                  Continuing Education for Healthcare Professions
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href=""></a>
                        
                        
                        
                        
                        
                        
                        
                        <p>Personal Fitness Training</p>
                        
                        <h4>Prepare for ACE Certification </h4>
                        
                        <p>Please go to <a href="http://www.valencia.efslibrary.net/">our partner website</a>  for more information and to register for this program
                        </p>
                        
                        <p>Online Professional Certificates in Exercise, Health, and Nutrition are a collaboration
                           between industry experts, instructors, internship affiliates, board members, and national
                           organizations.&nbsp; These innovative, Web-based certificate programs provide an interactive
                           educational experience that will allow students from around the world to fit courses
                           into their busy work and home schedules, go on-line wherever they are, and complete
                           the certificates and optional internship course assignments within five months depending
                           on the certificate program, or up to two years if necessary.
                        </p>
                        
                        <p><strong>According to the U.S. Department of Labor’s Bureau publication <em>Occupational Outlook Handbook</em>, overall employment for fitness professionals is expected to increase 44% from 2002
                              to 2012.</strong><br>
                           
                        </p>
                        
                        <h3><strong>Who Should Attend?</strong></h3>
                        
                        <ul>
                           
                           <li>Fitness professionals from all fields</li>
                           
                           <li>Individuals seeking a new career as a health/fitness professional</li>
                           
                           <li>Allied health and medical professionals including physical therapists, RD's, athletic
                              trainers, nurses, physicians, chiropractors, and wellness consultants seeking advanced
                              education, CECs/CEUs, or new career opportunities 
                           </li>
                           
                           <li>Companies that require the certificate programs as a professional development program
                              for employees to achieve key business goals, career objectives, and skills to advance
                              beyond entry-level positions
                           </li>
                           
                        </ul>
                        
                        <h3><strong>Earning Your Certificate</strong></h3>
                        
                        <p>To earn a certificate you must:</p>
                        
                        <ul>
                           
                           <li>Complete all required core courses for the certificate program within a period of
                              two years
                           </li>
                           
                           <li>Receive a passing grade of (C- or above) in all courses:&nbsp; which includes quizzes,
                              exams, written/research projects, and class assignments
                           </li>
                           
                        </ul>
                        
                        <h3>
                           <strong>Certificate Program Benefits</strong> 
                        </h3>
                        
                        <p><strong>Program Coordinators - </strong>Our certificate program coordinators are leading experts, national presenters, and
                           instructors in the health and fitness industry that will support their students and
                           will serve as valuable resources and mentors.
                        </p>
                        
                        <p><strong>Instructor Guidance</strong> – You benefit from ongoing guidance, instruction, and interaction from instructors
                           who hold either a <strong>master's or doctoral degree</strong> in exercise physiology, kinesiology, biomechanics, physical therapy, nutrition, or
                           other exercise science related fields.&nbsp; 
                        </p>
                        
                        <p><strong>Obtain Continuing Education Credits/Units (CEC’s/CEU’s)</strong> - Courses will provide advanced education and CEC's or CEU's<strong>,</strong> for personal trainers, group fitness instructors, physical therapists, RD's, athletic
                           trainers, nurses, and chiropractors seeking recertification or career advancement.
                        </p>
                        
                        <p><strong>Practical Field Internship Elective Course</strong> - One of the unique benefits of several of our certificate programs is the opportunity
                           for you to practice your skills in a health and fitness setting through an optional
                           field internship course. 
                        </p>
                        
                        
                        <p><strong>A Complete Learning Experience</strong> - Courses are designed to enhance interaction between instructors and students. Chat
                           rooms, discussion questions, email, and the phone are used to facilitate discussions.
                           Courses may include the use of supplemental materials such as video/audio tapes, DVDs,
                           and CDs.&nbsp; Team and individual projects and small group discussions are used to help
                           students share their teaching, educational, and professional experiences with each
                           other as part of the learning process.
                        </p>
                        
                        <p><strong>National Organizations</strong> – Many of the nation’s leading health and fitness organizations have partnered to
                           provide students with educational, membership, and national certification exam opportunities.&nbsp;
                           The following national organizations are our partners: the American Council of Exercise
                           (ACE), the International Council on Active Aging (ICAA), Healthy Moms Fitness, Health
                           Club Managers, IDEA Health and Fitness Association, and the Women’s Health Organization.
                        </p>
                        
                        
                        <h3><strong>Why Online Learning?</strong></h3>
                        
                        <ul>
                           
                           <li>Learning occurs in a user-friendly environment and is accessible to participants with
                              little or no computer experience.<strong> </strong>
                              
                           </li>
                           
                           <li>Online courses give you the knowledge and tools you need to stay ahead in today’s
                              rapidly changing professional marketplace.<strong> </strong>
                              
                           </li>
                           
                           <li>Online education allows you to access your courses from anywhere there is an Internet
                              connection even if you are traveling for business or on vacation.<strong></strong>
                              
                           </li>
                           
                           <li>Flexibility: You can attend class in the comfort and convenience of your own home,
                              office, library, or Internet café and complete assignments at a time that is convenient
                              for you.<strong> </strong>
                              
                           </li>
                           
                           <li>Cost-effective: Expenses related to facilities, travel, and non-productive time is
                              reduced.<strong>&nbsp;</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <h3><strong>Continuing Education Credits/Units</strong></h3>
                        
                        <p>The following National Organizations/Associations have approved the online certificate
                           programs/courses for Continuing Education Credits/Units (CECs/CEUs):
                        </p>
                        
                        <ul>
                           
                           <li>The <strong>Commission on Dietetic Registration (CDR)</strong>, the credentialing agency for the <strong>American Dietetic Association (ADA)</strong> has approved the Certificate in Personal Training program for 150 Continuing Professional
                              Education Units (CPEUs), the Advanced Certificate program for 90 CPEUs, and the Optional
                              Field Internship in Personal Fitness Training Course for 60 CPEUs&nbsp; In addition, the
                              Nutrition program has been approved for 120 Continuing Professional Education Units
                              (CPEUs)/ or 30 (CPEUs) per course for both Registered Dietitian (RDs) and Dietetic
                              Technician-Registered (DTRs). 
                           </li>
                           
                           <li>The <strong>American Senior Fitness Association (SFA)</strong> has approved each course in every program for 2.0 CEUs.
                           </li>
                           
                           <li>The <strong>National Federation of Professional Trainers (NFPT)</strong> has approved each course in every program for 1 CEC, equivalent to an entire 6 month
                              CEC requirement. 
                           </li>
                           
                        </ul>
                        
                        
                        <h3><strong>Enrollment and Registration</strong></h3>
                        
                        <p>There are no prerequisite requirements for admission into the certificate programs.&nbsp;
                           Courses are 5 to 6 weeks in length and are offered year-round, in the <strong>Fall,</strong> <strong>Spring</strong>, and <strong>Summer </strong>semester.&nbsp; Courses may be taken individually if desired.<strong>&nbsp;</strong></p>
                        
                        <h3>
                           <strong><u>VERY IMPORTANT:&nbsp; Structure 
                                 and Start Dates </u></strong><br>
                           To earn the certificate you are required to complete all five core 
                           courses. You may also elect to take the optional field internship 
                           course. The five core courses plus the optional field internship 
                           are offered in a <strong>three-module format</strong>; each module consists of two 
                           courses that are offered simultaneously. 
                        </h3>
                        
                        <h3><strong>MODULE I:&nbsp;Anatomy &amp; Kinesiology and Exercise Physiology </strong></h3>
                        
                        <h3><strong>MODULE II: Health Risk Profiles, Business Administration </strong></h3>
                        
                        <h3><strong>MODULE III: Designing Exercise Prescriptions </strong></h3>
                        
                        <h3>You are not required to 
                           take course in any order; however, <strong>it is highly recommended that 
                              you begin with two courses in MODULE I</strong>.&nbsp; You may also register for individual 
                           courses if desired. The three-module series is offered three times 
                           per year in the fall, spring, and summer. The program can be completed 
                           in as little as five months but you must complete the requirements 
                           within two-years. 
                        </h3>
                        
                        <p>Students that have completed the core Certificate may then 
                           register for the <strong>advanced</strong> <strong>certificate</strong> which requires three additional 
                           courses. The advanced certificate is designed for students interested 
                           in furthering their personal training education by incorporating 
                           specialized resistance training, functional movements, and nutritional 
                           programs for their clients in partnership with RDs. 
                        </p>
                        
                        
                        <p><u><strong>More Information About Our Content and Instructional 
                                 Partner</strong></u></p>
                        
                        <p>Ken 
                           Baldwin, M.ED., is the Program Coordinator and an instructor for 
                           the Professional Certificate in Personal Fitness Training Program. 
                           He is the former Chair of IDEA's National Personal Trainer Committee 
                           and Chair of the Senior Fitness Subcommittee for the Massachusetts's 
                           Governor's Committee on Physical Fitness and Sports. Ken is the 
                           Co-Editor and lead author for ACSM's Resources for the Personal 
                           Trainer Manual (2 nd Ed.) and textbook. He continues to serve on 
                           national committees for ACSM, AAHPERD (AAPAR), and the MFA. He has 
                           been honored with numerous awards including IDEA's Personal Trainer 
                           of the Year Award, presented at the IDEA International Personal 
                           Training Summit. 
                        </p>
                        
                        <p>Ken 
                           Baldwin, M.ED 
                        </p>
                        
                        <p>Program 
                           Coordinator <br>
                           Professional Certificate in Personal Fitness Training <br>
                           IDEA Personal Trainer of Year Award Winner 
                        </p>
                        
                        <div> 
                           
                           <p><strong>In agreement with</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt="" height="89" src="../../../../locations/west/departments/health-sciences/continuing-education/EFS.jpg" width="234">
                              
                           </p>
                           
                           
                           <p><strong>To register for courses via web: <br>
                                 <a href="http://www.valencia.efslibrary.net/">Please go to our partner website for more information and to register for this program</a> </strong></p>
                           
                        </div>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>APPROVED PROVIDERS</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 <div>
                                    <a href="http://cebroker.com/">Florida Department of Health</a><br>
                                    <strong>Provider # 50-2109</strong><br><br>
                                    <a href="http://floridacertificationboard.org/">Florida Certification for Addiction Professionals</a><br> <strong>Provider #16-SS</strong><br> 
                                 </div>
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Dentistry</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Clinical Social Work, Marriage &amp; Family Therapy, Mental Health Counseling</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Nursing</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Nutritionists and Dietitians</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Respiratory Therapists</div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        <h3>COURSE CATALOG</h3>
                        
                        <p>Valencia currently offers courses and programs in areas such as Basic Life Support,
                           Healthcare Mandatory and License Renewal Courses, and Dental Education.
                        </p>
                        
                        <p><a href="../../../../locations/west/departments/health-sciences/continuing-education/Courses/index.html">View Course Catalog</a></p>
                        
                        
                        
                        
                        
                        
                        
                        <h3>UPCOMING COURSES</h3>
                        
                        <div>
                           
                           <div>
                              <div>
                                 
                                 <div>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=8153%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 8:30 a.m. - 12:30 p.m. -  West Campus Bldg HSB Rm 118 - ">
                                          
                                          AIDS 104 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=8153%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 8:30 a.m. - 12:30 p.m. -  West Campus Bldg HSB Rm 118 - ">
                                          
                                          AIDS 104 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=736&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9500%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 9 a.m. - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers course covers core material such as adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS Health Care Provider 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3105&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9510%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 9 a.m. - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers course covers core material such as adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS HC PRO for A.S. Students 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=737&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9501%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 1 - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers Recertification course reviews core material, paying attention to protocol changes. For the purpose of recertification, the instructor performs a BLS skill check off for adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS Health Care Provider Recert 
                                          </a></p>
                                    
                                    
                                    
                                    <br><a href="../../../../locations/west/departments/health-sciences/continuing-education/incl_calendar_continuity_Detail.html">View All Course</a> 
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/continuing-education/fitness.pcf">©</a>
      </div>
   </body>
</html>