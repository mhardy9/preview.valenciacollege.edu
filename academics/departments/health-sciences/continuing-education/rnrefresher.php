<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>
         
         Continuing Education for Healthcare Professions | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/continuing-education/rnrefresher.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Continuing Education for Healthcare Professions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/departments/health-sciences/continuing-education/">Continuing Education</a></li>
               <li>
                  
                  Continuing Education for Healthcare Professions
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href=""></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3>RN REFRESHER 1 - Online Only </h3>
                        
                        <p>Employment of RNs in Florida is projected to grow from approximately 137,000 to over
                           166,000 by 2013, and during this period 22,000 RNs are projected to retire, change
                           occupations, or become deceased. When examined by age categories, the data show that
                           Florida can expect to lose more than 40% of its currently licensed RN population over
                           the next decade (Florida Center for Nursing, January 2007).
                        </p>
                        
                        <p>In response to these projections, Florida’s nursing programs are addressing capacity
                           constraints and the state is working to become less dependent on outside sources for
                           its nursing workforce. 
                        </p>
                        
                        <p> In an effort to further increase the number of Florida RNs in the workforce, Continuing
                           Education for Health Professions at Valencia has designed an interactive, instructor-led
                           online course to prepare licensed RNs to return to the workforce. This online course,
                           together with a proven clinical proficiency in lab and a 96-hour medical-surgical
                           rotation, enhance the RN’s opportunity for employment in acute healthcare settings.
                        </p>
                        
                        <p><strong>PRE-REQUISITE REQUIREMENTS:&nbsp; Must hold a valid Florida RN license</strong></p>
                        
                        <p>The course provides a review of all areas of nursing practice and includes several
                           tools for knowledge testing, skills review, and critical thinking.&nbsp; <strong>Prior to gaining access to the online course, participants must register and attend
                              a <span>mandatory live orientation.</span></strong></p>
                        
                        <p>The RN Refresher 1 course is 12 weeks long and includes the following:</p>
                        
                        <ul>
                           
                           <li>
                              <strong>Overview and Nursing Fundamentals </strong>: Week 1 
                           </li>
                           
                           <li>
                              <strong>Medical-Surgical Focus/Care of the Adult </strong>:&nbsp; Weeks 2-4
                           </li>
                           
                           <li>
                              <strong>Psychiatric Care</strong>:&nbsp; Week 5 
                           </li>
                           
                           <li>
                              <strong>Maternal-Neonatal Care</strong>:&nbsp; Week 6 
                           </li>
                           
                           <li>
                              <strong>Care of the Child</strong>:&nbsp; Week 7 
                           </li>
                           
                           <li>
                              <strong>Professional Issues</strong>:&nbsp; Week 8
                           </li>
                           
                           <li>
                              <strong>Essential Skills Review and Check Off:</strong> Weeks 9, 10, 11
                           </li>
                           
                           <li>
                              <strong>Final Project </strong>:&nbsp; Week 12 
                           </li>
                           
                        </ul>
                        
                        <p>Components of the course will include:</p>
                        
                        <ul>
                           
                           <li>Weekly text reading assignments</li>
                           
                           <li>Weekly assignments: &nbsp;graded</li>
                           
                           <li>Online discussion questions:&nbsp; graded</li>
                           
                           <li>Live chat room discussions with instructor and other students: graded </li>
                           
                           <li>Textbook-based practice quizzes and tests for review </li>
                           
                           <li>Final individual project: graded </li>
                           
                           <li>Clinical Skills Review and Practice (laboratory setting)</li>
                           
                        </ul>
                        
                        <p><strong>IMPORTANT:</strong>&nbsp; <strong>Successful completion of this content and skills-based course is prerequisite to eligibility
                              for clinical rotation in RN Refresher 2.</strong></p>
                        
                        <h3>Your Next Steps</h3>
                        
                        <h3>Step One: Application to Program</h3>
                        
                        <p><span><strong>Prior to registering for this course, please click this<a href="documents/application_rnrefresher1.doc"> application link</a><a href="documents/application_rnrefresher1.doc">.</a> </strong></span>Complete the application and email it to the address at the top of the form.  Once
                           we have determined whether or not this course is appropriate for you, you will receive
                           an email which will include registration instructions if you are eligible to take
                           the course.
                        </p>
                        
                        <h3>Step Two: Dates, Cost and Enrollment</h3>
                        
                        <p><span><strong>Registration is on a first come basis and must be preceeded by approval of your application</strong></span>. To  learn more about the dates and cost of the program and to enroll with our handy
                           shopping cart, <a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2317&amp;master_version=1&amp;course_area=CNH&amp;course_number=80451&amp;course_subtitle=VEH"> click here. </a></p>
                        
                        <h3>Step Three: Required Textbooks</h3>
                        
                        <p> The course fee does not include the cost of your required text.&nbsp;You may order through
                           any resource you choose to achieve cost savings. Use the ISBN information provided
                           below and be sure to purchase the right edition.
                        </p>
                        
                        <p>Publisher: Elsevier</p>
                        
                        <p><strong>Saunders Comprehensive Review for the NCLEX-RN® Examination, 5th Edition </strong></p>
                        
                        <p><strong>ISBN: 9781437708257</strong></p>
                        
                        <h3>Step Four: Course Access</h3>
                        
                        <p>You will not be able to access the course prior to the orientation when you will be
                           issued  a user name and password.
                        </p>
                        
                        
                        
                        <h3>RN REFRESHER 2 - Clinical Practicum </h3>
                        
                        
                        <p>Most potential employers will require  proven clinical proficiency in lab environment
                           and in a medical-surgical rotation.&nbsp; This course works with hospital and/or acute
                           care settings to arrange a 96-hour clinical rotation with a hospital-based clinical
                           preceptor and we provide a  instructor to oversee our Refresher students while they
                           are in the clinical environment.&nbsp;
                        </p>
                        
                        
                        <p>To register for RN Refresher 2, you must successfully complete the <strong>RN Refresher 1 - Online </strong>content and skills-based course described above.&nbsp; In addition to the prerequisite
                           course, participants in this course will be required to provide the following:
                        </p>
                        
                        <ul>
                           
                           <li>Proof of <strong>immunizations</strong>
                              
                           </li>
                           
                           <li>Proof of current <strong>American Heart Association BLS Certification</strong>
                              
                           </li>
                           
                           <li>Proof of  <strong>respiratory mask fitting</strong> (FIT testing); will be conducted during RN Refresher 2 orientation 
                           </li>
                           
                           <li>
                              <strong>Criminal background check and finger printing</strong> through college-contracted service
                           </li>
                           
                           <li>
                              <strong>Drug screening </strong>through college-contracted service
                           </li>
                           
                           <li>Proof of <strong>Professional Liability Insurance</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p>A packet of information and forms will be provided upon enrollment in RN Refresher
                           2. 
                        </p>
                        
                        <h3>The clinical practicum will be scheduled in  twelve hour shifts at the discretion
                           of the assigned preceptor. <strong>It is vital that participants understand that clinical space is seriously limited
                              by the large number of schools requiring clinical space and that the hospital staff
                              will govern the schedule based on preceptor availability and the need for continuity.&nbsp;</strong> Also, hospitals will require orientation to their medical records documentation and
                           successful completion of such training will also be prerequisite to your assignment
                           to a clinical rotation. 
                        </h3>
                        
                        <h3>Your Next Step: Enrollment in RN Refresher 2 for Clinical Rotation <a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2318&amp;master_version=1&amp;course_area=CNH&amp;course_number=80452&amp;course_subtitle=VEH">click here.</a>
                           
                        </h3>
                        
                        <h3>Estimated Costs Associated with RN Refresher Program</h3>
                        
                        <ul>
                           
                           <li>
                              <strong>RN Refresher 1: Online Course </strong>
                              
                              <ul>
                                 
                                 <li>Course Fee: $799</li>
                                 
                                 <li>Textbooks: $70</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong>RN Refresher 2: Practicum </strong>
                              
                              <ul>
                                 
                                 <li>Course Fee: $429</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong>Estimated costs: clinical affiliation requirements</strong>    
                              
                              <ul>
                                 
                                 <li>Immunization Tracking, background check, finger printing, and drug testing: $190 (Castlebranch)
                                    
                                 </li>
                                 
                                 <li>AHA Basic Life Support Certification: $40 (renewal $20)</li>
                                 
                                 <li>Malpractice Insurance: $100</li>
                                 
                                 <li>Required immunizations and/or titers will vary by each student depending upon available
                                    records 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Financial Assistance Opportunities</h3>
                        
                        
                        <p>While there is no financial aid available for these non-credit courses, there are
                           some resources available and we will pass those along as we become aware.
                        </p>
                        
                        <p>Worforce Central Florida has some funds available for qualified candidates. Go the
                           the web location below to explore the qualifications and and availability fo financial
                           assistance. 
                        </p>
                        
                        <p> <a href="http://www.workforcecentralflorida.com/jobseekers/wcf_financial.asp">http://www.workforcecentralflorida.com/jobseekers/wcf_financial.asp</a> 
                        </p>
                        
                        <hr>
                        
                        <p>For more information regarding these courses, <a href="http://net4.valenciacollege.edu/west/health/cehealth/Info/contact.cfm" target="_blank"><strong>submit</strong></a> a direct inquiry and we will respond as quickly as possible. 
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>APPROVED PROVIDERS</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 <div>
                                    <a href="http://cebroker.com/">Florida Department of Health</a><br>
                                    <strong>Provider # 50-2109</strong><br><br>
                                    <a href="http://floridacertificationboard.org/">Florida Certification for Addiction Professionals</a><br> <strong>Provider #16-SS</strong><br> 
                                 </div>
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Dentistry</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Clinical Social Work, Marriage &amp; Family Therapy, Mental Health Counseling</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Nursing</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Nutritionists and Dietitians</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><img alt="" height="16" src="../../../../locations/west/departments/health-sciences/continuing-education/bullet_triangle_red.gif" width="10"></div>
                                 
                                 <div>Respiratory Therapists</div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        <h3>COURSE CATALOG</h3>
                        
                        <p>Valencia currently offers courses and programs in areas such as Basic Life Support,
                           Healthcare Mandatory and License Renewal Courses, and Dental Education.
                        </p>
                        
                        <p><a href="../../../../locations/west/departments/health-sciences/continuing-education/Courses/index.html">View Course Catalog</a></p>
                        
                        
                        
                        
                        
                        
                        
                        <h3>UPCOMING COURSES</h3>
                        
                        <div>
                           
                           <div>
                              <div>
                                 
                                 <div>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=8153%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 8:30 a.m. - 12:30 p.m. -  West Campus Bldg HSB Rm 118 - ">
                                          
                                          AIDS 104 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=2346&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=8153%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 8:30 a.m. - 12:30 p.m. -  West Campus Bldg HSB Rm 118 - ">
                                          
                                          AIDS 104 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=736&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9500%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 9 a.m. - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers course covers core material such as adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS Health Care Provider 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=3105&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9510%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 9 a.m. - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers course covers core material such as adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS HC PRO for A.S. Students 
                                          </a></p>
                                    
                                    
                                    
                                    
                                    
                                    <p><strong>Oct 14</strong></p>
                                    
                                    <p><a href="https://c2k.valenciacollege.edu/ce_health/CourseListing.asp?master_id=737&amp;master_version=1&amp;course_area=CNH%20%20%20&amp;course_number=9501%20%20%20&amp;course_subtitle=00" target="_blank" title="Oct. 14, 2017 &amp;lt;br /&amp;gt;Sat., 1 - 5 p.m. -  West Campus Bldg HSB Rm 120 - The BLS for Healthcare Providers Recertification course reviews core material, paying attention to protocol changes. For the purpose of recertification, the instructor performs a BLS skill check off for adult and pediatric CPR (including two-rescuer scenarios and use of the bag mask), foreign-body airway obstruction, and automated external defibrillation.">
                                          
                                          CPR/BLS Health Care Provider Recert 
                                          </a></p>
                                    
                                    
                                    
                                    <br><a href="../../../../locations/west/departments/health-sciences/continuing-education/incl_calendar_continuity_Detail.html">View All Course</a> 
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/continuing-education/rnrefresher.pcf">©</a>
      </div>
   </body>
</html>