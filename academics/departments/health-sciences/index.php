<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Health Sciences Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, health sciences, nursing, advising, continuing education, allied health">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="container-fluid header-college">
         <nav class="container navbar navbar-expand-lg">
            <div class="navbar-toggler-right"><button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-val" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div>
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/_menu-collegewide.inc"); ?></div>
         </nav>
         <nav class="container navbar navbar-expand-lg">
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
         </nav>
      </div>
      <div class="page-header bg-interior">
         <div id="intro-txt">
            <h1>Health Sciences</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Health Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid">
            <div class="container margin-60" role="main">
               		
               <div class="row">
                  			
                  <div class="col-md-12">
                     				
                     <h2>Health Sciences Divisions</h2>
                     				
                     <div class="row">
                        					
                        <div class="col-md-6">
                           						
                           <h3><i class="far fa-medkit fa-fw fa-2x v-red"></i><a title="Division of Allied Health" href="/academics/departments/health-sciences/allied-health/">Division of Allied Health</a></h3>
                           						
                           <div class="wrapper_indent">
                              							
                              <p class="over"><strong>Division Office</strong><br> West Campus, Building 2-208
                              </p>
                              							
                              <p class="over"><strong>Administration</strong><br> Dean: Jamy Chulak
                              </p>
                              						
                           </div>
                           						
                           <h3><i class="far fa-heartbeat fa-fw fa-2x v-red"></i><a title="Division of Nursing" href="/academics/departments/health-sciences/nursing/">Division of Nursing</a></h3>
                           						
                           <div class="wrapper_indent">
                              							
                              <p class="over"><strong>Division Office</strong><br> West Campus, Health Sciences (HSB) 200<br>Fax: 407-582-1278
                              </p>
                              							
                              <p class="over"><strong>Administration</strong><br> Dean: Risë Sandrowitz , MSN
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <div class="col-md-6">
                           						
                           <h3><i class="far fa-clipboard fa-fw fa-2x v-red"></i><a title="Health Sciences Advising" href="/academics/departments/health-sciences/advising/">Health Sciences Advising</a></h3>
                           						
                           <div class="wrapper_indent">
                              							
                              <p><strong>West Campus</strong><br> Building 1, Room 130 <br> Phone: (407) 582-1288 <strong>*</strong>Email is preferred for the most efficient means of communication <br> Fax: (407) 582-5462 <br> Email: <a href="mailto:healthscienceadvising@valenciacollege.edu">HealthScienceAdvising@valenciacollege.edu</a>&nbsp; or <a href="mailto:Enrollment@valenciacollege.edu">Enrollment@valenciacollege.edu</a></p>
                              						
                           </div>
                           						
                           <h3><i class="far fa-chart-line fa-fw fa-2x v-red"></i><a title="Continuing Education for Health Professionals" href="/academics/departments/health-sciences/continuing-education/">Continuing Education for Health Professionals</a></h3>
                           						
                           <div class="wrapper_indent">
                              							
                              <p>Carol Millenson, Manager</p>
                              						
                           </div>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div>
                        					
                        <h2>Health Sciences Programs &amp; Degrees</h2>
                        					
                        <div class="row">
                           						
                           <div class="col-md-6">
                              							
                              <h3 class="subheader">Associate in Science</h3>
                              							
                              <ul>
                                 								
                                 <li><a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/dental-hygiene/">Dental Hygiene</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/diagnostic-medical-sonography/">Diagnostic Medical Sonography</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/emergency-medical-services/">Emergency Medical Services Technology</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/health-information-technology/">Health Information Technology</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/nursing/">Nursing Programs</a>
                                    									
                                    <ul>
                                       										
                                       <li><a href="/academics/programs/health-sciences/nursing/traditional-daytime.php">Nursing Traditional, Daytime</a>
                                          											
                                          <ul>
                                             <li><a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index">Nursing, Valencia/UCF Concurrent A.S.N. to B.S.N. Option</a></li>
                                          </ul>
                                          										
                                       </li>
                                       										
                                       <li><a href="/academics/programs/health-sciences/nursing/traditional-evening.php">Nursing Traditional, Evening/Weekend</a>
                                          											
                                          <ul>
                                             <li><a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index">Nursing, Valencia/UCF Concurrent A.S.N. to B.S.N. Option</a></li>
                                          </ul>
                                          										
                                       </li>
                                       										
                                       <li><a href="/academics/programs/health-sciences/nursing/accelerated-track.php">Nursing, Accelerated Track in Nursing</a></li>
                                       									
                                    </ul>
                                    								
                                 </li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/radiography/">Radiography</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/respiratory-care/">Respiratory Care</a></li>
                                 								
                                 <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/">Veterinary Technology</a>
                                    									
                                    <ul>
                                       <li>This AS Degree is offered through a Cooperative Agreement with <a href="http://www.spcollege.edu/vt/">St. Petersburg College</a>.  Students interested in this program may take the general education requirements
                                          at Valencia and complete the rest of the degree online through St. Petersburg College.
                                          Valencia Catalog has more information about the <a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/">Cooperative Agreement with St. Petersburg College</a>.
                                          										
                                       </li>
                                    </ul>
                                 </li>
                                 							
                              </ul>
                              						
                           </div>
                           						
                           <div class="col-md-6">
                              							
                              <h3 class="subheader">Bachelor of Science</h3>
                              							
                              <ul>
                                 								
                                 <li><a href="/academics/programs/health-sciences/cardiopulmonary-sciences/">Cardiopulmonary Sciences</a></li>
                                 								
                                 <li><a href="/academics/prograsm/health-sciences/nursing/bachelors.php">Nursing</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/radiologic-imaging-science/">Radiologic and Imaging Sciences</a></li>
                                 							
                              </ul>
                              							
                              <h3>Advanced Technical Certificate</h3>
                              							
                              <ul>
                                 								
                                 <li><a href="/academics/programs/health-sciences/computed-tomography/">Computed Tomography</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/echocardiography/">Echocardiography</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/leadership-healthcare/">Leadership in Healthcare</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/magnetic-resonance-imaging/">Magnetic Resonance Imaging</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/mammography/">Mammography</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/vascular-sonography/">Vascular Sonography</a></li>
                                 
                                 							
                              </ul>
                              							
                              <h3 class="subheader">Technical Certificate</h3>
                              							
                              <ul>
                                 								
                                 <li><a href="/academics/programs/health-sciences/emergency-medical-services/emergency-medical-technician.pdf">Emergency Medical Technician</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/medical-info-coder/">Medical Information Coder/Biller</a></li>
                                 								
                                 <li><a href="/academics/programs/health-sciences/emergency-medical-services/paramedic-tech.pdf">Paramedic Technology</a></li>
                                 							
                              </ul>
                              						
                           </div>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="row">
                        <div class="col-md-4 col-sm-4">
                           								<a class="box_feat" href="/academics/departments/health-sciences/international-student-eligibility.php" target=""><i class="far fa-globe"></i><h3>
                                 												International Student Eligibility
                                 											
                              </h3>
                              <p>
                                 												Effective Fall 2017, International Students will be eligible to apply
                                 for the Valencia College Health Sciences Programs.
                                 												Learn More
                                 											
                              </p></a>
                           							
                        </div>
                        <div class="col-md-4 col-sm-4">
                           								<a class="box_feat" href="/documents/academics/programs/health-sciences/application-2018-Medical-Coder-tc-form-with-com-form.pdf" target=""><i class="far fa-clipboard"></i><h3>
                                 												Career in Medical Coding
                                 											
                              </h3>
                              <p>
                                 												Interested in pursuing a career in Medical Coding? We are excited to announce
                                 the start of our New Technical Certificate in Medical Information Coder/Biller.
                                 												Learn More
                                 											
                              </p></a>
                           							
                        </div>
                        <div class="col-md-4 col-sm-4">
                           								<a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/advancedtechnicalcertificates/" target=""><i class="far fa-certificate"></i><h3>
                                 												Advanced Technical Certificates
                                 											
                              </h3>
                              <p>
                                 												Valencia offers ATCs in the following areas:Computed Tomography (CT),
                                 Echocardiography (ECHO), Leadership in Healthcare, Magnetic Resonance Imaging (MRI)
                                 &amp;Mammography
                                 												Learn More
                                 											
                              </p></a>
                           							
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-sm-6">
                           								<a class="box_feat" href="http://preview.valenciacollege.edu/future-students/degree-options/bachelors/?_ga=2.200250350.1958698805.1513603294-2079824746.1503317573" target=""><i class="far fa-medkit"></i><h3>
                                 												Bachelor's Degrees
                                 											
                              </h3>
                              <p>
                                 												Valencia offers Bachelor's Degrees for Cardiopulmonary Sciences &amp; Radiologic
                                 and Imaging Sciences
                                 												Learn More
                                 											
                              </p></a>
                           							
                        </div>
                        <div class="col-md-6 col-sm-6">
                           								<a class="box_feat" href="/academics/departments/health-sciences/professions-of-caring.php" target=""><i class="far fa-heart"></i><h3>
                                 												Professions of Caring
                                 											
                              </h3>
                              <p>
                                 												Interested in a Health Care career? Sign up for HSC1004 today!
                                 												Learn More
                                 											
                              </p></a>
                           							
                        </div>
                     </div>
                     			
                  </div>
                  		
               </div>
               		
            </div>
            <hr class="styled_2">
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/index.pcf">© 2018 - All rights reserved.</a>
      </div>
   </body>
</html>