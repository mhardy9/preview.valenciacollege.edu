<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Division of Nursing  | Valencia College</title>
      <meta name="Description" content="Division of Nursing | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/health-sciences/nursing/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Division of Nursing</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/health-sciences/">Health Sciences</a></li>
               <li>Nursing</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <h2>Division of Nursing</h2>
                     
                     <h3>Office and Administration</h3>
                     
                     <div class="col-md-6">
                        
                        <h4 class="val">Dean of Nursing</h4>
                        <i class="far fa-address-card fa-fw"></i>Risë Sandrowitz , MSN<br>
                        
                        <h4 class="val">Clinical Program Director</h4>
                        <i class="far fa-address-card fa-fw"></i>Leann Hudson<br>
                        
                        <h4 class="val">Technology Support</h4>
                        <i class="far fa-address-card fa-fw"></i>Jeff Hogan, MS, Functional IS Support Specialist 
                        
                        <hr class="styled_2">
                        
                        <h4 class="val">Location</h4>
                        <i class="far fa-map-marker fa-fw"></i>West Campus, Health Sciences (HSB) 200<br>
                        
                        <h4 class="val">Hours</h4>
                        <i class="far fa-calendar fa-fw"></i>Monday - Thursday 8 a.m. - 5 p.m.<br>
                        <i class="far fa-calendar fa-fw"></i>Friday 9 a.m. – 5 p.m. <br>
                        
                        <h4 class="val">Contact Us</h4>
                        <i class="far fa-envelope fa-fw"></i><a href="mailto:#">#@valenciacollege.edu</a> <br>
                        <i class="far fa-phone fa-fw"></i>407-582-xxxx<br>
                        <i class="far fa-fax fa-fw"></i>407-582-1278 (fax)
                        
                     </div>
                     
                     <div class="col-md-6">
                        					
                        <div class="aspect-ratio" style="height: 500px;">
                           						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaHealthSciences&amp;tabs=timeline&amp;width=500&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId=203940866824764" min-height="500 px;" style="border:none;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                           					
                        </div>
                        				
                     </div>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="row">
                     
                     <h2>Health Science Programs</h2>
                     
                     <h3>Associate in Science</h3>
                     
                     <p>The A.S. degree&nbsp;prepares you to enter a specialized career field in about two years.&nbsp;It
                        also transfers to the Bachelor of Applied Science program offered at some universities.
                        If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                        at a designated university.
                     </p>
                     
                     <ul>
                        
                        <li><a href="/academics/programs/health-sciences/nursing/">Nursing Programs</a>
                           									
                           <ul>
                              										
                              <li><a href="/academics/programs/health-sciences/nursing/traditional-daytime.php">Nursing Traditional, Daytime</a>
                                 											
                                 <ul>
                                    <li><a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index">Nursing, Valencia/UCF Concurrent A.S.N. to B.S.N. Option</a></li>
                                 </ul>
                                 										
                              </li>
                              										
                              <li><a href="/academics/programs/health-sciences/nursing/traditional-evening.php">Nursing Traditional, Evening/Weekend</a>
                                 											
                                 <ul>
                                    <li><a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index">Nursing, Valencia/UCF Concurrent A.S.N. to B.S.N. Option</a></li>
                                 </ul>
                                 										
                              </li>
                              										
                              <li><a href="/academics/programs/health-sciences/nursing/accelerated-track.php">Nursing, Accelerated Track in Nursing</a></li>
                              									
                           </ul>
                           								
                        </li>
                        
                     </ul>
                     	
                     <h3>Bachelor of Science</h3>
                     				
                     <p>The B.S. degree&nbsp;prepares you to enter an advanced position within a specialized career
                        field.&nbsp;The upper-division courses build on an associate degree and take about two
                        years to complete.
                     </p>
                     				
                     <ul>
                        					
                        <li><a href="/academics/programs/health-sciences/nursing/bachelors.php">Nursing</a></li>
                        				
                     </ul>
                     
                     <h3>Continuing Education</h3>
                     
                     <p><a href="/academics/programs/health-sciences/continuing-education/index.php" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                        development, language courses, certifications and custom course development for organizations.
                        Day, evening, weekend and online learning opportunities are available.<br>
                        <i class="far fa-address-card fa-fw"></i>Carol Millenson, Operations Manager - HSB 219
                     </p>
                     
                  </div>
                  
                  <hr class="styled_2">
                  
                  <div class="row">
                     
                     <p>Under the new admission criteria, implemented Fall 2015, 78-80% of students are progressing
                        to the next semester.  The first class admitted under the new criteria has not yet
                        graduated.
                     </p>
                     
                     
                     <p>Graduates of the nursing program for academic year 2015-2016 passed the licensing
                        exam (NCLEX-RN) above 97%, well above state and national averages.
                     </p>
                     
                     
                     <p>Job placement of Valencia nursing grads is 97% within 6 months to one year of graduating.</p>
                     
                     
                     <p>The nursing division had its reaccreditation site visit Oct. 2015.  We received continuing
                        accreditation with a two year follow-up report on one of six standards (due Feb. 2018).
                        
                     </p>
                     	
                  </div>
                  			
                  <hr class="styled_2">
                  
                  <div class="container margin-60 box_style_1">
                     
                     <div class="main-title">
                        
                        <h2>Health Sciences Advising</h2>
                        
                        <p></p>
                        
                     </div>
                     
                     <div class="row">
                        
                        <p>Health Sciences Advising staff offer specialized educational advisors who can guide
                           students in the successful completion of an Associate in Science Degree or Bachelor
                           of Science Degree or Certificate Program in Nursing or nursing. Attendance at a Health
                           Sciences Information Session is mandatory before meeting with a Health Sciences Advisor
                           for an associate-level program but not for meeting about a bachelor-level program.
                        </p>
                        
                        <p>The advising staff can answer program-specific questions, assist in career and educational
                           planning, and offer expert educational guidance for students seeking career opportunities
                           through the completion of an A.S. or B.S. Degree or a Certificate program.
                        </p>
                        
                        <p>Associate in Science students are expected to complete their educational goals and
                           be professionally marketable in a much shorter period of time than many others. This
                           is why the early development of career and education plans is essential to a successful
                           transition from school to work.
                        </p>
                        
                        <div class="row">
                           
                           <div class="col-md-8 col-sm-8">
                              <img src="/images/academics/programs/health-sciences/health-sciences-advising.jpg" class="img-responsive"> 
                           </div>
                           
                           <div class="col-md-4 col-sm-4">
                              
                              <h5 class="val">Location</h5>
                              <i class="far fa-map-marker fa-fw"></i>West Building 1, Room 130 <br>
                              
                              <h5 class="val">Hours</h5>
                              <i class="far fa-calendar fa-fw"></i>Monday - Thursday 8 a.m. - 5 p.m.<br>
                              <i class="far fa-calendar fa-fw"></i>Friday 9 a.m. – 5 p.m. <br>
                              
                              <h5 class="val">Contact Us</h5>
                              <i class="far fa-envelope fa-fw"></i><a href="mailto:HealthScienceAdvising@valenciacollege.edu">HealthScienceAdvising@valenciacollege.edu</a> <br>
                              <i class="far fa-phone fa-fw"></i>407-582-1228<br>
                              <i class="far fa-fax fa-fw"></i>407-582-5462 (fax) 
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/health-sciences/nursing/index.pcf">©</a>
      </div>
   </body>
</html>