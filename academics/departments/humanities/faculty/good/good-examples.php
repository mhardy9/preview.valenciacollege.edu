<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/faculty/good/good-examples.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li><a href="/academics/departments/humanities/faculty/">Faculty</a></li>
               <li><a href="/academics/departments/humanities/faculty/good/">Good</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <div>  
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../../../locations/east/humanities/faculty/good/arrow.gif" width="8">
                                 <img alt="" height="8" src="../../../../../locations/east/humanities/faculty/good/arrow.gif" width="8">
                                 
                                 
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121" target="_blank">College-Wide Arts &amp; Events Calendar</a></li>
                                                   
                                                   <li><a href="http://www.flprints.com/" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                   
                                                   <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                   
                                                   <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                          </div> 
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          <a name="content" id="content"></a>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      <link href="../../../../../locations/includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured 1" border="0" height="260" src="../../../../../locations/east/humanities/faculty/good/featured-1.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 2" border="0" height="260" src="../../../../../locations/east/humanities/faculty/good/featured-2.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 3" border="0" height="260" src="../../../../../locations/east/humanities/faculty/good/featured-3.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 4" border="0" height="260" src="../../../../../locations/east/humanities/faculty/good/featured-4.jpg" width="770">
                                                               
                                                            </div>  
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <h2>Examples of Selected Applications</h2>
                                                      
                                                      <p>In order to help you be successful in your application process here are some examples
                                                         for you to reference.
                                                      </p>
                                                      
                                                      <a href="documents/model-application-for-magruder-fellowship.pdf" target="_blank">Model Application for Magruder Fellowship</a><p>
                                                         <a href="documents/model-application-for-institute-participant.pdf" target="_blank">Model Application for Institute Participant</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../../../locations/east/humanities/faculty/good/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../../../locations/east/humanities/faculty/good/spacer.gif" width="770"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="../../../../../locations/east/humanities/faculty/good/spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="../../../../../locations/east/humanities/faculty/good/spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/faculty/good/good-examples.pcf">©</a>
      </div>
   </body>
</html>