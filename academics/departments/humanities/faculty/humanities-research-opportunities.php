<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/faculty/humanities-research-opportunities.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li><a href="/academics/departments/humanities/faculty/">Faculty</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <div>  
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../../locations/east/humanities/faculty/arrow.gif" width="8">
                                 <img alt="" height="8" src="../../../../locations/east/humanities/faculty/arrow.gif" width="8">
                                 
                                 
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121" target="_blank">College-Wide Arts &amp; Events Calendar</a></li>
                                                   
                                                   <li><a href="http://www.flprints.com/" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                   
                                                   <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                   
                                                   <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                          </div> 
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          <a name="content" id="content"></a>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      <link href="../../../../locations/includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured 1" border="0" height="260" src="../../../../locations/east/humanities/faculty/featured-1.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 2" border="0" height="260" src="../../../../locations/east/humanities/faculty/featured-2.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 3" border="0" height="260" src="../../../../locations/east/humanities/faculty/featured-3.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 4" border="0" height="260" src="../../../../locations/east/humanities/faculty/featured-4.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 5" border="0" height="260" src="../../../../locations/east/humanities/faculty/featured-5.jpg" width="770">
                                                               
                                                            </div>        
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <h2>Humanities Research Opportunities</h2>
                                                      
                                                      
                                                      
                                                      <p><strong><u>Bank of  America Eminent Scholar Chair In Humanities</u></strong>: Any tenured or tenure-track, humanities  faculty member may apply to lead the yearly
                                                         National Endowment for the  Humanities Institute. Any faculty member who teaches courses
                                                         in Area-2 of the  Gen-Ed Humanities requirements, regardless of contract status, may
                                                         apply to  participate.
                                                      </p>
                                                      
                                                      <p>See a list of Area-2 Courses here: <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#General_Education_%23General_Education_%23General_Education_">General Education and Course Requirements &amp;lt; Valencia College</a></p>
                                                      
                                                      <p>The Bank  of America Eminent Scholar Chair in Humanities is a one-year award provided
                                                         for  under Valencia's NEH Challenge Grant. The professor who is awarded the Bank of
                                                         America Eminent Scholar Chair receives release time and stipend. The chair's  primary
                                                         responsibilities revolve around designing, planning, and leading the  University Club
                                                         of Orlando Foundation Humanities Institute. This may be a  ten-day institute for advanced
                                                         study for Valencia faculty who teach courses for  humanities credit (area 2 of general
                                                         education) and will be held over the  course of one calendar year. Previous institutes
                                                         have consisted of seminar  series on Chinese, Russian, Cuban, Spanish, Peruvian, and
                                                         Argentinean culture  and history, and many of these institutes(funds allowing) included
                                                         travel  excursions to these countries as well. <br>
                                                         To begin  planning this institute, the chair will assist in the selection of a nationally
                                                         known scholar in the field of the topic who will be the primary consultant for  the
                                                         planning, design and organization of the summer institute. Releases from  regular
                                                         teaching duties will give the chair time to prepare him/herself to lead  the institute
                                                         and to plan, design, and organize it. The chair will also receive  funds for local
                                                         travel, as well as for educational materials for the institute,  as provided for by
                                                         the grant. The annual term of the chair is from January 1  until December 31.<br>
                                                         The call  for applications to serve as chair is usually announced every Fall semester.
                                                         An  announcement of the topic of the Institute, as well as a call for applications
                                                         to participate usually occurs early, every Spring semester. 
                                                      </p>
                                                      
                                                      <p>
                                                         For more  questions regarding the Institute contact: <a href="mailto:ewallman@valenciacollege.edu">ewallman@valenciacollege.edu</a>
                                                         
                                                         
                                                      </p>
                                                      
                                                      <p><strong><u>First  Union Bank National Bank of Florida and Chelsey G. Magruder Foundation
                                                               Fellowships</u></strong>: Any Valencia College instructor, regardless  of contract status, that teaches courses
                                                         in Area-2 of the Gen-Ed Humanities  requirements may apply for funding. 
                                                      </p>
                                                      
                                                      <p><strong>Amount:</strong> between $500 and $6,000 each
                                                      </p>
                                                      
                                                      <p>
                                                         <strong>Eligibility:</strong> Valencia faculty, either full-time or adjunct,  who teach courses in Area 2: Humanities
                                                         
                                                      </p>
                                                      
                                                      <p>
                                                         See a list of Area-2 Courses here: <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#General_Education_%23General_Education_%23General_Education_">General Education and Course Requirements &amp;lt; Valencia College</a></p>
                                                      
                                                      <p><strong>Purpose:</strong> To provide opportunities for faculty to design  a program of study for themselves
                                                         in a humanities discipline and to conduct  primary research which will contribute
                                                         to cultural diversity in the humanities  curriculum at Valencia.<br>
                                                         The call for applications is announced  simultaneously with the call for applications
                                                         to participate in the annual NEH  Institute. 
                                                      </p>
                                                      
                                                      <p>For more  questions regarding Humanities fellowships contact: <a href="mailto:ewallman@valenciacollege.edu">ewallman@valenciacollege.edu</a>
                                                         
                                                      </p>
                                                      
                                                      <p>
                                                         <strong><u>Selections  Committee </u></strong></p>
                                                      
                                                      <p>
                                                         A standing committee consisting of budget  manager, David O. Sutton, Humanities deans
                                                         from each campus, faculty co-chair  of the committee, Eric Wallman, and the previous
                                                         year’s institute leader  selects participants for the annual NEH Institute as well
                                                         as recipients of  fellowship funds. If for some reason a dean or committee member
                                                         cannot  participate in the selections process, they will nominate a tenure or  tenure-track
                                                         faculty member that has participated and/or led an institute in  the past to vote
                                                         as proxy. If the faculty co-chair or previous year’s institute  chair is applying
                                                         to lead the institute, they will abstain from voting.
                                                      </p>
                                                      
                                                      <p><u>The selections committee takes a number of  factors into consideration when making
                                                            selections. Selection is based on the  following</u>: 
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>quality of and intentions expressed in one’s  application</li>
                                                         
                                                         <li>previous participation. First time applicants  with strong applications will take
                                                            priority over previous institute  participants with strong applications
                                                         </li>
                                                         
                                                         <li>relevance of one’s proposal of study during  Institute participation or fellowship
                                                            research
                                                         </li>
                                                         
                                                         <li>service to the division and the college</li>
                                                         
                                                         <li>collegiality, misbehavior on past travel  excursions and/or breach of participation
                                                            contract
                                                         </li>
                                                         
                                                         <li>one’s use of former NEH Institute experiences  or fellowship funds and the quality
                                                            of their work
                                                         </li>
                                                         
                                                         <li>the fulfillment of previous obligations as a  participant of former institutes/fellowships</li>
                                                         
                                                         <li>overall effort and presentation of  application</li>
                                                         
                                                      </ul>
                                                      
                                                      <p>
                                                         <strong><a href="documents/neh-participants-agreement.pdf" target="_blank">National Endowment for the Humanities Institute Participation Agreement</a></strong> - If selected for participation in this program you must print and sign this agreement.
                                                         
                                                      </p>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../../locations/east/humanities/faculty/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../../locations/east/humanities/faculty/spacer.gif" width="770"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="../../../../locations/east/humanities/faculty/spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="../../../../locations/east/humanities/faculty/spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/faculty/humanities-research-opportunities.pcf">©</a>
      </div>
   </body>
</html>