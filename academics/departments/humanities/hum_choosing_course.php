<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/hum_choosing_course.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../locations/east/humanities/arrow.gif" width="8">
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121" target="_blank">College-Wide Arts &amp; Events Calendar</a></li>
                                                   
                                                   <li><a href="http://www.flprints.com/" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                   
                                                   <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                   
                                                   <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         <img alt="Humanities Banner" border="0" height="260" src="../../../locations/east/humanities/humanities-foreign-languages.jpg" width="770">
                                                         
                                                      </div>
                                                      
                                                      <h2>Choosing a Humanities Course:</h2>
                                                      Many students ask, <strong>"Why should I take a Humanities course?"</strong><br>
                                                      <br>
                                                      
                                                      <strong>There are several reasons. By learning the humanities, you will learn to:</strong><br>
                                                      
                                                      <ul>
                                                         
                                                         <li>Think through your own ideas with help from the world's greatest thinkers</li>
                                                         
                                                         <li>Apply your creative abilities to new problems</li>
                                                         
                                                         <li>Communicate your thoughts and feelings more clearly</li>
                                                         
                                                         <li>Interact successfully with people of diverse backgrounds</li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                      <strong>People who love the humanities may choose a career as:</strong><br>
                                                      
                                                      <ul>  
                                                         
                                                         <li>Analyst for companies requiring insightful employees</li>
                                                         
                                                         <li>Curator of a museum or gallery</li>
                                                         
                                                         <li>Designer of educational computer software</li>
                                                         
                                                         <li>Elementary or secondary school teacher</li>
                                                         
                                                         <li>Free-lance magazine writer</li>
                                                         
                                                      </ul>
                                                      
                                                      <strong>In any career, the humanities will help you to:</strong><br>
                                                      
                                                      <ul> 
                                                         
                                                         <li>Reason through new problems and important decisions</li>
                                                         
                                                         <li>Analyze and interpret symbolic expression of all kinds</li>
                                                         
                                                         <li>Work in a team with different kinds of people</li>
                                                         
                                                         <li>Be sensitive to the diverse cultural influences in family and workplace</li>
                                                         
                                                         <li>Savor the beauty of the world around you</li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                      <p>In order to receive an associate’s degree, each student is required to take Humanities
                                                         Courses.
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li> But which course is right for you? </li>
                                                         
                                                         <li>Are some courses more beneficial for your major?
                                                            
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <p>These are all great questions. Choosing courses that address your interests and area
                                                         of study will enhance your experience at Valencia, so we have put together the following
                                                         list to help you choose a Humanities Course.
                                                      </p>
                                                      
                                                      <p>All Humanities courses challenge students to use critical thinking and to understand
                                                         the creative and intellectual arts in a new way. So regardless of the course you choose,
                                                         your education will be enhanced by these courses.
                                                      </p>
                                                      
                                                      <h3>Foreign Culture</h3>
                                                      
                                                      <p>All Humanities courses offer an integrated approach to the creative arts and intellectual
                                                         developments. Some courses offer this integrated approach as a means of studying a
                                                         foreign culture. So if you are studying or interested in international relations,
                                                         politics, foreign languages, etc., one of the following courses might be of interest
                                                         to you:
                                                         <br>
                                                         
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Asian Humanities</li>
                                                         
                                                         <li>Latin American Humanities</li>
                                                         
                                                         <li>Middle Eastern Humanities</li>
                                                         
                                                      </ul>
                                                      
                                                      <h3>Philosophy</h3>
                                                      
                                                      <p>Some courses emphasize philosophy more than others, as philosophical developments
                                                         were central to the culture in that era. So if you are studying or interested in philosophy,
                                                         the following courses might be of interest to you:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li> Philosophy</li>
                                                         
                                                         <li>Enlightenment and Romanticism</li>
                                                         
                                                         <li>Greeks and Romans</li>
                                                         
                                                      </ul>
                                                      
                                                      <h3>Religion                        </h3>
                                                      
                                                      <p>Some courses emphasize religion more than others, as religious developments were central
                                                         to the culture in that era. So if you are studying or interested in religion, the
                                                         following courses might be of interest to you:<br>
                                                         
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Late Roman Early Medieval</li>
                                                         
                                                         <li>Renaissance and Baroque</li>
                                                         
                                                         <li>Middle Eastern Humanities</li>
                                                         
                                                      </ul>   
                                                      
                                                      
                                                      <p><a href="hum_choosing_course.html#top">TOP</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="582"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="162"></div>
                                       </div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="598"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/hum_choosing_course.pcf">©</a>
      </div>
   </body>
</html>