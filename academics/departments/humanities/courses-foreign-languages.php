<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/courses-foreign-languages.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../locations/east/humanities/arrow.gif" width="8">
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121" target="_blank">College-Wide Arts &amp; Events Calendar</a></li>
                                                   
                                                   <li><a href="http://www.flprints.com/" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                   
                                                   <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                   
                                                   <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         <img alt="Humanities Banner" border="0" height="260" src="../../../locations/east/humanities/humanities-foreign-languages.jpg" width="770">
                                                         
                                                      </div>
                                                      
                                                      <h2>Foreign Languages</h2>
                                                      
                                                      <div>
                                                         
                                                         <ul>
                                                            
                                                            <li data-id="#tab-1">Courses Offered</li>
                                                            
                                                            <li data-id="#tab-2">Resources</li>
                                                            
                                                         </ul>
                                                         
                                                         
                                                         <div>
                                                            
                                                            
                                                            
                                                            
                                                            <div>
                                                               
                                                               <div><a href="courses-foreign-languages.html#">ARA1120 - Elementary Arabic I</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY ARABIC I<br>
                                                                        For students without two years of recent high school Arabic completed within the last
                                                                        three years or department approval. An introduction to written and spoken Modern Standard
                                                                        Arabic (MSA). Emphasis on Modern Standard Arabic (MSA) alphabet, use of the language,
                                                                        integrating basic grammar, vocabulary, composition and culture through a conversational
                                                                        approach to Arabic. Not open to native speakers. A minimum grade of C is required
                                                                        to pass this course if being used to satisfy the General Education Foreign Language
                                                                        requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">ARA1121 - Elementary Arabic II</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY ARABIC II <br>
                                                                        Prerequisites: Minimum grade of C in&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ARA%201120">ARA&nbsp;1120</a>&nbsp;or equivalent (ARA 1100 or two years of high school Arabic completed within the last
                                                                        three years and department approval). Continuation of the Modern Standard Arabic,
                                                                        developing, speaking, reading, and writing skills. Use of Arabic is emphasized throughout
                                                                        course focusing on increasing vocabulary and grammatical skills. Fundamental skills
                                                                        in Arabic comprehension, expression, and structure. Increasing awareness and understanding
                                                                        of the culture. Not open to native speakers. A minimum grade of C is required to pass
                                                                        this course if being used to satisfy the General Education Foreign Language Proficiency
                                                                        Requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">CHI1120 - Elementary Chinese I</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY CHINESE I<br>
                                                                        For students without two years of recent high school Chinese completed within the
                                                                        last three years or department approval. Emphasis on everyday use of the language,
                                                                        integrating basic grammar, vocabulary, composition and culture through a conversational
                                                                        approach to Chinese. Not open to native speakers. A minimum grade of C is required
                                                                        to pass this course if being used to satisfy the Foreign Language Proficiency requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">CHI1121 - Elementary Chinese II</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY CHINESE II <br>
                                                                        Prerequisite:&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=CHI%201120">CHI&nbsp;1120</a>&nbsp;or Departmental Approval Continuation of fundamental skills in Chinese comprehension,
                                                                        expression, and structure. Increasing awareness and understanding of the culture.
                                                                        A minimum grade of C is required to pass this course if being used to satisfy the
                                                                        Foreign Language Proficiency requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">JAP1120 - Elementary Japanese I</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY JAPANESE I  <br>
                                                                        For students without two years of recent high school Japanese completed within the
                                                                        last three years or department approval. Emphasis on everyday use of the language,
                                                                        integrating basic grammar, vocabulary, composition and culture through a conversational
                                                                        approach to Japanese. Not open to native speakers. A minimum grade of C is required
                                                                        to pass this course if being used to satisfy the Foreign Language Proficiency requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">JAP1121 - Elementary Japanese II</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY JAPANESE II <br>
                                                                        Prerequisite:&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=JPN%201120">JPN&nbsp;1120</a>&nbsp;or department approval Continuation of fundamental skills in Japanese comprehension,
                                                                        expression, and structure. Increasing awareness and understanding of the culture.
                                                                        A minimum grade of C is required to pass this course if being used to satisfy the
                                                                        Foreign Language Proficiency requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">FRE1120 - Elementary French I</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY FRENCH I <br>
                                                                        For students without two years of recent high school French completed    within the
                                                                        last three years or department approval. Emphasis on everyday use    of the language;
                                                                        integrating basic grammar, vocabulary, composition and    culture through a conversational
                                                                        approach to French. Not open to native    speakers. A minimum of a C is required to
                                                                        pass this course if being used to    satisfy the General Education Foreign Language
                                                                        requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">FRE1121 - Elementary French II</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY FRENCH II Prerequisite: FRE 1120 or Departmental approval.<br>
                                                                        Continuation of FRE 1120. Emphasis on everyday use of the    language, integrating
                                                                        basic grammar, vocabulary, composition, and culture    through a conversational approach
                                                                        to French. Not open to native speakers. A    minimum grade of C is required to pass
                                                                        this course if being used to satisfy    the General Education Foreign Language requirement.
                                                                        
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours:</strong> 4<br>
                                                                        
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">FRE2200 - Intermediate French I</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>INTERMEDIATE FRENCH I Prerequisite: FRE 1101 or departmental approval. <br>
                                                                        Conversational    approach with readings adapted from French newspapers, magazines,
                                                                        short    stories, and film. Increasing awareness and understanding of the French 
                                                                        culture.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">FRE2941 -  Intern Exploration in French</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>INTERNSHIP EXPLORATION IN FRENCH Prerequisites: Satisfactory completion of all mandated
                                                                        courses in reading, mathematics, English and English for Academic Purposes; a minimum
                                                                        2.0 institutional or overall GPA; and 12 credits, including a foreign language course
                                                                        at the intermediate level. The Program Director/Program Chair/Program Coordinator
                                                                        or Internship Placement Office has the discretion to provide override approval as
                                                                        it relates to the waiver of required program/discipline-related courses. 
                                                                        This course is a planned work-based experience that provides students with supervised
                                                                        career exploration activities and/ or practical experiences. Each earned credit of
                                                                        internship requires a minimum of 80 clock hours of work. Multiple credit course. May
                                                                        be repeated for credit, but grade forgiveness cannot be applied. (Internship Fee:
                                                                        $10.00) 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 2</strong></p>
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#"> LAT1120 - Elementary Latin I </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY LATIN I <br>
                                                                        Fundamental skills in Latin comprehension, expression and structure. Increasing awareness
                                                                        and understanding of the Roman culture. A minimum grade of C is required to pass this
                                                                        course if being used to satisfy the General Education Foreign Language Proficiency
                                                                        Requirement. 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 4</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#"> SPN1120 - Elementary Spanish I </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY SPANISH I <br>
                                                                        For students without two years of recent high school Spanish completed within the
                                                                        last three years or department approval. Emphasis on everyday use of the language;
                                                                        integrating basic grammar, vocabulary, composition and culture through a conversational
                                                                        approach to Spanish. Not open to native speakers. A minimum of a C is required to
                                                                        pass this course if being used to satisfy the General Education Foreign Language requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 4</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#"> SPN1120 - Intensive Elementary Spanish I </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY SPANISH I <br>
                                                                        For students without two years of recent high school Spanish completed within the
                                                                        last three years or department approval. Emphasis on everyday use of the language;
                                                                        integrating basic grammar, vocabulary, composition and culture through a conversational
                                                                        approach to Spanish. Not open to native speakers. A minimum of a C is required to
                                                                        pass this course if being used to satisfy the General Education Foreign Language requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 4</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#"> SPN1121 - Elementary Spanish II </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY SPANISH II Prerequisite: SPN 1120 or Departmental Approval.<br>
                                                                        Continuation of fundamental skills in Spanish comprehension, expression, and structure.
                                                                        Increasing awareness and understanding of the culture. A minimum grade of C is required
                                                                        to pass this course if being used to satisfy the General Education Foreign Language
                                                                        requirement. 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 4</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#"> SPN1121 - Intensive Elementary Spanish II </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ELEMENTARY SPANISH II Prerequisite: SPN 1120 or Departmental Approval.<br>
                                                                        Continuation of fundamental skills in Spanish comprehension, expression, and structure.
                                                                        Increasing awareness and understanding of the culture. A minimum grade of C is required
                                                                        to pass this course if being used to satisfy the General Education Foreign Language
                                                                        requirement. 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 4</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#"> SPN2200 - Intermediate Spanish I </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>INTERMEDIATE SPANISH I Prerequisite: SPN 1101 or department approval. <br>
                                                                        Conversational approach with readings adapted from Spanish newspapers, magazines,
                                                                        short novels, Spanish essays, poems, and a wide variety of Spanish articles. Emphasis
                                                                        on written narrative as well as language integrating grammar through compositions,
                                                                        vocabulary, films, presentations, and cultural presentations.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">SPN2941 - Intern Exploration in Spanish </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>INTERNSHIP EXPLORATION IN SPANISH Prerequisites: Satisfactory completion of all mandated
                                                                        courses in Reading, Mathematics, English, and English for Academic Purposes; a minimum
                                                                        2.0 institutional or overall GPA; and 12 credits including a foreign language course
                                                                        at the intermediate level. The Program Director/Program Chair/Program Coordinator
                                                                        or Internship Placement Office has the discretion to provide override approval as
                                                                        it relates to the waiver of required program/discipline-related courses. 
                                                                        This course is a planned work-based experience that provides students with supervised
                                                                        career exploration activities and/ or practical experiences. Each earned credit of
                                                                        internship requires a minimum of 80 clock hours of work. Multiple credit course. May
                                                                        be repeated for credit, but grade forgiveness cannot be applied. (Internship Fee:
                                                                        $10.00) 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 1</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">SPN2941 - Intern Exploration in Spanish </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>INTERNSHIP EXPLORATION IN SPANISH Prerequisites: Satisfactory completion of all mandated
                                                                        courses in Reading, Mathematics, English, and English for Academic Purposes; a minimum
                                                                        2.0 institutional or overall GPA; and 12 credits including a foreign language course
                                                                        at the intermediate level. The Program Director/Program Chair/Program Coordinator
                                                                        or Internship Placement Office has the discretion to provide override approval as
                                                                        it relates to the waiver of required program/discipline-related courses. 
                                                                        This course is a planned work-based experience that provides students with supervised
                                                                        career exploration activities and/ or practical experiences. Each earned credit of
                                                                        internship requires a minimum of 80 clock hours of work. Multiple credit course. May
                                                                        be repeated for credit, but grade forgiveness cannot be applied. (Internship Fee:
                                                                        $10.00) 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 2</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <div><a href="courses-foreign-languages.html#">SPN1340 - Spanish for Heritage Speaker </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>SPANISH FOR HERITAGE SPEAKER <br>
                                                                        This course is designed for heritage speakers of Spanish who have acquired oral proficiency
                                                                        in a non-academic environment. The activities in this course will improve several
                                                                        aspects of language learning: oral comprehension, speaking, reading comprehension
                                                                        and analysis, and writing skills. The emphasis will be on improving linguistic skills
                                                                        through specific grammatical and lexical studies designed to meet the particular needs
                                                                        of heritage speakers of Spanish. Special attention will be given to Hispanic culture.
                                                                        
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 4</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         <div>
                                                            
                                                            <h3>Foreign Languages Resources</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="../../../locations/east/academic-success/language/index.html">Foreign Language Lab</a></li>
                                                               
                                                            </ul>
                                                            
                                                            <h3>French</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/languages/french/index.shtml?survey=no&amp;url=www.bbc.co.uk/languages/french/index.shtml&amp;site=languagesfrench&amp;js=yes" target="_blank">BBC  - Languages - Learn French</a></li>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/french/" target="_blank">BBCAfrique.com Informations French  News index</a></li>
                                                               
                                                               <li><a href="http://espaces.vhlcentral.com/" target="_blank">Espaces Supersite</a></li>
                                                               
                                                               <li><a href="http://promenades.vhlcentral.com/" target="_blank">http://promenades.vhlcentral.com/</a></li>
                                                               
                                                               <li><a href="http://ielanguages.com/french.html" target="_blank">French Tutorials Index</a></li>
                                                               
                                                               <li><a href="http://conjuguemos.com/home/docs/php/list.php?type=verbs&amp;division=verbs&amp;language=french" target="_blank">http://conjuguemos.com/FRENCH</a></li>
                                                               
                                                               <li><a href="http://french.about.com/" target="_blank">Learn French - Online Lessons and Tips to  Learn French</a></li>
                                                               
                                                               <li><a href="http://books.quia.com/books" target="_blank">Quia - Quia Books</a></li>
                                                               
                                                               <li><a href="http://faculty.valencia.cc.fl.us/schater/" target="_blank">Samira Chater -  Communications</a></li>
                                                               
                                                               <li><a href="http://french.typeit.org/">Type French accents online</a></li>
                                                               
                                                               <li>
                                                                  <a href="http://www.wordreference.com/conj/FrVerbs.asp" target="_blank">WordReference.com</a>
                                                                  
                                                               </li>
                                                               
                                                            </ul>
                                                            
                                                            <h3>German</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/languages/german/">BBC - Languages - German</a></li>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/languages/german/">BBC - Languages - Learn German<br>
                                                                     </a></li>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/languages/german/lj/">http://www.bbc.co.uk/languages/german/lj/</a></li>
                                                               
                                                               <li><a href="http://www.uni.edu/becker/German2.html">Best German Web Sites</a></li>
                                                               
                                                               <li><a href="http://conjuguemos.com/home/docs/php/list.php?type=verbs&amp;division=verbs&amp;language=german">http://conjuguemos.com/GERMAN</a></li>
                                                               
                                                               <li><a href="http://www.fredriley.org.uk/call/langsite/german.html">German  Language Sites</a></li>
                                                               
                                                               <li><a href="http://ielanguages.com/German.html">German Tutorials Index Basic  Phrases, Vocabulary and Grammar</a></li>
                                                               
                                                               <li><a href="http://www.goethe.de/enindex.htm">Goethe-Institut&nbsp;- Learning  German, Experiencing Culture</a></li>
                                                               
                                                               <li><a href="http://gut.languageskills.co.uk/index.html">Homepage - Gut! -  Language Skills</a></li>
                                                               
                                                               <li><a href="http://www.italki.com/">italki - Language Exchange and Learning </a></li>
                                                               
                                                               <li><a href="http://german.about.com/">Learn German - German Language Lessons -  Speak German - Deutsch</a></li>
                                                               
                                                               <li><a href="http://www.isu.edu/~nickcrai/german.html">Sites for German</a></li>
                                                               
                                                               <li><a href="http://german.typeit.org/">Type German characters online</a></li>
                                                               
                                                               <li>
                                                                  <a href="http://www.heinle.com/cgi-wadsworth/course_products_wp.pl?fid=M20bI&amp;flag=instructor&amp;product_isbn_issn=9781413012828&amp;disciplinenumber=305">WieGehts</a>
                                                                  
                                                               </li>
                                                               
                                                            </ul>
                                                            
                                                            <h3>Spanish</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/mundo/index.shtml">http://www.bbc.co.uk/mundo/</a></li>
                                                               
                                                               <li><a href="http://conjuguemos.com/home/docs/php/list.php?type=verbs&amp;division=verbs&amp;language=spanish">http://conjuguemos.com/SPANISH</a></li>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/languages/spanish/">BBC - Languages - Learn  Spanish</a></li>
                                                               
                                                               <li><a href="http://www.bbc.co.uk/languages/spanish/">BBC - Languages - Spanish</a></li>
                                                               
                                                               <li><a href="http://news.bbc.co.uk/hi/spanish/news/">BBC Mundo Portada</a></li>
                                                               
                                                               <li><a href="http://www.diccionarios.com/">Diccionarios.com</a></li>
                                                               
                                                               <li><a href="http://www.italki.com/">italki - Language Exchange and Learning </a></li>
                                                               
                                                               <li><a href="http://www.spaleon.com/index.php">Learn Spanish online</a></li>
                                                               
                                                               <li><a href="http://www.studyspanish.com/">Learn Spanish</a></li>
                                                               
                                                               <li><a href="http://cwx.prenhall.com/bookbind/pubbooks/mosaicos2/">Mosaicos</a></li>
                                                               
                                                               <li><a href="http://pegasus2.pearsoned.com/Pegasus/frmLogin.aspx?s=3">My Spanish  Lab</a></li>
                                                               
                                                               <li><a href="http://quizlet.com/search/spanish/">Quizlet Search</a></li>
                                                               
                                                               <li><a href="http://www.myhq.com/public/v/a/vanderwerken/">myHq Spanish Websites  — MRTS</a></li>
                                                               
                                                               <li><a href="http://www.colby.edu/~bknelson/SLC/index.php">Spanish Grammar  Exercises</a></li>
                                                               
                                                               <li><a href="http://spanish.about.com/">Spanish Language learn Spanish grammar,  vocabulary and culture</a></li>
                                                               
                                                               <li><a href="http://ielanguages.com/spanish.html">Spanish Tutorials Index Basic  Phrases, Vocabulary and Grammar</a></li>
                                                               
                                                               <li><a href="http://spanish.typeit.org/">Type Spanish accents online</a></li>
                                                               
                                                               <li><a href="http://www.verbix.com/webverbix/index.asp">Verbix -- conjugate  verbs in 100+ languages</a></li>
                                                               
                                                               <li><a href="http://www.wordreference.com/conj/EsVerbs.asp">WordReference.com</a></li>
                                                               
                                                            </ul>
                                                            
                                                            <h3>Other Links</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="https://atlas.valenciacollege.edu/">Atlas</a></li>
                                                               
                                                               <li><a href="http://espaces.vhlcentral.com/">Espaces Supersite</a></li>
                                                               
                                                               <li><a href="http://www.google.com/">Google</a></li>
                                                               
                                                               <li><a href="http://myspanishlab.com/">MySpanishLab</a></li>
                                                               
                                                               <li><a href="http://books.quia.com/books">Quia - Quia Books</a></li>
                                                               
                                                               <li>
                                                                  <a href="http://typeit.org/">TypeIt</a>                
                                                               </li>
                                                               
                                                            </ul>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      <p><a href="courses-foreign-languages.html#top">TOP</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="582"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="162"></div>
                                       </div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="598"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/courses-foreign-languages.pcf">©</a>
      </div>
   </body>
</html>