<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/hum_faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../locations/east/humanities/arrow.gif" width="8">
                                 <img alt="" height="8" src="../../../locations/east/humanities/arrow.gif" width="8">
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121" target="_blank">College-Wide Arts &amp; Events Calendar</a></li>
                                                   
                                                   <li><a href="http://www.flprints.com/" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                   
                                                   <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                   
                                                   <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         <img alt="Humanities Banner" border="0" height="260" src="../../../locations/east/humanities/humanities-foreign-languages.jpg" width="770">
                                                         
                                                      </div>
                                                      
                                                      <h2>Faculty &amp; Staff List           
                                                         
                                                      </h2>
                                                      
                                                      <h3>Humanities</h3>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Professor Allen" height="200" src="../../../locations/east/humanities/RachelAllen2.jpg" width="139"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div> <strong><a href="mailto:Rallen39@valenciacollege.edu">Rachel Allen</a></strong>
                                                                                                
                                                                                             </div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2709</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room: </strong>1-357
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong> Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/?rallen39">Click Here</a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <blockquote>&nbsp;</blockquote>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="George Brooks" height="200" src="../../../locations/east/humanities/Brooks_WebPortrait.jpg" width="156"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div> <a href="mailto:gbrooks@valenciacollege.edu">George Brooks </a>
                                                                                                
                                                                                             </div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2721</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 1-115A
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong> Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=gbrooks"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div><a href="mailto:jbassetti@valenciacollege.edu">Jeremy Bassetti </a></div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><em>Professor of Humanities </em></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2364</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 3-132 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>Homepage: <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=jbassetti">Faculty FrontDoor</a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div> 
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong><a href="mailto:ddutkofski@valenciacollege.edu">Dan Dutkofski</a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2067</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 1-338
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=ddutkofski">Faculty Frontdoor</a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="John Edwards" height="203" src="../../../locations/east/humanities/JohnEdwardsPhoto.jpg" width="162"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div> <a href="mailto:jedwards@valenciacollege.edu">John Edwards </a>
                                                                                                
                                                                                             </div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities</i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2790</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room: </strong>1-359
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong> Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=jedwards"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong><a href="mailto:jorlando@valenciacollege.edu">Jeff Orlando</a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2843</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 1-147
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=jorlando"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div> 
                                                                              <p><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></p>
                                                                              
                                                                              
                                                                           </div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             <a href="mailto:tpasfield@valenciacollege.edu">Nicole Hill </a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><em>Professor of Humanities </em></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2759</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room: </strong>2-313
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=nhill9"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong><a href="mailto:jhasars@valenciacollege.edu">Jessica Hasara</a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2163</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 1-333
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div><a href="mailto:slake4@valenciacollege.edu">Sean Lake</a></div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2054</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 1-145 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>Homepage: <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=slake4">Faculty FrontDoor</a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             <a href="mailto:jstone2@valenciacollege.edu">Justin Stone</a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2052</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room: </strong>1-141 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=jstone2"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Dr Jennifer Taylor" height="179" src="../../../locations/east/humanities/Jen.jpg" width="145"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div><strong>
                                                                                                   
                                                                                                   <div> <a href="mailto:jtaylor@valenciacollege.edu">Jennifer Taylor</a>
                                                                                                      
                                                                                                   </div>
                                                                                                   </strong></div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Humanities Department Coordinator </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2746</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 6-118C 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=jtaylor"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div><strong><a href="mailto:kstyles@valenciacollege.edu">Karen Styles</a></strong></div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2232</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 1-335 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=kstyles"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Professor Wallman" height="200" src="../../../locations/east/humanities/wallmanamibia2.jpg" width="142"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div> <a href="mailto:ewallman@valenciacollege.edu">Eric Wallman</a>
                                                                                                
                                                                                             </div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i> Professor of Humanities and Philosophy </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2814</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 1-319 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong> Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=ewallman"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Areej Zufari" height="178" src="../../../locations/east/humanities/Areejphoto.jpg" width="130"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div><strong><a href="mailto:azufari1@valenciacollege.edu">Areej Zufari</a></strong></div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><i>Professor of Humanities </i></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2349</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room:</strong> 6-119 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Homepage:</strong> <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=azufari1"><strong>Faculty FrontDoor</strong></a>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <p><strong>Foreign Languages</strong></p>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div><a href="mailto:azehel@valenciacollege.edu">Ashley Zehel</a></div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><em>Professor of Foreign Languages </em></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2316</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>Building/Room: 3-132</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>Homepage: <strong><a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=azehel">Faculty FrontDoor</a></strong>
                                                                                          
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo: No Photo" height="133" src="../../../locations/east/humanities/no_photo.gif" width="100"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <div>
                                                                                             <strong><a href="mailto:ygonzalez@valenciacollege.edu">Yolanda Gonzalez </a></strong> 
                                                                                          </div>
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><em>Professor of Foreign Languages </em></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>407-582-2477</div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>
                                                                                          <strong>Building/Room: </strong>3-107 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>Homepage: <a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=ygonzalez">Faculty FrontDoor</a></strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p><a href="hum_faculty.html#top">TOP</a></p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div><img alt="Photo of Professor Salto" height="201" src="../../../locations/east/humanities/Juan.jpg" width="156"></div>
                                                                           
                                                                           <div>
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><strong>
                                                                                             
                                                                                             <div><a href="mailto:kfowler12@valenciacollege.edu">Juan-Alberto Salto </a></div>
                                                                                             </strong></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div><em>Professor of Foreign Languages </em></div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div> (407) 582-2083. </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>Office: 
                                                                                          
                                                                                          
                                                                                          Building 3 - Room 129
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       <div>Homepage: 
                                                                                          
                                                                                          
                                                                                          <a href="http://frontdoor.valenciacollege.edu/?jsalto"><strong>Faculty Frontdoor</strong></a> 
                                                                                       </div>
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                    <div>
                                                                                       
                                                                                       
                                                                                    </div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              <br>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="582"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="162"></div>
                                       </div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="598"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/hum_faculty.pcf">©</a>
      </div>
   </body>
</html>