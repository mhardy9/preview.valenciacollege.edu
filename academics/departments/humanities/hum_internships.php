<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/hum_internships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../locations/east/humanities/arrow.gif" width="8">
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121" target="_blank">College-Wide Arts &amp; Events Calendar</a></li>
                                                   
                                                   <li><a href="http://www.flprints.com/" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                   
                                                   <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                   
                                                   <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         <img alt="Humanities Banner" border="0" height="260" src="../../../locations/east/humanities/humanities-foreign-languages.jpg" width="770">
                                                         
                                                      </div>
                                                      
                                                      <h2>Internships:</h2>
                                                      
                                                      <p> Earn credit towards your degree AND 
                                                         gain experience for your resume.
                                                      </p>
                                                      
                                                      <p><strong>Intern at the:</strong><br>
                                                         
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Albin Polasek Museum &amp; Sculpture Garden </li>
                                                         
                                                         <li>Orlando Weekly </li>
                                                         
                                                         <li>Orlando Museum of Art </li>
                                                         
                                                         <li>Coalition for the Homeless </li>
                                                         
                                                         <li>Girl Scouts of Citrus Council </li>
                                                         
                                                         <li>Heart of Florida United Way </li>
                                                         
                                                         <li>Garden Theatre</li>
                                                         
                                                         <li>Valencia College Osceola Campus Arts Department </li>
                                                         
                                                      </ul>
                                                      For more information, email Professor Zufari - <a href="mailto:azufari1@valenciacollege.edu">azufari1@valenciacollege.edu</a> and log onto<br>
                                                      <a href="../../../locations/internship/index.html" target="_blank" title="Link to IPO Home Page">Internship and Placemement Office</a><br>
                                                      <br>
                                                      
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p><strong>Prefix:</strong> HUM2941
                                                                  </p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p><strong>Title:</strong> Intern Explor in Humanities
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p><strong>Credit Hours:</strong> 1
                                                                  </p>
                                                               </div>
                                                               
                                                               
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p><strong>Description:</strong><br>
                                                                     INTERNSHIP EXPLORATION IN HUMANITIES Prerequisites: Satisfactory completion    of
                                                                     all mandated courses in Reading, Mathematics, English, and English for    Academic
                                                                     Purposes; a minimum 2.0 institutional or overall GPA; and 12 credits    including
                                                                     6 credits in Humanities, three of which must be a Gordon Rule    writing course; and
                                                                     Internship Office approval. The Program Director/Program    Chair/Program Coordinator
                                                                     or Internship Placement Office has the discretion    to provide override approval
                                                                     as it relates to the waiver of required    program/discipline-related courses. <br>
                                                                     This course is a planned work-based experience that provides    students with supervised
                                                                     career exploration activities and/ or practical    experiences. Each earned credit
                                                                     of internship requires a minimum of 80 clock    hours of work. Multiple credit course.
                                                                     May be repeated for credit, but grade    forgiveness cannot be applied. (Internship
                                                                     Fee: $10.00) <br>
                                                                     <br>
                                                                     
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p><strong>Prefix:</strong> HUM2941
                                                                  </p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p><strong>Title:</strong> Intern Explor in Humanities
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p><strong>Credit Hours:</strong> 2
                                                                  </p>
                                                               </div>
                                                               
                                                               
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p><strong>Description:</strong><br>
                                                                     INTERNSHIP EXPLORATION IN HUMANITIES Prerequisites: Satisfactory completion    of
                                                                     all mandated courses in Reading, Mathematics, English, and English for    Academic
                                                                     Purposes; a minimum 2.0 institutional or overall GPA; and 12 credits    including
                                                                     6 credits in Humanities, three of which must be a Gordon Rule    writing course; and
                                                                     Internship Office approval. The Program Director/Program    Chair/Program Coordinator
                                                                     or Internship Placement Office has the discretion    to provide override approval
                                                                     as it relates to the waiver of required    program/discipline-related courses. <br>
                                                                     This course is a planned work-based experience that provides    students with supervised
                                                                     career exploration activities and/ or practical    experiences. Each earned credit
                                                                     of internship requires a minimum of 80 clock    hours of work. Multiple credit course.
                                                                     May be repeated for credit, but grade    forgiveness cannot be applied. (Internship
                                                                     Fee: $10.00) <br>
                                                                     <br>
                                                                     
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <p><a href="hum_internships.html#top">TOP</a>
                                                         
                                                      </p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="582"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="162"></div>
                                       </div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="598"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/hum_internships.pcf">©</a>
      </div>
   </body>
</html>