<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/courses-humanities.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../locations/east/humanities/arrow.gif" width="8">
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://events.valenciacollege.edu/calendar/month?event_types%5B%5D=13121" target="_blank">College-Wide Arts &amp; Events Calendar</a></li>
                                                   
                                                   <li><a href="http://www.flprints.com/" target="_blank">Florida  Department of Education Fingerprinting</a></li>
                                                   
                                                   <li><a href="http://www.fldoe.org/edcert/steps.asp" target="_blank">Florida  Department of Education – Steps to Certification</a></li>
                                                   
                                                   <li><a href="http://www.osceola.k12.fl.us/" target="_blank">Osceola County  Public Schools</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         <img alt="Humanities Banner" border="0" height="260" src="../../../locations/east/humanities/humanities-foreign-languages.jpg" width="770">
                                                         
                                                      </div>
                                                      
                                                      <h2>Humanities</h2>
                                                      
                                                      <div>
                                                         
                                                         <ul>
                                                            
                                                            <li data-id="#tab-1">Courses Offered</li>
                                                            
                                                            <li data-id="#tab-2">Resources</li>
                                                            
                                                         </ul>
                                                         
                                                         
                                                         <div>
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            <div>
                                                               
                                                               <div><a href="courses-humanities.html#"> ARH2050 - Intro to Art History I </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Survey of development of visual art forms from    prehistory through Middle Ages.
                                                                        <br>
                                                                        
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">ARH2051 - Introduction to Art History II </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Prerequisite: Minimum grade of C in ENC1101 or    ENC 1101H or IDH 1110. </p>
                                                                     
                                                                     <p>Survey of development of visual arts from Renaissance    to present. Gordon Rule course
                                                                        in which the student is required to    demonstrate college-level writing skills through
                                                                        multiple assignments.    
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement. <br>
                                                                        
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong><br>
                                                                        
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">ARH1000 - Art Appreciation </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Introductory art appreciation course    designed to provide student with foundation
                                                                        for understanding contemporary    visual arts.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">HUM1020 - Introduction to Humanities</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>Basic introduction to humanities. Focuses on central concepts, historical development
                                                                        and fundamental nature of philosophy, architecture, music, religion and art. Concepts
                                                                        from such disciplines integrated with contemporary American culture.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3 </strong></p>
                                                                     
                                                                     <p><strong>HUM1020-H Introduction to Humanities- Honors </strong>content will satisfy one Honors Program learning outcome. Honors Program permission
                                                                        required
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">HUM2220 - Greek and Roman Humanities</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>Prerequisite: ENC 1101 or ENC 1101H or IDH 1110 -Integrated examination of dominant
                                                                        ideas in Western culture as expressed in art, literature, music, philosophy and religion.
                                                                        Covers period from Trojan War through Roman era, emphasizing development and influence
                                                                        of classical ideas. Gordon Rule course which requires 6,000 words of writing. 
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement. </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3 </strong></p>
                                                                     
                                                                     <p><strong>HUM2220H- Greek and Roman Humanities- Honors </strong>content will satisfy one Honors Program learning outcome. Honors Program permission
                                                                        required
                                                                     </p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">HUM2223 - Late Roman and Medieval Humanities</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Prerequisites: Minimum grade of C in ENC 1101 or ENC 1101H or IDH 1110 </p>
                                                                     
                                                                     <p>Integrated examination of dominant ideas in Western culture expressed in art, literature,
                                                                        music, philosophy and religion. Covers period from late Roman Empire through Middle
                                                                        Ages, emphasizing development and historical influence of Christianity. . 
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement. </p>
                                                                     
                                                                     <p><strong>HUM2223H- Late Roman and Medieval Humnaities- Honors </strong>content will satisfy one Honors Program learning outcome. Honors Program permission
                                                                        required
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">HUM2232 - Renaissance and Baroque Humanities</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Prerequisites: Minimum grade of C in ENC 1101 or ENC 1101H or IDH 1110 </p>
                                                                     
                                                                     <p> Integrated examination of dominant ideas in Western culture expressed in art, literature,
                                                                        music, philosophy and religion. Covers period from Renaissance through Baroque era,
                                                                        emphasizing synthesis of classical and Christian elements. Gordon Rule course in which
                                                                        the student is required to demonstrate college-level writing skills through multiple
                                                                        writing assignments  . 
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement. </p>
                                                                     
                                                                     <p><strong>HUM2232H- Renissance and Baroque Humanities- Honors </strong>content will satisfy one Honors Program learning outcome. Honors Program permission
                                                                        required
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">HUM2234 - Enlightenment and Romantisicm Humanities </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Prerequisites: Minimum grade of C in ENC 1101 or ENC 1101H or IDH 1110 </p>
                                                                     
                                                                     <p> IIntegrated examination of dominant ideas in Western culture expressed in art, literature,
                                                                        music, philosophy and religion. Covers period from Enlightenment through 19th century,
                                                                        emphasizing emergence of rationalism and modern science and Romantic rebellion. Gordon
                                                                        Rule course which requires 6,000 words of writing.
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement. </p>
                                                                     
                                                                     <p><strong>HUM2234H- Enlightenment and Romantisicm Humanities- Honors </strong>content will satisfy one Honors Program learning outcome. Honors Program permission
                                                                        required
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">HUM2250 - 20th Century Humanities </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Prerequisites: Minimum grade of C in ENC 1101 or ENC 1101H or IDH 1110 </p>
                                                                     
                                                                     <p> Integrated examination of dominant ideas in Western culture expressed in art, literature,
                                                                        music, philosophy and religion. Covers period from turn of century to present. Focuses
                                                                        on creative forces which have shaped contemporary consciousness from pioneering work
                                                                        of Einstein, Picasso, Stravinsky and Wright through dominance of objective consciousness
                                                                        to newly emerging guiding myths of today. Gordon Rule course which requires 6,000
                                                                        words of writing.
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement. </p>
                                                                     
                                                                     <p><strong>HUM2250H-20th Century Humnaities- Honors </strong>content will satisfy one Honors Program learning outcome. Honors Program permission
                                                                        required
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#">HUM2410 - Asian Humanities </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p> Prerequisites: Minimum grade of C in ENC 1101 or ENC 1101H    or IDH 1110 </p>
                                                                     
                                                                     <p>Examines dominant ideas and arts in South and East Asian cultures    expressed in
                                                                        philosophy, literature, art, architecture, and music. Focus on    India, China, and
                                                                        Japan; covers period from earliest civilization to present.    Gordon rule course
                                                                        in which the student is required to demonstrate college -    level writing skills
                                                                        through multiple writing assignments. 
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C    required if used to satisfy Gordon Rule requirement. </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> HUM2461 - Latin American Humanities </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>LATIN AMERICAN HUMANITIES Prerequisite: Minimum grade of C in ENC1101 or ENC    101H
                                                                        or IDH 1110 An integrated study of the history and culture and Latin    America. Focuses
                                                                        on how modern Latin American cultures are the product of a    historic biological
                                                                        and cultural interchange between Europe, Africa, and the    Americas. Topics include
                                                                        pre-Columbian civilizations, the Encounter and its    aftermath, independence and
                                                                        neo-colonialism, and modernist art, literature,    and cinema. Gordon Rule course
                                                                        in which the student is required to    demonstrate college-level writing skills through
                                                                        multiple assignments.    
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement.</p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> LAT1121 - Elemtary Latin II</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>LAT 1121 ELEMENTARY LATIN II<br>
                                                                        Prerequisite: Minimum grade of C in LAT 1120 or Departmental approval Continuation
                                                                        of LAT 1120, Elementary Latin I. A minimum grade of C is required to pass this course
                                                                        if being used to satisfy the General Education Foreign Proficiency Requirement.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> HUM2403 - Middle Eastern Humanities </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>MIDDLE EASTERN HUMANITIES <br>
                                                                        The course covers topics such as Middle Eastern religions,    philosophy, literature,
                                                                        architecture, visual arts, music, and the effects of    modernity on the Middle East.
                                                                        Gordon Rule course in which the student is    required to demonstrate college-level
                                                                        writing skills through multiple writing    assignments.
                                                                     </p>
                                                                     
                                                                     <p> Minimum grade of C required if used to satisfy Gordon Rule    requirement. </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> HUM2310 - Mythology </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>MYTHOLOGY Prerequisite: Minimum grade of C in ENC 1101 or ENC 1101H or IDH    1110
                                                                        Examines world mythology in comparative perspective, analyzes myths with    a variety
                                                                        of methods, and considers the application of mythological ideas and    symbols in
                                                                        the humanities. Gordon Rule course in which the student is required to    demonstrate
                                                                        college-level writing skills through multiple assignments.    
                                                                     </p>
                                                                     
                                                                     <p>Minimum grade of C required if used to satisfy Gordon Rule requirement. </p>
                                                                     
                                                                     <p><strong><br>
                                                                           HUM2310H-MYTHOLOGY- HONORS</strong> Same as HUM2310 with honors content. Honors program permission required         
                                                                        
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> PHI2600 - Ethics and Critical Thinking </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>ETHICS AND CRITICAL THINKING Study of major theoretical principles on which    claims
                                                                        to good life and moral action have been based, such as hedonism,    utilitarianism
                                                                        and rationalism. Each theory illustrated by representative    selections from works
                                                                        of great philosophers from classical period to 20th    century. 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> PHI2010 - Philosophy </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>PHILOSOPHY Contemporary problems introduce major areas of philosophy:    metaphysics,
                                                                        ethics, aesthetics, theories of knowledge and philosophy of    religion. Students
                                                                        explore writings of notable philosophers, past and    present, and examine how their
                                                                        ideas have shed light on problems and their    relevance to modern life.
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> REL2300 - Understanding Religious Traditions</a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <p>UNDERSTANDING RELIGIOUS TRADITIONS Designed for students interested in    exploring
                                                                        various ways people have expressed religious views. Explores    questions that lead
                                                                        people to formulate religious answers and various    religious doctrines that formalize
                                                                        human concerns. Balances different    opinions from major religious traditions such
                                                                        as Christianity, Judaism,    Hinduism and Buddhism, among others, and helps students
                                                                        broaden perspectives    on religion. 
                                                                     </p>
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <div>x</div>
                                                               
                                                               
                                                               <div><a href="courses-humanities.html#"> REL2300 - World Religions </a></div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     RELIGION: Prerequisite:&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>&nbsp;or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101H">ENC&nbsp;1101H</a>&nbsp;or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=IDH%201110">IDH&nbsp;1110</a>&nbsp;Basic introduction to the world's religions. Explores practices and beliefs of major
                                                                     religious traditions including Christianity, Judaism, Hinduism, Islam, and Buddhism.
                                                                     Gordon Rule course which requires demonstration of college level writing skills through
                                                                     multiple assignments. Minimum grade of C required if used to satisfy Gordon Rule requirement.
                                                                     
                                                                     <p><strong>Credit Hours: 3</strong></p>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         <div>
                                                            
                                                            <h3>Recommended Web sites</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="http://www.metmuseum.org/" target="_blank">The Metropolitan Museum of Art</a></li>
                                                               
                                                               <li><a href="http://www.wccb-classical-fm.com/" target="_blank">The World Center for Classical Broadcasting</a></li>
                                                               
                                                               <li><a href="http://www.vroma.org/~bmcmanus/house.html">Roman Architecture</a></li>
                                                               
                                                               <li>
                                                                  <a href="http://www.neh.gov/">NEH~National Endowment For The Humanities</a><br>
                                                                  
                                                               </li>
                                                               
                                                            </ul>
                                                            
                                                            <h3>Research Tools</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="http://www.mla.org/style" target="_blank">MLA Style</a></li>
                                                               
                                                               <li><a href="http://bcs.bedfordstmartins.com/writersref6e/lmcontent/ch07/PDF/Hacker-Orlov-MLA.pdf">Sample MLA Research Paper </a></li>
                                                               
                                                               <li><a href="https://owl.english.purdue.edu/">The Owl Writing Lab</a></li>
                                                               
                                                               <li><a href="http://owl.excelsior.edu/posts/view/67">Documenting your material</a></li>
                                                               
                                                               <li>
                                                                  <a href="http://www.ijrhss.org/">International Journal of Research in Humanities &amp; Social Sciences</a><br>
                                                                  
                                                               </li>
                                                               
                                                            </ul>
                                                            
                                                            <h3>Library Services</h3>
                                                            
                                                            <ul>
                                                               
                                                               <li><a href="http://www.valenciacollege.edu/library/east/" target="_blank">East Campus Library </a></li>
                                                               
                                                               <li><a href="http://www.valenciacollege.edu/east/academicsuccess/tutoring/" target="_blank">East Campus Tutoring Center </a></li>
                                                               
                                                            </ul>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      <p><a href="courses-humanities.html#top">TOP</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="582"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="162"></div>
                                       </div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/east/humanities/spacer.gif" width="598"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/courses-humanities.pcf">©</a>
      </div>
   </body>
</html>