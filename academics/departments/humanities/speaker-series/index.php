<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/speaker-series/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/speaker-series/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li>Speaker Series</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../../locations/east/humanities/speaker-series/arrow.gif" width="8">
                                 
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       
                                       
                                       
                                       <div> 
                                          
                                          <div>
                                             <img alt="Featured Image 1" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured-Logo.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured1.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured-Logo.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured2.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured-Logo.jpg" width="930">
                                             
                                             
                                          </div> 
                                          
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured3.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured-Logo.jpg" width="930">
                                             
                                             
                                          </div> 
                                          
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured4.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured-Logo.jpg" width="930">
                                             
                                             
                                          </div> 
                                          
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured5.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured-Logo.jpg" width="930">
                                             
                                             
                                          </div>  
                                          
                                          
                                          <div>
                                             <img alt="Featured Image 2" border="0" height="314" src="../../../../locations/east/humanities/speaker-series/Featured6.jpg" width="930">
                                             
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h2>Don't Miss Francesco Attesti </h2>
                                       
                                       <p> Experience masterful musicianship in person!</p>
                                       
                                       <center>
                                          
                                          <p><a href="resources.html"><img alt="Attesti" border="0" height="161" src="../../../../locations/east/humanities/speaker-series/Attestisquare_000.jpg" width="185"></a></p>
                                          
                                          <p><a href="https://www.facebook.com/Valencia-College-Humanities-Speakers-Series-107927826070833/"><img alt="facebook logo" border="0" height="44" src="../../../../locations/east/humanities/speaker-series/Find_Us_On_Facebook_Logo_05_000.gif" width="150"></a></p>
                                          
                                       </center>
                                       
                                       <p><br>
                                          <br>
                                          
                                       </p>
                                       
                                       
                                       
                                       
                                       <p><br>
                                          
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <a href="about.html" title="About the Series"><strong></strong><center><strong>About the Series</strong></center></a>
                                       <a href="events.html" title="Up-coming Events"><strong></strong><center><strong>Up-coming Events</strong></center></a>
                                       <a href="past.html" title="Past Presentation"><strong></strong><center><strong>Past Presentation</strong></center></a>
                                       <a href="support.html" title="Supporting the Series"><strong></strong><center><strong>Supporting the Series</strong></center></a>
                                       <a href="resources.html" title="Academic Resources"><strong></strong><center><strong>Academic Resources</strong></center></a>
                                       <a href="http://net4.valenciacollege.edu/forms/east/humanities/contact.cfm" target="_blank" title="Contact Us"><strong></strong><center><strong>Contact Us</strong></center></a>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h2>Coming Soon</h2>
                                       
                                       <p><a href="https://www.eventbrite.com/e/books-for-living-talk-and-signing-with-author-will-schwalbe-tickets-31602809855"><img alt="Books for Living book cover" border="0" height="138" hspace="0" src="../../../../locations/east/humanities/speaker-series/books-for-living-fb-share_001.jpg" vspace="0" width="266"></a></p>
                                       
                                       <p>The award-winning and beloved writer of <em>End of Your Life Bookclub,</em> Will Schwalbe, will be   discussing and signing his new book<em> Books for Living</em> February 28th from 6-8 pm. 
                                       </p>
                                       
                                       <h4>Reserve your ticket <a href="https://www.eventbrite.com/e/books-for-living-talk-and-signing-with-author-will-schwalbe-tickets-31602809855"><strong> on Eventbrite.</strong></a> 
                                       </h4>
                                       
                                       <center> 
                                          
                                       </center> 
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>          
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        <div>
                           
                           
                           
                           
                           
                        </div> 
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/speaker-series/index.pcf">©</a>
      </div>
   </body>
</html>