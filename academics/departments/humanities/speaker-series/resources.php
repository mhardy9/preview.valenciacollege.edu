<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Speaker Series | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/speaker-series/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/speaker-series/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Speaker Series</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li><a href="/academics/departments/humanities/speaker-series/">Speaker Series</a></li>
               <li>Speaker Series</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Academic Resources:</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       
                                       <p><img alt="Concert poster-free piano concert" height="402" src="../../../../locations/east/humanities/speaker-series/PianoConcertflyer_000.jpg" width="300"></p>
                                       
                                       
                                       <p><a href="documents/attestiinfo.pdf">More about Francesco Attesti</a></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    
                                    <h2>Performance Program </h2>
                                    
                                    <p>                    <br>
                                       G. B. Pescetti (1704 – 1766): Allegretto<br>
                                       <br>
                                       F. Schubert (1797 – 1828): Impromptu Op. 90 n. 2 in A major<br>
                                       Impromptu Op. 90 n. 4 in A flat major<br>
                                       
                                       
                                       <br>
                                       <a href="http://www.musicacademyonline.com/composer/biographies.php?bid=29">F. Chopin (1810 – 1849): Waltz Op.69n. 2 in A flat major</a></p>
                                    
                                    <p>Mazurka Op. 17 n. 4 in A minor</p>
                                    
                                    <p><a href="http://www.musicacademyonline.com/composer/biographies.php?bid=26">J. Brahms (1833 - 1897): Intermezzo Op. 118 n. 2 in A major </a></p>
                                    
                                    <p>&nbsp; Rhapsody Op. 79 n. 1 in B minor</p>
                                    
                                    <p>Intermission</p>
                                    
                                    <p><a href="http://www.musicacademyonline.com/composer/biographies.php?bid=55">G. Verdi (1813 – 1901): Valzer</a></p>
                                    
                                    <p><a href="http://www.musicacademyonline.com/composer/biographies.php?bid=46">F. Liszt (1811 – 1886): Orage</a></p>
                                    
                                    <p><a href="http://www.mfiles.co.uk/composers/Erik-Satie.htm">E. Satie (1866 – 1925): Gnossienne N. 1</a></p>
                                    
                                    <p><a href="http://gershwin.com/the-gershwin-brothers/">G. Gershwin (1898 – 1937): 3 Preludes</a></p>
                                    
                                    <p>- Allegro ben ritmato e deciso</p>
                                    
                                    <p>- Andante con moto</p>
                                    
                                    <p>- Agitato</p>
                                    
                                    <p><a href="http://www.piazzolla.org/biography/biography-english.html">A. Piazzolla (1921 – 1992): Invierno porteño</a></p>
                                    
                                    <p>Verano porteño</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/speaker-series/resources.pcf">©</a>
      </div>
   </body>
</html>