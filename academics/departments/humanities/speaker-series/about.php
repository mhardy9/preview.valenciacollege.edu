<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Speaker Series | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/speaker-series/about.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/speaker-series/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Speaker Series</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li><a href="/academics/departments/humanities/speaker-series/">Speaker Series</a></li>
               <li>Speaker Series</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>About the Series</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="logo" height="127" src="../../../../locations/east/humanities/speaker-series/HSSLogo.jpg" width="300"></p>
                                    
                                    <p>The Humanities Speakers Series began on a shoestring budget over 30 years ago. Professors
                                       Elizabeth Eschbach and Carol Foltz established an annual roster of writers, thinkers,
                                       artists, activists, and scientists to speak at Valencia College. 
                                    </p>
                                    
                                    <p>Many other faculty members have led and contributed to the series such as Dr. George
                                       Brooks, Nichole Jackson, Nicole Hill, and Jeff Orlando. 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Dr. Kaku with students" height="269" src="../../../../locations/east/humanities/speaker-series/Students5_000.jpg" width="350"></p>
                                    
                                    <p>Physicist Dr. Michio Kaku greeting and posing for pictures. </p>                  
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>The Humanities Speakers Series was established and run with volunteer effort for over
                                       25 years until recently with Dr. Stacey Johnson adding her support to the faculty
                                       coordinating the series. 
                                    </p>
                                    
                                    <p>Funding comes from the office of Student Development on the East Campus and the Valencia
                                       Foundation. 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>                    
                                    <p><img alt="Nafisi with audience in conversation" height="179" src="../../../../locations/east/humanities/speaker-series/Students6_000.jpg" width="350"></p>                    
                                    <p>Dr. Azar Nafisi speaking with students and audience members after her keynote presentation.</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Today, the Series continues to grow and develop with our emphasis on connecting students
                                       directly to the great minds of today and inviting our Central Florida community to
                                       participate in our events.
                                    </p>
                                    
                                    <p>If you would like to support the series as a donor or as a sponsor, please go directly
                                       to the <a href="https://donate.valencia.org/pages/donation-forms/discipline-and-academic-enhancement">Valencia Foundation's website</a> and designate the East Campus Humanities Speakers Advancement as the recipient of
                                       your donation.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Temple Grandin" height="200" src="../../../../locations/east/humanities/speaker-series/students2_000.jpg" width="373"></p>
                                    
                                    <p>Dr. Temple Grandin signing books and meeting with students. </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p>Importantly, the views, comments, and work of our presenters do not represent the
                                          views of Valencia College, our administrators, or faculty. 
                                       </p>
                                       
                                       <p>The views, opinions, and positions expressed by our presenters are theirs alone. Our
                                          speakers and presenters are chosen by the Humanities Speakers Series advising committee
                                          and input from students, faculty, and community members.
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><img alt="Will Haygood" height="234" src="../../../../locations/east/humanities/speaker-series/students4.jpg" width="370"></p>
                                    
                                    <p>Author Wil Haygood best known for <em>The Butler</em> taking pictures and signing his latest book. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p>We would love to hear from you if you have a suggestion for a presenter. To send in
                           a suggestion, please click the button below. 
                        </p>
                        
                        <p><a href="https://www.facebook.com/Valencia-College-Humanities-Speakers-Series-107927826070833/"><img alt="Facebook link" border="0" height="66" src="../../../../locations/east/humanities/speaker-series/Facebook-Logo-Wallpaper-Full-HD_000.jpg" width="201"></a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/speaker-series/about.pcf">©</a>
      </div>
   </body>
</html>