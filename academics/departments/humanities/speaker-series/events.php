<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Speaker Series | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/speaker-series/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/speaker-series/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Speaker Series</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li><a href="/academics/departments/humanities/speaker-series/">Speaker Series</a></li>
               <li>Speaker Series</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Up-coming Events</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><img alt="Flyer for Attesti Performance" height="477" src="../../../../locations/east/humanities/speaker-series/PianoConcertflyer_001.jpg" width="356"></div>
                                 </div>
                                 
                                 <div>
                                    
                                    <h2>Francesco Attesti  </h2>
                                    
                                    <h6>Tuesday, February 7 7:00pm </h6>
                                    
                                    <h6>East Campus, Performing Arts Center</h6>
                                    
                                    <p><a href="https://www.eventbrite.com/e/dr-azar-nafisi-the-republic-of-imagination-tickets-27386098558">Tickets are not required for this event, but you can ensure a seat by reserving a
                                          ticket through Eventbrite.com. </a></p>
                                    
                                    <p>Tickets are free.</p>
                                    
                                    <p>Always drawn to the perfection of phrasing and the sound of the instrument, he has
                                       developed very special and unique expressions. Maestro Perticaroli himself has said:<br>
                                       “Francesco Attesti’s piano playing is unusual. Everything that happens in the sound,
                                       in the phrasing, and in the musical conception, starts from an aesthetic and musical
                                       conviction and it brings the performance out of every formal prevision.”
                                    </p>
                                    
                                    <p>Currently, Francesco Attesti performs regularly in internationally prestigious concert
                                       halls like: Philharmonia Hall of Saint Petersburg, Tchaikovsky Conservatory of Moscow,
                                       Mozarteum of Salzburg, Philharmonie Essen, International Piano Festival of Warsaw,
                                       Sarajevo Winter Festival, Cambridge University, Leicester University, Columbia University
                                       in New York, Denver University in Colorado, and the Conservatory “G. Verdi” in Milan.
                                    </p>                  
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div>
                           
                           
                           <p><a href="http://www.attesti.com/index.php?cID=140">Learn more about Francesco Attesti by clicking on this link. </a></p>
                           
                        </div>
                        
                        <h4>Additional Events: </h4>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h4><em>Special talk with Francesco Attesti </em></h4>
                                    
                                    <p><em>join us for a special discussion about classical music </em></p>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Wednesday, February 8 </strong><strong></strong></p>
                                    
                                    <p>10:00am East Campus</p>
                                    
                                    <p>Performing Arts Center </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><img alt="Attesti Masterclass photograph" height="224" src="../../../../locations/east/humanities/speaker-series/Attestimasterclass.jpg" width="500"></p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Book Talk and Signing with author Will Schwalbe</p>
                        
                        <p>From the author of the beloved <em>New York Times</em> best-selling <em>The End of Your Life Book Club,</em> an inspiring and magical exploration of the power of books to shape our lives in
                           an era of constant connectivity.
                        </p>
                        
                        <p>We invite all fellow book loversto join us for a book talk and signing with the author,
                           Will Schwalbe.
                        </p>
                        
                        <p><a href="https://www.eventbrite.com/e/books-for-living-talk-and-signing-with-author-will-schwalbe-tickets-31602809855">Reserve your ticket today on Eventbrite.com </a></p>
                        
                        <p><img alt="Information for Will Shcwalbe event" height="400" src="../../../../locations/east/humanities/speaker-series/Schwalbepostcards.jpg" width="741">
                           
                        </p>
                        
                        <p>Sponsored by Student Development</p>
                        
                        <p> In collaboration with Writer's Block Bookstore. </p>
                        
                        <p><a href="http://www.writersblockbookstore.com/"><img alt="Writer's block bookstore" border="0" height="240" src="../../../../locations/east/humanities/speaker-series/Writersblock.jpg" width="300"></a> 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/speaker-series/events.pcf">©</a>
      </div>
   </body>
</html>