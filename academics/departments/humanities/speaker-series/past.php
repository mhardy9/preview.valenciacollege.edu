<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Speaker Series | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/humanities/speaker-series/past.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/humanities/speaker-series/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Speaker Series</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/humanities/">Humanities</a></li>
               <li><a href="/academics/departments/humanities/speaker-series/">Speaker Series</a></li>
               <li>Speaker Series</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Past Presentations</h2>
                        
                        <h2>The Humanities Speakers Series has had the honor of bringing some of the most exciting
                           and innovative writers, musicians, scientists, and activists in the country.
                        </h2>
                        
                        <p><a href="templegrandin.html">Temple Grandin </a></p>
                        
                        <p><a href="http://livestream.com/accounts/4834874/events/4423020/videos/103795111">This link will take you to Valencia Live's Archives to view Dr. Grandin's Keynote
                              Presentation. </a></p>
                        
                        <p><img alt="Dr. Grandin speaking" height="225" src="../../../../locations/east/humanities/speaker-series/DSCN0970_000.JPG" width="300"> <img alt="book signing" height="225" src="../../../../locations/east/humanities/speaker-series/DSCN1018.JPG" width="300"> <img alt="PTK and Temple Grandin" height="225" src="../../../../locations/east/humanities/speaker-series/DSCN1013.JPG" width="300"></p>
                        
                        <p><a href="phoenixjazzorchestra.html">Phoenix Jazz Orchestra </a></p>
                        
                        <p><img alt="A Night of Jazz" height="250" src="../../../../locations/east/humanities/speaker-series/jazznight_000.jpg" width="300"></p>
                        
                        <p>Francesco Attesti </p>
                        
                        <p> <a href="http://livestream.com/accounts/4834874/events/3760598/videos/76020841">This link will take you to Valencia Live's Archives to view Francesco Attesti's performance.</a> 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><img alt="Photo of Francesco Attesti" height="361" src="../../../../locations/east/humanities/speaker-series/foto3.jpg" width="289"></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p>Master pianist Francesco Attesti spent two days at Valencia in a program that was
                                          a collaboration with the Humanities Speakers Series, the Osceola Campus, and the East
                                          Campus Music Department.
                                       </p>
                                       
                                       <p>Music students attended a master class in which Attesti provided feedback to students,
                                          gave career advice, and shared his experiences.
                                       </p>
                                       
                                       <p>Humanities Students attended the second master class which featured Attesti explaining
                                          the development of Western music while illustrating key concepts by playing for students.
                                          
                                       </p>
                                       
                                       <p>Attesti performed works by Schubert, Chopin, Brahms, as well as contemporary composers.</p>
                                       
                                       <p>To learn more about the work of these composers, please click on the links below:</p>
                                       
                                       <p> <a href="http://preview.valenciacollege.edu/east/humanities/speaker-series/documents/DezsoBarthaSchubertImpromptusver2.docx">Franz. Schubert (1797 - 1828)</a></p>
                                       
                                       <p> <a href="http://preview.valenciacollege.edu/east/humanities/speaker-series/documents/FredericChopin.docx">Frédéric Chopin (1810 - 1849) </a></p>
                                       
                                       <p> <a href="http://preview.valenciacollege.edu/east/humanities/speaker-series/documents/JohannesBrahms.docx">Johannes Brahms (1833 - 1896)</a></p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>To see photos and videos of our events, please visit our Facebook page: <a href="mailto:https://www.facebook.com/pages/Valencia-College-Humanities-Speakers-Series/107927826070833?ref=hl">Valencia College Humanities Speakers Series </a></p>
                        
                        
                        <div>
                           
                           <p>Dr. Michael Shermer </p>
                           
                           <p><a href="http://livestream.com/accounts/4834874/events/3521589/videos/66368649">See a recording from Dr. Shermer's performance on Valencia Live's Archives</a></p>
                           
                           <p>During the last week in October, 2014, Dr. Michael Shermer joined the Valencia community
                              for a scholar-in-residence program. Here are some of the highlights of his presentation:
                           </p>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><img alt="Dr. Shermer" height="250" src="../../../../locations/east/humanities/speaker-series/DSCN0180.JPG" width="284"></div>
                                    
                                    <div><img alt="Primary thinking errors" height="250" src="../../../../locations/east/humanities/speaker-series/DSCN0153.JPG" width="260"></div>
                                    
                                    <div><img alt="Shermer Conclusion" height="250" src="../../../../locations/east/humanities/speaker-series/DSCN0199.JPG" width="331"></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><img alt="Students with Shermer books" height="250" src="../../../../locations/east/humanities/speaker-series/DSCN0203.JPG" width="333"></div>
                                    </div>
                                    
                                    <div>
                                       <div><img alt="Shermer discussion" height="250" src="../../../../locations/east/humanities/speaker-series/DSCN0307.JPG" width="187"></div>
                                    </div>
                                    
                                    <div>
                                       <div><img alt="Humanities Speakers Series" height="250" src="../../../../locations/east/humanities/speaker-series/DSCN0214_000.JPG" width="174"></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        <div>
                           
                           <p>Award-winning Writer, Musician, and Educator, James McBride </p>
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><img alt="Image of James McBride" height="283" src="../../../../locations/east/humanities/speaker-series/Picture1_000.jpg" width="300"></div>
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>              
                        
                        <p>Renaissance man and bestselling author James McBride presented the powerful story
                           of John Brown, a white American abolitionist who in 1859--along with 19 others--attacked
                           the largest arsenal of weapons in America, ultimately prompting the Civil War by terrorizing
                           the South and galvanizing the abolitionists in the North. 
                        </p>
                        
                        
                        <p>The presentation had the audience as McBride not only read excerpts from his award-winning
                           novel, but also performed  spiritual music from the time to make history come alive.
                        </p>
                        
                        <p> To see a short video of McBride describing his presentation, follow <a href="http://www.apbspeakers.com/speaker/james-mcbride">THIS LINK</a> and click on "Videos" then on "The Good Lord Bird." 
                        </p>
                        
                        <div>
                           
                           <h2>David Pogue </h2>
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><img alt="David Pogue" height="235" src="../../../../locations/east/humanities/speaker-series/Picture2.jpg" width="244"></div>
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>              
                           
                           <p>Edgy and hilarious science expert David Pogue wowed us with a presentation about the
                              blazing-fast torrent of new inventions. He predict ed which will actually cause major,
                              disruptive changes. 
                           </p>
                           
                           <p>Musical Events/Presentations</p>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div><img alt="A Night of Jazz" height="312" src="../../../../locations/east/humanities/speaker-series/Jazz.jpg" width="240"></div>
                                       
                                       <div>
                                          
                                          <p>The musicians  discussed the great musicians of the past, honoring them by playing
                                             their music, and at the same time will look forward by played new synthesis of exciting
                                             music.
                                          </p>
                                          
                                          <p>Wednesday, September 17 at 7:30pm</p>
                                          
                                          <p><a href="http://www.phoenixjazzorch.com/whoweare.html">The Phoenix Jazz Orchestra</a></p>
                                          
                                          <p>Learn more about them on their website.</p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div><img alt="Performance of Mozart's Requiem" height="250" src="../../../../locations/east/humanities/speaker-series/Requiem1.jpeg" width="333"></div>
                                       
                                       <div>
                                          
                                          <h2> Mozart's Requiem </h2>
                                          
                                          <h3>&nbsp;</h3>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           <p>Mozart’s Requiem was a rich discussion about the mysteries that surround Mozart’s
                              final work, “Requiem”, before his untimely death. The lecture was hosted by Humanities
                              Professor Dr. Matthew McAllister, who holds a PhD in historical musicology, and was
                              accompanied by professional and student musicians from the Orlando Philharmonic, Central
                              Florida Southern Winds, Orlando Gay Chorus, and the Valencia College East Campus Music
                              Department. 
                           </p>
                           
                           <p>Dr. McAllister deconstructed central themes throughout the piece and took an in depth
                              look at questions such as how much of this work did Mozart actually compose? Did Mozart
                              believe he was composing this work for his own funeral? Who actually finished this
                              composition after Mozart's untimely death?
                           </p>              
                           
                           
                           <p>Stay informed with our up-coming events. </p>
                           
                           
                           <p><a href="https://www.facebook.com/pages/Valencia-College-Humanities-Speakers-Series/107927826070833?ref=hl">We invite you to visit our page on Facebook</a>.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/humanities/speaker-series/past.pcf">©</a>
      </div>
   </body>
</html>