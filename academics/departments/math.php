<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           
                           <header><a name="top" id="top"></a>
                              
                              <div>
                                 <a href="math.html#navigate">Skip to Local Navigation</a> | <a href="math.html#content">Skip to Content</a>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          
                                          <a aria-label="Valencia College" href="../index.html"><img alt="Valencia College" height="42" role="img" src="west/valencia-college-logo.svg" width="275"></a>
                                          
                                          
                                       </div>
                                       
                                       <div> 
                                          <a href="https://atlas.valenciacollege.edu/" title="Atlas Login"> <span><span>A</span>T<span>LAS</span>&nbsp;<span>L</span><span>OGIN</span></span></a>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div aria-label="Sitewide Navigation" role="navigation">
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><a href="http://preview.valenciacollege.edu/future-students/">Future Students</a></li>
                                          
                                          <li>
                                             
                                          </li>
                                          <li><a href="../students/index.html">Current Students</a></li>
                                          
                                          <li>
                                             
                                          </li>
                                          <li><a href="../faculty.html">Faculty &amp; Staff</a></li>
                                          
                                          <li>
                                             
                                          </li>
                                          <li><a href="../visitors.html">Visitors &amp; Friends</a></li>
                                          
                                          <li>
                                             
                                          </li>
                                          <li>
                                             
                                             <div>
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="../includes/quicklinks.html">Quick Links</a>
                                                      
                                                      <ul>
                                                         
                                                         <li><a href="../calendar/index.html">Academic Calendar</a></li>
                                                         
                                                         <li><a href="../students/admissions-records/index.html">Admissions</a></li>
                                                         
                                                         <li><a href="http://net4.valenciacollege.edu/promos/internal/blackboard.cfm">Blackboard Learn</a></li>
                                                         
                                                         <li><a href="http://www.valenciabookstores.com/valencc/">Bookstore</a></li>
                                                         
                                                         <li><a href="../business-office/index.html">Business Office</a></li>
                                                         
                                                         <li><a href="../calendar/index.html">Calendars</a></li>
                                                         
                                                         <li><a href="../map/index.html">Campuses</a></li>
                                                         
                                                         <li><a href="../students/catalog/index.html">Catalog</a></li>
                                                         
                                                         <li><a href="http://preview.valenciacollege.edu/schedule/">Class Schedule</a></li>
                                                         
                                                         <li><a href="http://preview.valenciacollege.edu/continuing-education/">Continuing Education</a></li>
                                                         
                                                         <li><a href="../academics/programs/index.html">Degree &amp; Career Programs</a></li>
                                                         
                                                         <li><a href="index.html">Departments</a></li>
                                                         
                                                         <li><a href="http://preview.valenciacollege.edu/economicdevelopment/">Economic Development</a></li>
                                                         
                                                         <li><a href="../human-resources/index.html">Employment</a></li>
                                                         
                                                         <li><a href="../finaid/index.html">Financial Aid Services</a></li>
                                                         
                                                         <li><a href="../international/index.html">International</a></li>
                                                         
                                                         <li><a href="../library/index.html">Libraries</a></li>
                                                         
                                                         <li><a href="../map/index.html">Locations</a></li>
                                                         
                                                         <li><a href="../contact/directory.html">Phone Directory</a></li>
                                                         
                                                         <li><a href="../students/admissions-records/index.html">Records/Transcripts</a></li>
                                                         
                                                         <li><a href="../security/index.html">Security/Parking</a></li>
                                                         
                                                         <li><a href="../support/index.html">Support</a></li>
                                                         
                                                         <li><a href="http://preview.valenciacollege.edu/events/">Valencia Events</a></li>
                                                         
                                                      </ul>
                                                      
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                          </li>
                                          <li>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <form action="https://search.valenciacollege.edu/search" label="search" method="get" name="seek" id="seek">
                                                      <label for="q"><span>Search</span></label>
                                                      <input alt="Search" aria-label="Search" name="q" type="text" value="">
                                                      <button name="submitMe" onblur="clearTextSubmitOut(document.seek.q)" onfocus="clearTextSubmit(document.seek.q)" onmouseout="clearTextSubmitOut(document.seek.q)" onmouseover="clearTextSubmit(document.seek.q)" type="submit" value="seek">go</button>
                                                      <input name="site" type="hidden" value="default_collection">
                                                      <input name="client" type="hidden" value="default_frontend">
                                                      <input name="proxystylesheet" type="hidden" value="default_frontend">
                                                      <input name="output" type="hidden" value="xml_no_dtd">
                                                      
                                                   </form>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                           </header>
                           
                        </div>
                        
                     </div>
                     
                     <div>  
                        
                        <div>
                           
                           <blockquote>
                              
                              
                              <h2>This Web site has moved. </h2>
                              
                              <h2>Please update your bookmarks.</h2>
                              
                              <h2><a href="../west/math">http://preview.valenciacollege.edu/west/math</a></h2>
                              
                           </blockquote>
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math.pcf">©</a>
      </div>
   </body>
</html>