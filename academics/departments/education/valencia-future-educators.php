<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/education/valencia-future-educators.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/education/">Education</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Valencia Future Educators Club (VFE)</h2>
                        
                        <p>All TEP students are encouraged to join Valencia Future Educators Club to benefit
                           from  the programming and skillshops relevant to future teachers. VFE is active only
                           on East Campus currently  and hosts skillshops each Tuesday from 1:00-2:15pm during
                           the Activity Hour (room assigned at the beginning of each semester). Skillshop topics:
                           resume writing, General  Knowledge Test prep, Technology in Education, Interview tips,
                           cyberbullying and more. Watch this video to see why VFE was awarded 2014 Organization
                           of the Year by Student Development: <a href="https://www.youtube.com/watch?v=m6DwzMuOMMc">https://www.youtube.com/watch?v=m6DwzMuOMMc</a></p>
                        
                        <p><strong>To Join VFE</strong>: go to our Facebook page and click on  the sign up button in our banner pic <a href="http://www.facebook.com/ValenciaFutureEducatorsEastCampus">www.facebook.com/ValenciaFutureEducatorsEastCampus</a></p>
                        
                        <p><strong>Contact us:</strong><br>
                           VFE email: <a href="mailto:valenciafutureeducatorsec@gmail.com">valenciafutureeducatorsec@gmail.com</a> <br>
                           Advisors:  Professor Lauri Lott <a href="mailto:LLott1@valenciacollege.edu">LLott1@valenciacollege.edu</a> <br>
                           Dr. Yasmeen  Qadri <a href="mailto:yqadri@valenciacollege.edu">yqadri@valenciacollege.edu</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/education/valencia-future-educators.pcf">©</a>
      </div>
   </body>
</html>