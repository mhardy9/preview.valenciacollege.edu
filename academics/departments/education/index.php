<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Education  | Valencia College</title>
      <meta name="Keywords" content="engineering, technology, transfer, university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/education/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="main-title">
                           
                           <h2>Education</h2>
                           
                           <p>Associate in Science (A.S.)</p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        <p>Valencia’s Education transfer plans and the Educator Preparation Institute will give
                           you the foundation to become a teacher in Florida. You’ll become familiar with the
                           Florida Educator Accomplished Practices, Sunshine State Standards and the Professional
                           Educator Competencies. Plus, through field-based experiences and internships, you’ll
                           get a chance to see what it’s like in front of the classroom too.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in"> <i class="pe-7s-gym"></i>
                           
                           <h3>Career Coach</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Explore related careers, salaries and job listings.</p>
                           
                           <p><a href="https://valenciacollege.emsicareercoach.com/" target="_blank" onclick="_gaq.push(['_trackEvent', 'CLICK THRU', 'CAREER COACH', document.title. toUpperCase()]);" class="button small">Learn More</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <h2>Programs &amp; Degrees</h2>
                        
                        <ul>
                           
                        </ul>
                        
                        <h3>Associate in Arts Transfer Plan</h3>
                        
                        <p><a href="http://catalog.valenciacollege.edu/transferplans/" target="_blank">Transfer Plans</a> are designed to prepare students to transfer to a Florida public university as a
                           junior.  The courses listed in the plans are the common prerequisites required for
                           the degree. They are placed within the general education requirements and/or the elective
                           credits.  Specific universities may have additional requirements, so it is best check
                           your transfer institution catalog and meet with a Valencia advisor.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/earlychildhoodeducation/?utm_source=direct&amp;utm_medium=vanity&amp;utm_content=15EPI001&amp;utm_campaign=EPI">Early Childhood Education</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/educationgeneralpreparation/">Education (General Preparation)</a></li>
                           
                        </ul>
                        
                        <h3>Certificate</h3>
                        
                        <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                           for job advancement. Most can be completed in one year or less. Credits earned can
                           be applied toward a related A.S. degree program.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/epi/">Educator Preparation Institute (EPI)</a></li>
                           
                        </ul>
                        
                        <h3>Continuing Education</h3>
                        
                        <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                           development, language courses, certifications and custom course development for organizations.
                           Day, evening, weekend and online learning opportunities are available.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/">Continuing Education Programs</a></li>
                           
                        </ul>
                        
                        <h3>Associate in Arts Pre-Major</h3>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/elementaryeducation/">Elementary Education</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="wrapper_indent">
                           
                           <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-action home">Apply Now</a>
                           
                           <a href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm" class="button-action_outline home">Request Information</a>
                           
                           <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/" class="button-action_outline home split">Visit Valencia</a>
                           
                           <a href="https://vcchat.valenciacollege.edu/CCPROChat/CPChatRequest.jsp?form.ServiceID=29&amp;form.CustName=WebCaller&amp;form.SEcureChat=1" class="button-action_outline home split" target="foo" onsubmit="window.open('', 'foo', 'width=450,height=500,status=yes,resizable=yes,scrollbars=yes,location=no')">Chat With Us</a>
                           
                           
                           <p>&nbsp;</p>
                           
                           
                           <h3>Need Help?</h3>
                           
                           <p>For general enrollment questions, assistance is available on a walk-in basis at the
                              <a href="http://preview.valenciacollege.edu/answer-center/" onclick="__gaTracker('send', 'event', 'outbound-widget', 'http://preview.valenciacollege.edu/answer-center/', 'Answer Center');" title="Answer Center" target="_blank">Answer Center</a> or contact Enrollment Services.<br>
                              <i class="far fa-phone"></i> <a href="tel:14075821507" style="margin-left:10px;">407-582-1507</a><br>
                              <i class="far fa-envelope"></i>&nbsp;<a href="mailto:enrollment@valenciacollege.edu?subject=Website%20Inquiry" style="margin-left:10px;">enroll@valenciacollege.edu</a>
                              
                           </p>
                           
                           
                        </div>           
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/education/index.pcf">©</a>
      </div>
   </body>
</html>