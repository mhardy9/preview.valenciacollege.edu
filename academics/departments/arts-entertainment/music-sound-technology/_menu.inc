<div class="header header-site">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-3" role="banner"> <div id="logo">
        <div id="logo"><a href="/index.php"> <img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College Logo" data-retina="true" /></a></div>
      </div>
      <nav class="col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>

        <div class="site-menu">
          <div id="site_menu"><img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College" data-retina="true" /></div>
          <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>

          <ul>
            <li>
<a href="/academics/departments/arts-entertainment/index.php">Arts and Entertainment </a>
            </li>
            <li class="megamenu submenu">
<a href="javascript:void(0);" class="show-submenu-mega">Arts Programs<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
              <div class="menu-wrapper">
                <div class="col-md-4">
                  <h3>Art Studio/Fine Arts</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/index.php">Art
                      Studio</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/annual-student-art-sale.php">Annual
                        Student Art Sale</a>
</li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Artstudio/contact.cfm" target="_blank">Contact</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Dance</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/index.php">Dance</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/auditions.php">Auditions</a>
                    </li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/events-tickets.php">Events
                      Tickets</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/faqs.php">Frequently Ask
                      Questions</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/dance-degree.php">Dance
                      Degree</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/non-majors-programs.php">Non
                      major Programs</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/valencia-summer-dance-institute.php">Valencia
                        Summer Dance Institute</a>
</li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/valencia-dance-theatre.php">Valencia
                        Dance Theatre</a>
</li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/performance-schedule.php">Performance
                      Schedule</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Graphic Technology</h3>
                  <ul>
                    <li><a href="/academics/departments/arts-entertainment/graphics-technology/index.php">Graphics
                      Technology</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/graphics-technology/degrees.php">Degrees</a>
                    </li>
                    <li>
<a href="/academics/departments/arts-entertainment/graphics-technology/events.php">Events</a>
                    </li>
                    <li>
<a href="/academics/departments/arts-entertainment/graphics-technology/labs.php">Labs</a>
                    </li>
                    <li><a href="/academics/departments/arts-entertainment/graphics-technology/faqs.php">Frequently
                      Ask Questions</a></li>
                    <li>
                      <a href="/academics/departments/arts-entertainment/graphics-technology/student-work.php">Student
                        Work</a>
</li>
                  </ul>

                  <h3>Theatre</h3>
                  <ul>
                    <li>
<a href="/academics/departments/arts-entertainment/theater/index.php">Theater</a>
                    </li>
                    <li><a href="/academics/departments/arts-entertainment/theater/theater-schedule.php">Theater
                      Schedule</a></li>

                  </ul>
                </div>

              </div>
            </li>

            <li class="megamenu submenu">
              <a href="javascript:void(0);" class="show-submenu-mega">Entertainment Programs<i class="far fa-angle-down" aria-hidden="true"></i></a>
              <div class="menu-wrapper">
                <div class="col-md-4">
                  <h3>Digital Media</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/index.php">Digital
                      Media</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/degrees-cetificates.php">Degrees
                        and Certificates</a>
</li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/courses.php">Courses</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/equipment-information.php">Equipment
                        Information</a>
</li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/faculty-staff.php">Faculty
                        and Staff</a>
</li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/facilities.php">Facilities</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/video.php">Video</a>
                    </li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/resources.php">Resources</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Entertainment Design Technology</h3>
                  <ul>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/entertainment-design-technology/index.php">Entertainment</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/entertainment-design-technology/faculty-staff.php">Faculty
                        and Staff</a>
</li>
                    <li><a href="http://preview.valenciacollege.edu/artsandentertainment/Entertainmentdesignandtech/contact.cfm" target="_blank">Contact</a></li>
                  </ul>

                  <h3>Film</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/film-technology/index.php">Film
                      Technology</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Filmtech/contact.cfm" target="_blank">Contact</a></li>
                  </ul>

                </div>
                <div class="col-md-4">
                  <h3>Music</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/index.php">Music</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/faculty-staff.php">Faculty
                      and Staff</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/music/music-and-sound-technology-program.php">Music
                        and Sound Technology Program</a>
</li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/voices-of-valencia.php">Voices
                      of Valencia</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Music/contact.cfm" target="_blank">Contact</a></li>
                  </ul>
                  <h3>Music and Sound Technology </h3>
                  <ul>
                    <li><a href="index.php">Music and Sound Technology</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li><a href="/academics/departments/arts-entertainment/camps.php">Camps</a></li>
            <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/anita-s-wooten-gallery.php">Anita
              S. Wooten Gallery</a></li>
          </ul>
           

        </div>
         
      </nav>
    </div>
  </div>
   
</div>
 

 

 
<div class="sub-header bg-1">
  <div id="intro-txt">
    <h1>Arts and Entertainment</h1>
    <p>Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp; entertainment degree can
      help you turn what you love into what you do for a living.</p>
  </div>
</div>
 

 

 
<div>
  <div id="route" role="navigation" aria-label="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/">Valencia College</a></li>
        <li><a href="/LOCATIONS">Locations</a></li>
        <li><a href="/academics/departments/arts-entertainment/index.php">Arts and Entertainment</a></li>
        <li>Music and Sound Technology</li>
      </ul>
    </div>
  </div>
   
   
     
<main role="main">
  <div class="container margin-60">
    <div class="row">
      <div class="col-md-9">
        <div class="box_style_1">
          <div class="indent_title_in">
            <h3>Music and Sound Technology</h3>
            <p> From theatre to soundstage, studio and post-production</p>
          </div>
          <div class="wrapper_indent">
            <h4></h4>
            <p>Valencia's Music and Sound Technology program takes a unique approach to preparing you for an exciting
              career in the music and sound industry. You will have the opportunity to study studio and live sound
              techniques, post-production and MIDI, as well as, musicianship and performance. This will ensure that you
              develop the technical skills and aesthetic judgement to handle both the artistic and technical demands of
              music and sound production. This practical approach gives you real world experience in a wide range of
              settings. From theatre to soundstage, studio and post-production, and with Valencia's own completely
              student-run record company, "FlatFoot Music:, you will have many opportunities to develop, practice,
              and refine your craft on your way to a rewarding high wage career.</p>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p></p>
              <h4></h4>
            </div>
          </div>
          <hr class="styled_2" />
          <div class="indent_title_in">
            <h4>This program offers specializations in:</h4>
            <p>
            </p>
<ul class="list_style_1">
              <li>Sound Technology</li>
              <li>Music Performance and Sound</li>
            </ul>
            

            <h4>Primary Occupations:</h4>
            <ul class="list_style_1">
              <li>Music Producer/ Director</li>
              <li>Sound Mixer/ Designer</li>
              <li>Post-production Mixer/Editor</li>
              <li>Assistant Audio Engineer</li>
              <li>Sound Recordist</li>
              <li>Sound Effects/ Dialog Editor</li>
              <li>Surround Sound Mixer/Author</li>
              <li>Sound Designer</li>
            </ul>

          </div>
          <div class="wrapper_indent">
            <p></p>
            <div class="row">
              <div class="col-md-12">
                <h4>Contact:</h4>

                <p>Program Director<br /> <a href="mailto:rvalery@valenciacollege.edu">Raul Valery </a><br /> (407)
                  582-2882</p>

                <p>Career Program Advisor <br /> <a href="mailto:kbell@valenciacollege.edu">Kristol Bell</a><br /> (407)
                  582-2097</p>

                <p>Dean, Arts &amp; Entertainment<br /> <a href="mailto:wgivoglu@valenciacollege.edu">Wendy
                  Givoglu</a><br /> (407) 582-2340</p>
</div>
            </div>
          </div>

        </div>
      </div>

      <aside class="col-md-3">
        <div class="banner">
<i class=" iconcustom-school"></i>

          <h3>Rock and Roll Camp 2017</h3>
          <p>June 5 - June 9, 2017<br /> July 24 - July 28, 2017<br /> Rock and Roll Camp is an intensive musical
            experience for students in middle and high school.
          </p>
          <a href="../../../../locations/east/departments/arts-entertainment/music-sound-technology/camp.php" class="banner_bt">Registration Form</a>
</div>

        <hr class="styled" />

        <div class="box_side">
          <h5>By Phone</h5>
          <i class="icon-phone"></i>
          <p><a href="tel://4077617730">407 761 7730</a></p>
        </div>
        <hr class="styled" />
        <div class="box_side">
          <h5>Performing Arts Center</h5>
          <i class="icon_pencil-edit"></i> <br /><em>Valencia College, East Campus <br /> 701 N. Econlockhatchee Tr<br />
          Orlando, Fl 32825</em>
        </div>

        <hr class="styled" />
        
        <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
<span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
          <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
</div>
        
        <hr class="styled" />

    </aside>
</div>
    
  </div>
   
</main>
</div>
 
</div>
