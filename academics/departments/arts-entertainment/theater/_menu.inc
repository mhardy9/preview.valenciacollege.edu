<div class="header header-site">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-3" role="banner"> <div id="logo">
        <div id="logo"><a href="/index.php"> <img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College Logo" data-retina="true" /></a></div>
      </div>
      <nav class="col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>

        <div class="site-menu">
          <div id="site_menu"><img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College" data-retina="true" /></div>
          <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>

          <ul>
            <li>
<a href="/academics/departments/arts-entertainment/index.php">Arts and Entertainment </a>
            </li>
            <li class="megamenu submenu">
<a href="javascript:void(0);" class="show-submenu-mega">Arts Programs<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
              <div class="menu-wrapper">
                <div class="col-md-4">
                  <h3>Art Studio/Fine Arts</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/index.php">Art
                      Studio</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/faqs.php">Frequently
                      Ask Questions</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/annual-student-art-sale.php">Annual
                        Student Art Sale</a>
</li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Artstudio/contact.cfm" target="_blank">Contact</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Dance</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/index.php">Dance</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/auditions.php">Auditions</a>
                    </li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/events-tickets.php">Events
                      Tickets</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/faqs.php">Frequently Ask
                      Questions</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/dance-degree.php">Dance
                      Degree</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/non-majors-programs.php">Non
                      major Programs</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/valencia-summer-dance-institute.php">Valencia
                        Summer Dance Institute</a>
</li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/valencia-dance-theatre.php">Valencia
                        Dance Theatre</a>
</li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/performance-schedule.php">Performance
                      Schedule</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Graphic Technology</h3>
                  <ul>
                    <li><a href="/academics/departments/arts-entertainment/graphics-technology/index.php">Graphics
                      Technology</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/graphics-technology/degrees.php">Degrees</a>
                    </li>
                    <li>
<a href="/academics/departments/arts-entertainment/graphics-technology/events.php">Events</a>
                    </li>
                    <li>
<a href="/academics/departments/arts-entertainment/graphics-technology/labs.php">Labs</a>
                    </li>
                    <li><a href="/academics/departments/arts-entertainment/graphics-technology/faqs.php">Frequently
                      Ask Questions</a></li>
                    <li>
                      <a href="/academics/departments/arts-entertainment/graphics-technology/student-work.php">Student
                        Work</a>
</li>
                  </ul>

                  <h3>Theatre</h3>
                  <ul>
                    <li><a href="index.php">Theater</a></li>
                    <li><a href="theater-schedule.php">Theater Schedule</a></li>
                  </ul>
                </div>

              </div>
            </li>

            <li class="megamenu submenu">
              <a href="javascript:void(0);" class="show-submenu-mega">Entertainment Programs<i class="far fa-angle-down" aria-hidden="true"></i></a>
              <div class="menu-wrapper">
                <div class="col-md-4">
                  <h3>Digital Media</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/index.php">Digital
                      Media</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/degrees-cetificates.php">Degrees
                        and Certificates</a>
</li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/courses.php">Courses</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/equipment-information.php">Equipment
                        Information</a>
</li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/faculty-staff.php">Faculty
                        and Staff</a>
</li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/facilities.php">Facilities</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/video.php">Video</a>
                    </li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/resources.php">Resources</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Entertainment Design Technology</h3>
                  <ul>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/entertainment-design-technology/index.php">Entertainment</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/entertainment-design-technology/faculty-staff.php">Faculty
                        and Staff</a>
</li>
                    <li><a href="http://preview.valenciacollege.edu/artsandentertainment/Entertainmentdesignandtech/contact.cfm" target="_blank">Contact</a></li>
                  </ul>

                  <h3>Film</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/film-technology/index.php">Film
                      Technology</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Filmtech/contact.cfm" target="_blank">Contact</a></li>
                  </ul>

                </div>
                <div class="col-md-4">
                  <h3>Music</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/index.php">Music</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/faculty-staff.php">Faculty
                      and Staff</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/music/music-and-sound-technology-program.php">Music
                        and Sound Technology Program</a>
</li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/voices-of-valencia.php">Voices
                      of Valencia</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Music/contact.cfm" target="_blank">Contact</a></li>
                  </ul>
                  <h3>Music and Sound Technology </h3>
                  <ul>
                    <li>
                      <a href="/academics/departments/arts-entertainment/music-sound-technology/index.php">Music
                        and Sound Technology</a>
</li>
                  </ul>
                </div>
              </div>
            </li>
            <li><a href="/academics/departments/arts-entertainment/camps.php">Camps</a></li>
            <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/anita-s-wooten-gallery.php">Anita
              S. Wooten Gallery</a></li>
          </ul>
           

        </div>
         
      </nav>
    </div>
  </div>
   
</div>
 

 

 
<div class="sub-header bg-1">
  <div id="intro-txt">
    <h1>Arts and Entertainment</h1>
    <p>Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp; entertainment degree can
      help you turn what you love into what you do for a living.</p>
  </div>
</div>
 

 

 
<div>
  <div id="route" role="navigation" aria-label="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/">Valencia College</a></li>
        <li><a href="/LOCATIONS">Locations</a></li>
        <li><a href="/academics/departments/arts-entertainment/index.php">Arts and Entertainment</a></li>
        <li>Theater</li>
      </ul>
    </div>
  </div>
   
   
     
<main role="main">
  <div class="container margin-60">
    <div class="row">
      <div class="col-md-9">
        <div class="box_style_1">
          <div class="indent_title_in">
            <h3>Theater</h3>
            <p>Recent productions have included major musicals, avant-garde plays, classical drama and comedy,
              contemporary Broadway hits, and favorite revivals</p>
          </div>
          <div class="wrapper_indent">
            <h4>Welcome</h4>
            <p>Valencia College Theater produces four major productions each year, as well as a series of
              student-directed one-act plays. Productions are held either in our 558-seat Performing Arts Center, or our
              more intimate Black Box Theater, both located on the East Campus. </p>
            <p>The season runs from October through July each school year, with variety and educational opportunities
              for both students and audiences driving the selection of plays. Recent productions have included major
              musicals, avant-garde plays, classical drama and comedy, contemporary Broadway hits, and favorite
              revivals. Acting and directing students also produce two sets of student-directed one-act plays that are
              offered on a designated weekend in April. (The Student-Directed One Act Festival is held at the Lowndes
              Shakespeare Center, Loch Haven Park, Orlando and is free). </p>
            <p>&nbsp;</p>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p></p>
              <h4></h4>
            </div>
          </div>
          <hr class="styled_2" />
          <div class="indent_title_in">
            <h3>Ticket Information</h3>
            <p></p>
          </div>
          <div class="wrapper_indent">
            <p></p>
            <div class="row">
              <div class="col-md-12">
                <p>Tickets - please visit <a href="http://preview.valenciacollege.edu/arts" target="_blank">valenciacollege.edu/arts</a>
                  for further information about ticket prices, discounts, and purchase tickets for theater events.<br />
                  Box Office: 407-582-2900
                </p>
              </div>
            </div>
          </div>

          <hr class="styled_2" />
          <div class="indent_title_in">
            <h2>Faculty</h2>
            <p>Full-time and Adjunct Theater Faculty</p>
          </div>
          <div class="wrapper_indent">
            <p></p>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <ul class="list_teachers">
                  <h4>Full-time Faculty</h4>
                  <li>
<h5>John DiDonna</h5>
                    <p> Program Chair/Artistic Director, Theater </p>
</li>
                  <li>
<h5>Kristin Abel </h5>
                    <p>Theater Technology</p>
</li>
                  <li>
<h5>Ginny Kopf</h5>
                    <p> (Voice for the Actor, Movement, Stage Dialects, Acting)</p>
</li>
                  <li>
<h5>Ann LaPietra</h5>
                    <p> Audio/Visual (PAC Manager) </p>
</li>
                  <li>
<h5>Greg Loftus</h5>
                    <p> Theater Technology</p>
</li>
                  <li>
<h5>Sonia Pasqual </h5>
                    <p>Theater Technology </p>
</li>
                  <li>
<h5>David 'Ross' Rauschkolb</h5>
                    <p> Technical Director, Theater</p>
</li>
                  <li>
<h5>Heather Sladick</h5>
                    <p> Theater Technology (PAC Tech Spvr) </p>
</li>

                </ul>
              </div>
              <div class="col-md-6 col-sm-6">
                <ul class="list_teachers">
                  <h4>Adjunct Faculty</h4>
                  <li>
<h5>Jehad Choate</h5>
                    <p></p>
</li>
                  <li>
<h5>Eric Craft </h5>
                    <p></p>
</li>
                  <li>
<h5>Timothy DeBaun</h5>
                    <p></p>
</li>
                  <li>
<h5>Rebekah Lane </h5>
                    <p></p>
</li>
                  <li>
<h5>Kathleen Lindsey-Moulds</h5>
                    <p></p>
</li>
                  <li>
<h5>Virginia McKinney</h5>
                    <p></p>
</li>
                  <li>
<h5>Donald Rupe </h5>
                    <p></p>
</li>
                  <li>
<h5>Michael Shugg </h5>
                    <p></p>
</li>
                  <li>
<h5>Aradhana Tiwari </h5>
                    <p></p>
</li>
                  <li>
<h5>Jonathan Whiteley</h5>
                    <p></p>
</li>
                  <li>
<h5>Timothy Williams </h5>
                    <p></p>
</li>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <aside class="col-md-3">
        <div class="banner">
<i class=" iconcustom-school"></i>

          <h3>Rock and Roll Camp 2017</h3>
          <p>June 5 - June 9, 2017<br /> July 24 - July 28, 2017<br /> Rock and Roll Camp is an intensive musical
            experience for students in middle and high school.
          </p>
          <a href="../../../../locations/east/departments/arts-entertainment/theater/camp.php" class="banner_bt">Registration Form</a>
</div>

        <hr class="styled" />

        <div class="box_side">
          <h5>By Phone</h5>
          <i class="icon-phone"></i>
          <p><a href="tel://4077617730">407 761 7730</a></p>
        </div>
        <hr class="styled" />
        <div class="box_side">
          <h5>Performing Arts Center</h5>
          <i class="icon_pencil-edit"></i> <br /><em>Valencia College, East Campus <br /> 701 N. Econlockhatchee Tr<br />
          Orlando, Fl 32825</em>
        </div>

        <hr class="styled" />
        
        <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
<span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
          <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
</div>
        
        <hr class="styled" />

    </aside>
</div>
    
  </div>
   
</main>
</div>
 
</div>
