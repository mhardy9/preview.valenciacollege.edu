<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Theater Schedule  | Valencia College</title>
      <meta name="Description" content="Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp;amp; entertainment degree can help you turn what you love into what you do for a living">
      <meta name="Keywords" content="arts and entertainment, art studio, dance, digital media, entertainment, graphics technology, music, theatre, valencia arts, fine arts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/theater/theater-schedule.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/theater/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/departments/arts-entertainment/theater/">Theater</a></li>
               <li>Theater Schedule </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list"><a href="http://events.valenciacollege.edu/event/next_to_normal" target="_blank"> <img src="http://images-cf.localist.com/photos/458195/big/4a73d4c2004a758e37ee5f4272a0110f9c415e29.jpg" alt="Next To Normal">
                                    
                                    
                                    <div class="short_info">
                                       
                                       <h3>Next To Normal</h3>
                                       
                                    </div>
                                    </a></div>
                              
                           </div>
                           
                           
                           <div class="clearfix visible-xs-block"></div>
                           
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3><strong>Next To Normal</strong></h3>
                                 
                                 <h4>June 09 at 7:30PM </h4>
                                 
                                 <p>Winner of the 2010 Pulitzer Prize for Drama, this rock musical chronicles the life
                                    of Diana, a
                                    suburban mother with bipolar disorder. Nominated for 11 Tony Awards and winner...
                                 </p>
                                 <span>Subscribe</span> <span class="event-subscribe" id="event_subscribe">
                                    <a href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;dates=20170609T233000Z%2F20170610T013000Z&amp;details=Winner+of+the+2010+Pulitzer+Prize+for+Drama%2C+this+rock+musical+chronicles+the+life+of+Diana%2C+a+suburban+mother+with+bipolar+disorder.++Nominated+for+11+Tony+Awards+and+winner+of+three%2C+including+Best+Musical+Score%2C+Next+To+Normal+explores+mental+illness+from+the+points+of+view+of+all+who+are+affected%2C+including+Diana%E2%80%99s+husband+and+daughter.++Music+by+Tom+Kitt.+Book+and+lyrics+by+Brian+Yorkey.+%0A%0ADirected+by+Julia+Gagne.%0A%0ANOTE%3A++This+play+depicts+mental+illness+and+may+not+be+appropriate+for+children+or+audience+members+who+are+sensitive+to+the+topics+portrayed.+Contains+adult+language+and+adult+themes.%0A%0A%2A+%2A+%2A+Due+to+the+intimate+setting+of+the+Black+Box+Theater%2C+late+seating+cannot+be+accommodated.+We+advise+arriving+at+least-campus+20+minutes+prior+to+show+time.%0A%0Ahttp%3A%2F%2Fevents.valenciacollege.edu%2Fevent%2Fnext_to_normal&amp;location=East+Campus+Black+Box+Theater&amp;sprop=website%3Aevents.valenciacollege.edu&amp;text=Next+To+Normal" class="icon-calendar-2" title="Save to Google Calendar" target="_blank"></a>
                                    <a href="http://events.valenciacollege.edu/event/next_to_normal.ics" class="icon-calendar" title="Save to iCal" target="_blank"></a>
                                    <a href="http://events.valenciacollege.edu/event/next_to_normal.ics" class="icon-calendar-1" title="Save to Outlook" target="_blank"></a>
                                    </span>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div><a href="http://events.valenciacollege.edu/event/next_to_normal" class="button-outline" target="_blank">Details</a></div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p>June 5 - June 9, 2017<br> July 24 - July 28, 2017<br> Rock and Roll Camp is an intensive musical
                           experience for students in middle and high school.
                           
                        </p>
                        <a href="../../../../locations/east/departments/arts-entertainment/theater/camp.html" class="banner_bt">Registration Form</a>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        <p><a href="tel://4077617730">407 761 7730</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Performing Arts Center</h5>
                        <i class="icon_pencil-edit"></i> <br><em>Valencia College, East Campus <br> 701 N. Econlockhatchee Tr<br>
                           Orlando, Fl 32825</em>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
                        <span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
                        <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/theater/theater-schedule.pcf">©</a>
      </div>
   </body>
</html>