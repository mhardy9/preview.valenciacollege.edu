<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Theater  | Valencia College</title>
      <meta name="Description" content="Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp;amp; entertainment degree&amp;amp;#10;&amp;amp;#9;&amp;amp;#9;&amp;amp;#9;can help you turn what you love into what you do for a living">
      <meta name="Keywords" content="theatre, arts and entertainment, art studio, dance, digital media, entertainment, graphics technology, music, theatre, valencia arts, fine arts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/theater/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/theater/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-entertainment/">Arts Entertainment</a></li>
               <li>Theater</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Theater</h3>
                           
                           <p>Recent productions have included major musicals, avant-garde plays, classical drama
                              and comedy,
                              contemporary Broadway hits, and favorite revivals
                           </p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Welcome</h4>
                           
                           <p>Valencia College Theater produces four major productions each year, as well as a series
                              of
                              student-directed one-act plays. Productions are held either in our 558-seat Performing
                              Arts Center, or our
                              more intimate Black Box Theater, both located on the East Campus. 
                           </p>
                           
                           <p>The season runs from October through July each school year, with variety and educational
                              opportunities
                              for both students and audiences driving the selection of plays. Recent productions
                              have included major
                              musicals, avant-garde plays, classical drama and comedy, contemporary Broadway hits,
                              and favorite
                              revivals. Acting and directing students also produce two sets of student-directed
                              one-act plays that are
                              offered on a designated weekend in April. (The Student-Directed One Act Festival is
                              held at the Lowndes
                              Shakespeare Center, Loch Haven Park, Orlando and is free). 
                           </p>
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-12">
                              
                              <p></p>
                              
                              <h4></h4>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Ticket Information</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>Tickets - please visit <a href="http://preview.valenciacollege.edu/arts" target="_blank">valenciacollege.edu/arts</a>
                                    for further information about ticket prices, discounts, and purchase tickets for theater
                                    events.<br>
                                    Box Office: 407-582-2900
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>Faculty</h2>
                           
                           <p>Full-time and Adjunct Theater Faculty</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <h4>Full-time Faculty</h4>
                                    
                                    <li>
                                       
                                       <h5>John DiDonna</h5>
                                       
                                       <p> Program Chair/Artistic Director, Theater </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Kristin Abel </h5>
                                       
                                       <p>Theater Technology</p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Ginny Kopf</h5>
                                       
                                       <p> (Voice for the Actor, Movement, Stage Dialects, Acting)</p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Ann LaPietra</h5>
                                       
                                       <p> Audio/Visual (PAC Manager) </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Greg Loftus</h5>
                                       
                                       <p> Theater Technology</p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Sonia Pasqual </h5>
                                       
                                       <p>Theater Technology </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>David 'Ross' Rauschkolb</h5>
                                       
                                       <p> Technical Director, Theater</p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Heather Sladick</h5>
                                       
                                       <p> Theater Technology (PAC Tech Spvr) </p>
                                       
                                    </li>
                                    
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <h4>Adjunct Faculty</h4>
                                    
                                    <li>
                                       
                                       <h5>Jehad Choate</h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Eric Craft </h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Timothy DeBaun</h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Rebekah Lane </h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Kathleen Lindsey-Moulds</h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Virginia McKinney</h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Donald Rupe </h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Michael Shugg </h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Aradhana Tiwari </h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Jonathan Whiteley</h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>Timothy Williams </h5>
                                       
                                       <p></p>
                                       
                                    </li>
                                    
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p>June 5 - June 9, 2017<br> July 24 - July 28, 2017<br> Rock and Roll Camp is an intensive musical
                           experience for students in middle and high school.
                           
                        </p>
                        <a href="../../../../locations/east/departments/arts-entertainment/theater/camp.html" class="banner_bt">Registration Form</a>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        <p><a href="tel://4077617730">407 761 7730</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Performing Arts Center</h5>
                        <i class="icon_pencil-edit"></i> <br><em>Valencia College, East Campus <br> 701 N. Econlockhatchee Tr<br>
                           Orlando, Fl 32825</em>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
                        <span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
                        <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/theater/index.pcf">©</a>
      </div>
   </body>
</html>