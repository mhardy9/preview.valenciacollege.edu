<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Camps  | Valencia College</title>
      <meta name="Description" content="Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp;amp; entertainment degree&amp;amp;#10;&amp;amp;#9;&amp;amp;#9;&amp;amp;#9;can help you turn what you love into what you do for a living">
      <meta name="Keywords" content="arts and entertainment, art studio, dance, digital media, entertainment, graphics technology, music, theatre, valencia arts, fine arts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/camps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-entertainment/">Arts Entertainment</a></li>
               <li>Camps </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Camps</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Rock and Roll Camp 2017</h4>
                           
                           <strong>GUITAR, DRUMS, VOICE, BASS, KEYS</strong><br> Serving Grades 5-12 (Younger but experienced music
                           students please request information (407)761-7730. The Original Rock and Roll camp
                           in Orlando, serving young
                           Rockers in Orlando since 1999. <strong>WEEK 1</strong>: June 5-June 9, 2017, 9 am to 4 pm<br> <strong>WEEK
                              2</strong>: July 24-July 28, 2017, 9 am to 4 pm
                           <p></p>
                           
                           <p></p>
                           <strong>Performing Arts Center<br> Valencia College, East Campus<br> 701 N. Econlockhatchee Tr,
                              Orlando, Fl 32825</strong>
                           
                           
                           <div align="center">
                              
                              
                              <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
                                 <span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
                                 <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
                                 
                              </div>
                              
                              <br>
                              
                           </div>
                           
                           <p>Rock and Roll Camp is an intensive musical experience for students in middle and high
                              school. Be a part
                              of the entire process of creating, rehearsing and performing a Rock and Roll show.
                              Guitar, Bass, Drums,
                              Voice, Keyboards. Activities include instrumental classes, band rehearsals, clinics,
                              and guest performers.
                              All students perform in the final concert on Friday with lights and sound. Small groups,
                              highly qualified
                              instructors and veteran rock musicians make this camp the original and best Rock and
                              Roll camp in Central
                              Florida, since 1997.<br> <strong>$320 per week.</strong></p>
                           
                           <p><strong>For Rock and Roll Camp, contact Dr Diane Cardarelli, Director, at 407.761.7730.</strong><br> <a href="http://www.rockandrollcamp.net" target="_blank"><strong>Website:
                                    www.rockandrollcamp.net</strong></a><br> Please click the link below and choose "Save". Print the forms,
                              fill out the form and bring to get registered for Camp!<br> <a href="/documents/locations/east/departments/arts-entertainment/music_camps_registration_form.pdf" title="Valencia College Music Camps Registration Form" class="icon_for_pdf">Music Camps Registration
                                 Form</a></p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p>June 5 - June 9, 2017<br> July 24 - July 28, 2017<br> Rock and Roll Camp is an intensive musical
                           experience for students in middle and high school.
                           
                        </p>
                        <a href="../../../locations/east/departments/arts-entertainment/camp.html" class="banner_bt">Registration Form</a>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        <p><a href="tel://4077617730">407 761 7730</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Performing Arts Center</h5>
                        <i class="icon_pencil-edit"></i> <br><em>Valencia College, East Campus <br> 701 N. Econlockhatchee Tr<br>
                           Orlando, Fl 32825</em>
                        
                     </div>
                     
                     
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/camps.pcf">©</a>
      </div>
   </body>
</html>