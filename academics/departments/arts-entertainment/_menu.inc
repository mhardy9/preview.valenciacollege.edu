 <ul>
            <li><a href="/academics/programs/arts-entertainment/index.php">Arts and Entertainment </a></li>
            <li class="submenu">
<a href="#">Associate in Arts Pre-Major <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
              <ul>
                <li><a href="/academics/programs/arts-entertainment/art-studio-fine-art/index.php">Art Studio/Fine Art</a></li>
                    <li><a href="/academics/programs/arts-entertainment/dance-performance/index.php">Dance Performance</a></li>
                    <li><a href="/academics/programs/arts-entertainment/music-performance/index.php">Music Performance</a></li>
				  	<li><a href="/academics/programs/arts-entertainment/music-theatre/index.php">Music Theatre</a></li>
                    <li><a href="/academics/programs/arts-entertainment/theater/index.php">Theater</a></li>
              </ul>
            </li>
            <li class="submenu">
<a href="#">Associate in Science <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
              <ul>
                 <li><a href="/academics/programs/arts-entertainment/digital-media-technology/index.php">Digital Media Technology</a></li>
                  <li><a href="/academics/programs/arts-entertainment/entertainment-design-technology/index.php"> Entertainment Design Technology</a></li>
                 <li><a href="/academics/programs/arts-entertainment/film-production-technology/index.php">Film Production Technology</a></li>
                  <li><a href="/academics/programs/arts-entertainment/graphic-interactive-design/index.php">Graphic and Interactive Design</a></li>
                  <li><a href="/academics/programs/arts-entertainment/sound-music-technology/index.php">Sound and Music Technology</a></li>
              </ul>
            </li>
             <li><a href="/academics/programs/arts-entertainment/certificate.php">Certificate</a></li>
    <li class="submenu">
<a href="#">Events & Venues <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
              <ul>
                <li><a href="/academics/programs/arts-entertainment/camps.php">Camps</a></li>
				<li><a href="/academics/programs/arts-entertainment/performing-arts/index.php">Performing Arts Center</a></li>  
				<li><a href="/academics/programs/arts-entertainment/gallery/index.php">Anita S. Wooten Gallery</a></li>  
              </ul>
	 
	 
          </ul>
  