<div class="header header-site">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-3" role="banner"> <div id="logo">
        <div id="logo"><a href="/index.php"> <img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College Logo" data-retina="true" /></a></div>
      </div>
      <nav class="col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>

        <div class="site-menu">
          <div id="site_menu"><img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College" data-retina="true" /></div>
          <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>

          <ul>
            <li>
<a href="/academics/departments/arts-entertainment/index.php">Arts and Entertainment </a>
            </li>
            <li class="megamenu submenu">
<a href="javascript:void(0);" class="show-submenu-mega">Arts Programs<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
              <div class="menu-wrapper">
                <div class="col-md-4">
                  <h3>Art Studio/Fine Arts</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/index.php">Art
                      Studio</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/faqs.php">Frequently
                      Ask Questions</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/annual-student-art-sale.php">Annual
                        Student Art Sale</a>
</li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Artstudio/contact.cfm" target="_blank">Contact</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Dance</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/index.php">Dance</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/auditions.php">Auditions</a>
                    </li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/events-tickets.php">Events
                      Tickets</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/faqs.php">Frequently Ask
                      Questions</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/dance-degree.php">Dance
                      Degree</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/non-majors-programs.php">Non
                      major Programs</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/valencia-summer-dance-institute.php">Valencia
                        Summer Dance Institute</a>
</li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/dance/valencia-dance-theatre.php">Valencia
                        Dance Theatre</a>
</li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/dance/performance-schedule.php">Performance
                      Schedule</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Graphic Technology</h3>
                  <ul>
                    <li><a href="index.php">Graphics Technology</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="labs.php">Labs</a></li>
                    <li><a href="faqs.php">Frequently Ask Questions</a></li>
                    <li><a href="student-work.php">Student Work</a></li>
                  </ul>

                  <h3>Theatre</h3>
                  <ul>
                    <li>
<a href="/academics/departments/arts-entertainment/theater/index.php">Theater</a>
                    </li>
                    <li><a href="/academics/departments/arts-entertainment/theater/theater-schedule.php">Theater
                      Schedule</a></li>

                  </ul>
                </div>

              </div>
            </li>

            <li class="megamenu submenu">
              <a href="javascript:void(0);" class="show-submenu-mega">Entertainment Programs<i class="far fa-angle-down" aria-hidden="true"></i></a>
              <div class="menu-wrapper">
                <div class="col-md-4">
                  <h3>Digital Media</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/index.php">Digital
                      Media</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/degrees-cetificates.php">Degrees
                        and Certificates</a>
</li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/courses.php">Courses</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/equipment-information.php">Equipment
                        Information</a>
</li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/faculty-staff.php">Faculty
                        and Staff</a>
</li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/facilities.php">Facilities</a>
                    </li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/video.php">Video</a>
                    </li>
                    <li>
<a href="/LOCATIONS/east/departments/arts-entertainment/digital-media/resources.php">Resources</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>Entertainment Design Technology</h3>
                  <ul>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/entertainment-design-technology/index.php">Entertainment</a>
                    </li>
                    <li><a href="http://preview.valenciacollege.edu/artsandentertainment/Entertainmentdesignandtech/contact.cfm" target="_blank">Contact</a></li>
                  </ul>

                  <h3>Film</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/film-technology/index.php">Film
                      Technology</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Filmtech/contact.cfm" target="_blank">Contact</a></li>
                  </ul>

                </div>
                <div class="col-md-4">
                  <h3>Music</h3>
                  <ul>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/index.php">Music</a></li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/faculty-staff.php">Faculty
                      and Staff</a></li>
                    <li>
                      <a href="/LOCATIONS/east/departments/arts-entertainment/music/music-and-sound-technology-program.php">Music
                        and Sound Technology Program</a>
</li>
                    <li><a href="/LOCATIONS/east/departments/arts-entertainment/music/voices-of-valencia.php">Voices
                      of Valencia</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Music/contact.cfm" target="_blank">Contact</a></li>
                  </ul>
                  <h3>Music and Sound Technology </h3>
                  <ul>
                    <li>
                      <a href="/academics/departments/arts-entertainment/music-sound-technology/index.php">Music
                        and Sound Technology</a>
</li>
                  </ul>
                </div>
              </div>
            </li>
            <li><a href="/academics/departments/arts-entertainment/camps.php">Camps</a></li>
            <li><a href="/LOCATIONS/east/departments/arts-entertainment/art-studio/anita-s-wooten-gallery.php">Anita
              S. Wooten Gallery</a></li>
          </ul>
           

        </div>
         
      </nav>
    </div>
  </div>
   
</div>
 

 

 
<div class="sub-header bg-1">
  <div id="intro-txt">
    <h1>Arts and Entertainment</h1>
    <p>Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp; entertainment degree can
      help you turn what you love into what you do for a living.</p>
  </div>
</div>
 

 

 
<div>
  <div id="route" role="navigation" aria-label="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/">Valencia College</a></li>
        <li><a href="/LOCATIONS">Locations</a></li>
        <li><a href="/academics/departments/arts-entertainment/index.php">Arts and Entertainment</a></li>
        <li>Graphic Technology</li>
      </ul>
    </div>
  </div>
   
   
     
<main role="main">
  <div class="container margin-60">
    <div class="row">
      <div class="col-md-9">
        <div class="box_style_1">
          <div class="indent_title_in">
            <h3>Graphic Technology</h3>
            <p>Graphics Technology prepares graduates for careers in print and advertising design or web and interactive
              design</p>
          </div>
          <div class="wrapper_indent">
            <h4>Plan Your Degree</h4>
            <a href="/documents/locations/east/departments/arts-entertainment/graphics_technology_flow_chart.pdf"><strong>Download
              Flow Chart to plan your Degree path</strong></a><h4>Recharging creative minds</h4>
            <p>As a graphics major you&rsquo;ll know what it means to kern type, crop photos, and &ldquo;lose the widows.&rdquo; You&rsquo;ll
              become intrigued by art direction, and know a stage is not just for acting it&rsquo;s also for animating. And
              you&rsquo;ll understand that speaking new languages prepares you for your journey to designing for the World
              Wide Web. Valencia&rsquo;s A.S. degree program in Graphics Technology prepares graduates for careers in print
              and advertising design or web and interactive design, giving them the skills they need and insight from
              real-world designers. Graduates have landed jobs in design studios, advertising agencies, printing
              companies, web design firms, interactive design companies and other highly creative environments.</p>
            <p>Orlando boasts a wide-reaching and wildly diverse graphics arts community. Designers here are involved in
              advertising, band poster design, web design, animation, interface design and everything in between.
              Orlando boasts a wide-reaching and wildly diverse graphics arts community. Designers here are involved in
              advertising, band poster design, web design, animation, interface design and everything in between.
            </p>

            <h4>Starting with creative excellence</h4>
            <p>With a trusted reputation in the Orlando creative industry, we pride ourselves in educating some of the
              most talented designers in central Florida. Our students have received recognition in local, regional,
              national, and even international competitions. Their accomplishments include over 100 ADDY Awards, Florida
              Print Awards, the Create Awards, the ONE Show, Print Magazine, and Siggraph. And perhaps part of the
              reason why many local employers are impressed with hiring our graduates is also because we understand what
              the industry expects from graduates. Real world experience combined with reinforcing various employable
              skills ranging from the creative process to communication all make the graphics technology program a great
              start for anyone seeking to start right in a creative career.</p>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p></p>
              <h4></h4>
            </div>
          </div>

          <hr class="styled_2" />
          <div class="indent_title_in">
            <h3>Degree Programs and Certificates</h3>
            <p>Degrees and Certificates We Offer</p>
          </div>
          <div class="wrapper_indent">
            <p></p>
            <div class="row">
              <div class="col-md-12">
                <p>
                  </p>
<ul class="list_teachers">
                    <h4>Degrees We Offer:</h4>
                    <li>
<h5>AS Degree: Graphic Design</h5>
                <p>The Graphic Design Specialization prepares students to become graphic designers, publication
                  designers, layout artists, advertising designers, user interface designers, user experience designers,
                  production artists or creative technicians. It provides instruction in courses directly related to
                  developing job skills for entry-level positions in advertising agencies, design studios and art
                  departments for retail and other businesses. This specialization is best suited for people who are
                  artistic, creative and enjoy traditional as well as computer design.<br />
                </p>
</li>
                <li>
<h5>AS Degree: Interactive Design</h5>
                  <p>The Interactive Design specialization prepares students to become entry-level web designers, user
                    interface designers, user experience designers, multimedia artists or interactive designers. If you
                    enjoy creating graphics, developing interactive media and/or designing web pages, you will enjoy
                    this specialization. <br />
                  </p>
</li>
                <h4>Certificates We Offer:</h4>

                <li>
<h5>Graphic Design Production</h5>
                  <p>This certificate is designed to provide students with the skills required to create layouts,
                    imagery, and designs for print, advertising and web design. The certificate focuses on the
                    application of good design principles and the utilization of industry-standard production techniques
                    as well as software and hardware at an advanced level. </p>
</li>

                <li>
<h5>Graphic Design Support</h5>
                  This certificate is designed to provide students with the skills required to create layouts and
                  graphics for print and basic web or interactive media venues. The certificate focuses on the
                  application of good design principles and the utilization of industry-standard production techniques
                  as well as software and hardware at an intermediate level.
                </li>

                <li>
<h5>Interactive Design Production</h5>
                  <p>This certificate is designed to provide students with the skills required to create images, web
                    design and interactive media for the internet or similar venues. The certificate focuses on the
                    application of appropriate production techniques and the utilization of industry-standard software
                    and hardware at an advanced level. </p>
</li>

                <li>
<h5>Interactive Design Support</h5>
                  <p>This certificate is designed to provide students with the skills required to create images, basic
                    web design and interactive media for the internet or similar venues. The certificate focuses on the
                    application of appropriate production techniques and the utilization of industry-standard software
                    and hardware at an intermediate level.</p>
</li>

                </ul>

              </div>
            </div>
          </div>
          <hr class="styled_2" />
          <div class="indent_title_in">
            <h3>Faculty and Staff</h3>
            <p></p>
          </div>
          <div class="wrapper_indent">
            <p></p>
            <div class="row">
              <div class="col-md-12">
                <p>Students enrolled in the Graphics Technology program will be instructed by faculty who are trained
                  and experienced in the graphics industry. Many of the instructors have worked or are currently working
                  with some of the leading design companies and adverstising agencies in the central Florida area. Some
                  of our instructors have impressive work experience with companies such as Disney, Anson-Stoner,
                  Harcourt, Flitehaus, and the Orlando Sentinel. Students will receive training from instructors who are
                  proficient with industry-standard software.</p>
                <p>The Graphics Technology Program has dedicated staff assisting with maintaining the graphics lab. Lab
                  aides are available to assist troubleshooting any technical problems in the graphics lab. In addition,
                  lab aides coordinate the print cards and release of prints for course projects. Lab aides are
                  available to assist as much as possible, however, they are not able to assist tutoring students on the
                  the use of graphics applications.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <aside class="col-md-3">
        <div class="banner">
<i class=" iconcustom-school"></i>

          <h3>Rock and Roll Camp 2017</h3>
          <p>June 5 - June 9, 2017<br /> July 24 - July 28, 2017<br /> Rock and Roll Camp is an intensive musical
            experience for students in middle and high school.
          </p>
          <a href="../../../../locations/east/departments/arts-entertainment/graphics-technology/camp.php" class="banner_bt">Registration Form</a>
</div>

        <hr class="styled" />

        <div class="box_side">
          <h5>By Phone</h5>
          <i class="icon-phone"></i>
          <p><a href="tel://4077617730">407 761 7730</a></p>
        </div>
        <hr class="styled" />
        <div class="box_side">
          <h5>Performing Arts Center</h5>
          <i class="icon_pencil-edit"></i> <br /><em>Valencia College, East Campus <br /> 701 N. Econlockhatchee Tr<br />
          Orlando, Fl 32825</em>
        </div>

        <hr class="styled" />
        
        <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
<span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
          <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
</div>
        
        <hr class="styled" />

    </aside>
</div>
    
  </div>
   
</main>
</div>
 
</div>
