<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Events  | Valencia College</title>
      <meta name="Description" content="Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp;amp; entertainment degree can help you turn what you love into what you do for a living">
      <meta name="Keywords" content="arts and entertainment, art studio, dance, digital media, entertainment, graphics technology, music, theatre, valencia arts, fine arts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/graphics-technology/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/graphics-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/departments/arts-entertainment/graphics-technology/">Graphics Technology</a></li>
               <li>Events </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Events</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4></h4>
                           
                           <p><strong>Congratulations</strong> to all of the Graphic + Interactive Design students and Digital Media
                              students who won awards at the 2017 Annual Student Juried Show Opening and Awards
                              Ceremony that took place
                              Thursday, April 20th! Here is the list of awards, winners and works:
                           </p>
                           
                           <p>Form, Function and Error Free – <strong>Aleitha Burton</strong> / Awoke Vintage Website<br> Excellence
                              in Typography Award (Web) – <strong>Christina Anzalone</strong> / Valencia School of Design Website<br>
                              Best Usability Award – <strong>Aleitha Burton</strong> / Awoke Vintage Website<br> Dean’s Purchase Award
                              for Graphics – <strong>Christina Anzalone</strong> / Valencia School of Design Website<br> Video Ninja
                              Award – <strong>Thalia Bunch</strong> / They Live Titles Reimagining<br> Master of Space, Time and
                              Dimension Award – <strong>Thalia Bunch</strong> / Medlee App Spot<br>
                              &amp;lt;!– I make the web more awesome. –&amp;gt; Award – <strong>Jennifer Melear</strong> / Comic
                              Museum Website<br> Best of Web Award – <strong>Christina Anzalone</strong> / Valencia School of Design
                              Website<br> Excellence in Spot Color Design – <strong>Heather Hunsinger</strong> / Malicious
                              Affliction<br> Dr. R. S. Grove Memorial Award in Illustration – <strong>Monica Belakbir</strong> / Hwang
                              Jin Yi<br> For the Fun of It (Purchase Award) – <strong>Steph Aziz</strong> / Master Peace<br> ACME “Now
                              That’s Using Your Head” Award – <strong>Adam Case</strong> / Lego Ad Campaign<br> Theodora and Peter
                              Mohess Memorial Award – <strong>Beatriz Toledo Zanin</strong> / MARITACA packaging<br> The I Never Won an
                              Award but you did (!!) Award – <strong>Joaris Manning</strong> / Infusion Tea Rebrand<br> One Hun Concept
                              Award – <strong>Aleitha Burton</strong> / Sextremism<br> Chris and Devin Wish They Were As Good As You
                              Award – <strong>Lianne Hayre</strong> / Times Poster<br> Free Business Cards Award – <strong>Jamie
                                 Mariscal</strong> / Zuzu Bikes &amp; Gears<br> you’re clearly awesome at what you do, shut up and
                              take my moneys –<strong> Christina Anzalone</strong> / Finch’s Landing<br> The Dazzling Illustration
                              Award – <strong>Heather Hunsinger</strong> / Artist Book Covers<br> For the third consecutive year of
                              unbiased greatness, this award continues to be (and will always be) a better award
                              then Jason Ellison’s
                              hack of an award, award – <strong>Joaris Manning</strong> / Kokoa Chocolate<br> Excellence in Design for
                              Philanthropy Award – <strong>Christina Anzalone</strong> / Cocoon Branding<br> Microsoft Award – <strong>Jesse
                                 Woodall</strong> / Self Portrait With Bear<br> Jason Had to Have it Award (purchase award) – <strong>Jesse
                                 Woodall</strong> / Is That All There is to a Five<br> Graphic Design Award – <strong>Heather
                                 Hunsinger</strong> / Barley &amp; Vine Biergarten Branding<br> Barbara Peterson Excellence in Design
                              Award – <strong>Rebekah Rigel</strong> / Happy Henna Hedgehog Branding<br> Excellence in Typography Award
                              (Print) – <strong>Aleitha Burton</strong> / Forte Magazine<br> Enduring Optimism Award – <strong>Natalie
                                 Lauletta</strong> / How to Make a Smoothie Animation
                           </p>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-12">
                              
                              <p></p>
                              
                              <h4></h4>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p>June 5 - June 9, 2017<br> July 24 - July 28, 2017<br> Rock and Roll Camp is an intensive musical
                           experience for students in middle and high school.
                           
                        </p>
                        <a href="../../../../locations/east/departments/arts-entertainment/graphics-technology/camp.html" class="banner_bt">Registration Form</a>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        <p><a href="tel://4077617730">407 761 7730</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Performing Arts Center</h5>
                        <i class="icon_pencil-edit"></i> <br><em>Valencia College, East Campus <br> 701 N. Econlockhatchee Tr<br>
                           Orlando, Fl 32825</em>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
                        <span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
                        <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/graphics-technology/events.pcf">©</a>
      </div>
   </body>
</html>