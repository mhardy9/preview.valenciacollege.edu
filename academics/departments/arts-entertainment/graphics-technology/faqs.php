<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graphic Technology  | Valencia College</title>
      <meta name="Description" content="Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp;amp; entertainment degree can help you turn what you love into what you do for a living">
      <meta name="Keywords" content="arts and entertainment, art studio, dance, digital media, entertainment, graphics technology, music, theatre, valencia arts, fine arts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/graphics-technology/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/graphics-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/departments/arts-entertainment/graphics-technology/">Graphics Technology</a></li>
               <li>Graphic Technology </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Frequently Asked Questions (FAQ)</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4></h4>
                           
                           <p><strong>Send only work that is near or at portfolio quality.</strong><br> Much like your own portfolios,
                              we want to showcase the best work. If you aren't sure it doesn't hurt to send us your
                              work. But if you
                              know it still needs a lot of work please take the time to refine your project before
                              sending it to us.
                           </p>
                           
                           <p><strong>Send us the details.</strong><br> Because we weren't in class with you we really don't have any
                              information but what you provide us, so please provide the following when you send
                              your files: Title of
                              piece, your name, your instructor's name, semester you completed the project, course
                              name, any techniques
                              used (if applicable: i.e. gradient mesh, etc.)
                           </p>
                           
                           <p><strong>Only send us jpg files.</strong><br> To help us speed up the process of displaying your work
                              please crop your work and send it in jpg format. Please keep your file over 1000px
                              by 1000px but less than
                              2000px by 2000px. This will allow for a larger resolution version to be downloadable
                              online. Do NOT send
                              any files over 1MB, reducing your image size and saving for the web will keep your
                              file size to a minimum.
                              
                           </p>
                           
                           <p><strong>Send a photo of the piece only if it compliments the project.</strong><br> Some project such as
                              products, brochures, and self promotional pieces are most effective if they are photographed.
                              There is a
                              light booth on east-campus campus that works well for photoshoots.
                           </p>
                           
                           <p><strong>Web/Interactive Design</strong><br> I am currently working on adding more web and interactive
                              work in the upcoming weeks, unfortunately it takes a bit more work because we will
                              be linking to the
                              actual interactive project which we will host on our program web site. Here are a
                              few guidelines for
                              interactive projects:<br> <br> <strong>Any Flash over 100k must have a preloader.</strong><br> If you
                              did not incorporate a preloader for a project, before it is posted it must have a
                              preloader.<br> <br>
                              <strong>Incorporate your flash file into an html page.</strong><br> This will speed up posting. You can
                              do this through Flash or Dreamweaver.<br> <br> <strong>Make sure your home page is named
                                 index.html</strong><br> <br> <strong>Zip your files</strong><br> You will need to zip your files to
                              send them via email. If they are still several megabytes you may need to send to us
                              using alternative
                              methods such as ftp or using the dropbox on campus.<br> <br> <strong>We DON'T need your source
                                 files</strong><br> Please don't send any FLA, PSD, or AI files. If we need them we will request
                              them.<br> <br> <strong>Send us a screenshot.</strong><br> Obviously we can't post the interactive
                              project directly in Flickr so we'll need a screen shot of your project like this.
                              Send your screenshot in
                              jpg format as described above for print design.<br> <br> <strong>Send us the details.</strong><br>
                              Because we weren't in class with you we really don't have any information but what
                              you provide us, so
                              please provide the following when you send your files: Title of piece, your name,
                              your instructors name,
                              semester you completed the project, course name, and any special interactive features.<br> <br> So send
                              us your work, we're eager to see more great projects! Send all work to <a href="mailto:akern@valenciacollege.edu">akern [at] valenciacollege.edu.</a> If you don't see your work
                              in Flickr or you don't receive a response from me it's likely that your email might
                              not have gone through
                              so send us a follow up email if that happens. (Sorry, VCC's email is a tad bit secure!)<br> <br>
                              <strong>Note:</strong><em>Sending your work to us does not guarantee it will be posted in Flickr. We
                                 reserve the right to decline posting in Flickr, and of course we'll let you know why.
                                 </em></p>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-12">
                              
                              <p></p>
                              
                              <h4></h4>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p>June 5 - June 9, 2017<br> July 24 - July 28, 2017<br> Rock and Roll Camp is an intensive musical
                           experience for students in middle and high school.
                           
                        </p>
                        <a href="../../../../locations/east/departments/arts-entertainment/graphics-technology/camp.html" class="banner_bt">Registration Form</a>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        <p><a href="tel://4077617730">407 761 7730</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Performing Arts Center</h5>
                        <i class="icon_pencil-edit"></i> <br><em>Valencia College, East Campus <br> 701 N. Econlockhatchee Tr<br>
                           Orlando, Fl 32825</em>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
                        <span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
                        <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/graphics-technology/faqs.pcf">©</a>
      </div>
   </body>
</html>