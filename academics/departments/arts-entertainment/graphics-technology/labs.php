<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Labs  | Valencia College</title>
      <meta name="Description" content="Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp;amp; entertainment degree can help you turn what you love into what you do for a living">
      <meta name="Keywords" content="arts and entertainment, art studio, dance, digital media, entertainment, graphics technology, music, theatre, valencia arts, fine arts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/graphics-technology/labs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/graphics-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/departments/arts-entertainment/graphics-technology/">Graphics Technology</a></li>
               <li>Labs </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Labs</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4></h4>
                           
                           <h5><strong>East Campus Lab Building 1 Room 213&nbsp;</strong></h5>
                           
                           <p>Students have access to large and small format laser and ink jet printers, various
                              format paper cutters,
                              board cutters, headphone check-out, loupe check-out, spray booth, viewing booth, light
                              table, camera
                              check-out, scanners, wacom tablet check-out and Cintiq monitors. All this in a friendly
                              social working
                              environment with a modern resources for students to browse and lounge while working
                              on their laptops.<br>
                              Phone:&nbsp;<strong>407-582-2762</strong></p>
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                           <p>Visit <a href="http://multimedia.valenciacollege.edu" target="_blank">http://multimedia.valenciacollege.edu</a>&nbsp;for
                              current hours and closed dates. Check before you drive.<br>
                              
                              <em>No Food or Drink Allowed. Certain supplies like custom printing paper, staples, paperclips,
                                 xacto
                                 blades, adhesive, etc. not supplied to students in the lab.</em></p>
                           
                           
                           <h5><strong>West Campus Lab 3-150</strong></h5>
                           
                           <p>Students have access to large and small format laser and ink jet printers, various
                              cutting areas,
                              headphone check-out, camera check-out, light table, scanners, loupe check-out, photo
                              tent and wacom tablet
                              check-out. Please be mindful if there are classes in session when using this lab by
                              using only the
                              computers in the back of the room.<br> Phone:&nbsp;<strong>407-582-1592</strong><br>
                              
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li><strong>West Campus Graphics Lab (5-130) Hours:&nbsp;</strong></li>
                              
                              <li>Monday: 9am – 9pm</li>
                              
                              <li>Tuesday: 9am – 9pm</li>
                              
                              <li>Wednesday: 9am – 9pm</li>
                              
                              <li>Thursday: 9am – 9pm</li>
                              
                              <li>Friday: 9am – 12pm</li>
                              
                              <li>Saturday: 9am – 3pm</li>
                              
                           </ul>
                           <em>No Food or Drink Allowed. Certain supplies like custom printing paper, staples, paperclips,
                              xacto
                              blades, adhesive, etc. not supplied to students in the lab.</em>
                           
                           <h5><strong>Osceola Campus Lab 1-244</strong></h5>
                           
                           <p>Students have access to large and small format laser and ink jet printers, various
                              cutting areas, camera
                              check-out, lights and light cube, media card readers, scanners, loupe check-out, Pantone
                              swatch books, and
                              wacom tablet check-out. Please be patient while we work on making lab improvements
                              you’re going to
                              love.<br> Phone 407-582-4953<br>
                              
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li><strong>Osceola Campus Graphics Lab (1-244) Hours:&nbsp;</strong></li>
                              
                              <li>Call for current hours</li>
                              
                           </ul>
                           <em>No Food or Drink Allowed. Certain supplies like custom printing paper, staples, paperclips,
                              xacto
                              blades, adhesive, etc. might not be available for students in the lab.</em>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-12">
                              
                              <p></p>
                              
                              <h4></h4>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p>June 5 - June 9, 2017<br> July 24 - July 28, 2017<br> Rock and Roll Camp is an intensive musical
                           experience for students in middle and high school.
                           
                        </p>
                        <a href="../../../../locations/east/departments/arts-entertainment/graphics-technology/camp.html" class="banner_bt">Registration Form</a>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        <p><a href="tel://4077617730">407 761 7730</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Performing Arts Center</h5>
                        <i class="icon_pencil-edit"></i> <br><em>Valencia College, East Campus <br> 701 N. Econlockhatchee Tr<br>
                           Orlando, Fl 32825</em>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
                        <span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
                        <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/graphics-technology/labs.pcf">©</a>
      </div>
   </body>
</html>