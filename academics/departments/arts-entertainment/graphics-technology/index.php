<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graphic Technology  | Valencia College</title>
      <meta name="Description" content="Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts &amp;amp; entertainment degree can help you turn what you love into what you do for a living">
      <meta name="Keywords" content="arts and entertainment, art studio, dance, digital media, entertainment, graphics technology, music, theatre, valencia arts, fine arts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/graphics-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/graphics-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-entertainment/">Arts Entertainment</a></li>
               <li>Graphics Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Graphic Technology</h3>
                           
                           <p>Graphics Technology prepares graduates for careers in print and advertising design
                              or web and interactive
                              design
                           </p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Plan Your Degree</h4>
                           <a href="/documents/locations/east/departments/arts-entertainment/graphics_technology_flow_chart.pdf"><strong>Download
                                 Flow Chart to plan your Degree path</strong></a><h4>Recharging creative minds</h4>
                           
                           <p>As a graphics major you’ll know what it means to kern type, crop photos, and “lose
                              the widows.” You’ll
                              become intrigued by art direction, and know a stage is not just for acting it’s also
                              for animating. And
                              you’ll understand that speaking new languages prepares you for your journey to designing
                              for the World
                              Wide Web. Valencia’s A.S. degree program in Graphics Technology prepares graduates
                              for careers in print
                              and advertising design or web and interactive design, giving them the skills they
                              need and insight from
                              real-world designers. Graduates have landed jobs in design studios, advertising agencies,
                              printing
                              companies, web design firms, interactive design companies and other highly creative
                              environments.
                           </p>
                           
                           <p>Orlando boasts a wide-reaching and wildly diverse graphics arts community. Designers
                              here are involved in
                              advertising, band poster design, web design, animation, interface design and everything
                              in between.
                              Orlando boasts a wide-reaching and wildly diverse graphics arts community. Designers
                              here are involved in
                              advertising, band poster design, web design, animation, interface design and everything
                              in between.
                              
                           </p>
                           
                           
                           <h4>Starting with creative excellence</h4>
                           
                           <p>With a trusted reputation in the Orlando creative industry, we pride ourselves in
                              educating some of the
                              most talented designers in central Florida. Our students have received recognition
                              in local, regional,
                              national, and even international competitions. Their accomplishments include over
                              100 ADDY Awards, Florida
                              Print Awards, the Create Awards, the ONE Show, Print Magazine, and Siggraph. And perhaps
                              part of the
                              reason why many local employers are impressed with hiring our graduates is also because
                              we understand what
                              the industry expects from graduates. Real world experience combined with reinforcing
                              various employable
                              skills ranging from the creative process to communication all make the graphics technology
                              program a great
                              start for anyone seeking to start right in a creative career.
                           </p>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-12">
                              
                              <p></p>
                              
                              <h4></h4>
                              
                           </div>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Degree Programs and Certificates</h3>
                           
                           <p>Degrees and Certificates We Offer</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>
                                    
                                 </p>
                                 
                                 <ul class="list_teachers">
                                    
                                    <h4>Degrees We Offer:</h4>
                                    
                                    <li>
                                       
                                       <h5>AS Degree: Graphic Design</h5>
                                       
                                       <p>The Graphic Design Specialization prepares students to become graphic designers, publication
                                          designers, layout artists, advertising designers, user interface designers, user experience
                                          designers,
                                          production artists or creative technicians. It provides instruction in courses directly
                                          related to
                                          developing job skills for entry-level positions in advertising agencies, design studios
                                          and art
                                          departments for retail and other businesses. This specialization is best suited for
                                          people who are
                                          artistic, creative and enjoy traditional as well as computer design.<br>
                                          
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <h5>AS Degree: Interactive Design</h5>
                                       
                                       <p>The Interactive Design specialization prepares students to become entry-level web
                                          designers, user
                                          interface designers, user experience designers, multimedia artists or interactive
                                          designers. If you
                                          enjoy creating graphics, developing interactive media and/or designing web pages,
                                          you will enjoy
                                          this specialization. <br>
                                          
                                       </p>
                                       
                                    </li>
                                    
                                    <h4>Certificates We Offer:</h4>
                                    
                                    
                                    <li>
                                       
                                       <h5>Graphic Design Production</h5>
                                       
                                       <p>This certificate is designed to provide students with the skills required to create
                                          layouts,
                                          imagery, and designs for print, advertising and web design. The certificate focuses
                                          on the
                                          application of good design principles and the utilization of industry-standard production
                                          techniques
                                          as well as software and hardware at an advanced level. 
                                       </p>
                                       
                                    </li>
                                    
                                    
                                    <li>
                                       
                                       <h5>Graphic Design Support</h5>
                                       This certificate is designed to provide students with the skills required to create
                                       layouts and
                                       graphics for print and basic web or interactive media venues. The certificate focuses
                                       on the
                                       application of good design principles and the utilization of industry-standard production
                                       techniques
                                       as well as software and hardware at an intermediate level.
                                       
                                    </li>
                                    
                                    
                                    <li>
                                       
                                       <h5>Interactive Design Production</h5>
                                       
                                       <p>This certificate is designed to provide students with the skills required to create
                                          images, web
                                          design and interactive media for the internet or similar venues. The certificate focuses
                                          on the
                                          application of appropriate production techniques and the utilization of industry-standard
                                          software
                                          and hardware at an advanced level. 
                                       </p>
                                       
                                    </li>
                                    
                                    
                                    <li>
                                       
                                       <h5>Interactive Design Support</h5>
                                       
                                       <p>This certificate is designed to provide students with the skills required to create
                                          images, basic
                                          web design and interactive media for the internet or similar venues. The certificate
                                          focuses on the
                                          application of appropriate production techniques and the utilization of industry-standard
                                          software
                                          and hardware at an intermediate level.
                                       </p>
                                       
                                    </li>
                                    
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Faculty and Staff</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>Students enrolled in the Graphics Technology program will be instructed by faculty
                                    who are trained
                                    and experienced in the graphics industry. Many of the instructors have worked or are
                                    currently working
                                    with some of the leading design companies and adverstising agencies in the central
                                    Florida area. Some
                                    of our instructors have impressive work experience with companies such as Disney,
                                    Anson-Stoner,
                                    Harcourt, Flitehaus, and the Orlando Sentinel. Students will receive training from
                                    instructors who are
                                    proficient with industry-standard software.
                                 </p>
                                 
                                 <p>The Graphics Technology Program has dedicated staff assisting with maintaining the
                                    graphics lab. Lab
                                    aides are available to assist troubleshooting any technical problems in the graphics
                                    lab. In addition,
                                    lab aides coordinate the print cards and release of prints for course projects. Lab
                                    aides are
                                    available to assist as much as possible, however, they are not able to assist tutoring
                                    students on the
                                    the use of graphics applications.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p>June 5 - June 9, 2017<br> July 24 - July 28, 2017<br> Rock and Roll Camp is an intensive musical
                           experience for students in middle and high school.
                           
                        </p>
                        <a href="../../../../locations/east/departments/arts-entertainment/graphics-technology/camp.html" class="banner_bt">Registration Form</a>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        <p><a href="tel://4077617730">407 761 7730</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Performing Arts Center</h5>
                        <i class="icon_pencil-edit"></i> <br><em>Valencia College, East Campus <br> 701 N. Econlockhatchee Tr<br>
                           Orlando, Fl 32825</em>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div id="kaltura_player_1495127090" style="width: 300px; height: 200px;" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
                        <span itemprop="name" content="Rock and Roll Camp 2010"></span> <span itemprop="description" content="Rock and Roll Camp"></span> <span itemprop="duration" content="94"></span> <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/2070621/sp/207062100/thumbnail/entry_id/1_1l0o86d5/version/100001"></span>
                        <span itemprop="width" content="270"></span> <span itemprop="height" content="200"></span>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/graphics-technology/index.pcf">©</a>
      </div>
   </body>
</html>