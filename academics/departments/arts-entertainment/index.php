<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Arts and Entertainment | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-entertainment/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-entertainment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Entertainment</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Arts Entertainment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="col-md-9">
                  		
                  		
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/art-studio/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/art-studio.png" alt="ART STUDIO"><div class="short_info">
                                    <h3>ART STUDIO</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              					 
                              <h3><strong>Art Studio</strong></h3>
                              					
                              <p>The Visual Arts Club is a group dedicated to fun artistic activities while helping
                                 to build skills and provide useful resources and knowledge to become successful in
                                 the art industry.
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/art-studio/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/dance-performace/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/dance_program.png" alt="DANCE"><div class="short_info">
                                    <h3>DANCE PERFORMANCE</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Dance Performance</strong></h3>
                              						
                              <p> The Valencia College Dance Program goal is to provide quality technical training
                                 and the enhancement of the overall dance experience.
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/dance-performace/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/digital-media-technology/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/digital-media.png" alt="Digital Media"><div class="short_info">
                                    <h3>Digital Media Technology</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Digital Media Technology</strong></h3>
                              						 
                              <p>Valencia's Entertainment Design and Technology degree program is one of only a few
                                 in the nation and the only one in Central Florida that prepares students to work in
                                 the production aspects of the entertainment industry.
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/digital-media-technology/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/entertainment-design-technology/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/entertainment-design.png" alt="Entertainment Design Technology"><div class="short_info">
                                    <h3>Entertainment Design Technology</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Entertainment Design Technology</strong></h3>
                              						
                              <p>Valencia's Entertainment Design and Technology degree program is one of only a few
                                 in the nation and the only one in Central Florida that prepares students to work in
                                 the production aspects of the entertainment industry.
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/entertainment-design-technology/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/film-technology/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/film.png" alt="Film Technology"><div class="short_info">
                                    <h3>Film Technology</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Film Technology</strong></h3>
                              						
                              <p>Valencia's nationally recognized Film Production Technology program is a selective
                                 admission, limited enrollment program and....
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/film-technology/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/graphics-technology/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/graphics-technology.png" alt="Graphics Technology"><div class="short_info">
                                    <h3>Graphics Technology</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Graphics Technology</strong></h3>
                              						 
                              <p>With a trusted reputation in the Orlando creative industry, we pride ourselves in
                                 educating some of the most talented designers in central Florida. Our students have
                                 received....
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/graphics-technology/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/music-performance/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/music.png" alt="Music"><div class="short_info">
                                    <h3>Music Performance</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Music Performance</strong></h3>
                              						 
                              <p>Since 1975, the music program has been dedicated to the idea of inspiring future performers
                                 by providing students with musically trained faculty and....
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/music-performance/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/sound-music-technology/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/music-technology.png" alt="Music Technology"><div class="short_info">
                                    <h3>Sound and Music Technology</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Sound and Music Technology</strong></h3>
                              						
                              <p>Valencia's Sound and Music Technology program takes a unique approach to preparing
                                 you for an exciting career in the music and sound industry.
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/sound-music-technology/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="img_list"><a href="/academics/programs/arts-and-entertainment/theater/index.php" target=""><img src="/_resources/images/academics/programs/arts-entertainment/theater.png" alt="Theater"><div class="short_info">
                                    <h3>Theatre/Dramatic Arts</h3>
                                 </div></a></div>
                        </div>
                        <div class="clearfix visible-xs-block"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="course_list_desc">
                              						
                              <h3><strong>Theatre/Dramatic Arts</strong></h3>
                              						
                              <p>Valencia College Theater produces four major productions each year, as well as a series
                                 of student-directed one-act plays. Productions are held either in our 558-seat....
                              </p>
                              					
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                           <div class="details_list_col">
                              <div><a href="/academics/programs/arts-and-entertainment/theater/index.php" target="" class="button-outline">Details</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  	
               </div>
               <aside class="col-md-3">
                  		
                  <div class="banner"><i class="iconcustom-school"></i>
                     						
                     <h3>Career Coach</h3>	
                     					
                     <p>Web Production certificate earners make the second highest salaries of all Valencia
                        graduates, averaging <strong>$47,897</strong> annually. 
                     </p>	
                     					<a href="https://valenciacollege.emsicareercoach.com" target="" class="banner_bt">LEARN MORE</a></div>
                  			
                  <hr>
                  
                  <p><a class="button-action" href="/academics/apply_online.html">Apply Now</a><br>
                     <a class="button-action" href="/academics/apply_online.html">Visit Valencia</a><br>
                     <a class="button-action" href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskwpro.P_ChooseProspectType">Request Information</a></p>
                  
                  <hr>			
                  	
                  <h4><strong>Need Help?</strong></h4>
                  For general enrollment questions, assistance is available on a walk-in basis at the
                  <a href="http://preview.valenciacollege.edu/answer-center/">Answer Center</a> or contact Enrollment Services.
                  		<br>
                  		<i class="fas fa-phone"></i> <a href="tel:14075821507" style="margin-left:10px;">407-582-1507</a><br>
                  		<i class="fas fa-envelope"></i> <a href="mailto:enrollment@valenciacollege.edu?subject=Website%20Inquiry" style="margin-left:10px;">enroll@valenciacollege.edu</a>
                  
                  		
                  <p></p>
                  
                  	
                  	
               </aside>
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-entertainment/index.pcf">©</a>
      </div>
   </body>
</html>