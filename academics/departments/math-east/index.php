<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Math Department | Valencia College</title>
      <meta name="Description" content="East Campus Math Department">
      <meta name="Keywords" content="college, school, educational, east, math">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-east/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Math Department</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Math East</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h3>Administration</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td style="text-align: left;"><img src="http://valenciacollege.edu/east/math/images/MLeeCropped_000.png" alt="1" width="76" height="76" align="default" hspace="0" vspace="0"></td>
                                 
                                 <td style="text-align: left;">
                                    
                                    <p><a href="mailto:mllee@valenciacollege.edu"><b>Dr. Maryke Lee</b></a></p>
                                    
                                    <p>Dean of Mathematics</p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td style="text-align: left;"><img src="http://valenciacollege.edu/east/math/images/Alison-Hammack.jpg" alt="" width="76" height="78"></td>
                                 
                                 <td style="text-align: left;">
                                    
                                    <p><a href="mailto:ahammack@valenciacollege.edu"><strong>Prof. Alison Hammack</strong></a></p>
                                    
                                    <p>Academic Organizational Support</p>
                                    
                                    <p>(407) 582-2027</p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td style="text-align: left;"><img src="http://valenciacollege.edu/east/math/images/GMillerCropped.png" alt="2" width="76" height="76" align="default" border="0" hspace="0" vspace="0"></td>
                                 
                                 <td style="text-align: left;">
                                    
                                    <p><a href="mailto:gmiller34@valenciacollege.edu"><b>Guy Miller</b></a></p>
                                    
                                    <p>Administrative Assistant</p>
                                    
                                    <p>(407) 582-2438</p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td style="text-align: left;"><img src="http://valenciacollege.edu/east/math/images/Dawn-Chesire.jpg" alt="2" width="70" height="82" align="default" border="0" hspace="0" vspace="0"></td>
                                 
                                 <td style="text-align: left;">
                                    
                                    <p><a href="mailto:dchesire@valenciacollege.edu"><strong>Dawn Chesire</strong></a></p>
                                    
                                    <p>Administrative Assistant</p>
                                    
                                    <p>(407) 582-2417</p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td style="text-align: left;"><img src="http://valenciacollege.edu/east/math/images/Tasleema-Khan.jpg" alt="2" width="70" height="79" align="default" border="0" hspace="0" vspace="0"></td>
                                 
                                 <td style="text-align: left;">
                                    
                                    <p><a href="mailto:tkhan9@valenciacollege.edu"><strong>Tasleema Khan</strong></a></p>
                                    
                                    <p>Staff Assistant</p>
                                    
                                    <p>(407) 582-2451</p>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>&nbsp;</p>
                        
                        <h3 class="heading_divider_red">General Information</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td width="116"><b>Location:</b></td>
                                 
                                 <td>Bldg. 7, Room 142</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td width="116"><b>Phone:</b></td>
                                 
                                 <td width="378">(407) 582-2366</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td width="116"><b>Fax:</b></td>
                                 
                                 <td width="378">(407) 582-8913</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td width="116"><b>Mail Code:</b></td>
                                 
                                 <td width="378">3-16</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>&nbsp;</p>
                        
                        <h3 class="heading_divider_red">Office Hours</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th colspan="2">Fall/Spring</th>
                                 
                                 <th colspan="2">Summer</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Mon-Thu</td>
                                 
                                 <td>7:30 am - 5:30 pm</td>
                                 
                                 <td>Mon-Thu</td>
                                 
                                 <td>7:30 am - 5:30 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Fri</td>
                                 
                                 <td>7:30 am - 5:00 pm</td>
                                 
                                 <td>Fri</td>
                                 
                                 <td>8:00 am - 12:00 pm</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3 class="heading_divider_red">Calculator Checkout Dates for&nbsp;Spring</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><b>Calculator Checkout Begins:</b></td>
                                 
                                 <td>January 8th</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><b>Calculators Must Be Retured By:</b></td>
                                 
                                 <td>April 30th</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>&nbsp;</p>
                        
                        <h3 class="heading_divider_red">Resources</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td width="233">
                                    
                                    <p><a href="http://site.valenciacollege.edu/eastcampus/Shared%20Documents/Forms/AllItems.aspx?RootFolder=/eastcampus/Shared%20Documents/Mathematics%20Meeting%20Minutes&amp;FolderCTID=0x01200030CA6BA89F2CBF4FB19BC19FFAEB7C04&amp;View=%7B021FAE1A-7984-477E-95FC-03B0CCF578DF%7D" target="_blank">Department Meeting Minutes</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/faculty/development/centers/index.php" target="_blank">Faculty Development</a><span>&nbsp;</span>&nbsp;
                                    </p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/academics/departments/math-east/FacultyResources.php">Faculty Resources</a></p>
                                    
                                    <p><a href="http://valenciacollege.edu/edge/" target="_blank">The Edge</a></p>
                                    
                                 </td>
                                 
                                 <td width="260">
                                    
                                    <p align="right"><a href="/students/math/livescribe.php"><span><span><img src="http://valenciacollege.edu/math/images/math-24-7_245x60.png" alt="Math 24/7 Help" width="245" height="60" border="0"></span></span></a><span></span></p>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h2>&nbsp;</h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-east/index.pcf">©</a>
      </div>
   </body>
</html>