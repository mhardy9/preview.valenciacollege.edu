<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Resources | East Campus Math Department | Valencia College</title>
      <meta name="Description" content="Faculty Resources | East Campus Math Department">
      <meta name="Keywords" content="college, school, educational, east, math, faculty, resources">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-east/facultyresources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-east/">Math East</a></li>
               <li>Faculty Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Faculty Resources</h2>
                        
                        
                        <p>&nbsp;&nbsp;<a href="/students/math/livescribe.php" title="Math Help 24/7"><img alt="Math Help 24/7" border="0" height="60" src="images/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <div>
                           
                           <ul>
                              
                              <li tabindex="0">Main</li>
                              
                              <li tabindex="0">MAT0018C</li>
                              
                              <li tabindex="0">MAT0022C</li>
                              
                              <li tabindex="0">MAT0028C</li>
                              
                              <li tabindex="0">MAT1033C</li>
                              
                              <li tabindex="0">STA1001C</li>
                              
                              <li tabindex="0">MAC1105</li>
                              
                              <li tabindex="0">MGF1106</li>
                              
                              <li tabindex="0">MGF1107</li>
                              
                              <li tabindex="0">MAC1114</li>
                              
                              <li tabindex="0">MAC1140</li>
                              
                              <li tabindex="0">STA2023</li>
                              
                              <li tabindex="0">MAC2233</li>
                              
                              <li tabindex="0">MAC2311</li>
                              
                              <li tabindex="0">MAC2312</li>
                              
                              <li tabindex="0">MAC2313</li>
                              
                              <li tabindex="0">MAP2302</li>
                              
                              <li tabindex="0">MAE2801</li>
                              
                              <li tabindex="0">MHF2300</li>
                              
                              <li tabindex="0">MAS2103</li>
                              
                           </ul>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>Welcome to East Campus Mathematics Faculty Resources page. Please click on the tabs
                                             above to access course information. The course names associated with their prefixes
                                             are listed below for reference.
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><span>MAT0018C - Developmental Math I</span></div>
                                          
                                          <div><span>MAT0022C - Developmental Math Combined</span></div>
                                          
                                          <div><span>MAT0028C - Developmental Math II</span></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><span>STA1001C - Introduction to Statistical Reasoning</span></div>
                                          
                                          <div><span>MAT1033C - Intermediate Algebra</span></div>
                                          
                                          <div><span>MAC1105 - College Algebra</span></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><span>MGF1106 - College Mathematics</span></div>
                                          
                                          <div><span>MGF1107 - Liberal Arts Math</span></div>
                                          
                                          <div><span>MAC1114 - College Trigonometry</span></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><span>MAC1140 - Precalculus Algebra</span></div>
                                          
                                          <div><span>STA2023 - Statistical Methods</span></div>
                                          
                                          <div><span>MAC2233 - Business Calculus</span></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><span>MAC2311 - Calculus I</span></div>
                                          
                                          <div><span>MAC2312 - Calculus II</span></div>
                                          
                                          <div><span>MAC2313 - Calculus III</span></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><span>MAP2302 - Differential Equations</span></div>
                                          
                                          <div><span>MAE2801 - Elementary School Math</span></div>
                                          
                                          <div><span>MHF2300 - Logic &amp; Proof in Mathematics</span></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div><span>MAS2103 - Introduction to Linear Algebra</span></div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><strong>Additional Course Information</strong></p>
                                 
                                 <ul>
                                    
                                    <li><a href="../../../locations/calendar/FinalExam.html">Final                 exam information</a></li>
                                    
                                    <li><a href="courseinfo/syllabus.htm">Generic         syllabus information</a></li>
                                    
                                    <li><a href="courseinfo/grade.htm">Grading Options</a></li>
                                    
                                    <li><a href="../../../locations/catalog/index.html">College Catalog</a></li>
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAT0018C - Developmental Math I</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><a href="mailto:jphelps@valenciacollege.edu">Julie Phelps</a></div>
                                          
                                          <div>7-142</div>
                                          
                                          <div>2527</div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:ksiler@valenciacollege.edu">Keri Siler</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>1-365</div>
                                          
                                          <div>2867</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAT0018C.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/Summer_MAT0018C.docx">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Prealgebra" by Elayn Martin-Gay.  6th edition. ISBN 1256786578</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Chapter 1</u>: Whole Numbers<br>
                                    <u>Chapter 2</u>: Integers<br>
                                    <u>Chapter 3</u>: Solving Equations &amp; Applications<br>
                                    <u>Chapter 4</u>: Fractions<br>
                                    <u>Chapter 5</u>: Decimals<br>
                                    <u>Chapter 6</u>: Ratios, Proportions, Percentages, and Applications<br>
                                    <u>Chapter 7</u>: Polynomials, Exponents, Area, Volume, and Measurements
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8<br>
                                    <u>Week 2</u>: 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, Chapter 1/2 Test<br>
                                    <u>Week 3</u>: 3.1, 3.2, 3.3, 3.4, 3.5, 7.1, 7.2, 7.3 <br>
                                    <u>Week 4</u>: Chapter 3/7 Test, 4.1, 4.2, 4.3, 4.4<br>
                                    <u>Week 5</u>: 4.5, 4.6, 4.7, 4.8, 5.1, 5.2, 5.3, 5.4, 5.5<br>
                                    <u>Week 6</u>: 5.6, Chapter 4/5 Test, 6.1, 6.2, 6.4, 6.5<br>
                                    <u>Week 7</u>: 6.6, 6.7, 6.8, 6.9, 7.4, 7.5, 7.6, 7.7<br>
                                    <u>Week 8</u>: Chapter 6/7 Test, Final Exam<br>
                                    
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAT0022C - Developmental Math Combined</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:lcohen5@valenciacollege.edu">Lisa Cohen</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>6-225</div>
                                          
                                          <div>2257</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAT0022C.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_mat0022c.doc">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Developmental Math Combined" by  Al Groccia. ISBN 978-1607974567</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Pre Chapter A:</u> Whole Numbers <br>
                                    <u>Pre Chapter B</u>: Fractions <br>
                                    <u>Pre Chapter C</u>: Decimals<br>
                                    <u>Pre Chapter D</u>: Conversions, Percent, Proportions <br>
                                    <u>Pre Chapter E</u>: Integers <br>
                                    <u>Chapter 1 </u>: 1.6, 1.7 <br>
                                    <u>Chapter 2 </u>: 2.1, 2.2, 2.5, 2.6, 2.7 <br>
                                    <u>Chapter 3 </u>: Graphing <br>
                                    <u>Chapter 4 </u>: Polynomials <br>
                                    <u>Chapter 5 </u>: Factoring/Rational Expressions <br>
                                    <u>Chapter 6 </u>: 6.1, 6.2, 6.3, 6.4, 6.5
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: Pre Chapter A/B <br>
                                    <u>Week 2</u>: Pre Chapter B/C <br>
                                    <u>Week 3</u>: Pre Chapter D/E <br>
                                    <u>Week 4</u>: 1.6, 1.7, Pre Chapters +1.6, 1.7 Test <br>
                                    <u>Week 5</u>: 2.1, 2.2, 2.5 <br>
                                    <u>Week 6</u>: 2.6, 2.7, 3.1, 3.2 <br>
                                    <u>Week 7</u>: 3.3, 3.4, 3.5, Chapter 2/3 Test <br>
                                    <u>Week 8</u>: 4.1, 4.2, 4.3<br>
                                    <u>Week 9</u>: 4.4, 4.5, 4.6, 4.7<br>
                                    <u>Week 10</u>: Chapter 4 Test, 5.1, 5.2, 5.3<br>
                                    <u>Week 11</u>: 5.4, 5.5, 5.6<br>
                                    <u>Week 12</u>: 5.7, 5.8, 5.9, 5.10<br>
                                    <u>Week 13</u>: Chapter 5 Test, 6.1, 6.2<br>
                                    <u>Week 14</u>: 6.3, 6.4, 6.5, Chapter 6 Test <br>
                                    <u>Week 15</u>: Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAT0028C - Developmental Math II</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:lcohen5@valenciacollege.edu">Lisa Cohen</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>6-225</div>
                                          
                                          <div>2257</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAT0028C.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_mat0028c.doc">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Developmental Math II" by Al Groccia.  ISBN 978-1607974550</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Chapter 1</u>: Real Numbers<br>
                                    <u>Chapter 2</u>: Algebraic Expressions and Equations<br>
                                    <u>Chapter 3</u>: Graphing<br>
                                    <u>Chapter 4</u>: Polynomials<br>
                                    <u>Chapter 5</u>: Factoring/Rational Expressions<br>
                                    <u>Chapter 6</u>: Radicals
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 2.1<br>
                                    <u>Week 2</u>: 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, Chapter 1/2 Test<br>
                                    <u>Week 3</u>: 3.1, 3.2, 3.3, 3.4, 3.5<br>
                                    <u>Week 4</u>: 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, Chapter 3/4 Test<br>
                                    <u>Week 5</u>: 5.1, 5.2, 5.3, 5.4, 5.5, 5.6<br>
                                    <u>Week 6</u>: 5.7, 5.9, 5.10, Chapter 5 Test<br>
                                    <u>Week 7</u>: 6.1, 6.2, 6.4, 6.5, 6.6, 6.7<br>
                                    <u>Week 8</u>: Chapter 6 Test, Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAT1033C - Intermediate Algebra</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:jberman@valenciacollege.edu">Joel Berman</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>4-240</div>
                                          
                                          <div>2130</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/mat1033c.pdf">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_mat1033c.doc">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Intermediate Algebra:  Connecting Concepts through Applications" by Mark Clark &amp;
                                    Cynthia Anfinson.  1st edition. ISBN 978-0495983255
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Scientific notation - arithmetic operations,  convertingï¿½ (Appendix A)<br>
                                    Using the slope and the y intercept to graph  linear functions (1.3)<br>
                                    Finding the equation of a line: point-slope,  standard, slope-intercept forms (1.5)<br>
                                    Analyzing a set of data to determine whether it  can be modeled by a linear function
                                    (1.5)<br>
                                    Using the slope to identify parallel and  perpendicular lines (1.5)<br>
                                    Finding the domain and range of a function  represented graphically (1.7)<br>
                                    Finding the x and y intercepts of a function  represented graphically. (1.7 and throughout)<br>
                                    Solving applications using function notation  (1.7 and throughout)<br>
                                    Identifying when a relation represented  verbally, numerically, algebraically or graphically
                                    is a function (1.7)<br>
                                    Understanding function notation (1.7)<br>
                                    Evaluating functions (1.7)<br>
                                    Finding the domain of a function represented  algebraically (1.7, and reviewed throughout)<br>
                                    Identifying and graphing linear functions (1.7)<br>
                                    Identifying and graphing by plotting points  quadratic (Chapter 4), cubic (?), square
                                    root (8.1) and absolute value  functions (?)Interpreting graphically the number of
                                    solutions of systems of  linear equations in two variables. (2.1)<br>
                                    Using a graphical approach to solve systems of  equations in two variables. (2.1)<br>
                                    Solving systems of linear equations in two  variables algebraically. (2.2, 2.3)<br>
                                    Using systems of linear equations in two  variables to model and solve applications.
                                    (2.1, 2.2, 2.3)<br>
                                    Solving absolute value equations reducing to  linear equations. (2.5)<br>
                                    Solving compound inequalities (this text only  presents compound inequalities as a
                                    result of absolute value inequalities in  2.5.ï¿½ I have included several exercises
                                    in the WebAssign course.)<br>
                                    Solving a system of linear inequalities  graphically (2.6)<br>
                                    Understanding the meaning of rational exponents  (3.1)<br>
                                    Converting between the radical and rational  exponent form (3.1)<br>
                                    Evaluating rational exponents (3.1)<br>
                                    Performing algebraic operations with rational  exponents (3.1)<br>
                                    Using the sum and difference of cubes to factor  (3.5)<br>
                                    Given the graph of a quadratic function,  finding the vertex, the axis of symmetry,
                                    the maximum and minimum value. (4.1)<br>
                                    Solving quadratic equations using the square  root property (4.4)<br>
                                    Solving quadratic equations by factoring (4.5)<br>
                                    Solving quadratic equations by using the  quadratic formula (4.6)<br>
                                    Given the algebraic representation of a  rational function, finding its domain (7.1)<br>
                                    Given the algebraic representation of a  rational function, identifying its vertical
                                    asymptotes (7.1)<br>
                                    Performing long division of polynomials (7.2)<br>
                                    Perform operations with rational expressions  (7.3, 7.4)<br>
                                    Simplifying complex fractions (7.4)<br>
                                    Identifying a rational equation (7.5)<br>
                                    Solving rational equations (7.5)<br>
                                    Solving literal rational equations (7.5)<br>
                                    Evaluating radical expressions (8.1)<br>
                                    Simplifying radicalsï¿½ (8.1)<br>
                                    Given the algebraic representation of a radical  function, finding its domain (8.1)<br>
                                    Evaluating radical functions (8.1)<br>
                                    Performing operations with radicals (8.2, 8.3)<br>
                                    Rationalizing radical expressions (8.3)<br>
                                    Solving radical equations (8.4)<br>
                                    Writing complex numbers in standard form (8.5)<br>
                                    Finding the conjugate of a complex number (8.5)<br>
                                    Performing arithmetic operations with complex  numbers including rationalizing (8.5)<br>
                                    Identifying the real and imaginary parts (8.5)
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 1.1, 1.2, 1.3<br>
                                    <u>Week 2</u>: 1.4, 1.5, 1.7<br>
                                    <u>Week 3</u>: 2.1, 2.2<br>
                                    <u>Week 4</u>: 2.3, 2.4, 2.5<br>
                                    <u>Week 5</u>: 2.6, Test 1<br>
                                    <u>Week 6</u>: Exponents, Scientific Notation, 3.4, 3.5<br>
                                    <u>Week 7</u>: 4.1, 4.2 <br>
                                    <u>Week 8</u>: 4.4, 4.5, 4.6<br>
                                    <u>Week 9</u>: 4.7, Test 2<br>
                                    <u>Week 10</u>: 7.1, 7.2<br>
                                    <u>Week 11</u>: 7.3, 7.4<br>
                                    <u>Week 12</u>: 7.5, 8.1, Rational Exponents (3.1)<br>
                                    <u>Week 13</u>: 8.2, 8.3<br>
                                    <u>Week 14</u>: 8.4 <br>
                                    <u>Week 15</u>: 8.5, Test 3<br>
                                    <u>Week 16</u>: Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>Section</p>
                                          </div>
                                          
                                          <div>
                                             <p>Exercises</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p>Chapter 1</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>11, 13, 15,    17, 23, 25, 27, 31, 35, 39, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61,
                                                63, 65,    71, 73, 75, 77, 79, 81, 83
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>3-6, 7, 19,    11, 13, 17, 23, 25, 29, 31, 33, 35, 37, 39</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>3, 7, 9,    13, 15, 17,[only lines no quadratics] 23, 25, 27, 29, 31, 33, 41, 43,
                                                45,    47, 49, 61, 63, 65, 67
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>11, 13, 15,    17, 19, 21, 23, 25, 27, 29, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57,
                                                59
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7,    9, 11, 13, 19, 21, 25, 27, 31, 35, 37, 39, 43, 59, 63, 67, 71, 75,
                                                79, 83,    87, 91, 95
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.6</p>
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.7</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7,    9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 35, 37, 39, 41, 43, 45, 47,
                                                49, 51,    53,ï¿½ 55, 65, 67, 69
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p>Chapter 2</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7,    9, 11, 13, 15, 17, 19, 21, 23, 25, 45, 47, 49, 51, 53, 55, 57</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7,    9, 19, 21, 23, 25, 31, 33, 35, 41, 45, 47, 49, 51, 55, 57, 61</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7,    9, 11, 13, 15, 19, 21, 22, 25, 27, 29, 31, 41, 43, 45, 49, 53, 57,
                                                59, 61,    63, 65
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>7, 9, 13,    15, 25, 27, 29, 33, 35, 37, 39, 43, 45, 47, 49, 51, 53, 55, 57, 59</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3,5, 7,    9,11, 13,15, 17,19</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.6</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7,    9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41,
                                                43, 45,    47, 49
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p>Chapter 3</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>63, 67, 73, 75, 79, 81, 93, 97, 99, 103</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.2</p>
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>Scientific    Notatation</p>
                                          </div>
                                          
                                          <div>
                                             <p>Appendix A: 89-104</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 43, 45,    47, 49, 51, 53, 55, 57,
                                                59, 61, 63, 65, 67, 69, 71, 73, 83, 85
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 9, 11, 12, 15, 17, 19, 21, 27, 29, 31, 33, 35,    37, 39, 41, 43, 45,
                                                47, 49, 51, 53, 55, 57, 59, 61, 63
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p>Chapter 4</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 7, 9, 11, 15, 17, 19, 21, 25, 29, 31, 33, 35, 37,    41, 43, 45, 47</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31,    33, 35, 37, 43, 57,
                                                61, 63, 65, 67, 69, 71, 75
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.3</p>
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 7, 9, 13, 15, 19, 21, 25, 45, 47, 49, 53, 55, 57, 59,    65, 87, 89, 91, 93, 99</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 5, 11, 15, 17, 21, 22, 25, 27, 35, 39, 41, 49, 51, 57,    55, 59, 63, 65, 69, 71,
                                                73, 75, 77, 79, 83, 85, 91, 93, 95
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.6</p>
                                          </div>
                                          
                                          <div>
                                             <p>11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37,    39, 41, 43, 45, 47, 49,
                                                51, 53, 55, 57, 59
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.7</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39,    41, 43, 45, 47, 49</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p>Chapter 7</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 23, 25, 27, 29, 33, 35, 39, 41, 43, 51, 53, 55, 31,    63, 65, 69</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 7, 13, 15, 17, 19, 21, 23, 29, 33, 39, 43, 45, 47, 49,    51, 79, 81, 83, 85</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31,    32, 35, 37, 39, 41,
                                                43, 45, 47, 49
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>21, 23, 25, 27, 29, 45, 47, 49, 51, 53, 55, 57, 59, 61,    63, 65, 67, 69, 71, 73,
                                                77
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 5, 9 ,11, 13, 17, 21, 23, 25 [you want to do this    numerically] 35, 39, 45, 51,
                                                53, 57, 61, 63
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p>Chapter 8</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>8.1</p>
                                          </div>
                                          
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 11, 13, 19, 21, 23, 25, 27, 29, 31, 33, 35,    43, 45, 49, 51, 61, 63,
                                                65, 71, 73, 75
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>8.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 5, 7, 9, 11, 15, 17, 19, 25 ,27, 31, 33, 41, 43, 45,    47, 49, 51, 53, 59, 61,
                                                63, 65, 67, 69, 71, 73, 75
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>8.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 5, 9, 13, 17, 21, 25, 29, 31, 35, 41, 45, 49, 53, 57,    61, 65, 69, 73, 77, 81,
                                                85, 89
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>8.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 9, 13, 17, 21, 25, 27, 29, 33, 35, 37, 39, 41,    47, 49, 61, 65</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>8.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>3, 7, 11, 15, 19, 21, 25, 29, 33, 37, 41, 43, 47, 51, 55,    59, 63, 67, 69, 73, 77,
                                                81
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>STA1001C - Introduction to Statistical Reasoning</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><a href="mailto:dhoward@valenciacollege.edu">Deborah Howard </a></div>
                                          
                                          <div>8-207</div>
                                          
                                          <div>2092</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/STA1001C.docx">Fall/Spring</a></div>
                                          
                                          <div>Summer</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Statistical Reasoning for Understanding Part I" by Jody DeVoe &amp; Roberta Carew.</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Steps in the statistical analysis process<br>
                                    Chance variability<br>
                                    Populations vs. samples<br>
                                    Data collection techniques (sampling, bias,  etc.)<br>
                                    Types of studies (observational vs. experiment)<br>
                                    Types of research questions <br>
                                    Study Design <br>
                                    Appropriate conclusions <br>
                                    Displaying and Summarizing Data <br>
                                    Characteristics of a distribution <br>
                                    Range <br>
                                    Standard Deviation/Variance <br>
                                    5-number summary <br>
                                    Inner Quartile Range <br>
                                    Median, Mean <br>
                                    Dot plot <br>
                                    Histogram <br>
                                    Box plot <br>
                                    Use of technology (graphing calculator,  Statcrunch, etc.) <br>
                                    Bivariate Relationships - Quanitative Data <br>
                                    Linear and non-linear patterns <br>
                                    Introduction to lines and graphing <br>
                                    Linear regression <br>
                                    Correlation Coefficient - r <br>
                                    Interpreting slope and y-intercept in context <br>
                                    Intro to exponential behavior <br>
                                    Exponential regression <br>
                                    Growth vs. Decay <br>
                                    Interpreting y-intercept and base in context <br>
                                    Bivariate Relationships - Categorical Data <br>
                                    Conditional Probability <br>
                                    Marginal Probability <br>
                                    Joint Probability <br>
                                    Interpreting probabilities in context <br>
                                    Creating hypothetical two-way tables <br>
                                    Fundamental Math Skills (Lab/In-Class) <br>
                                    Order of Operations <br>
                                    Decimals: Place values and comparison <br>
                                    Exponents &amp; Scientific Notation <br>
                                    Solving basic equations (including literal equations) <br>
                                    Word Problem Solution Skills (effective reading  strategies) <br>
                                    Student Success Skills (Lab/In-Class) <br>
                                    Note-Taking Skills <br>
                                    Study Skills <br>
                                    Time Management <br>
                                    Stress Management <br>
                                    Test Taking Strategies 
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 1.1, 1.2 <br>
                                    <u>Week 2</u>: 2.1<br>
                                    <u>Week 3</u>: 2.2, 2.3 <br>
                                    <u>Week 4</u>: 2.4, 2.5 <br>
                                    <u>Week 5</u>: 2.5, Test 1 <br>
                                    <u>Week 6</u>: 3.1, 3.2 <br>
                                    <u>Week 7</u>: 3.3, 3.4<br>
                                    <u>Week 8</u>: 3.5, 3.6 <br>
                                    <u>Week 9</u>: 3.7<br>
                                    <u>Week 10</u>: Test 2, 4.1 <br>
                                    <u>Week 11</u>: 4.2, 5.1 <br>
                                    <u>Week 12</u>: 5.1, 5.2, 5.3 <br>
                                    <u>Week 13</u>: 6.1a, 6.1b, 6.2 <br>
                                    <u>Week 14</u>: Test 4 <br>
                                    <u>Week 15</u>: Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Online through <a href="http://www.pearsonmylabandmastering.com/northamerica/mystatlab/" target="_blank">MyStatLab</a> <br>
                                    
                                 </p>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAC1105 - College Algebra</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:asaxman@valenciacollege.edu">Amanda Saxman</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>8-201</div>
                                          
                                          <div>2865</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAC1105.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/Summer_MAC1105.docx">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"College Algebra with Current Interesting Applications and Facts" by Gisela Acosta
                                    &amp; Margie Karwowski.  1st edition. ISBN 978-1465250254
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 3</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 2.1, 2.2, 2.3, 2.4 <br>
                                    <u>Week 2</u>: Chapter 2 Test, 3.1, 3.2 <br>
                                    <u>Week 3</u>: 3.3, 3.4 <br>
                                    <u>Week 4</u>: Chapter 3 Test, 4.1, 4.2 <br>
                                    <u>Week 5</u>: 4.3, 4.4 <br>
                                    <u>Week 6</u>: 4.5, 4.6 <br>
                                    <u>Week 7</u>: Chapter 4 Test, 5.1, 5.2, 5.3 <br>
                                    <u>Week 8</u>: Chapter 5 Test, 7.1, 7.2 <br>
                                    <u>Week 9</u>: 7.3, 7.4 <br>
                                    <u>Week 10</u>: Chapter 7 Test, 6.1 <br>
                                    <u>Week 11</u>: 6.2, 6.3 <br>
                                    <u>Week 12</u>: 6.4 <br>
                                    <u>Week 13</u>: Chapter 6 Test <br>
                                    <u>Week 14</u>: Review for Final Exam <br>
                                    <u>Week 15</u>: Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Online through <a href="https://www.webassign.net/login.html" target="_blank">WebAssign</a></p>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MGF1106 - College Mathematics</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:jmccormick5@valenciacollege.edu">Jennifer Adams </a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>4-235</div>
                                          
                                          <div>2023</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MGF1106.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_mgf1106.pdf">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"A Survey of Mathematics w/ Applications" by Allen R. Angel, Christine D. Abbott &amp;
                                    Dennis C. Runde.  9th edition. ISBN 0-321-83753-3
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 3</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Online through <a href="http://www.pearsonmylabandmastering.com/northamerica/mymathlab/" target="_blank">MyMathLab</a> 
                                 </p>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MGF1107 - Liberal Arts Math</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:pmurphy2@valenciacollege.edu">Patrick Murphy</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>8-257</div>
                                          
                                          <div>2482</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/mgf1107.doc">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_mgf1107.doc">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Mathematics Beyond the Numbers" by George T. Gilbert &amp; Rhonda L. Hatcher.  2nd edition.
                                    ISBN 1-4652-0417-2
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>See "Suggested Homework" below </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>See "Suggested Homework" below </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Read Section 4.1 (Polygons)<br>
                                    Section 4.1 Exercises: p. 251 # 1-31 ODD<br>
                                    Read Section 4.2 (Tilings)<br>
                                    Section 4.2 Exercises: p. 266 # 1-9 ODD, 13, 17<br>
                                    Read Section 4.3 (Polyhedra)<br>
                                    Section 4.3 Exercises: p. 281 # 5-17 ODD, 21ab, 31<br>
                                    Chapter 4 Review Exercises: p. 289 # 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 13, 14, 16<br>
                                    <strong>TEST 1 covers sections 4.1, 4.2, 4.3</strong></p>
                                 
                                 <p>Read Section 2.2 (Simple Interest)<br>
                                    Section 2.2 Exercises: p. 97 # 1-31 ODD<br>
                                    Read Section 2.3 (Compound Interest)<br>
                                    Section 2.3 Exercises: p. 112 # 1-11 ODD, 27, 29, 33, 35, 39, 41, 43, 47, 49, 51<br>
                                    Read Section 2.4 (The Rewards of Systematic Savings)<br>
                                    Section 2.4 Exercises: p. 123 # 1-19 ODD, 23<br>
                                    Read Chapter 2.5 (Amortized Loans)<br>
                                    Section 2.5 Exercises: p. 138 # 1-15 ODD, 17 (remaining balance), 19-33 ODD<br>
                                    Chapter 2 Review Exercises: p. 147 # 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 16, 17,
                                    18, 19<br>
                                    <strong>TEST 2 covers sections 2.2, 2.3, 2.4, 2.5</strong></p>
                                 
                                 <p>Read Section 3.1 (Eulerian Paths and Circuits on Graphs)<br>
                                    Section 3.1 Exercises: p. 171 # 1-27 ODD<br>
                                    Read Section 3.2 (The Traveling Salesman Problem)<br>
                                    Section 3.2 Exercises: p. 197 (Hamilton Circuit, Nearest Neighbor Algorithm) # 1,
                                    3, 5, 7a, 9a, 11a, 13a, 17ab<br>
                                    p. 198 (Greedy Algorithm) # 7b, 9b, 11b, 13b, 15, 17acd, 19ac, 21abc<br>
                                    Read Section 3.3 (Efficient Networking: Minimal Spanning Trees, Prim's Algorithm)<br>
                                    Section 3.3 Exercises: p. 227 # 1-25 ODD<br>
                                    Chapter 3 Review Exercises: p. 237 # 1-14 ALL, 16<br>
                                    <strong>TEST 3 covers sections 3.1, 3.2, 3.3</strong></p>
                                 
                                 <p>Read Section 5.1 (Divisibility and Primes)<br>
                                    Section 5.1 Exercises: p. 311 # 13-27 ODD, 39-47 ODD <br>
                                    Read Section 5.2 (Modular Arithmetic)<br>
                                    Section 5.2 Exercises: p. 318 (positive numbers mod n) # 1, 5, 7, 13, 17, 21-43 ODD<br>
                                    p. 318 (negative numbers mod n) # 3, 9, 11, 15, 19<br>
                                    Read Section 5.4 (Check Digits)<br>
                                    Section 5.4 Exercises: p. 335 # 1-17 ODD <br>
                                    Read Section 5.6 (Introduction to Cryptology)<br>
                                    Section 5.6 Exercises: p. 356 # 1-9 ODD, 10, 11, 12, 13<br>
                                    Chapter 5 Review Exercises: p. 375 # 1, 2, 3, 5, 6, 7, 8, 13, 15, 16, 17, 18, 21,
                                    22, 23<br>
                                    <strong>TEST 4 covers sections 5.1, 5.2, 5.4, 5.6</strong></p>
                                 
                                 <p>Read Section 1.1 (Plurality and Runoff Methods)<br>
                                    Section 1.l Exercises: p. 15 # 1-15 ODD, 25<br>
                                    Read Section 1.2 (Borda's Method: A Scoring System)<br>
                                    Section 1.2 Exercises: p. 28 # 1, 3, 5, 7, 9, 11abc, 13a<br>
                                    Read Section 1.3 (Head-to-head Comparisons)<br>
                                    Section 1.3 Exercises: p. 41 # 1-11 ODD, 15, 19<br>
                                    Read Section 1.4 (Approval Voting)<br>
                                    Section 1.4 Exercises: p. 53 # 1, 3a, 5, 7, 9a-e, 11a-e, 13<br>
                                    Chapter 1 Review Exercises: p. 75 # 1, 2, 3, 4, 5, 6a-d, 7a-e<br>
                                    <strong>TEST 5 covers sections 1.1, 1.2, 1.3, 1.4</strong></p>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAC1114 - College Trigonometry</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><a href="mailto:ashaw17@valenciacollege.edu">Agatha Shaw</a></div>
                                          
                                          <div>8-249</div>
                                          
                                          <div>2117</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAC1114.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/Summer_MAC1114.docx">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Analytic Trigonometry w/Applications" by Raymond A. Barnett.  11th edition. ISBN
                                    978-1118427057
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <ul>
                                    
                                    <li> Given an angle in standard position and either the x-coordinate, y-coordinate, or
                                       radius, find the other two (unit circle definition).
                                    </li>
                                    
                                    <li> Given a point on the terminal side of an angle in standard position, find the values
                                       of the trigonometric functions.
                                    </li>
                                    
                                    <li> Given special angles (multiples of pi/2, pi/3, pi/4, pi/6), know the exact values
                                       of the trigonometric functions.
                                    </li>
                                    
                                    <li> Given a right triangle, express the trigonometric ratios in terms of the sides of
                                       the triangle.
                                    </li>
                                    
                                    <li> Given an applied problem involving right triangles, make a graphic representation
                                       that describes the situation, and solve the problem.
                                    </li>
                                    
                                    <li> Given an applied problem that requires the use of the Law of Sines and/or the Law
                                       of Cosines, make a graphic representation that describes the situation, and solve
                                       the problem.
                                    </li>
                                    
                                    <li> Given the equation of a trigonometric function, determine the amplitude, period,
                                       phase shift, and vertical shift, and the graph of the function.
                                    </li>
                                    
                                    <li> Given the graph of a sine or cosine function, determine the equation of the function.</li>
                                    
                                    <li> Given an applied problem involving a periodic function, determine a graphic and symbolic
                                       representation of the problem.
                                    </li>
                                    
                                    <li> Know and apply basic trigonometric identities and basic algebra skills to verify
                                       identifies and simplify expressions.
                                    </li>
                                    
                                    <li> Prove or verify trigonometric identities using algebraic manipulation.</li>
                                    
                                    <li> Apply trigonometric identities for sum, difference, or multiple angles to find equivalent
                                       trigonometric expressions including inverse functions
                                    </li>
                                    
                                    <li> Determine the solutions of trigonometric equations.</li>
                                    
                                    <li> Solve trigonometric equations that arise from applied problems.</li>
                                    
                                    <li> Define and graph inverse Trigonometric functions.</li>
                                    
                                    <li> Evaluate inverse trig functions and compositions of trig and inverse trig functions.</li>
                                    
                                    <li> Solve Inverse trigonometric equations</li>
                                    
                                    <li> Solve Inverse trigonometric equations that arise from applied problems.</li>
                                    
                                    <li> Perform operations on two-dimensional vectors.</li>
                                    
                                    <li> Solve application problems using two-dimensional vectors.</li>
                                    
                                    <li> Graph conic sections and polar representations of conic sections</li>
                                    
                                    <li> Convert between polar coordinates and rectangular coordinates </li>
                                    
                                    <li> Graph equations given in polar form.</li>
                                    
                                    <li> Parametric equations</li>
                                    
                                    <li> Express complex numbers in trig form and be able to find the product, quotient, powers
                                       and roots of complex numbers.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><br>
                                    
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 1.1, 1.2 <br>
                                    <u>Week 2</u>: 1.3, 1.4 <br>
                                    <u>Week 3</u>: 2.1, 2.3<br>
                                    <u>Week 4</u>: 2.5, Test 1 <br>
                                    <u>Week 5</u>: 3.1, 3.2 <br>
                                    <u>Week 6</u>: 3.3, 3.6 <br>
                                    <u>Week 7</u>: 4.1, 4.2, 4.3 <br>
                                    <u>Week 8</u>: 4.4, 4.5 <br>
                                    <u>Week 9</u>: Test 2, 5.1 <br>
                                    <u>Week 10</u>: 5.2, 5.3<br>
                                    <u>Week 11</u>: 6.1, 6.2 <br>
                                    <u>Week 12</u>: 6.4, 6.5 <br>
                                    <u>Week 13</u>: Test 3 <br>
                                    <u>Week 14</u>: 7.1, 7.2 <br>
                                    <u>Week 15</u>: Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><strong>Section</strong> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p>Problems</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 9, 11, 15, 17, 21, 23, 29, 39, 41, 45, 47, 49, 51, 61, 63</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 7, 13, 15, 27, 31, 33, 35</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>17, 19, 21, 23, 29, 31, 41, 43, 45, 47, 49</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>1.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 5, 7, 11, 13, 15, 17, 19, 25, 37, 39</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 21-25 all, 27, 29, 37, 39, 43, 45, 49, 51, 53, 55, 71</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>Omit this section</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 9, 13, 17, 19, 21, 23, 49, 51, 53, 55, 57, 59, 77</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>Omit this section</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 7, 9, 11, 13, 15, 17, 19, 21, 22, 23, 24, 27, 29, 31, 32, 33, 35, 37, 39, 41,
                                                55, 57, 59, 71,73,75, 79, 81
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 9, 11, 13, 15, 19-24 all</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 7, 9, 11, 13, 19, 31, 33, 35, 37, 39, 57</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>7, 9, 11, 17, 19, 21, 23, 25, 27, 31, 33, 35, 39, 41, 47, 51</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>Omit this section</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>Omit this section</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3.6</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 5, 7, 9, 11, 13, 15, 17, 21, 23, 25, 27, 29, 31, 33</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 9, 13, 15, 17, 25, 27, 31, 33, 43, 45, 47, 53, 59</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 9, 11, 15, 17, 19, 21, 23, 29, 30, 31, 33, 37, 41, 49, 57, 67, 71</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>13, 17, 21, 25, 27, 29, 31, 33, 41, 43, 45, 55, 63, 65</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 5, 11, 13, 15, 19, 21, 27, 45, 47, 49, 51, 53, 63, 65, 67</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 7, 13, 15, 17, 21, 25, 29, 33, 45, 47, 59</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <br>
                                             
                                             <p>5.1</p>
                                             
                                          </div>
                                          
                                          <div>
                                             <p>1, 3, 7, 9, 11, 13, 15, 17,&nbsp; 27-39 odd, 51, 53, 65-77 odd</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>5.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 9, 13, 17, 23, 25, 27, 33, 37, 43, 47, 55, 59, 63, 65, 69, 71, 75</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>5.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 11, 13, 15, 19, 29, 31, 33, 35, 37, 39, 51, 53</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>5.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>Omit this section</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>6.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>7, 9, 11, 15, 17,31, 37, 43, 47, 49, 57, 59, 61, 65, 67, 69</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>6.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>11, 15, 17, 25, 29, 33, 35, 37, 43, 51, 55, 57, 61</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>6.3</p>
                                          </div>
                                          
                                          <div>
                                             <p>Omit this section</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>6.4</p>
                                          </div>
                                          
                                          <div>
                                             <p>3, 5, 9, 13, 17, 19, 23, 25, 33, 35, 37, 43, 47, 77</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>6.5</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 9, 13, 15, 19, 21, 23, 25, 27, 29, 31, 35, 37, 41, 45, 47</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7.1</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 7, 13, 17, 23, 25, 31, 33, 53, 57, 61, 65, 67, 69, 73, 79</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7.2</p>
                                          </div>
                                          
                                          <div>
                                             <p>5, 7, 9, 11, 13, 19, 21, 23, 25</p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAC1140 - Precalculus Algebra</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:jmccloskey@valenciacollege.edu">Jim McCloskey</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>1-121</div>
                                          
                                          <div>2221</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAC1140.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/Summer_MAC1140.docx">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Precalculus" by Coburn.  2nd edition. ISBN 1259121682</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u><strong>Functions and Their Graphs</strong></u></p>
                                 
                                 <p><em>Rectangular Coordinates</em><br>
                                    Plot points in the Cartesian plane.<br>
                                    Use the Distance Formula to find the distance between two points.<br>
                                    Use the Midpoint Formula to find the midpoint of a line segment.<br>
                                    Use a coordinate plane and geometric formulas to model and solve real-life problems.<br>
                                    <em>Graphs of Equations</em><br>
                                    Sketch graphs of equations.<br>
                                    Find x- and y-intercepts of graphs of equations.<br>
                                    Use symmetry to sketch graphs of equations.<br>
                                    Find equations of and sketch graphs of circles.<br>
                                    Use graphs of equations in solving real-life problems.<br>
                                    <em>Linear Equations in Two Variables</em><br>
                                    Use slope to graph linear equations in two variables.<br>
                                    Find slopes of lines.<br>
                                    Write linear equations in two variables.<br>
                                    Use slope to identify parallel and perpendicular lines.<br>
                                    Use slope and linear equations in two variables to model and solve real-life problems.<br>
                                    <em>Functions</em><br>
                                    Determine whether relations between two variables are functions.<br>
                                    Use function notation and evaluate functions.<br>
                                    Find the domains of functions.<br>
                                    Use functions to model and solve real-life problems.<br>
                                    Evaluate difference quotients.<br>
                                    <em>Analyzing Graphs of Functions</em><br>
                                    Use the Vertical Line Test for functions.<br>
                                    Find the zeros of functions.<br>
                                    Determine intervals on which functions are increasing or decreasing and determine
                                    relative maximum and relative minimum values of functions.<br>
                                    Determine the average rate of change of a function.<br>
                                    Identify even and odd functions.<br>
                                    <em>A Library of Parent Functions</em><br>
                                    Identify and graph linear and squaring functions.<br>
                                    Identify and graph cubic, square root, and reciprocal functions.<br>
                                    Identify and graph step and other piecewise-defined functions.<br>
                                    Recognize graphs of parent functions<br>
                                    <em>Transformations of Functions</em><br>
                                    Use vertical and horizontal shifts to sketch graphs of functions.<br>
                                    Use reflections to sketch graphs of functions.<br>
                                    Use non-rigid transformations to sketch graphs of functions.<br>
                                    <em>Combinations of Functions: Composite Functions</em><br>
                                    Add, subtract, multiply, and divide functions.<br>
                                    Find the composition of one function with another function.<br>
                                    Use combinations and compositions of functions to model and solve real-life problems.<br>
                                    <em>Inverse Functions</em><br>
                                    Find inverse functions informally and verify that two functions are inverse functions
                                    of each other.<br>
                                    Use graphs of functions to determine whether functions have inverse functions.<br>
                                    Use the Horizontal Line Test to determine if functions are one-to-one.<br>
                                    Find inverse functions algebraically.<br>
                                    <em>Mathematical Modeling and Variations</em><br>
                                    Use mathematical models to approximate sets of data points.<br>
                                    Use the regression feature of a graphing utility to find the equation of a least squares
                                    regression line.<br>
                                    Write mathematical models for direct variation.<br>
                                    Write mathematical models for direct variation as an nth power.<br>
                                    Write mathematical models for inverse variation.<br>
                                    Write mathematical models for joint variation.<br>
                                    
                                 </p>
                                 
                                 <p><u><strong>Polynomial and Rational Functions</strong></u></p>
                                 
                                 <p><em>Quadratic Functions and Models</em><br>
                                    Analyze graphs of quadratic functions.<br>
                                    Write quadratic functions in standard form and use the results to sketch graphs of
                                    functions.<br>
                                    Use quadratic functions to model and solve real-life problems.<br>
                                    <em>Polynomial Functions of Higher Degree</em><br>
                                    Use transformations to sketch graphs of polynomial functions.<br>
                                    Use the Leading Coefficient Test to determine the end behavior of graphs of polynomial
                                    functions.<br>
                                    Find and use zeros of polynomial functions as sketching aids.<br>
                                    Use the Intermediate Value Theorem to help locate zeros of polynomial functions.<br>
                                    <em>Complex Numbers</em><br>
                                    Use the imaginary unit i to write complex numbers.<br>
                                    Add, subtract, and multiply complex numbers.<br>
                                    Use complex conjugates to write the quotient of two complex numbers in standard form.<br>
                                    Find complex solutions of quadratic equations.<br>
                                    <em>Zeros of Polynomial Functions</em><br>
                                    Use the Fundamental Theorem of Algebra to determine the number of zeros of polynomial
                                    functions.<br>
                                    Find rational zeros of polynomial functions.<br>
                                    Find conjugate pairs of complex zeros.<br>
                                    Find zeros of polynomials by factoring.<br>
                                    Use Descartes' Rule of Signs and the Upper and Lower Bound Rules to find zeros of
                                    polynomials.<br>
                                    <em>Rational Functions</em><br>
                                    Find the domains of rational functions.<br>
                                    Find the horizontal and vertical asymptotes of graphs of rational functions.<br>
                                    Analyze and sketch graphs of rational functions.<br>
                                    Sketch graphs of rational functions that have slant asymptotes.<br>
                                    Use rational functions to model and solve real-life problems.<br>
                                    <em>Nonlinear Inequalities</em><br>
                                    Solve polynomial inequalities.<br>
                                    Solve rational inequalities.<br>
                                    Use inequalities to model and solve real-life problems.
                                 </p>
                                 
                                 <p><u><strong>Exponential and Logarithmic Functions</strong></u><br>
                                    <em>Exponential Functions and Their Graphs</em><br>
                                    Recognize and evaluate exponential functions with base a.<br>
                                    Graph exponential functions and use the One-to-One Property.<br>
                                    Recognize, evaluate, and graph exponential functions with base .<br>
                                    Use exponential functions to model and solve real-life problems.<br>
                                    <em>Logarithmic Functions and Their Graphs</em><br>
                                    Recognize and evaluate logarithmic functions with base a.<br>
                                    Graph logarithmic functions.<br>
                                    Recognize, evaluate, and graph natural logarithmic functions.<br>
                                    Use logarithmic functions to model and solve real-life problems.<br>
                                    <em>Properties of Logarithms</em><br>
                                    Use the change-of-base formula to rewrite and evaluate or rewrite logarithmic expressions.<br>
                                    Use properties of logarithms to expand or condense logarithmic expressions.<br>
                                    Use logarithmic functions to model and solve real-life problems.<br>
                                    <em>Exponential and Logarithmic Equations</em><br>
                                    Solve simple exponential and logarithmic equations.<br>
                                    Solve more complicated exponential equations.<br>
                                    Solve more complicated logarithmic equations.<br>
                                    Use exponential and logarithmic equations to model and solve real-life problems.<br>
                                    Exponential and Logarithmic Models<br>
                                    Recognize the five most common types of models involving exponential and logarithmic
                                    functions.<br>
                                    Use exponential growth and decay functions to model and solve real-life problems.<br>
                                    Use logistic growth functions to model and solve real-life problems.<br>
                                    Use logarithmic functions to model and solve real-life problems.
                                 </p>
                                 
                                 <p><u><strong>Systems of Equations and Inequalities</strong></u></p>
                                 
                                 <p><em>Linear and Nonlinear Systems of Equations</em><br>
                                    Use the method of substitution to solve systems of linear equations in two variables.<br>
                                    Use the method of substitution to solve systems of nonlinear equations in two variables.<br>
                                    Use a graphical approach to solve systems of equations in two variables.<br>
                                    Use systems of equations to model and solve real-life problems.<br>
                                    <em>Two-Variable Linear Systems</em><br>
                                    Use the method of elimination to solve systems of linear equations in two variables.<br>
                                    Interpret graphically the numbers of solutions of systems of linear equations in two
                                    variables.<br>
                                    Use systems of linear equations in two variables to model and solve real-life problems.<br>
                                    <em>Multivariable Linear Systems</em><br>
                                    Use back-substitution to solve linear systems in row-echelon form.<br>
                                    Use Gaussian elimination to solve systems of linear equations.<br>
                                    Solve non-square systems of linear equations.<br>
                                    Use systems of linear equations in three or more variables to model and solve real-life
                                    problems.<br>
                                    <em>Partial Fractions</em><br>
                                    Recognize partial fraction decompositions of rational expressions.<br>
                                    Find partial fraction decompositions of rational expressions.<br>
                                    <em>Systems of Inequalities</em><br>
                                    Sketch the graphs of inequalities in two variables.<br>
                                    Solve systems of inequalities.<br>
                                    Use systems of inequalities in two variables to model and solve real-life problems.
                                 </p>
                                 
                                 <p><u><strong>Matrices and Determinants</strong></u></p>
                                 
                                 <p><em>Matrices and Systems of Equations</em><br>
                                    Write matrices and identify their orders.<br>
                                    Perform elementary row operations on matrices.<br>
                                    Use matrices and Gaussian elimination to solve systems on linear equations.<br>
                                    Use matrices and Gauss-Jordan elimination to solve systems of linear equations.<br>
                                    <em>Operations with Matrices</em><br>
                                    Decide whether two matrices are equal.<br>
                                    Add and subtract matrices and multiply matrices by scalars.<br>
                                    Multiply two matrices.<br>
                                    Use matrix operations to model and solve real-life problems.<br>
                                    <em>The Inverse of a Square Matrix</em><br>
                                    Verify that two matrices are inverses of each other.<br>
                                    Use Gauss-Jordan elimination to find the inverses of matrices.<br>
                                    Use a formula to find the inverses of 2 x 2 matrices.<br>
                                    Use inverse matrices to solve systems of linear equations.<br>
                                    <em>Applications of Matrices and Determinants</em><br>
                                    Use Cramer's Rule to solve systems of linear equations.<br>
                                    Use determinants to find the areas of triangles.<br>
                                    Use a determinant to test for collinear points and find an equation of a line passing
                                    through two points.<br>
                                    Use matrices to encode and decode messages.
                                 </p>
                                 
                                 <p><u><strong>Sequences, Series, and Probability</strong></u><br>
                                    <em>Sequences and Series</em><br>
                                    Use sequence notation to write the terms of sequences.<br>
                                    Use factorial notation.<br>
                                    Use summation notation to write sums.<br>
                                    Find the sums of infinite series.<br>
                                    Use sequences and series to model and solve real-life problems.<br>
                                    <em>Arithmetic Sequences and Partial Sums</em><br>
                                    Recognize, write, and find the nth terms of arithmetic sequences.<br>
                                    Find nth partial sums of arithmetic sequences.<br>
                                    Use arithmetic sequences to model and solve real-life problems.<br>
                                    <em>Geometric Sequences and Series</em><br>
                                    Recognize, write, and find the nth terms of geometric sequences.<br>
                                    Find nth partial sums of geometric sequences.<br>
                                    Find the sum of an infinite geometric series.<br>
                                    Use geometric sequences to model and solve real-life problems.<br>
                                    <em>Mathematical Induction</em><br>
                                    Use mathematical induction to prove statements involving a positive integer n.<br>
                                    Recognize patterns and write the nth term of a sequence.<br>
                                    Find the sums of powers of integers.<br>
                                    Find finite differences of sequences.<br>
                                    <em>The Binomial Theorem</em><br>
                                    Use the Binomial Theorem to calculate binomial coefficients.<br>
                                    Use Pascal's Triangle to calculate binomial coefficients.<br>
                                    Use binomial coefficients to write binomial expansions.
                                 </p>
                                 
                                 <p><u><strong>Introduction to Conics</strong></u><br>
                                    <em>Parabolas</em><br>
                                    Recognize a conic as the intersection of a plane and a double-napped cone.<br>
                                    Write equations of parabolas in standard form and graph parabolas.<br>
                                    Use the reflective property of parabolas to solve real-life problems.<br>
                                    <em>Ellipses</em><br>
                                    Write equations of ellipses in standard form and graph ellipses.<br>
                                    Use properties of ellipses to model and solve real-life problems.<br>
                                    Find eccentricities of ellipses.<br>
                                    <em>Hyperbolas</em><br>
                                    Write equations of hyperbolas in standard form.<br>
                                    Find asymptotes of and graph hyperbolas.<br>
                                    Use properties of hyperbolas to solve real-life problems.<br>
                                    Classify conics from their general equations.
                                 </p>
                                 
                                 <p><u><strong>Introduction to Limits</strong></u><br>
                                    <em>Techniques for Evaluating Limits</em><br>
                                    Use the dividing out technique to evaluate limits of functions.<br>
                                    Use the rationalizing technique to evaluate limits of functions.<br>
                                    Approximate limits of functions graphically and numerically.<br>
                                    Evaluate one-sides limits of functions.<br>
                                    Evaluate limits of difference quotients from calculus.<br>
                                    <em>The Tangent Line Problem</em><br>
                                    Use a tangent line to approximate the slope of a graph at a point.<br>
                                    Use the limit definition of slope to find exact slopes of graphs.<br>
                                    Find derivatives to find slopes of graphs.<br>
                                    <em>Limits at Infinity and Limits of Sequences</em><br>
                                    Evaluate limits of functions at infinity.<br>
                                    Find limits of sequences.<br>
                                    <em>The Area Problem</em><br>
                                    Find limits of summations<br>
                                    Use rectangles to approximate areas of plane regions.<br>
                                    Use limits of summations to find areas of plane regions.
                                 </p>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 8.2, 8.3, 8.4 <br>
                                    <u>Week 2</u>: 8.3, 8.5 <br>
                                    <u>Week 3</u>: 8.5, 8.6, 8.7, 8.8 <br>
                                    <u>Week 4</u>: 9.2, Test 1 <br>
                                    <u>Week 5</u>: 9.2, 9.3, 9.4 <br>
                                    <u>Week 6</u>: 9.5, 10.1, 10.2 <br>
                                    <u>Week 7</u>: 10.3, Test 2 <br>
                                    <u>Week 8</u>: 10.7, 11.1, 11.2, 11.3 <br>
                                    <u>Week 9</u>: 3.2, 3.3, 3.4 <br>
                                    <u>Week 10</u>: 3.5, Test 3 <br>
                                    <u>Week 11</u>: 3.6, 3.7 <br>
                                    <u>Week 12</u>: 4.1, 4.2, 4.3 <br>
                                    <u>Week 13</u>: 4.4, Test 4 <br>
                                    <u>Week 14</u>: 4.5, 10.4 <br>
                                    <u>Week 15</u>: Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>STA2023 - Statistical Methods</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:azainulabdeen@valenciacollege.edu">Wakeel Zainulabdeen</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>8-202</div>
                                          
                                          <div>2343</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/STA2023.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/Summer_STA2023.docx">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Stats: Modeling the World" by David E. Bock, Paul F. Velleman &amp; Richard D. De Veaux.
                                    4th edition. ISBN 0-321-85401-2
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Summarizing Data Graphically</u><br>
                                    - Bar Chart / Pie Chart<br>
                                    - Dot Plot<br>
                                    - Histogram<br>
                                    - Stem- and- Leaf<br>
                                    - Boxplot / modified Boxplot<br>
                                    - Describe Shape, Center and Shape<br>
                                    - Time Series (Optional)<br>
                                    <u>Summarizing Data Numerically</u><br>
                                    - Quartiles, Percentiles<br>
                                    - Standard Deviation/Variance<br>
                                    - Range<br>
                                    - Inner Quartile Range<br>
                                    - Median Mean Mode<br>
                                    - Z-score<br>
                                    - Normal Model for Data<br>
                                    - 68%-95%-99.7% Rule (Empirical Rule) <br>
                                    - Chebyshev's Theorem (Optional)<br>
                                    - Rule of Thumb Estimate of SD (Optional)<br>
                                    <u>Statistical Design</u><br>
                                    - Sampling techniques<br>
                                    - Statistical Experiments and Observational Studies<br>
                                    - Using Randomization<br>
                                    <u>Probability</u><br>
                                    - Relative Frequency Interpretation<br>
                                    - Random Variables Independence Normal Distribution<br>
                                    - Sampling Distributions Means and Proportions<br>
                                    - Central Limit Theorem<br>
                                    - Contingency Tables<br>
                                    <u>Statistical Inference for Single Population Means and Proportions</u><br>
                                    - Confidence Intervals<br>
                                    - Margin of Error / Sample Size<br>
                                    - Hypothesis Testing<br>
                                    - Interpret Conclusions of Statistical Inference in Context<br>
                                    <u>Statistical Inference for Other Parameters</u><br>
                                    - Difference in Population Means<br>
                                    - Difference in Population Proportions<br>
                                    - Chi-Square Test for Independence (Optional) <br>
                                    - Chi-Square Goodness of Fit Test (Optional)<br>
                                    <u>Bivariate Data</u><br>
                                    - Scatterplots<br>
                                    - Linear Regression <br>
                                    - Correlation Coefficient <br>
                                    - Interpreting R^2<br>
                                    <u>Review Prerequisites</u><br>
                                    - Interpret slope and y-intercept<br>
                                    - Equation of lines<br>
                                    - Percent and Proportion<br>
                                    - Order of Operations<br>
                                    - Number sense - comparing decimals<br>
                                    - Scientific Notation<br>
                                    - Solving literal and linear equations<br>
                                    - Reading, writing, and comprehension<br>
                                    - Experience with problems in context
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>Chapter </p>
                                          </div>
                                          
                                          <div>
                                             <p>Suggested problems</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>D</strong><strong>a</strong><strong>i</strong><strong>ly Assignments</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>2 </p>
                                          </div>
                                          
                                          <div>
                                             <p>23, 31</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>2</strong><strong>4</strong><strong>, 26, 28, 32</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>3 </p>
                                          </div>
                                          
                                          <div>
                                             <p>13, 19</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>4</strong><strong>, 20, 38</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>4 </p>
                                          </div>
                                          
                                          <div>
                                             <p>25, 32</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>2</strong><strong>6</strong><strong>, 34</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>5 </p>
                                          </div>
                                          
                                          <div>
                                             <p>12, 29, 41, 43</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>4</strong><strong>, 30, 42, 44</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>12 </p>
                                          </div>
                                          
                                          
                                          <div>
                                             <p>Read, submit worksheet</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p><strong>Test-1</strong> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>D</strong><strong>a</strong><strong>te:</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>7 </p>
                                          </div>
                                          
                                          <div>
                                             <p>17, 47</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>8</strong><strong>, 46, 48</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>13 </p>
                                          </div>
                                          
                                          
                                          <div>
                                             <p>Read, submit worksheet</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>17 </p>
                                          </div>
                                          
                                          <div>
                                             <p>11, 13, 21, 28, 55, 57</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>2</strong><strong>, 14, 20, 32, 54(a,c,d), 56, 58</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p><strong>Test-2</strong> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>D</strong><strong>a</strong><strong>te:</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>18 </p>
                                          </div>
                                          
                                          <div>
                                             <p>13, 17, 29</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>4</strong><strong>, 22, 24, 30</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>19 </p>
                                          </div>
                                          
                                          <div>
                                             <p>1,2, 13, 26</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>4</strong><strong>, 16, 22, 24</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>21 </p>
                                          </div>
                                          
                                          <div>
                                             <p>13, 15</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>4</strong><strong>, 16</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p><strong>Test-3</strong> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>D</strong><strong>a</strong><strong>te:</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>22 </p>
                                          </div>
                                          
                                          <div>
                                             <p>17, 19, 35, 45(b)</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>8</strong><strong>, 20, 36, 380</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>23 </p>
                                          </div>
                                          
                                          <div>
                                             <p>15, 17</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>1</strong><strong>6</strong><strong>, 18</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p>24 </p>
                                          </div>
                                          
                                          <div>
                                             <p>23, 27</p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>2</strong><strong>4</strong><strong>, 26</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                          <div>
                                             <p><strong>F</strong><strong>inal Exam</strong> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><strong>D</strong><strong>a</strong><strong>t</strong><strong>e:</strong> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAC2233 - Business Calculus</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:koverhiser@valenciacollege.edu">Sidra Van De Car </a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>6-227</div>
                                          
                                          <div>2032</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/mac2233.pdf">Fall/Spring</a></div>
                                          
                                          <div>Summer</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Brief Calculus for the Business, Social, and Life Sciences" by Bill Armstrong and
                                    Don Davis.  3rd edition. ISBN 978-1284036282
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 3</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAC2311 - Calculus I</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:jdevoe@valenciacollege.edu">Jody DeVoe</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>4-239</div>
                                          
                                          <div>2305</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/mac2311.pdf">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_mac2311.doc">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Single Variable Calculus, Volume 1" by James Stewart.  7th edition. ISBN 0-538-49784-X</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Chapter 1</u>: 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.8<br>
                                    <u>Chapter 2</u>: 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.8<br>
                                    <u>Chapter 3</u>: 3.1, 3.3, 3.4, 3.7, 3.9<br>
                                    <u>Chapter 4</u>: 4.1, 4.2, 4.3, 4.4, 4.5<br>
                                    <u>Chapter 5</u>: 5.1, 5.2, 5.3<br>
                                    <u>Supplement</u>: Exponential Functions
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 1.1, 1.2, 1.3, 1.4, 2.1<br>
                                    <u>Week 2</u>: 2.1, 2.2, 2.3<br>
                                    <u>Week 3</u>: Exam 1<br>
                                    <u>Week 4</u>: 1.5, 1.6, 1.8, 2.3<br>
                                    <u>Week 5</u>: 2.3, 2.4, 2.5<br>
                                    <u>Week 6</u>: 2.4, 2.5, 2.8<br>
                                    <u>Week 7</u>: 2.6, 2.8, Exponential Functions<br>
                                    <u>Week 8</u>: 3.9, Exam 2<br>
                                    <u>Week 9</u>: 4.1, 4.2, 4.3, 4.4<br>
                                    <u>Week 10</u>: 4.4, 4.5, 5.1<br>
                                    <u>Week 11</u>: 5.2, Exam 3<br>
                                    <u>Week 12</u>: 3.1, 3.3, 3.7<br>
                                    <u>Week 13</u>: 3.3, 3.4, 3.7<br>
                                    <u>Week 14</u>: 5.2, 5.3<br>
                                    <u>Week 15</u>: Exam 4
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>Section</div>
                                          
                                          <div>
                                             <p><span>Exe</span><span>rcises</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>1.1</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3, 11, 61, 63</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>1.2</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3, 11, 17</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>1.3</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>13</span><span>, 15, 21, 51a-b</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>1.4</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>5, 7a</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>1.5</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 5, 7, 13, 15, 17, 23, 29, 3<span>1</span></span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>1.6</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>10, 11, 17, 19, 23, 31</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>1</span><span>.8</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 19</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>2</span><span>.1</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1, 3ai, 3b, 7, 11a, 13, 17, 19, 23, 25, 27, 47b, 48, 51</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>2</span><span>.2</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3, 5, 13, 21, 29</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>2</span><span>.3</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1-13 odd, 15, 17, 23, 25, 31, 33, 35, 41, 43, 51, 55, 59, 63, 77, 91</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>2</span><span>.4</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1, 3, 5, 7, 21, 23, 25, 27, 35</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>2.5</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>7, 9, 11, 13, 29, 47, 51, 53, 55a</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>2.6</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>7</span><span>, 13, 27</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>2.8</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3, 5, 11, 15</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>3.1</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1, 7, 45, 47, 55</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>3.3</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3, 5, 7, 8, 9, 11, </span><span>21, 28, 29, 31, 41, 51</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>3.4</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 5, 9, 11, 23, 25, 33, 35, 39. 45, 53</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>3.7</span> 
                                             </p>
                                          </div>
                                          
                                          <div>
                                             <p><span>11, 13, 15, 17, <span>3</span></span><span>3</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>3.9</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 11, 15, 21, 27, 31</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>4.1</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3, 5a, 9, 11, 17</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>4.2</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3, 5c, 13, 41, 51</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>4.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>19-35 odd, 43</span><span>, 47, 55</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>4.4</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 5, 9, 11, 15, 25, 27, 47, 49, 53, 55, 59, 60</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>4.5</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>7, 9, 11, 13, 17, 21, 23, 25, 27, 29, 35, 37, 39, 53, 57, 68, 71, 73</span> 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>5.1</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1, 5, 7, </span><span>17, 23</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>5.2</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1, 3, 5, 7, 9, 19, 23, 27, 45a</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>5.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1, 3</span><span>, 5</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAC2312 - Calculus II</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:cferrer@valenciacollege.edu">Cathy Ferrer</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>1-220</div>
                                          
                                          <div>2757</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/mac2312.pdf">Fall/Spring</a></div>
                                          
                                          <div>Summer</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Single Variable Calculus, Volume 2" by James Stewart.  7th edition. ISBN 0-538-49785-8</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Chapter 6</u>: 6.2, 6.3, 6.4, 6.5, 6.6, 6.7, 6.8<br>
                                    <u>Chapter 7</u>: 7.1, 7.2, 7.3, 7.4, 7.7, 7.8<br>
                                    <u>Chapter 8</u>: 8.1, 8.2, 8.3<br>
                                    <u>Chapter 9</u>: 9.1, 9.3<br>
                                    <u>Chapter 10</u>: 10.1, 10.2, 10.3, 10.4, 10.5<br>
                                    <u>Chapter 11</u>: 11.1-11.6, 11.8-11.10
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><u>Week 1</u>: 6.2, 6.3, 6.4<br>
                                    <u>Week 2</u>: 6.5, 6.6, 6.7<br>
                                    <u>Week 3</u>: 6.8, Chapter 6 Test<br>
                                    <u>Week 4</u>: 7.1, 7.2, 7.3<br>
                                    <u>Week 5</u>: 7.4, 7.7, 7.8<br>
                                    <u>Week 6</u>: 8.1, 8.2, 8.3<br>
                                    <u>Week 7</u>: Chapter 7/8 Test, 10.1, 10.2<br>
                                    <u>Week 8</u>: 10.3, 10.4<br>
                                    <u>Week 9</u>: 10.5, Chapter 10 Test<br>
                                    <u>Week 10</u>: 11.1, 11.2, 11.3, 11.4<br>
                                    <u>Week 11</u>: 11.5, 11.6, 11.8<br>
                                    <u>Week 12</u>: 11.9, 11.10<br>
                                    <u>Week 13</u>: 11.10, Chapter 11 Test<br>
                                    <u>Week 14</u>: 9.1, 9.3, Review<br>
                                    <u>Week 15</u>: Final Exam
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>Sec</span><span>ti<span>on</span></span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>Exe</span><span>rcises</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>6</span><span>.2</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>31</span><span>,33, 39, 41, 43, 51, 53, 65, 69, 81, 83, 87, 89, 91, 97</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>6</span><span>.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>,9, 13, 27, 37, 65</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>6</span><span>.4</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 5, 7, 11, 17, 21, 25, 27, 37, 43, 45, 49, 53, 71, 73, 77, 79, 81</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>6</span><span>.5</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 9</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>6</span><span>.6</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 5, 7, 13, 23, 25, 29, 3<span>1</span>, 49, 59, 61, 63, 67</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>6</span><span>.7</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 31, 33, 35, 37, 57, 59, 61</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>6</span><span>.8</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>7</span><span>, 17, 25, 33, 35, 39, 41, 45, 57,61, 65</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>7</span><span>.1</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 5, 7, 11, 13, 15, 17, 23, 25, 27, 29, 69</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>7</span><span>.2</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 5, 7, 21, 23, 29, 35</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>7</span><span>.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 5, 9, 13, 21, 25</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>7</span><span>.4</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>7</span><span>, 9, 17, 19, 23</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>7</span><span>.7</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 7, 29, 37</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>7</span><span>.8</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>5</span><span>, 9, 13, 17, 21, 25, 29, 37, 63</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>8</span><span>.1</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 7, 9, 13, 19, 23, 37</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>8</span><span>.2</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>5</span><span>, 9, 17</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>8</span><span>.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>21</span><span>, 23, 25, 27, 29, 31</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>9</span><span>.1</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3a, 5</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>9</span><span>.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 7, 11, 13</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>10</span><span>.1</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 5, 7, 9, 11, 13, 19, 21</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>10</span><span>.2</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 7, 9, 13 <span>(</span>onlydy/dx),17, 19, 21, 25, 37, 39</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>10</span><span>.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 5, 11, 15, 17, 21, 23, 25, 29, 31, 35, 39, 55, 57, 63, 69</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>10</span><span>.4</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 5, 9, 13, 17, 21, 27, 29, 49</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>10</span><span>.5</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 5, 13, 15, 19, 21, 25, 27, 29, 31, 33, 37, 43, 45</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.1</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 7, 9, 13, 23, 25, 27, 41, 47, 55, 57, 59, 61, 73</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.2</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>9</span><span>, 15, 17, 21, 23, 25, 31, 57</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.3</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 5, 9, 11, 13</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.4</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 5, 7, 17</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.5</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 5, 7, 11</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.6</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>1</span><span>, 3, 7, 9, 13, 15, 17, 21</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.8</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 7, 11, 15, 19, 23, 27</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.9</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>3</span><span>, 5, 7, 13a,25</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <p><span>11</span><span>.10</span></p>
                                          </div>
                                          
                                          <div>
                                             <p><span>5</span><span>, 7, 13, 19, 29, 31, 33, 39, 41, 47, 49, 63, 67, 69</span></p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAC2313 - Calculus III</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:pfernandez@valenciacollege.edu">Paul Fernandez</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>4-241</div>
                                          
                                          <div>2755</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAC2313.docx">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_mac2313.doc">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Calculus:  Mulitvariable" by William G. McCallum, Deborah Hughes-Hallett, Andrew
                                    M. Gleason &amp; David O. Lomen.  6th edition. ISBN 978-0470888674
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 3</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAP2302 - Differential Equations</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div>
                                                <strong>*New Instructors*</strong> - Please contact the <a href="mailto:jdevoe@valenciacollege.edu">course chair</a> before teaching this course in order to obtain additional information
                                             </div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:jdevoe@valenciacollege.edu">Jody DeVoe</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>4-239</div>
                                          
                                          <div>2305</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/map2302.pdf">Fall/Spring</a></div>
                                          
                                          <div><a href="documents/summer_map2302.doc">Summer</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Schaum's Outline of Differential Equations" by Richard Bronson &amp; Gabriel Costa. 
                                    4th edition. ISBN 978-0071824859. 
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>First Order DE and Applications <br> 
                                    Higher Order Linear DE and Applications <br> 
                                    Laplace Transform Method
                                    
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAE2801 - Elementary School Math</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:asaxman@valenciacollege.edu">Amanda Saxman</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>8-201</div>
                                          
                                          <div>2865</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/2801webmusser.doc">Fall/Spring</a></div>
                                          
                                          <div>Summer</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"A Problem Solving Approach to Math for Elementary School Teachers" by Rick Billstein,
                                    Shlomo Libeskind &amp; Johnny W. Lott.  11th edition. ISBN 0-321-84313-4      
                                 </p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 3</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MHF2300 - Logic &amp; Proof in Mathematics</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="mailto:koverhiser@valenciacollege.edu">Kurt Overhiser</a><a href="mailto:ashaw17@valenciacollege.edu"></a>
                                             
                                          </div>
                                          
                                          <div>8-206</div>
                                          
                                          <div>2481</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="documents/2300websundstrom.doc">Fall/Spring</a></div>
                                          
                                          <div>Summer</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>" "</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 3</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <div><strong>MAS2103 - Introduction to Linear Algebra</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Chair</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong><u>Name:</u></strong></div>
                                          
                                          <div><strong><u>Office:</u></strong></div>
                                          
                                          <div><strong><u>Extension: </u></strong></div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div><a href="mailto:ashaw17@valenciacollege.edu">Agatha Shaw</a></div>
                                          
                                          <div>8-249</div>
                                          
                                          <div>2117</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Syllabus</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><a href="http://preview.valenciacollege.edu/academics/departments/math-east/documents/MAS2103_201420.docx">Fall/Spring</a></div>
                                          
                                          <div>Summer</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Textbook</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>"Elementary Linear Algebra" by Howard Anton.  10th edition. ISBN 978-0470458211</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Topic List/Sections to Cover</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 3</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Course Timeline</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p>Content 4</p>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div><strong>Suggested Homework</strong></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-east/facultyresources.pcf">©</a>
      </div>
   </body>
</html>