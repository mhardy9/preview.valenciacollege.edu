<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty | East Campus Math Department | Valencia College</title>
      <meta name="Description" content="Faculty | East Campus Math Department">
      <meta name="Keywords" content="college, school, educational, faculty, east, math">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-east/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-east/">Math East</a></li>
               <li>Faculty</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Faculty&nbsp;</h2>
                        
                        <div>
                           
                           <div>
                              
                              <table class="table ">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <th width="50px">&nbsp;</th>
                                       
                                       <th width="200px">Name</th>
                                       
                                       <th width="300px">Website</th>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="/academics/departments/math-east/images/_cfimg-7933755235771988985.png" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jmccormick5">Jennifer Adams</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?jmccormick5" target="_blank">http://frontdoor.valenciacollege.edu?jmccormick5</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jberman">Joel Berman</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?jberman" target="_blank">http://frontdoor.valenciacollege.edu?jberman</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=hboustique">Hatim Boustique</a></td>
                                       
                                       <td><a href="http://faculty.valenciacollege.edu/hboustique/" target="_blank">http://faculty.valenciacollege.edu/hboustique/</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=lcohen5">Lisa Cohen</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=adasser">Abdellatif Dasser</a></td>
                                       
                                       <td><a href="http://faculty.valenciacollege.edu/adasser/" target="_blank">http://faculty.valenciacollege.edu/adasser/</a><span>&nbsp;</span><br><a href="http://frontdoor.valenciacollege.edu/?adasser" target="_blank">http://frontdoor.valenciacollege.edu?adasser</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jdevoe">Jody DeVoe</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?jdevoe" target="_blank">http://frontdoor.valenciacollege.edu?jdevoe</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=adixon30">Anthony Dixon</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?adixon30" target="_blank">http://frontdoor.valenciacollege.edu?adixon30</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ielhaitami">Ibrahim El Haitami</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?ielhaitami" target="_blank">http://frontdoor.valenciacollege.edu?ielhaitami</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="/academics/departments/math-east/images/_cfimg-6293225782433463931.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cferrer">Cathy Ferrer</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?cferrer" target="_blank">http://frontdoor.valenciacollege.edu?cferrer</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=pflores">Paul Flores</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=sfrancis17">Steve Francis</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?sfrancis17" target="_blank">http://frontdoor.valenciacollege.edu?sfrancis17</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jfranklin">Jim Franklin</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jgeil">Joe Geil</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="/academics/departments/math-east/images/_cfimg-9123486596952373351.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jguillemette">Joshua Guillemette</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?jguillemette" target="_blank">http://frontdoor.valenciacollege.edu?jguillemette</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ahammack">Alison Hammack</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?ahammack" target="_blank">http://frontdoor.valenciacollege.edu?ahammack</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dhoward">Deborah Howard</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?dhoward" target="_blank">http://frontdoor.valenciacollege.edu?dhoward</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=nhristova">Nely Hristova</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mkarkkainen">Michelle Karkkainen</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?mkarkkainen" target="_blank">http://frontdoor.valenciacollege.edu?mkarkkainen</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mkarwowski">Marjorie Karwowski</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?mkarwowski" target="_blank">http://frontdoor.valenciacollege.edu?mkarwowski</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jkiriazes">Joanne Kiriazes</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?jkiriazes" target="_blank">http://frontdoor.valenciacollege.edu?jkiriazes</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="/academics/departments/math-east/images/_cfimg6423435874041727025.PNG" alt="Faculty Photo" width="31" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jlawhon">Jennifer Lawhon</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?jlawhon" target="_blank">http://frontdoor.valenciacollege.edu?jlawhon</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="/academics/departments/math-east/images/_cfimg-332924738928462640.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dmatthews13">Donna Matthews</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?dmatthews13" target="_blank">http://frontdoor.valenciacollege.edu?dmatthews13</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jmccloskey">Jim McCloskey</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=pmurphy2">Patrick Murphy</a></td>
                                       
                                       <td><a href="http://faculty.valenciacollege.edu/pmurphy2/" target="_blank">http://faculty.valenciacollege.edu/pmurphy2/</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=aoneil1">AnnMarie O'Neil</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=koverhiser">Kurt Overhiser</a></td>
                                       
                                       <td><a href="http://faculty.valenciacollege.edu/koverhiser/" target="_blank">http://faculty.valenciacollege.edu/koverhiser/</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jphelps">Julie Phelps</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?jphelps" target="_blank">http://frontdoor.valenciacollege.edu/?jphelps</a><span>&nbsp;</span><br><a href="http://frontdoor.valenciacollege.edu/?jphelps" target="_blank">http://frontdoor.valenciacollege.edu?jphelps</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=lpotchen">Lisa Potchen</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?lpotchen" target="_blank">http://frontdoor.valenciacollege.edu?lpotchen</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=spurtee">Steve Purtee</a></td>
                                       
                                       <td>&nbsp;</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="/academics/departments/math-east/images/_cfimg5964838023833595817.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=asaxman">Amanda Saxman</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?asaxman" target="_blank">http://frontdoor.valenciacollege.edu?asaxman</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="/academics/departments/math-east/images/_cfimg1247787873163980642.PNG" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ashaw17">Agatha Shaw</a></td>
                                       
                                       <td><a href="http://faculty.valenciacollege.edu/ashaw17/" target="_blank">http://faculty.valenciacollege.edu/ashaw17/</a><span>&nbsp;</span><br><a href="http://frontdoor.valenciacollege.edu/?ashaw17" target="_blank">http://frontdoor.valenciacollege.edu?ashaw17</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ksiler">Keri Siler</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?ksiler" target="_blank">http://frontdoor.valenciacollege.edu?ksiler</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mtaylo12">Michelle Taylor</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?mtaylo12" target="_blank">http://frontdoor.valenciacollege.edu?mtaylo12</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ivandecar">Sidra Van De Car</a></td>
                                       
                                       <td><a href="http://faculty.valenciacollege.edu/ivandecar" target="_blank">http://faculty.valenciacollege.edu/ivandecar</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=svester">Sylvana Vester</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?svester" target="_blank">http://frontdoor.valenciacollege.edu?svester</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dwhittlesey">David Whittlesey</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?dwhittlesey" target="_blank">http://frontdoor.valenciacollege.edu?dwhittlesey</a></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                       
                                       <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=azainulabdeen">Abdul Zainulabdeen</a></td>
                                       
                                       <td><a href="http://frontdoor.valenciacollege.edu/?azainulabdeen" target="_blank">http://frontdoor.valenciacollege.edu?azainulabdeen</a></td>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                              </table>
                              <br>
                              
                              <div>&nbsp;</div>
                              
                           </div>
                           
                        </div>
                        
                        <h2>Committee Reports :</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Faculty Senate </strong></div>
                                 
                                 <div>Joel Berman, Cathy Ferrer, Abdul Zainulabdeen</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Supplemental Learning </strong></div>
                                 
                                 <div>Jennifer Adams</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong> UCF/Valencia Alignment</strong></div>
                                 
                                 <div>Jim McCloskey</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>STATEWIDE CURRICULUM ALIGNMENT GROUP</strong></div>
                                 
                                 <div>
                                    
                                    <p>Lisa Cohen, Paul Flores, Jim McCloskey, Sidra Van De Car</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong> Collegiality Work Group</strong></div>
                                 
                                 <div>Sidra VanDeCar</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Peace and Justice Institute</strong></div>
                                 
                                 <div>Nely Hristova</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Math Fitness Group</strong></div>
                                 
                                 <div>Steve Francis</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h4>&nbsp;</h4>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-east/faculty.pcf">©</a>
      </div>
   </body>
</html>