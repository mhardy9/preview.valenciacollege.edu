<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Math Department  | Valencia College</title>
      <meta name="Description" content="West Campus Math Department">
      <meta name="Keywords" content="college, school, educational, west, math">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>West Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Math West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <h3 class="row">Vision</h3>
                  
                  <div class="row"><span>Valencia's West Campus Mathematics Division will foster appreciation of the beauty
                        of mathematics, and will produce students accomplished in its application.</span></div>
                  
                  <h3 class="row">Values</h3>
                  
                  <div class="row">
                     
                     <table class="table ">
                        
                        <tbody>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value personal interaction with learners as a means of assisting them technically
                                    and encouraging them in their work.</span></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value sensitivity to the individual styles of learners.</span></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value the cognitive processes required to learn mathematical problem solving.</span></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value the growth provided by working with concepts that are different and challenging.</span></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value the discipline and perseverance it takes to learn as a lifelong process.</span></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value a sense of community within the department and the college.</span></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value accessibility for learners to the resources of the department.</span></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td class="style1" valign="top"><span>We value on-going feedback and assessment of the department's effectiveness in meeting
                                    the needs of Valencia's learning community.</span></td>
                              
                           </tr>
                           
                        </tbody>
                        
                     </table>
                     
                  </div>
                  
                  <h3 class="row">Mission</h3>
                  
                  <table class="table ">
                     
                     <tbody>
                        
                        <tr>
                           
                           <td class="style1" valign="top">The mission of the West Campus Division of Mathematics is to focus on learning to
                              provide excellent instruction and support for students.
                           </td>
                           
                        </tr>
                        
                     </tbody>
                     
                  </table>
                  
                  <p><a href="http://valenciacollege.edu/west/math/documents/AcademicCalendar20152016.pdf">View the academic calendar for academic year 2015/2016.</a></p>
                  
                  <p><a href="http://valenciacollege.edu/calendar/documents/16-17approvedbyCSC9-21-15.pdf">View the academic calendar for academic year 2016/2017.</a></p>
                  
                  <p><a href="http://valenciacollege.edu/calendar/documents/IDCFall-Spring-Summer2015-16COMBINED11-16-15.pdf">View the Important Dates and Deadlines calendar for 2015/2016.</a></p>
                  
                  <p><a href="http://valenciacollege.edu/calendar/documents/FallSpringSummer16-17ImportantDates-rev5-11-16.pdf">View the Important Dates and Deadlines calendar for 2016/2017.</a></p>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-west/index.pcf">©</a>
      </div>
   </body>
</html>