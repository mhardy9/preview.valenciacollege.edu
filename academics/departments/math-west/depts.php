<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Contacts | Math | West Campus | Valencia College</title>
      <meta name="Description" content="Contacts | Math | West Campus">
      <meta name="Keywords" content="college, school, educational, math, west, contacts">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-west/depts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>West Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-west/">Math West</a></li>
               <li>Contacts</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <p><a id="content" name="content"></a></p>
                        
                        <h2>Location, Phone and Address</h2>
                        
                        <p>West Campus, 7-108</p>
                        407-582-1625 voice<br>
                        
                        <p>407-582-1848 voice<br> 407-582-1856 fax
                        </p>
                        
                        <p>1800 South Kirkman Road<br> Mail Code 4-23 <br> Orlando FL 32811-2302
                        </p>
                        
                        <h4>Administration</h4>
                        
                        <p><a href="mailto:pblankenship1@valenciacollege.edu">Paul Blankenship</a>, Dean of Mathematics
                        </p>
                        
                        <p><a href="mailto:hfairfax@valenciacollege.edu">Heather Fairfax</a>, Administrative Assistant to the Dean
                        </p>
                        
                        <p><a href="mailto:cbelinmortera@valenciacollege.edu">Christine Belin-Mortera</a>, Administrative Assistant to the Dean
                        </p>
                        
                        <h4>College Level Math Program</h4>
                        
                        <p><a href="mailto:bnguyen@valenciacollege.edu">Boris Nguyen </a>, Program Director, College Level Mathematics
                        </p>
                        
                        <h4>Developmental Math Program</h4>
                        
                        <p><a href="mailto:cgenovesemartinez@valenciacollege.edu">Claudia Genovese-Martinez </a>, Program Director, Developmental Mathematics
                        </p>
                        
                        <h4>Math Center, and Math Labs</h4>
                        
                        <p><a href="mailto:abatra@valenciacollege.edu">Aditi Batra</a>, Senior Instructional Assistant, Math Labs
                        </p>
                        
                        <p><a href="mailto:nnavarro1@valenciacollege.edu">Nick Navarro</a>, Senior Instructional Assistant, Math Labs
                        </p>
                        
                        <p><a href="mailto:cwatson17@valenciacollege.edu">Courtney Watson </a>, Hands-On Math Resource Area
                        </p>
                        
                        <p><a href="mailto:tgallagher@valenciacollege.edu">Teresa Gallagher</a>, Administrative Support Specialist, Tutoring
                        </p>
                        
                        <p><a href="mailto:adagiau@valenciacollege.edu">Amanda DaGiau</a>, Tutorial Center Support Specialist
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-west/depts.pcf">©</a>
      </div>
   </body>
</html>