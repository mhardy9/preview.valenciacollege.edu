<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Statway | Math | West Campus | Valencia College</title>
      <meta name="Description" content="Statway | Math | West Campus">
      <meta name="Keywords" content="college, school, educational, statway, math, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-west/statway/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-west/statway/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Math West</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-west/">Math West</a></li>
               <li>Statway</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>What is Statway™ ?</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div><br> Statway™ is a class that allows you to complete Developmental Math, MAT0029, and
                                 transfer-level statistics, STA2023, in 2-semesters. It is designed for students who
                                 are not STEM majors (Science, Technology, Engineering, or Math)
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>What are the benefits of Statway™ ?</h3>
                                    <br> <strong>Save time</strong>- take fewer classes and complete one math requirement for transfer.<br><strong>Save money</strong>-No expensive textbook required!
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>What are the prerequisites?</h3>
                                    <br> MAT0018C-Developmental Math I or PERT-M score 96-112
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>What majors benefit from Statway™?</h3>
                                    <br> Art - Advertising - Anthropology<br> English - Film - Graphic Design<br> History - Humanities - Journalism<br> Music - Nursing- Philosophy<br> Photography - Political Science - Psychology - Public Administration<br> Religious Studies - Sociology - Social Work<br> Spanish - Sport Science - Theatre
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>How do I sign up for Statway™?</h3>
                                    <br> The first semester course is MAT 0029, and you can register for the course or you
                                    can visit the Math Dean on your campus to register.
                                 </div>
                                 
                              </div>
                              <br>
                              
                              <div>
                                 
                                 <p><a title="East Campus Math Department" href="/academics/departments/math-east/" target="_blank">East Campus Math Department</a></p>
                                 
                                 <p><a title="West Campus Math Departments" href="/academics/departments/math-west/" target="_blank">West Campus Math Department</a></p>
                                 
                                 <p><a title="Osceola Campus Math Department" href="/academics/departments/math-osceola/" target="_blank">Osceola Campus Math Department</a></p>
                                 
                                 <!--<p><a title="Classic Catalog" href="/students/math-support-center-winter-park/" target="_blank">Winter Park Math Department</a></p>-->
                                 
                              </div>
                              
                           </div>
                           
                           <div>&nbsp;</div>
                           
                           <div>&nbsp;</div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-west/statway/index.pcf">©</a>
      </div>
   </body>
</html>