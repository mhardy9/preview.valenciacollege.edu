
<ul>
<li><a href="/academics/departments/math-west/index.php">Math Home</a></li>
<li><a href="/academics/departments/math-west/depts.php">Contacts</a></li>
	
<li class="submenu">
<a class="show-submenu" href="javascript:void(0);">Courses <i aria-hidden="true" class="far fa-angle-down"></i></a>
<ul>
<li><a href="/academics/departments/math-west/courses_descriptions.php">Course Descriptions</a></li>
	<li><a href="/academics/departments/math-west/starterkits/index.php">Course Starter Kits</a></li>
</ul>
</li>	
<li><a href="/academics/departments/math-west/faculty.php">Faculty</a></li>
<li><a href="/academics/departments/math-west/statway/index.php">Statway</a></li>
</ul>

