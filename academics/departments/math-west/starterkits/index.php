<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Starter Kits | Math | West Campus | Valencia College</title>
      <meta name="Description" content="Starter Kits | Math | West Campus">
      <meta name="Keywords" content="college, school, educational, math, west, starter, kits">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-west/starterkits/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>West Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-west/">Math West</a></li>
               <li>Starterkits</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Course Starter Kits for Faculty</h2>
                        
                        <p>Course starter kits consist of course outlines, course syllabi, assignment sheets,
                           time lines, and other reference materials. <a href="/academics/programs/math/faculty_matharea.html">Course coordinators</a> may be contacted for additional course-specific information. <a href="mailto:acomerford@valenciacollege.edu"> Amy Comerford </a> may be contacted with general questions and suggestions.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>MAT 0018C 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-0018c-cousre-outline.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAT-0018C-Syllabus-201730-H1-and-H2.docx" target="_blank">MAT 0018C H1 H2 Syllabus</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-0018c-timeline-h1-201730.pdf" target="_blank">MAT 0018C H1 Timeline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-0018c-timeline-h2-201730.pdf" target="_blank">MAT 0018C H2 Timeline</a></p>
                                    
                                    <p>This course has a department-wide final exam.</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>MAT 0022C 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-002c-course-outline.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAT-0022C-Master-Syllabus-201730.docx" target="_blank">MAT 0022C Syllabus</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-0022c-timelines-full-term-201730.pdf" target="_blank">MAT 0022C Timelines</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>MAT 0028C 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-0028c-course-outline.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAT-0028C-Syllabus-201730-H1-and-H2.docx" target="_blank">MAT 0028C H1 H2 Syllabus </a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-0028c-timeline-h1-201730.pdf" target="_blank"> H1 TimeLine </a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-0028c-timeline-h2-201730.pdf" target="_blank">H2 Timeline</a></p>
                                    
                                    <p>This course has a department-wide final exam.</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>MAT 1033C 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-course-outline-201730.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAT-1033C-Syllabus-201730.docx" target="_blank">MAT 1033C Syllabus </a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAT-1033C-Self-Diagnostic-Test.docx" target="_blank">MAT 1033C Self Diagnostic Test </a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-mw-full-timelines-201730.doc" target="_blank">MAT 1033C MW Timeline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-timelines-tr-full-201730.doc" target="_blank">MAT1033C TR Timeline </a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-h1-mtwr-timeline-201730.doc" target="_blank">MAT1033C MTWR H1 Timeline </a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-timelines-h1-tr-201730.doc" target="_blank">MAT1033C TR H1 Timeline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-timelines-mtwr-h2-201730.doc" target="_blank">MAT 1033C MTWR H2 Timeline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-lab-syllabus-spring-2017.pdf" target="_blank">MAT1033C Math Lab Syllabus</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033-lab-overviews-and-instructor-tips-fall-2016.pdf" target="_blank">MAT 1033C Lab Overviews and Instructor Tips</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mat-1033c-homework-assignments-7th-edition-with-optional-sections.doc" target="_blank">Assignments</a></p>
                                    
                                    <p>This course has a set of department-wide common questions to be incorporated in the
                                       final exam.&nbsp; Contact <a href="mailto:cbelinmortera@valenciacollege.edu">Christine Belin-Mortera</a>&nbsp; for details on securing and returning common questions.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>STA 1001C 201620</h3>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/STA-1001C-Syllabus-201620.docx" target="_blank">Syllabus</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/STA-1001C-Timeline-201620-MW.docx" target="_blank">STA 1001C MW Timeline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/STA-1001C-Timeline-201620-TR.docx" target="_blank">STA 1001C TR Timeline</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>MAC 1105 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1105-syllabus-201730.pdf" target="_blank">Syllabus </a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1105-timeline-201730.pdf" target="_blank">Timeline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-1105-Homework-Assignments-Sullivan-10th-Edition.docx" target="_blank">Assignments</a></p>
                                    
                                    <p>This course has a college-wide common question contributing to the assessment of general
                                       education program outcomes, as well as a set of department-wide common questions,
                                       all of which are to be incorporated in the final exam. The syllabus addendum above
                                       must be distributed to students to advise them of the assessment program in which
                                       they are participating. Contact <a href="mailto:cbelinmortera@valenciacollege.edu">Christine Belin-Mortera</a>&nbsp; for details on securing and returning common questions.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>MAC 1114 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1114-course-outline-builder.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1114-syllabus-201510.doc" target="_blank">Syllabus</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1114-timeline-mw-201730.pdf" target="_blank">MAC 1114 MW Timeline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-1114-Assignment.docx" target="_blank">Assignments</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>MAC 1140 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1140-course-outline-builder.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1140-syllabus-201610.doc" target="_blank">Syllabus</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-1140-timeline-201730.pdf" target="_blank">Timeline </a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-1140-Homework-201710.docx" target="_blank">Assignments</a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>MAC 2233 201730</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2233-syllabus-30186-201730.pdf" target="_blank">Syllabus (CRN 30186)</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2233-syllabus-31216-201730.pdf" target="_blank">Syllabus (CRN 31216)</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2233-timeline-201730.pdf" target="_blank">Timeline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-2233-Assignments.docx" target="_blank">Assignments</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>MAC 2311 201710</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2311-course-outline-builder.pdf" target="_blank"> Course Outline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2311-syllabus-fall2016-course-starter-kit.doc" target="_blank"> Syllabus</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-2311-Assignments-Stewart-8th-edition-ET.docx" target="_blank">Assignments</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2311-timeline-mw-fall-2016-course-starter-kit.doc" target="_blank">MAC 2311 MW Timeline </a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2311-timeline-tr-fall-2016-course-starter-kit.doc" target="_blank">MAC 2311 TR Timeline</a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>MAC 2312 201710</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2312-course-outline-builder.pdf" target="_blank"> Course Outline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2312-syllabus-course-starter-kit.doc" target="_blank">Syllabus</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2312-assignments.doc" target="_blank">Assignments</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-2312-Timeline-Fall-2016-Course-Starter-Kit.docx" target="_blank">MAC 2312 Timeline </a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>MAC 2313 201620</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2313-course-outline-builder.pdf">Course Outline</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mac-2313-syllabus-crn-10502.pdf" target="_blank">Syllabus</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-2313-Assignments.docx" target="_blank">Assignments</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MAC-2313-Timeline-201620.docx" target="_blank">MAC 2313 Timeline</a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>MAP 2302 201510</h3>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>MGF 1106 201710</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mgf-1106-course-outline.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MGF-1106-Syllabus-201710.docx" target="_blank">Syllabus</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MGF-1106-timeline-and-homework-201710.docx" target="_blank">Timeline and Homework </a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>MGF 1107 201710</h3>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/mgf-1107-course-outline.pdf" target="_blank">Course Outline</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MGF-1107-Syllabus-201710.docx" target="_blank">Syllabus</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/MGF-1107-timeline-and-homework-201710.docx" target="_blank">Timeline and Homework </a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>STA 2023 201730</h3>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/math-west/starterkits/documents/STA-2023-Syllabus-201730.docx" target="_blank"> Syllabus</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/sta-2023-timeline-2027130.pdf" target="_blank"> TimeLine</a></p>
                                    
                                    <p><a href="/academics/departments/math-west/starterkits/documents/sta-2023-assignments-201730.pdf" target="_blank">Assignments</a></p>
                                    
                                    <p>This course has a set of department-wide common questions to be incorporated in the
                                       final exam.&nbsp; Contact <a href="mailto:cbelinmortera@valenciacollege.edu">Christine Belin-Mortera</a>&nbsp; for details on securing and returning common questions.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-west/starterkits/index.pcf">©</a>
      </div>
   </body>
</html>