<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Course Descriptions | West Campus Math Department | Valencia College</title>
      <meta name="Description" content="Course Descriptions | Math | West Campus">
      <meta name="Keywords" content="college, school, educational, math, west, course, descriptions">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-west/courses_descriptions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>West Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-west/">Math West</a></li>
               <li>Course Descriptions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <h3 align="left"><strong>MATHEMATICS COURSE DESCRIPTIONS</strong></h3>
                     
                     <p align="left"><em><strong>For a complete list of Valencia's courses, see the course catalog<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/">here</a>.</strong></em></p>
                     
                     <table class="table ">
                        
                        <tbody>
                           
                           <tr>
                              
                              <td bgcolor="#FFFFFF" width="100%" height="94">
                                 
                                 <table class="table ">
                                    
                                    <tbody>
                                       
                                       <tr>
                                          
                                          <td width="703">
                                             
                                             <h3>&nbsp;</h3>
                                             
                                          </td>
                                          
                                          <td align="center" width="52">Cr</td>
                                          
                                          <td align="center" width="141">Contact</td>
                                          
                                          <td align="center" width="71">Lab</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;1105. COLLEGE ALGEBRA.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a><span>&nbsp;</span>or appropriate score on an approved assessment . Course based on the study of functions
                                                and their role in problem solving. Topics include graphing, the linear, quadratic,
                                                and exponential families of functions, and inverse functions. Students will be required
                                                to solve applied problems and communicate their findings effectively. Technology tools
                                                will be utilized in addition to analytical methods. Gordon Rule course. Minimum grade
                                                of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>is used to satisfy Gordon Rule and general education requirements.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;1105H. COLLEGE ALGEBRA - HONORS.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a><span>&nbsp;</span>or either appropriate score on an approved assessment. Course based on the study of
                                                functions and their role in problem solving. Topics include graphing, the linear,
                                                quadratic, and exponential families of functions, and inverse functions. In addition,
                                                course content will satisfy one honors program learning outcome. Students will be
                                                required to solve applied problems and communicate their findings effectively. Technology
                                                tools will be utilized in addition to analytical methods. Gordon Rule course. Minimum
                                                grade of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105H">MAC&nbsp;1105H</a><span>&nbsp;</span>is used to satisfy Gordon Rule.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;1114. COLLEGE TRIGONOMETRY.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or an appropriate score on an approved assessment. Topics include a symbolical, graphical
                                                and numerical analysis of trigonometric functions; solutions of plane triangles and
                                                vectors. Applications emphasizing connections with other disciplines and with the
                                                real world will be included. Technology tools will be utilized in addition to analytical
                                                methods. Gordon Rule course. Minimum grade of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201114">MAC&nbsp;1114</a><span>&nbsp;</span>is used to satisfy Gordon Rule and general education requirements.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;1140. PRECALCULUS ALGEGRA.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or appropriate score on an approved assessment. Algebra preparation for the calculus
                                                sequence. Topics include a symbolical, graphical, and numerical analysis of polynomials,
                                                exponential, logarithmic, power, and rational functions; matrices, sequences, induction,
                                                binomial theorem and conic sections. Applications emphasizing connections with other
                                                disciplines and with the real world will be included. Technology tools will be utilized
                                                in addition to analytical methods. Gordon Rule course. Minimum grade of C required
                                                if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201140">MAC&nbsp;1140</a><span>&nbsp;</span>is used to satisfy Gordon Rule and general education requirements.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;2233. CALCULUS FOR BUSINESS AND SOCIAL SCIENCE.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">4</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr bgcolor="#FFFFFF">
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or appropriate score on an approved assessment. Introduction to calculus with applications
                                                to business and social science. Topics include differentiation and integration of
                                                algebraic, exponential and logarithmic functions, rates of change, curve sketching,
                                                and applications of the derivative and integration. Gordon Rule course. Minimum grade
                                                of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202233">MAC&nbsp;2233</a><span>&nbsp;</span>is used to satisfy Gordon Rule and general education requirements.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;2233H. CALCULUS FOR BUSINESS AND SOCIAL SCIENCE.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">3</td>
                                          
                                          <td align="center">4</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or appropriate score on an approved assessment. Introduction to calculus with applications
                                                to business and social science. Topics include differentiation and integration of
                                                algebraic, exponential and logarithmic functions, rates of change, curve sketching,
                                                and applications of the derivative and integration. Gordon Rule course. Minimum grade
                                                of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202233">MAC&nbsp;2233</a><span>&nbsp;</span>is used to satisfy Gordon Rule and General Education requirements.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;2311. CALCULUS WITH ANALYTIC GEOMETRY I.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201140">MAC&nbsp;1140</a><span>&nbsp;</span>and<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201114">MAC&nbsp;1114</a><span>&nbsp;</span>or MAC 1147. Topics include limits and continuity, derivatives of algebraic, trigonometric,
                                                and transcendental functions, applications of derivatives, definite and indefinite
                                                integral. Gordon Rule course. Minimum grade of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202311">MAC&nbsp;2311</a><span>&nbsp;</span>is used to satisfy the Gordon Rule and general education requirements.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;2311H. CALCULUS WITH ANALYTIC GEOMETRY I - HONORS.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisites: Minimum grades of C in (<a href="http://catalog.valenciacollege.edu/search/?P=MAC%201140">MAC&nbsp;1140</a><span>&nbsp;</span>and<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201114">MAC&nbsp;1114</a>) or MAC 1147, or appropriate score on an approved assessment. Same as<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202311">MAC&nbsp;2311</a><span>&nbsp;</span>with honors content. Honors program permission required.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;2312. CALCULUS WITH ANALYTIC GEOMETRY II.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202311">MAC&nbsp;2311</a><span>&nbsp;</span>Topics include differentiation and integration, techniques of integration, conic sections,
                                                and infinite series. Gordon Rule course. Minimum grade of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202312">MAC&nbsp;2312</a><span>&nbsp;</span>is used to satisfy Gordon Rule and general education requirements.<span>&nbsp;</span></p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             
                                             <p><strong>MAC&nbsp;2312H. CALCULUS WITH ANALYTIC GEOMETRY II - HONORS.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">5</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td colspan="4">
                                             
                                             <p>Prerequisite: Minimum grade of C in&nbsp;MAC&nbsp;2311&nbsp;Same as&nbsp;MAC&nbsp;2312&nbsp;with honors content.
                                                Honors program permission required.
                                             </p>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td height="18">
                                             
                                             <p><strong>MAC&nbsp;2313. CALCULUS WITH ANALYTICAL GEOMETRY III.</strong></p>
                                             
                                          </td>
                                          
                                          <td align="center">4</td>
                                          
                                          <td align="center">4</td>
                                          
                                          <td align="center">0</td>
                                          
                                       </tr>
                                       
                                       <tr align="left" valign="top">
                                          
                                          <td colspan="4">
                                             
                                             <p align="left">Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202312">MAC&nbsp;2312</a>. Topics include polar coordinates, vectors, three dimensional analytic geometry,
                                                parametric equations, partial derivatives, multiple integration.
                                             </p>
                                             
                                             <table class="table ">
                                                
                                                <tbody>
                                                   
                                                   <tr>
                                                      
                                                      <td width="704">
                                                         
                                                         <p><strong>MAE&nbsp;2801. ELEMENTARY SCHOOL MATHEMATICS CURRICULUM AND INSTRUCTION.</strong></p>
                                                         
                                                      </td>
                                                      
                                                      <td align="center" width="51"><strong>4</strong></td>
                                                      
                                                      <td align="center" width="139"><strong>4</strong></td>
                                                      
                                                      <td align="center" width="65"><strong>0</strong></td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td colspan="4">
                                                         
                                                         <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or appropriate score on an approved assessment. Mathematics appropriate for elementary
                                                            school teacher. Topics include six basic sets of numbers, operations with whole numbers,
                                                            integers and rational numbers, learning sequences, algorithms, problem solving techniques,
                                                            error patterns, number systems, geometry, statistics, and graphing. Does not apply
                                                            toward Gordon Rule or general education requirements.
                                                         </p>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                </tbody>
                                                
                                             </table>
                                             
                                             <table class="table ">
                                                
                                                <tbody>
                                                   
                                                   <tr>
                                                      
                                                      <td width="704">
                                                         
                                                         <p><strong>MAT&nbsp;0018C. DEVELOPMENTAL MATHEMATICS I.<span>&nbsp;</span><em>H1/H2 Eight (8) WEEK COURSES OFFERED IN FALL/SPRING</em></strong></p>
                                                         
                                                      </td>
                                                      
                                                      <td align="center" width="53"><strong>3</strong></td>
                                                      
                                                      <td align="center" width="138"><strong>3</strong></td>
                                                      
                                                      <td align="center" width="64"><strong>1</strong></td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td colspan="4">
                                                         
                                                         <p>This is the first course in a college-preparatory, two-course sequence (<a href="http://catalog.valenciacollege.edu/search/?P=MAT%200018C">MAT&nbsp;0018C</a><span>&nbsp;</span>and<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%200028C">MAT&nbsp;0028C</a>) designed to prepare students for<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a>, Intermediate Algebra. This course emphasizes the fundamental mathematical operations
                                                            with applications to beginning algebra. Significant time will be devoted to connections
                                                            between mathematics and other academic disciplines and to applications outside educational
                                                            settings. Minimum grade of C required for successful completion. This course does
                                                            not apply towards mathematics requirements in general education or towards any associate
                                                            degree. (Special Fee: $59.00).<span>&nbsp;</span></p>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td>
                                                         
                                                         <p><strong>MAT&nbsp;0022C. DEVELOPMENTAL MATHEMATICS COMBINED.</strong></p>
                                                         
                                                      </td>
                                                      
                                                      <td align="center"><strong>4</strong></td>
                                                      
                                                      <td align="center"><strong>4</strong></td>
                                                      
                                                      <td align="center"><strong>1</strong></td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td colspan="4">
                                                         
                                                         <p>Pre-requisite: Appropriate score on an approved assessment. This college-preparatory
                                                            course is designed to prepare students for<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a><span>&nbsp;</span>Intermediate Algebra. Topics include sets, computations with decimals, percent, integer,
                                                            operations with rational and polynomial expressions, solving linear equations and
                                                            simplifying expressions, plan geometric figures and applications, graphic ordered
                                                            pairs and lines and determining the intercepts of lines. A minimum grade of C is required
                                                            for successful completion of this course. This course does not apply towards mathematics
                                                            requirements in general education or towards any associate degree. (Special Fee: $59.00)<span>&nbsp;</span></p>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td>
                                                         
                                                         <p><strong>MAT&nbsp;0028C. DEVELOPMENTAL MATHEMATICS II.<span>&nbsp;</span><em>H1/H2 Eight (8) WEEK COURSES OFFERED IN FALL/SPRING</em></strong></p>
                                                         
                                                      </td>
                                                      
                                                      <td align="center"><strong>3</strong></td>
                                                      
                                                      <td align="center"><strong>3</strong></td>
                                                      
                                                      <td align="center"><strong>1</strong></td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td colspan="4">
                                                         
                                                         <p>Pre-requisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%200018C">MAT&nbsp;0018C</a><span>&nbsp;</span>or appropriate score on approved assessment. This college-preparatory course is designed
                                                            to supplement the algebraic background of students prior to taking<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a><span>&nbsp;</span>Intermediate Algebra. Topics include sets, fundamental operations with polynomials,
                                                            linear equations and inequalities with applications, factoring and its use in algebra,
                                                            introduction to graphing of linear equations, introduction to radicals, and use of
                                                            calculators to enhance certain concepts. A minimum grade of C is required for successful
                                                            completion of this course. This course does not apply towards mathematics requirements
                                                            in general education or towards any associate degree. (Special Fee: $59.00).<span>&nbsp;</span></p>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td>
                                                         
                                                         <p><strong>MAT&nbsp;1033C. INTERMEDIATE ALGEBRA.</strong></p>
                                                         
                                                      </td>
                                                      
                                                      <td align="center"><strong>3</strong></td>
                                                      
                                                      <td align="center"><strong>3</strong></td>
                                                      
                                                      <td align="center"><strong>1</strong></td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td colspan="4">
                                                         
                                                         <p>Prerequisite: Minimum grade of C in MAT0022C or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%200028C">MAT&nbsp;0028C</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%200055">MAT&nbsp;0055</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%200056">MAT&nbsp;0056</a><span>&nbsp;</span>or appropriate score on an approved assessment. This course presents algebraic skills
                                                            for<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a>. Topics include linear equations and inequalities in two variables and their graphs,
                                                            systems of linear equations and inequalities, introductions to functions, factoring,
                                                            algebraic functions, rational equations, radical and rational exponents, complex numbers,
                                                            quadratic equations, scientific notation, applications of the above topics and the
                                                            communication of mathematics. Applications emphasizing connections with disciplines
                                                            and the real world will be included. This course carries general elective credit but
                                                            does not satisfy either Gordon Rule or general education requirements. (Special Fee:
                                                            $59.00).<span>&nbsp;</span></p>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td>
                                                         
                                                         <p><strong>MAT&nbsp;1905. DIRECTED INDIVIDUAL STUDIES.</strong></p>
                                                         
                                                      </td>
                                                      
                                                      <td align="center"><strong>1-4</strong></td>
                                                      
                                                      <td align="center"><strong>variable</strong></td>
                                                      
                                                      <td>&nbsp;</td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td colspan="4">
                                                         
                                                         <p>Provides opportunity to develop specific individual program-related mathematical skills
                                                            and/or concepts in individualized setting. May not be used in lieu of any mathematics
                                                            course listed in catalog. Application must be made to department office.
                                                         </p>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td>
                                                         
                                                         <p><strong>MAT&nbsp;2930. SELECTED TOPICS IN MATHEMATICS.</strong></p>
                                                         
                                                      </td>
                                                      
                                                      <td align="center"><strong>1-3</strong></td>
                                                      
                                                      <td align="center"><strong>variable</strong></td>
                                                      
                                                      <td>&nbsp;</td>
                                                      
                                                   </tr>
                                                   
                                                   <tr>
                                                      
                                                      <td colspan="4">
                                                         
                                                         <p>Prerequisite: Minimum grade of C in MAC 1102 or MAC 1104 or&nbsp;MAC&nbsp;1105&nbsp;or appropriate
                                                            score on an approved assessment. Examines selected topics in mathematics based on
                                                            historical, theoretical, application or research approach. Does not apply toward Gordon
                                                            Rule or general education requirements. Multiple credit course. May be repeated for
                                                            credit, but grade forgiveness cannot be applied.
                                                         </p>
                                                         
                                                         <table class="table ">
                                                            
                                                            <tbody>
                                                               
                                                               <tr>
                                                                  
                                                                  <td><strong>MAP 2302. DIFFERENTIAL EQUATIONS.</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>0</strong></td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202313">MAC&nbsp;2313</a><span>&nbsp;</span>or departmental approval. Introduction to methods and applications of ordinary differential
                                                                     equations. Topics include first order differential equations and applications; higher
                                                                     order linear differential equations with applications; Laplace transforms; introduction
                                                                     to numerical methods.
                                                                  </td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td>&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td><strong>MGF 1106. COLLEGE MATHEMATICS.</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>0</strong></td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%200018C">MAT&nbsp;0018C</a><span>&nbsp;</span>or higher or appropriate score on an approved assessment. Topics include systematic
                                                                     counting, probability, statistics, geometry, sets, logic, and the history of mathematics.
                                                                     Gordon Rule course. Minimum grade of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MGF%201106">MGF&nbsp;1106</a><span>&nbsp;</span>is used to satisfy Gordon Rule and general education requirements.
                                                                  </td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td>&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td><strong>MGF 1107. MATH FOR LIBERAL ARTS.</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>0</strong></td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MGF%201106">MGF&nbsp;1106</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=STA%202023">STA&nbsp;2023</a><span>&nbsp;</span>or appropriate score on an approved assessment. This course covers topics chosen from
                                                                     problem solving, numeration and mathematical systems, financial mathematics, voting
                                                                     techniques and apportionment, chaos theory, graphy theory, knot theory, tilings and
                                                                     polyhedra, game theory, number theory, connections to other disciplines, and other
                                                                     special topics in mathematics. Gordon Rule course. Minimum grade of C required if
                                                                     course is used to satisfy Gordon Rule and General Education Requirements.
                                                                  </td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td>&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td><strong>STA&nbsp;1001C. INTRODUCTION TO STATISTICAL REASONING.</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>1</strong></td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">Prerequisite: Min. grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%200018C">MAT&nbsp;0018C</a><span>&nbsp;</span>or higher or appropriate score on an approved assessment. This course provides students
                                                                     with an opportunity to acquire a reasonable level of statistical literacy and expand
                                                                     their understanding of statistical approaches to problem-solving. The main objective
                                                                     of this course is the development of statistical reasoning techniques and an introduction
                                                                     to the statistical analysis process. (Special Fee: $59.00).
                                                                  </td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td>&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                                  <td align="center">&nbsp;</td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td width="703">
                                                                     
                                                                     <p><strong>STA&nbsp;2023. STATISTICAL METHODS.</strong></p>
                                                                     
                                                                  </td>
                                                                  
                                                                  <td align="center" width="54"><strong>3</strong></td>
                                                                  
                                                                  <td align="center" width="138"><strong>3</strong></td>
                                                                  
                                                                  <td align="center" width="56"><strong>0</strong></td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">
                                                                     
                                                                     <p>. Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=STA%201001C">STA&nbsp;1001C</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or satisfactory score on an approved assessment. An introductory statistics course
                                                                        covering collection, description and interpretation of data. Topics include sampling,
                                                                        summarizing data graphically and numerically, probability distributions, confidence
                                                                        interval estimation, hypothesis testing, correlation and regression. Gordon Rule course.
                                                                        Minimum grade of C required if<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=STA%202023">STA&nbsp;2023</a><span>&nbsp;</span>is used to satisfy Gordon Rule and general education requirements.<span>&nbsp;</span></p>
                                                                     
                                                                  </td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td>
                                                                     
                                                                     <p><strong>STA&nbsp;2023H. STATISTICAL METHODS - HONORS.</strong></p>
                                                                     
                                                                  </td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>3</strong></td>
                                                                  
                                                                  <td align="center"><strong>0</strong></td>
                                                                  
                                                               </tr>
                                                               
                                                               <tr>
                                                                  
                                                                  <td colspan="4">
                                                                     
                                                                     <p>Prerequisite: Minimum grade of C in<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=STA%201001C">STA&nbsp;1001C</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT&nbsp;1033C</a><span>&nbsp;</span>or<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC&nbsp;1105</a><span>&nbsp;</span>or satisfactory score on an approved assessment. Same as<span>&nbsp;</span><a href="http://catalog.valenciacollege.edu/search/?P=STA%202023">STA&nbsp;2023</a><span>&nbsp;</span>with honors content. Honors program permission required.
                                                                     </p>
                                                                     
                                                                  </td>
                                                                  
                                                               </tr>
                                                               
                                                            </tbody>
                                                            
                                                         </table>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                </tbody>
                                                
                                             </table>
                                             
                                          </td>
                                          
                                       </tr>
                                       
                                    </tbody>
                                    
                                 </table>
                                 
                              </td>
                              
                           </tr>
                           
                        </tbody>
                        
                     </table>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-west/courses_descriptions.pcf">©</a>
      </div>
   </body>
</html>