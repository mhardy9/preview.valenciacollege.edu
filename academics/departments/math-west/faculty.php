<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty | Math | West Campus | Valencia College</title>
      <meta name="Description" content="Faculty | Math | West Campus">
      <meta name="Keywords" content="college, school, educational, math, west, faculty">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-west/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>West Campus Math Department</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-west/">Math West</a></li>
               <li>Faculty</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2 align="left">Faculty, West Campus Division of Mathematics</h2>
                        
                        <p align="left"><strong><span size="3" style="font-size: medium;">Mail Code: 4-23<br>Telephone Number: (407) 582-Extension</span></strong></p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr class="tableHead_3" valign="top">
                                 
                                 <th align="left" width="40%">Full Time Mathematics Faculty Member</th>
                                 
                                 <th align="left" width="30%">Office</th>
                                 
                                 <th align="left" width="30%">Extension</th>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:jsobkowiak@valenciacollege.edu">Anfinson, Jessica</a></td>
                                 
                                 <td>4-212</td>
                                 
                                 <td>1036</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:jbaez@valenciacollege.edu">Baez, Jose</a></td>
                                 
                                 <td>4-226</td>
                                 
                                 <td>1427</td>
                                 
                              </tr>
                              
                              <tr valign="top" bgcolor="#FFFFFF">
                                 
                                 <td width="40%"><a href="mailto:nbaker@valenciacollege.edu">Baker, Nathan</a></td>
                                 
                                 <td width="30%">4-227</td>
                                 
                                 <td>1398</td>
                                 
                              </tr>
                              
                              <tr valign="top" bgcolor="#FFFFFF">
                                 
                                 <td><a href="mailto:rbrown75@valenciacollege.edu">Carew, Roberta</a></td>
                                 
                                 <td>9-119</td>
                                 
                                 <td>5605</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td width="40%"><a href="mailto:scisneros4@valenciacollege.edu">Cisneros, Simon</a></td>
                                 
                                 <td width="30%">4-229</td>
                                 
                                 <td>5702</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:%20pcristwell@valenciacollege.edu%20">Cristwell, Precious</a></td>
                                 
                                 <td>4-213</td>
                                 
                                 <td>1097</td>
                                 
                              </tr>
                              
                              <tr valign="top" bgcolor="#FFFFFF">
                                 
                                 <td><a href="mailto:acomerford@valenciacollege.edu">Comerford, Amy<span>&nbsp;</span></a>(Interim Dean)
                                 </td>
                                 
                                 <td>7-108D</td>
                                 
                                 <td>1280</td>
                                 
                              </tr>
                              
                              <tr valign="top" bgcolor="#FFFFFF">
                                 
                                 <td><a href="mailto:sdraper2@valenciacollege.edu">Draper, Sandra</a></td>
                                 
                                 <td>3-245</td>
                                 
                                 <td>1436</td>
                                 
                              </tr>
                              
                              <tr valign="top" bgcolor="#FFFFFF">
                                 
                                 <td><a href="mailto:ffarquharson@valenciacollege.edu">Farquharson, Fitzroy</a></td>
                                 
                                 <td>5-159</td>
                                 
                                 <td>1198</td>
                                 
                              </tr>
                              
                              <tr valign="top" bgcolor="#FFFFFF">
                                 
                                 <td width="40%"><a href="mailto:rgarci12@valenciacollege.edu">Garcia, Jose</a></td>
                                 
                                 <td width="30%">4-219</td>
                                 
                                 <td>1056</td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=cgegenheimer">Gegenheimer, Courtney</a></td>
                                 
                                 <td>1-139</td>
                                 
                                 <td>1690</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray">
                                 
                                 <td valign="top" width="40%"><a href="mailto:cgenovesemartinez@valenciacollege.edu">Genovese-Martinez, Claudia</a></td>
                                 
                                 <td valign="top" width="30%">7-108B</td>
                                 
                                 <td valign="top">5109</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><a href="mailto:mgiblin@valenciacollege.edu">Giblin, Melissa</a></td>
                                 
                                 <td valign="top">4-218</td>
                                 
                                 <td valign="top">1333</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td width="40%"><a href="mailto:sgraff@valenciacollege.edu">Graff, Sophia</a></td>
                                 
                                 <td width="30%">4-209</td>
                                 
                                 <td>1673</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td>Hair, Gene</td>
                                 
                                 <td>3-219</td>
                                 
                                 <td>1625</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:lhoward13@valenciacollege.edu">Howard, Lynn</a></td>
                                 
                                 <td>7-212</td>
                                 
                                 <td>5629</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:djohnson190@valenciacollege.edu">Johnson, Daniela</a></td>
                                 
                                 <td>3-256</td>
                                 
                                 <td>1889</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top" width="40%"><a href="mailto:rkasha@valenciacollege.edu">Kasha, Ryan</a></td>
                                 
                                 <td valign="top" width="30%">5-247</td>
                                 
                                 <td valign="top">1475</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><a href="mailto:kkraakmo@valenciacollege.edu">Kraakmo, Kristina</a></td>
                                 
                                 <td valign="top">4-210</td>
                                 
                                 <td valign="top">5815</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><a href="mailto:skrise@valenciacollege.edu">Krise, Scott</a></td>
                                 
                                 <td valign="top">1-243A</td>
                                 
                                 <td valign="top">1884</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=DLACOSTE&amp;CFID=4008993&amp;CFTOKEN=29311130">Lacoste, Darren</a></td>
                                 
                                 <td>4-232</td>
                                 
                                 <td>1087</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><a href="mailto:kmccarthy20@valenciacollege.edu">McCarthy, Kelli</a></td>
                                 
                                 <td valign="top">5-119</td>
                                 
                                 <td valign="top">1024</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:kmilton2@valenciacollege.edu">Milton, Kory</a></td>
                                 
                                 <td>3-120</td>
                                 
                                 <td>1114</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td width="40%"><a href="mailto:akincade@valenciacollege.edu">Montague-Kincade, Amy</a></td>
                                 
                                 <td width="30%">4-221</td>
                                 
                                 <td>1150</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:cmortimer2@valenciacollege.edu">Mortimer, Charlotte</a></td>
                                 
                                 <td>3-238</td>
                                 
                                 <td>1013</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td width="40%"><a href="mailto:bnguyen@valenciacollege.edu">Nguyen, Boris</a></td>
                                 
                                 <td width="30%">7-108C</td>
                                 
                                 <td>1649</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:gnguyen5@valenciacollege.edu">Nguyen, George</a></td>
                                 
                                 <td>5-124</td>
                                 
                                 <td>1298</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><a href="mailto:jnudel@valenciacollege.edu">Nudel, Julia</a></td>
                                 
                                 <td valign="top">5-250</td>
                                 
                                 <td valign="top">1991</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td width="40%"><a href="mailto:dpal@valenciacollege.edu">Pal, Rick</a></td>
                                 
                                 <td width="30%">7-213</td>
                                 
                                 <td>1307</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:ppalmer13@valenciacollege.edu">Palmer, Peter</a></td>
                                 
                                 <td>5-245</td>
                                 
                                 <td>1091</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top" bgcolor="#CCCCCC">
                                 
                                 <td><a href="mailto:rsandefur@valenciacollege.edu">Sandefur, Ryan</a></td>
                                 
                                 <td>4-224</td>
                                 
                                 <td>1794</td>
                                 
                              </tr>
                              
                              <tr class="tableRow_gray" valign="top">
                                 
                                 <td><a href="mailto:pthanki@valenciacollege.edu">Thanki, Puja</a></td>
                                 
                                 <td>5-132</td>
                                 
                                 <td>5298</td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><a href="mailto:nwagh@valenciacollege.edu">Wagh, Niraj</a></td>
                                 
                                 <td>7-215</td>
                                 
                                 <td>1401</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>To reach other faculty members in the West Campus Division of Mathematics, please
                           use the contact information provided in your syllabus, or call (407) 582-1625 or (407)
                           582-1848.
                        </p>
                        
                        <hr>
                        
                        <p>Interested in teaching mathematics? Please visit<span>&nbsp;</span><a href="http://valenciacollege.edu/HR/">Human Resources</a><span>&nbsp;</span>online.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-west/faculty.pcf">©</a>
      </div>
   </body>
</html>