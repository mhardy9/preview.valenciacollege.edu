<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Divisions &amp; Departments | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Academic Divisions &amp; Departments</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Departments</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <h3>Continuing Education</h3>
                           
                           <h4><a href="/LOCATIONS/school-of-public-safety/">School of Public Safety</a></h4>
                           
                           <address>
                              8600 Valencia College Lane
                              <br> Orlando, FL 32825
                              <br> Office: 407-582-8200
                              
                           </address>
                           <a href="/locations/map/public-safety.html">Location Map</a>
                           
                           
                           <h4><a href="/west/">West Campus</a></h4>
                           
                           <address>
                              1800 S Kirkman Road, Building 10
                              <br> Orlando, FL&nbsp; 32811
                              <br> Office: 407-582-6688
                              <br>Fax: 407-582-6610
                              
                           </address>
                           <a href="/map/west.html">Location Map</a>
                           <br> Email: <a href="mailto:ce_info@valenciacollege.edu">ce_info@valenciacollege.edu</a>
                           
                           
                           <h4><a href="http://preview.valenciacollege.edu/continuing-education/">Continuing Education - Learn More</a></h4>
                           
                        </div>
                        
                        <div>
                           
                           <h3>East</h3>
                           
                           <h4><a href="/artsandentertainment/">Arts &amp; Entertainment Division</a></h4>
                           
                           <p>Bldg. 3 Room 106 (3-106)
                              <br> Office: 407-582-2340
                              
                           </p>
                           
                           <ul>
                              
                              <li><a href="/artsandentertainment/artstudio/">Art</a></li>
                              
                              <li><a href="/artsandentertainment/dance/">Dance</a></li>
                              
                              <li><a href="/artsandentertainment/digitalmedia/">Digital Media Technology</a></li>
                              
                              <li><a href="/artsandentertainment/filmtech/">Film</a></li>
                              
                              <li><a href="/artsandentertainment/gallery/">Gallery</a></li>
                              
                              <li><a href="/artsandentertainment/graphicstech/">Graphics Technology</a></li>
                              
                              <li><a href="/artsandentertainment/music/">Music</a></li>
                              
                              <li><a href="/artsandentertainment/musicandsoundtech/">Music And Sound Technology</a></li>
                              
                              <li><a href="/artsandentertainment/Performingarts/">Performing Arts Center</a></li>
                              
                              <li><a href="/artsandentertainment/theater/">Theater</a></li>
                              
                              <li><a href="/artsandentertainment/theater/">Theater Technology</a></li>
                              
                           </ul>
                           
                           <h4><a href="/departments/east/business/">Business, IT &amp; Public Services Division</a></h4>
                           
                           <p>Bldg. 8 Room 105 (8-105)
                              <br> Office: 407-582-2807
                              
                           </p>
                           
                           <ul>
                              
                              <li><a href="/departments/east/business/accounting/">Accounting  Technology</a></li>
                              
                              <li><a href="/departments/east/business/businessadmin/">Business Administration</a></li>
                              
                              <li><a href="/departments/east/business/it/">Computer Information Technology </a></li>
                              
                              <li><a href="/departments/east/business/it/">Computer Programming &amp; Analysis</a></li>
                              
                              <li><a href="/departments/east/business/cjt/">Criminal Justice</a></li>
                              
                              <li><a href="/departments/east/business/it/">Drafting</a></li>
                              
                              <li><a href="/departments/east/business/office/">Office Administration</a></li>
                              
                              <li><a href="/departments/east/business/paralegal/">Paralegal Studies</a></li>
                              
                           </ul>
                           
                           <h4><a href="/east/communications/">Communications Division</a></h4>
                           
                           <p>Bldg. 7 Room 163 (7-163)
                              <br> Office: 407-582-2433 or 407-582-2370
                              
                           </p>
                           
                           <ul>
                              
                              <li>English</li>
                              
                              <li><a href="/eap/">English for Academic Purposes (EAP)</a></li>
                              
                              <li>Journalism</li>
                              
                              <li>Prep English </li>
                              
                              <li>Prep Reading </li>
                              
                              <li>Speech</li>
                              
                              <li>Student Support Lab (English, EAP, Reading) </li>
                              
                           </ul>
                           
                           <h4>Humanities &amp; Foreign Language Division</h4>
                           
                           <p>Bldg. 6 Room 118 (6-118)
                              <br> Office: 407-582-2036 or 407-582-2341
                              
                           </p>
                           
                           <ul>
                              
                              <li><a href="http://preview.valenciacollege.edu/east/humanities/courses-humanities.html">Humanities</a></li>
                              
                              <li><a href="/east/language.html">Foreign Language</a></li>
                              
                           </ul>
                           
                           <h4><a href="/library">Library</a></h4>
                           
                           <p>Bldg. 4
                              <br> Office: 407-582-2467
                              
                           </p>
                           
                           <h4><a href="/east/math/">Mathematics</a></h4>
                           
                           <p>Bldg. 7 Room 142 (7-142)
                              <br> Office: 407-582-2366
                              
                           </p>
                           
                           <h4><a href="/east/science/">Science Department</a></h4>
                           
                           <p>Bldg. 1 Room 216 (1-216)
                              <br> Office: 407-582-2434
                              
                           </p>
                           
                           <ul>
                              
                              <li>Biology</li>
                              
                              <li>Chemistry</li>
                              
                              <li>Earth Science (Geology, Astronomy &amp; Oceanography)</li>
                              
                              <li>Physics (Physical Sciences)</li>
                              
                           </ul>
                           
                           <h4><a href="/east/socialsciences/">Social Sciences &amp; Physical Education Division</a></h4> Bldg. 8 Room 105 (8-105)
                           <br> Office: 407-582-2628
                           
                           <ul>
                              
                              <li><a href="../east/socialsciences/African-AmericanStudies.html">African-American Studies</a></li>
                              
                              <li><a href="../east/socialsciences/AmericanSignLanguage.html">American Sign Language</a></li>
                              
                              <li><a href="../east/socialsciences/Anthropology.html">Anthropology</a></li>
                              
                              <li><a href="../east/socialsciences/Economics.html">Economics</a></li>
                              
                              <li><a href="../east/socialsciences/Education.html">Education</a></li>
                              
                              <li>Geography</li>
                              
                              <li><a href="/east/socialsciences/fitness/">Health &amp; Fitness </a></li>
                              
                              <li><a href="../east/socialsciences/history.html">History</a></li>
                              
                              <li>Physical Education </li>
                              
                              <li><a href="../east/socialsciences/politicalscience.html">Political Science </a></li>
                              
                              <li><a href="../east/socialsciences/Psychology.html">Psychology</a></li>
                              
                              <li><a href="../east/socialsciences/Sociology.html">Sociology</a></li>
                              
                           </ul>
                           
                           <h4><a href="/faculty/resources/east/">Adjunct Faculty - East Campus</a></h4>
                           
                        </div>
                        
                        <div>
                           
                           <h3>Lake Nona</h3>
                           
                           <h4>Office of the Executive Dean</h4>
                           
                           <p>Bldg. 1 Room 302 (1-302)
                              <br> 407-582-7100
                              
                           </p>
                           
                           <h4><a href="/lakenona/student_services.html">Student Services</a></h4> 12350 Narcoossee Rd
                           <br> Orlando, FL 32832
                           <br> 407-582-1507
                           <br> Email: <a href="mailto:jhernandez71@valenciacollege.edu">jhernandez71@valenciacollege.edu</a>
                           
                        </div>
                        
                        <div>
                           
                           <h3>Osceola</h3>
                           
                           <h4>Communication &amp; Languages</h4>
                           
                           <p>Bldg. 1 Room 141 (1-141)
                              <br> 407-582-4262
                              <br> 407-582-4103
                              
                           </p>
                           
                           <h4>Humanities &amp; Social Sciences</h4>
                           
                           <p>Bldg. 3 Room 319 (3-319)
                              <br> 407-582-4108
                              <br> 407-582-4904
                              
                           </p>
                           
                           <h4>Career and Technical Programs</h4>
                           
                           <p>Bldg. 3 Room 319 (3-319)
                              <br> 407-582-4895
                              <br> 407-582-4815
                              
                           </p>
                           
                           <h4>Mathematics &amp; Science</h4>
                           
                           <p>Bldg. 4 Room 231 (4-231)
                              <br> 407-582-4107
                              <br> 407-582-4157
                              
                           </p>
                           
                           <h4><a href="/faculty/resources/osceola/">Adjunct Faculty - Osceola Campus</a></h4>
                           
                        </div>
                        
                        <div>
                           
                           <h3>Poinciana</h3>
                           
                           <h4>Student Services</h4>
                           
                           <p> Room 101
                              <br> Office: 407-582-6504
                              
                           </p>
                           
                           <p>Mon-Thurs: 8am-6pm
                              <br> Friday: 9am-5pm
                              <br> Saturday: closed during non-peak registration
                           </p>
                           
                           <h4>Culinary Department</h4>
                           
                           <p> Room 112
                              <br> Office: 407-582-6083
                           </p>
                           
                           <p>Monday-Friday: 8am-5pm
                              <br> Saturday: closed
                           </p>
                           
                           <h4>Science  Department</h4>
                           
                           <p> Room 326
                              <br> Office: 407-582-6082
                           </p>
                           
                           <p>Monday-Friday: 8am-5pm
                              <br> Saturday: closed
                           </p>
                           
                        </div>
                        
                        <div>
                           
                           <h3>West</h3>
                           
                           <h4><a href="/west/health/">Allied Health Division</a></h4> Bldg. 2 Room 208 (2-208)
                           
                           <ul>
                              
                              <li><a href="/west/health/cehealth/">Continuing Education</a></li>
                              
                              <li><a href="/west/health/cvt/">Cardiovascular Technology</a></li>
                              
                              <li><a href="/west/health/ct/">Computed Tomography</a></li>
                              
                              <li><a href="/west/health/sonography/">Diagnostic Medical Sonography</a></li>
                              
                              <li><a href="/west/health/dental/">Dental Hygiene</a></li>
                              
                              <li><a href="/west/health/ems/">Emergency Medical Services</a></li>
                              
                              <li><a href="/west/health/mri/">Magnetic Resonance Imaging</a></li>
                              
                              <li><a href="/west/health/radiography/">Radiography</a></li>
                              
                              <li><a href="/west/health/respiratory/">Respiratory Care</a></li>
                              
                              <li>Sonography</li>
                              
                           </ul>
                           
                           <h4><a href="/west/arts-and-humanities/">Arts and Humanities</a></h4> Bldg. 5 Room 146 (5-146)
                           <br> Office: 407-582-5730
                           <br> Fax: 407-582-5436
                           
                           <ul>
                              
                              <li>Architecture</li>
                              
                              <li>Art</li>
                              
                              <li>Film</li>
                              
                              <li>Graphics Technology</li>
                              
                              <li>Humanities</li>
                              
                              <li>Interdisciplinary Studies</li>
                              
                              <li>Foreign Languages</li>
                              
                              <li>Music</li>
                              
                              <li>Philosophy</li>
                              
                              <li>Religion</li>
                              
                              <li>Theater</li>
                              
                           </ul>
                           
                           <h4><a href="/west/engineering/">Engineering, Computer Programming, &amp; Technology</a></h4> Bldg. 9 Room 140 (9-140)
                           <br> Office: 407-582-1902
                           <br> Fax: 407-582-1900
                           
                           <ul>
                              
                              <li>Building Construction Technology</li>
                              
                              <li>Computer Engineering Technology (Networking)</li>
                              
                              <li>Civil/Surveying Engineering Technology</li>
                              
                              <li>Computer Information Technology, Computer Programming &amp; Analysis, &amp; Data Base Technology</li>
                              
                              <li>Drafting and Design Technology</li>
                              
                              <li>Electronics Engineering Technology</li>
                              
                              <li>Engineering</li>
                              
                           </ul>
                           
                           <h4><a href="/west/behavioral-and-social-sciences/">Behavioral &amp; Social Sciences</a></h4> Bldg. 11 Room 103 (11-103)<br>
                           Office: 407-582-1203
                           <br> Fax: 407-582-1675
                           
                           <ul>
                              
                              <li>Anthropology</li>
                              
                              <li>Criminal Justice Technology</li>
                              
                              <li>Economics</li>
                              
                              <li>Education</li>
                              
                              <li>Educator Preparation Institute (EPI)</li>
                              
                              <li>Geography</li>
                              
                              <li>History</li>
                              
                              <li>Physical Education</li>
                              
                              <li>Political Science</li>
                              
                              <li>Psychology</li>
                              
                              <li>Sociology</li>
                              
                           </ul>
                           
                           <h4><a href="/library/">Library</a></h4> Bldg. 6
                           <br> Office: 407-582-1210
                           
                           <h4><a href="/west/business-and-hospitality/">Business and Hospitality</a></h4> Bldg. 7 Room 107 (7-107)
                           <br> Office: 407-582-1069
                           
                           <ul>
                              
                              <li>Accounting</li>
                              
                              <li>Business Administration &amp; Management</li>
                              
                              <li>Finance</li>
                              
                              <li>Hospitality and Tourism, Baking and
                                 <br> Pastry Management, Culinary and Restaurant Management
                              </li>
                              
                              <li>Office Systems Technology</li>
                              
                              <li>Real Estate</li>
                              
                           </ul>
                           
                           <h4><a href="/departments/west/communications/">Communications</a></h4> Bldg. 5 Room 231 (5-231)
                           <br> Office: 407-582-1313
                           <br> Fax: 407-582-1676
                           
                           <ul>
                              
                              <li>English</li>
                              
                              <li>English for Academic Purposes</li>
                              
                              <li>Prep English</li>
                              
                              <li>Journalism</li>
                              
                              <li>Prep Reading</li>
                              
                              <li>Speech</li>
                              
                           </ul>
                           
                           <h4><a href="/west/math/">Mathematics</a></h4> Bldg. 7 Room 108 (7-108)
                           <br> Office: 407-582-1625
                           
                           <ul>
                              
                              <li>Mathematics</li>
                              
                              <li>Prep Math</li>
                              
                           </ul>
                           
                           <h4><a href="/west/health/nursingdivision/">Nursing</a></h4> Health Sciences Building (HSB) Room 200
                           <br> Office: 407-582-1278
                           
                           <ul>
                              
                              <li>Continuing Education for Health Professions</li>
                              
                              <li>Nursing</li>
                              
                           </ul>
                           
                           <h4><a href="/departments/west/Science/">Science</a></h4> Allied Health and Sciences Building - AHS-231
                           <br> Office: 407-582-1407
                           
                           <ul>
                              
                              <li>Astronomy</li>
                              
                              <li>Biology</li>
                              
                              <li>Chemistry</li>
                              
                              <li>Environmental Science Technology</li>
                              
                              <li>Geology</li>
                              
                              <li>Landscape &amp; Horticulture Technology</li>
                              
                              <li>Meteorology</li>
                              
                              <li>Oceanography</li>
                              
                              <li>Physical Science</li>
                              
                              <li>Physics</li>
                              
                           </ul>
                           
                           <h4>Learning Support Services</h4> Communications Center: 407-582-5454
                           <br> Math Center: 407-582-1780 &amp; 407-582-1720
                           <br> Testing Center: 407-582-1323
                           <br> Tutoring Center: 407-582-1745
                           
                           <ul>
                              
                              <li><a href="/learning-support/communications/">Communications Center</a></li>
                              
                              <li><a href="/learning-support/math/">Math Center</a></li>
                              
                              <li><a href="/learning-support/testing/">Testing Center</a></li>
                              
                              <li><a href="/learning-support/tutoring/">Tutoring Center</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <div>
                           
                           <h3>Winter Park</h3>
                           
                           <h4><a href="http://net5.valenciacollege.edu/schedule/wp.cfm">Credit Course Department 
                                 </a></h4> Bldg. 1 Room 244 (1-244)
                           <br> Office: 407-582-6802
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/index.pcf">©</a>
      </div>
   </body>
</html>