<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-osceola/mathfair.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Osceola Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-osceola/">Math Osceola</a></li>
               <li>Osceola Campus Math Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2> Osceola's Math Fair </h2>
                        
                        <p>The goal of the Math Depot is to create a learning community that maximizes student
                           success.<br>
                           If you are a new student preparing to take the PERT, or  currently enrolled in one
                           of the following Courses: 
                           
                           
                           MAT0018 Developmental I (formaly known as MAT0012 Pre-Algebra), MAT0028 Developmental
                           II (formaly known as MAT0024 Beginning Algebra),
                           
                           MAT1033 Intermediate Algebra, or you are needing help preparing for a final exam,
                           then the Math Fair is for you. Some Professors even AWARD EXTRA CREDIT to students
                           who attend and complete each station in the Math Fair.<br>
                           
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>
                                       <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" height="177" width="277">
                                          
                                          <param name="movie" value="documents/Mathfairmovie.swf">
                                          
                                          <param name="quality" value="high">
                                          
                                          <embed height="177" pluginspage="http://www.macromedia.com/go/getflashplayer" quality="high" src="documents/Mathfairmovie.swf.html" type="application/x-shockwave-flash" width="277">
                                          </object>
                                       <br>
                                       
                                    </p>
                                    
                                    <p><strong>The Math Fair helps you build your skills. </strong></p>
                                    
                                    <p>Email:  <a href="mailto:osceolamathdepot@valenciacollege.edu">Math Depot</a></p>
                                    
                                    <p>Phone: 407.582.4856 or 321.697.4856<br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>                
                                    
                                    <div>
                                       
                                       <p><span>Math Fair</span> <br>
                                          <br>
                                          
                                       </p>
                                       
                                       <p>Raffle to win a fabulous prize every hour!</p>
                                       
                                       <p>Enjoy Delicious Free Food</p>
                                       
                                       <p>• Opportunity to meet Math Professors<br>
                                          <br>
                                          • Join any one of many campus clubs
                                       </p>
                                       
                                       <p><br>
                                          <br>
                                          <br>
                                          
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        <br>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-osceola/mathfair.pcf">©</a>
      </div>
   </body>
</html>