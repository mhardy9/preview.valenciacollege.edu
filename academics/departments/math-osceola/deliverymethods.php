<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-osceola/deliverymethods.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Osceola Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-osceola/">Math Osceola</a></li>
               <li>Osceola Campus Math Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <p>
                           <a name="content" id="content"></a>
                           
                        </p>
                        
                        
                        
                        
                        <h2>Delivery Methods            </h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h3><strong>Traditional Courses</strong></h3>
                                    </div>
                                    
                                    <div>
                                       <h3><strong>Supplemental Learning Courses</strong></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><span>Traditional courses take place on campus. Many instructors use lecture 
                                             as a form of delivery for the traditional courses. Depending on the 
                                             instructor, different resources may be available on their Faculty Web sites or 
                                             through WebCT (an online component). These courses can meet anywhere 
                                             from 1 to 3 times a week depending on the schedule that fits your needs.</span></p>
                                       
                                       <p><br>
                                          
                                       </p>
                                       
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>This kind of course is similar to the Traditional course with a small 
                                             difference. These courses are enhanced by Supplemental Learning {SL}. <br>
                                             SL classes are supported by small group sessions led by Supplemental 
                                             Learning Leaders (former students) who are selected because they passed 
                                             the course with a high grade. These study sessions are regularly 
                                             scheduled, casual sessions in which students from your class compare 
                                             notes discuss assignments, and develop organizational tools and study 
                                             skills. Students who participate in these sessions make better grades 
                                             in the course. <br>
                                             For more information and a  schedule of SL courses click <a href="../../supplemental-learning/index.html">here</a>.<br>
                                             <br>
                                             <em>Or contact <a href="mailto:agroccia@valenciacollege.edu">Prof. Al Groccia</a>, the coordinator on the Osceola Campus</em></p>
                                          
                                          <p><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h3><strong>LinC Courses</strong></h3>
                                    </div>
                                    
                                    <div>
                                       <h3><strong>Academic Systems</strong></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>A LinC course matches two courses that students take together. They make   friends,
                                          study together, and succeed together. LinC professors team to connect   course materials
                                          and help students put the pieces together. LinC creates a   "community of learners"
                                          where everyone learns together! For more information and a  schedule of LinC courses
                                          click <a href="../../linc/LinCSchedule/LinCSchedules.asp.html">here.</a></p>
                                       
                                       <p><em>Or contact<a href="mailto:mpierre23@valenciacollege.edu"> Prof. Mia Pierre</a>, the coordinator on the Osceola Campus</em></p>                    
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <p>Students complete these courses entirely on the <em><strong>computer </strong></em>in a classroom environment. Through the 
                                             use of this highly engaging, advanced multimedia software application, as well 
                                             as the personalized one on one attention from our instructors, and in-class tutors,
                                             students will be 
                                             able to master the needed skills for success in the available courses. 
                                             Students can move at an accelerated pace making it possible to complete a 
                                             course early and continue on to the next course in the same semester.
                                          </p>
                                          
                                       </div>
                                       
                                       <p><em>To take a short tour of  
                                             Academic Systems click <a href="documents/ASTour.swf.html">here </a></em></p>                    
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h3>Hybrid/Blended</h3>
                                    </div>
                                    
                                    <div>
                                       <h3>Online</h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>A  course that blends online and face-to-face delivery. If a course is not over  75%
                                          online, over  75% videotape, or over 75% on-site it is called a Hybrid course. A 
                                          hybrid/blended delivery may  be On-site/Online, Hybrid On-site/DVD, etc. What makes
                                          a course a hybrid is  simply the degree  of use of the alternative modality.
                                       </p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p>A  course in which all of the content is delivered online using the  college-approved
                                          course management  system (WebCT).
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="DeliveryMethods.html#top">TOP</a></p>
                        
                        <h2>&nbsp;</h2>          
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-osceola/deliverymethods.pcf">©</a>
      </div>
   </body>
</html>