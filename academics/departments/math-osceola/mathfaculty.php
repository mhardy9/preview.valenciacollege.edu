<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-osceola/mathfaculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Osceola Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-osceola/">Math Osceola</a></li>
               <li>Osceola Campus Math Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Math Faculty</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Full Time Math Support </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>  <br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Jforsythe" height="123" src="johnie.jpg" width="123"> <br>
                                       <a href="mailto:jforsythe@valenciacollege.edu">Johnie Forsythe</a><br>
                                       <em>Math Coordinator</em> 
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="aberry" height="122" src="aberry.jpg" width="122"><br>
                                       <a href="mailto:aberry@valenciacollege.edu">Andi Berry</a><br>
                                       <em>Instructional Assistant </em></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p> <img alt="Dan DeRosa" height="116" src="025_002.jpg" width="157"> <br>
                                       <a href="mailto:dderosa1@valenciacollege.edu">Dan DeRosa</a><br>
                                       <em>Senior Instructional Assistant</em> <br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:ewashington3@valenciacollege.edu"></a><img alt="Prof. Miranda, A" height="110" src="amiranda_000.jpg" width="148"><br>
                                       <a href="mailto:amiranda@valenciacollege.edu">Prof. Miranda,A</a><br>
                                       <em>Senior Instructional Assistant</em> <br>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Full Time Math Faculty </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>                                    
                                    <p><a href="mailto:aashkani@valenciacollege.edu">Prof. Ashkani, A</a></p>
                                    
                                 </div>
                                 
                                 <div>                  
                                    <p><img alt="Dr. Bush, W" border="0" height="102" src="wendi.jpg" width="99"><br>
                                       <a href="mailto:wbush@valenciacollege.edu">Dr. Bush, W</a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><img alt="Prof. E. Elrod" height="104" src="eelrod.jpg" width="122"><br>
                                          <a href="mailto:eelrod1@valenciacollege.edu">Prof. Elrod, E </a></p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Prof. Emmanuel, M" height="110" src="emmanuel_000.jpg" width="101"><a href="mailto:memmanuel@valenciacollege.edu"><br>
                                          Prof. Emmanuel, M</a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Prof. Groccia, A" height="102" src="al_groccia.jpg" width="138"><a href="mailto:agroccia@valenciacollege.edu"><br>
                                          Prof. Groccia, A</a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Prof. Stephanie Melzer" height="104" src="stephanie.jpg" width="92"><br>
                                       <a href="mailto:smelzer@mail.valenciacollege.edu">Stephanie Kokaisel </a>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:kmilton2@valenciacollege.edu">Prof. Milton, K </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:lnegron@valenciacollege.edu">Prof. Negron, L</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:trivero@valenciacollege.edu">Prof. Riverio, T </a></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:gscible@valenciacollege.edu"><br>
                                          Prof. Scible, G <br>
                                          </a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Prof. Sisson, L" height="99" src="lsisson.jpg" width="132"><a href="mailto:lsisson@valenciacollege.edu"><br>
                                          Prof. Sisson, L</a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <img alt="Prof. Allison Sloan" height="101" src="facultypicture.jpg" width="134"><a href="mailto:asloan@valenciacollege.edu"><br>
                                          Prof. Sloan, A</a>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <p><img alt="Stevens" height="105" src="Stevens.jpg" width="80"><br>
                                             <a href="mailto:jstevens41@valenciacollege.edu">Prof. Stevens, J </a></p>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:mmcdowell5@valenciacollege.edu">Prof. Thompson, M </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>                
                                 
                                 <div>Part-Time Faculty</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:aabad@mail.valenciacollege.edu">Prof. Abad, A</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:pkinsella@valenciacollege.edu">Prof. Kinsella, P</a></div>
                                 </div>
                                 
                                 <div><a href="mailto:twarrell@valenciacollege.edu">Prof. Warrell, T</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:abolanodelahoz@atlas.valenciacollege.edu"></a><a href="mailto:vbarnett2@valenciacollege.edu">Prof. Barnett,V</a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:klazaar@valenciacollege.edu">Prof. Lazaar, K </a></div>
                                 </div>
                                 
                                 <div><a href="mailto:ewashington3@valenciacollege.edu">Prof. Washington, E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:zblack@valenciacollege.edu">Prof. Black, Z </a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:cpersaud5@atlas.valenciacollege.edu"></a><a href="mailto:mmcdowell5@mail.valenciacollege.edu">Prof. McDowell, M</a>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:cbrennan12@valenciacollege.edu">Prof. Brennan, C </a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:lmarrerozamora@atlas.valenciacollege.edu"></a><a href="mailto:lmcnealy@mail.valenciacollege.edu">Prof. McNealy, L</a>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:ecelestin@valenciacollege.edu">Prof. Celestin, E </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:rmurdoch@valenciacollege.edu">Prof. Murdoch, R </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:kcollier@valenciacollege.edu">Prof. Collier, K </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:mosbourne@valenciacollege.edu">Prof. Osbourne, M </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:dcollins33@mail.valenciacollege.edu">Prof.Collins, D</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:npatel48@valenciacollege.edu">Prof. Patel, N </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:jcolon132@valenciacollege.edu">Prof. Colon, J</a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:rsallee@atlas.valenciacollege.edu"></a><a href="mailto:ygutierrez2@valenciacollege.edu">Prof. Paulino, Y </a>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:mdiaz94@mail.valenciacollege.edu">Prof. Diaz, M</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:cpersaud5@mail.valenciacollege.edu">Prof. Persaud, C</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:ddillman@mail.valenciacollege.edu">Prof. Dillman, D</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:lmarrerozamora@mail.valenciacollege.edu">Prof. Pettett, L</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:jdoerr3@mail.valenciacollege.edu">Prof. Doerr, J</a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:fponceano@valenciacollege.edu">Prof. Ponceano, F</a> 
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:leagen1@mail.valenciacollege.edu">Prof. Eagen, L</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:mratliff1@valenciacollege.edu">Prof. Ratliff, M</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:hesteban@mail.valenciacollege.edu">Prof. Esteban, H</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:frestituyo@valenciacollege.edu">Prof. Restituyo, F</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:sestrella@valenciacollege.edu">Prof. Estrella, S</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:crethwisch1@valenciacollege.edu">Prof. Rethwisch, C </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:gfagiolino@atlas.valenciacollege.edu"></a><a href="mailto:gfagiolino@mail.valenciacollege.edu">Prof. Fagiolino, G </a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:jruge@valenciacollege.edu">Prof. Ruge, J </a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:cspear@atlas.valenciacollege.edu%20"></a></div>
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:hfeldkamp1@mail.valenciacollege.edu">Prof. Feldkamp</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:fsepulveda3@valenciacollege.edu">Prof. Sepulveda ,F</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><a href="mailto:agreller@atlas.valenciacollege.edu"></a><a href="mailto:sferguson8@mail.valenciacollege.edu">Prof. Ferguson, S</a><br>
                                          
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:fshamieh@valenciacollege.edu">Prof. Shamieh, F</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:nstarr1@atlas.valenciacollege.edu"></a></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Prof. Figueroa, K</div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:tsheppard@valenciacollege.edu">Prof. Sheppard, T </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:mlamb3@atlas.valenciacollege.edu"></a><a href="mailto:jgorman@mail.valenciacollege.edu">Prof. Gorman, J</a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="mailto:nstarr1@valenciacollege.edu">Prof. Starr, N</a> 
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:nstarr1@atlas.valenciacollege.edu"></a></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="mailto:agreller@mail.valenciacollege.edu">Prof. Greller, A</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="mailto:lvasquez11@valenciacollege.edu">Prof. Vasquez, L </a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="MathFaculty.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-osceola/mathfaculty.pcf">©</a>
      </div>
   </body>
</html>