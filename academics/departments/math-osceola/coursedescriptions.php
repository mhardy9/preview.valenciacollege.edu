<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-osceola/coursedescriptions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Osceola Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-osceola/">Math Osceola</a></li>
               <li>Osceola Campus Math Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Course Descriptions</h2>
                        
                        <p><br>
                           Click on the course to see a description and prerequisites
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="documents/MAT0020C.rtf">MAT 0020C</a><br>
                                       Prep Math Intensive
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAT0018C.rtf">MAT 0018C</a><br>
                                       Developmental I<br> 
                                       <em>(formerly known as MAT0012 Pre-Algebra),
                                          </em>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><a href="documents/MAT0018N.rtf">MAT 0018<strong>N</strong><strong></strong></a><strong><br>
                                             </strong>Intensive Mathematics
                                          Tutorial<br>
                                          
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><a href="documents/MAT0028C.rtf">MAT 0028C</a><br>
                                          MAT0028 Developmental II<br>
                                          <em>(formerly known as MAT0024<br> 
                                             Beginning Algebra)</em><br>
                                          
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAT0028N.rtf">MAT 0028<strong>N</strong></a><br>
                                       Intensive Mathematics
                                       Tutorial
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAT1033C.rtf">MAT 1033</a><br>
                                       Intermediate Algebra
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAC1105.rtf">MAC 1105</a><br>
                                       College Algebra
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAC1114.rtf">MAC 1114</a><br>
                                       College Trigonometry
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             <a href="documents/MAC1140.rtf">MAC 1140</a><br>
                                             Precalculus Algebra
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAC1147.rtf">MAC 1147</a><br>
                                       Precalculus
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAC2233.rtf">MAC 2233</a><br>
                                       Business Calculus
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAC2311.rtf">MAC 2311</a><br>
                                       Calculus with Analytic Geometry I
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><a href="documents/MAC2312.rtf">MAC 2312</a><br>
                                          Calculus with Analytic Geometry II
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          <a href="documents/MAC2313.rtf">MAC 2313</a><br>
                                          Calculus with Analytic<br>
                                          Geometry III
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAE2801.rtf">MAE 2801</a><br>
                                       Elementary School Mathematics
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAC2302.rtf">MAP 2302</a><br>
                                       Differential Equations 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MAS2103.rtf">MAS 2103</a><br>
                                       Introduction to Linear Algebra
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          <a href="documents/MGF1106.rtf">MGF 1106</a><br>
                                          College Mathematics
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MGF1107.rtf">MGF 1107</a><br>
                                       Mathematics for the Liberal Arts
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/MHF2300.rtf">MHF 2300</a><br>
                                       Logic and Proof in Mathematics
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <a href="documents/STA2023.rtf">STA 2023</a><br>
                                       Statistical Methods
                                    </div>
                                 </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="CourseDescriptions.html#top">TOP</a></p>          
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-osceola/coursedescriptions.pcf">©</a>
      </div>
   </body>
</html>