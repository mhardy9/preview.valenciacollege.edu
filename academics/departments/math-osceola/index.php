<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-osceola/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Osceola Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Math Osceola</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Welcome to the Osceola Campus Math Department</h2>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <a href="MathEquipmentCheckOut(New).html"></a><a href="MathEquipmentCheckOut.html">Math Lab Equipment for Student &amp; Faculty and Staff to check out</a> 
                                    
                                    <p><a href="Prospectivestudents/index.html"> I have not taken a Math class, what should I do?</a> 
                                    </p>
                                    
                                    <div>
                                       
                                       <p><a href="Currentstudents/index.html">I am not sure which Math class  I need to take.</a></p>
                                       
                                       <p><a href="MathHelp.html">I need help with my Math class. </a></p>
                                       
                                       <p>                          <a href="MathFaculty.html">Who teaches Math on the Osceola Campus?</a></p>
                                       
                                       <p>                            <a href="MathDepot.html">Math Depot </a></p>
                                       
                                    </div>                    
                                    
                                    <div>
                                       
                                       
                                    </div>                    
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><br>
                                          <a href="mailto:mpedone@valenciacollege.edu"><img alt="Dr. Pedone, Dean of Mathematics" height="115" src="mpedone.jpg" width="119"><br>
                                             Dr. Melissa Pedone</a><br>
                                          Dean of Math and Science<br>
                                          Building 4 Room 231A
                                       </p>
                                       
                                       <p><img alt="Johnie Forsythe" height="123" src="johnie.jpg" width="123"><br>
                                          <a href="mailto:jforsythe3@valenciacollege.edu">Johnie Forsythe</a> <br>
                                          Math Coordinator<br>
                                          Phone 407.582.4178<br>
                                          Building 4 Room 231B                        <br>
                                          
                                       </p>
                                       
                                       <div>
                                          
                                          <p><br>
                                             <a href="mailto:molivier4@valenciacollege.edu"><img alt="Melanie Oliver" height="127" hspace="0" src="melanieOliver.jpg" vspace="0" width="109"><br>                        
                                                Melanie Olivier </a><br>
                                             Administrative Assistant<br>
                                             to the Dean<br>
                                             Phone 407.582.4107<br>
                                             Fax 407.582.4843<br>
                                             Building 4 Room 231 
                                          </p>
                                          
                                       </div>
                                       
                                       <p>                        <img alt="Lisandra Berrios-Henriquez" height="128" src="Lisy.jpg" width="105"><br>
                                          <a href="mailto:lberrios@valenciacollege.edu">Lisandra Berrios-Henriquez</a><br>
                                          Administrative Assistant<br>
                                          to the Dean<br>
                                          Phone 407.582.4157<br>
                                          Fax 407.582.4843<br>
                                          Building 4 Room 231 
                                       </p>
                                       
                                       
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><a href="MathDepot.html"></a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-osceola/index.pcf">©</a>
      </div>
   </body>
</html>